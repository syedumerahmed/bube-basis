package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Quelle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "quelle_seq")
    @SequenceGenerator(name = "quelle_seq", allocationSize = 50)
    private Long id;

    private String referenzAnlagenkataster;

    @NotNull
    private String quelleNr;

    // Eine Quelle kann eine Eltern Betriebsstätte haben
    private Long parentBetriebsstaetteId;

    // Eine Quelle kann eine Eltern Anlage habnen
    private Long parentAnlageId;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    @NotNull
    private String name;

    @ManyToOne
    private Referenz quellenArt;

    private Double flaeche;
    private Double laenge;
    private Double breite;
    private Double durchmesser;
    private Double bauhoehe;

    private String bemerkung;

    private boolean betreiberdatenUebernommen;
    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    private Instant letzteAenderungBetreiber;

    @Deprecated
    protected Quelle() {
    }

    public Quelle(String name, String quelleNr) {
        Assert.notNull(quelleNr, "quelleNr darf nicht null sein");
        Assert.notNull(name, "name darf nicht null sein");
        Assert.isTrue(!name.isEmpty(), "name darf nicht leer sein");
        this.name = name;
        this.quelleNr = quelleNr;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getReferenzAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    public void setReferenzAnlagenkataster(final String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
    }

    public String getQuelleNr() {
        return quelleNr;
    }

    public void setQuelleNr(final String quelleNr) {
        this.quelleNr = quelleNr;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Referenz getQuellenArt() {
        return quellenArt;
    }

    public void setQuellenArt(final Referenz quellenArt) {
        this.quellenArt = quellenArt;
    }

    public Double getFlaeche() {
        return flaeche;
    }

    public void setFlaeche(final Double flaeche) {
        this.flaeche = roundThreeDigits(flaeche);
    }

    public Double getLaenge() {
        return laenge;
    }

    public void setLaenge(final Double laenge) {
        this.laenge = roundThreeDigits(laenge);
    }

    public Double getBreite() {
        return breite;
    }

    public void setBreite(final Double breite) {
        this.breite = roundThreeDigits(breite);
    }

    public Double getDurchmesser() {
        return durchmesser;
    }

    public void setDurchmesser(final Double durchmesser) {
        this.durchmesser = roundThreeDigits(durchmesser);
    }

    public Double getBauhoehe() {
        return bauhoehe;
    }

    public void setBauhoehe(final Double bauhoehe) {
        this.bauhoehe = roundTwoDigits(bauhoehe);
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(final String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(final Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(final Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    public void setParentBetriebsstaetteId(final Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
    }

    public Long getParentAnlageId() {
        return parentAnlageId;
    }

    public void setParentAnlageId(final Long parentAnlageId) {
        this.parentAnlageId = parentAnlageId;
    }

    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public void setBetreiberdatenUebernommen(final boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
    }

    public Instant getLetzteAenderungBetreiber() {
        return letzteAenderungBetreiber;
    }

    public void setLetzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
    }

    /**
     * Prüft erst die id und wenn die null ist den fachlichen Key
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Quelle quelle = (Quelle) o;

        return id != null ? id.equals(quelle.id) : this.hasSameKey(quelle);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isNew() {
        return id == null;
    }

    private static Double roundThreeDigits(final Double number) {
        return number != null ? Math.round(number * 1000.0) / 1000.0 : null;
    }

    private static Double roundTwoDigits(final Double number) {
        return number != null ? Math.round(number * 100.0) / 100.0 : null;
    }

    public boolean istKindVonAnlage() {
        return this.parentAnlageId != null;
    }

    public boolean istKindVonBetriebsstaette() {
        return this.parentAnlageId == null;
    }

    public boolean hasSameKey(final Quelle other) {
        return quelleNr.equals(other.quelleNr) && (this.parentAnlageId != null ?
                parentAnlageId.equals(other.parentAnlageId) :
                Objects.equals(parentBetriebsstaetteId, other.parentBetriebsstaetteId));
    }

    public void markiereAlsUebernommen() {
        this.betreiberdatenUebernommen = true;
        this.letzteAenderungBetreiber = null;
    }
}
