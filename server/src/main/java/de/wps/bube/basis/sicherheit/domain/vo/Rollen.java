package de.wps.bube.basis.sicherheit.domain.vo;

/**
 * Alle BUBE-Rollen. String-Konstanten zur Nutzung in Annotations.
 */
public final class Rollen {

    private Rollen() {
    }

    public static final String ROLE_PREFIX = "ROLE_";

    public static final String BEHOERDE_READ = ROLE_PREFIX + "Behoerde_Read";
    public static final String BEHOERDE_WRITE = ROLE_PREFIX + "Behoerde_Write";
    public static final String BEHOERDE_DELETE = ROLE_PREFIX + "Behoerde_Delete";

    public static final String LAND_READ = ROLE_PREFIX + "Land_Read";
    public static final String LAND_WRITE = ROLE_PREFIX + "Land_Write";
    public static final String LAND_DELETE = ROLE_PREFIX + "Land_Delete";

    public static final String ADMIN = ROLE_PREFIX + "Admin";
    public static final String GESAMTADMIN = ROLE_PREFIX + "Gesamtadmin";

    public static final String REFERENZLISTEN_USE = ROLE_PREFIX + "Referenzlisten_Use";
    public static final String REFERENZLISTEN_BUND_READ = ROLE_PREFIX + "Referenzlisten_Bund_Read";
    public static final String REFERENZLISTEN_BUND_WRITE = ROLE_PREFIX + "Referenzlisten_Bund_Write";
    public static final String REFERENZLISTEN_BUND_DELETE = ROLE_PREFIX + "Referenzlisten_Bund_Delete";

    public static final String REFERENZLISTEN_LAND_READ = ROLE_PREFIX + "Referenzlisten_Land_Read";
    public static final String REFERENZLISTEN_LAND_WRITE = ROLE_PREFIX + "Referenzlisten_Land_Write";
    public static final String REFERENZLISTEN_LAND_DELETE = ROLE_PREFIX + "Referenzlisten_Land_Delete";

    public static final String BENUTZERVERWALTUNG_READ = ROLE_PREFIX + "Benutzerverwaltung_Read";
    public static final String BENUTZERVERWALTUNG_WRITE = ROLE_PREFIX + "Benutzerverwaltung_Write";
    public static final String BENUTZERVERWALTUNG_DELETE = ROLE_PREFIX + "Benutzerverwaltung_Delete";

    public static final String BETREIBER = ROLE_PREFIX + "Betreiber";
    public static final String BEHOERDENBENUTZER = ROLE_PREFIX + "Behoerdenbenutzer";

}
