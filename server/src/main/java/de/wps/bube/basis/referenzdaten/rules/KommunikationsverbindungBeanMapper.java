package de.wps.bube.basis.referenzdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.rules.bean.KommunikationsverbindungBean;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class KommunikationsverbindungBeanMapper {

    public abstract KommunikationsverbindungBean toBean(Kommunikationsverbindung kommunikationsverbindung);

}
