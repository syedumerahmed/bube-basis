package de.wps.bube.basis.benutzerverwaltung.web;

import static java.util.stream.Collectors.toList;

import java.io.ByteArrayOutputStream;
import java.util.List;
import javax.validation.Valid;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.ValidierungshinweisMapper;
import de.wps.bube.basis.base.web.openapi.api.BenutzerApi;
import de.wps.bube.basis.base.web.openapi.model.BenutzerDto;
import de.wps.bube.basis.base.web.openapi.model.DateiDto;
import de.wps.bube.basis.base.web.openapi.model.ExportiereBenutzerCommand;
import de.wps.bube.basis.base.web.openapi.model.UpdateProfile;
import de.wps.bube.basis.base.web.openapi.model.Validierungshinweis;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungImportExportService;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class BenutzerRestController implements BenutzerApi {

    private final AktiverBenutzer aktuellerAktiverBenutzer;

    private final BenutzerverwaltungService benutzerverwaltungService;
    private final BenutzerverwaltungImportExportService benutzerverwaltungImportExportService;
    private final StammdatenService stammdatenService;
    private final BenutzerMapper mapper;
    private final ValidierungshinweisMapper validierungshinweisMapper;

    BenutzerRestController(AktiverBenutzer aktuellerAktiverBenutzer,
            BenutzerverwaltungService benutzerverwaltungService,
            BenutzerverwaltungImportExportService benutzerverwaltungImportExportService,
            StammdatenService stammdatenService, BenutzerMapper mapper,
            ValidierungshinweisMapper validierungshinweisMapper) {
        this.aktuellerAktiverBenutzer = aktuellerAktiverBenutzer;
        this.benutzerverwaltungService = benutzerverwaltungService;
        this.benutzerverwaltungImportExportService = benutzerverwaltungImportExportService;
        this.stammdatenService = stammdatenService;
        this.mapper = mapper;
        this.validierungshinweisMapper = validierungshinweisMapper;
    }

    @Override
    @Valid
    public ResponseEntity<List<BenutzerDto>> listAllBetreiberBenutzer() {
        return ResponseEntity.ok(benutzerverwaltungService.listAllBetreiberBenutzer()
                                                          .map(mapper::toDto)
                                                          .map(this::addBehoerdenSchluesselForBetreiberBenutzer)
                                                          .toList());
    }

    private BenutzerDto addBehoerdenSchluesselForBetreiberBenutzer(BenutzerDto benutzer) {
        var betriebsstaettenNummern = benutzer.getDatenberechtigung().getBetriebsstaetten();

        benutzer.setBehoerdenSchluessel(this.stammdatenService.findAllZustaendigeBehoerdenSchluessel(
                betriebsstaettenNummern, Land.of(benutzer.getDatenberechtigung().getLand())));

        return benutzer;
    }

    @Override
    @Valid
    public ResponseEntity<List<BenutzerDto>> listAllBehoerdeBenutzer() {
        return ResponseEntity.ok(
                benutzerverwaltungService.listAllBehoerdenBenutzer().map(mapper::toDto).collect(toList()));
    }

    @Override
    @Valid
    public ResponseEntity<BenutzerDto> loadBenutzer(String benutzername) {
        var benutzer = isCurrentUser(benutzername) ?
                benutzerverwaltungService.loadCurrentBenutzer() :
                benutzerverwaltungService.loadBenutzerByName(benutzername);
        return ResponseEntity.ok(mapper.toDto(benutzer));
    }

    @Override
    @Valid
    public ResponseEntity<BenutzerDto> createBenutzer(BenutzerDto benutzerDto) {
        return ResponseEntity.ok(mapper.toDto(benutzerverwaltungService.createBenutzer(mapper.fromDto(benutzerDto))));
    }

    @Override
    @Valid
    public ResponseEntity<BenutzerDto> updateBenutzer(BenutzerDto benutzerDto) {
        var benutzer = mapper.fromDto(benutzerDto);
        if (isCurrentUser(benutzer.getBenutzername())) {
            return ResponseEntity.ok(mapper.toDto(benutzerverwaltungService.updateCurrentBenutzer(benutzer)));
        }
        return ResponseEntity.ok(mapper.toDto(benutzerverwaltungService.updateBenutzer(benutzer)));
    }

    @Override
    public ResponseEntity<Void> updateProfile(UpdateProfile updateProfileCommand) {
        benutzerverwaltungService.updateProfile(updateProfileCommand.getEmail());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteBenutzer(String benutzername) {
        benutzerverwaltungService.deleteBenutzer(benutzername);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> resetPassword(String benutzername) {
        if (isCurrentUser(benutzername)) {
            benutzerverwaltungService.resetOwnPassword();
        } else {
            benutzerverwaltungService.resetPassword(benutzername);
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> resetTwoFactor(String benutzername) {
        if (isCurrentUser(benutzername)) {
            benutzerverwaltungService.resetOwnTwoFactor();
        } else {
            benutzerverwaltungService.resetTwoFactor(benutzername);
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<DateiDto> existierendeDatei() {
        DateiDto dateiDto = new DateiDto();
        dateiDto.setDateiName(
                benutzerverwaltungImportExportService.existierendeDatei(aktuellerAktiverBenutzer.getUsername()));
        return ResponseEntity.ok(dateiDto);
    }

    @Override
    public ResponseEntity<Void> upload(MultipartFile file) {
        benutzerverwaltungImportExportService.dateiSpeichern(file, aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Validierungshinweis>> validiere() {
        return ResponseEntity.ok(benutzerverwaltungImportExportService.validiere(
                aktuellerAktiverBenutzer.getUsername()).stream().map(validierungshinweisMapper::mapToDto).toList());
    }

    @Override
    public ResponseEntity<Resource> exportBenutzer(ExportiereBenutzerCommand exportiereBenutzerCommand) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        benutzerverwaltungImportExportService.exportBenutzer(outputStream, exportiereBenutzerCommand.getBenutzerIds());

        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType("application/octet-stream"))
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"benutzer.xml\"")
                             .body(byteArrayResource);
    }

    @Override
    public ResponseEntity<Void> importBenutzer() {
        benutzerverwaltungImportExportService.triggerImport(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> dateiLoeschen() {
        benutzerverwaltungImportExportService.dateiLoeschen(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<BenutzerDto>> createTempPasswords(List<String> benutzerIds) {
        var updatedBenutzer = benutzerverwaltungService.createTemporaryPasswordsForBenutzer(benutzerIds);
        var response = updatedBenutzer.stream()
                                      .map(mapper::toDto)
                                      .map(this::addBehoerdenSchluesselForBetreiberBenutzer)
                                      .toList();
        return ResponseEntity.ok(response);
    }

    private boolean isCurrentUser(String benutzername) {
        return aktuellerAktiverBenutzer.getUsername().equals(benutzername);
    }

}
