package de.wps.bube.basis.euregistry.xml.bericht.dto.pf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base.Identifier;

@XmlAccessorType(XmlAccessType.FIELD)
public class InspireID {

    @XmlElement(required = true, name = "Identifier", namespace = SchemaSet.SCHEMA_BASE)
    private Identifier identifier;

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }
}
