package de.wps.bube.basis.euregistry.xml.bericht;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;

@Mapper
public abstract class ReportDataMapper {

    @Mapping(target = "reportingYear", source = ".")
    @Mapping(target = "countryId.code", constant = "DE")
    @Mapping(target = "countryId.schema", constant = SchemaSet.COUNTRY_CODES)
    public abstract ReportData mapReportData(String berichtsjahrSchluessel);
}
