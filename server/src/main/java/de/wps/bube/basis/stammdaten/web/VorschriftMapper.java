package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.VorschriftDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;

@Mapper(uses = { ReferenzMapper.class })
public abstract class VorschriftMapper {

    public abstract VorschriftDto toDto(Vorschrift vorschrift);

    @Mapping(target = "taetigkeitenOhneHaupttaetigkeit", ignore = true)
    public abstract Vorschrift fromDto(VorschriftDto vorschriftDto);

}
