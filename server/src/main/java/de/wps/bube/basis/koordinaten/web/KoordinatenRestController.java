package de.wps.bube.basis.koordinaten.web;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.KoordinatenApi;
import de.wps.bube.basis.base.web.openapi.model.ImportiereGemeindekoordinatenCommand;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenImportService;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;

@Controller
class KoordinatenRestController implements KoordinatenApi {

    private final KoordinatenImportService service;
    private final ReferenzMapper referenzMapper;

    public KoordinatenRestController(KoordinatenImportService service,
            ReferenzMapper referenzMapper) {
        this.service = service;
        this.referenzMapper = referenzMapper;
    }

    @Override
    public ResponseEntity<Void> importGemeindekoordinaten(
            ImportiereGemeindekoordinatenCommand importiereGemeindekoordinatenCommand) {
        Referenz zieljahr = referenzMapper.fromDto(importiereGemeindekoordinatenCommand.getZieljahr());
        String downloadLink = importiereGemeindekoordinatenCommand.getDownloadLink();

        service.triggerImport(downloadLink,zieljahr);

        return ResponseEntity.ok().build();
    }
}
