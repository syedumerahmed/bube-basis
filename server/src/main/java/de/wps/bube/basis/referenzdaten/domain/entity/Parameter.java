package de.wps.bube.basis.referenzdaten.domain.entity;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.referenzdaten.domain.vo.ParameterTyp;

/**
 * Applikationsparameter (EPARA)
 */
@Entity
public class Parameter {

    public static final String URL_VISIT_KEY = "url_visit";
    public static final String URL_MONI_KEY = "url_moni";
    public static final String URL_PUBLIC_DISCLOSURE_KEY = "url_pdiscl";

    public static final String QUELLE_ZUORDNUNG = "que_zuord";
    public static final String GEO_BUFFER = "geo_buff";

    public static final String INSP_NS = "insp_ns";
    public static final String INSP_SITE = "insp_site";
    public static final String INSP_EXTENSION = "insp_extension";

    @Id
    private String schluessel;

    // Bezeichnung des Steuerungsparameters
    @NotNull
    private String text;

    // Legt den Typ des Parameters fest
    @NotNull
    @Enumerated(EnumType.STRING)
    private ParameterTyp typ;

    @NotNull
    private boolean fuerBehoerden;

    @Deprecated
    protected Parameter() {
    }

    public Parameter(String schluessel, ParameterTyp typ, String text, boolean fuerBehoerden) {
        this.schluessel = schluessel;
        this.text = text;
        this.typ = typ;
        this.fuerBehoerden = fuerBehoerden;
    }

    public String getSchluessel() {
        return schluessel;
    }

    public void setSchluessel(String schluessel) {
        this.schluessel = schluessel;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ParameterTyp getTyp() {
        return typ;
    }

    public void setTyp(ParameterTyp typ) {
        this.typ = typ;
    }

    public boolean isFuerBehoerden() {
        return fuerBehoerden;
    }

    public void setFuerBehoerden(boolean forBehoerde) {
        this.fuerBehoerden = forBehoerde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Parameter parameter = (Parameter) o;
        return schluessel.equals(parameter.schluessel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schluessel);
    }
}
