package de.wps.bube.basis.euregistry.domain;

public class EuRegistryException extends RuntimeException {
    public EuRegistryException(String message) {
        super(message);
    }

    public EuRegistryException(String message, Exception e) {
        super(message, e);
    }
}
