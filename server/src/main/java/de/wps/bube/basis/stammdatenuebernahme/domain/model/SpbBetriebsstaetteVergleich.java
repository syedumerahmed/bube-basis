package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import java.util.List;

public class SpbBetriebsstaetteVergleich {
    private Long id;
    private Long spbId;
    private boolean hasChanges;

    private StringVector name;

    private GeoPunktVector geoPunkt;
    private ReferenzVector betriebsstatus;
    private LocalDateVector betriebsstatusSeit;
    private LocalDateVector inbetriebnahme;

    private AdresseVergleich adresse;
    private List<AnsprechpartnerVector> ansprechpartner;
    private List<KommunikationsverbindungVector> kommunikationsverbindung;
    private List<StringVector> einleiterNummern;
    private List<StringVector> abfallerzeugerNummern;
    private ReferenzVector flusseinzuggebiet;
    private ReferenzVector naceCode;
    private BooleanVector direkteinleiter;
    private BooleanVector indirekteinleiter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public StringVector getName() {
        return name;
    }

    public void setName(StringVector name) {
        this.name = name;
    }

    public GeoPunktVector getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunktVector geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public ReferenzVector getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(ReferenzVector betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDateVector getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDateVector betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDateVector getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDateVector inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public AdresseVergleich getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseVergleich adresse) {
        this.adresse = adresse;
    }

    public List<AnsprechpartnerVector> getAnsprechpartner() {
        return ansprechpartner;
    }

    public void setAnsprechpartner(
            List<AnsprechpartnerVector> ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
    }

    public List<KommunikationsverbindungVector> getKommunikationsverbindung() {
        return kommunikationsverbindung;
    }

    public void setKommunikationsverbindung(
            List<KommunikationsverbindungVector> kommunikationsverbindung) {
        this.kommunikationsverbindung = kommunikationsverbindung;
    }

    public List<StringVector> getEinleiterNummern() {
        return einleiterNummern;
    }

    public void setEinleiterNummern(List<StringVector> einleiterNummern) {
        this.einleiterNummern = einleiterNummern;
    }

    public List<StringVector> getAbfallerzeugerNummern() {
        return abfallerzeugerNummern;
    }

    public void setAbfallerzeugerNummern(
            List<StringVector> abfallerzeugerNummern) {
        this.abfallerzeugerNummern = abfallerzeugerNummern;
    }

    public ReferenzVector getFlusseinzuggebiet() {
        return flusseinzuggebiet;
    }

    public void setFlusseinzuggebiet(ReferenzVector flusseinzuggebiet) {
        this.flusseinzuggebiet = flusseinzuggebiet;
    }

    public ReferenzVector getNaceCode() {
        return naceCode;
    }

    public void setNaceCode(ReferenzVector naceCode) {
        this.naceCode = naceCode;
    }

    public BooleanVector getDirekteinleiter() {
        return direkteinleiter;
    }

    public void setDirekteinleiter(BooleanVector direkteinleiter) {
        this.direkteinleiter = direkteinleiter;
    }

    public BooleanVector getIndirekteinleiter() {
        return indirekteinleiter;
    }

    public void setIndirekteinleiter(BooleanVector indirekteinleiter) {
        this.indirekteinleiter = indirekteinleiter;
    }

    public boolean isHasChanges() {
        return hasChanges;
    }

    public void setHasChanges(boolean hasChanges) {
        this.hasChanges = hasChanges;
    }
}
