package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBETZ;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RCONF;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

/**
 * Diese Klasse kapselt die gemeinsamen Attribute ihrer Unterklassen {@link BetriebsstaetteBean},
 * {@link AnlageBean} und {@link AnlagenteilBean}.
 * Dadurch wird beispielsweise der generische Zugriff auf den GeoPunkt des StammdatenObjekts einer
 * Bean vom Typ {@code EURegBetriebsstaetteBean} oder {@code EURegAnlageBean} ermöglicht.
 */
@RegelApi.Type(name = "StammdatenObjekt")
public abstract class StammdatenObjektBean<T extends StammdatenObjektBean<T>> implements PruefObjekt {

    private final Long id;
    private final String localId;
    private final String refAnlagenkataster;
    private final GeoPunktBean geoPunkt;
    private final List<ZustaendigeBehoerdeBean> zustaendigeBehoerden;
    private final String name;
    private final ReferenzBean vertraulichkeitsgrund;
    private final ReferenzBean betriebsstatus;
    private final LocalDate betriebsstatusSeit;
    private final LocalDate inbetriebnahme;
    private final String bemerkung;
    private final boolean betreiberdatenUebernommen;
    private final Instant ersteErfassung;
    private final Instant letzteAenderung;
    private final Supplier<T> vorgaenger;
    private final Supplier<T> nachfolger;

    protected StammdatenObjektBean(Long id, String localId, String refAnlagenkataster, GeoPunktBean geoPunkt,
            List<ZustaendigeBehoerdeBean> zustaendigeBehoerden, String name, ReferenzBean vertraulichkeitsgrund,
            ReferenzBean betriebsstatus, LocalDate betriebsstatusSeit, LocalDate inbetriebnahme, String bemerkung,
            boolean betreiberdatenUebernommen, Instant ersteErfassung, Instant letzteAenderung,
            Supplier<T> vorgaenger, Supplier<T> nachfolger) {
        this.id = id;
        this.localId = localId;
        this.refAnlagenkataster = refAnlagenkataster;
        this.geoPunkt = geoPunkt;
        this.zustaendigeBehoerden = zustaendigeBehoerden;
        this.name = name;
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        this.betriebsstatus = betriebsstatus;
        this.betriebsstatusSeit = betriebsstatusSeit;
        this.inbetriebnahme = inbetriebnahme;
        this.bemerkung = bemerkung;
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        this.ersteErfassung = ersteErfassung;
        this.letzteAenderung = letzteAenderung;
        this.vorgaenger = Suppliers.memoize(vorgaenger::get);
        this.nachfolger = Suppliers.memoize(nachfolger::get);
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @RegelApi.Member(description = "Local ID für InspireId eines fallweise generierten Objektes ProductionInstallation oder ProductionInstallationPart")
    public String getLocalId() {
        return localId;
    }

    @RegelApi.Member(description = "Anlagenkataster")
    public String getReferenzAnlagenkataster() {
        return refAnlagenkataster;
    }

    @RegelApi.Member(description = "Geo-Koordinaten")
    public GeoPunktBean getGeoPunkt() {
        return geoPunkt;
    }

    @RegelApi.Member(description = "Zuständige Behörden")
    public List<ZustaendigeBehoerdeBean> getZustaendigeBehoerden() {
        return zustaendigeBehoerden;
    }

    @RegelApi.Member(description = "Name")
    public String getName() {
        return name;
    }

    @RegelApi.Member(description = "Vertraulichkeitsgrund", referenzliste = RCONF)
    public ReferenzBean getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    @RegelApi.Member(description = "Betriebsstatus", referenzliste = RBETZ)
    public ReferenzBean getBetriebsstatus() {
        return betriebsstatus;
    }

    @RegelApi.Member(description = "Datum der letzten Änderung des Betriebsstatus")
    public LocalDate getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    @RegelApi.Member(description = "Inbetriebnahme")
    public LocalDate getInbetriebnahme() {
        return inbetriebnahme;
    }

    @RegelApi.Member(description = "Bemerkung")
    public String getBemerkung() {
        return bemerkung;
    }

    @RegelApi.Member(description = "Betreiberdaten übernommen")
    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @RegelApi.Member(description = "Die Betriebsstätte des Stammdaten-Objekts")
    public abstract BetriebsstaetteBean getBetriebsstaette();


    @RegelApi.Member(description = "StammdatenObjekt des jeweiligen Vorjahres.")
    public T getVorgaenger() {
        return vorgaenger.get();
    }

    @RegelApi.Member(description = "StammdatenObjekt des jeweiligen Folgejahres.")
    public T getNachfolger() {
        return nachfolger.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StammdatenObjektBean<?> that)) {
            return false;
        }
        return betreiberdatenUebernommen == that.betreiberdatenUebernommen && Objects.equals(id,
                that.id) && Objects.equals(localId, that.localId) && Objects.equals(refAnlagenkataster,
                that.refAnlagenkataster) && Objects.equals(geoPunkt, that.geoPunkt) && Objects.equals(
                zustaendigeBehoerden, that.zustaendigeBehoerden) && Objects.equals(name,
                that.name) && Objects.equals(vertraulichkeitsgrund,
                that.vertraulichkeitsgrund) && Objects.equals(betriebsstatus,
                that.betriebsstatus) && Objects.equals(betriebsstatusSeit,
                that.betriebsstatusSeit) && Objects.equals(inbetriebnahme,
                that.inbetriebnahme) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(
                ersteErfassung, that.ersteErfassung) && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, localId, refAnlagenkataster, geoPunkt, zustaendigeBehoerden, name,
                vertraulichkeitsgrund,
                betriebsstatus, betriebsstatusSeit, inbetriebnahme, bemerkung, betreiberdatenUebernommen,
                ersteErfassung,
                letzteAenderung);
    }

    @Override
    public String toString() {
        return "StammdatenObjektBean{" +
                "id=" + id +
                ", localId='" + localId + '\'' +
                ", refAnlagenkataster='" + refAnlagenkataster + '\'' +
                ", geoPunkt=" + geoPunkt +
                ", zustaendigeBehoerden=" + zustaendigeBehoerden +
                ", name='" + name + '\'' +
                ", vertraulichkeitsgrund=" + vertraulichkeitsgrund +
                ", betriebsstatus=" + betriebsstatus +
                ", betriebsstatusSeit=" + betriebsstatusSeit +
                ", inbetriebnahme=" + inbetriebnahme +
                ", bemerkung='" + bemerkung + '\'' +
                ", betreiberdatenUebernommen=" + betreiberdatenUebernommen +
                ", ersteErfassung=" + ersteErfassung +
                ", letzteAenderung=" + letzteAenderung +
                '}';
    }
}
