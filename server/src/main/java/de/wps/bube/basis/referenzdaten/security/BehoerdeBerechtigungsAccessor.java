package de.wps.bube.basis.referenzdaten.security;

import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;

import java.util.Set;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsAccessor;

@Component
@Primary
public class BehoerdeBerechtigungsAccessor implements BerechtigungsAccessor {
    @Override
    public Predicate land(Land land) {
        return land == Land.DEUTSCHLAND ? null : behoerde.land.eq(land);
    }

    @Override
    public Predicate behoerdenIn(Set<String> behoerden) {
        return null;
    }

    @Override
    public Predicate akzIn(Set<String> akz) {
        return null;
    }

    @Override
    public Predicate verwaltungsgebieteIn(Set<String> verwaltungsgebiete) {
        return null;
    }

    @Override
    public Predicate betriebsstaettenIn(Set<String> betriebsstaetten) {
        return null;
    }
}
