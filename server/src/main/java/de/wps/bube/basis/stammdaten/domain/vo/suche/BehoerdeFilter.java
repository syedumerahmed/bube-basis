package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;
import java.util.function.Function;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde;
import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public class BehoerdeFilter implements Filter {

    private final Function<QBetriebsstaette, QBehoerde> relativePath;
    private final Function<Suchattribute, List<Long>> attributExtractor;

    public BehoerdeFilter(Function<QBetriebsstaette, QBehoerde> relativePath,
            Function<Suchattribute, List<Long>> attributExtractor) {
        this.relativePath = relativePath;
        this.attributExtractor = attributExtractor;
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        final var behoerdeIds = attributExtractor.apply(suchattribute);
        return behoerdeIds != null ? relativePath.apply(bstRoot).id.in(behoerdeIds) : null;
    }

}
