package de.wps.bube.basis.base.web.dto;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;

import com.querydsl.core.types.OrderSpecifier;

import de.wps.bube.basis.base.persistence.Column;

public class ListStateDto<C extends Column> {
    public PageStateDto page;
    public SortStateDto<C> sort;
    public List<FilterStateDto<C>> filters;

    public Pageable toPageable() {
        if (page == null) {
            return Pageable.unpaged();
        }
        if (sort != null) {
            OrderSpecifier<?> direction = (sort.reverse ?
                    sort.by.getExpression().desc() :
                    sort.by.getExpression().asc()).nullsLast();
            return QPageRequest.of(page.current, page.size, direction);
        }
        return PageRequest.of(page.current, page.size);
    }

    public static class PageStateDto {
        public int size;
        public int current;
    }

    public static class SortStateDto<C extends Column> {
        public C by;
        public boolean reverse;
    }

    public static class FilterStateDto<C extends Column> {
        public C property;
        public String value;
    }

}
