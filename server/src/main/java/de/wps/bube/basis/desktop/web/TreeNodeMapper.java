package de.wps.bube.basis.desktop.web;

import java.util.Optional;

import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;

public interface TreeNodeMapper {
    DesktopTyp getTyp();
    Optional<TreeNodeDto> map(DesktopItem item);
}
