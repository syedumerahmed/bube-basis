package de.wps.bube.basis.euregistry.xml.bericht.dto.gml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;

@XmlAccessorType(XmlAccessType.FIELD)
public class Point {
    @XmlElement(name = "pos", namespace = SchemaSet.SCHEMA_GML)
    String pos;
    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    private String gmlId;
    @XmlAttribute
    private int srsDimension = 2;

    public String getGmlId() {
        return gmlId;
    }

    public void setGmlId(String gmlId) {
        this.gmlId = gmlId;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public int getSrsDimension() {
        return srsDimension;
    }

    public void setSrsDimension(int srsDimension) {
        this.srsDimension = srsDimension;
    }
}
