package de.wps.bube.basis.sicherheit.domain.vo;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.security.core.GrantedAuthority;

/**
 * Alle Rollen in BUBE Online. Die Rollen und ihre Beziehungen zueinander werden
 * im Keycloak hinterlegt; Anpassungen hier müssen daher auch in Keycloak
 * erfolgen.
 */
public enum Rolle implements GrantedAuthority {
    BEHOERDE_READ(Rollen.BEHOERDE_READ),
    BEHOERDE_WRITE(Rollen.BEHOERDE_WRITE),
    BEHOERDE_DELETE(Rollen.BEHOERDE_DELETE),

    LAND_READ(Rollen.LAND_READ),
    LAND_WRITE(Rollen.LAND_WRITE),
    LAND_DELETE(Rollen.LAND_DELETE),

    ADMIN(Rollen.ADMIN),
    GESAMTADMIN(Rollen.GESAMTADMIN),

    REFERENZLISTEN_USE(Rollen.REFERENZLISTEN_USE),
    REFERENZLISTEN_BUND_READ(Rollen.REFERENZLISTEN_BUND_READ),
    REFERENZLISTEN_BUND_WRITE(Rollen.REFERENZLISTEN_BUND_WRITE),
    REFERENZLISTEN_BUND_DELETE(Rollen.REFERENZLISTEN_BUND_DELETE),

    REFERENZLISTEN_LAND_READ(Rollen.REFERENZLISTEN_LAND_READ),
    REFERENZLISTEN_LAND_WRITE(Rollen.REFERENZLISTEN_LAND_WRITE),
    REFERENZLISTEN_LAND_DELETE(Rollen.REFERENZLISTEN_LAND_DELETE),

    BENUTZERVERWALTUNG_READ(Rollen.BENUTZERVERWALTUNG_READ),
    BENUTZERVERWALTUNG_WRITE(Rollen.BENUTZERVERWALTUNG_WRITE),
    BENUTZERVERWALTUNG_DELETE(Rollen.BENUTZERVERWALTUNG_DELETE),

    BETREIBER(Rollen.BETREIBER),
    BEHOERDENBENUTZER(Rollen.BEHOERDENBENUTZER);

    private final String authority;

    Rolle(final String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public String toKeycloakRepresentation() {
        return authority.substring(Rollen.ROLE_PREFIX.length());
    }

    public static Rolle of(final String value) {
        Rolle rolle = EnumUtils.getEnumIgnoreCase(Rolle.class, value);
        if (rolle == null) {
            throw new IllegalArgumentException("Ungültige Rolle: " + value);
        }
        return rolle;
    }

    public static boolean isValid(final String value) {
        return EnumUtils.isValidEnumIgnoreCase(Rolle.class, value);
    }
}
