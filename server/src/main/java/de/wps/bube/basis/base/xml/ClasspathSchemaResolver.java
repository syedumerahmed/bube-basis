package de.wps.bube.basis.base.xml;

import static org.apache.commons.codec.CharEncoding.UTF_8;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;

import org.apache.xerces.dom.DOMInputImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.security.web.util.UrlUtils;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

public class ClasspathSchemaResolver implements LSResourceResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClasspathSchemaResolver.class);

    private final String basePath;

    public ClasspathSchemaResolver(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
        LOGGER.debug("Locating Resource namespace='{}' systemId='{}' publicId='{}'", namespaceURI, systemId, publicId);
        var fileName = systemId;
        if (UrlUtils.isAbsoluteUrl(systemId)) {
            fileName = Paths.get(URI.create(systemId).getPath()).getFileName().toString();
        }
        LOGGER.debug("Extracted filename '{}'", fileName);
        var resource = new DefaultResourceLoader().getResource(basePath + fileName);
        LOGGER.debug("Resource {} exists? {}", resource, resource.exists());
        if (resource.exists()) {
            try {
                return new DOMInputImpl(publicId, systemId, baseURI, resource.getInputStream(), UTF_8);
            } catch (IOException e) {
                LOGGER.warn("Failed to read schema from classpath, falling back to lookup", e);
            }
        } else {
            LOGGER.warn("Missing {} for schema {} in namespace {}, using default resource lookup", resource, systemId,
                    namespaceURI);
        }
        return null;
    }
}
