package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

public class AnlagenteilBearbeitetEvent {
    public static final String EVENT_NAME = "Anlagenteil bearbeitet";

    public final Anlagenteil anlagenteil;

    public AnlagenteilBearbeitetEvent(Anlagenteil a) {
        this.anlagenteil = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AnlagenteilBearbeitetEvent that = (AnlagenteilBearbeitetEvent) o;
        return Objects.equals(anlagenteil, that.anlagenteil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anlagenteil);
    }
}
