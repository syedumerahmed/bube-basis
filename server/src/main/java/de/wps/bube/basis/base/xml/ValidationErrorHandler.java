package de.wps.bube.basis.base.xml;

import javax.xml.bind.JAXBException;

public interface ValidationErrorHandler<S> {
    void onError(S entity, JAXBException exception);
}
