package de.wps.bube.basis.referenzdaten.domain.entity;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.AssociationOverride;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Behoerde implements IReferenz {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "behoerde_seq")
    @SequenceGenerator(name = "behoerde_seq", allocationSize = 50)
    private Long id;
    @NotNull
    private Land land;
    @NotNull
    @Size(max = 10)
    private String schluessel;
    @Size(max = 3)
    private String kreis;
    @Size(max = 500)
    private String bezeichnung;
    @Size(max = 500)
    private String bezeichnung2;
    @Embedded
    @AssociationOverride(name = "vertraulichkeitsgrund", joinColumns = @JoinColumn(name = "adresse_vertraulichkeitsgrund_id"))
    private Adresse adresse;
    @ElementCollection
    private List<Kommunikationsverbindung> kommunikationsverbindungen;
    @Enumerated(EnumType.STRING)
    @ElementCollection
    private Set<Zustaendigkeit> zustaendigkeiten;

    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_von"))
    private Gueltigkeitsjahr gueltigVon;

    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_bis"))
    private Gueltigkeitsjahr gueltigBis;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        // Es darf keine Bundesbehörden geben
        if (Land.DEUTSCHLAND.equals(land)) {
            throw new ReferenzdatenException("Es dürfen keine Behörden für Deutschland angelegt werden.");
        }
        this.land = land;
    }

    public String getKreis() {
        return kreis;
    }

    public void setKreis(String kreis) {
        this.kreis = kreis;
    }

    public String getSchluessel() {
        return schluessel;
    }

    public void setSchluessel(String schluessel) {
        this.schluessel = schluessel;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBezeichnung2() {
        return bezeichnung2;
    }

    public void setBezeichnung2(String bezeichnung2) {
        this.bezeichnung2 = bezeichnung2;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<Kommunikationsverbindung> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    public void setKommunikationsverbindungen(List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    public Set<Zustaendigkeit> getZustaendigkeiten() {
        return zustaendigkeiten;
    }

    public void setZustaendigkeiten(Set<Zustaendigkeit> zustaendigkeiten) {
        this.zustaendigkeiten = zustaendigkeiten;
    }

    public Gueltigkeitsjahr getGueltigVon() {
        return gueltigVon;
    }

    public void setGueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
    }

    public Gueltigkeitsjahr getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Behoerde behoerde = (Behoerde) o;
        return Objects.equals(id, behoerde.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean isOfLand(Land land) {
        return this.land == land;
    }

    @Override
    public void pruefeGueltigkeit() {
        if (gueltigBis.liegtVor(gueltigVon)) {
            throw new ReferenzdatenException("Gültig bis liegt zeitlich vor Gültig von");
        }
    }

    @Override
    public String toString() {
        return "Behörde " + "ID=" + id + ", Land=" + land.getNr() + ", Schlüssel='" + schluessel + '\'';
    }

    public boolean gueltigFuer(Gueltigkeitsjahr jahr) {
        return gueltigVon.getJahr() <= jahr.getJahr() && jahr.getJahr() <= gueltigBis.getJahr();
    }

    public String getEmail() {
        if (kommunikationsverbindungen == null) {
            return null;
        }
        return kommunikationsverbindungen.stream().filter(Kommunikationsverbindung::isEmail).findFirst().map(
                Kommunikationsverbindung::getVerbindung).orElse(null);
    }

    public String getFax() {
        if (kommunikationsverbindungen == null) {
            return null;
        }
        return kommunikationsverbindungen.stream().filter(Kommunikationsverbindung::isFax).findFirst().map(
                Kommunikationsverbindung::getVerbindung).orElse(null);
    }

    public String getTelefon() {
        if (kommunikationsverbindungen == null) {
            return null;
        }
        return kommunikationsverbindungen.stream().filter(Kommunikationsverbindung::isTelefon).findFirst().map(
                Kommunikationsverbindung::getVerbindung).orElse(null);
    }
}
