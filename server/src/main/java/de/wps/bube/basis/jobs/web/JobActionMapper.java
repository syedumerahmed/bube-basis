package de.wps.bube.basis.jobs.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.JobActionDto;
import de.wps.bube.basis.jobs.domain.JobAction;

@Mapper
public interface JobActionMapper {

    JobActionDto toDto(JobAction jobAction);

}
