package de.wps.bube.basis.euregistry.xml.bericht.dto.pf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Status {
    @XmlElement(required = true, name = "StatusType")
    private final StatusType status = new StatusType();

    public ReferenceType getStatusType() {
        return status.statusType;
    }

    public void setStatusType(ReferenceType statusType) {
        status.statusType = statusType;
    }

    public String getDescription() {
        return status.description;
    }

    public void setDescription(String description) {
        status.description = description;
    }

    public XMLGregorianCalendar getValidFrom() {
        return status.validFrom;
    }

    public void setValidFrom(XMLGregorianCalendar validFrom) {
        status.validFrom = validFrom;
    }

    public XMLGregorianCalendar getValidTo() {
        return status.validTo;
    }

    public void setValidTo(XMLGregorianCalendar validTo) {
        status.validTo = validTo;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class StatusType {
        @XmlElement(required = true, name = "statusType")
        ReferenceType statusType;

        @XmlElement(required = false, name = "description")
        String description;

        @XmlElement(required = true, name = "validFrom", nillable = true)
        XMLGregorianCalendar validFrom;

        @XmlElement(required = false, name = "validTo")
        XMLGregorianCalendar validTo;
    }
}
