package de.wps.bube.basis.benutzerverwaltung.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;

public class BenutzerBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {
    private final Benutzer benutzer;

    public BenutzerBerechtigungsAdapter(Benutzer benutzer) {
        this.benutzer = benutzer;
    }

    @Override
    public Land getLand() {
        return benutzer.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return benutzer.getBehoerden();
    }

    @Override
    public Set<String> getAkzSet() {
        return benutzer.getAkzSet();
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return benutzer.getVerwaltungsgebiete();
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return benutzer.getBetriebsstaetten();
    }
}
