package de.wps.bube.basis.euregistry.web.mapper;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.UebernimmEuRegDatenInJahrCommandDto;
import de.wps.bube.basis.base.web.openapi.model.XeurTeilberichtErstellenCommandDto;
import de.wps.bube.basis.euregistry.domain.UebernimmEuRegDatenInJahrCommand;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtErstellenCommand;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;

@Mapper(uses = ReferenzMapper.class) public interface EuRegCommandMapper {
    UebernimmEuRegDatenInJahrCommand fromDto(UebernimmEuRegDatenInJahrCommandDto uebernimmCommand);

    XeurTeilberichtErstellenCommand fromDto(XeurTeilberichtErstellenCommandDto xeurCommand);
}
