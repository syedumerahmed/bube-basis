package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;
import java.util.function.Function;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ListPath;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.QVorschrift;

public class AnlagenMitAllenVorschriftenFilter implements Filter {
    private final Function<QBetriebsstaette, ListPath<?, QVorschrift>> anlageVorschriften;
    private final Function<QBetriebsstaette, ListPath<?, QVorschrift>> anlagenteilVorschriften;
    private final Function<Suchattribute, List<Long>> attributExtractor;

    public AnlagenMitAllenVorschriftenFilter(
            Function<QBetriebsstaette, ListPath<?, QVorschrift>> anlageVorschriften,
            Function<QBetriebsstaette, ListPath<?, QVorschrift>> anlagenteilVorschriften,
            Function<Suchattribute, List<Long>> attributExtractor) {

        this.anlageVorschriften = anlageVorschriften;
        this.anlagenteilVorschriften = anlagenteilVorschriften;
        this.attributExtractor = attributExtractor;
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        var vorschriften = attributExtractor.apply(suchattribute);
        if (vorschriften == null) {
            return null;
        }

        var builder = new BooleanBuilder();
        vorschriften.stream()
                    .map(vors -> anlageVorschriften.apply(bstRoot).any().art.id.eq(vors))
                    .forEach(builder::and);

        if (suchattribute.anlage.searchAnlagenteile) {
            var anlagenteilBuilder = new BooleanBuilder();
            vorschriften.stream()
                        .map(vors -> anlagenteilVorschriften.apply(bstRoot).any().art.id.eq(vors))
                        .forEach(anlagenteilBuilder::and);
            builder.or(anlagenteilBuilder.getValue());
        }

        return builder.getValue();
    }
}
