package de.wps.bube.basis.referenzdaten.domain.service;

import static java.lang.String.format;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.xml.ReferenzXMLMapper;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;

@Service
@Transactional
public class ReferenzdatenImportierenJob implements ReferenzdatenJob {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.REFERENZDATEN_IMPORTIEREN;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReferenzdatenImportierenJob.class);

    private static final int COUNT_SIZE = 10;

    private final ReferenzdatenImportExportService referenzdatenImportExportService;
    private final BenutzerJobRegistry benutzerJobRegistry;
    private final XMLService xmlService;
    private final DateiService dateiService;
    private final ReferenzXMLMapper referenzXMLMapper;

    public ReferenzdatenImportierenJob(ReferenzdatenImportExportService referenzdatenImportExportService,
            BenutzerJobRegistry benutzerJobRegistry, XMLService xmlService,
            DateiService dateiService, ReferenzXMLMapper referenzXMLMapper) {
        this.referenzdatenImportExportService = referenzdatenImportExportService;
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.xmlService = xmlService;
        this.dateiService = dateiService;
        this.referenzXMLMapper = referenzXMLMapper;
    }

    @Async
    public void runAsync(String username) {
        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(JOB_NAME + " Start: " + DateTime.nowCET() + " Benutzer: " + username);

        try {
            AtomicInteger count = new AtomicInteger();
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.REFERENZDATEN);
            xmlService.read(inputStream, ReferenzType.class, referenzXMLMapper, ReferenzlisteXDto.XML_ROOT_ELEMENT_NAME,
                    null, referenz -> {
                        referenzdatenImportExportService.importiereReferenz(referenz.entity, jobProtokoll);
                        if (count.incrementAndGet() % COUNT_SIZE == 0) {
                            benutzerJobRegistry.jobCountChanged(COUNT_SIZE);
                        }
                    });

            benutzerJobRegistry.jobCountChanged(count.get() % COUNT_SIZE);
            benutzerJobRegistry.jobCompleted(false);
            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Referenzen: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());

        } finally {
            // Datei wird gelöscht, egal ob der Job durchlief oder nicht
            referenzdatenImportExportService.dateiLoeschen(username);

            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }
}
