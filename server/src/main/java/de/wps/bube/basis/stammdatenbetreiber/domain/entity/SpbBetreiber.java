package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;

/**
 * Betreiberdaten der Betreiber
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class SpbBetreiber {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "spb_betreiber_seq")
    @SequenceGenerator(name = "spb_betreiber_seq", allocationSize = 50)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    private Betreiber betreiber;

    @Embedded
    private SpbAdresse adresse;

    private String name;

    @NotNull
    private Instant ersteErfassung;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected SpbBetreiber() {
    }

    public SpbBetreiber(Betreiber betreiber) {
        this.betreiber = betreiber;

        this.name = this.betreiber.getName();
        this.ersteErfassung = this.betreiber.getErsteErfassung();
        this.letzteAenderung = this.betreiber.getLetzteAenderung();

        Adresse adresse = this.betreiber.getAdresse();
        this.adresse = new SpbAdresse(adresse.getStrasse(), adresse.getHausNr(), adresse.getOrt(), adresse.getPlz());
        this.adresse.setLandIsoCode(adresse.getLandIsoCode());
        this.adresse.setOrtsteil(adresse.getOrtsteil());
        this.adresse.setPostfach(adresse.getPostfach());
        this.adresse.setPostfachPlz(adresse.getPostfachPlz());

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SpbAdresse getAdresse() {
        return adresse;
    }

    public void setAdresse(SpbAdresse adresse) {
        this.adresse = adresse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpbBetreiber that = (SpbBetreiber) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getBetreiberId() {
        return this.betreiber != null ? this.betreiber.getId() : null;
    }

}
