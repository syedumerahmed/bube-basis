package de.wps.bube.basis.euregistry.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.euregistry.domain.event.EuRegDatenUebernehmenTriggeredEvent;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Component
public class EuRegDatenUebernehmenListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(EuRegDatenUebernehmenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final EuRegDatenUebernehmenJob job;

    public EuRegDatenUebernehmenListener(BenutzerJobRegistry jobRegistry, EuRegDatenUebernehmenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleEuRegDatenUebernehmen(EuRegDatenUebernehmenTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", EuRegDatenUebernehmenTriggeredEvent.EVENT_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(EuRegDatenUebernehmenJob.JOB_NAME));

        job.runAsync(event.betriebsstaettenIds(), event.zieljahr());
    }
}
