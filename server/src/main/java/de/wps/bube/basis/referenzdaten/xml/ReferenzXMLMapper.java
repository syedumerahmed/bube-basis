package de.wps.bube.basis.referenzdaten.xml;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = CommonXMLMapper.class)
public abstract class ReferenzXMLMapper implements XDtoMapper<ReferenzType, Referenz> {

    @Override
    public abstract ReferenzType toXDto(Referenz referenz);

    @Override
    public EntityWithError<Referenz> fromXDto(ReferenzType xDto) {
        return new EntityWithError<>(map(xDto));
    }

    @Mapping(target = "id", ignore = true)
    public abstract Referenz map(ReferenzType referenzXDto);

    public static String schluessel(Referenz referenz) {
        return referenz == null ? null : referenz.getSchluessel();
    }

    public static Gueltigkeitsjahr map(int jahr) {
        return Gueltigkeitsjahr.of(jahr);
    }

    public static int map(Gueltigkeitsjahr gueltigkeitsjahr) {
        return gueltigkeitsjahr.getJahr();
    }

    public static Land map(String land) {
        return Land.of(land);
    }

    public static String map(Land land) {
        return land != null ? land.getNr() : null;
    }
}
