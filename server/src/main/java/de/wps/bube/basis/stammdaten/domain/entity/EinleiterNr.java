package de.wps.bube.basis.stammdaten.domain.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

@Embeddable
public class EinleiterNr {

    @NotEmpty
    private String einleiterNr;

    protected EinleiterNr() {
    }

    public EinleiterNr(final String nr) {
        this.einleiterNr = nr;
    }

    public String getEinleiterNr() {
        return einleiterNr;
    }
}
