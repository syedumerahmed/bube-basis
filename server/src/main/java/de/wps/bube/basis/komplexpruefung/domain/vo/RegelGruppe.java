package de.wps.bube.basis.komplexpruefung.domain.vo;

import java.util.stream.Stream;

public enum RegelGruppe {
    GRUPPE_A("A"), GRUPPE_B("B");

    private final String bezeichner;

    RegelGruppe(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static RegelGruppe of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültige Regelgruppe: " + bezeichner));
    }
}
