package de.wps.bube.basis.jobs;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

import org.slf4j.MDC;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Configuration
@EnableAsync
public class SpringAsyncConfig implements AsyncConfigurer {

    @Value("${thread.pool.core-pool-size}")
    private int corePoolSize;

    @Value("${thread.pool.max-pool-size}")
    private int maxPoolSize;

    @Value("${thread.pool.queue-capacity}")
    private int queueCapacity;

    @Value("${thread.pool.await-termination-seconds}")
    private int awaitTerminationSeconds;

    @Override
    public Executor getAsyncExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix("BubeExecutor-");
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(awaitTerminationSeconds);
        executor.setTaskDecorator(new ContextTaskDecorator());
        executor.initialize();
        return executor;
    }

    @Override
    @Bean
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }

    /*
     * Dieser TaskDecorator liest beim Erstellen eines asynchronen Tasks den aktuellen RequestContext aus und
     * stellt ihn dem neuen Thread zur Verfügung, damit in @Async Methoden weiterhin Request-gebundene Beans
     * (z.B. Benutzer und dessen Datenberechtigungen) genutzt werden können.
     */
    private static class ContextTaskDecorator implements TaskDecorator {
        @Nonnull
        @Override
        public Runnable decorate(@Nonnull Runnable runnable) {
            RequestAttributes context = cloneRequestAttributes(RequestContextHolder.currentRequestAttributes());
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Map<String, String> contextMap = MDC.getCopyOfContextMap();
            return () -> {
                try {
                    SecurityContextHolder.setContext(securityContext);
                    RequestContextHolder.setRequestAttributes(context);
                    if (contextMap != null) {
                        MDC.setContextMap(contextMap);
                    }
                    runnable.run();
                } finally {
                    SecurityContextHolder.clearContext();
                    RequestContextHolder.resetRequestAttributes();
                    MDC.clear();
                }
            };
        }

        private RequestAttributes cloneRequestAttributes(RequestAttributes requestAttributes) {
            try {
                RequestAttributes clonedRequestAttribute = new ServletRequestAttributes(
                        ((ServletRequestAttributes) requestAttributes).getRequest(),
                        ((ServletRequestAttributes) requestAttributes).getResponse());
                Stream.of(RequestAttributes.SCOPE_REQUEST, RequestAttributes.SCOPE_SESSION)
                      .forEach(scope -> Arrays.stream(requestAttributes.getAttributeNames(scope))
                                              .forEach(name -> clonedRequestAttribute.setAttribute(name,
                                                      requestAttributes.getAttribute(name, scope), scope)));
                return clonedRequestAttribute;
            } catch (Exception e) {
                return requestAttributes;
            }
        }
    }
}

