package de.wps.bube.basis.desktop.domain.vo;

import java.util.stream.Stream;

public enum DesktopTyp {

    STAMMDATEN("Stammdaten"),
    EUREG("EUReg"),
    SPB_STAMMDATEN("Betreiber-Stammdaten");

    private final String bezeichner;

    DesktopTyp(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static DesktopTyp of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Desktop-Typ: " + bezeichner));
    }

    public static DesktopTyp byName(final String name) {
        return Stream.of(values())
                     .filter(p -> p.name().equals(name))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Desktop-Typ: " + name));

    }
}
