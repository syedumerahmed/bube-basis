package de.wps.bube.basis.stammdaten.domain.vo;

import static de.wps.bube.basis.stammdaten.domain.entity.QQuelle.quelle;

import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum QuelleColumn implements Column {

    QUELLEN_NUMMER(emptyIfNull(quelle.quelleNr)),
    NAME(emptyIfNull(quelle.name)),
    AENDERUNG_BETREIBER(emptyIfNull(quelle.letzteAenderungBetreiber.stringValue()));

    private static StringExpression emptyIfNull(final StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final StringExpression expression;

    QuelleColumn(StringExpression expression) {
        this.expression = expression;
    }

    @Override
    public StringExpression getExpression() {
        return expression;
    }
}
