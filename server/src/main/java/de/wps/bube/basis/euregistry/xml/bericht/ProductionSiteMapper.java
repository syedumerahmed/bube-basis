package de.wps.bube.basis.euregistry.xml.bericht;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base.Identifier;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;

@Mapper(uses = { CommonEURegMapper.class })
public abstract class ProductionSiteMapper {

    @Mapping(target = "inspireID.identifier", source = ".")
    @Mapping(target = "siteName.nameOfFeature", source = "betriebsstaette.name")
    @Mapping(target = "siteName.confidentialityReason", source = "betriebsstaette.vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    @Mapping(target = "reportData.schema", constant = "#")
    @Mapping(target = "reportData.code", expression = "java(context.getReportData().getGmlId())")
    @Mapping(target = "location", source = "betriebsstaette.geoPunkt")
    @Mapping(target = "gmlId", expression = "java(context.createProductionSiteId())")
    public abstract ProductionSite toXDto(EURegBetriebsstaette entity, @Context ProductionSiteMapperContext context);

    Identifier mapInspireID(EURegBetriebsstaette entity, @Context ProductionSiteMapperContext context) {

        var localId = entity.getBetriebsstaette().getLocalId() + context.getInspireParam().getSite();

        var identifier = new Identifier();
        identifier.setNamespace(context.getInspireParam().getNamespace());
        identifier.setLocalId(localId);
        return identifier;
    }

    interface ProductionSiteMapperContext {
        ReportData getReportData();

        String createProductionSiteId();

        InspireParam getInspireParam();

        @AfterMapping
        void afterProductionSite(@MappingTarget ProductionSite productionSite);
    }
}
