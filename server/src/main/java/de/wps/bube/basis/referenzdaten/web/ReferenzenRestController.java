package de.wps.bube.basis.referenzdaten.web;

import static java.util.stream.Collectors.toList;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.ValidierungshinweisMapper;
import de.wps.bube.basis.base.web.openapi.api.ReferenzenApi;
import de.wps.bube.basis.base.web.openapi.model.BehoerdeDto;
import de.wps.bube.basis.base.web.openapi.model.BehoerdeListItemDto;
import de.wps.bube.basis.base.web.openapi.model.DateiDto;
import de.wps.bube.basis.base.web.openapi.model.ExportiereBehoerdenCommand;
import de.wps.bube.basis.base.web.openapi.model.ExportiereReferenzdatenCommand;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.base.web.openapi.model.ReferenzlisteMetadataDto;
import de.wps.bube.basis.base.web.openapi.model.Validierungshinweis;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenImportExportService;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenLoeschenService;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzdatenImportExportService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Controller
class ReferenzenRestController implements ReferenzenApi {

    private final ReferenzenService referenzenService;
    private final BehoerdenService behoerdenService;
    private final BehoerdenLoeschenService behoerdenLoeschenService;
    private final ReferenzdatenImportExportService referenzdatenImportExportService;
    private final ReferenzMapper mapper;
    private final BehoerdenMapper behoerdenMapper;
    private final ReferenzlisteMetadataMapper referenzlisteMetadataMapper;
    private final AktiverBenutzer aktuellerAktiverBenutzer;
    private final BehoerdenImportExportService behoerdenImportExportService;
    private final ValidierungshinweisMapper validierungshinweisMapper;

    public ReferenzenRestController(ReferenzenService referenzenService, BehoerdenService behoerdenService,
            BehoerdenLoeschenService behoerdenLoeschenService,
            ReferenzdatenImportExportService referenzdatenImportExportService, ReferenzMapper mapper,
            BehoerdenMapper behoerdenMapper, ReferenzlisteMetadataMapper referenzlisteMetadataMapper,
            AktiverBenutzer aktuellerAktiverBenutzer, BehoerdenImportExportService behoerdenImportExportService,
            ValidierungshinweisMapper validierungshinweisMapper) {
        this.referenzenService = referenzenService;
        this.behoerdenService = behoerdenService;
        this.behoerdenLoeschenService = behoerdenLoeschenService;
        this.referenzdatenImportExportService = referenzdatenImportExportService;
        this.mapper = mapper;
        this.behoerdenMapper = behoerdenMapper;
        this.referenzlisteMetadataMapper = referenzlisteMetadataMapper;
        this.aktuellerAktiverBenutzer = aktuellerAktiverBenutzer;
        this.behoerdenImportExportService = behoerdenImportExportService;
        this.validierungshinweisMapper = validierungshinweisMapper;
    }

    @Override
    public ResponseEntity<List<ReferenzDto>> readReferenzlisteForEdit(String name) {
        return ResponseEntity.ok(
                referenzenService.readReferenzlisteForEdit(Referenzliste.valueOf(name), mapper::toDto));
    }

    @Override
    public ResponseEntity<List<ReferenzDto>> readReferenzliste(String name, Integer jahr,
            String land) {
        return ResponseEntity.ok(
                referenzenService.readReferenzliste(Referenzliste.valueOf(name), Gueltigkeitsjahr.of(jahr),
                        Land.of(land), mapper::toDto));
    }

    @Override
    public ResponseEntity<ReferenzDto> readReferenz(String name, Integer jahr, String land, String schluessel) {
        return ResponseEntity.ok(mapper.toDto(
                referenzenService.readReferenz(Referenzliste.valueOf(name), schluessel, Gueltigkeitsjahr.of(jahr),
                        Land.of(land))));
    }

    @Override
    public ResponseEntity<ReferenzDto> readAktuellesBerichtsjahr() {
        return ResponseEntity.ok(mapper.toDto(referenzenService.getAktuellesBerichtsjahr()));
    }

    @Override
    public ResponseEntity<ReferenzDto> createReferenz(ReferenzDto referenzDto) {
        Referenz neueReferenz = referenzenService.createReferenz(mapper.fromDto(referenzDto));
        return ResponseEntity.ok(mapper.toDto(neueReferenz));
    }

    @Override
    public ResponseEntity<ReferenzDto> updateReferenz(ReferenzDto referenzDto) {
        Referenz referenz = referenzenService.updateReferenz(mapper.fromDto(referenzDto));
        return ResponseEntity.ok(mapper.toDto(referenz));
    }

    @Override
    public ResponseEntity<Void> deleteReferenz(Long id) {
        referenzenService.deleteReferenz(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleReferenzen(List<Long> ids) {
        referenzenService.deleteMultipleReferenzen(ids);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<BehoerdeListItemDto>> readAllBehoerden() {
        return ResponseEntity.ok(
                behoerdenService.readAllBehoerden().stream().map(behoerdenMapper::toDto).collect(toList()));
    }

    @Override
    public ResponseEntity<List<BehoerdeListItemDto>> readBehoerden(Integer jahr,
            Optional<String> landNr) {
        var land = landNr.map(Land::of).orElse(null);
        return ResponseEntity.ok(
                behoerdenService.readBehoerden(Gueltigkeitsjahr.of(jahr), land).stream()
                                .map(behoerdenMapper::toDto)
                                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<BehoerdeDto> readBehoerde(Long id) {
        return ResponseEntity.ok(behoerdenMapper.toDto(behoerdenService.readBehoerde(id)));
    }

    @Override
    public ResponseEntity<Void> deleteBehoerde(Long id) {
        behoerdenLoeschenService.deleteBehoerde(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleBehoerden(List<Long> ids) {
        behoerdenLoeschenService.deleteMultipleBehoerden(ids);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<BehoerdeDto> createBehoerde(BehoerdeDto dto) {
        return ResponseEntity.ok(behoerdenMapper.toDto(behoerdenService.createBehoerde(behoerdenMapper.fromDto(dto))));
    }

    @Override
    public ResponseEntity<BehoerdeDto> updateBehoerde(BehoerdeDto dto) {
        return ResponseEntity.ok(behoerdenMapper.toDto(behoerdenService.updateBehoerde(behoerdenMapper.fromDto(dto))));
    }

    @Override
    public ResponseEntity<Resource> exportBehoerden(ExportiereBehoerdenCommand command) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        this.behoerdenImportExportService.exportBehoerden(outputStream,
                command.getBehoerdenList());

        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType("application/octet-stream"))
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"behoerden.xml\"")
                             .body(byteArrayResource);
    }

    @Override
    public ResponseEntity<DateiDto> existierendeBehoerdenDatei() {
        DateiDto dateiDto = new DateiDto();
        dateiDto.setDateiName(behoerdenImportExportService.existierendeDatei(aktuellerAktiverBenutzer.getUsername()));
        return ResponseEntity.ok(dateiDto);
    }

    @Override
    public ResponseEntity<Void> uploadBehoerden(MultipartFile file) {
        behoerdenImportExportService.dateiSpeichern(file, aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Validierungshinweis>> validiereBehoerden() {
        return ResponseEntity.ok(behoerdenImportExportService.validiere(aktuellerAktiverBenutzer.getUsername())
                                                             .stream()
                                                             .map(validierungshinweisMapper::mapToDto)
                                                             .toList());
    }

    @Override
    public ResponseEntity<Void> importBehoerden() {
        behoerdenImportExportService.triggerImport(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> behoerdenDateiLoeschen() {
        behoerdenImportExportService.dateiLoeschen(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<ReferenzlisteMetadataDto>> readAllReferenzlistenMetadata() {
        return ResponseEntity.ok(referenzenService.readAllReferenzlistenMetadata()
                                                  .stream()
                                                  .map(referenzlisteMetadataMapper::toDto)
                                                  .collect(toList()));
    }

    @Override
    public ResponseEntity<ReferenzlisteMetadataDto> readReferenzlisteMetadata(String name) {
        return ResponseEntity.ok(referenzlisteMetadataMapper.toDto(
                referenzenService.readReferenzlisteMetadata(Referenzliste.valueOf(name))));
    }

    @Override
    public ResponseEntity<Resource> exportReferenzen(ExportiereReferenzdatenCommand command) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        this.referenzdatenImportExportService.exportReferenzdaten(
                outputStream, command.getReferenzlisteList());

        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
        return ResponseEntity.ok()
                             .contentType(MediaType.APPLICATION_OCTET_STREAM)
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"referenzdaten.xml\"")
                             .body(byteArrayResource);
    }

    @Override
    public ResponseEntity<DateiDto> existierendeReferenzenDatei() {
        DateiDto dateiDto = new DateiDto();
        dateiDto.setDateiName(
                referenzdatenImportExportService.existierendeDatei(aktuellerAktiverBenutzer.getUsername()));
        return ResponseEntity.ok(dateiDto);
    }

    @Override
    public ResponseEntity<Void> uploadReferenzen(MultipartFile file) {
        referenzdatenImportExportService.dateiSpeichern(file, aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Validierungshinweis>> validiereReferenzen() {
        return ResponseEntity.ok(referenzdatenImportExportService.validiere(
                aktuellerAktiverBenutzer.getUsername()).stream().map(validierungshinweisMapper::mapToDto).toList());
    }

    @Override
    public ResponseEntity<Void> importReferenzen() {
        referenzdatenImportExportService.triggerImport(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> referenzenDateiLoeschen() {
        referenzdatenImportExportService.dateiLoeschen(aktuellerAktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }

}
