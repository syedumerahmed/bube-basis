package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.function.Function;
import java.util.regex.Matcher;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public class StringFilter implements Filter {

    static final char ESCAPE_CHAR = '\\';
    private final Function<QBetriebsstaette, StringExpression> relativePath;
    private final Function<Suchattribute, String> attributExtractor;

    public StringFilter(Function<QBetriebsstaette, StringExpression> relativePath,
            Function<Suchattribute, String> attributExtractor) {
        this.relativePath = relativePath;
        this.attributExtractor = attributExtractor;
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        var stringValue = attributExtractor.apply(suchattribute);
        if (stringValue == null) {
            return null;
        }
        return relativePath.apply(bstRoot).likeIgnoreCase(createLikeString(stringValue), ESCAPE_CHAR);
    }

    // BUBE-50: Als Wildcard-Zeichen wird ‚*‘ vereinbart,
    // es kann als erstes oder/und letztes Zeichen bei einem eingetragenen Wert vorkommen.
    private static String createLikeString(String str) {
        // replace LIKE placeholders
        return str.replaceAll("([_%])", Matcher.quoteReplacement(String.valueOf(ESCAPE_CHAR)) + "$1")
                  // replace '*' at start or end with '%'
                  .replaceAll("^\\*|\\*$", "%");

    }
}
