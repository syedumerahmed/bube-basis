package de.wps.bube.basis.stammdatenuebernahme.domain.model;

public class AdresseVergleich {
    private StringVector strasse;
    private StringVector hausNr;
    private StringVector plz;
    private StringVector ort;
    private StringVector ortsteil;
    private StringVector postfachPlz;
    private StringVector postfach;
    private ReferenzVector landIsoCode;

    public StringVector getStrasse() {
        return strasse;
    }

    public void setStrasse(StringVector strasse) {
        this.strasse = strasse;
    }

    public StringVector getHausNr() {
        return hausNr;
    }

    public void setHausNr(StringVector hausNr) {
        this.hausNr = hausNr;
    }

    public StringVector getPlz() {
        return plz;
    }

    public void setPlz(StringVector plz) {
        this.plz = plz;
    }

    public StringVector getOrt() {
        return ort;
    }

    public void setOrt(StringVector ort) {
        this.ort = ort;
    }

    public StringVector getOrtsteil() {
        return ortsteil;
    }

    public void setOrtsteil(StringVector ortsteil) {
        this.ortsteil = ortsteil;
    }

    public StringVector getPostfachPlz() {
        return postfachPlz;
    }

    public void setPostfachPlz(StringVector postfachPlz) {
        this.postfachPlz = postfachPlz;
    }

    public StringVector getPostfach() {
        return postfach;
    }

    public void setPostfach(StringVector postfach) {
        this.postfach = postfach;
    }

    public ReferenzVector getLandIsoCode() {
        return landIsoCode;
    }

    public void setLandIsoCode(ReferenzVector landIsoCode) {
        this.landIsoCode = landIsoCode;
    }
}
