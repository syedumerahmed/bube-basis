package de.wps.bube.basis.base.persistence;

public class DuplicateKeyException extends RuntimeException {
    public DuplicateKeyException(final String message) {
        super(message);
    }
}
