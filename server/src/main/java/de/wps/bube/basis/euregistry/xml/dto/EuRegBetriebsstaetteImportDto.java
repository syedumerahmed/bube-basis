package de.wps.bube.basis.euregistry.xml.dto;

import java.time.LocalDate;
import java.util.List;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;

public class EuRegBetriebsstaetteImportDto {

    private List<EURegAnlage> anlagen;
    private EURegBetriebsstaette euRegBetriebsstaette;


    public EuRegBetriebsstaetteImportDto(
            EURegBetriebsstaette euRegBetriebsstaette,
            List<EURegAnlage> anlagen
    ) {
        this.anlagen = anlagen;
        this.euRegBetriebsstaette = euRegBetriebsstaette;
    }

    public List<EURegAnlage> getAnlagen() {
        return anlagen;
    }

    public void setAnlagen(List<EURegAnlage> anlagen) {
        this.anlagen = anlagen;
    }

    private LocalDate bearbeitungsstatusSeit;

    public EURegBetriebsstaette getEuRegBetriebsstaette() {
        return euRegBetriebsstaette;
    }

    public void setEuRegBetriebsstaette(EURegBetriebsstaette euRegBetriebsstaette) {
        this.euRegBetriebsstaette = euRegBetriebsstaette;
    }
}
