package de.wps.bube.basis.benutzerverwaltung.domain.entity;

import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.JobAction;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Entity
@Table(name = "job_status_benutzer_import")
@PrimaryKeyJoinColumn(name = "username")
public class BenutzerImportJobStatus extends JobStatus {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.BENUTZER_IMPORTIEREN;

    @ElementCollection
    @CollectionTable(name = "job_status_benutzer_import_name", joinColumns = @JoinColumn(name="username"))
    @Column(name = "benutzer_name")
    private List<String> benutzerNamen;

    public BenutzerImportJobStatus() {
        super(JOB_NAME);
    }

    public List<String> getBenutzerNamen() {
        return benutzerNamen;
    }

    public void setBenutzerNamen(List<String> benutzerNamen) {
        this.benutzerNamen = benutzerNamen;
    }

    @Override
    public JobAction getAction() {
        if (benutzerNamen == null || benutzerNamen.isEmpty()) {
            return null;
        }
        JobAction action = new JobAction("Einmalpasswörter generieren", "/benutzerverwaltung/betreiber");
        action.addPayloadData("benutzerNamen", benutzerNamen);
        return action;
    }

}
