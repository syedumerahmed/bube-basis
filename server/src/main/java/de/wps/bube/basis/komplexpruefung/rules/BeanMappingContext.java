package de.wps.bube.basis.komplexpruefung.rules;

import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

public class BeanMappingContext {

    private final Land land;
    private final Gueltigkeitsjahr jahr;

    public BeanMappingContext(Land land, Gueltigkeitsjahr jahr) {
        this.land = land;
        this.jahr = jahr;
    }

    public Land getLand() {
        return land;
    }

    public Gueltigkeitsjahr getJahr() {
        return jahr;
    }

    public BeanMappingContext vorgaenger() {
        Gueltigkeitsjahr vorgaengerJahr = Gueltigkeitsjahr.of(this.jahr.getJahr() - 1);
        return new BeanMappingContext(this.land, vorgaengerJahr);
    }

    public BeanMappingContext nachfolger() {
        Gueltigkeitsjahr nachfolgerJahr = Gueltigkeitsjahr.of(this.jahr.getJahr() + 1);
        return new BeanMappingContext(this.land, nachfolgerJahr);
    }

}
