package de.wps.bube.basis.koordinaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;
import static java.lang.String.format;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.koordinaten.persistence.GemeindekoordinatenRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

@Service
public class KoordinatenPruefungService {

    private final GemeindekoordinatenRepository gemeindekoordinatenRepository;
    private final GeometryFactory geometryFactory;
    private final ParameterService parameterService;

    public KoordinatenPruefungService(
            GemeindekoordinatenRepository gemeindekoordinatenRepository,
            ParameterService parameterService) {
        this.gemeindekoordinatenRepository = gemeindekoordinatenRepository;
        this.parameterService = parameterService;
        geometryFactory = JTSFactoryFinder.getGeometryFactory();
    }

    @Secured(BEHOERDENBENUTZER)
    public boolean isGeoPunktInAgsGeometrie(String ags, GeoPunkt geoPunkt, Gueltigkeitsjahr jahr, Land land, Referenz berichtsjahr) throws KoordinatenException {
        if(!this.agsExists(ags)) {
            throw new KoordinatenException(format("Der AGS-Code %s ist nicht bekannt.", ags));
        }

        ParameterWert geo_buff = this.parameterService.findParameterWert(Parameter.GEO_BUFFER, jahr, land).orElseThrow(() ->
                new KoordinatenException(format("Der Parameter %s wurde für das Jahr %s und das Land %s nicht gefunden", Parameter.GEO_BUFFER, jahr.getJahr(), land.name())));
        int buffer = Integer.parseInt(geo_buff.getWert());

        // Nutze ausgewähltes Berichtsjahr, wenn keine Daten vorhanden, nutze neustes Berichtsjahr mit Daten
        if(this.gemeindekoordinatenRepository.intersectsPointByAgs(toPoint(geoPunkt),
                buffer, ags, berichtsjahr.getId())) {
            return true;
        }

        Referenz newestBerichtsjahr = this.gemeindekoordinatenRepository.findAllBerichtsjahrByAgs(ags).get(0);
        return this.gemeindekoordinatenRepository.intersectsPointByAgs(toPoint(geoPunkt),
                buffer, ags, newestBerichtsjahr.getId());
    }

    private boolean agsExists(String ags) {
        return this.gemeindekoordinatenRepository.existsByAgs(ags);
    }

    private Point toPoint(GeoPunkt geoPunkt) {
        Coordinate coord = new Coordinate(geoPunkt.getOst(), geoPunkt.getNord());
        Point point = geometryFactory.createPoint(coord);
        point.setSRID(Integer.parseInt(geoPunkt.getEpsgCode().getSchluessel()));
        return point;
    }
}
