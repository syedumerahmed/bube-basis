package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RZUST;

import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.BehoerdeBean;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

@RegelApi.Type(name = "ZuständigeBehörde")
public class ZustaendigeBehoerdeBean {

    private final BehoerdeBean behoerde;
    private final ReferenzBean zustaendigkeit;

    public ZustaendigeBehoerdeBean(BehoerdeBean behoerde, ReferenzBean zustaendigkeit) {
        this.behoerde = behoerde;
        this.zustaendigkeit = zustaendigkeit;
    }

    @RegelApi.Member(description = "Behörde")
    public BehoerdeBean getBehoerde() {
        return behoerde;
    }

    @RegelApi.Member(description = "Zuständigkeit", referenzliste = RZUST)
    public ReferenzBean getZustaendigkeit() {
        return zustaendigkeit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ZustaendigeBehoerdeBean that = (ZustaendigeBehoerdeBean) o;
        return Objects.equals(behoerde, that.behoerde) && Objects.equals(zustaendigkeit,
                that.zustaendigkeit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(behoerde, zustaendigkeit);
    }

    @Override
    public String toString() {
        return "ZustaendigeBehoerdeBean{" +
                "behoerde=" + behoerde +
                ", zustaendigkeit=" + zustaendigkeit +
                '}';
    }

}
