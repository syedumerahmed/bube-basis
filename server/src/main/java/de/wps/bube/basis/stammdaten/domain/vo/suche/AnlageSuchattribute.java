package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;

public class AnlageSuchattribute {
    public boolean searchAnlagenteile;
    public String name;
    public String nummer;
    public String localId;
    public List<Long> vorschriftenIn;
    public List<Long> zustaendigkeitIn;
    public List<Long> behoerdeIn;
    public List<Long> statusIn;
    public Boolean uebernommen;
    public Taetigkeiten taetigkeiten = new Taetigkeiten();

    public static class Taetigkeiten {
        public List<Long> nr4BImSchV;
        public List<Long> prtr;
        public List<Long> ierl;
    }

}
