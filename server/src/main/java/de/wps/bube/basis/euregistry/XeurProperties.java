package de.wps.bube.basis.euregistry;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Validated
@ConfigurationProperties("bube.xeur")
public class XeurProperties {
    private @Valid @NotNull UbaProperties uba;

    public UbaProperties getUba() {
        return uba;
    }

    public void setUba(UbaProperties uba) {
        this.uba = uba;
    }

    public static final class UbaProperties {
        private @NotBlank String organisationName;
        private @NotBlank String individualName;
        private @NotBlank String electronicMailAddress;
        private @NotBlank String telephoneNo;
        private @NotBlank String faxNo;
        private @Valid @NotNull AddressProperties address;

        public UbaProperties(String organisationName, String individualName, String electronicMailAddress,
                String telephoneNo, String faxNo, AddressProperties address) {
            this.organisationName = organisationName;
            this.individualName = individualName;
            this.electronicMailAddress = electronicMailAddress;
            this.telephoneNo = telephoneNo;
            this.faxNo = faxNo;
            this.address = address;
        }

        public UbaProperties() {
        }

        public String getOrganisationName() {
            return organisationName;
        }

        public void setOrganisationName(String organisationName) {
            this.organisationName = organisationName;
        }

        public String getIndividualName() {
            return individualName;
        }

        public void setIndividualName(String individualName) {
            this.individualName = individualName;
        }

        public String getElectronicMailAddress() {
            return electronicMailAddress;
        }

        public void setElectronicMailAddress(String electronicMailAddress) {
            this.electronicMailAddress = electronicMailAddress;
        }

        public String getTelephoneNo() {
            return telephoneNo;
        }

        public void setTelephoneNo(String telephoneNo) {
            this.telephoneNo = telephoneNo;
        }

        public String getFaxNo() {
            return faxNo;
        }

        public void setFaxNo(String faxNo) {
            this.faxNo = faxNo;
        }

        public AddressProperties getAddress() {
            return address;
        }

        public void setAddress(AddressProperties address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return "UbaProperties[" +
                   "organisationName=" + organisationName + ", " +
                   "individualName=" + individualName + ", " +
                   "electronicMailAddress=" + electronicMailAddress + ", " +
                   "telephoneNo=" + telephoneNo + ", " +
                   "faxNo=" + faxNo + ", " +
                   "address=" + address + ']';
        }

        public static final class AddressProperties {
            private @NotBlank String streetName;
            private @NotBlank String buildingNumber;
            private @NotBlank String city;
            private @NotBlank String postalCode;

            public AddressProperties(String streetName, String buildingNumber, String city, String postalCode) {
                this.streetName = streetName;
                this.buildingNumber = buildingNumber;
                this.city = city;
                this.postalCode = postalCode;
            }

            public AddressProperties() {
            }

            public String getStreetName() {
                return streetName;
            }

            public void setStreetName(String streetName) {
                this.streetName = streetName;
            }

            public String getBuildingNumber() {
                return buildingNumber;
            }

            public void setBuildingNumber(String buildingNumber) {
                this.buildingNumber = buildingNumber;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public void setPostalCode(String postalCode) {
                this.postalCode = postalCode;
            }

            @Override
            public String toString() {
                return "AddressProperties[" +
                       "streetName=" + streetName + ", " +
                       "buildingNumber=" + buildingNumber + ", " +
                       "city=" + city + ", " +
                       "postalCode=" + postalCode + ']';
            }

        }
    }
}
