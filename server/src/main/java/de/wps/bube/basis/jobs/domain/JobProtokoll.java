package de.wps.bube.basis.jobs.domain;

import java.util.StringJoiner;

import de.wps.bube.basis.base.mapping.MappingError;

public class JobProtokoll {

    private final StringJoiner jobProtokoll = new StringJoiner(System.lineSeparator());
    private boolean warnungVorhanden = false;

    /**
     * Öffentlicher Konstruktor eines JobProtokolls mit einem Eröffnungstext.
     *
     * @param opening Der Eröffnungstext des JobProtokolls.
     */
    public JobProtokoll(String opening) {
        jobProtokoll.add(opening);
    }

    public JobProtokoll() {
    }

    public void addEintrag(String eintrag) {
        jobProtokoll.add(eintrag);
    }

    public void addWarnung(String warnung) {
        jobProtokoll.add(warnung);
        warnungVorhanden = true;
    }

    public void addEintrag(MappingError error) {
        jobProtokoll.add(error.toString());
        if (error.istWarnung()) {
            warnungVorhanden = true;
        }
    }

    public void addFehler(String eintrag, String technischeUrsache) {
        jobProtokoll.add(eintrag);
        jobProtokoll.add("Technische Ursache: " + technischeUrsache);
    }

    public String getProtokoll() {
        return jobProtokoll.toString();
    }

    public boolean isWarnungVorhanden() {
        return warnungVorhanden;
    }
}
