package de.wps.bube.basis.base.persistence;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;

public class JoinProperties<J> {

    public final EntityPath<J> entityPath;
    public final Path<J> path;

    public JoinProperties(EntityPath<J> entityPath, Path<J> path) {
        this.entityPath = entityPath;
        this.path = path;
    }
}
