package de.wps.bube.basis.stammdatenbetreiber.domain.vo;

public class SpbQuelleListItem {

    private Long spbQuelleId;
    private String quelleNr;
    private String name;
    private boolean quelleVorhanden;
    private String originalQuelleNr;

    public SpbQuelleListItem(Long spbQuelleId, String quelleNr, String name, boolean quelleVorhanden,
            String originalQuelleNr) {
        this.spbQuelleId = spbQuelleId;
        this.quelleNr = quelleNr;
        this.name = name;
        this.quelleVorhanden = quelleVorhanden;
        this.originalQuelleNr = originalQuelleNr;
    }

    public Long getSpbQuelleId() {
        return spbQuelleId;
    }

    public void setSpbQuelleId(Long spbQuelleId) {
        this.spbQuelleId = spbQuelleId;
    }

    public String getQuelleNr() {
        return quelleNr;
    }

    public void setQuelleNr(String quelleNr) {
        this.quelleNr = quelleNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isQuelleVorhanden() {
        return quelleVorhanden;
    }

    public void setQuelleVorhanden(boolean quelleVorhanden) {
        this.quelleVorhanden = quelleVorhanden;
    }

    public String getOriginalQuelleNr() {
        return originalQuelleNr;
    }

    public void setOriginalQuelleNr(String originalQuelleNr) {
        this.originalQuelleNr = originalQuelleNr;
    }

    public boolean isVeraendert() {
        return spbQuelleId != null && quelleVorhanden;
    }

    public boolean isNeu() {
        return spbQuelleId != null && !quelleVorhanden;
    }

}
