package de.wps.bube.basis.euregistry.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;

import java.time.LocalDate;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.BehoerdeBean;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class BerichtsdatenBean {

    private final BehoerdeBean zustaendigeBehoerde;
    private final String akz;
    private final ReferenzBean berichtsart;
    private final ReferenzBean bearbeitungsstatus;
    private final LocalDate bearbeitungsstatusSeit;
    private final String bemerkung;
    private final String thematicId;
    private final String schema;

    public BerichtsdatenBean(BehoerdeBean zustaendigeBehoerde, String akz,
            ReferenzBean berichtsart, ReferenzBean bearbeitungsstatus, LocalDate bearbeitungsstatusSeit,
            String bemerkung, String thematicId, String schema) {
        this.zustaendigeBehoerde = zustaendigeBehoerde;
        this.akz = akz;
        this.berichtsart = berichtsart;
        this.bearbeitungsstatus = bearbeitungsstatus;
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
        this.bemerkung = bemerkung;
        this.thematicId = thematicId;
        this.schema = schema;
    }

    @RegelApi.Member(description = "Zuständige Behörde")
    public BehoerdeBean getZustaendigeBehoerde() {
        return zustaendigeBehoerde;
    }

    @RegelApi.Member(description = "AKZ")
    public String getAkz() {
        return akz;
    }

    @RegelApi.Member(description = "Berichtsart", referenzliste = RRTYP)
    public ReferenzBean getBerichtsart() {
        return berichtsart;
    }

    @RegelApi.Member(description = "Bearbeitungsstatus", referenzliste = RBEA)
    public ReferenzBean getBearbeitungsstatus() {
        return bearbeitungsstatus;
    }

    @RegelApi.Member(description = "Datum der letzten Änderung des Bearbeitungsstatus")
    public LocalDate getBearbeitungsstatusSeit() {
        return bearbeitungsstatusSeit;
    }

    @RegelApi.Member(description = "Bemerkung")
    public String getBemerkung() {
        return bemerkung;
    }

    public String getThematicId() {
        return thematicId;
    }

    public String getSchema() {
        return schema;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BerichtsdatenBean that = (BerichtsdatenBean) o;
        return Objects.equals(zustaendigeBehoerde, that.zustaendigeBehoerde) && Objects.equals(akz,
                that.akz) && Objects.equals(berichtsart, that.berichtsart) && Objects.equals(bearbeitungsstatus,
                that.bearbeitungsstatus) && Objects.equals(bearbeitungsstatusSeit,
                that.bearbeitungsstatusSeit) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(thematicId,
                that.thematicId) && Objects.equals(schema, that.schema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zustaendigeBehoerde, akz, berichtsart, bearbeitungsstatus, bearbeitungsstatusSeit,
                bemerkung, thematicId, schema);
    }

    @Override
    public String toString() {
        return "BerichtsdatenBean{" +
                "zustaendigeBehoerde=" + zustaendigeBehoerde +
                ", akz='" + akz + '\'' +
                ", berichtsart=" + berichtsart +
                ", bearbeitungsstatus=" + bearbeitungsstatus +
                ", bearbeitungsstatusSeit=" + bearbeitungsstatusSeit +
                ", bemerkung='" + bemerkung + '\'' +
                ", thematicId='" + thematicId + '\'' +
                ", schema='" + schema + '\'' +
                '}';
    }
}
