package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegAnlage.eURegAnlage;
import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository.berichtsjahrIdEquals;
import static de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository.byJahrAndLocalId;
import static de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository.byStammdatenKey;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.URL_PUBLIC_DISCLOSURE_KEY;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.*;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.UebernimmEuRegDatenInJahrCommand;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.domain.event.EuRegDatenUebernehmenTriggeredEvent;
import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.persistence.EuRegAnlageRepository;
import de.wps.bube.basis.euregistry.security.EuRegAnlageBerechtigungsAdapter;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAdapter;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageBearbeitetEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageWurdeGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilBearbeitetEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilWurdeGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaetteGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsobjektService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.security.BetriebsstaetteBerechtigungsAdapter;

@Service
public class EURegistryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EURegistryService.class);

    static final String BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL = "00";

    private final EURegBetriebsstaetteRepository euRegBetriebsstaetteRepository;
    private final EuRegAnlageRepository euRegAnlageRepository;
    private final ReferenzenService referenzenService;
    private final StammdatenService stammdatenService;
    private final BerichtsobjektService berichtsobjektService;
    private final KomplexpruefungService komplexpruefungService;
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final EuRegBstBerechtigungsAccessor berechtigungsAccessor;
    private final ParameterService parameterService;
    private final AktiverBenutzer bearbeiter;

    private final EuRegUpdater euRegUpdater;
    private final ApplicationEventPublisher eventPublisher;
    private final EuRegistryBerichtsjahrwechselMapper euRegistryBerichtsjahrwechselMapper;

    public EURegistryService(EURegBetriebsstaetteRepository euRegBetriebsstaetteRepository,
            ReferenzenService referenzenService,
            StammdatenService stammdatenService,
            BerichtsobjektService berichtsobjektService,
            StammdatenBerechtigungsContext stammdatenBerechtigungsContext,
            EuRegBstBerechtigungsAccessor berechtigungsAccessor,
            EuRegAnlageRepository euRegAnlageRepository,
            KomplexpruefungService komplexpruefungService,
            ParameterService parameterService,
            ApplicationEventPublisher eventPublisher,
            EuRegistryBerichtsjahrwechselMapper euRegistryBerichtsjahrwechselMapper,
            AktiverBenutzer bearbeiter,
            EuRegUpdater euRegUpdater) {
        this.euRegBetriebsstaetteRepository = euRegBetriebsstaetteRepository;
        this.referenzenService = referenzenService;
        this.stammdatenService = stammdatenService;
        this.berichtsobjektService = berichtsobjektService;
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.berechtigungsAccessor = berechtigungsAccessor;
        this.euRegAnlageRepository = euRegAnlageRepository;
        this.komplexpruefungService = komplexpruefungService;
        this.parameterService = parameterService;
        this.eventPublisher = eventPublisher;
        this.euRegistryBerichtsjahrwechselMapper = euRegistryBerichtsjahrwechselMapper;
        this.bearbeiter = bearbeiter;
        this.euRegUpdater = euRegUpdater;
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegBetriebsstaette loadBetriebsstaettenBerichtByJahrLandNummer(String jahrSchluessel, Land land,
            String nummer) {
        return euRegBetriebsstaetteRepository
                .findOne(stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                                       .and(byStammdatenKey(jahrSchluessel, land, nummer)))
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegBetriebsstaette.class));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegBetriebsstaette loadBetriebsstaettenBerichtByJahrAndLocalId(String jahrSchluessel,
            String localId) {
        return findBetriebsstaettenBerichtByJahrAndLocalId(jahrSchluessel, localId)
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegBetriebsstaette.class));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Optional<EURegBetriebsstaette> findBetriebsstaettenBerichtByJahrAndLocalId(String jahrSchluessel,
            String localId) {
        return euRegBetriebsstaetteRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(byJahrAndLocalId(jahrSchluessel, localId)));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegBetriebsstaette loadBetriebsstaettenBericht(Long id) {
        return findBetriebsstaettenBericht(id)
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegBetriebsstaette.class, "ID " + id));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Optional<EURegBetriebsstaette> findBetriebsstaettenBericht(Long id) {
        return euRegBetriebsstaetteRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.id.eq(id)));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegBetriebsstaette loadBetriebsstaettenBerichtById(Long berichtId) {
        return euRegBetriebsstaetteRepository.findOne(stammdatenBerechtigungsContext
                                                     .createPredicateRead(berechtigungsAccessor)
                                                     .and(eURegBetriebsstaette.id.eq(berichtId)))
                                             .orElseThrow(
                                                     () -> new BubeEntityNotFoundException(EURegBetriebsstaette.class));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Optional<EURegBetriebsstaette> findBetriebsstaettenBerichtByBstId(Long bstId) {
        return euRegBetriebsstaetteRepository.findOne(stammdatenBerechtigungsContext
                .createPredicateRead(berechtigungsAccessor)
                .and(eURegBetriebsstaette.betriebsstaette.id.eq(bstId)));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegBetriebsstaette loadBetriebsstaettenBerichtByBstId(Long bstId) {
        return findBetriebsstaettenBerichtByBstId(bstId).orElseThrow(
                () -> new BubeEntityNotFoundException(EURegBetriebsstaette.class));
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleBetriebsstaetteGeloeschtEvent(BetriebsstaetteGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", BetriebsstaetteGeloeschtEvent.EVENT_NAME);
        deleteBerichtsdatenBst(event.betriebsstaetteId);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleAnlageGeloeschtEvent(AnlageGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageGeloeschtEvent.EVENT_NAME);
        deleteBerichtsdatenAnlage(event.anlageId);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleAnlageWurdeGeloeschtEvent(AnlageWurdeGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageWurdeGeloeschtEvent.EVENT_NAME);
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(event.bstId);
        createOrDeleteBerichtsdatenBst(bst);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleAnlagenteilGeloeschtEvent(AnlagenteilGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlagenteilGeloeschtEvent.EVENT_NAME);
        deleteBerichtsdatenAnlagenteil(event.anlagenteilId);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleAnlagenteilWurdeGeloeschtEvent(AnlagenteilWurdeGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlagenteilWurdeGeloeschtEvent.EVENT_NAME);
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(event.bstId);
        createOrDeleteBerichtsdatenBst(bst);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleAnlageAngelegtEvent(AnlageAngelegtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageAngelegtEvent.EVENT_NAME);
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(event.anlage.getParentBetriebsstaetteId());
        createOrDeleteBerichtsdatenBst(bst);
        createOrDeleteBerichtsdatenAnlage(event.anlage, bst);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleAnlageBearbeitetEvent(AnlageBearbeitetEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageBearbeitetEvent.EVENT_NAME);
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(event.anlage.getParentBetriebsstaetteId());
        createOrDeleteBerichtsdatenBst(bst);
        createOrDeleteBerichtsdatenAnlage(event.anlage, bst);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleAnlagenteilAngelegtEvent(AnlagenteilAngelegtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlagenteilAngelegtEvent.EVENT_NAME);
        Anlage a = stammdatenService.loadAnlage(event.anlagenteil.getParentAnlageId());
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(a.getParentBetriebsstaetteId());
        createOrDeleteBerichtsdatenBst(bst);
        createOrDeleteBerichtsdatenAnlagenteil(event.anlagenteil, bst);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleAnlagenteilBearbeitetEvent(AnlagenteilBearbeitetEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlagenteilBearbeitetEvent.EVENT_NAME);
        Anlage a = stammdatenService.loadAnlage(event.anlagenteil.getParentAnlageId());
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(a.getParentBetriebsstaetteId());
        createOrDeleteBerichtsdatenBst(bst);
        createOrDeleteBerichtsdatenAnlagenteil(event.anlagenteil, bst);
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void createOrDeleteBerichtsdatenBst(Betriebsstaette bst) {

        // Berichtsart Matrix fragen
        if (bst.hatEURegBst()) {
            // EuRegBst sollte vorhanden sein. Prüfen und falls nicht vorhanden anlegen
            if (!euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())) {
                createEURegBetriebsstaette(bst);
                LOGGER.info("EURegBst angelegt für bst.id=" + bst.getId());
            }
        } else {
            // EuRegBst sollte nicht vorhanden sein. Prüfen und falls vorhanden löschen
            deleteBerichtsdatenBst(bst.getId());
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void deleteBerichtsdatenBst(Long betriebsstaetteId) {
        // Prüfen und falls vorhanden löschen
        if (euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(betriebsstaetteId)) {
            Long euRegBetriebsstaetteId = euRegBetriebsstaetteRepository
                    .findAllIds(eURegBetriebsstaette.betriebsstaette.id.eq(betriebsstaetteId))
                    .get(0);
            Optional<EURegBetriebsstaette> euRegBst = euRegBetriebsstaetteRepository.findById(euRegBetriebsstaetteId);
            euRegBst.map(EURegBetriebsstaette::getKomplexpruefung).ifPresent(komplexpruefungService::delete);
            euRegBetriebsstaetteRepository.deleteByBetriebsstaetteId(betriebsstaetteId);
            berichtsobjektService.deleteBerichtsobjekt(betriebsstaetteId, Berichtsdaten.EUREG_BST_SCHLUESSEL);
            LOGGER.info("EURegBst gelöscht für bst.id=" + betriebsstaetteId);
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void createEURegBetriebsstaette(Betriebsstaette bst) {

        // Berichtsart = EURegBST
        Referenz berichtsart = referenzenService.readReferenz(Referenzliste.RRTYP, Berichtsdaten.EUREG_BST_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand());
        // Status = unbearbeitet
        Referenz bearbeitungsstatus = referenzenService.readReferenz(Referenzliste.RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand());
        Berichtsdaten berichtsdaten = new Berichtsdaten(bst.getZustaendigeBehoerde(), bst.getAkz(), berichtsart,
                bearbeitungsstatus, LocalDate.now());

        EURegBetriebsstaette euRegBst = new EURegBetriebsstaette(bst, berichtsdaten);
        stammdatenBerechtigungsContext.checkAccessToWrite(new EuRegBstBerechtigungsAdapter(euRegBst));

        euRegBst = euRegBetriebsstaetteRepository.save(euRegBst);
        berichtsobjektService.addBerichtsobjekt(bst.getId(), berichtsart.getSchluessel(),
                bearbeitungsstatus.getSchluessel(), euRegBst.getBerichtsdaten().getBearbeitungsstatusSeit(),
                "/eureg/berichtsdaten/betriebsstaette/" + euRegBst.getId());
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public EURegBetriebsstaette updateBetriebsstaettenBericht(EURegBetriebsstaette euRegBetriebsstaette) {
        Assert.notNull(euRegBetriebsstaette.getId(), "EURegBetriebsstaette ohne ID");
        Assert.notNull(euRegBetriebsstaette.getBerichtsdaten(), "EURegBetriebsstaette ohne Berichtsdaten");

        EURegBetriebsstaette existing = loadBetriebsstaettenBericht(euRegBetriebsstaette.getId());

        // Hat sich der Bearbeitungsstatus geändert dann setzte das Bearbeitungsstatus-Seit Datum auf das aktuelle Datum
        if (!euRegBetriebsstaette.getBerichtsdaten()
                                 .getBearbeitungsstatus()
                                 .equals(existing.getBerichtsdaten().getBearbeitungsstatus())) {
            euRegBetriebsstaette.getBerichtsdaten().setBearbeitungsstatusSeit(LocalDate.now());
        }

        euRegUpdater.update(existing, euRegBetriebsstaette);

        // Prüfe Berechtigung der EURegBetriebsstaette
        stammdatenBerechtigungsContext.checkAccessToWrite(new EuRegBstBerechtigungsAdapter(existing));

        EURegBetriebsstaette result = euRegBetriebsstaetteRepository.save(existing);
        berichtsobjektService.updateBerichtsobjekt(existing.getBetriebsstaette().getId(),
                Berichtsdaten.EUREG_BST_SCHLUESSEL, existing.getBerichtsdaten().getBearbeitungsstatus().getSchluessel(),
                existing.getBerichtsdaten().getBearbeitungsstatusSeit());
        return result;
    }

    public boolean canWrite(EURegBetriebsstaette euRegBetriebsstaette) {
        if (bearbeiter.hatEineDerRollen(Rolle.LAND_WRITE, Rolle.BEHOERDE_WRITE)) {
            return stammdatenBerechtigungsContext.hasAccessToWrite(
                    new EuRegBstBerechtigungsAdapter(
                            euRegBetriebsstaette)) || stammdatenBerechtigungsContext.hasAccessToWrite(
                    new BetriebsstaetteBerechtigungsAdapter(euRegBetriebsstaette.getBetriebsstaette()));
        }
        return false;
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void createOrDeleteBerichtsdatenAnlage(Anlage anlage, Betriebsstaette bst) {

        // Berichtsart Matrix fragen
        if (anlage.hatEURegAnl()) {
            // EuRegAnl sollte vorhanden sein. Prüfen und falls nicht vorhanden anlegen
            if (!euRegAnlageRepository.existsByAnlageId(anlage.getId())) {
                createEURegAnlage(anlage, null, bst);
                LOGGER.info("EURegAnl angelegt für anlage.id=" + anlage.getId());
            } else {
                // EuRegAnl ist vorhanden, dann Update der Daten vornehmen
                updateBerichtAnlage(anlage, bst);
            }
        } else {
            // EuRegAnl sollte nicht vorhanden sein. Prüfen und falls vorhanden löschen
            deleteBerichtsdatenAnlage(anlage.getId());
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void createOrDeleteBerichtsdatenAnlagenteil(Anlagenteil anlagenteil, Betriebsstaette bst) {

        // Berichtsart Matrix fragen
        if (anlagenteil.hatEURegAnl()) {
            // EuRegAnl sollte vorhanden sein. Prüfen und falls nicht vorhanden anlegen
            if (!euRegAnlageRepository.existsByAnlagenteilId(anlagenteil.getId())) {
                createEURegAnlage(null, anlagenteil, bst);
                LOGGER.info("EURegAnl angelegt für anlagenteil.id=" + anlagenteil.getId());
            } else {
                // EuRegAnl ist vorhanden, dann Update der Daten vornehmen
                updateBerichtAnlagenteil(anlagenteil, bst);
            }
        } else {
            // EuRegAnl sollte nicht vorhanden sein. Prüfen und falls vorhanden löschen
            deleteBerichtsdatenAnlagenteil(anlagenteil.getId());
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void deleteBerichtsdatenAnlage(Long anlageId) {
        // Prüfen und falls vorhanden löschen
        if (euRegAnlageRepository.existsByAnlageId(anlageId)) {
            euRegAnlageRepository.deleteByAnlageId(anlageId);
            LOGGER.info("EURegAnl gelöscht für anlage.id=" + anlageId);
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void deleteBerichtsdatenAnlagenteil(Long anlagenteilId) {
        // Prüfen und falls vorhanden löschen
        if (euRegAnlageRepository.existsByAnlagenteilId(anlagenteilId)) {
            euRegAnlageRepository.deleteByAnlagenteilId(anlagenteilId);
            LOGGER.info("EURegAnl gelöscht für anlagenteil.id=" + anlagenteilId);
        }
    }

    // Nur zum internen Gebrauch über die Event-Handler
    private void createEURegAnlage(Anlage anlage, Anlagenteil anlagenteil, Betriebsstaette bst) {

        String berichtsartSchluessel = Berichtsdaten.EUREG_ANL_SCHLUESSEL;
        FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten = null;
        //Ist es eine Feuerungsanlage?
        if (anlage != null && anlage.isFeuerungsanlage() || anlagenteil != null && anlagenteil.isFeuerungsanlage()) {
            berichtsartSchluessel = Berichtsdaten.EUREG_F_SCHLUESSEL;
            feuerungsanlageBerichtsdaten = createFeuerungsanlageBerichtsdaten(bst,
                    ((anlage != null) && anlage.isFeuerungsanlageMit17BV())
                            || ((anlagenteil != null) && anlagenteil.isFeuerungsanlageMit17BV()));
        }

        // Berichtsart = EURegANL bei Anlage, oder Berichtsart = EURegF bei Feuerungsanlage
        Referenz berichtsart = referenzenService.readReferenz(Referenzliste.RRTYP, berichtsartSchluessel,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand());
        // Status = unbearbeitet
        Referenz bearbeitungsstatus = referenzenService.readReferenz(Referenzliste.RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand());
        Berichtsdaten berichtsdaten = new Berichtsdaten(bst.getZustaendigeBehoerde(), bst.getAkz(), berichtsart,
                bearbeitungsstatus, LocalDate.now());

        EURegAnlage euRegAnl = new EURegAnlage(anlage, anlagenteil, berichtsdaten);
        euRegAnl.setFeuerungsanlage(feuerungsanlageBerichtsdaten);
        euRegAnl.setLocalId2(anlage != null ? anlage.getLocalId() : anlagenteil.getLocalId());
        euRegAnl.setReport(Ausgangszustandsbericht.NICHT_ERFORDERLICH);
        euRegAnl.setVisits(0);

        //VisitUrl als ParameterWert beziehen. Dies ist ein Pflichtfeld
        Optional<ParameterWert> visitUrlOpt = parameterService.findParameterWert(Parameter.URL_VISIT_KEY,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand(), bst.getZustaendigeBehoerde().getId());
        if (visitUrlOpt.isPresent()) {
            euRegAnl.setVisitUrl(visitUrlOpt.get().getWert());
        } else {
            euRegAnl.setVisitUrl("");
        }

        //Monitor als ParameterWert beziehen
        Optional<ParameterWert> monitorUrlOpt = parameterService.findParameterWert(Parameter.URL_MONI_KEY,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand(), bst.getZustaendigeBehoerde().getId());
        monitorUrlOpt.ifPresent(parameterWert -> euRegAnl.setMonitorUrl(parameterWert.getWert()));

        stammdatenBerechtigungsContext.checkAccessToWrite(new EuRegAnlageBerechtigungsAdapter(bst, euRegAnl));

        euRegAnlageRepository.save(euRegAnl);
    }

    private FeuerungsanlageBerichtsdaten createFeuerungsanlageBerichtsdaten(Betriebsstaette bst, boolean has17Bimschv) {
        FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten = new FeuerungsanlageBerichtsdaten(0.0, 0.0, 0.0);

        if (has17Bimschv) {
            // Öffentliche Bekanntmachung URL als ParameterWert beziehen
            parameterService.findParameterWert(URL_PUBLIC_DISCLOSURE_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                                    bst.getLand(), bst.getZustaendigeBehoerde().getId())
                            .map(ParameterWert::getWert)
                            .ifPresent(feuerungsanlageBerichtsdaten::setOeffentlicheBekanntmachungUrl);
        }

        return feuerungsanlageBerichtsdaten;
    }

    private void updateBerichtAnlage(Anlage anlage, Betriebsstaette bst) {
        checkWrite(bst.getId());
        EURegAnlage bericht = findAnlagenBerichtByAnlageId(anlage.getId())
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegAnlage.class));

        // Berichtsart ändert sich?
        if (anlage.isFeuerungsanlage() && bericht.isEURegAnl()) {
            updateBerichtsart(Berichtsdaten.EUREG_F_SCHLUESSEL, bericht, bst);
            bericht.setFeuerungsanlage(new FeuerungsanlageBerichtsdaten(0.0, 0.0, 0.0));
        } else if (!anlage.isFeuerungsanlage() && bericht.isEURegF()) {
            updateBerichtsart(Berichtsdaten.EUREG_ANL_SCHLUESSEL, bericht, bst);
        }

        // Berichtsdaten für Feuerungsanlage löschen?
        if (!anlage.isFeuerungsanlage()) {
            bericht.setFeuerungsanlage(null);
        } else {
            var feuerungsanlage = bericht.getFeuerungsanlage();
            if (anlage.isFeuerungsanlageMit17BV()) {
                // Öffentliche BekanntmachungUrl auf Default setzen, falls 17.BImSchV neu hinzugefügt wurde
                if (feuerungsanlage.getOeffentlicheBekanntmachungUrl() == null) {
                    parameterService.findParameterWert(URL_PUBLIC_DISCLOSURE_KEY,
                                            bst.getGueltigkeitsjahrFromBerichtsjahr(),
                                            bst.getLand(), bst.getZustaendigeBehoerde().getId())
                                    .map(ParameterWert::getWert)
                                    .ifPresent(feuerungsanlage::setOeffentlicheBekanntmachungUrl);
                }
            }

            // Wird nur die 13.BV gelöscht und die 17.BV ist noch vorhanden, so wird die Liste der IE-Ausnahmen (nach Art 31-35 IE-RL) leer gemacht.
            if (!anlage.isFeuerungsanlageMit13BV() && anlage.isFeuerungsanlageMit17BV()
                    && feuerungsanlage.getIeAusnahmen() != null) {
                feuerungsanlage.getIeAusnahmen().clear();
            }

            // BUBE-702 Wird die 17.BV gelöscht, so werden überflüssige Daten aus den Reitern „Spezielle Bedingungen“
            // und "13./17. BImSchV" (Abfallmengen) gelöscht.
            if (anlage.isFeuerungsanlageMit13BV() && !anlage.isFeuerungsanlageMit17BV()) {
                feuerungsanlage.clear17BVData();
            }
        }

        euRegAnlageRepository.save(bericht);

    }

    private void updateBerichtAnlagenteil(Anlagenteil anlagenteil, Betriebsstaette bst) {
        checkWrite(bst.getId());
        EURegAnlage bericht = findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegAnlage.class));

        //Berichtsart ändert sich?
        if (anlagenteil.isFeuerungsanlage() && bericht.isEURegAnl()) {
            updateBerichtsart(Berichtsdaten.EUREG_F_SCHLUESSEL, bericht, bst);
        } else if (!anlagenteil.isFeuerungsanlage() && bericht.isEURegF()) {
            updateBerichtsart(Berichtsdaten.EUREG_ANL_SCHLUESSEL, bericht, bst);
        }

        // Berichtsdaten für Feuerungsanlage löschen?
        if (!anlagenteil.isFeuerungsanlage()) {
            bericht.setFeuerungsanlage(null);
        } else {
            var feuerungsanlage = bericht.getFeuerungsanlage();
            if (anlagenteil.isFeuerungsanlageMit17BV()) {
                // Öffentliche BekanntmachungUrl auf Default setzen, falls 17.BImSchV neu hinzugefügt wurde
                if (feuerungsanlage.getOeffentlicheBekanntmachungUrl() == null) {
                    parameterService.findParameterWert(URL_PUBLIC_DISCLOSURE_KEY,
                                            bst.getGueltigkeitsjahrFromBerichtsjahr(),
                                            bst.getLand())
                                    .map(ParameterWert::getWert)
                                    .ifPresent(feuerungsanlage::setOeffentlicheBekanntmachungUrl);
                }
            }

            // Wird nur die 13.BV gelöscht und die 17.BV ist noch vorhanden, so wird die Liste der IE-Ausnahmen (nach Art 31-35 IE-RL) leer gemacht.
            if (!anlagenteil.isFeuerungsanlageMit13BV() && anlagenteil.isFeuerungsanlageMit17BV()) {
                feuerungsanlage.getIeAusnahmen().clear();
            }

            // BUBE-702 Wird die 17.BV gelöscht, so werden überflüssige Daten aus den Reitern „Spezielle Bedingungen“
            // und "13./17. BImSchV" (Abfallmengen) gelöscht.
            if (anlagenteil.isFeuerungsanlageMit13BV() && !anlagenteil.isFeuerungsanlageMit17BV()) {
                feuerungsanlage.clear17BVData();
            }
        }

        euRegAnlageRepository.save(bericht);
    }

    private void updateBerichtsart(String schluessel, EURegAnlage bericht, Betriebsstaette bst) {
        Referenz berichtsart = referenzenService.readReferenz(Referenzliste.RRTYP, schluessel,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand());

        bericht.setBerichtsart(berichtsart);
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegAnlage loadAnlagenBericht(Long betriebsstaetteId, String anlagenNummer) {
        checkRead(betriebsstaetteId);
        return euRegAnlageRepository.findOne(
                                            new BooleanBuilder(eURegAnlage.anlage.parentBetriebsstaetteId.eq(betriebsstaetteId)).and(
                                                    eURegAnlage.anlage.anlageNr.equalsIgnoreCase(anlagenNummer)))
                                    .orElseThrow(() -> new BubeEntityNotFoundException(EURegAnlage.class));
    }

    private EURegAnlage loadAnlagenBericht(Long euRegAnlagenId) {
        return euRegAnlageRepository.findById(euRegAnlagenId)
                                    .orElseThrow(() -> new BubeEntityNotFoundException(EURegAnlage.class));
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public EURegAnlage updateAnlagenBericht(EURegAnlage euRegAnlage) {
        EURegAnlage existing = loadAnlagenBericht(euRegAnlage.getId());
        if (existing.getAnlage() != null) {
            checkWrite(existing.getAnlage().getParentBetriebsstaetteId());
        } else {
            checkWriteAnlage(existing.getAnlagenteil().getParentAnlageId());
        }

        // Hat sich der Bearbeitungsstatus geändert dann setzte das Bearbeitungsstatus-Seit Datum auf das aktuelle Datum
        if (!euRegAnlage.getBerichtsdaten()
                        .getBearbeitungsstatus()
                        .equals(existing.getBerichtsdaten().getBearbeitungsstatus())) {
            euRegAnlage.getBerichtsdaten().setBearbeitungsstatusSeit(LocalDate.now());
        }

        euRegUpdater.update(existing, euRegAnlage);
        return euRegAnlageRepository.save(existing);
    }

    private boolean existsByBerichtsjahrIdAndLocalId(Long zieljahrId, String localId) {
        return this.euRegBetriebsstaetteRepository.findOne(
                eURegBetriebsstaette.betriebsstaette.localId.eq(localId)
                                                            .and(berichtsjahrIdEquals(zieljahrId))).isPresent();
    }

    // Wird vom EUReg-Daten übernehmen Job aufgerufen
    @Secured({ Rollen.LAND_WRITE, BEHOERDE_WRITE })
    public boolean copyDaten(Betriebsstaette betriebsstaette, Referenz zieljahr, JobProtokoll jobProtokoll) {

        if (betriebsstaette.getLocalId() == null) {
            String msg = format("Betriebsstätte %s Nr. %s kann nicht kopiert werden: Local-ID muss gesetzt sein.",
                    betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr());
            jobProtokoll.addEintrag(msg);
            return false;
        }

        if (!existsByBerichtsjahrIdAndLocalId(zieljahr.getId(), betriebsstaette.getLocalId())) {
            jobProtokoll.addEintrag("Betriebsstätte %s Nr. %s. Existiert nicht und wird nicht übernommen"
                    .formatted(betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr()));
            return false;
        }

        try {

            List<String> anlagenLocalIds = betriebsstaette.getAnlagen()
                                                          .stream()
                                                          .map(Anlage::getLocalId)
                                                          .filter(Objects::nonNull)
                                                          .collect(toList());
            List<String> anlagenTeileLocalIDs = betriebsstaette.getAnlagen()
                                                               .stream()
                                                               .flatMap(anlage -> anlage.getAnlagenteile().stream().map(
                                                                       Anlagenteil::getLocalId))
                                                               .filter(Objects::nonNull)
                                                               .collect(toList());

            copyEuRegBetriebsstaetteDaten(betriebsstaette, zieljahr, jobProtokoll);
            anlagenLocalIds.forEach(
                    anlagenLocalId -> copyEuRegAnlageDaten(anlagenLocalId, zieljahr, betriebsstaette, jobProtokoll));
            anlagenTeileLocalIDs.forEach(
                    anlagenLocalId -> copyEuRegAnlagenTeilDaten(anlagenLocalId, zieljahr, betriebsstaette,
                            jobProtokoll));
            return true;
        } catch (Exception e) {
            jobProtokoll.addFehler(format("Betriebsstätte %s Nr. %s. Fehler", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()), e.getMessage());
            throw e;
        }
    }

    private void copyEuRegAnlageDaten(String euRegAnlageLocalId, Referenz zieljahr,
            Betriebsstaette bst,
            JobProtokoll jobProtokoll) {

        EURegAnlage euRegAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlage(euRegAnlageLocalId,
                bst.getBerichtsjahr());
        EURegAnlage newEuRegAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlage(euRegAnlageLocalId,
                zieljahr);

        if (euRegAnlage == null) {
            return;
        }

        if (newEuRegAnlage == null) {
            jobProtokoll.addEintrag(
                    format("EuRegAnlagenBericht %s Nr. %s. wurde übersprungen, da keine Daten im Zieljahr vorhanden sind.",
                            euRegAnlage.getAnlage().getName(), euRegAnlage.getAnlage().getAnlageNr())
            );
            return;
        }

        if (newEuRegAnlage.getBerichtsdaten()
                          .getBearbeitungsstatus()
                          .getSchluessel()
                          .equals(BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL)) {

            var errorList = euRegistryBerichtsjahrwechselMapper.uebernimmBerichtsdaten(euRegAnlage, newEuRegAnlage,
                    zieljahr, bst);

            errorList.forEach(jobProtokoll::addEintrag);

            euRegAnlageRepository.save(newEuRegAnlage);
            jobProtokoll.addEintrag(
                    format("EuRegAnlagenBericht %s Nr. %s. Fehlerfrei", euRegAnlage.getAnlage().getName(),
                            euRegAnlage.getAnlage().getAnlageNr()));
        } else {
            jobProtokoll.addWarnung(
                    format("EuRegAnlagenBericht %s Nr. %s. wurde bereits bearbeitet und deshalb nicht kopiert.",
                            euRegAnlage.getAnlage().getName(),
                            euRegAnlage.getAnlage().getAnlageNr()));
        }

    }

    private void copyEuRegAnlagenTeilDaten(String euRegAnlageLocalId, Referenz zieljahr,
            Betriebsstaette bst,
            JobProtokoll jobProtokoll) {

        EURegAnlage euRegAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlagenTeil(euRegAnlageLocalId,
                bst.getBerichtsjahr());
        EURegAnlage newEuRegAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlagenTeil(
                euRegAnlageLocalId, zieljahr);

        if (euRegAnlage == null) {
            return;
        }

        if (newEuRegAnlage == null) {
            jobProtokoll.addEintrag(
                    format("EuRegAnlagenBericht %s Nr. %s. wurde übersprungen, da keine Daten im Zieljahr vorhanden sind.",
                            euRegAnlage.getAnlagenteil().getName(), euRegAnlage.getAnlagenteil().getAnlagenteilNr())
            );
            return;
        }
        if (newEuRegAnlage.getBerichtsdaten()
                          .getBearbeitungsstatus()
                          .getSchluessel()
                          .equals(BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL)) {

            var errorList = euRegistryBerichtsjahrwechselMapper.uebernimmBerichtsdaten(euRegAnlage,
                    newEuRegAnlage, zieljahr, bst);

            errorList.forEach(jobProtokoll::addEintrag);

            euRegAnlageRepository.save(newEuRegAnlage);
            jobProtokoll.addEintrag(
                    format("EuRegAnlagenBericht %s Nr. %s. Fehlerfrei", euRegAnlage.getAnlagenteil().getName(),
                            euRegAnlage.getAnlagenteil().getAnlagenteilNr()));
        } else {
            jobProtokoll.addWarnung(
                    format("EuRegAnlagenBericht %s Nr. %s. wurde bereits bearbeitet und deshalb nicht kopiert.",
                            euRegAnlage.getAnlagenteil().getName(),
                            euRegAnlage.getAnlagenteil().getAnlagenteilNr()));
        }

    }

    private void copyEuRegBetriebsstaetteDaten(Betriebsstaette betriebsstaette, Referenz zieljahr,
            JobProtokoll jobProtokoll) {

        String betriebsstaetteLocalId = betriebsstaette.getLocalId();

        EURegBetriebsstaette euRegBetriebsstaette = euRegBetriebsstaetteRepository
                .findOne(stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                                       .and(eURegBetriebsstaette.betriebsstaette.localId
                                                               .eq(betriebsstaetteLocalId))
                                                       .and(eURegBetriebsstaette.betriebsstaette.berichtsjahr.schluessel
                                                               .eq(betriebsstaette.getBerichtsjahr().getSchluessel())))
                .orElseThrow(() -> new BubeEntityNotFoundException(EURegBetriebsstaette.class));
        EURegBetriebsstaette newEuRegBetriebsstaette = euRegBetriebsstaetteRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.betriebsstaette.localId.eq(
                                                      betriebsstaetteLocalId))
                                              .and(eURegBetriebsstaette.betriebsstaette.berichtsjahr.schluessel
                                                      .eq(zieljahr.getSchluessel()))).orElse(null);
        if (newEuRegBetriebsstaette != null) {
            if (newEuRegBetriebsstaette.getBerichtsdaten()
                                       .getBearbeitungsstatus()
                                       .getSchluessel()
                                       .equals(BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL)) {
                var copyWithError = euRegistryBerichtsjahrwechselMapper.copyEuRegBerichtsdaten(
                        euRegBetriebsstaette.getBerichtsdaten(), zieljahr, betriebsstaette);

                copyWithError.errorsOrEmpty().forEach(jobProtokoll::addEintrag);

                newEuRegBetriebsstaette.setBerichtsdaten(copyWithError.entity);
                euRegBetriebsstaetteRepository.save(newEuRegBetriebsstaette);
                jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s. Fehlerfrei", betriebsstaette.getName(),
                        betriebsstaette.getBetriebsstaetteNr()));
            } else {
                jobProtokoll.addWarnung(
                        format("Betriebsstätte %s Nr. %s. wurde bereits bearbeitet und deshalb nicht kopiert.",
                                betriebsstaette.getName(),
                                betriebsstaette.getBetriebsstaetteNr()));
            }
        }
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Optional<EURegAnlage> findAnlagenBerichtByAnlageId(Long anlageId) {
        var bericht = euRegAnlageRepository.findOne(eURegAnlage.anlage.id.eq(anlageId));
        bericht.ifPresent(b -> checkRead(b.getAnlage().getParentBetriebsstaetteId()));
        return bericht;
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Optional<EURegAnlage> findAnlagenBerichtByAnlagenteilId(Long anlagenteilId) {
        var bericht = euRegAnlageRepository.findOne(eURegAnlage.anlagenteil.id.eq(anlagenteilId));
        bericht.ifPresent(b -> stammdatenService.loadAnlage(b.getAnlagenteil().getParentAnlageId()));
        return bericht;
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public Stream<EURegAnlage> findAllAnlagenberichteForBstBericht(EURegBetriebsstaette euregBst) {
        return euregBst.getBetriebsstaette().getAnlagen().stream().flatMap(
                anlage -> Stream.concat(findAnlagenBerichtByAnlageId(anlage.getId()).stream(),
                        anlage.getAnlagenteile()
                              .stream()
                              .map(Anlagenteil::getId)
                              .map(this::findAnlagenBerichtByAnlagenteilId)
                              .flatMap(Optional::stream)
                ));
    }

    @Secured({ BEHOERDE_READ, LAND_READ })
    @Transactional(readOnly = true)
    public EURegAnlage loadAnlagenteilBericht(Long betriebsstaetteId, String anlagenNummer, String anlagenteilNummer) {
        var anlage = stammdatenService.loadAnlageByAnlageNummer(betriebsstaetteId, anlagenNummer);
        return euRegAnlageRepository.findOne(
                                            new BooleanBuilder(eURegAnlage.anlagenteil.parentAnlageId.eq(anlage.getId())).and(
                                                    eURegAnlage.anlagenteil.anlagenteilNr.equalsIgnoreCase(anlagenteilNummer)))
                                    .orElseThrow(() -> new BubeEntityNotFoundException(EURegAnlage.class));
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void uebernimmEuRegDaten(UebernimmEuRegDatenInJahrCommand command) {
        List<Long> betriebsstaettenIds = euRegBetriebsstaetteRepository.findAllBstIds(
                stammdatenBerechtigungsContext.createPredicateWrite(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.id.in(command.getBerichtsIds())));
        if (betriebsstaettenIds.size() != command.getBerichtsIds().size()) {
            throw new AccessDeniedException("Keine Berechtigung");
        }

        // Publish ein Event, welches den Job aufruft
        eventPublisher.publishEvent(
                new EuRegDatenUebernehmenTriggeredEvent(command.getZieljahr(), betriebsstaettenIds));
    }

    @Secured(BEHOERDENBENUTZER)
    public void updateKomplexpruefung(EURegBetriebsstaette euRegBetriebsstaette, boolean pruefungsfehler) {
        euRegBetriebsstaette.setPruefungsfehler(pruefungsfehler);
        euRegBetriebsstaette.setPruefungVorhanden(true);
        euRegBetriebsstaetteRepository.save(euRegBetriebsstaette);
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public Stream<EURegBetriebsstaette> streamAllByBetriebsstaetteIdIn(List<Long> ids) {
        if (!stammdatenService.canReadAllBetriebsstaette(ids)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
        return euRegBetriebsstaetteRepository.streamAllByBetriebsstaetteIdIn(ids);
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public Stream<EURegBetriebsstaette> streamAllByBerichtIdIn(List<Long> ids) {
        checkDatenberechtigungForBerichtIds(ids);
        return euRegBetriebsstaetteRepository.streamAllByIdIn(ids);
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public boolean haveSameLand(List<Long> berichtIds) {
        return euRegBetriebsstaetteRepository.countDistinctByLandAndIdIn(berichtIds) == 1;
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public boolean haveSameJahr(List<Long> berichtIds) {
        return euRegBetriebsstaetteRepository.countDistinctByJahrAndIdIn(berichtIds) == 1;
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public List<Long> getBetriebsstaetteIdsForBerichtIds(List<Long> berichtsIds) {
        var result = euRegBetriebsstaetteRepository.findAllBstIds(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.id.in(berichtsIds)));
        if (result.size() != berichtsIds.size()) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
        return result;
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public void checkDatenberechtigungForBerichtIds(Collection<Long> berichtIds) {
        checkDatenberechtigungForBerichtIds(berichtIds.size(), eURegBetriebsstaette.id.in(berichtIds));
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public void checkDatenberechtigungForBerichtIds(Collection<Long> berichtIds, Land land, Referenz jahr) {
        checkDatenberechtigungForBerichtIds(berichtIds.size(),
                eURegBetriebsstaette.id.in(berichtIds)
                                       .and(eURegBetriebsstaette.betriebsstaette.berichtsjahr.eq(jahr))
                                       .and(eURegBetriebsstaette.betriebsstaette.land.eq(land)));
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional(readOnly = true)
    public void checkDatenberechtigungForBerichtIds(int expectedSize, BooleanExpression predicate) {
        long repositoryCount = euRegBetriebsstaetteRepository.count(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(predicate));
        if (repositoryCount != expectedSize) {
            LOGGER.warn(format("Expected %d results, but got %d", expectedSize, repositoryCount));
            throw new AccessDeniedException(
                    "Auf ein oder mehrere ausgewählte Berichte konnte nicht zugegriffen werden");
        }
    }

    private void checkRead(Long betriebsstaetteId) {
        if (!stammdatenService.canReadBetriebsstaette(betriebsstaetteId)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
    }

    private void checkWrite(Long betriebsstaetteId) {
        if (!stammdatenService.canWriteBetriebsstaette(betriebsstaetteId)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
    }

    private void checkWriteAnlage(Long anlageId) {
        if (!stammdatenService.canWriteAnlage(anlageId)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
    }

    @Transactional
    @Secured({ BEHOERDE_READ, LAND_READ })
    public Stream<Referenz> getAllYearsForBetriebsstaetteId(Long id) {
        EURegBetriebsstaette bericht = this.loadBetriebsstaettenBericht(id);

        Stream<Referenz> referenzStream = this.euRegBetriebsstaetteRepository.findAll(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.betriebsstaette.betriebsstaetteNr.equalsIgnoreCase(
                                                      bericht.getBetriebsstaette().getBetriebsstaetteNr()))
        ).stream().map(euRegBetriebsstaette -> euRegBetriebsstaette.getBetriebsstaette().getBerichtsjahr());
        if (bericht.getLocalId() == null) {
            return referenzStream;
        } else {
            Stream<Referenz> berichtsjahrePerLocalId = this.euRegBetriebsstaetteRepository.findAll(
                    stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                                  .and(eURegBetriebsstaette.betriebsstaette.localId.eq(
                                                          bericht.getBetriebsstaette().getLocalId()))
            ).stream().map(euRegBetriebsstaette -> euRegBetriebsstaette.getBetriebsstaette().getBerichtsjahr());
            return Stream.concat(referenzStream, berichtsjahrePerLocalId).distinct();
        }
    }

}
