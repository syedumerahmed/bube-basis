package de.wps.bube.basis.stammdatenbetreiber.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;

public interface SpbAnlageRepository
        extends JpaRepository<SpbAnlage, Long> {

    Optional<SpbAnlage> findOneByAnlageId(Long anlageId);

    Optional<SpbAnlage> findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(Long parentBetriebsstaetteId,
            String anlageNr);

    List<SpbAnlage> findByParentBetriebsstaetteId(Long betriebsstaetteId);

    List<SpbAnlage> findByParentBetriebsstaetteIdAndAnlageIdIsNull(Long betriebsstaetteId);

    boolean existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(Long parentBetriebsstaetteId, String anlageNr);

    boolean existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCaseAndIdNot(Long parentBetriebsstaetteId, String anlageNr,
            Long exceptedSpbAnlageId);

}
