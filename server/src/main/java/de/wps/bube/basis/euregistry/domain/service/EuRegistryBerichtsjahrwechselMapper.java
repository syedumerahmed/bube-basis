package de.wps.bube.basis.euregistry.domain.service;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.control.DeepClone;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.Genehmigung;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverContext;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Mapper(mappingControl = DeepClone.class)
public abstract class EuRegistryBerichtsjahrwechselMapper {

    @Autowired
    private ReferenzResolverService referenzResolverService;

    public EntityWithError<Berichtsdaten> copyEuRegBerichtsdaten(Berichtsdaten origin, Referenz jahr,
            Betriebsstaette bst) {
        var errors = new ArrayList<MappingError>();

        var berichtsdaten = copyEuRegBerichtsdaten(origin,
                new MappingContext(bst.getLand(),
                        Gueltigkeitsjahr.of(jahr.getSchluessel()), bst.getName(), bst.getBetriebsstaetteNr(),
                        errors::add));
        return new EntityWithError<>(berichtsdaten, errors);
    }

    abstract Berichtsdaten copyEuRegBerichtsdaten(Berichtsdaten origin, @Context MappingContext context);

    public List<MappingError> uebernimmBerichtsdaten(EURegAnlage origin, EURegAnlage destination,
            Referenz jahr, Betriebsstaette bst) {
        var errors = new ArrayList<MappingError>();

        uebernimmBerichtsdaten(origin, destination,
                new MappingContext(bst.getLand(),
                        Gueltigkeitsjahr.of(jahr.getSchluessel()), bst.getName(), bst.getBetriebsstaetteNr(),
                        errors::add));

        return errors;
    }

    @Mapping(target = "berichtsart", ignore = true)
    @Mapping(target = "anlage", ignore = true)
    @Mapping(target = "anlagenteil", ignore = true)
    @Mapping(target = "id", ignore = true)
    abstract void uebernimmBerichtsdaten(EURegAnlage origin, @MappingTarget EURegAnlage destination,
            @Context MappingContext context);

    abstract Genehmigung copyToBerichtsjahr(Genehmigung genehmigung, @Context MappingContext context);

    abstract FeuerungsanlageBerichtsdaten copyToBerichtsjahr(FeuerungsanlageBerichtsdaten berichtsdaten,
            @Context MappingContext context);

    Referenz copyToBerichtsjahr(Referenz referenz, @Context MappingContext context) {
        if (referenz == null) {
            return null;
        }
        return referenzResolverService.loadReferenz(referenz.getReferenzliste(), referenz.getSchluessel(), context);
    }

    Behoerde copyToBerichtsjahr(Behoerde behoerde, @Context MappingContext context) {
        if (behoerde == null) {
            return null;
        }
        if (behoerde.gueltigFuer(context.jahr)) {
            return behoerde;
        }
        return referenzResolverService.loadBehoerde(behoerde.getSchluessel(), context);
    }

    static class MappingContext implements ReferenzResolverContext {
        private final Land land;
        private final Gueltigkeitsjahr jahr;
        private final String bstName, bstNummer;
        private final Consumer<MappingError> errorConsumer;

        MappingContext(Land land, Gueltigkeitsjahr jahr,
                String bstName, String bstNummer, Consumer<MappingError> errorConsumer) {
            this.land = land;
            this.jahr = jahr;
            this.bstName = bstName;
            this.bstNummer = bstNummer;
            this.errorConsumer = errorConsumer;
        }

        @Override
        public Land getLand() {
            return land;
        }

        @Override
        public Gueltigkeitsjahr getJahr() {
            return jahr;
        }

        @Override
        public void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s wurde nicht gefunden", referenzliste.name(), schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s ist ungültig", referenzliste.name(), schluessel), true);
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeNotFound(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s wurde nicht gefunden", schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeUngueltig(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s ist ungültig", schluessel), true);
            errorConsumer.accept(error);
        }
    }
}
