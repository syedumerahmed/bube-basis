package de.wps.bube.basis.euregistry.domain.vo;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegAnlage.eURegAnlage;

import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum EuRegAnlageColumn implements Column {

    BERICHTSART(emptyIfNull(eURegAnlage.berichtsdaten.berichtsart.ktext)),
    ANLAGEN_NUMMER(emptyIfNull(eURegAnlage.anlage.anlageNr)),
    NAME(emptyIfNull(eURegAnlage.anlage.name)),
    BETRIEBSSTATUS(emptyIfNull(eURegAnlage.anlage.betriebsstatus.ktext));

    private static StringExpression emptyIfNull(StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final StringExpression expression;

    EuRegAnlageColumn(StringExpression expression) {
        this.expression = expression;
    }

    @Override
    public StringExpression getExpression() {
        return expression;
    }
}
