package de.wps.bube.basis.base.web;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Validated
@ConfigurationProperties("webapp")
public class WebappProperties {
    private @Valid @NotNull KeycloakProperties keycloak;

    public KeycloakProperties getKeycloak() {
        return keycloak;
    }

    public void setKeycloak(KeycloakProperties keycloak) {
        this.keycloak = keycloak;
    }

    public static class KeycloakProperties {
        private @Valid @NotNull ConfigProperties config;

        public ConfigProperties getConfig() {
            return config;
        }

        public void setConfig(ConfigProperties config) {
            this.config = config;
        }

        public static class ConfigProperties {

            private @NotBlank String url;
            private @NotBlank String realm;
            private @NotBlank String clientId;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getRealm() {
                return realm;
            }

            public void setRealm(String realm) {
                this.realm = realm;
            }

            public String getClientId() {
                return clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }
        }
    }
}
