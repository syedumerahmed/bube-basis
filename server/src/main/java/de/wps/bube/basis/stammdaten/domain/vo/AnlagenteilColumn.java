package de.wps.bube.basis.stammdaten.domain.vo;

import static de.wps.bube.basis.stammdaten.domain.entity.QAnlagenteil.anlagenteil;

import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum AnlagenteilColumn implements Column {

    ANLAGENTEIL_NUMMER(emptyIfNull(anlagenteil.anlagenteilNr)),
    NAME(emptyIfNull(anlagenteil.name)),
    BETRIEBSSTATUS(emptyIfNull(anlagenteil.betriebsstatus.ktext)),
    AENDERUNG_BETREIBER(emptyIfNull(anlagenteil.letzteAenderungBetreiber.stringValue()));

    private static StringExpression emptyIfNull(StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final StringExpression expression;

    AnlagenteilColumn(StringExpression expression) {
        this.expression = expression;
    }

    @Override
    public StringExpression getExpression() {
        return expression;
    }
}
