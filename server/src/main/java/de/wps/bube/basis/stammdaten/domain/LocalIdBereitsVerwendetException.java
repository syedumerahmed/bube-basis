package de.wps.bube.basis.stammdaten.domain;

import de.wps.bube.basis.base.persistence.DuplicateKeyException;

public class LocalIdBereitsVerwendetException extends DuplicateKeyException {
    public LocalIdBereitsVerwendetException(final String localId) {
        super("Local-ID " + localId + " wird bereits verwendet");
    }
}
