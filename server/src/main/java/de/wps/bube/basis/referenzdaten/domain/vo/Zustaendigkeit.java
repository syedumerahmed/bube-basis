package de.wps.bube.basis.referenzdaten.domain.vo;

public enum Zustaendigkeit {

    PRTR, UEBERWACHUNG, GENEHMIGUNG, LAND, KREIS, BIMSCHV11, BIMSCHV42

}
