package de.wps.bube.basis.referenzdaten.domain.entity;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Embeddable
public class Kommunikationsverbindung {

    public static final String EMAIL_SCHLUESSEL = "01";
    public static final String FAX_SCHLUESSEL = "05";
    public static final String TELEFON_SCHLUESSEL = "02";

    @NotNull
    @ManyToOne
    private Referenz typ;
    @NotEmpty
    @Length(max = 500)
    private String verbindung;

    @Deprecated
    protected Kommunikationsverbindung() {
    }

    public Kommunikationsverbindung(Referenz typ, String verbindung) {
        this.typ = typ;
        this.verbindung = verbindung;
    }

    public Referenz getTyp() {
        return typ;
    }

    public String getVerbindung() {
        return verbindung;
    }

    public void setTyp(Referenz r) {
        this.typ = r;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Kommunikationsverbindung that = (Kommunikationsverbindung) o;
        return typ.equals(that.typ) && Objects.equals(verbindung, that.verbindung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typ, verbindung);
    }

    public boolean isEmail() {
        return typ.getSchluessel().equals(EMAIL_SCHLUESSEL);
    }

    public boolean isFax() {
        return typ.getSchluessel().equals(FAX_SCHLUESSEL);
    }

    public boolean isTelefon() {
        return typ.getSchluessel().equals(TELEFON_SCHLUESSEL);
    }
}
