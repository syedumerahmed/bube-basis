package de.wps.bube.basis.referenzdaten.rules.bean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public class RegelApi {

    enum ChildIterationType {
        ALL, DOCUMENTED_ONLY
    }

    public static class Null {

    }

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Type {
        String name() default "";

        String description() default "";

        ChildIterationType fieldIterationType() default ChildIterationType.ALL;

        ChildIterationType methodIterationType() default ChildIterationType.DOCUMENTED_ONLY;
    }

    @Target({ ElementType.FIELD, ElementType.METHOD })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Member {
        String description() default "";

        Class<?> type() default Null.class;

        boolean exclude() default false;

        Referenzliste[] referenzliste() default {};
    }

}
