package de.wps.bube.basis.stammdaten.domain.event;

public class BetriebsstaetteGeloeschtEvent {
    public static final String EVENT_NAME = "Betriebsstätte gelöscht";

    public final Long betriebsstaetteId;

    public BetriebsstaetteGeloeschtEvent(Long betriebsstaetteId) {
        this.betriebsstaetteId = betriebsstaetteId;
    }
}
