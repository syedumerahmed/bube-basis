package de.wps.bube.basis.euregistry.web.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.base.web.openapi.model.BerichtsdatenDto;

@Mapper(uses = { ReferenzMapper.class, BehoerdenMapper.class })
public abstract class BerichtsdatenMapper {

    public abstract BerichtsdatenDto toDto(Berichtsdaten berichtsdaten);

    @Mapping(target = "thematicId", ignore = true)
    @Mapping(target = "schema", ignore = true)
    public abstract Berichtsdaten fromDto(BerichtsdatenDto berichtsdatenDto);

}
