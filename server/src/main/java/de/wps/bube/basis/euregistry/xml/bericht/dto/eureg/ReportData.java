package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ReportData {

    @XmlElement(required = true, name = "reportingYear")
    String reportingYear;

    @XmlElement(required = true, name = "countryId")
    ReferenceType countryId;

    public String getReportingYear() {
        return reportingYear;
    }

    public void setReportingYear(String reportingYear) {
        this.reportingYear = reportingYear;
    }

    public ReferenceType getCountryId() {
        return countryId;
    }

    public void setCountryId(ReferenceType countryId) {
        this.countryId = countryId;
    }

    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    public String getGmlId() {
        return countryId.getCode() + ".RD." + reportingYear;
    }
}
