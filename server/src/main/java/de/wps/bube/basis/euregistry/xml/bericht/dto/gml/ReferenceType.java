package de.wps.bube.basis.euregistry.xml.bericht.dto.gml;

import static java.util.Objects.requireNonNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;

@XmlAccessorType(XmlAccessType.NONE)
@XmlType(name = "ReferenceType")
public class ReferenceType {

    public static class MissingCodeException extends RuntimeException {
        public MissingCodeException(String message) {
            super(message);
        }
    }

    private final String schema;
    private String code;

    public ReferenceType(String schema, String code) {
        this.schema = requireNonNull(schema);
        if (code == null) {
            throw new MissingCodeException("Fehlender Wert zum Schema " + schema);
        }
        this.code = code;
    }

    @XmlAttribute(name = AttributeNames.HREF, namespace = SchemaSet.SCHEMA_XLINK)
    public String getHref() {
        return schema + code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = requireNonNull(code);
    }

    public String getSchema() {
        return schema;
    }
}
