package de.wps.bube.basis.stammdatenuebernahme.domain.vo;

public class QuelleUebernahme {
    private Long id;
    private Long spbId;
    private Boolean name;
    private Boolean quelleNr;
    private Boolean geoPunkt;
    private Boolean flaeche;
    private Boolean laenge;
    private Boolean breite;
    private Boolean durchmesser;
    private Boolean bauhoehe;

    public QuelleUebernahme() {
    }

    public QuelleUebernahme(Long id, Long spbId, Boolean name, Boolean quelleNr, Boolean geoPunkt,
            Boolean flaeche, Boolean laenge, Boolean breite, Boolean durchmesser, Boolean bauhoehe) {
        this.id = id;
        this.spbId = spbId;
        this.name = name;
        this.quelleNr = quelleNr;
        this.geoPunkt = geoPunkt;
        this.flaeche = flaeche;
        this.laenge = laenge;
        this.breite = breite;
        this.durchmesser = durchmesser;
        this.bauhoehe = bauhoehe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public Boolean getName() {
        return name;
    }

    public void setName(Boolean name) {
        this.name = name;
    }

    public Boolean getQuelleNr() {
        return quelleNr;
    }

    public void setQuelleNr(Boolean quelleNr) {
        this.quelleNr = quelleNr;
    }

    public Boolean getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(Boolean geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public Boolean getFlaeche() {
        return flaeche;
    }

    public void setFlaeche(Boolean flaeche) {
        this.flaeche = flaeche;
    }

    public Boolean getLaenge() {
        return laenge;
    }

    public void setLaenge(Boolean laenge) {
        this.laenge = laenge;
    }

    public Boolean getBreite() {
        return breite;
    }

    public void setBreite(Boolean breite) {
        this.breite = breite;
    }

    public Boolean getDurchmesser() {
        return durchmesser;
    }

    public void setDurchmesser(Boolean durchmesser) {
        this.durchmesser = durchmesser;
    }

    public Boolean getBauhoehe() {
        return bauhoehe;
    }

    public void setBauhoehe(Boolean bauhoehe) {
        this.bauhoehe = bauhoehe;
    }
}
