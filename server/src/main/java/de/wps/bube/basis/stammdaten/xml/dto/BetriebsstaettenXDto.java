package de.wps.bube.basis.stammdaten.xml.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

@XmlRootElement(name = BetriebsstaettenXDto.XML_ROOT_ELEMENT_NAME)
public class BetriebsstaettenXDto {

    public static final String XML_ROOT_ELEMENT_NAME = "betriebsstaetten";
    public static final String XSD_SCHEMA_URL = "classpath:xsd/XSTAMM_1.30.xsd";
    public static final String XSD_NAMESPACE = "http://basis.bube.wps.de/stammdaten";
    public static final QName QUALIFIED_NAME = new QName(XSD_NAMESPACE, XML_ROOT_ELEMENT_NAME);

    public List<BetriebsstaetteType> betriebsstaette;

}
