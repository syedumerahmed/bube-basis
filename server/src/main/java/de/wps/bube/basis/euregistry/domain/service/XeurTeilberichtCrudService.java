package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtException;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Service
public class XeurTeilberichtCrudService {

    private final XeurTeilberichtRepository xeurTeilberichtRepository;
    private final AktiverBenutzer aktiverBenutzer;

    public XeurTeilberichtCrudService(XeurTeilberichtRepository xeurTeilberichtRepository,
            AktiverBenutzer aktiverBenutzer) {
        this.xeurTeilberichtRepository = xeurTeilberichtRepository;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Secured({ LAND_READ })
    @Transactional(readOnly = true)
    public List<XeurTeilbericht> getAllTeilberichte(Referenz jahr) {
        return getAllTeilberichte(jahr, aktiverBenutzer.getLand());
    }

    @Secured({ LAND_READ })
    @Transactional(readOnly = true)
    public List<XeurTeilbericht> getAllTeilberichte(Referenz jahr, Land land) {
        if (land.equals(Land.DEUTSCHLAND)) {
            return this.xeurTeilberichtRepository.findAllByJahr(jahr).toList();
        }
        return this.xeurTeilberichtRepository.findAllByLandAndJahr(land, jahr).toList();
    }

    @Secured({ LAND_READ })
    @Transactional(readOnly = true)
    public Optional<XeurTeilbericht> findLatestFreigegeben(Long jahrId, String land, String localId) {
        return this.xeurTeilberichtRepository.findLatestFreigegeben(jahrId, land, localId);
    }

    @Secured({ LAND_READ })
    @Transactional(readOnly = true)
    public void downloadTeilbericht(ByteArrayOutputStream outputStream, Long xeurTeilberichtId) {

        XeurTeilbericht xeurTeilbericht = xeurTeilberichtRepository.getById(xeurTeilberichtId);
        datenberechtigungPruefen(xeurTeilbericht);

        ZipOutputStream zipOut = new ZipOutputStream(outputStream);

        try {
            byte[] teilberichtInhaltXml = xeurTeilbericht.getXeurFile().getBytes();
            ZipEntry teilberichtInhaltEntry = new ZipEntry(xeurTeilbericht.getFilename());
            zipOut.putNextEntry(teilberichtInhaltEntry);
            zipOut.write(teilberichtInhaltXml);
            zipOut.closeEntry();

            byte[] protokollTxt = xeurTeilbericht.getProtokoll().getBytes();
            ZipEntry protokollEntry = new ZipEntry(xeurTeilbericht.getFilenameProtokoll());
            zipOut.putNextEntry(protokollEntry);
            zipOut.write(protokollTxt);
            zipOut.closeEntry();

            zipOut.close();
        } catch (IOException ioException) {
            throw new XeurTeilberichtException("Fehler beim Erzeugen der Teilbericht ZIP Datei", ioException);
        }
    }

    private void datenberechtigungPruefen(XeurTeilbericht teilbericht) {
        if (!aktiverBenutzer.isGesamtadmin() && !teilbericht.getLand().equals(aktiverBenutzer.getLand())) {
            throw new AccessDeniedException("Zugriff auf Teilbericht verweigert");
        }
    }

}
