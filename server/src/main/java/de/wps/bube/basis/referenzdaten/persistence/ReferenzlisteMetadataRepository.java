package de.wps.bube.basis.referenzdaten.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzlisteMetadata;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public interface ReferenzlisteMetadataRepository extends JpaRepository<ReferenzlisteMetadata, Referenzliste> {

}
