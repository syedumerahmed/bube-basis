package de.wps.bube.basis.desktop.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.entity.DesktopItemId;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;

public interface DesktopRepository extends JpaRepository<DesktopItem, DesktopItemId> {

    //DesktopItemId steht vor den Attributen des Schlüssels
    List<DesktopItem> findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp(String username,
            String jahrSchluessel, DesktopTyp desktopTyp);

    //DesktopItemId steht vor den Attributen des Schlüssels
    void deleteAllByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp(String username,
            String jahrSchluessel, DesktopTyp desktopTyp);
}
