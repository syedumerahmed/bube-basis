package de.wps.bube.basis.referenzdaten.xml.referenz.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

@XmlRootElement(name = ReferenzlisteXDto.XML_ROOT_ELEMENT_NAME, namespace = ReferenzlisteXDto.XSD_NAMESPACE)
public class ReferenzlisteXDto {

    public static final String XML_ROOT_ELEMENT_NAME = "referenzliste";
    public static final String XSD_SCHEMA_URL = "classpath:xsd/XREF_1.29.xsd";
    public static final String XSD_NAMESPACE = "http://basis.bube.wps.de/referenz";
    public static final QName QUALIFIED_NAME = new QName(XSD_NAMESPACE, XML_ROOT_ELEMENT_NAME);

    @XmlElement
    public List<ReferenzType> referenz;

}
