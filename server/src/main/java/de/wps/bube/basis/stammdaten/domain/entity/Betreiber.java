package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.AssociationOverride;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Betreiber implements Cloneable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "betreiber_seq")
    @SequenceGenerator(name = "betreiber_seq", allocationSize = 50)
    private Long id;

    private String betreiberNummer;

    @NotNull
    @ManyToOne
    private Referenz berichtsjahr;

    @NotNull
    private Land land;

    private String referenzAnlagenkataster;

    @Embedded
    @AssociationOverride(name = "vertraulichkeitsgrund", joinColumns = @JoinColumn(name = "adresse_vertraulichkeitsgrund_id"))
    private Adresse adresse;
    @NotNull
    private String name;

    @ManyToOne
    private Referenz vertraulichkeitsgrund;
    private String url;
    private String bemerkung;
    private boolean betreiberdatenUebernommen;

    @CreatedDate
    private Instant ersteErfassung;
    @LastModifiedDate
    private Instant letzteAenderung;

    private Instant letzteAenderungBetreiber;

    @Deprecated
    protected Betreiber() {
    }

    public Betreiber(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBetreiberNummer() {
        return betreiberNummer;
    }

    public void setBetreiberNummer(String betreiberNummer) {
        this.betreiberNummer = betreiberNummer;
    }

    public Referenz getBerichtsjahr() {
        return berichtsjahr;
    }

    public void setBerichtsjahr(Referenz berichtsjahr) {
        this.berichtsjahr = berichtsjahr;
    }

    public String getReferenzAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    public void setReferenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Referenz getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    public void setVertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public void setBetreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public Instant getLetzteAenderungBetreiber() {
        return letzteAenderungBetreiber;
    }

    public void setLetzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Betreiber betreiber = (Betreiber) o;

        return Objects.equals(id, betreiber.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isNew() {
        return id == null;
    }

    public void copyToZieljahr(Referenz zieljahr) {
        berichtsjahr = zieljahr;
    }

    @Override
    public Betreiber clone() {
        try {
            var clone = (Betreiber) super.clone();
            clone.id = null;
            return clone;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
