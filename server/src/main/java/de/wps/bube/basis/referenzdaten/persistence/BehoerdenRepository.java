package de.wps.bube.basis.referenzdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.persistence.QBeanWithPropertyCheck;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.BehoerdeListItem;

public interface BehoerdenRepository
        extends JpaRepository<Behoerde, Long>, QuerydslPredicateProjectionRepository<Behoerde> {

    @Query(value = "select exists(select 1 from behoerde where (:id is null or id != CAST(CAST(:id AS TEXT) AS BIGINT))" +
            "                and land = :land" +
            "                and schluessel = :schluessel" +
            "                and (gueltig_von between :von and :bis" +
            "                   or :von between gueltig_von and gueltig_bis))", nativeQuery = true)
    boolean functionalKeyDuplicate(Long id, String land, String schluessel, int von, int bis);

    default List<BehoerdeListItem> findAllAsListItems(Predicate predicate) {
        return findAll(predicate, QBeanWithPropertyCheck.fields(BehoerdeListItem.class,
                behoerde.id,
                behoerde.land,
                behoerde.schluessel,
                behoerde.bezeichnung,
                behoerde.kreis,
                behoerde.gueltigVon,
                behoerde.gueltigBis));
    }

    Optional<Behoerde> findFirstByLandAndSchluesselOrderByGueltigBisDesc(Land land, String schluessel);

}
