package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class StricterPermitConditionsType {

    @XmlElement(name = "StricterPermitConditionsType")
    private final StricterPermitConditionTypeType strictType = new StricterPermitConditionTypeType();

    public Boolean getArticle18() {
        return strictType.article18;
    }

    public void setArticle18(Boolean article18) {
        strictType.article18 = article18;
    }

    public Boolean getArticle14_4() {
        return strictType.article14_4;
    }

    public void setArticle14_4(Boolean article14_4) {
        strictType.article14_4 = article14_4;
    }

    public String getBatael() {
        return strictType.batael == null ? null : strictType.batael.getCode();
    }

    public void setBatael(String bataelCode) {
        if (bataelCode == null || bataelCode.trim().isEmpty()) {
            strictType.batael = null;
        } else if (strictType.batael == null) {
            strictType.batael = new ReferenceType(SchemaSet.BATAEL_CODES, bataelCode);
        } else {
            strictType.batael.setCode(bataelCode);
        }
    }

    public Boolean getStricterPermitConditionsIndicator() {
        return strictType.stricterPermitConditionsIndicator;
    }

    public void setStricterPermitConditionsIndicator(Boolean stricterPermitConditionsIndicator) {
        strictType.stricterPermitConditionsIndicator = stricterPermitConditionsIndicator;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class StricterPermitConditionTypeType {
        @XmlElement(required = true, name = "article18", namespace = SchemaSet.SCHEMA_EUREG)
        private Boolean article18;
        @XmlElement(required = true, name = "article14.4", namespace = SchemaSet.SCHEMA_EUREG)
        private Boolean article14_4;
        @XmlElement(required = false, name = "BATAEL", namespace = SchemaSet.SCHEMA_EUREG)
        private ReferenceType batael;
        @XmlElement(required = true,
                name = "stricterPermitConditionsIndicator",
                namespace = SchemaSet.SCHEMA_EUREG)
        private Boolean stricterPermitConditionsIndicator;
    }
}
