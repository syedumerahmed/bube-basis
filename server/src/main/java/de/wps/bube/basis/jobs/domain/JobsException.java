package de.wps.bube.basis.jobs.domain;

public class JobsException extends RuntimeException {
    public JobsException(final String message) {
        super(message);
    }
}
