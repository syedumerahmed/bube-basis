package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;
import java.util.function.Function;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.referenzdaten.domain.entity.QReferenz;
import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public class ReferenzFilter implements Filter {

    private final Function<QBetriebsstaette, QReferenz> relativePath;
    private final Function<Suchattribute, List<Long>> attributExtractor;

    public ReferenzFilter(Function<QBetriebsstaette, QReferenz> relativePath,
            Function<Suchattribute, List<Long>> attributExtractor) {
        this.relativePath = relativePath;
        this.attributExtractor = attributExtractor;
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        var referenzIds = attributExtractor.apply(suchattribute);
        return referenzIds != null ? relativePath.apply(bstRoot).id.in(referenzIds) : null;
    }

}
