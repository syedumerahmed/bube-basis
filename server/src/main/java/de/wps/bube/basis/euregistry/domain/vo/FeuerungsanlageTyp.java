package de.wps.bube.basis.euregistry.domain.vo;

import java.util.stream.Stream;

public enum FeuerungsanlageTyp {

    LCP("LCP"), WI("WI"), CO_WI("co-WI");

    // Bezeichner wird direkt für EU-EXPORT verwendet
    private final String bezeichner;

    FeuerungsanlageTyp(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static FeuerungsanlageTyp of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Feuerungsanlage-Typ: " + bezeichner));
    }
}
