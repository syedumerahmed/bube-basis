package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.euregistry.rules.bean.SpezielleBedingungIE51Bean;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class SpezielleBedingungIE51BeanMapper {

    public abstract SpezielleBedingungIE51Bean toBean(SpezielleBedingungIE51 spezielleBedingungIE51);

    protected int map(Gueltigkeitsjahr jahr) {
        return jahr.getJahr();
    }

}
