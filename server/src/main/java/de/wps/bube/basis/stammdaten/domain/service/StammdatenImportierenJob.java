package de.wps.bube.basis.stammdaten.domain.service;

import static java.lang.String.format;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapper;
import de.wps.bube.basis.stammdaten.xml.StammdatenImportException;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;

@Service
@Transactional
public class StammdatenImportierenJob implements StammdatenJob {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.STAMMDATEN_IMPORTIEREN;

    private static final Logger LOGGER = LoggerFactory.getLogger(StammdatenImportierenJob.class);

    private static final int COUNT_SIZE = 10;

    private final StammdatenImportExportService stammdatenImportExportService;
    private final BenutzerJobRegistry benutzerJobRegistry;
    private final AktiverBenutzer aktiverBenutzer;
    private final XMLService xmlService;
    private final DateiService dateiService;
    private final BetriebsstaetteXMLMapper betriebsstaetteXMLMapper;

    public StammdatenImportierenJob(StammdatenImportExportService stammdatenImportExportService,
            BenutzerJobRegistry benutzerJobRegistry, AktiverBenutzer aktiverBenutzer,
            XMLService xmlService, DateiService dateiService,
            BetriebsstaetteXMLMapper betriebsstaetteXMLMapper) {
        this.stammdatenImportExportService = stammdatenImportExportService;
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.aktiverBenutzer = aktiverBenutzer;
        this.xmlService = xmlService;
        this.dateiService = dateiService;
        this.betriebsstaetteXMLMapper = betriebsstaetteXMLMapper;
    }

    @Async
    public void runAsync(String berichtsjahr) {
        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(
                format("%s Start: %s Berichtsjahr: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), berichtsjahr,
                        aktiverBenutzer.getUsername()));

        try {
            AtomicInteger count = new AtomicInteger();
            AtomicInteger exceptionCount = new AtomicInteger();

            InputStream inputStream = dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.STAMMDATEN);
            xmlService.read(inputStream, BetriebsstaetteType.class, betriebsstaetteXMLMapper,
                    BetriebsstaettenXDto.XML_ROOT_ELEMENT_NAME, null, betriebsstaette -> {
                try {
                    stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, berichtsjahr,
                            jobProtokoll);
                    if (count.incrementAndGet() % COUNT_SIZE == 0) {
                        benutzerJobRegistry.jobCountChanged(COUNT_SIZE);
                    }
                } catch (StammdatenImportException e) {
                    jobProtokoll.addEintrag(e.getMessage());
                    exceptionCount.incrementAndGet();
                    benutzerJobRegistry.jobError(e);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    exceptionCount.incrementAndGet();
                    jobProtokoll.addEintrag(
                            format("Betriebsstätte %s Nr. %s. Fehler %s", betriebsstaette.entity.getName(),
                                    betriebsstaette.entity.getBetriebsstaetteNr(),
                                    e.getMessage()));
                    benutzerJobRegistry.jobError(e);
                }
            });

            if(exceptionCount.get() >0) {
                benutzerJobRegistry.jobErrorMultiple(
                        format("%s Betriebsstätten werden aufgrund der im Protokoll aufgeführten Fehler nicht importiert.", exceptionCount.get()));
            }

            benutzerJobRegistry.jobCountChanged(count.get() % COUNT_SIZE);
            benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Betriebsstätten: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(format("%s Fehler: %s", JOB_NAME, DateTime.nowCET()), e.getMessage());
        } finally {
            // Datei wird gelöscht, egal ob der Job durchlief oder nicht
            stammdatenImportExportService.dateiLoeschen();

            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }
}
