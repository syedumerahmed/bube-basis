package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface SuchattributeMapper {
    Suchattribute fromDto(de.wps.bube.basis.base.web.openapi.model.Suchattribute suchattribute);
}
