package de.wps.bube.basis.stammdaten.xml;

import de.wps.bube.basis.base.xml.ImportException;

public class StammdatenImportException extends ImportException {

    public StammdatenImportException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public StammdatenImportException(String betriebsstaetteNr, String name) {
        super("Betriebsstätte %s Nr. %s wird aufgrund der aufgelisteten Fehler nicht importiert".formatted(
                name, betriebsstaetteNr));
    }

    public StammdatenImportException(String message) {
        super(message);
    }
}
