package de.wps.bube.basis.euregistry.rules.bean;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class TeilberichtBean implements PruefObjekt {

    private final Supplier<TeilberichtBean> vorgaenger;
    private final Supplier<TeilberichtBean> nachfolger;
    private final Supplier<List<EURegBetriebsstaetteBean>> berichte;

    public TeilberichtBean(Supplier<TeilberichtBean> vorgaenger, Supplier<TeilberichtBean> nachfolger,
            Supplier<List<EURegBetriebsstaetteBean>> berichte) {
        this.vorgaenger = Suppliers.memoize(vorgaenger::get);
        this.nachfolger = Suppliers.memoize(nachfolger::get);
        this.berichte = Suppliers.memoize(berichte::get);
    }

    @Override
    @RegelApi.Member(exclude = true)
    public Long getId() {
        throw new UnsupportedOperationException();
    }

    public List<EURegBetriebsstaetteBean> getBerichte() {
        return berichte.get();
    }

    @RegelApi.Member(description = "Teilbericht des jeweiligen Vorjahres; muss in Land übereinstimmen.")
    public TeilberichtBean getVorgaenger() {
        return vorgaenger.get();
    }

    @RegelApi.Member(description = "Teilbericht des jeweiligen Folgejahres; muss in Land übereinstimmen.")
    public TeilberichtBean getNachfolger() {
        return nachfolger.get();
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.TEILBERICHT;
    }

}
