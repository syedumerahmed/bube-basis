package de.wps.bube.basis.euregistry.xml;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import de.wps.bube.basis.euregistry.xml.dto.*;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.euregistry.domain.entity.Auflage;
import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverContext;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.xml.ReferenzXMLMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Mapper(uses = { CommonXMLMapper.class, ReferenzXMLMapper.class, ReferenzResolverService.class,
        StammdatenService.class })
public abstract class EuRegBetriebsstaetteImportDtoMapper
        implements XDtoMapper<EuRegBst, EuRegBetriebsstaetteImportDto> {

    @Autowired
    EURegistryService euRegistryService;
    @Autowired
    StammdatenService stammdatenService;
    @Autowired
    BehoerdenService behoerdenService;

    @Override
    public EntityWithError<EuRegBetriebsstaetteImportDto> fromXDto(EuRegBst euRegBst) {
        var errors = new ArrayList<MappingError>();

        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteByJahrLandLocalId(euRegBst.getJahr(),
                Land.of(euRegBst.getLand()), euRegBst.getInspire().getLocalId());
        MappingContext context = new MappingContext(errors::add);
        context.setBetriebsstaette(betriebsstaette);
        return new EntityWithError<>(map(euRegBst, context), errors);
    }

    @Mapping(target = "anlagen", source = ".", qualifiedByName = "mapAnlagen")
    @Mapping(target = "euRegBetriebsstaette", source = ".", qualifiedByName = "mapBetriebsstaette")
    abstract EuRegBetriebsstaetteImportDto map(EuRegBst euRegBst, @Context MappingContext context);

    @Named("mapBetriebsstaette")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "betriebsstaette", source = ".", qualifiedByName = "getBetriebsstaette")
    @Mapping(target = "berichtsdaten", source = ".", qualifiedByName = "getBerichtsdaten")
    @Mapping(target = "pruefungsfehler", constant = "false")
    @Mapping(target = "pruefungVorhanden", constant = "false")
    @Mapping(target = "komplexpruefung", ignore = true)
    abstract EURegBetriebsstaette mapBetriebsstaette(EuRegBst xdto, @Context MappingContext context);

    @AfterMapping
    void afterBetriebsstaette(@MappingTarget EURegBetriebsstaette euRegBst) {
        euRegistryService.findBetriebsstaettenBerichtByBstId(euRegBst.getBetriebsstaette().getId())
                         .map(EURegBetriebsstaette::getId)
                         .ifPresent(euRegBst::setId);
    }

    @Named("getBetriebsstaette")
    Betriebsstaette getBetriebsstaette(EuRegBst bst, @Context MappingContext context) {
        return context.getBetriebsstaette();
    }

    @Named("getBerichtsdaten")
    @Mapping(target = "berichtsart", source = "berichtsart", qualifiedByName = "RRTYP")
    @Mapping(target = "bearbeitungsstatus", source = "bearbeitungsstatus", qualifiedByName = "RBEA")
    @Mapping(target = "thematicId", source = "inspire.thematicId")
    @Mapping(target = "schema", source = "inspire.schema")
    abstract Berichtsdaten getBerichtsdaten(EuRegBst bst, @Context MappingContext context);

    @Named("mapAnlagen")
    List<EURegAnlage> mapAnlagen(EuRegBst euRegBst, @Context MappingContext context) {
        List<EURegAnlage> anlagen = new ArrayList<>(
                euRegBst.getEuRegAnl().stream().map(euRegAnl -> this.mapAnl(euRegAnl, context)).toList());
        anlagen.addAll(euRegBst.getEuRegF().stream().map(euRegF -> this.mapAnlF(euRegF, context)).toList());
        return anlagen;
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "anlage", source = ".", qualifiedByName = "getAnlage")
    @Mapping(target = "anlagenteil", source = ".", qualifiedByName = "getAnlagenteil")
    @Mapping(target = "berichtsdaten", source = ".")
    @Mapping(target = "berichtsart", ignore = true)
    @Mapping(target = "report", source = "ausgangszustandsbericht")
    @Mapping(target = "eSpirsNr", source = "ESpirs")
    @Mapping(target = "tehgNr", source = "tehg")
    @Mapping(target = "feuerungsanlage", ignore = true)
    @Mapping(target = "monitor", source = "oeffentlichesMonitoring")
    @Mapping(target = "monitorUrl", source = "monitoringUrl")
    @Mapping(target = "bvtAusnahmen", source = "bvtAusnahme")
    @Mapping(target = "auflagen", source = "strengereGenehmigungsauflage")
    @Mapping(target = "kapitelIeRl", source = "kapitelIeRl", qualifiedByName = "RRELCH")
    @Mapping(target = "anwendbareBvt", source = "anwendbareBvt", qualifiedByName = "RBATC")
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    abstract EURegAnlage mapAnl(EuRegAnl euRegAnl, @Context MappingContext context);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "anlage", source = ".", qualifiedByName = "getAnlage")
    @Mapping(target = "anlagenteil", source = ".", qualifiedByName = "getAnlagenteil")
    @Mapping(target = "berichtsdaten", source = ".")
    @Mapping(target = "berichtsart", ignore = true)
    @Mapping(target = "report", source = "ausgangszustandsbericht")
    @Mapping(target = "eSpirsNr", source = "ESpirs")
    @Mapping(target = "tehgNr", source = "tehg")
    @Mapping(target = "monitor", source = "oeffentlichesMonitoring")
    @Mapping(target = "monitorUrl", source = "monitoringUrl")
    @Mapping(target = "bvtAusnahmen", source = "bvtAusnahme")
    @Mapping(target = "auflagen", source = "strengereGenehmigungsauflage")
    @Mapping(target = "kapitelIeRl", source = "kapitelIeRl", qualifiedByName = "RRELCH")
    @Mapping(target = "anwendbareBvt", source = "anwendbareBvt", qualifiedByName = "RBATC")
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    abstract EURegAnlage mapAnlF(EuRegF euRegF, @Context MappingContext context);

    @Mapping(target = "berichtsart", source = "berichtsart", qualifiedByName = "RRTYP")
    @Mapping(target = "bearbeitungsstatus", source = "bearbeitungsstatus", qualifiedByName = "RBEA")
    @Mapping(target = "thematicId", source = "inspire.thematicId")
    @Mapping(target = "schema", source = "inspire.schema")
    abstract Berichtsdaten mapBerichtsdaten(EuRegAnl euRegAnl, @Context MappingContext context);

    @Mapping(target = "berichtsart", source = "berichtsart", qualifiedByName = "RRTYP")
    @Mapping(target = "bearbeitungsstatus", source = "bearbeitungsstatus", qualifiedByName = "RBEA")
    @Mapping(target = "thematicId", source = "inspire.thematicId")
    @Mapping(target = "schema", source = "inspire.schema")
    abstract Berichtsdaten mapBerichtsdaten(EuRegF euRegF, @Context MappingContext context);

    @Mapping(target = "emissionswertAusnahme", source = "emissionswertAusnahme", qualifiedByName = "RBATE")
    abstract BVTAusnahme mapBvtAusnahme(BvtAusnahme bvtAusnahme, @Context MappingContext context);

    @Mapping(target = "emissionswert", source = "emissionswert", qualifiedByName = "RBATE")
    abstract Auflage strengereGenehmigungsauflage(StrengereGenehmigungsauflage strengereGenehmigungsauflage,
            @Context MappingContext context);

    @Mapping(target = "ieAusnahmen", source = "ausnahmeNachIeRl", qualifiedByName = "RDERO")
    @Mapping(target = "spezielleBedingungen", source = "BImSchV17.spezielleBedingung")
    @Mapping(target = ".", source = "BImSchV17")
    abstract FeuerungsanlageBerichtsdaten mapFeuerungsanlageBerichtsdaten(Feuerungsanlage feuerungsanlage,
            @Context MappingContext context);

    @Mapping(target = "art", source = "aenderungArt", qualifiedByName = "RSPECC")
    abstract SpezielleBedingungIE51 mapSpezielleBedingungIE51(SpezielleBedingung spezielleBedingung,
            @Context MappingContext context);

    @Named("getAnlage")
    Anlage getAnlage(EuRegAnl euRegAnl, @Context MappingContext context) {
        if (euRegAnl.getAnlagenNr() == null) {
            return null;
        }
        return stammdatenService.readAnlageByLocalId(context.getBetriebsstaette().getId(),
                euRegAnl.getInspire().getLocalId());
    }

    @Named("getAnlagenteil")
    Anlagenteil getAnlagenteil(EuRegAnl euRegAnl, @Context MappingContext context) {
        if (euRegAnl.getAnlagenteilNr() == null) {
            return null;
        }
        return stammdatenService.readAnlagenteilByLocalId(context.getBetriebsstaette().getId(),
                euRegAnl.getInspire().getLocalId());
    }

    @Override
    public EuRegBst toXDto(EuRegBetriebsstaetteImportDto entity) {
        throw new UnsupportedOperationException();
    }

    static class MappingContext implements ReferenzResolverContext {
        public Land land;
        public Gueltigkeitsjahr jahr;
        private String bstNummer;
        private Betriebsstaette betriebsstaette;
        private final Consumer<MappingError> errorConsumer;

        MappingContext(Consumer<MappingError> errorConsumer) {
            this.errorConsumer = errorConsumer;
        }

        @BeforeMapping
        void beforeFromDto(EuRegBst xdto) {
            land = Land.of(xdto.getLand());
            jahr = Gueltigkeitsjahr.of(xdto.getJahr());
            bstNummer = xdto.getBetriebsstaetteNr();
        }

        @Override
        public Land getLand() {
            return land;
        }

        @Override
        public Gueltigkeitsjahr getJahr() {
            return jahr;
        }

        @Override
        public void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("EuRegBetriebsstätte Nr. %s", bstNummer),
                    format("Referenz %s mit Schlüssel %s wurde nicht gefunden", referenzliste.name(), schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("EuRegBetriebsstätte Nr. %s", bstNummer),
                    format("Referenz %s mit Schlüssel %s ist ungültig", referenzliste.name(), schluessel), true);
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeNotFound(String schluessel) {
            var error = new MappingError(format("EuRegBetriebsstätte Nr. %s", bstNummer),
                    format("Behörde mit Schlüssel %s wurde nicht gefunden", schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeUngueltig(String schluessel) {
            var error = new MappingError(format("EuRegBetriebsstätte Nr. %s", bstNummer),
                    format("Behörde mit Schlüssel %s ist ungültig", schluessel), true);
            errorConsumer.accept(error);
        }

        public void setBetriebsstaette(Betriebsstaette betriebsstaette) {
            this.betriebsstaette = betriebsstaette;
        }

        public Betriebsstaette getBetriebsstaette() {
            return betriebsstaette;
        }
    }
}
