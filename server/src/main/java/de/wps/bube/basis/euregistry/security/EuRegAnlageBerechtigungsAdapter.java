package de.wps.bube.basis.euregistry.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

public class EuRegAnlageBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {

    private final EURegAnlage euRegAnl;
    private final Betriebsstaette bst;

    public EuRegAnlageBerechtigungsAdapter(Betriebsstaette bst, EURegAnlage euRegAnl) {
        this.euRegAnl = euRegAnl;
        this.bst = bst;
    }

    @Override
    public Land getLand() {
        return bst.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return Set.of(euRegAnl.getBerichtsdaten().getZustaendigeBehoerde().getSchluessel());
    }

    @Override
    public Set<String> getAkzSet() {
        if (euRegAnl.getBerichtsdaten().getAkz() != null) {
            return Set.of(euRegAnl.getBerichtsdaten().getAkz());
        }
        return null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return Set.of(bst.getGemeindekennziffer().getSchluessel());
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return Set.of(bst.getBetriebsstaetteNr());
    }


}
