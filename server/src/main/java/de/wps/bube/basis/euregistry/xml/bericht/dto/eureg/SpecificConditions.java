package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class SpecificConditions {
    @XmlElement(required = true, name = "SpecificConditionsType")
    private final SpecificConditionsType data = new SpecificConditionsType();

    public String getSpecificConditionsPermitURL() {
        return data.specificConditionsPermitURL;
    }

    public void setSpecificConditionsPermitURL(String specificConditionsPermitURL) {
        this.data.specificConditionsPermitURL = specificConditionsPermitURL;
    }

    public String getConditionsInformation() {
        return data.conditionsInformation;
    }

    public void setConditionsInformation(String conditionsInformation) {
        this.data.conditionsInformation = conditionsInformation;
    }

    public String getSpecificConditions() {
        return data.specificConditions == null ? null : data.specificConditions.getCode();
    }

    public void setSpecificConditions(String specificConditionsCode) {
        if (specificConditionsCode == null || specificConditionsCode.trim().isEmpty()) {
            data.specificConditions = null;
        } else if (data.specificConditions == null) {
            data.specificConditions = new ReferenceType(SchemaSet.ARTICLE51_VALUE_CODES, specificConditionsCode);
        } else {
            data.specificConditions.setCode(specificConditionsCode);
        }
    }

    private static class SpecificConditionsType {
        @XmlElement(required = false, name = "specificConditionsPermitURL")
        String specificConditionsPermitURL;
        @XmlElement(required = false, name = "conditionsInformation")
        String conditionsInformation;
        @XmlElement(required = false, name = "specificConditions")
        ReferenceType specificConditions;
    }
}
