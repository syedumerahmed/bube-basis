package de.wps.bube.basis.datei.domain.entity;

public enum DateiScope {
    BENUTZER(true), STAMMDATEN(true), REFERENZDATEN(true), BEHOERDE(true), EUREG(true);

    private final boolean temporary;

    DateiScope(boolean temporary) {
        this.temporary = temporary;
    }

    boolean isTemporary() {
        return temporary;
    }
}
