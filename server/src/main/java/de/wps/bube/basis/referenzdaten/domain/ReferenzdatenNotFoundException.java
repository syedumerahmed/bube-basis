package de.wps.bube.basis.referenzdaten.domain;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public class ReferenzdatenNotFoundException extends RuntimeException {

    public final Referenzliste referenzliste;
    public final String schluessel;
    public final Gueltigkeitsjahr jahr;
    public final Land land;

    public ReferenzdatenNotFoundException(Referenzliste referenzliste, String schluessel, Land land,
            Gueltigkeitsjahr jahr) {
        super();
        this.referenzliste = referenzliste;
        this.schluessel = schluessel;
        this.land = land;
        this.jahr = jahr;
    }

    @Override
    public String getMessage() {
        return String.format("Kein Referenzwert gefunden für %s Schlüssel=%s Land=%s Jahr=%d", referenzliste,
                schluessel, land, jahr.getJahr());
    }
}
