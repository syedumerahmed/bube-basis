package de.wps.bube.basis.base.persistence;

import com.querydsl.core.types.dsl.ComparableExpression;

public interface Column {
    ComparableExpression<?> getExpression();
}
