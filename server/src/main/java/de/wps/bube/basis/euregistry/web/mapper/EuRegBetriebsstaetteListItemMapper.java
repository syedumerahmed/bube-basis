package de.wps.bube.basis.euregistry.web.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteListItem;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;

@Mapper(uses = ReferenzMapper.class)
public interface EuRegBetriebsstaetteListItemMapper {
    @Mapping(target = "nummer", source = "betriebsstaette.betriebsstaetteNr")
    EuRegBetriebsstaetteListItemDto toDto(EuRegBetriebsstaetteListItem betriebsstaette);
}
