package de.wps.bube.basis.stammdatenbetreiber.domain.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;

@Mapper
public interface SpbBetriebsstaetteUpdater {

    @Mapping(target = "betriebsstaette", ignore = true)
    void update(@MappingTarget SpbBetriebsstaette target, SpbBetriebsstaette source);

}
