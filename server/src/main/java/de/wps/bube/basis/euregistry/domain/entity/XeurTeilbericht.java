package de.wps.bube.basis.euregistry.domain.entity;

import java.time.Instant;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.base.vo.Land;

@Entity(name = "EuregXeurTeilbericht")
@EntityListeners(AuditingEntityListener.class)
public class XeurTeilbericht {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eureg_xeur_teilbericht_seq")
    @SequenceGenerator(name = "eureg_xeur_teilbericht_seq", allocationSize = 50)
    private Long id;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    private Instant freigegebenAt;

    private boolean abgabefaehig;

    @ManyToOne
    private Referenz jahr;

    private Land land;

    @ElementCollection
    private List<String> localIds;

    private String xeurFile;

    private String protokoll;

    @Deprecated
    protected XeurTeilbericht() {
    }

    public XeurTeilbericht(Referenz jahr, Land land) {
        this.jahr = jahr;
        this.land = land;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public String getXeurFile() {
        return xeurFile;
    }

    public void setXeurFile(String xeurFile) {
        this.xeurFile = xeurFile;
    }

    public String getProtokoll() {
        return protokoll;
    }

    public void setProtokoll(String protokoll) {
        this.protokoll = protokoll;
    }

    public Long getId() {
        return id;
    }

    public Referenz getJahr() {
        return jahr;
    }

    public void setJahr(Referenz jahr) {
        this.jahr = jahr;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public Instant getFreigegebenAt() {
        return freigegebenAt;
    }

    public void setFreigegebenAt(Instant freigegebenAt) {
        this.freigegebenAt = freigegebenAt;
    }

    public boolean isFreigegeben() {
        return this.freigegebenAt != null;
    }

    public boolean isAbgabefaehig() {
        return abgabefaehig;
    }

    public void setAbgabefaehig(boolean abgabefaehig) {
        this.abgabefaehig = abgabefaehig;
    }

    public List<String> getLocalIds() {
        return localIds;
    }

    public void setLocalIds(List<String> betriebsstaettenBerichte) {
        this.localIds = betriebsstaettenBerichte;
    }

    public String getFilename() {
        return jahr.getSchluessel() + "_" + land.getNr() + "_eureg_XEUR_" + DateTime.atCETForFile(
                ersteErfassung) + ".xml";
    }

    public String getFilenameProtokoll() {
        return jahr.getSchluessel() + "_" + land.getNr() + "_eureg_protocol_" + DateTime.atCETForFile(
                ersteErfassung) + ".txt";
    }

}
