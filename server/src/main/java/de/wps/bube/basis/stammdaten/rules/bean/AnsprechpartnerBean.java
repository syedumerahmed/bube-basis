package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RFKN;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.KommunikationsverbindungBean;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class AnsprechpartnerBean {

    private final Long id;
    private final String vorname;
    private final String nachname;
    private final ReferenzBean funktion;
    private final String bemerkung;
    private final List<KommunikationsverbindungBean> kommunikationsverbindungen;

    public AnsprechpartnerBean(Long id, String vorname, String nachname, ReferenzBean funktion, String bemerkung,
            List<KommunikationsverbindungBean> kommunikationsverbindungen) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.funktion = funktion;
        this.bemerkung = bemerkung;
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @RegelApi.Member(description = "Vorname")
    public String getVorname() {
        return vorname;
    }

    @RegelApi.Member(description = "Nachname")
    public String getNachname() {
        return nachname;
    }

    @RegelApi.Member(description = "Funktion", referenzliste = RFKN)
    public ReferenzBean getFunktion() {
        return funktion;
    }

    @RegelApi.Member(description = "Bemerkung")
    public String getBemerkung() {
        return bemerkung;
    }

    public List<KommunikationsverbindungBean> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AnsprechpartnerBean that = (AnsprechpartnerBean) o;
        return Objects.equals(id, that.id) && Objects.equals(vorname,
                that.vorname) && Objects.equals(nachname, that.nachname) && Objects.equals(funktion,
                that.funktion) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(
                kommunikationsverbindungen, that.kommunikationsverbindungen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vorname, nachname, funktion, bemerkung, kommunikationsverbindungen);
    }

    @Override
    public String toString() {
        return "AnsprechpartnerBean{" +
                "id=" + id +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", funktion=" + funktion +
                ", bemerkung='" + bemerkung + '\'' +
                ", kommunikationsverbindungen=" + kommunikationsverbindungen +
                '}';
    }

}
