package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class BATDerogation {

    @XmlElement(name = "BATderogationType", namespace = SchemaSet.SCHEMA_EUREG)
    private final BATderogationType batDerogationType = new BATderogationType();

    public String getDerogationDurationEndDate() {
        return batDerogationType.derogationDurationEndDate;
    }

    public void setDerogationDurationEndDate(String derogationDurationEndDate) {
        this.batDerogationType.derogationDurationEndDate = derogationDurationEndDate;
    }

    public String getDerogationDurationStartDate() {
        return batDerogationType.derogationDurationStartDate;
    }

    public void setDerogationDurationStartDate(String derogationDurationStartDate) {
        this.batDerogationType.derogationDurationStartDate = derogationDurationStartDate;
    }

    public ReferenceType getBatael() {
        return batDerogationType.batael;
    }

    public void setBatael(ReferenceType batael) {
        this.batDerogationType.batael = batael;
    }

    public String getPublicReasonURL() {
        return batDerogationType.publicReasonURL;
    }

    public void setPublicReasonURL(String publicReasonURL) {
        this.batDerogationType.publicReasonURL = publicReasonURL;
    }

    public Boolean getBatDerogationIndicator() {
        return batDerogationType.batDerogationIndicator;
    }

    public void setBatDerogationIndicator(Boolean batDerogationIndicator) {
        batDerogationType.batDerogationIndicator = batDerogationIndicator;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class BATderogationType {
        @XmlElement(required = false, name = "derogationDurationEndDate", namespace = SchemaSet.SCHEMA_EUREG)
        String derogationDurationEndDate;

        @XmlElement(required = false,
                name = "derogationDurationStartDate",
                namespace = SchemaSet.SCHEMA_EUREG)
        String derogationDurationStartDate;

        @XmlElement(required = false, name = "BATAEL", namespace = SchemaSet.SCHEMA_EUREG)
        ReferenceType batael;

        @XmlElement(required = false, name = "publicReasonURL", namespace = SchemaSet.SCHEMA_EUREG)
        String publicReasonURL;

        @XmlElement(required = true, name = "BATDerogationIndicator", namespace = SchemaSet.SCHEMA_EUREG)
        Boolean batDerogationIndicator;
    }
}
