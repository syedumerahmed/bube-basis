package de.wps.bube.basis.euregistry.domain.service;

import static java.lang.String.format;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobContext;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Service
public class XeurTeilberichtErstellenJob {
    public final static JobStatusEnum JOB_NAME = JobStatusEnum.XEUR_TEILBERICHT_ERSTELLEN;
    final String PROTOKOLL_OPENING = "Bitte Protokoll bis zum Ende durchlesen, um das Ergebnis der Erstellung des XEUR-Teilberichtes zu erfahren.";
    private static final int COUNT_SIZE = 10;

    private final Logger logger = LoggerFactory.getLogger(XeurTeilberichtErstellenJob.class);

    private final BenutzerJobRegistry benutzerJobRegistry;
    private final XeurTeilberichtService xeurTeilberichtService;
    private final AktiverBenutzer aktiverBenutzer;

    public XeurTeilberichtErstellenJob(BenutzerJobRegistry benutzerJobRegistry,
            XeurTeilberichtService xeurTeilberichtService, AktiverBenutzer aktiverBenutzer) {
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.xeurTeilberichtService = xeurTeilberichtService;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(List<Long> berichtIds, Referenz zieljahr, Land land) {
        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(
                format("%s Start: %s Zieljahr: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), zieljahr.getSchluessel(),
                        aktiverBenutzer.getUsername()));

        try {
            runJob(berichtIds, zieljahr, land, jobProtokoll);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }

    private void runJob(List<Long> berichtIds, Referenz zieljahr, Land land, JobProtokoll jobProtokoll) {
        var jobCount = new AtomicLong();

        var context = new JobContext<EURegBetriebsstaette>(jobProtokoll, bst -> {
            if (jobCount.incrementAndGet() % COUNT_SIZE == 0) {
                benutzerJobRegistry.jobCountChanged(COUNT_SIZE);
            }
        });

        xeurTeilberichtService.erstellXeurTeilbericht(berichtIds, zieljahr, land, context);

        benutzerJobRegistry.jobCountChanged(jobCount.get() % COUNT_SIZE);
        benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
        JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
        jobProtokoll.addEintrag(
                format("%s Ende: %s Anzahl Betriebsstätten: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                        jobStatus.getCount(), jobStatus.getRuntimeSec()));
    }
}
