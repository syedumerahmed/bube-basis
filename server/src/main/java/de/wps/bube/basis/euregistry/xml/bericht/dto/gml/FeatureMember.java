package de.wps.bube.basis.euregistry.xml.bericht.dto.gml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionFacility;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionInstallation;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionInstallationPart;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;

@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureMember {

    private  static final String NAMESPACE = SchemaSet.SCHEMA_EUREG;

    @XmlElement(name = "ProductionSite", namespace = NAMESPACE)
    private ProductionSite productionSite;

    @XmlElement(name = "ProductionFacility", namespace = NAMESPACE)
    private ProductionFacility productionFacility;

    @XmlElement(name = "ProductionInstallation", namespace = NAMESPACE)
    private ProductionInstallation productionInstallation;

    @XmlElement(name = "ProductionInstallationPart", namespace = NAMESPACE)
    private ProductionInstallationPart productionInstallationPart;

    @XmlElement(name = "ReportData", namespace = NAMESPACE)
    private ReportData reportData;

    public FeatureMember(ProductionSite productionSite) {
        this.productionSite = productionSite;
    }

    public FeatureMember(ProductionFacility productionFacility) {
        this.productionFacility = productionFacility;
    }

    public FeatureMember(ProductionInstallation productionInstallation) {
        this.productionInstallation = productionInstallation;
    }

    public FeatureMember(ProductionInstallationPart productionInstallationPart) {
        this.productionInstallationPart = productionInstallationPart;
    }

    public FeatureMember(ReportData reportData) {
        this.reportData = reportData;
    }

    public ProductionSite getProductionSite() {
        return productionSite;
    }

    public ReportData getReportData() {
        return reportData;
    }

    public ProductionFacility getProductionFacility() {
        return productionFacility;
    }

    public ProductionInstallation getProductionInstallation() {
        return productionInstallation;
    }

    public ProductionInstallationPart getProductionInstallationPart() {
        return productionInstallationPart;
    }

}
