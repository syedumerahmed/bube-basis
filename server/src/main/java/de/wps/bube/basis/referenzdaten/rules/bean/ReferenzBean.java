package de.wps.bube.basis.referenzdaten.rules.bean;

import java.time.Instant;
import java.util.Objects;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public class ReferenzBean {

    private final Long id;
    private final Land land;
    private final Referenzliste referenzliste;
    private final String schluessel;
    private final String sortier;
    private final String ktext;
    private final String ltext;
    private final String eu;
    private final String strg;
    private final int gueltigVon;
    private final int gueltigBis;
    private final Instant letzteAenderung;

    public ReferenzBean(Long id, Referenzliste referenzliste, Land land, String schluessel, String sortier,
            String ktext, String ltext, String eu, String strg, int gueltigVon, int gueltigBis,
            Instant letzteAenderung) {
        this.id = id;
        this.referenzliste = referenzliste;
        this.land = land;
        this.schluessel = schluessel;
        this.sortier = sortier;
        this.ktext = ktext;
        this.ltext = ltext;
        this.eu = eu;
        this.strg = strg;
        this.gueltigVon = gueltigVon;
        this.gueltigBis = gueltigBis;
        this.letzteAenderung = letzteAenderung;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    public Land getLand() {
        return land;
    }

    @RegelApi.Member(description = "Schlüssel")
    public String getSchluessel() {
        return schluessel;
    }

    @RegelApi.Member(description = "Sortierung")
    public String getSortier() {
        return sortier;
    }

    @RegelApi.Member(description = "Gültig von")
    public int getGueltigVon() {
        return gueltigVon;
    }

    @RegelApi.Member(description = "Gültig bis")
    public int getGueltigBis() {
        return gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @RegelApi.Member(description = "Kurztext")
    public String getKtext() {
        return ktext;
    }

    @RegelApi.Member(description = "Langtext")
    public String getLtext() {
        return ltext;
    }

    public Referenzliste getReferenzliste() {
        return referenzliste;
    }

    @RegelApi.Member(description = "EU")
    public String getEu() {
        return eu;
    }

    @RegelApi.Member(description = "Steuerung")
    public String getStrg() {
        return strg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReferenzBean referenz = (ReferenzBean) o;

        return Objects.equals(id, referenz.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Referenz " + "ID=" + id + ", Land=" + land.getNr() + ", Referenzliste=" + referenzliste + ", Schlüssel='" + schluessel + '\'';
    }

}
