package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.*;

import java.util.Optional;

import org.mapstruct.Context;
import org.mapstruct.Named;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

@Service
public class ReferenzResolverService {

    private final ReferenzenService referenzenService;
    private final BehoerdenService behoerdenService;

    public ReferenzResolverService(ReferenzenService referenzenService,
            BehoerdenService behoerdenService) {
        this.referenzenService = referenzenService;
        this.behoerdenService = behoerdenService;
    }

    @Named("RCONF")
    public Referenz vertraulichkeitsgrundFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RCONF, schluessel, context);
    }

    @Named("RSTAAT")
    public Referenz landIsoCodeFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RSTAAT, schluessel, context);
    }

    @Named("RNACE")
    public Referenz naceCodeFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RNACE, schluessel, context);
    }

    @Named("RJAHR")
    public Referenz berichtsjahrFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RJAHR, schluessel, context);
    }

    @Named("RGMD")
    public Referenz gemeindeFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RGMD, schluessel, context);
    }

    @Named("RFLEZ")
    public Referenz flusseinzugsgebietFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RFLEZ, schluessel, context);
    }

    @Named("RBETZ")
    public Referenz betriebsstatusFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RBETZ, schluessel, context);
    }

    @Named("RKOM")
    public Referenz kommunikationsArtFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RKOM, schluessel, context);
    }

    @Named("RFKN")
    public Referenz ansprechpartnerFunktionFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RFKN, schluessel, context);
    }

    @Named("RQUEA")
    public Referenz quelleArtFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RQUEA, schluessel, context);
    }

    @Named("REINH")
    public Referenz einheitFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(REINH, schluessel, context);
    }

    @Named("RZUST")
    public Referenz zustaendigkeitFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RZUST, schluessel, context);
    }

    @Named("REPSG")
    public Referenz koordinatensystemFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(REPSG, schluessel, context);
    }

    @Named("RVORS")
    public Referenz vorschriftFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RVORS, schluessel, context);
    }

    @Named("RBEA")
    public Referenz bearbeitungsstatusFromDto(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RBEA, schluessel, context);
    }

    @Named("RBATE")
    public Referenz emissionswertAusnahme(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RBATE, schluessel, context);
    }

    @Named("RRTYP")
    public Referenz berichtsart(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RRTYP, schluessel, context);
    }

    @Named("RRELCH")
    public Referenz kapitelIerl(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RRELCH, schluessel, context);
    }

    @Named("RBATC")
    public Referenz anwendbareBvt(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RBATC, schluessel, context);
    }

    @Named("RSPECC")
    public Referenz spezielleBedingungenIE51(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RSPECC, schluessel, context);
    }

    @Named("RDERO")
    public Referenz ieAusnahmen(String schluessel, @Context ReferenzResolverContext context) {
        return loadReferenz(RDERO, schluessel, context);
    }

    public Referenz loadReferenz(Referenzliste liste, String schluessel, ReferenzResolverContext context) {
        if (schluessel == null) {
            return null;
        }

        Optional<Referenz> optReferenz = referenzenService.findReferenz(liste, schluessel, context.getJahr(),
                context.getLand());
        if (optReferenz.isPresent()) {
            return optReferenz.get();
        } else {

            Optional<Referenz> optReferenzGesucht = referenzenService.sucheReferenzUeberAlleJahre(liste, schluessel,
                    context.getLand());

            if (optReferenzGesucht.isPresent()) {
                // Warnung ausgeben für ungültige Referenz
                context.onReferenzUngueltig(liste, schluessel);
                return optReferenzGesucht.get();
            } else {
                // Error ausgeben für Referenz auch bei der Suche nicht gefunden
                context.onReferenzNotFound(liste, schluessel);
                return null;
            }
        }
    }

    public Behoerde loadBehoerde(String schluessel, @Context ReferenzResolverContext context) {
        if (schluessel == null) {
            return null;
        }

        Optional<Behoerde> optBehoerde = behoerdenService.findBehoerde(schluessel, context.getJahr(),
                context.getLand());
        if (optBehoerde.isPresent()) {
            return optBehoerde.get();
        } else {

            Optional<Behoerde> optBehoerdeGesucht = behoerdenService.sucheBehoerdeUeberAlleJahre(schluessel,
                    context.getLand());

            if (optBehoerdeGesucht.isPresent()) {
                // Warnung ausgeben für ungültige Behörde
                context.onBehoerdeUngueltig(schluessel);
                return optBehoerdeGesucht.get();
            } else {
                // Error ausgeben für Behörde auch bei der Suche nicht gefunden
                context.onBehoerdeNotFound(schluessel);
                return null;
            }
        }
    }

}
