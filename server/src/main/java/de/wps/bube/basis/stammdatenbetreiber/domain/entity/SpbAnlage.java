package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class SpbAnlage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "spb_anlage_seq")
    @SequenceGenerator(name = "spb_anlage_seq", allocationSize = 50)
    private Long id;

    // Eine Spb-Anlage hat eine Eltern Betriebsstätte
    @NotNull
    private Long parentBetriebsstaetteId;

    // Eine Spb-Anlage verändert diese Anlage
    private Long anlageId;

    @NotNull
    private String anlageNr;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    // hat Vorschriften 0..n
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Vorschrift> vorschriften;

    // hat Leistungen 0..n
    @ElementCollection
    private List<Leistung> leistungen;

    @NotNull
    private String name;

    @ManyToOne
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;

    @NotNull
    private Instant ersteErfassung = Instant.now();

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    public SpbAnlage() {
    }

    public SpbAnlage(Anlage anlage) {
        this.anlageId = anlage.getId();
        this.parentBetriebsstaetteId = anlage.getParentBetriebsstaetteId();
        this.anlageNr = anlage.getAnlageNr();
        var geo = anlage.getGeoPunkt();
        if (geo != null) {
            this.geoPunkt = new GeoPunkt(geo.getNord(), geo.getOst(), geo.getEpsgCode());
        }
        this.name = anlage.getName();
        this.betriebsstatus = anlage.getBetriebsstatus();
        this.betriebsstatusSeit = anlage.getBetriebsstatusSeit();
        this.inbetriebnahme = anlage.getInbetriebnahme();
        this.ersteErfassung = anlage.getErsteErfassung();
        this.letzteAenderung = anlage.getLetzteAenderung();

        // Kopieren der Leistungen
        this.leistungen = new ArrayList<>();
        this.leistungen.addAll(anlage.getLeistungen());

        // Kopieren der Vorschriften
        this.vorschriften = new ArrayList<>();
        anlage.getVorschriften().forEach(vorschrift -> {
            Vorschrift clone = new Vorschrift(vorschrift.getArt(), new ArrayList<>(vorschrift.getTaetigkeiten()),
                    vorschrift.getHaupttaetigkeit());
            this.vorschriften.add(clone);
        });
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    public void setParentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
    }

    public Long getAnlageId() {
        return anlageId;
    }

    public void setAnlageId(Long anlageId) {
        this.anlageId = anlageId;
    }

    public String getAnlageNr() {
        return anlageNr;
    }

    public void setAnlageNr(String anlageNr) {
        this.anlageNr = anlageNr;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public List<Vorschrift> getVorschriften() {
        if (vorschriften == null) {
            return Collections.emptyList();
        }
        return vorschriften;
    }

    public void setVorschriften(List<Vorschrift> vorschriften) {
        this.vorschriften = vorschriften;
    }

    public List<Leistung> getLeistungen() {
        return leistungen;
    }

    public void setLeistungen(List<Leistung> leistungen) {
        this.leistungen = leistungen;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDate getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDate getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public boolean isNew() {
        return id == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpbAnlage spbAnlage = (SpbAnlage) o;
        return id.equals(spbAnlage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean hasAnlage() {
        return anlageId != null;
    }
}
