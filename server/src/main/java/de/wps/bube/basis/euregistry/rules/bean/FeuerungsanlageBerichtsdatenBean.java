package de.wps.bube.basis.euregistry.rules.bean;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.euregistry.domain.vo.FeuerungsanlageTyp;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi.Member;

public class FeuerungsanlageBerichtsdatenBean {

    private final FeuerungsanlageTyp feuerungsanlageTyp;
    private final Double gesamtKapazitaetAbfall;
    private final Double kapazitaetGefAbfall;
    private final Double kapazitaetNichtGefAbfall;
    private final boolean mehrWaermeVonGefAbfall;
    private final boolean mitverbrennungVonUnbehandeltemAbfall;
    private final String oeffentlicheBekanntmachung;
    private final String oeffentlicheBekanntmachungUrl;
    private final List<SpezielleBedingungIE51Bean> spezielleBedingungen;
    private final List<ReferenzBean> ieAusnahmen;

    public FeuerungsanlageBerichtsdatenBean(FeuerungsanlageTyp feuerungsanlageTyp, Double gesamtKapazitaetAbfall,
            Double kapazitaetGefAbfall, Double kapazitaetNichtGefAbfall, boolean mehrWaermeVonGefAbfall,
            boolean mitverbrennungVonUnbehandeltemAbfall, String oeffentlicheBekanntmachung,
            String oeffentlicheBekanntmachungUrl,
            List<SpezielleBedingungIE51Bean> spezielleBedingungen,
            List<ReferenzBean> ieAusnahmen) {
        this.feuerungsanlageTyp = feuerungsanlageTyp;
        this.gesamtKapazitaetAbfall = gesamtKapazitaetAbfall;
        this.kapazitaetGefAbfall = kapazitaetGefAbfall;
        this.kapazitaetNichtGefAbfall = kapazitaetNichtGefAbfall;
        this.mehrWaermeVonGefAbfall = mehrWaermeVonGefAbfall;
        this.mitverbrennungVonUnbehandeltemAbfall = mitverbrennungVonUnbehandeltemAbfall;
        this.oeffentlicheBekanntmachung = oeffentlicheBekanntmachung;
        this.oeffentlicheBekanntmachungUrl = oeffentlicheBekanntmachungUrl;
        this.spezielleBedingungen = spezielleBedingungen;
        this.ieAusnahmen = ieAusnahmen;
    }

    @Member(description = "Typ Feuerungsanlage")
    public FeuerungsanlageTyp getFeuerungsanlageTyp() {
        return feuerungsanlageTyp;
    }

    @Member(description = "Genehmigte Kapazität Abfall [t/h]")
    public Double getGesamtKapazitaetAbfall() {
        return gesamtKapazitaetAbfall;
    }

    @Member(description = "Genehmigte Kapazität gefährliche Abfälle [t/h]")
    public Double getKapazitaetGefAbfall() {
        return kapazitaetGefAbfall;
    }

    @Member(description = "Genehmigte Kapazität nicht gefährliche Abfälle [t/h]")
    public Double getKapazitaetNichtGefAbfall() {
        return kapazitaetNichtGefAbfall;
    }

    @Member(description = "Mehr als 40% der Wärme stammt von gef. Abfall")
    public boolean isMehrWaermeVonGefAbfall() {
        return mehrWaermeVonGefAbfall;
    }

    @Member(description = "Mitverbrennung von unbehandeltem kommunalem Abfall")
    public boolean isMitverbrennungVonUnbehandeltemAbfall() {
        return mitverbrennungVonUnbehandeltemAbfall;
    }

    @Member(description = "Angabe über öffentliche Bekanntmachung nach Art. 55(2) IE-RL")
    public String getOeffentlicheBekanntmachung() {
        return oeffentlicheBekanntmachung;
    }

    @Member(description = "URL öffentliche Bekanntmachung")
    public String getOeffentlicheBekanntmachungUrl() {
        return oeffentlicheBekanntmachungUrl;
    }

    public List<SpezielleBedingungIE51Bean> getSpezielleBedingungen() {
        return spezielleBedingungen;
    }

    @Member(description = "Ausnahme nach Art. 31 bis 35 sowie 72 (4)b) IE-RL", referenzliste = Referenzliste.RDERO)
    public List<ReferenzBean> getIeAusnahmen() {
        return ieAusnahmen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        FeuerungsanlageBerichtsdatenBean that = (FeuerungsanlageBerichtsdatenBean) o;
        return mehrWaermeVonGefAbfall == that.mehrWaermeVonGefAbfall
                && mitverbrennungVonUnbehandeltemAbfall == that.mitverbrennungVonUnbehandeltemAbfall
                && feuerungsanlageTyp == that.feuerungsanlageTyp
                && Objects.equals(gesamtKapazitaetAbfall, that.gesamtKapazitaetAbfall)
                && Objects.equals(kapazitaetGefAbfall, that.kapazitaetGefAbfall)
                && Objects.equals(kapazitaetNichtGefAbfall, that.kapazitaetNichtGefAbfall)
                && Objects.equals(oeffentlicheBekanntmachung, that.oeffentlicheBekanntmachung)
                && Objects.equals(oeffentlicheBekanntmachungUrl, that.oeffentlicheBekanntmachungUrl)
                && Objects.equals(spezielleBedingungen, that.spezielleBedingungen)
                && Objects.equals(ieAusnahmen, that.ieAusnahmen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feuerungsanlageTyp, gesamtKapazitaetAbfall, kapazitaetGefAbfall, kapazitaetNichtGefAbfall,
                mehrWaermeVonGefAbfall, mitverbrennungVonUnbehandeltemAbfall, oeffentlicheBekanntmachung,
                oeffentlicheBekanntmachungUrl, spezielleBedingungen, ieAusnahmen);
    }

    @Override
    public String toString() {
        return "FeuerungsanlageBerichtsdatenBean{" +
                "feuerungsanlageTyp=" + feuerungsanlageTyp +
                ", gesamtKapazitaetAbfall=" + gesamtKapazitaetAbfall +
                ", kapazitaetGefAbfall=" + kapazitaetGefAbfall +
                ", kapazitaetNichtGefAbfall=" + kapazitaetNichtGefAbfall +
                ", mehrWaermeVonGefAbfall=" + mehrWaermeVonGefAbfall +
                ", mitverbrennungVonUnbehandeltemAbfall=" + mitverbrennungVonUnbehandeltemAbfall +
                ", oeffentlicheBekanntmachung='" + oeffentlicheBekanntmachung + '\'' +
                ", oeffentlicheBekanntmachungUrl='" + oeffentlicheBekanntmachungUrl + '\'' +
                ", spezielleBedingungen=" + spezielleBedingungen +
                ", ieAusnahmen=" + ieAusnahmen +
                '}';
    }

}