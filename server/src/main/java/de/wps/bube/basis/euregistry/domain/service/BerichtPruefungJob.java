package de.wps.bube.basis.euregistry.domain.service;

import static java.lang.String.format;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;

@Service
public class BerichtPruefungJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(BerichtPruefungJob.class);

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.KOMPLEXPRUEFUNG;

    private static final String PROTOKOLL_OPENING = "Das Ergebnis der letzten Komplexprüfung ist " +
            "(für die geprüfte EURegistry Betriebsstätte und das geprüfte Jahr) " +
            "dem Protokoll der Komplexprüfung zu entnehmen, welches unter Aktionen in der EURegistry Betriebsstättenübersicht abrufbar ist " +
            "(Button “letztes Komplexprüfungsprotokoll herunterladen”)";

    private final BerichtPruefungService berichtPruefungService;
    private final EURegistryService euRegistryService;
    private final BenutzerJobRegistry benutzerJobRegistry;

    public BerichtPruefungJob(BerichtPruefungService berichtPruefungService,
            EURegistryService euRegistryService,
            BenutzerJobRegistry benutzerJobRegistry) {
        this.berichtPruefungService = berichtPruefungService;
        this.euRegistryService = euRegistryService;
        this.benutzerJobRegistry = benutzerJobRegistry;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(List<Long> ids, String username) {

        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(format("%s Start: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), username));

        RegelPruefungService.StopWatch stopWatch = RegelPruefungService.getStopWatch().reset();
        long time = -System.currentTimeMillis();

        try {

            euRegistryService
                    .streamAllByBetriebsstaetteIdIn(ids)
                    .forEach(
                            euRegBetriebsstaette -> {
                                berichtPruefungService.komplexpruefung(euRegBetriebsstaette);
                                benutzerJobRegistry.jobCountChanged(1);
                                jobProtokoll.addEintrag(
                                        format("Komplexprüfung Gruppe A und B für BST-Nr: %s ausgeführt.",
                                                euRegBetriebsstaette.getBetriebsstaette().getBetriebsstaetteNr()));
                            });

            benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
            benutzerJobRegistry.getJobStatus().ifPresent(jobStatus ->
                    jobProtokoll.addEintrag(
                            format("%s Ende: %s Anzahl: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                                    jobStatus.getCount(), jobStatus.getRuntimeSec())));

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
            time += System.currentTimeMillis() - stopWatch.getMapping() - stopWatch.getEvaluation();
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Construction of the Rules-API objects took {} milliseconds", stopWatch.getMapping());
                LOGGER.trace("Evaluation of the rules conditions objects took {} milliseconds", stopWatch.getEvaluation());
                LOGGER.trace("Other actions took {} milliseconds", time);
            }
        }
    }

}
