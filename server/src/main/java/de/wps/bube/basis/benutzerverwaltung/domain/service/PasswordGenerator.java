package de.wps.bube.basis.benutzerverwaltung.domain.service;

public interface PasswordGenerator {
    String generatePassword();
}
