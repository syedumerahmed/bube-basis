package de.wps.bube.basis.stammdaten.domain.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Ansprechpartner implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ansprechpartner_seq")
    @SequenceGenerator(name = "ansprechpartner_seq", allocationSize = 50)
    private Long id;

    private String vorname;
    private String nachname;

    @ManyToOne
    private Referenz funktion;
    private String bemerkung;

    @ElementCollection
    private List<Kommunikationsverbindung> kommunikationsverbindungen;

    public Ansprechpartner() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Referenz getFunktion() {
        return funktion;
    }

    public void setFunktion(Referenz funktion) {
        this.funktion = funktion;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public List<Kommunikationsverbindung> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    public void setKommunikationsverbindungen(List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ansprechpartner that = (Ansprechpartner) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean equalValues(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ansprechpartner that = (Ansprechpartner) o;
        return Objects.equals(vorname, that.vorname) && nachname.equals(
                that.nachname) && Objects.equals(funktion, that.funktion) && kommunikationsverbindungen.containsAll(that.kommunikationsverbindungen) && that.kommunikationsverbindungen.containsAll(kommunikationsverbindungen);
    }
}
