package de.wps.bube.basis.sicherheit.domain.vo;

import java.util.stream.Stream;

public enum Fachmodul {

    PRTR_LCP("PRTR+LCP"),
    E_ERKLAERUNG("E-Erklaerung"),
    RECHERCHE("Recherche"),
    PROTOTYP("Prototyp");

    private final String bezeichner;

    Fachmodul(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static Fachmodul of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiges Fachmodul: " + bezeichner));
    }
}
