package de.wps.bube.basis.stammdatenuebernahme.domain.model;

public class SpbQuelleVergleich {
    private Long id;
    private Long spbId;
    private StringVector name;
    private StringVector quelleNr;
    private GeoPunktVector geoPunkt;
    private ReferenzVector quelleArt;
    private NumberVector flaeche;
    private NumberVector laenge;
    private NumberVector breite;
    private NumberVector durchmesser;
    private NumberVector bauhoehe;
    private boolean hasChanges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public StringVector getName() {
        return name;
    }

    public void setName(StringVector name) {
        this.name = name;
    }

    public StringVector getQuelleNr() {
        return quelleNr;
    }

    public void setQuelleNr(StringVector quelleNr) {
        this.quelleNr = quelleNr;
    }

    public GeoPunktVector getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunktVector geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public ReferenzVector getQuelleArt() {
        return quelleArt;
    }

    public void setQuelleArt(ReferenzVector quelleArt) {
        this.quelleArt = quelleArt;
    }

    public NumberVector getFlaeche() {
        return flaeche;
    }

    public void setFlaeche(NumberVector flaeche) {
        this.flaeche = flaeche;
    }

    public NumberVector getLaenge() {
        return laenge;
    }

    public void setLaenge(NumberVector laenge) {
        this.laenge = laenge;
    }

    public NumberVector getBreite() {
        return breite;
    }

    public void setBreite(NumberVector breite) {
        this.breite = breite;
    }

    public NumberVector getDurchmesser() {
        return durchmesser;
    }

    public void setDurchmesser(NumberVector durchmesser) {
        this.durchmesser = durchmesser;
    }

    public NumberVector getBauhoehe() {
        return bauhoehe;
    }

    public void setBauhoehe(NumberVector bauhoehe) {
        this.bauhoehe = bauhoehe;
    }

    public boolean isHasChanges() {
        return hasChanges;
    }

    public void setHasChanges(boolean hasChanges) {
        this.hasChanges = hasChanges;
    }
}
