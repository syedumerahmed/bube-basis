package de.wps.bube.basis.euregistry.domain.service;

import java.io.IOException;
import java.io.Writer;
import java.util.Optional;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettePruefungProtokollGenerator;

public class BerichtPruefungProtokollGenerator extends BetriebsstaettePruefungProtokollGenerator {

    private final EURegBetriebsstaette bericht;
    private final EURegistryService eURegistryService;

    public BerichtPruefungProtokollGenerator(Betriebsstaette betriebsstaette, EURegBetriebsstaette bericht,
            Komplexpruefung komplexpruefung,
            KomplexpruefungService komplexpruefungService,
            EURegistryService eURegistryService) {
        super(betriebsstaette, komplexpruefung, komplexpruefungService);
        this.bericht = bericht;
        this.eURegistryService = eURegistryService;
    }

    @Override
    protected void protokolliereBetriebsstaetteAbhaengigkeiten(Writer out) throws IOException {
        if (bericht != null) {
            protokolliereBericht(out);
        }
        super.protokolliereBetriebsstaetteAbhaengigkeiten(out);
    }

    protected void protokolliereBericht(Writer out) throws IOException {
        out.write("EUReg Betriebsstätte Bericht:".indent(EINZUG));
        protokolliereRegelPruefung(out, bericht.getId(), RegelObjekt.EUREG_BST, EINZUG);
    }

    @Override
    protected void protokolliereAnlageAbhaengigkeiten(Writer out, Anlage anlage) throws IOException {
        if (bericht != null) {
            Optional<EURegAnlage> euRegAnlageOpt = eURegistryService.findAnlagenBerichtByAnlageId(anlage.getId());
            if (euRegAnlageOpt.isPresent()) {
                protokolliereAnlagenbericht(out, euRegAnlageOpt.get());
            }
        }

        super.protokolliereAnlageAbhaengigkeiten(out, anlage);
    }

    protected void protokolliereAnlagenbericht(Writer out, EURegAnlage anlagenbericht) throws IOException {
        int einzug = 2 * EINZUG;
        out.write("EUReg Anlage Bericht:".indent(einzug));
        protokolliereRegelPruefung(out, anlagenbericht.getId(), RegelObjekt.EUREG_ANLAGE, einzug);
        protokolliereRegelPruefung(out, anlagenbericht.getId(), RegelObjekt.EUREG_F, einzug);
    }

    @Override
    protected void protokolliereAnlagenteil(Writer out, Anlagenteil anlagenteil) throws IOException {
        super.protokolliereAnlagenteil(out, anlagenteil);
        if (bericht != null) {
            Optional<EURegAnlage> euRegAnlageOpt = eURegistryService.findAnlagenBerichtByAnlagenteilId(
                    anlagenteil.getId());
            if (euRegAnlageOpt.isPresent()) {
                protokolliereAnlagenteilbericht(out, euRegAnlageOpt.get());
            }
        }
    }

    private void protokolliereAnlagenteilbericht(Writer out, EURegAnlage anlagenbericht) throws IOException {
        int einzug = 3 * EINZUG;
        out.write("EUReg Anlagenteil Bericht:".indent(einzug));
        protokolliereRegelPruefung(out, anlagenbericht.getId(), RegelObjekt.EUREG_ANLAGE, einzug);
        protokolliereRegelPruefung(out, anlagenbericht.getId(), RegelObjekt.EUREG_F, einzug);
    }

}
