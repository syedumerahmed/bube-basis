package de.wps.bube.basis.referenzdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RCONF;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RSTAAT;

import java.util.Objects;

public class AdresseBean {

    private final String strasse;
    private final String hausNr;
    private final String plz;
    private final String ort;
    private final String ortsteil;
    private final String postfachPlz;
    private final String postfach;
    private final ReferenzBean landIsoCode;
    private final ReferenzBean vertraulichkeitsgrund;
    private final String notiz;

    public AdresseBean(String strasse, String hausNr, String plz, String ort, String ortsteil, String postfachPlz,
            String postfach, ReferenzBean landIsoCode, ReferenzBean vertraulichkeitsgrund, String notiz) {
        this.strasse = strasse;
        this.hausNr = hausNr;
        this.plz = plz;
        this.ort = ort;
        this.ortsteil = ortsteil;
        this.postfachPlz = postfachPlz;
        this.postfach = postfach;
        this.landIsoCode = landIsoCode;
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        this.notiz = notiz;
    }

    @RegelApi.Member(description = "Straße")
    public String getStrasse() {
        return strasse;
    }

    @RegelApi.Member(description = "Haus-Nr.")
    public String getHausNr() {
        return hausNr;
    }

    @RegelApi.Member(description = "PLZ")
    public String getPlz() {
        return plz;
    }

    @RegelApi.Member(description = "Ort")
    public String getOrt() {
        return ort;
    }

    @RegelApi.Member(description = "Ortsteil")
    public String getOrtsteil() {
        return ortsteil;
    }

    @RegelApi.Member(description = "Postfach PLZ")
    public String getPostfachPlz() {
        return postfachPlz;
    }

    @RegelApi.Member(description = "Postfach")
    public String getPostfach() {
        return postfach;
    }

    @RegelApi.Member(description = "Land", referenzliste = RSTAAT)
    public ReferenzBean getLandIsoCode() {
        return landIsoCode;
    }

    @RegelApi.Member(description = "Vertraulichkeitsgrund", referenzliste = RCONF)
    public ReferenzBean getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    @RegelApi.Member(description = "Adressnotiz")
    public String getNotiz() {
        return notiz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AdresseBean that = (AdresseBean) o;
        return Objects.equals(strasse, that.strasse) && Objects.equals(hausNr,
                that.hausNr) && Objects.equals(plz, that.plz) && Objects.equals(ort,
                that.ort) && Objects.equals(ortsteil, that.ortsteil) && Objects.equals(postfachPlz,
                that.postfachPlz) && Objects.equals(postfach, that.postfach) && Objects.equals(
                landIsoCode, that.landIsoCode) && Objects.equals(vertraulichkeitsgrund,
                that.vertraulichkeitsgrund) && Objects.equals(notiz, that.notiz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(strasse, hausNr, plz, ort, ortsteil, postfachPlz, postfach, landIsoCode,
                vertraulichkeitsgrund,
                notiz);
    }

    @Override
    public String toString() {
        return "AdresseBean{" +
                "strasse='" + strasse + '\'' +
                ", hausNr='" + hausNr + '\'' +
                ", plz='" + plz + '\'' +
                ", ort='" + ort + '\'' +
                ", ortsteil='" + ortsteil + '\'' +
                ", postfachPlz='" + postfachPlz + '\'' +
                ", postfach='" + postfach + '\'' +
                ", landIsoCode=" + landIsoCode +
                ", vertraulichkeitsgrund=" + vertraulichkeitsgrund +
                ", notiz='" + notiz + '\'' +
                '}';
    }

}
