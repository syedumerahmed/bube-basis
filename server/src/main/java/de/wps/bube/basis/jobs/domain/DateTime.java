package de.wps.bube.basis.jobs.domain;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTime {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    private static final DateTimeFormatter formatterForFile = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH_mm_ss");

    public static String format(ZonedDateTime date) {
        return date.format(formatter);
    }

    public static String formatForFile(ZonedDateTime date) {
        return date.format(formatterForFile);
    }

    public static String nowCET() {
        return format(ZonedDateTime.now(ZoneId.of("Europe/Berlin")));
    }

    public static String atCET(Instant date) {
        return format(date.atZone(ZoneId.of("Europe/Berlin")));
    }

    public static String atCETForFile(Instant date) {
        return formatForFile(date.atZone(ZoneId.of("Europe/Berlin")));
    }

}
