package de.wps.bube.basis.referenzdaten.xml;

import de.wps.bube.basis.base.xml.ImportException;

public class ReferenzdatenImportException extends ImportException {

    public ReferenzdatenImportException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ReferenzdatenImportException(String referenzSchluessel, String refListe) {
        super("Referenz %s aus Liste %s wird aufgrund der aufgelisteten Fehler nicht importiert".formatted(
                referenzSchluessel, refListe));
    }

    public ReferenzdatenImportException(String message) {
        super(message);
    }
}
