package de.wps.bube.basis.referenzdaten.domain;

public class ReferenzdatenSecurityException extends RuntimeException {
    public ReferenzdatenSecurityException(String message) {
        super(message);
    }

    public ReferenzdatenSecurityException(String message, Exception e) {
        super(message, e);
    }
}
