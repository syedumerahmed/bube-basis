package de.wps.bube.basis.desktop.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.entity.DesktopItemId;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.desktop.persistence.DesktopRepository;

@Service
@Transactional
public class DesktopService {

    private final DesktopRepository repository;

    public DesktopService(DesktopRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public List<DesktopItem> readDesktop(String username, String jahrSchluessel, DesktopTyp desktopTyp) {
        return repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp(username,
                jahrSchluessel, desktopTyp);
    }

    public void saveDesktopItems(String username, String jahrSchluessel, List<Long> itemIds,
            DesktopTyp desktopTyp) {
        repository.saveAll(itemIds.stream()
                                  .map(id -> new DesktopItemId(username, id, desktopTyp))
                                  .map(id -> new DesktopItem(id, jahrSchluessel))
                                  .toList());
    }

    public void deleteDesktopItem(String username, Long id, DesktopTyp desktopTyp) {
        repository.deleteById(new DesktopItemId(username, id, desktopTyp));
    }

    public void deleteDesktop(String username, String jahrSchluessel, DesktopTyp desktopTyp) {
        repository.deleteAllByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp(username, jahrSchluessel,
                desktopTyp);
    }

}
