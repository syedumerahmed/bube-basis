package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

public class AnlagenteilWurdeGeloeschtEvent {
    public static final String EVENT_NAME = "Anlagenteil wurde gelöscht";

    public final Long anlagenteilId;
    public final Long bstId;

    public AnlagenteilWurdeGeloeschtEvent(Long anlagenteilId, Long bstId) {
        this.anlagenteilId = anlagenteilId;
        this.bstId = bstId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnlagenteilWurdeGeloeschtEvent that = (AnlagenteilWurdeGeloeschtEvent) o;
        return anlagenteilId.equals(that.anlagenteilId) && bstId.equals(that.bstId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anlagenteilId, bstId);
    }
}
