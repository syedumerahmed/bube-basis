package de.wps.bube.basis.komplexpruefung.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.RegelDto;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

@Mapper
public interface RegelMapper {

    RegelDto toDto(Regel regel);

    Regel fromDto(RegelDto regelDto);

    static Gueltigkeitsjahr gueltigkeitsjahrFromDto(int jahr) {
        return Gueltigkeitsjahr.of(jahr);
    }

    static int gueltigkeitsjahrToDto(Gueltigkeitsjahr jahr) {
        return jahr.getJahr();
    }

    static Land landFromDto(String landNr) {
        return Land.of(landNr);
    }

    static String landToDto(Land land) {
        return land.getNr();
    }
}
