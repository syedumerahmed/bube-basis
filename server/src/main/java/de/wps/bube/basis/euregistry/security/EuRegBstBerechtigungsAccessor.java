package de.wps.bube.basis.euregistry.security;

import static com.querydsl.core.types.ExpressionUtils.or;
import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsAccessor;

@Component
public class EuRegBstBerechtigungsAccessor implements BerechtigungsAccessor {

    @Override
    public Predicate land(Land land) {
        return land.equals(Land.DEUTSCHLAND) ? null : eURegBetriebsstaette.betriebsstaette.land.eq(land);
    }

    @Override
    public Predicate behoerdenIn(Set<String> behoerden) {
        return behoerden.isEmpty() ?
                null :
                or(eURegBetriebsstaette.berichtsdaten.zustaendigeBehoerde.schluessel.in(behoerden),
                        eURegBetriebsstaette.betriebsstaette.zustaendigeBehoerde.schluessel.in(behoerden));
    }

    @Override
    public Predicate akzIn(Set<String> akz) {
        return akz.isEmpty() ?
                null :
                or(eURegBetriebsstaette.berichtsdaten.akz.in(akz), eURegBetriebsstaette.betriebsstaette.akz.in(akz));
    }

    @Override
    public Predicate verwaltungsgebieteIn(Set<String> verwaltungsgebiete) {
        return verwaltungsgebiete.isEmpty() ?
                null :
                verwaltungsgebiete.stream()
                                  .map(eURegBetriebsstaette.betriebsstaette.gemeindekennziffer.schluessel::startsWith)
                                  .collect(BooleanBuilder::new, BooleanBuilder::or, BooleanBuilder::or);
    }

    @Override
    public Predicate betriebsstaettenIn(Set<String> betriebsstaetten) {
        if (betriebsstaetten.isEmpty()) {
            return null;
        }
        List<String> upper = betriebsstaetten.stream().map(String::toUpperCase).toList();
        return eURegBetriebsstaette.betriebsstaette.betriebsstaetteNr.upper().in(upper);
    }
}
