package de.wps.bube.basis.referenzdaten.domain.vo;

public enum ParameterTyp {

    INTEGER, STRING

}
