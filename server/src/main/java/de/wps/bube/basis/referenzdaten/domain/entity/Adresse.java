package de.wps.bube.basis.referenzdaten.domain.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Embeddable
public class Adresse {

    private String strasse;
    private String hausNr;
    @NotEmpty
    private String plz;
    @NotEmpty
    private String ort;
    @Size(max = 64)
    private String ortsteil;
    private String postfachPlz;
    private String postfach;
    @ManyToOne
    private Referenz landIsoCode;
    @ManyToOne
    private Referenz vertraulichkeitsgrund;
    private String notiz;

    @Deprecated
    protected Adresse() {
    }

    public Adresse(String strasse, String hausNr, String ort, String plz) {
        this.strasse = strasse;
        this.hausNr = hausNr;
        this.ort = ort;
        this.plz = plz;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausNr() {
        return hausNr;
    }

    public void setHausNr(String strasseNr) {
        this.hausNr = strasseNr;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getPostfachPlz() {
        return postfachPlz;
    }

    public void setPostfachPlz(String plzPostfach) {
        this.postfachPlz = plzPostfach;
    }

    public String getPostfach() {
        return postfach;
    }

    public void setPostfach(String postfach) {
        this.postfach = postfach;
    }

    public Referenz getLandIsoCode() {
        return landIsoCode;
    }

    public void setLandIsoCode(Referenz landISOCode) {
        this.landIsoCode = landISOCode;
    }

    public String getNotiz() {
        return notiz;
    }

    public void setNotiz(String adresseNotiz) {
        this.notiz = adresseNotiz;
    }

    public String getOrtsteil() {
        return ortsteil;
    }

    public void setOrtsteil(String ortsteil) {
        this.ortsteil = ortsteil;
    }

    public Referenz getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    public void setVertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
    }
}
