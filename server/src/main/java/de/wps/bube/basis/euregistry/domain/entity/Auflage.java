package de.wps.bube.basis.euregistry.domain.entity;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class Auflage {
    @NotNull
    @ManyToOne
    private Referenz emissionswert;

    @NotNull
    private boolean nachArt18;

    @NotNull
    private boolean nachArt14;

    @Deprecated
    protected Auflage() {
    }

    public Auflage(Referenz emissionswert, boolean nachArt18, boolean nachArt14) {
        this.emissionswert = emissionswert;
        this.nachArt18 = nachArt18;
        this.nachArt14 = nachArt14;
    }

    public Referenz getEmissionswert() {
        return emissionswert;
    }

    public boolean isNachArt18() {
        return nachArt18;
    }

    public boolean isNachArt14() {
        return nachArt14;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Auflage auflage = (Auflage) o;
        return nachArt18 == auflage.nachArt18 && nachArt14 == auflage.nachArt14 && Objects
                .equals(emissionswert, auflage.emissionswert);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emissionswert, nachArt18, nachArt14);
    }
}
