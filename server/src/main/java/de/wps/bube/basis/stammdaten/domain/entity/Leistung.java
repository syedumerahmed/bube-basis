package de.wps.bube.basis.stammdaten.domain.entity;

import static javax.persistence.EnumType.STRING;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;

@Embeddable
public class Leistung {

    private static final String BEZUG_FWL_LOWER_CASE_1 = "fwl";
    private static final String BEZUG_FWL_LOWER_CASE_2 = "feuerungswärmeleistung";

    public static final String MEGAWATT = "MW";

    @NotNull
    private Double leistung;

    @NotNull
    @ManyToOne
    private Referenz leistungseinheit;

    private String bezug;

    @Enumerated(STRING)
    private Leistungsklasse leistungsklasse;

    @Deprecated
    protected Leistung() {
    }

    public Leistung(Double leistung, Referenz leistungseinheit, String bezug, Leistungsklasse leistungsklasse) {
        this.leistung = leistung;
        this.leistungseinheit = leistungseinheit;
        this.bezug = bezug;
        this.leistungsklasse = leistungsklasse;
    }

    public Double getLeistung() {
        return leistung;
    }

    public Referenz getLeistungseinheit() {
        return leistungseinheit;
    }

    public String getBezug() {
        return bezug;
    }

    public Leistungsklasse getLeistungsklasse() {
        return leistungsklasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Leistung leistung1 = (Leistung) o;

        if (!Objects.equals(leistung, leistung1.leistung)
            || !Objects.equals(leistungseinheit, leistung1.leistungseinheit)
            || !Objects.equals(bezug,leistung1.bezug)) {
            return false;
        }
        return leistungsklasse == leistung1.leistungsklasse;
    }

    @Override
    public int hashCode() {
        int result = leistung.hashCode();
        result = 31 * result + leistungseinheit.hashCode();
        result = 31 * result + bezug.hashCode();
        result = 31 * result + leistungsklasse.hashCode();
        return result;
    }

    public void setLeistungseinheit(Referenz r) {
        this.leistungseinheit = r;
    }

    public boolean istGenehmigt() {
        return Leistungsklasse.GENEHMIGT.equals(leistungsklasse);
    }

    public boolean istInstalliert() {
        return Leistungsklasse.INSTALLIERT.equals(leistungsklasse);
    }

    public boolean wirdBetrieben() {
        return Leistungsklasse.BETRIEBEN.equals(leistungsklasse);
    }

    public boolean isMegaWatt() {
        return leistungseinheit.getSchluessel().equals(MEGAWATT);
    }

    public boolean isFWL() {
        if (bezug != null) {
            return BEZUG_FWL_LOWER_CASE_1.equalsIgnoreCase(bezug.trim()) || BEZUG_FWL_LOWER_CASE_2.equalsIgnoreCase(
                    bezug.trim());
        } else {
            return false;
        }
    }
}
