package de.wps.bube.basis.euregistry.domain.service;

import static java.lang.String.format;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.euregistry.xml.EuRegBetriebsstaetteImportDtoMapper;
import de.wps.bube.basis.euregistry.xml.EuRegistryImportException;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBst;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBstListXDto;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenJob;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class EuRegistryImportierenJob {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.EUREGISRTY_IMPORTIEREN;

    private static final Logger LOGGER = LoggerFactory.getLogger(EuRegistryImportierenJob.class);

    private static final int COUNT_SIZE = 10;

    private final EURegistryImportExportService euRegistryImportExportService;
    private final BenutzerJobRegistry benutzerJobRegistry;
    private final AktiverBenutzer aktiverBenutzer;
    private final XMLService xmlService;
    private final DateiService dateiService;
    private final EuRegBetriebsstaetteImportDtoMapper euRegBetriebsstaetteImportDtoMapper;

    public EuRegistryImportierenJob(EURegistryImportExportService euRegistryImportExportService,
            BenutzerJobRegistry benutzerJobRegistry, XMLService xmlService,
            AktiverBenutzer aktiverBenutzer, DateiService dateiService,
            EuRegBetriebsstaetteImportDtoMapper euRegBetriebsstaetteImportDtoMapper) {
        this.euRegistryImportExportService = euRegistryImportExportService;
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.aktiverBenutzer = aktiverBenutzer;
        this.xmlService = xmlService;
        this.dateiService = dateiService;
        this.euRegBetriebsstaetteImportDtoMapper = euRegBetriebsstaetteImportDtoMapper;
    }

    @Async
    public void runAsync(String berichtsjahr) {
        JobProtokoll jobProtokoll = new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(
                format("%s Start: %s Berichtsjahr: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), berichtsjahr,
                        aktiverBenutzer.getUsername()));

        try {
            AtomicInteger count = new AtomicInteger();
            AtomicInteger exceptionCount = new AtomicInteger();

            InputStream inputStream = dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG);
            xmlService.read(inputStream, EuRegBst.class, euRegBetriebsstaetteImportDtoMapper,
                    EuRegBstListXDto.XML_ROOT_ELEMENT_NAME, null, euRegFullBst -> {
                        try {
                            euRegistryImportExportService.importiereEuRegBetriebsstaette(euRegFullBst, berichtsjahr, jobProtokoll);
                            if (count.incrementAndGet() % COUNT_SIZE == 0) {
                                benutzerJobRegistry.jobCountChanged(COUNT_SIZE);
                            }
                        } catch (EuRegistryImportException e) {
                            jobProtokoll.addEintrag(e.getMessage());
                            exceptionCount.incrementAndGet();
                            benutzerJobRegistry.jobError(e);
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                            exceptionCount.incrementAndGet();
                            jobProtokoll.addEintrag(
                                    format("Betriebsstätte %s. Fehler %s", euRegFullBst.entity, e.getMessage()));
                            benutzerJobRegistry.jobError(e);
                        }
                    });

            if(exceptionCount.get() >0) {
                benutzerJobRegistry.jobErrorMultiple(
                        format("%s EuReg-Berichtsdaten werden aufgrund der im Protokoll aufgeführten Fehler nicht importiert.", exceptionCount.get()));
            }

            benutzerJobRegistry.jobCountChanged(count.get() % COUNT_SIZE);
            benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Betriebsstätten: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(format("%s Fehler: %s", JOB_NAME, DateTime.nowCET()), e.getMessage());
        } finally {
            // Datei wird gelöscht, egal ob der Job durchlief oder nicht
            euRegistryImportExportService.dateiLoeschen();

            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }
}
