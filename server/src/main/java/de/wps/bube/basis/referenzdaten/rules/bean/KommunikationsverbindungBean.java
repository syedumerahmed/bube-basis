package de.wps.bube.basis.referenzdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RKOM;

import java.util.Objects;

public class KommunikationsverbindungBean {

    private final ReferenzBean typ;
    private final String verbindung;

    public KommunikationsverbindungBean(ReferenzBean typ, String verbindung) {
        this.typ = typ;
        this.verbindung = verbindung;
    }

    @RegelApi.Member(description = "Typ", referenzliste = RKOM)
    public ReferenzBean getTyp() {
        return typ;
    }

    @RegelApi.Member(description = "Verbindung")
    public String getVerbindung() {
        return verbindung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        KommunikationsverbindungBean that = (KommunikationsverbindungBean) o;
        return typ.equals(that.typ) && verbindung.equals(that.verbindung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typ, verbindung);
    }

    @Override
    public String toString() {
        return "KommunikationsverbindungBean{" +
                "typ=" + typ +
                ", verbindung='" + verbindung + '\'' +
                '}';
    }

}
