package de.wps.bube.basis.referenzdaten.rules.bean;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;

@RegelApi.Type(name = "Behörde")
public class BehoerdeBean {

    private final Long id;
    private final Land land;
    private final String schluessel;
    private final String kreis;
    private final String bezeichnung;
    private final String bezeichnung2;
    private final AdresseBean adresse;
    private final List<KommunikationsverbindungBean> kommunikationsverbindungen;
    private final Set<Zustaendigkeit> zustaendigkeiten;
    private final int gueltigVon;
    private final int gueltigBis;
    private final Instant letzteAenderung;

    public BehoerdeBean(Long id, Land land, String schluessel, String kreis, String bezeichnung, String bezeichnung2,
            AdresseBean adresse, List<KommunikationsverbindungBean> kommunikationsverbindungen,
            Set<Zustaendigkeit> zustaendigkeiten, int gueltigVon, int gueltigBis, Instant letzteAenderung) {
        this.id = id;
        this.land = land;
        this.schluessel = schluessel;
        this.kreis = kreis;
        this.bezeichnung = bezeichnung;
        this.bezeichnung2 = bezeichnung2;
        this.adresse = adresse;
        this.kommunikationsverbindungen = kommunikationsverbindungen;
        this.zustaendigkeiten = zustaendigkeiten;
        this.gueltigVon = gueltigVon;
        this.gueltigBis = gueltigBis;
        this.letzteAenderung = letzteAenderung;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @RegelApi.Member(description = "Land")
    public Land getLand() {
        return land;
    }

    @RegelApi.Member(description = "Schlüssel")
    public String getSchluessel() {
        return schluessel;
    }

    @RegelApi.Member(description = "Kreis")
    public String getKreis() {
        return kreis;
    }

    @RegelApi.Member(description = "Bezeichnung")
    public String getBezeichnung() {
        return bezeichnung;
    }

    @RegelApi.Member(description = "Bezekichnung Teil 2")
    public String getBezeichnung2() {
        return bezeichnung2;
    }

    @RegelApi.Member(description = "Adresse")
    public AdresseBean getAdresse() {
        return adresse;
    }

    @RegelApi.Member(description = "Kommunikation")
    public List<KommunikationsverbindungBean> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    @RegelApi.Member(description = "Zuständigkeiten")
    public Set<Zustaendigkeit> getZustaendigkeiten() {
        return zustaendigkeiten;
    }

    @RegelApi.Member(description = "Gültig von")
    public int getGueltigVon() {
        return gueltigVon;
    }

    @RegelApi.Member(description = "Gültig bis")
    public int getGueltigBis() {
        return gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BehoerdeBean that = (BehoerdeBean) o;
        return gueltigVon == that.gueltigVon && gueltigBis == that.gueltigBis && Objects.equals(id,
                that.id) && land == that.land && Objects.equals(schluessel,
                that.schluessel) && Objects.equals(kreis, that.kreis) && Objects.equals(bezeichnung,
                that.bezeichnung) && Objects.equals(bezeichnung2, that.bezeichnung2) && Objects.equals(
                adresse, that.adresse) && Objects.equals(kommunikationsverbindungen,
                that.kommunikationsverbindungen) && Objects.equals(zustaendigkeiten,
                that.zustaendigkeiten) && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, land, schluessel, kreis, bezeichnung, bezeichnung2, adresse, kommunikationsverbindungen,
                zustaendigkeiten, gueltigVon, gueltigBis, letzteAenderung);
    }

    @Override
    public String toString() {
        return "BehoerdeBean{" +
                "id=" + id +
                ", land=" + land +
                ", schluessel='" + schluessel + '\'' +
                ", kreis='" + kreis + '\'' +
                ", bezeichnung='" + bezeichnung + '\'' +
                ", bezeichnung2='" + bezeichnung2 + '\'' +
                ", adresse=" + adresse +
                ", kommunikationsverbindungen=" + kommunikationsverbindungen +
                ", zustaendigkeiten=" + zustaendigkeiten +
                ", gueltigVon=" + gueltigVon +
                ", gueltigBis=" + gueltigBis +
                ", letzteAenderung=" + letzteAenderung +
                '}';
    }

}
