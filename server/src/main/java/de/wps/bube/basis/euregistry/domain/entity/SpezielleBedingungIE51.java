package de.wps.bube.basis.euregistry.domain.entity;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class SpezielleBedingungIE51 {

    // Referenz auf RSPECC
    @NotNull
    @ManyToOne
    private Referenz art;

    private String info;

    @NotNull
    private String genehmigungUrl;

    @Deprecated
    protected SpezielleBedingungIE51() {
    }

    public SpezielleBedingungIE51(Referenz art, String info, String genehmigungUrl) {
        Assert.notNull(art, "Art muss angegeben werden");
        Assert.notNull(genehmigungUrl, "Genehmigung Url muss angegeben werden");

        this.art = art;
        this.info = info;
        this.genehmigungUrl = genehmigungUrl;
    }

    public Referenz getArt() {
        return art;
    }

    public String getInfo() {
        return info;
    }

    public String getGenehmigungUrl() {
        return genehmigungUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpezielleBedingungIE51 that = (SpezielleBedingungIE51) o;
        return art.equals(that.art) && Objects.equals(info, that.info) && genehmigungUrl.equals(that.genehmigungUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(art, info, genehmigungUrl);
    }
}


