package de.wps.bube.basis.euregistry.rules.bean;

import java.util.Objects;

import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class SpezielleBedingungIE51Bean {

    private final ReferenzBean art;
    private final String info;
    private final String genehmigungUrl;

    public SpezielleBedingungIE51Bean(ReferenzBean art, String info, String genehmigungUrl) {
        this.art = art;
        this.info = info;
        this.genehmigungUrl = genehmigungUrl;
    }

    @RegelApi.Member(description = "Art der speziellen Bedingung (Artikel)", referenzliste = Referenzliste.RSPECC)
    public ReferenzBean getArt() {
        return art;
    }

    @RegelApi.Member(description = "Info")
    public String getInfo() {
        return info;
    }

    @RegelApi.Member(description = "URL zur Genehmigung der speziellen Bedingungen")
    public String getGenehmigungUrl() {
        return genehmigungUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof SpezielleBedingungIE51Bean that))
            return false;
        return Objects.equals(art, that.art) && Objects.equals(info,
                that.info) && Objects.equals(genehmigungUrl, that.genehmigungUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(art, info, genehmigungUrl);
    }

    @Override
    public String toString() {
        return "SpezielleBedingungIE51Bean{" +
                "art=" + art +
                ", info='" + info + '\'' +
                ", genehmigungUrl='" + genehmigungUrl + '\'' +
                '}';
    }

}
