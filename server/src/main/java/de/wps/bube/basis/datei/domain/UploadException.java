package de.wps.bube.basis.datei.domain;

public class UploadException extends RuntimeException {
    public UploadException() {
        super("Fehler beim Hochladen der Datei.");
    }
}
