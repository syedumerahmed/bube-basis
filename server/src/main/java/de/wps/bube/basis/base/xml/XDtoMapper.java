package de.wps.bube.basis.base.xml;

import de.wps.bube.basis.base.mapping.EntityWithError;

public interface XDtoMapper<T, S> {

    EntityWithError<S> fromXDto(T xdto);

    T toXDto(S entity);

}
