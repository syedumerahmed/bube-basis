package de.wps.bube.basis.desktop.web;

import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.ANLAGE;
import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.BETRIEBSSTAETTE;
import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.QUELLE;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTagsEnum;
import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Component
public class BetriebsstaetteTreeNodeMapper implements TreeNodeMapper {

    private final StammdatenService stammdatenService;

    public BetriebsstaetteTreeNodeMapper(StammdatenService stammdatenService) {
        this.stammdatenService = stammdatenService;
    }

    public static TreeNodeDto mapBetriebsstaette(Betriebsstaette b) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setId(b.getId());
        dto.setName(b.getName());
        dto.setNummer(b.getBetriebsstaetteNr());
        dto.setTyp(BETRIEBSSTAETTE);
        String basePath = "betriebsstaette/land/" + b.getLand().getNr() + "/nummer/" + b.getBetriebsstaetteNr();
        dto.setLink(basePath);
        if (b.getLocalId() != null) {
            dto.setQueryParams(Map.of("localId", b.getLocalId()));
        }
        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (b.getLetzteAenderungBetreiber() != null) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        for (Anlage a : b.getAnlagen()) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(BetriebsstaetteTreeNodeMapper.mapAnlage(a, basePath));
        }

        for (Quelle q : b.getQuellen()) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(BetriebsstaetteTreeNodeMapper.mapQuelle(q, basePath));
        }

        return dto;
    }

    private static TreeNodeDto mapAnlage(Anlage a, String basePath) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(a.getName());
        dto.setNummer(a.getAnlageNr());
        dto.setTyp(ANLAGE);
        dto.setLink(basePath + "/anlage/nummer/" + a.getAnlageNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (a.getLetzteAenderungBetreiber() != null) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        for (Anlagenteil t : a.getAnlagenteile()) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(BetriebsstaetteTreeNodeMapper.mapAnlagenteil(t, dto.getLink()));
        }

        for (Quelle q : a.getQuellen()) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(BetriebsstaetteTreeNodeMapper.mapQuelle(q, dto.getLink()));
        }

        return dto;
    }

    private static TreeNodeDto mapQuelle(Quelle q, String basePath) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(q.getName());
        dto.setNummer(q.getQuelleNr());
        dto.setTyp(QUELLE);
        dto.setLink(basePath + "/quelle/nummer/" + q.getQuelleNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (q.getLetzteAenderungBetreiber() != null) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        return dto;
    }

    private static TreeNodeDto mapAnlagenteil(Anlagenteil a, String basePath) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(a.getName());
        dto.setNummer(a.getAnlagenteilNr());
        dto.setTyp(ANLAGE);
        dto.setLink(basePath + "/anlagenteil/nummer/" + a.getAnlagenteilNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (a.getLetzteAenderungBetreiber() != null) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        return dto;
    }

    @Override
    public DesktopTyp getTyp() {
        return DesktopTyp.STAMMDATEN;
    }

    @Override
    public Optional<TreeNodeDto> map(DesktopItem item) {
        return stammdatenService.findBetriebsstaette(item.getDesktopItemId().getItemId())
                                .map(BetriebsstaetteTreeNodeMapper::mapBetriebsstaette);
    }

}
