package de.wps.bube.basis.desktop.web;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum;
import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Component
public class EuBerichtTreeNodeMapper implements TreeNodeMapper {

    private final EURegistryService eURegistryService;

    @Override
    public DesktopTyp getTyp() {
        return DesktopTyp.EUREG;
    }

    @Override
    public Optional<TreeNodeDto> map(DesktopItem item) {
        return eURegistryService.findBetriebsstaettenBericht(item.getDesktopItemId().getItemId())
                                .map(this::mapEURegBetriebsstaette);
    }

    public EuBerichtTreeNodeMapper(EURegistryService eURegistryService) {
        this.eURegistryService = eURegistryService;
    }

    public TreeNodeDto mapEURegBetriebsstaette(EURegBetriebsstaette bericht) {
        Betriebsstaette b = bericht.getBetriebsstaette();
        TreeNodeDto dto = new TreeNodeDto();
        dto.setChildren(new ArrayList<>());
        dto.setId(bericht.getId());
        dto.setName(b.getName());
        dto.setNummer(b.getBetriebsstaetteNr());
        dto.setTyp(TreeNodeTypEnum.EUREG_BST);
        String basePath = "betriebsstaette/land/" + b.getLand().getNr() + "/nummer/" + b.getBetriebsstaetteNr();
        dto.setLink(basePath);
        if(b.getLocalId() != null) {
            dto.setQueryParams(Map.of("localId", b.getLocalId()));
        }

        for (Anlage a : b.getAnlagen()) {
            mapAnlage(a, basePath, dto);
        }

        return dto;
    }

    private void mapAnlage(Anlage a, String basePath, TreeNodeDto parentDto) {
        TreeNodeDto dto = new TreeNodeDto();
        dto.setLink(basePath + "/anlage/nummer/"+ a.getAnlageNr());

        Optional<EURegAnlage> berichtOpt = eURegistryService.findAnlagenBerichtByAnlageId(a.getId());

        if (berichtOpt.isPresent()) {

            if (berichtOpt.get().isEURegAnl()) {
                dto.setTyp(TreeNodeTypEnum.EUREG_ANL);
            } else {
                dto.setTyp(TreeNodeTypEnum.EUREG_F);
            }
            dto.setName(a.getName());
            dto.setNummer(a.getAnlageNr());
            parentDto.getChildren().add(dto);
        }

        for (Anlagenteil anlagenteil : a.getAnlagenteile()) {
            mapAnlagenteil(anlagenteil, dto.getLink(), parentDto);
        }
    }

    private void mapAnlagenteil(Anlagenteil anlagenteil, String basePath, TreeNodeDto parentDto) {
        TreeNodeDto dto = new TreeNodeDto();
        Optional<EURegAnlage> berichtOpt = eURegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId());

        if (berichtOpt.isPresent()) {

            if (berichtOpt.get().isEURegAnl()) {
                dto.setTyp(TreeNodeTypEnum.EUREG_ANL);
            } else {
                dto.setTyp(TreeNodeTypEnum.EUREG_F);
            }
            dto.setName(anlagenteil.getName());
            dto.setNummer(anlagenteil.getAnlagenteilNr());
            dto.setLink(basePath + "/anlagenteil/nummer/"+anlagenteil.getAnlagenteilNr());

            parentDto.getChildren().add(dto);
        }
    }
}
