package de.wps.bube.basis.desktop.web;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.DesktopApi;
import de.wps.bube.basis.base.web.openapi.model.SpeichereItemsAufDesktop;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.desktop.domain.service.DesktopService;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Controller
class DesktopRestController implements DesktopApi {

    private final DesktopService desktopService;
    private final AktiverBenutzer aktiverBenutzer;
    private final Map<DesktopTyp, ? extends TreeNodeMapper> mapperMap;

    public DesktopRestController(DesktopService desktopService,
            AktiverBenutzer aktiverBenutzer,
            Collection<TreeNodeMapper> mappers) {
        this.desktopService = desktopService;
        this.aktiverBenutzer = aktiverBenutzer;
        mapperMap = mappers.stream().collect(Collectors.toMap(TreeNodeMapper::getTyp, Function.identity()));
    }

    @Override
    public ResponseEntity<List<TreeNodeDto>> readDesktop(String typ, String jahrSchluessel) {
        var mapper = mapperMap.get(DesktopTyp.byName(typ));
        if (mapper == null) {
            throw new IllegalArgumentException("No mapper present for DesktopTyp " + typ);
        }

        return ResponseEntity.ok(
                desktopService.readDesktop(aktiverBenutzer.getUsername(), jahrSchluessel, DesktopTyp.byName(typ))
                              .stream()
                              .map(mapper::map)
                              .flatMap(Optional::stream)
                              .toList());
    }

    @Override
    public ResponseEntity<Void> saveDesktopItems(String typ, SpeichereItemsAufDesktop dto) {
        desktopService.saveDesktopItems(aktiverBenutzer.getUsername(), dto.getJahrSchluessel(), dto.getIds(),
                DesktopTyp.byName(typ));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteDesktopItem(String typ, Long id) {
        desktopService.deleteDesktopItem(aktiverBenutzer.getUsername(), id, DesktopTyp.byName(typ));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteDesktop(String typ, String jahrSchluessel) {
        desktopService.deleteDesktop(aktiverBenutzer.getUsername(), jahrSchluessel, DesktopTyp.byName(typ));
        return ResponseEntity.ok().build();
    }

}
