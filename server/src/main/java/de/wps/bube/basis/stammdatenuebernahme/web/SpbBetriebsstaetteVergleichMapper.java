package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteVergleichDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetriebsstaetteVergleich;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbBetriebsstaetteVergleichMapper {

    @Mapping(target = "allgemein", source = ".")
    public abstract SpbBetriebsstaetteVergleichDto toDto(SpbBetriebsstaetteVergleich spbBetriebsstaetteVergleich);
}
