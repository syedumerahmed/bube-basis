package de.wps.bube.basis.referenzdaten.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

/**
 * Die vom Land vorgegebenen Steuerungsparameter (RPARA)
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class ParameterWert {

    public static final String QUELLE_ZUORDNUNG_ANLAGE = "Anlage";
    public static final String QUELLE_ZUORDNUNG_BST = "BST";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "parameter_wert_seq")
    @SequenceGenerator(name = "parameter_wert_seq", allocationSize = 50)
    private Long id;

    @NotNull
    private Land land;

    // Identifikation (Schlüssel) des Parameters
    @NotNull
    private String pschluessel;

    @NotNull
    private String wert;

    // Gilt ab 1.1.yyyy bezogen auf Berichtsjahr, min. 1900
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_von"))
    private Gueltigkeitsjahr gueltigVon;

    // Gilt bis 31.12.yyyy bezogen auf Berichtsjahr, max. 9999
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_bis"))
    private Gueltigkeitsjahr gueltigBis;

    @ManyToOne
    private Behoerde behoerde;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected ParameterWert() {
    }

    public ParameterWert(Long id, Land land, String pschluessel, String wert, Gueltigkeitsjahr gueltigVon,
            Gueltigkeitsjahr gueltigBis, Behoerde behoerde, Instant letzteAenderung) {
        this.id = id;
        this.land = land;
        this.pschluessel = pschluessel;
        this.wert = wert;
        this.gueltigVon = gueltigVon;
        this.gueltigBis = gueltigBis;
        this.letzteAenderung = letzteAenderung;
        this.behoerde = behoerde;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public String getPschluessel() {
        return pschluessel;
    }

    public void setPschluessel(String pschluessel) {
        this.pschluessel = pschluessel;
    }

    public String getWert() {
        return wert;
    }

    public void setWert(String wert) {
        this.wert = wert;
    }

    public Gueltigkeitsjahr getGueltigVon() {
        return gueltigVon;
    }

    public void setGueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
    }

    public Gueltigkeitsjahr getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public Behoerde getBehoerde() {
        return behoerde;
    }

    public void setBehoerde(Behoerde behoerde) {
        this.behoerde = behoerde;
    }

    public void pruefeDaten() {
        if (gueltigBis.liegtVor(gueltigVon)) {
            throw new ReferenzdatenException("Gültig bis liegt zeitlich vor Gültig von");
        }
        if (Land.DEUTSCHLAND.equals(land) && behoerde != null) {
            throw new ReferenzdatenException("Bundeseinheitlichen Referenzen können keine Behörden zugeordnet werden");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParameterWert that = (ParameterWert) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getBehoerdeId() {
        return behoerde != null ? behoerde.getId() : null;
    }
}
