package de.wps.bube.basis.stammdaten.domain.service;

import java.util.Arrays;

import org.springframework.stereotype.Component;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;
import de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;
import de.wps.bube.basis.stammdaten.security.SecuredBehoerdennutzer;

@Component
public class SuchattributePredicateFactory {

    @SecuredBehoerdennutzer
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        if (suchattribute == null) {
            return null;
        }
        return Arrays.stream(SuchattributKonfiguration.values())
                     .map(col -> col.createPredicate(suchattribute, bstRoot))
                     .reduce(new BooleanBuilder(), BooleanBuilder::and, BooleanBuilder::and);
    }

}
