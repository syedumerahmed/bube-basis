package de.wps.bube.basis.stammdaten.domain;

public class InvalidTaetigkeitException extends IllegalArgumentException {
    public InvalidTaetigkeitException(String vorschriftSchluessel) {
        super(String.format("Die Vorschrift mit dem Schlüssel %s hat keine valide Tätigkeit oder darf keine Tätigkeit haben!",
                vorschriftSchluessel));
    }
}
