package de.wps.bube.basis.referenzdaten.domain.service;

import java.util.List;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdeGeloeschtEvent;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Service
@Transactional
public class BehoerdenLoeschenService {

    private final BehoerdenService behoerdenService;
    private final BehoerdenRepository behoerdenRepository;
    private final ParameterService parameterService;
    private final ApplicationEventPublisher eventPublisher;

    public BehoerdenLoeschenService(BehoerdenService behoerdenService, BehoerdenRepository behoerdenRepository,
            ParameterService parameterService, ApplicationEventPublisher eventPublisher) {
        this.behoerdenService = behoerdenService;
        this.behoerdenRepository = behoerdenRepository;
        this.parameterService = parameterService;
        this.eventPublisher = eventPublisher;
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_DELETE)
    public void deleteBehoerde(Long id) {
        var behoerde = behoerdenService.readBehoerdeForDelete(id);
        eventPublisher.publishEvent(new BehoerdeGeloeschtEvent(behoerde));
        checkBehoerdeUsed(behoerde);

        try {
            behoerdenRepository.deleteById(id);
            behoerdenRepository.flush(); // Damit hier die Exception ausgelöst wird
        } catch (DataIntegrityViolationException e) {
            throw new ReferenzdatenException("Behörde kann nicht gelöscht werden, da sie bereits verwendet wird.");
        } catch (DataAccessException e) {
            throw new ReferenzdatenException(
                    "Behörde kann nicht gelöscht werden, obwohl sie nicht mehr verwendet wird.");
        }
    }

    void checkBehoerdeUsed(Behoerde behoerde) {
        if (parameterService.behoerdeUsedForBehoerdeLoeschen(behoerde)) {
            throw new ReferenzdatenException(
                    "Die Behörde wird noch in den Parametern verwendet und kann nicht gelöscht werden.");
        }
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_DELETE)
    public void deleteMultipleBehoerden(List<Long> ids) {
        ids.forEach(this::deleteBehoerde);
    }
}
