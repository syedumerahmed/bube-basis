package de.wps.bube.basis.euregistry.domain.service;

import java.io.IOException;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.List;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.xml.bericht.EURegGMLWriterProtokoll;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

public class TeilberichtProtokoll {

    public static final String SEPARATOR_LINE = "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";

    public enum Status {

        ABGABEFAEHIG("XEUR ist abgabefähig."),
        KOMPLEXPRUEFUNG_FEHLERHAFT("XEUR ist nicht abgabefähig, weil die Komplexprüfung Fehler entdeckt hat."),
        GLOBALE_FEHLER("XEUR ist nicht abgabefähig wegen globaler Fehler."),
        SCHEMAVERLETZUNG("XEUR ist nicht abgabefähig wegen Schemaverletzungen.");

        private final String display;

        Status(String display) {
            this.display = display;
        }
    }

    private static final String PROTOKOLL_SUMMARY = """
            Es {0,choice,0#wurde keine|1#wurde 1|1<wurden {0}} {0,choice,1#Betriebsstätte|1<Betriebsstätten} geprüft.
            In {1,choice,0#0 Betriebsstätten|1#1 Betriebsstätte|1<{1} Betriebsstätten} {2,choice,0#traten|1#trat|1<traten} insgesamt {2} Fehler auf.
            {6,choice,0#Keine|0<{6}} {6,choice,1#Betriebsstätte|1<Betriebsstätten}, die im Vorjahr berichtet {6,choice,1#wurde|1<wurden}, {6,choice,1#fehlt|1<fehlen}.
            In {3,choice,0#0 Betriebsstätten|1#1 Betriebsstätte|1<{3} Betriebsstätten} {4,choice,0#traten|1#trat|1<traten} insgesamt {4} Hinweise auf.
            {5,choice,0#Keine|0<{5}} {5,choice,1#Betriebsstätte hat|1<Betriebsstätten haben} die Prüfung ohne Fehler bestanden.""";

    private static String display(Betriebsstaette betriebsstaette) {
        return "Betriebsstätte: " + betriebsstaette.getBetriebsstaetteNr() + " | " + betriebsstaette.getName();
    }

    private final String land;
    private final String jahr;
    private final String start;

    private final KomplexpruefungService komplexpruefungService;
    private final EURegistryService euRegistryService;

    private Status status = Status.ABGABEFAEHIG;

    private long komplexpruefungenGesamt = 0;
    private long komplexpruefungenFehlerhaft = 0;
    private long komplexpruefungenFehler = 0;
    private long komplexpruefungenMitHinweisen = 0;
    private long komplexpruefungHinweise = 0;
    private long komplexpruefungenFehlerfrei = 0;
    private long komplexpruefungenFehlend = 0;

    private final StringWriter komplexpruefungenDetails = new StringWriter();

    private EURegGMLWriterProtokoll euRegGMLWriterProtokoll;

    public TeilberichtProtokoll(String land, String jahr, KomplexpruefungService komplexpruefungService,
            EURegistryService euRegistryService) {
        this.land = land;
        this.jahr = jahr;
        this.komplexpruefungService = komplexpruefungService;
        this.euRegistryService = euRegistryService;
        this.start = DateTime.nowCET();
    }

    public Status getStatus() {
        return status;
    }

    public void addKomplexpruefung(EURegBetriebsstaette bericht, Komplexpruefung komplexpruefung) throws IOException {
        komplexpruefungenGesamt++;
        if (komplexpruefung.hatFehler()) {
            komplexpruefungenFehlerhaft++;
            komplexpruefungenFehler += komplexpruefung.getAnzahlFehler();
            status = Status.KOMPLEXPRUEFUNG_FEHLERHAFT;
        } else {
            komplexpruefungenFehlerfrei++;
        }
        if (komplexpruefung.getAnzahlWarnungen() > 0) {
            komplexpruefungenMitHinweisen++;
            komplexpruefungHinweise += komplexpruefung.getAnzahlWarnungen();
        }
        if (komplexpruefung.hatFehler() || komplexpruefung.getAnzahlWarnungen() > 0) {
            BerichtPruefungProtokollGenerator generator = new BerichtPruefungProtokollGenerator(
                    bericht.getBetriebsstaette(), bericht, bericht.getKomplexpruefung(), komplexpruefungService,
                    euRegistryService);
            generator.setProtokolliereHeader(false);
            generator.protokolliereBetriebsstaette(komplexpruefungenDetails);
            komplexpruefungenDetails.append("\n\n");
        }
    }

    public void setKomplexpruefungenFehlend(long komplexpruefungenFehlend) {
        this.komplexpruefungenFehlend = komplexpruefungenFehlend;
    }

    public void addTeilberichtPruefungErgebnisse(List<BerichtPruefungService.TestErgebnis> teilberichtErgebnisse) {
        for (BerichtPruefungService.TestErgebnis ergebnis : teilberichtErgebnisse) {
            komplexpruefungenDetails.append(display(ergebnis.betriebsstaette)).append("\n")
                                    .append(ergebnis.message).append("\n\n");
            status = Status.KOMPLEXPRUEFUNG_FEHLERHAFT;
        }
    }

    public void setEURegGMLWriterProtokoll(EURegGMLWriterProtokoll euRegGMLWriterProtokoll) {
        this.euRegGMLWriterProtokoll = euRegGMLWriterProtokoll;
        if (euRegGMLWriterProtokoll.hasValidationErrors()) {
            status = Status.SCHEMAVERLETZUNG;
        } else if (euRegGMLWriterProtokoll.hasMappingErrors()) {
            status = Status.GLOBALE_FEHLER;
        }
    }

    public String getProtokoll() {
        StringWriter writer = new StringWriter();

        // Überschrift
        writer.append("LAND-XEUR-Teilbericht Protokoll  Land: ").append(land).append("  Zieljahr: ");
        writer.append(jahr).append("  Start: ").append(start).append("\n\n");

        // Status
        writer.append(status.display).append("\n\n");

        // Summarische Angaben
        writer.append(SEPARATOR_LINE).append("\nPrüfstatistik\n").append(SEPARATOR_LINE).append("\n\n");
        writer.append(MessageFormat.format(PROTOKOLL_SUMMARY, komplexpruefungenGesamt, komplexpruefungenFehlerhaft,
                komplexpruefungenFehler, komplexpruefungenMitHinweisen, komplexpruefungHinweise,
                komplexpruefungenFehlerfrei, komplexpruefungenFehlend)).append("\n\n");

        // Übernommene Betriebsstätten und AnlAn
        writer.append(SEPARATOR_LINE).append("\nBetriebe ohne Fehler in der Komplexprüfung\n").append(SEPARATOR_LINE)
              .append("\n\n");
        euRegGMLWriterProtokoll.display(writer);

        // Fehlerhafte Betriebsstätten und AnlAn
        if (komplexpruefungenFehlerhaft > 0 || komplexpruefungenMitHinweisen > 0 || komplexpruefungenFehlend > 0) {
            writer.append(SEPARATOR_LINE).append("\nFehler und Warnungen in Komplexprüfungs-Protokollen\n")
                  .append(SEPARATOR_LINE).append("\n\n");
            writer.append(komplexpruefungenDetails.toString());
        }

        // Globale Fehler
        if (euRegGMLWriterProtokoll.hasMappingErrors()) {
            writer.append(SEPARATOR_LINE).append("\nGlobale Fehler\n").append(SEPARATOR_LINE).append("\n\n");
            euRegGMLWriterProtokoll.displayMappingErrors(writer);
            writer.append("\n");
        }

        // Schemaverletzungen
        if (euRegGMLWriterProtokoll.hasValidationErrors()) {
            writer.append(SEPARATOR_LINE).append("\nSchemaverletzungen\n").append(SEPARATOR_LINE).append("\n\n");
            euRegGMLWriterProtokoll.displayValidationErrors(writer);
        }

        return writer.toString();
    }

}
