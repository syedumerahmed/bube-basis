package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.function.Function;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public class BooleanFilter implements Filter {
    private final Function<QBetriebsstaette, BooleanExpression> relativeExpression;
    private final Function<Suchattribute, Boolean> extractor;

    public BooleanFilter(Function<QBetriebsstaette, BooleanExpression> relativeExpression,
            Function<Suchattribute, Boolean> extractor) {
        this.relativeExpression = relativeExpression;
        this.extractor = extractor;
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        final var booleanValue = extractor.apply(suchattribute);
        return booleanValue != null ? relativeExpression.apply(bstRoot).eq(booleanValue) : null;
    }
}
