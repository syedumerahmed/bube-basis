package de.wps.bube.basis.benutzerverwaltung.domain.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerImportJobStatus;
import de.wps.bube.basis.benutzerverwaltung.domain.event.BenutzerImportTriggeredEvent;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;

@Component
@Transactional
public class BenutzerImportierenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BenutzerImportierenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final BenutzerImportierenJob job;

    public BenutzerImportierenListener(BenutzerJobRegistry jobRegistry, BenutzerImportierenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleBenutzerImportieren(BenutzerImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", BenutzerImportTriggeredEvent.EVENT_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new BenutzerImportJobStatus());

        job.runAsync(event.username);
    }
}
