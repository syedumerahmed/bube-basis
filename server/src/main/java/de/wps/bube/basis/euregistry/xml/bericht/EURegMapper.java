package de.wps.bube.basis.euregistry.xml.bericht;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionFacility;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionInstallation;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.FeatureMember;

@Mapper(uses = { ProductionSiteMapper.class,
        ProductionFacilityMapper.class,
        ProductionInstallationMapper.class
})
public abstract class EURegMapper {

    public Stream<FeatureMember> toXDto(EURegBetriebsstaette euRegBst,
            Stream<EURegAnlage> euRegAnlagen,
            Supplier<Long> productionSiteIdSupplier,
            ReportData reportData, InspireParam inspireParam, XeurProperties.UbaProperties ubaProperties,
            List<String> anlageLocalIds) {
        var mappingContext = new EURegMappingContext(euRegBst.getBetriebsstaette().getLand(), productionSiteIdSupplier,
                reportData, inspireParam, ubaProperties, anlageLocalIds);
        var featureMembers = createFeatureMembers(euRegBst, euRegAnlagen, mappingContext);

        return Stream.concat(
                Stream.of(new FeatureMember(featureMembers.productionSite),
                        new FeatureMember(featureMembers.productionFacility)),
                featureMembers.productionInstallations.flatMap(this::streamFeatureMembers)
        );

    }

    private Stream<FeatureMember> streamFeatureMembers(ProductionInstallation productionInstallation) {
        if (productionInstallation.getProductionInstallationPart() != null) {
            return Stream.of(new FeatureMember(productionInstallation),
                    new FeatureMember(productionInstallation.getProductionInstallationPart()));
        } else {
            return Stream.of(new FeatureMember(productionInstallation));
        }
    }

    @Mapping(target = "productionSite", source = "euRegBst")
    @Mapping(target = "productionFacility", source = "euRegBst", dependsOn = "productionSite")
    @Mapping(target = "productionInstallations", source = "euRegAnlagen", dependsOn = "productionFacility")
    abstract FeatureMembers createFeatureMembers(EURegBetriebsstaette euRegBst,
            Stream<EURegAnlage> euRegAnlagen,
            @Context EURegMappingContext mappingContext);

    static class FeatureMembers {
        public ProductionSite productionSite;
        public ProductionFacility productionFacility;
        public Stream<ProductionInstallation> productionInstallations;
    }

    static final class EURegMappingContext implements
            ProductionSiteMapper.ProductionSiteMapperContext,
            ProductionFacilityMapper.ProductionFacilityMapperContext,
            ProductionInstallationMapperContext {

        private final Land land;
        private final Supplier<Long> productionSiteIdSupplier;
        private final ReportData reportData;
        private final InspireParam inspireParam;
        private final XeurProperties.UbaProperties ubaProperties;
        private final List<String> anlageLocalIds;

        private ProductionSite productionSite;

        EURegMappingContext(Land land, Supplier<Long> productionSiteIdSupplier, ReportData reportData,
                InspireParam inspireParam, XeurProperties.UbaProperties ubaProperties,
                List<String> anlageLocalIds) {
            this.land = land;
            this.productionSiteIdSupplier = productionSiteIdSupplier;
            this.reportData = reportData;
            this.inspireParam = inspireParam;
            this.ubaProperties = ubaProperties;
            this.anlageLocalIds = anlageLocalIds;
        }

        @Override
        public void afterProductionSite(ProductionSite productionSite) {
            this.productionSite = productionSite;
        }

        @Override
        public ReportData getReportData() {
            return reportData;
        }

        @Override
        public String createProductionSiteId() {
            return "_" + land.getNr() + "_" + productionSiteIdSupplier.get() + ".SITE";
        }

        @Override
        public ProductionSite getProductionSite() {
            return productionSite;
        }

        @Override
        public XeurProperties.UbaProperties getUbaProperties() {
            return ubaProperties;
        }

        @Override
        public InspireParam getInspireParam() {
            return inspireParam;
        }

        public List<String> getAnlageLocalIds() {
            return anlageLocalIds;
        }
    }
}
