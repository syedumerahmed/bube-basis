package de.wps.bube.basis.stammdaten.domain;

import de.wps.bube.basis.base.persistence.DuplicateKeyException;

public class AnlageDuplicateKeyException extends DuplicateKeyException {
    public AnlageDuplicateKeyException(String message) {
        super(message);
    }

    public static AnlageDuplicateKeyException forAnlageNr(String nummer) {
        return new AnlageDuplicateKeyException(String.format("Eine Anlage mit der Nummer '%s' existiert bereits.", nummer));
    }
}
