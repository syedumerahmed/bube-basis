package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class EPRTRAnnexIActivity {
    @XmlElement(required = true, name = "EPRTRAnnexIActivityType")
    private final EPRTRAnnexIActivityType details = new EPRTRAnnexIActivityType();

    public ReferenceType getMainActivity() {
        return details.mainActivity;
    }

    public void setMainActivity(ReferenceType mainActivity) {
        details.mainActivity = mainActivity;
    }

    public List<ReferenceType> getOtherActivity() {
        return details.otherActivity;
    }

    public void setOtherActivity(List<ReferenceType> otherActivity) {
        details.otherActivity = otherActivity;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class EPRTRAnnexIActivityType {
        @XmlElement(required = true, name = "mainActivity")
        ReferenceType mainActivity;

        @XmlElement(name = "otherActivity")
        List<ReferenceType> otherActivity;
    }
}
