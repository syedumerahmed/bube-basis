@XmlSchema(
        namespace = SchemaSet.SCHEMA_GML,
        xmlns = {
                // TODO eigentlich nicht nötig, da namespace schon an der FeatureCollection
                //  deklariert wird. JAXB ignoriert das aber scheinbar für xsi:nil
                @XmlNs(prefix = "xsi", namespaceURI = SchemaSet.SCHEMA_XSI)
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package de.wps.bube.basis.euregistry.xml.bericht.dto.gml;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
