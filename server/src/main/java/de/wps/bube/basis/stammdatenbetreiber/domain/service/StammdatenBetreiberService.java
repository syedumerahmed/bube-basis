package de.wps.bube.basis.stammdatenbetreiber.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.*;
import static de.wps.bube.basis.stammdatenbetreiber.domain.entity.QSpbBetreiber.spbBetreiber;
import static de.wps.bube.basis.stammdatenbetreiber.domain.entity.QSpbQuelle.spbQuelle;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.AnlageDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.AnlagenteilDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.BetriebsstaetteDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.QuelleDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetreiberGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaetteGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.service.SucheService;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenRead;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlageListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlagenteilListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbQuelleListItem;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbAnlageRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbAnlagenteilRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbBetreiberRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbBetriebsstaetteRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbQuelleRepository;

@Service
public class StammdatenBetreiberService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StammdatenBetreiberService.class);

    private final SpbBetriebsstaetteRepository spbBetriebsstaetteRepository;
    private final SpbAnlageRepository spbAnlageRepository;
    private final SpbAnlagenteilRepository spbAnlagenteilRepository;
    private final SpbBetreiberRepository spbBetreiberRepository;
    private final SpbQuelleRepository spbQuelleRepository;
    private final SucheService sucheService;
    private final StammdatenService stammdatenService;
    private final UpdateStammdatenDurchBetreiberAenderungService aenderungService;

    private final SpbBetriebsstaetteUpdater spbBetriebsstaetteUpdater;

    public StammdatenBetreiberService(SpbBetriebsstaetteRepository spbBetriebsstaetteRepository,
            SpbAnlageRepository spbAnlageRepository,
            SpbAnlagenteilRepository spbAnlagenteilRepository,
            SpbBetreiberRepository spbBetreiberRepository,
            SpbQuelleRepository spbQuelleRepository,
            SucheService sucheService, StammdatenService stammdatenService,
            UpdateStammdatenDurchBetreiberAenderungService aenderungService,
            SpbBetriebsstaetteUpdater spbBetriebsstaetteUpdater) {
        this.spbBetriebsstaetteRepository = spbBetriebsstaetteRepository;
        this.spbAnlageRepository = spbAnlageRepository;
        this.spbAnlagenteilRepository = spbAnlagenteilRepository;
        this.spbBetreiberRepository = spbBetreiberRepository;
        this.spbQuelleRepository = spbQuelleRepository;
        this.sucheService = sucheService;
        this.stammdatenService = stammdatenService;
        this.aenderungService = aenderungService;
        this.spbBetriebsstaetteUpdater = spbBetriebsstaetteUpdater;
    }

    /**
     * Betriebsstätte
     */
    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public Page<BetriebsstaetteListItemView> suche(Long berichtsjahr, Pageable page) {
        Page<BetriebsstaetteListItemView> suchePage = sucheService.suche(berichtsjahr, null, page);

        // Gibt es Änderungen an der Betriebsstätte durch den Betreiber?
        suchePage.forEach(item -> {
            Optional<SpbBetriebsstaette> spbBetriebsstaetteOpt = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(
                    item.id);
            if (spbBetriebsstaetteOpt.isPresent()) {
                var spb = spbBetriebsstaetteOpt.get();
                item.name = spb.getName();
                item.betriebsstatus = spb.getBetriebsstatus();
                item.ort = spb.getAdresse().getOrt();
                item.plz = spb.getAdresse().getPlz();
                item.strasse = spb.getAdresse().getStrasse();
                item.hausNr = spb.getAdresse().getHausNr();
            }
        });

        return suchePage;
    }

    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public List<Long> sucheIds(Long berichtsjahrId) {
        return sucheService.sucheIds(berichtsjahrId, new Suchattribute());
    }

    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public SpbBetriebsstaette loadSpbBetriebsstaetteByJahrLandNummer(String jahrSchluessel, Land land, String nummer) {

        // Berechtigungsprüfung erfolgt im StammdatenService
        Betriebsstaette bst = stammdatenService.loadBetriebsstaetteByJahrLandNummer(jahrSchluessel, land, nummer);

        Optional<SpbBetriebsstaette> spbBetriebsstaetteOpt = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(
                bst.getId());
        return spbBetriebsstaetteOpt.orElseGet(() -> new SpbBetriebsstaette(bst));
    }

    @Transactional
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbBetriebsstaette loadSpbBetriebsstaette(Long id) {
        SpbBetriebsstaette spbBetriebsstaette = this.spbBetriebsstaetteRepository.getById(id);
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(spbBetriebsstaette.getBetriebsstaette().getId());
        return spbBetriebsstaette;
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbBetriebsstaette updateSpbBetriebsstaette(SpbBetriebsstaette spbBetriebsstaette) {
        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(
                spbBetriebsstaette.getBetriebsstaette().getId()));

        Long spbBstId = spbBetriebsstaette.getId();
        Assert.notNull(spbBstId, "SPB Betriebsstätte nicht vorhanden");
        SpbBetriebsstaette existing = spbBetriebsstaetteRepository
                .findById(spbBstId)
                .orElseThrow(() -> new BubeEntityNotFoundException(SpbBetriebsstaette.class, "ID " + spbBstId));

        spbBetriebsstaetteUpdater.update(existing, spbBetriebsstaette);

        aenderungService.updateAenderungDurchBetreiber(spbBetriebsstaette);
        return spbBetriebsstaetteRepository.save(existing);
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbBetriebsstaette createSpbBetriebsstaette(SpbBetriebsstaette spbBetriebsstaette) {

        Assert.isNull(spbBetriebsstaette.getId(), "SPB Betriebsstätte bereits vorhanden");
        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(
                spbBetriebsstaette.getBetriebsstaette().getId()));
        checkSpbBetriebsstaetteFunctionalKeyExists(spbBetriebsstaette);

        aenderungService.updateAenderungDurchBetreiber(spbBetriebsstaette);
        spbBetriebsstaette.setErsteErfassung(spbBetriebsstaette.getBetriebsstaette().getErsteErfassung());
        return spbBetriebsstaetteRepository.save(spbBetriebsstaette);
    }

    private void checkSpbBetriebsstaetteFunctionalKeyExists(SpbBetriebsstaette spbBetriebsstaette) {
        Betriebsstaette betriebsstaette = spbBetriebsstaette.getBetriebsstaette();
        if (spbBetriebsstaetteRepository.existsByBetriebsstaetteId(betriebsstaette.getId())) {
            throw new BetriebsstaetteDuplicateKeyException(betriebsstaette.getBerichtsjahr().getSchluessel(),
                    betriebsstaette.getBetriebsstaetteNr());
        }
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void deleteSpbBetriebsstaetteUebernahme(SpbBetriebsstaette spbBetriebsstaette) {
        spbBetriebsstaetteRepository.delete(spbBetriebsstaette);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    @Transactional
    public void handleBetriebsstaetteGeloeschtEvent(BetriebsstaetteGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", BetriebsstaetteGeloeschtEvent.EVENT_NAME);

        spbAnlageRepository.findByParentBetriebsstaetteId(event.betriebsstaetteId)
                           .forEach(this::deleteSpbAnlage);

        spbQuelleRepository.deleteAll(spbQuelleRepository.findByParentBetriebsstaetteId(event.betriebsstaetteId));

        spbBetriebsstaetteRepository
                .findOneByBetriebsstaetteId(event.betriebsstaetteId)
                .ifPresent(spbBetriebsstaetteRepository::delete);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, LAND_READ, BEHOERDE_READ })
    public Optional<SpbBetriebsstaette> findSpbBetriebsstaetteByBetriebsstaetteId(Long id) {
        Assert.notNull(id, "BetriebsstaetteId ist nicht vorhanden");

        Optional<SpbBetriebsstaette> bstOpt = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(id);
        if (bstOpt.isPresent()) {
            stammdatenService.checkBetriebsstaetteDatenberechtigungRead(id);
        }
        return bstOpt;
    }

    /**
     * Anlage
     */
    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public List<SpbAnlageListItem> listAllSpbAnlagenAsListItemByBetriebsstaettenId(Long betriebsstaetteId) {

        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        List<Long> anlageIdList = new ArrayList<>();
        List<SpbAnlageListItem> spbAnlagen = new ArrayList<>();

        for (SpbAnlage spbAnlage : spbAnlageRepository.findByParentBetriebsstaetteId(betriebsstaetteId)) {
            if (spbAnlage.hasAnlage()) {
                anlageIdList.add(spbAnlage.getAnlageId());
            }
            spbAnlagen.add(new SpbAnlageListItem(
                    spbAnlage.getId(),
                    spbAnlage.getAnlageNr(),
                    spbAnlage.getName(),
                    spbAnlage.getBetriebsstatus(), spbAnlage.hasAnlage(), spbAnlage.getAnlageId()));
        }

        // Die Anlagen welche noch im "Originalzustand" sind hinzufügen
        for (Anlage anlage : stammdatenService.loadAllAnlagenByBetriebsstaettenId(betriebsstaetteId)) {
            if (!anlageIdList.contains(anlage.getId())) {
                spbAnlagen.add(new SpbAnlageListItem(null,
                        anlage.getAnlageNr(), anlage.getName(),
                        anlage.getBetriebsstatus(), false, anlage.getId()));
            }
        }

        return spbAnlagen;
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public void aktualisiereSpbNachAnlageUebernahme(Long anlageId, Long spbAnlageId) {
        SpbAnlage spbAnlage = loadSpbAnlage(spbAnlageId);

        // Neu Erstellte SPB-Anlagenteile und SPB-Quellen müssen von der SPB-Anlage an die Anlage umgehängt werden,
        // damit sie weiterhin als neue Objekte angezeigt werden.
        List<SpbAnlagenteil> anlagenteile = spbAnlagenteilRepository.findByParentSpbAnlageId(spbAnlage.getId());
        List<SpbQuelle> quellen = spbQuelleRepository.findByParentSpbAnlageId(spbAnlage.getId());

        anlagenteile.forEach(a -> {
            a.setParentSpbAnlageId(null);
            a.setParentAnlageId(anlageId);
            updateSpbAnlagenteil(a);
        });

        quellen.forEach(q -> {
            q.setParentAnlageId(anlageId);
            q.setParentSpbAnlageId(null);
            updateSpbQuelle(q);
        });

        deleteUebernommeneSpbAnlage(spbAnlage);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public List<SpbAnlageListItem> listAllNeueSpbAnlagenAsListItemByBetriebsstaettenId(Long betriebsstaetteId) {
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        return spbAnlageRepository.findByParentBetriebsstaetteIdAndAnlageIdIsNull(betriebsstaetteId).stream()
                                  .map(spbAnlage -> new SpbAnlageListItem(
                                          spbAnlage.getId(),
                                          spbAnlage.getAnlageNr(),
                                          spbAnlage.getName(),
                                          spbAnlage.getBetriebsstatus(),
                                          spbAnlage.hasAnlage(), null)).toList();
    }

    @Transactional(readOnly = true)
    @Secured({ BEHOERDE_READ, LAND_READ })
    public SpbAnlage loadSpbAnlage(Long id) {
        return spbAnlageRepository
                .findById(id)
                .filter(spba -> stammdatenService.canReadBetriebsstaette(spba.getParentBetriebsstaetteId()))
                .orElseThrow(() -> new BubeEntityNotFoundException(SpbAnlage.class, "ID " + id));
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbAnlage> findSpbAnlage(Long betriebsstaetteId, String anlagenNummer) {
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        return spbAnlageRepository
                .findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(betriebsstaetteId, anlagenNummer);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public SpbAnlage loadSpbAnlage(Long betriebsstaetteId, String anlagenNummer) {
        return findSpbAnlage(betriebsstaetteId, anlagenNummer).orElseGet(
                () -> new SpbAnlage(stammdatenService.loadAnlageByAnlageNummer(betriebsstaetteId, anlagenNummer)));
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbAnlage> findSpbAnlageByAnlageId(Long id) {
        Assert.notNull(id, "AnlageId ist nicht vorhanden");
        return spbAnlageRepository.findOneByAnlageId(id).filter(spbAnlage -> stammdatenService.canReadAnlage(id));
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbAnlage createSpbAnlage(SpbAnlage spbAnlage) {
        Assert.isNull(spbAnlage.getId(), "SPB Anlage bereits vorhanden");

        // Erstellungszeitpunkt aus SPL übernehmen
        spbAnlage.setErsteErfassung(Optional.ofNullable(spbAnlage.getAnlageId())
                                            .map(stammdatenService::loadAnlage)
                                            .map(Anlage::getErsteErfassung)
                                            .orElse(Instant.now()));
        return createOrUpdateSpbAnlage(spbAnlage);
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbAnlage updateSpbAnlage(SpbAnlage spbAnlage) {
        Assert.notNull(spbAnlage.getId(), "SPB Anlage nicht vorhanden");
        return createOrUpdateSpbAnlage(spbAnlage);
    }

    private SpbAnlage createOrUpdateSpbAnlage(SpbAnlage spbAnlage) {
        checkSchreibsperre(
                stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(spbAnlage.getParentBetriebsstaetteId()));

        checkSpbAnlageFunctionalKeyExists(spbAnlage);
        stammdatenService.checkAnlageFunctionalKeyExists(spbAnlage.getParentBetriebsstaetteId(),
                spbAnlage.getAnlageNr(), spbAnlage.getAnlageId());

        aenderungService.updateAenderungDurchBetreiber(spbAnlage);
        return spbAnlageRepository.save(spbAnlage);
    }

    private void checkSpbAnlageFunctionalKeyExists(SpbAnlage spbAnlage) {
        Long parentBetriebsstaetteId = spbAnlage.getParentBetriebsstaetteId();
        String anlageNr = spbAnlage.getAnlageNr();
        boolean exists = spbAnlage.getId() == null ?
                spbAnlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(parentBetriebsstaetteId,
                        anlageNr) :
                spbAnlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCaseAndIdNot(
                        parentBetriebsstaetteId, anlageNr, spbAnlage.getId());
        if (exists) {
            throw AnlageDuplicateKeyException.forAnlageNr(anlageNr);
        }
    }

    @Transactional
    @Secured(BETREIBER)
    public void deleteSpbAnlageByID(Long id) {
        deleteSpbAnlage(loadSpbAnlage(id));
    }

    @Transactional
    @Secured(BETREIBER)
    public void deleteSpbAnlage(SpbAnlage spbAnlage) {
        checkSchreibsperre(
                stammdatenService.checkBetriebsstaetteDatenberechtigungDelete(spbAnlage.getParentBetriebsstaetteId()));

        deleteSpbAnlageAndKindobjekte(spbAnlage);
    }

    private void deleteSpbAnlageAndKindobjekte(SpbAnlage spbAnlage) {
        spbAnlagenteilRepository.deleteAllByParentSpbAnlageId(spbAnlage.getId());
        spbAnlagenteilRepository.flush();

        spbQuelleRepository.deleteAllByParentSpbAnlageId(spbAnlage.getId());
        spbQuelleRepository.flush();

        spbAnlageRepository.deleteById(spbAnlage.getId());
        spbAnlageRepository.flush();
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void deleteUebernommeneSpbAnlage(SpbAnlage spbAnlage) {
        spbAnlageRepository.delete(spbAnlage);
        spbAnlageRepository.flush();
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleAnlageAngelegtEvent(AnlageAngelegtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageAngelegtEvent.EVENT_NAME);
        Anlage anlage = event.anlage;

        stammdatenService.checkAnlageDatenberechtigungWrite(anlage.getId());

        spbAnlageRepository.findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(
                                   anlage.getParentBetriebsstaetteId(), anlage.getAnlageNr())
                           .ifPresent(spbAnlage -> {
                               if (!spbAnlage.hasAnlage()) {
                                   spbAnlage.setAnlageId(anlage.getId());
                               }
                               aenderungService.updateAenderungDurchBetreiber(spbAnlage);
                           });
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    @Transactional
    public void handleAnlageGeloeschtEvent(AnlageGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlageGeloeschtEvent.EVENT_NAME);

        spbAnlagenteilRepository.deleteAllByParentAnlageId(event.anlageId);
        spbQuelleRepository.deleteAllByParentAnlageId(event.anlageId);

        spbAnlageRepository.findOneByAnlageId(event.anlageId)
                           .ifPresent(this::deleteSpbAnlageAndKindobjekte);
    }

    /**
     * Anlagenteil
     */
    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public List<SpbAnlagenteilListItem> listAllSpbAnlagenteileAsListItemByAnlageId(Long anlageId,
            Long betriebsstaetteId) {

        stammdatenService.loadAnlage(anlageId);

        List<Long> anlagenteilIdList = new ArrayList<>();
        List<SpbAnlagenteilListItem> spbAnlagenteile = new ArrayList<>();

        for (SpbAnlagenteil spbAnlagenteil : spbAnlagenteilRepository.findByParentAnlageId(anlageId)) {
            if (spbAnlagenteil.hasAnlagenteil()) {
                anlagenteilIdList.add(spbAnlagenteil.getAnlagenteilId());
            }
            spbAnlagenteile.add(new SpbAnlagenteilListItem(
                    spbAnlagenteil.getId(),
                    spbAnlagenteil.getAnlagenteilNr(),
                    spbAnlagenteil.getName(),
                    spbAnlagenteil.getBetriebsstatus(),
                    spbAnlagenteil.hasAnlagenteil()));
        }

        // Die Anlagenteile welche noch im "Originalzustand" sind hinzufügen
        for (Anlagenteil anlagenteil : stammdatenService.loadAllAnlagenteileByAnlageId(anlageId, betriebsstaetteId)) {
            if (!anlagenteilIdList.contains(anlagenteil.getId())) {
                spbAnlagenteile.add(new SpbAnlagenteilListItem(null,
                        anlagenteil.getAnlagenteilNr(), anlagenteil.getName(),
                        anlagenteil.getBetriebsstatus(), false));
            }
        }

        return spbAnlagenteile;
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public void aktualisiereSpbNachAnlagenteilUebernahme(Long spbAnlagenteilId) {
        spbAnlagenteilRepository.deleteById(spbAnlagenteilId);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public List<SpbAnlagenteilListItem> listAllNeueSpbAnlagenteileAsListItemByAnlageId(Long anlageId) {

        // Prüfe Datenberechtigung
        stammdatenService.loadAnlage(anlageId);

        return spbAnlagenteilRepository.findByParentAnlageIdAndAnlagenteilIdIsNull(anlageId).stream()
                                       .map(spbAnlagenteil -> new SpbAnlagenteilListItem(
                                               spbAnlagenteil.getId(),
                                               spbAnlagenteil.getAnlagenteilNr(),
                                               spbAnlagenteil.getName(),
                                               spbAnlagenteil.getBetriebsstatus(),
                                               spbAnlagenteil.hasAnlagenteil()))
                                       .toList();
    }

    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public List<SpbAnlagenteilListItem> listAllSpbAnlagenteileAsListItemBySpbAnlageId(Long spbAnlageId,
            Long betriebsstaetteId) {

        List<SpbAnlagenteilListItem> spbAnlagenteile = new ArrayList<>();

        if (spbAnlageId != null) {
            stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);
            spbAnlagenteile = spbAnlagenteilRepository.findByParentSpbAnlageId(spbAnlageId)
                                                      .stream()
                                                      .map(spbAnlagenteil -> new SpbAnlagenteilListItem(
                                                              spbAnlagenteil.getId(),
                                                              spbAnlagenteil.getAnlagenteilNr(),
                                                              spbAnlagenteil.getName(),
                                                              spbAnlagenteil.getBetriebsstatus(),
                                                              spbAnlagenteil.hasAnlagenteil()))
                                                      .toList();

        }
        return spbAnlagenteile;
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public SpbAnlagenteil loadSpbAnlagenteilByAnlageteilNummer(Long betriebsstaetteId, Long parentAnlageId,
            String anlagenteilNummer, boolean anSpbAnlage) {

        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        Optional<SpbAnlagenteil> spbAnlagenteilOpt;

        if (anSpbAnlage) {
            spbAnlagenteilOpt = spbAnlagenteilRepository.findOneByParentSpbAnlageIdAndAnlagenteilNrIgnoreCase(
                    parentAnlageId, anlagenteilNummer);
        } else {
            spbAnlagenteilOpt = spbAnlagenteilRepository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(
                    parentAnlageId, anlagenteilNummer);
        }

        return spbAnlagenteilOpt.orElseGet(() -> {
            Anlagenteil anlagenteil = stammdatenService
                    .loadAnlagenteilByAnlagenteilNummer(betriebsstaetteId, parentAnlageId, anlagenteilNummer);
            return new SpbAnlagenteil(anlagenteil);
        });
    }

    @Transactional(readOnly = true)
    @Secured({ BEHOERDE_READ, LAND_READ })
    public SpbAnlagenteil loadSpbAnlagenteilById(Long spbAnlagenteilId) {
        SpbAnlagenteil spbAnlagenteil = spbAnlagenteilRepository
                .findById(spbAnlagenteilId)
                .orElseThrow(() -> new BubeEntityNotFoundException(SpbAnlagenteil.class));
        checkSpbAnlagenteilDatenberechtigungRead(spbAnlagenteil);
        return spbAnlagenteil;
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbAnlagenteil> findSpbAnlagenteilByAnlagenteilId(Long anlagenteilId) {
        Assert.notNull(anlagenteilId, "AnlagenteilId ist nicht vorhanden");
        Optional<SpbAnlagenteil> spbAnlagenTeilOpt = this.spbAnlagenteilRepository.findOneByAnlagenteilId(
                anlagenteilId);
        spbAnlagenTeilOpt.ifPresent(this::checkSpbAnlagenteilDatenberechtigungRead);
        return spbAnlagenTeilOpt;
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbAnlagenteil> findSpbAnlagenteil(Long anlageId, String anlagenteilNummer) {
        if (!stammdatenService.canReadAnlage(anlageId)) {
            throw new BubeEntityNotFoundException(Anlage.class, "ID " + anlageId);
        }

        return spbAnlagenteilRepository
                .findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlageId, anlagenteilNummer);
    }

    private void checkSpbAnlagenteilDatenberechtigungRead(SpbAnlagenteil spbAnlagenteil) {
        if (spbAnlagenteil.getParentAnlageId() != null) {
            stammdatenService.loadAnlage(spbAnlagenteil.getParentAnlageId());
        } else {
            // Datenberechtigung findet im Stammdatenservice statt
            loadSpbAnlage(spbAnlagenteil.getParentSpbAnlageId());
        }
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbAnlagenteil createSpbAnlagenteil(SpbAnlagenteil spbAnlagenteil) {
        Assert.isNull(spbAnlagenteil.getId(), "SPB Anlagenteil vorhanden");
        // Erstellungszeitpunkt aus SPL übernehmen
        spbAnlagenteil.setErsteErfassung(
                Optional.ofNullable(spbAnlagenteil.getAnlagenteilId())
                        .map(stammdatenService::loadAnlagenteil)
                        .map(Anlagenteil::getErsteErfassung)
                        .orElse(Instant.now()));
        return spbAnlagenteilRepository.save(createOrUpdateSpbAnlagenteil(spbAnlagenteil));
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbAnlagenteil updateSpbAnlagenteil(SpbAnlagenteil spbAnlagenteil) {
        Assert.notNull(spbAnlagenteil.getId(), "SPB Anlagenteil nicht vorhanden");
        return createOrUpdateSpbAnlagenteil(spbAnlagenteil);
    }

    private SpbAnlagenteil createOrUpdateSpbAnlagenteil(SpbAnlagenteil spbAnlagenteil) {
        Long parentBetriebsstaetteId = getSpbAnlagenTeilParentBetriebsstaetteId(spbAnlagenteil);
        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(parentBetriebsstaetteId));

        checkSpbAnlagenteilFunctionalKeyExists(spbAnlagenteil);
        stammdatenService.checkAnlagenteilFunctionalKeyExists(spbAnlagenteil.getParentAnlageId(),
                spbAnlagenteil.getAnlagenteilNr(), spbAnlagenteil.getAnlagenteilId());

        aenderungService.updateAenderungDurchBetreiber(spbAnlagenteil, parentBetriebsstaetteId);
        return spbAnlagenteilRepository.save(spbAnlagenteil);
    }

    private Long getSpbAnlagenTeilParentBetriebsstaetteId(SpbAnlagenteil spbAnlagenteil) {
        Long parentBetriebsstaetteId;
        if (spbAnlagenteil.getParentAnlageId() != null) {
            Anlage anlage = stammdatenService.loadAnlage(spbAnlagenteil.getParentAnlageId());
            parentBetriebsstaetteId = anlage.getParentBetriebsstaetteId();
        } else {
            SpbAnlage spbAnlage = loadSpbAnlage(spbAnlagenteil.getParentSpbAnlageId());
            parentBetriebsstaetteId = spbAnlage.getParentBetriebsstaetteId();
        }
        return parentBetriebsstaetteId;
    }

    private void checkSpbAnlagenteilFunctionalKeyExists(SpbAnlagenteil spbAnlagenteil) {
        Long parentAnlageId = spbAnlagenteil.getParentAnlageId();
        String anlagenteilNr = spbAnlagenteil.getAnlagenteilNr();
        boolean exists = spbAnlagenteil.getId() == null ?
                this.spbAnlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(parentAnlageId,
                        anlagenteilNr) :
                this.spbAnlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCaseAndIdNot(parentAnlageId,
                        anlagenteilNr, spbAnlagenteil.getId());
        if (exists) {
            throw AnlagenteilDuplicateKeyException.forAnlagenteilNr(anlagenteilNr);
        }
    }

    @Transactional
    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void deleteSpbAnlagenteil(Long betriebsstaetteId, Long id) {

        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(betriebsstaetteId));

        var spbAnlagenteil = spbAnlagenteilRepository.findById(id)
                                                     .orElseThrow(() -> new BubeEntityNotFoundException(
                                                             SpbAnlagenteil.class));
        spbAnlagenteilRepository.delete(spbAnlagenteil);
    }

    @EventListener
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    @Transactional
    public void handleAnlagenteilGeloeschtEvent(AnlagenteilGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", AnlagenteilGeloeschtEvent.EVENT_NAME);

        spbAnlagenteilRepository.findOneByAnlagenteilId(event.anlagenteilId)
                                .ifPresent(spbAnlagenteilRepository::delete);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public void handleAnlagenteilAngelegtEvent(AnlagenteilAngelegtEvent event) {

        LOGGER.info("Event handling started for '{}'", AnlagenteilAngelegtEvent.EVENT_NAME);
        Anlagenteil anlagenteil = event.anlagenteil;

        spbAnlagenteilRepository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlagenteil.getParentAnlageId(),
                                        anlagenteil.getAnlagenteilNr())
                                .ifPresent(spbAnlagenteil -> {
                                    if (!spbAnlagenteil.hasAnlagenteil()) {
                                        spbAnlagenteil.setAnlagenteilId(anlagenteil.getId());
                                    }
                                });
    }

    /**
     * Betreiber
     */
    @EventListener
    @Transactional()
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleBetreiberGeloeschtEvent(BetreiberGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", BetreiberGeloeschtEvent.EVENT_NAME);
        Optional<SpbBetreiber> spbBetreiberOptional = this.findSpbBetreiberByBetreiberId(event.betreiberId);
        spbBetreiberOptional.map(SpbBetreiber::getId).ifPresent(this::deleteSpbBetreiber);
    }

    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public SpbBetreiber loadSpbBetreiberByJahrLandBetriebsstaetteNummer(String jahrSchluessel, Land land,
            String betriebsstaetteNummer) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteByJahrLandNummer(jahrSchluessel, land,
                betriebsstaetteNummer);
        var betreiber = Optional.ofNullable(betriebsstaette.getBetreiber());
        return betreiber.map(Betreiber::getId)
                        .flatMap(this::findSpbBetreiberByBetreiberId)
                        .or(() -> betreiber.map(SpbBetreiber::new))
                        .orElse(null);
    }

    @Transactional(readOnly = true)
    @Secured(BETREIBER)
    public SpbBetreiber loadSpbBetreiberByJahrLandBetriebsstaetteLocalId(String jahrSchluessel, Land land,
            String localId) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteByJahrLandLocalId(jahrSchluessel, land,
                localId);
        Betreiber betreiber = betriebsstaette.getBetreiber();
        if (betreiber == null) {
            return null;
        }

        Optional<SpbBetreiber> spbBetreiberOpt = this.findSpbBetreiberByBetreiberId(
                betreiber.getId());
        return spbBetreiberOpt.orElseGet(() -> new SpbBetreiber(betreiber));
    }

    @Transactional(readOnly = true)
    @Secured({ BEHOERDE_READ, LAND_READ, BETREIBER })
    public Optional<SpbBetreiber> findSpbBetreiberByBetreiberId(Long id) {
        Assert.notNull(id, "BetreiberId ist nicht vorhanden");
        return spbBetreiberRepository.findOne(spbBetreiber.betreiber.id.eq(id))
                                     .filter(betreiber -> stammdatenService.canReadBetreiber(id));
    }

    @Transactional(readOnly = true)
    @Secured({ BEHOERDE_READ, LAND_READ })
    public SpbBetreiber loadSpbBetreiber(Long id) {
        return spbBetreiberRepository.getById(id);
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbBetreiber createSpbBetreiber(Long betriebsstaetteId, SpbBetreiber spbBetreiber) {
        Assert.isNull(spbBetreiber.getId(), "SPB Betreiber bereits vorhanden");
        Assert.notNull(spbBetreiber.getBetreiberId(),
                "SPB Betreiber darf nur für einen vorhandenen Betreiber angelegt werden");

        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(betriebsstaetteId));

        // SpbBetreiber speichern und an SpbBetriebsstaette anheften (sofern diese existiert)
        SpbBetreiber finalSpbBetreiber = spbBetreiberRepository.save(spbBetreiber);
        spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(betriebsstaetteId).ifPresent(spbBst -> {
            spbBst.setSpbBetreiber(finalSpbBetreiber);
            aenderungService.updateAenderungDurchBetreiber(spbBst);
            spbBetriebsstaetteRepository.save(spbBst);
        });
        aenderungService.updateAenderungDurchBetreiber(spbBetreiber, betriebsstaetteId);
        return finalSpbBetreiber;
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbBetreiber updateSpbBetreiber(SpbBetreiber spbBetreiber, Long betriebsstaetteId) {
        Assert.notNull(spbBetreiber.getId(), "SPB Betreiber nicht vorhanden");
        Assert.notNull(spbBetreiber.getBetreiberId(), "Betreiber nicht vorhanden");
        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(betriebsstaetteId));
        aenderungService.updateAenderungDurchBetreiber(spbBetreiber, betriebsstaetteId);
        return spbBetreiberRepository.save(spbBetreiber);
    }

    @Transactional
    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public SpbBetreiber deleteSpbBetreiber(Long spbBetreiberId) {
        Betreiber betreiber = null;

        Optional<Long> betreiberIdOptional = spbBetreiberRepository.findById(spbBetreiberId)
                                                                   .map(SpbBetreiber::getBetreiberId);
        if (betreiberIdOptional.isPresent()) {
            betreiber = stammdatenService.loadBetreiber(betreiberIdOptional.get());
        }

        Optional<SpbBetriebsstaette> spbBetriebsstaetteOptional = spbBetriebsstaetteRepository.findOneBySpbBetreiberId(
                spbBetreiberId);
        spbBetriebsstaetteOptional.ifPresent(spbBst -> spbBst.setSpbBetreiber(null));

        spbBetreiberRepository.deleteById(spbBetreiberId);

        if (betreiber == null) {
            betreiber = spbBetriebsstaetteOptional.map(SpbBetriebsstaette::getBetriebsstaette)
                                                  .map(Betriebsstaette::getBetreiber)
                                                  .orElse(null);
        }

        return new SpbBetreiber(betreiber);
    }

    /**
     * Quelle
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<SpbQuelle> findSpbQuelleByQuelleId(Long id) {
        Assert.notNull(id, "QuelleId ist nicht vorhanden");
        return spbQuelleRepository.findOne(spbQuelle.quelleId.eq(id))
                                  .filter(quelle -> stammdatenService.canReadQuelle(id));
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbQuelle> findSpbQuelleOfAnlage(Long anlageId, String quelleNr) {
        if (!stammdatenService.canReadAnlage(anlageId)) {
            throw new BubeEntityNotFoundException(Anlage.class, "ID " + anlageId);
        }

        return spbQuelleRepository
                .findOneByParentAnlageIdAndQuelleNrIgnoreCase(anlageId, quelleNr);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Optional<SpbQuelle> findSpbQuelleOfBetriebsstaette(Long betriebsstaetteId, String quelleNr) {
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        return spbQuelleRepository
                .findOneByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(betriebsstaetteId, quelleNr);
    }

    @EventListener
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public void handleQuelleAngelegtEvent(QuelleAngelegtEvent event) {
        LOGGER.info("Event handling started for '{}'", QuelleAngelegtEvent.EVENT_NAME);
        Quelle quelle = event.quelle;

        if (quelle.istKindVonBetriebsstaette()) {
            spbQuelleRepository.findOneByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(
                    quelle.getParentBetriebsstaetteId(),
                    quelle.getQuelleNr()).ifPresent(spbQuelle -> {
                if (!spbQuelle.hasQuelle()) {
                    spbQuelle.setQuelleId(quelle.getId());
                }
                aenderungService.updateAenderungDurchBetreiber(spbQuelle);
            });
        } else if (quelle.istKindVonAnlage()) {
            spbQuelleRepository.findOneByParentAnlageIdAndQuelleNrIgnoreCase(quelle.getParentAnlageId(),
                    quelle.getQuelleNr()).ifPresent(spbQuelle -> {
                if (!spbQuelle.hasQuelle()) {
                    spbQuelle.setQuelleId(quelle.getId());
                }
                aenderungService.updateAenderungDurchBetreiber(spbQuelle);
            });
        }

    }

    @EventListener
    @Transactional
    @Secured({ BEHOERDE_DELETE, LAND_DELETE })
    public void handleQuelleGeloeschtEvent(QuelleGeloeschtEvent event) {
        LOGGER.info("Event handling started for '{}'", QuelleGeloeschtEvent.EVENT_NAME);
        Optional<SpbQuelle> spbQuelleOptional = this.findSpbQuelleByQuelleId(event.quelleId);
        spbQuelleOptional.ifPresent(
                quelle -> this.deleteSpbQuelle(quelle.getId()));
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    @Transactional
    public void aktualisiereSpbNachQuelleUebernahme(Long spbQuelleId) {
        deleteSpbQuelle(spbQuelleId);
    }

    @Transactional
    @Secured(BETREIBER)
    public SpbQuelle createSpbQuelle(SpbQuelle spbQuelle) {
        Assert.isNull(spbQuelle.getId(), "SPB Quelle bereits vorhanden");
        if (spbQuelle.getParentSpbAnlageId() != null) {
            checkDatenBerechtigungForQuelle(spbQuelle.getParentBetriebsstaetteId(), spbQuelle.getParentAnlageId());
            if (spbQuelle.getQuelleId() == null) {
                stammdatenService.findQuelle(spbQuelle.getParentBetriebsstaetteId(), spbQuelle.getQuelleNr())
                                 .map(Quelle::getId)
                                 .ifPresent(spbQuelle::setQuelleId);
            }
        }

        // Erstellungszeitpunkt aus SPL übernehmen
        spbQuelle.setErsteErfassung(
                Optional.ofNullable(spbQuelle.getQuelleId())
                        .map(stammdatenService::loadQuelle)
                        .map(Quelle::getErsteErfassung)
                        .orElse(Instant.now()));

        return createOrUpdateSpbQuelle(spbQuelle);
    }

    private SpbQuelle createOrUpdateSpbQuelle(SpbQuelle spbQuelle) {

        checkSpbQuelleFunctionalKeyExists(spbQuelle);

        if (spbQuelle.getParentBetriebsstaetteId() != null) {
            checkSchreibsperre(
                    stammdatenService.checkBetriebsstaetteDatenberechtigungRead(
                            spbQuelle.getParentBetriebsstaetteId()));
        } else if (spbQuelle.getParentAnlageId() != null) {
            checkSchreibsperre(stammdatenService.loadAnlage(spbQuelle.getParentAnlageId()));
        }

        if (spbQuelle.istKindVonBetriebsstaette()) {
            stammdatenService.checkBstQuelleFunctionalKeyExists(spbQuelle.getParentBetriebsstaetteId(),
                    spbQuelle.getQuelleNr(), spbQuelle.getQuelleId());
        } else {
            stammdatenService.checkAnlageQuelleFunctionalKeyExists(spbQuelle.getParentAnlageId(),
                    spbQuelle.getQuelleNr(), spbQuelle.getQuelleId());
        }

        aenderungService.updateAenderungDurchBetreiber(spbQuelle);
        return spbQuelleRepository.save(spbQuelle);
    }

    private void checkDatenBerechtigungForQuelle(Long betriebsstaetteId, Long anlageId) {
        if (betriebsstaetteId != null) {
            stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);
        } else if (anlageId != null) {
            stammdatenService.checkBetriebsstaetteDatenberechtigungRead(
                    stammdatenService.loadAnlage(anlageId).getParentBetriebsstaetteId());
        } else {
            throw new BubeEntityNotFoundException(Quelle.class);
        }
    }

    private void checkSpbQuelleFunctionalKeyExists(SpbQuelle spbQuelle) {
        if (spbQuelle.istKindVonAnlage()) {
            checkAnlageSpbQuelleFunctionalKeyExists(spbQuelle.getParentAnlageId(), spbQuelle.getQuelleNr(),
                    spbQuelle.getId());
        }
        if (spbQuelle.istKindVonBetriebsstaette()) {
            checkBstSpbQuelleFunctionalKeyExists(spbQuelle.getParentBetriebsstaetteId(), spbQuelle.getQuelleNr(),
                    spbQuelle.getId());
        }
    }

    private void checkAnlageSpbQuelleFunctionalKeyExists(Long parentAnlageId, String quelleNr, Long spbQuelleId) {
        boolean exists = spbQuelleId == null ?
                spbQuelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCase(parentAnlageId, quelleNr) :
                spbQuelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCaseAndIdNot(parentAnlageId, quelleNr,
                        spbQuelleId);
        if (exists) {
            throw QuelleDuplicateKeyException.forQuelleNr(quelleNr);
        }
    }

    private void checkBstSpbQuelleFunctionalKeyExists(Long parentBetriebsstaetteId, String quelleNr, Long spbQuelleId) {
        boolean exists = spbQuelleId == null ?
                spbQuelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(parentBetriebsstaetteId,
                        quelleNr) :
                spbQuelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCaseAndIdNot(
                        parentBetriebsstaetteId, quelleNr, spbQuelleId);
        if (exists) {
            throw QuelleDuplicateKeyException.forQuelleNr(quelleNr);
        }

    }

    @Transactional
    @Secured(BETREIBER)
    public SpbQuelle updateSpbQuelle(SpbQuelle spbQuelle) {
        // Berechtigungsprüfung
        checkDatenBerechtigungForQuelle(spbQuelle.getParentBetriebsstaetteId(), spbQuelle.getParentAnlageId());

        Assert.notNull(spbQuelle.getId(), "SPB Quelle nicht vorhanden");

        return createOrUpdateSpbQuelle(spbQuelle);
    }

    @Transactional
    @Secured(BETREIBER)
    public void deleteSpbQuelleFromRest(Long betriebsstaetteId, Long id) {
        checkSchreibsperre(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(betriebsstaetteId));

        spbQuelleRepository.deleteById(id);
    }

    @Transactional
    @Secured({ LAND_WRITE, BEHOERDE_WRITE })
    public void deleteSpbQuelle(Long id) {
        spbQuelleRepository.deleteById(id);
    }

    @Secured({ BETREIBER, LAND_READ })
    @Transactional(readOnly = true)
    public List<SpbQuelleListItem> listAllSpbQuellenAsListItemByBetriebsstaettenId(long betriebsstaetteId) {
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        Set<Long> quelleIds = new HashSet<>();
        List<SpbQuelleListItem> spbQuellen = new ArrayList<>();
        String originalQuelleNr;

        for (SpbQuelle spbQuelle : spbQuelleRepository.findByParentBetriebsstaetteId(
                betriebsstaetteId)) {
            if (spbQuelle.hasQuelle()) {
                quelleIds.add(spbQuelle.getQuelleId());

                originalQuelleNr = stammdatenService.loadQuelle(spbQuelle.getQuelleId()).getQuelleNr();

                spbQuellen.add(new SpbQuelleListItem(
                        spbQuelle.getId(),
                        spbQuelle.getQuelleNr(),
                        spbQuelle.getName(),
                        true,
                        originalQuelleNr));
            } else {
                spbQuellen.add(new SpbQuelleListItem(
                        spbQuelle.getId(),
                        spbQuelle.getQuelleNr(),
                        spbQuelle.getName(),
                        false,
                        null));
            }
        }

        // Die Quellen welche noch im "Originalzustand" sind hinzufügen
        for (Quelle quelle : stammdatenService.loadAllQuellenByBetriebsstaettenId(betriebsstaetteId)) {
            if (!quelleIds.contains(quelle.getId())) {
                spbQuellen.add(new SpbQuelleListItem(
                        null,
                        quelle.getQuelleNr(),
                        quelle.getName(),
                        true,
                        quelle.getQuelleNr()));
            }
        }

        return spbQuellen;
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public Collection<SpbQuelleListItem> listAllNeueSpbQuellenAsListItemByBetriebsstaettenId(long betriebsstaetteId) {

        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        return spbQuelleRepository.findByParentBetriebsstaetteIdAndQuelleIdIsNull(betriebsstaetteId).stream()
                                  .map(spbQuelle -> new SpbQuelleListItem(
                                          spbQuelle.getId(),
                                          spbQuelle.getQuelleNr(),
                                          spbQuelle.getName(),
                                          false,
                                          null)).toList();
    }

    private void createQuelleList(List<Long> quelleIdList, List<SpbQuelleListItem> spbQuellen, SpbQuelle spbQuelle) {
        quelleIdList.add(spbQuelle.getQuelleId());
        Quelle q = null;
        if (spbQuelle.getQuelleId() != null) {
            q = stammdatenService.loadQuelle(spbQuelle.getQuelleId());
        }
        spbQuellen.add(new SpbQuelleListItem(
                spbQuelle.getId(),
                spbQuelle.getQuelleNr(),
                spbQuelle.getName(),
                q != null,
                q == null ? null : q.getQuelleNr()));
    }

    @Secured({ BETREIBER, LAND_READ })
    @Transactional(readOnly = true)
    public List<SpbQuelleListItem> listAllSpbQuellenAsListItemByAnlageId(Long anlageId, Long spbAnlageId,
            long betriebsstaetteId) {
        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        List<Long> quelleIdList = new ArrayList<>();
        List<SpbQuelleListItem> spbQuellen = new ArrayList<>();

        if (anlageId != null) {
            stammdatenService.loadAnlage(anlageId);
            for (SpbQuelle spbQuelle : spbQuelleRepository.findByParentAnlageId(anlageId)) {
                createQuelleList(quelleIdList, spbQuellen, spbQuelle);

            }

            // Die Quellen welche noch im "Originalzustand" sind hinzufügen
            for (Quelle quelle : stammdatenService.loadAllQuellenByAnlagenId(anlageId, betriebsstaetteId)) {
                if (!quelleIdList.contains(quelle.getId())) {
                    spbQuellen.add(new SpbQuelleListItem(
                            null,
                            quelle.getQuelleNr(),
                            quelle.getName(),
                            true,
                            quelle.getQuelleNr()));
                }
            }
        } else if (spbAnlageId != null) {
            for (SpbQuelle spbQuelle : spbQuelleRepository.findByParentSpbAnlageId(spbAnlageId)) {
                createQuelleList(quelleIdList, spbQuellen, spbQuelle);
            }
        }

        return spbQuellen;
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, BEHOERDE_READ, LAND_READ })
    public List<SpbQuelleListItem> listAllNeueSpbQuellenAsListItemByAnlagenId(Long anlageId) {

        // Datenberechtigung prüfen
        stammdatenService.loadAnlage(anlageId);

        return spbQuelleRepository.findByParentAnlageIdAndQuelleIdIsNull(anlageId)
                                  .stream()
                                  .map(spbQuelle -> new SpbQuelleListItem(
                                          spbQuelle.getId(),
                                          spbQuelle.getQuelleNr(),
                                          spbQuelle.getName(),
                                          false,
                                          null))
                                  .toList();
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, LAND_READ, BEHOERDE_READ })
    public SpbQuelle loadSpbQuelle(Long id) {
        return spbQuelleRepository.findById(id)
                                  .orElseThrow(() -> new BubeEntityNotFoundException(SpbQuelle.class, "ID " + id));
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, LAND_READ, BEHOERDE_READ })
    public SpbQuelle loadSpbQuelleByBetriebsstaette(Long betriebsstaetteId, String quellenNummer) {

        // Dateneberechtigung findet in Stammdatenservice statt
        Optional<Quelle> oQuelle = stammdatenService.findQuelleOfBst(betriebsstaetteId, quellenNummer);

        if (oQuelle.isPresent()) {
            Optional<SpbQuelle> spbQuelleOpt = spbQuelleRepository.findOneByParentBetriebsstaetteIdAndQuelleId(
                    betriebsstaetteId, oQuelle.get().getId());
            return spbQuelleOpt.orElseGet(
                    () -> new SpbQuelle(oQuelle.get()));
        }
        return spbQuelleRepository.findOneByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(betriebsstaetteId,
                                          quellenNummer)
                                  .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, LAND_READ, BEHOERDE_READ })
    public SpbQuelle loadSpbQuelleByAnlage(Long anlageId, String quellenNummer) {

        // Datenberechtigung findet in Stammdatenservice statt
        Optional<Quelle> oQuelle = stammdatenService.findQuelleOfAnlage(anlageId, quellenNummer);
        if (oQuelle.isPresent()) {
            Optional<SpbQuelle> spbQuelleOpt = spbQuelleRepository.findOneByParentAnlageIdAndQuelleId(
                    anlageId, oQuelle.get().getId());

            return spbQuelleOpt.orElseGet(() -> new SpbQuelle(oQuelle.get()));
        }

        return spbQuelleRepository.findOneByParentAnlageIdAndQuelleNrIgnoreCase(anlageId, quellenNummer)
                                  .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional(readOnly = true)
    @Secured({ BETREIBER, LAND_READ, BEHOERDE_READ })
    public SpbQuelle loadSpbQuelleBySpbAnlage(Long spbAnlageId, Long betriebsstaetteId, String quellenNummer) {

        stammdatenService.checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);
        SpbAnlage spbAnlage = loadSpbAnlage(spbAnlageId);

        if (spbAnlage.hasAnlage()) {
            Optional<Quelle> oQuelle = stammdatenService.findQuelleOfAnlage(spbAnlage.getAnlageId(),
                    quellenNummer);
            if (oQuelle.isPresent()) {
                Optional<SpbQuelle> spbQuelleOpt = spbQuelleRepository.findOneByParentAnlageIdAndQuelleId(
                        spbAnlageId, oQuelle.get().getId());

                return spbQuelleOpt.orElseGet(
                        () -> new SpbQuelle(oQuelle.get()));
            }
        }

        return spbQuelleRepository.findOneByParentSpbAnlageIdAndQuelleNrIgnoreCase(spbAnlageId, quellenNummer)
                                  .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Betriebsstaette schliesseBetriebsstaetteUebernahmeAb(Long betriebsstaetteId) {
        stammdatenService.updateSchreibsperreBetreiber(betriebsstaetteId, true);
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForWrite(betriebsstaetteId);
        Betreiber betreiber = betriebsstaette.getBetreiber();

        List<Anlage> anlagen = stammdatenService.loadAllAnlagenByBetriebsstaettenId(betriebsstaetteId);
        List<Anlagenteil> anlagenteile = new ArrayList<>();
        List<Quelle> quellen = new ArrayList<>(
                stammdatenService.loadAllQuellenByBetriebsstaettenId(betriebsstaetteId));

        Optional<SpbBetriebsstaette> spbBetriebsstaetteOpt = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(
                betriebsstaetteId);
        Optional<SpbBetreiber> spbBetreiber = betreiber == null ?
                Optional.empty() :
                findSpbBetreiberByBetreiberId(betreiber.getId());
        List<SpbAnlage> spbAnlagen = spbAnlageRepository.findByParentBetriebsstaetteId(
                betriebsstaetteId);
        List<SpbAnlagenteil> spbAnlagenteile = new ArrayList<>();
        List<SpbQuelle> spbQuellen = new ArrayList<>(spbQuelleRepository.findByParentBetriebsstaetteId(
                betriebsstaetteId));

        spbAnlagen.stream()
                  .filter(spbAnlage -> !spbAnlage.hasAnlage())
                  .forEach(spbAnlage -> {
                      spbQuellen.addAll(spbQuelleRepository.findByParentSpbAnlageId(spbAnlage.getId()));
                      spbAnlagenteile.addAll(spbAnlagenteilRepository.findByParentSpbAnlageId(spbAnlage.getId()));
                  });
        anlagen.forEach(anlage -> {
            spbAnlagenteile.addAll(spbAnlagenteilRepository.findByParentAnlageId(anlage.getId()));
            anlagenteile.addAll(stammdatenService.loadAllAnlagenteileByAnlageId(anlage.getId(), betriebsstaetteId));
            spbQuellen.addAll(spbQuelleRepository.findByParentAnlageId(anlage.getId()));
            quellen.addAll(stammdatenService.loadAllQuellenByAnlagenId(anlage.getId(), betriebsstaetteId));
        });

        for (Quelle quelle : quellen) {
            spbQuelleRepository.findByQuelleId(quelle.getId()).ifPresent(spbQuellen::add);
        }

        deleteAllSpbObjects(spbBetriebsstaetteOpt, spbAnlagen, spbQuellen, spbAnlagenteile, spbBetreiber);

        return stammdatenService.updateStammdatenObjekteForAbschlussUebernahme(betriebsstaette, anlagen, quellen,
                anlagenteile, betreiber);
    }

    private void deleteAllSpbObjects(Optional<SpbBetriebsstaette> spbBetriebsstaetteOpt,
            List<SpbAnlage> spbAnlagen, List<SpbQuelle> spbQuellen, List<SpbAnlagenteil> spbAnlagenteile,
            Optional<SpbBetreiber> spbBetreiberOpt) {

        spbQuelleRepository.deleteAll(spbQuellen);
        spbAnlagenteilRepository.deleteAll(spbAnlagenteile);
        spbAnlageRepository.deleteAll(spbAnlagen);

        spbBetreiberOpt.ifPresent(spbBetreiberRepository::delete);
        spbBetriebsstaetteOpt.ifPresent(spbBetriebsstaetteRepository::delete);
    }

    private void checkSchreibsperre(Betriebsstaette betriebsstaette) {
        if (betriebsstaette.isSchreibsperreBetreiber()) {
            throw new BubeEntityNotFoundException(
                    "Die Betriebsstätte " + betriebsstaette.getBetriebsstaetteNr() + " hat eine Schreibsperre.");
        }
    }

    private void checkSchreibsperre(Anlage anlage) {
        checkSchreibsperre(stammdatenService.loadBetriebsstaetteForRead(anlage.getParentBetriebsstaetteId()));
    }

    @Transactional
    @Secured(BETREIBER)
    public Stream<Referenz> getAllYearsForBetriebsstaette(Long bstId) {
        return stammdatenService.getJahresReferenzenByNummer(stammdatenService.loadBetriebsstaetteForRead(bstId));
    }
}
