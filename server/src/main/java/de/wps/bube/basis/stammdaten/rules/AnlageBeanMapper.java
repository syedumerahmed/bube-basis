package de.wps.bube.basis.stammdaten.rules;

import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;

@Mapper(uses = { GeoPunktBeanMapper.class, VorschriftBeanMapper.class, LeistungBeanMapper.class,
        ZustaendigeBehoerdeBeanMapper.class, ReferenzBeanMapper.class, QuelleBeanMapper.class,
        AnlagenteilBeanMapper.class })
public abstract class AnlageBeanMapper {

    @Autowired
    protected StammdatenService stammdatenService;

    @Mapping(target = "betriebsstaette", expression = "java(betriebsstaette(anlage, context, mappers))")
    @Mapping(target = "vorgaenger", expression = "java(vorgaenger(anlage, context, mappers))")
    @Mapping(target = "nachfolger", expression = "java(nachfolger(anlage, context, mappers))")
    public abstract AnlageBean toBean(Anlage anlage, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers);

    protected Supplier<BetriebsstaetteBean> betriebsstaette(Anlage anlage, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(
                    anlage.getParentBetriebsstaetteId());
            return mappers.getBetriebsstaetteBeanMapper().toBean(betriebsstaette, context, mappers);
        };
    }

    protected Supplier<AnlageBean> vorgaenger(Anlage anlage, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            BetriebsstaetteBean betriebsstaette = betriebsstaette(anlage, context, mappers).get();
            BetriebsstaetteBean vorgaenger = betriebsstaette.getVorgaenger();
            return vorgaenger == null || anlage.getLocalId() == null ? null :
                    stammdatenService.findAnlageByLocalId(vorgaenger.getId(), anlage.getLocalId())
                                     .map(a -> mappers.getAnlageBeanMapper().toBean(a, context.vorgaenger(), mappers))
                                     .orElse(null);
        };
    }

    protected Supplier<AnlageBean> nachfolger(Anlage anlage, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            BetriebsstaetteBean betriebsstaette = betriebsstaette(anlage, context, mappers).get();
            BetriebsstaetteBean nachfolger = betriebsstaette.getNachfolger();
            return nachfolger == null || anlage.getLocalId() == null ? null :
                    stammdatenService.findAnlageByLocalId(nachfolger.getId(), anlage.getLocalId())
                                     .map(a -> mappers.getAnlageBeanMapper().toBean(a, context.nachfolger(), mappers))
                                     .orElse(null);
        };
    }

}
