package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Address {
    @XmlElement(required = true, name = "AddressDetails")
    private final AddressDetails details = new AddressDetails();

    public String getStreetName() {
        return details.streetName;
    }

    public void setStreetName(String streetName) {
        this.details.streetName = streetName;
    }

    public String getBuildingNumber() {
        return details.buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.details.buildingNumber = buildingNumber;
    }

    public String getCity() {
        return details.city;
    }

    public void setCity(String city) {
        this.details.city = city;
    }

    public String getPostalCode() {
        return details.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.details.postalCode = postalCode;
    }

    public ReferenceType getConfidentialityReason() {
        return details.confidentialityReason;
    }

    public void setConfidentialityReason(ReferenceType confidentialityReason) {
        details.confidentialityReason = confidentialityReason;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class AddressDetails {
        @XmlElement(required = true, name = "streetName")
        String streetName;
        @XmlElement(required = true, name = "buildingNumber")
        String buildingNumber;
        @XmlElement(required = true, name = "city")
        String city;
        @XmlElement(required = true, name = "postalCode")
        String postalCode;
        @XmlElement(name = "confidentialityReason")
        ReferenceType confidentialityReason;
    }
}
