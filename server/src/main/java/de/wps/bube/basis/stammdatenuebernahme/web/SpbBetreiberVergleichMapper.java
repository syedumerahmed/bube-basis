package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbBetreiberVergleichDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetreiberVergleich;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbBetreiberVergleichMapper {
    @Mapping(target = "allgemein", source = ".")
    public abstract SpbBetreiberVergleichDto toDto(SpbBetreiberVergleich spbBetreiberVergleich);
}
