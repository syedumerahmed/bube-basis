package de.wps.bube.basis.stammdaten.domain.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;

@Mapper
public interface AnlagenUpdater {

    @Mapping(target = "quellen", ignore = true)
    @Mapping(target = "anlagenteile", ignore = true)
    void update(@MappingTarget Anlage targetAnlage, Anlage sourceAnlage);

}
