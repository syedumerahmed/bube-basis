package de.wps.bube.basis.stammdaten.persistence.predicates;

import static de.wps.bube.basis.stammdaten.domain.entity.QAnlagenteil.anlagenteil;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.stammdaten.domain.vo.AnlagenteilColumn;

public class AnlagenteilPredicates {

    public static Predicate parentAnlageIdEquals(final long anlageId) {
        return anlagenteil.parentAnlageId.eq(anlageId);
    }

    public static Predicate mapAnlagenteilFiltersToPredicate(
            final List<ListStateDto.FilterStateDto<AnlagenteilColumn>> filters) {
        if (filters == null) {
            return null;
        }
        return createPredicateByAnlagenteilColumns(
                filters.stream().collect(Collectors.toMap(f -> f.property, f -> f.value)));
    }

    public static Predicate createPredicateByAnlagenteilColumns(final Map<AnlagenteilColumn, String> columns) {
        if (columns == null || columns.isEmpty()) {
            return null;
        }
        final BooleanBuilder booleanBuilder = new BooleanBuilder();
        columns.forEach((property, value) -> booleanBuilder.and(property.getExpression().containsIgnoreCase(value)));
        return booleanBuilder;
    }
}
