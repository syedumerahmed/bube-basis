package de.wps.bube.basis.base.xml;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import javax.xml.bind.DataBindingException;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class EUBerlinDateTimeAdapter{

    // xxx = Three letters outputs the hour and minute, with a colon, such as '+01:30'.
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssxxx");

    public static XMLGregorianCalendar unmarshal(String s){
        if (s == null) {
            return null;
        }
        // Wir verwenden die JAXB Funktion zum Einlesen von DateTime
        // Dabei wird die Zeitzone des Systems angenommen, wenn diese Info am String s fehlt. (DEV/TEST=UTC)
        // Das wollen wir nicht und prüfen zuerst, ob die Zeitzone am String enthalten ist
        if (s.length() < 20) {
            throw new IllegalArgumentException("DateTime Wert ohne Zeitzone");
        }

        // https://www.data2type.de/xml-xslt-xslfo/xml-schema/datentypen-referenz/xs-datetime/
        GregorianCalendar gregorianCalendar = (GregorianCalendar) DatatypeConverter.parseDateTime(s);

        // Siehe Bug https://bugs.java.com/bugdatabase/view_bug.do?bug_id=4827490
        gregorianCalendar.get(Calendar.HOUR_OF_DAY);

        //Umwandlung in UTC
        gregorianCalendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));

        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch(DatatypeConfigurationException datatypeConfigurationException){
            throw new IllegalStateException(datatypeConfigurationException);
        }

    }

    public static String marshal(XMLGregorianCalendar xmlGregorianCalendar) {
        if (xmlGregorianCalendar == null) {
            return null;
        }
        //Wandelt die UTC Zeit in CET um
        ZonedDateTime utcZonedDateTime = xmlGregorianCalendar.toGregorianCalendar().toZonedDateTime();
        ZonedDateTime cetZonedDateTime = utcZonedDateTime.withZoneSameInstant(ZoneId.of("Europe/Berlin"));

        return cetZonedDateTime.format(formatter);
    }
}
