package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BENUTZERVERWALTUNG_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BENUTZERVERWALTUNG_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BENUTZERVERWALTUNG_WRITE;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.Sets;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.security.BenutzerBerechtigungsAdapter;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdeGeloeschtEvent;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.security.SecuredAny;

@Service
@Transactional
public class BenutzerverwaltungService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BenutzerverwaltungService.class);

    private final KeycloakService keycloakService;
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final AktiverBenutzer bearbeiter;
    private final StammdatenService stammdatenService;
    private final PasswordGenerator passwordGenerator;

    public BenutzerverwaltungService(KeycloakService keycloakService,
            StammdatenBerechtigungsContext stammdatenBerechtigungsContext,
            StammdatenService stammdatenService, AktiverBenutzer bearbeiter,
            PasswordGenerator passwordGenerator) {
        this.keycloakService = keycloakService;
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.stammdatenService = stammdatenService;
        this.bearbeiter = bearbeiter;
        this.passwordGenerator = passwordGenerator;
    }

    @Secured(BENUTZERVERWALTUNG_READ)
    public Stream<Benutzer> listAllBetreiberBenutzer() {
        return keycloakService.listAllUsers("Betreiber").filter(this::canRead);
    }

    @Secured(BENUTZERVERWALTUNG_READ)
    public Stream<Benutzer> listAllBehoerdenBenutzer() {
        return keycloakService.listAllUsers("Behoerdenbenutzer").filter(this::canRead);
    }

    @Secured(BENUTZERVERWALTUNG_READ)
    public Benutzer loadBenutzerByName(String benutzername) {
        return keycloakService.findUserByUsername(benutzername)
                              .filter(this::canRead)
                              .orElseThrow(() -> new BubeEntityNotFoundException(Benutzer.class, benutzername));
    }

    @Secured(BENUTZERVERWALTUNG_READ)
    public Benutzer loadBenutzerById(String id) {
        Benutzer benutzer = keycloakService.getUserById(id);
        stammdatenBerechtigungsContext.checkAccessToRead(new BenutzerBerechtigungsAdapter(benutzer));
        return benutzer;
    }

    @SecuredAny
    public Benutzer loadCurrentBenutzer() {
        // Read-Recht wird nicht benötigt; @Secured greift nur bei Aufrufen außerhalb der Klasse
        return loadBenutzerByName(bearbeiter.getUsername());
    }

    @Secured(BENUTZERVERWALTUNG_WRITE)
    public Benutzer createBenutzer(Benutzer benutzer) {
        LOGGER.info("Creating new user " + benutzer.getBenutzername());
        stammdatenBerechtigungsContext.checkAccessToWrite(new BenutzerBerechtigungsAdapter(benutzer));
        pruefeRollen(benutzer, null);
        pruefeFachmoduleUndThemen(benutzer);
        pruefeBetriebsstaetten(benutzer);

        // Wenn keine E-Mail eingegeben wurde, wird ein Passwort generiert und an die UI geschickt
        if (!benutzer.hatEmail()) {
            LOGGER.info("No e-mail present for user {}. Assigning temporary password",
                    benutzer.getBenutzername());
            benutzer.setTempPassword(passwordGenerator.generatePassword());
        }
        Benutzer newBenutzer = keycloakService.createUser(benutzer);
        LOGGER.info("Created user %s successfully".formatted(benutzer.getBenutzername()));

        // Wenn der neue Benutzer eine E-Mail Adresse hat dann bekommt er auch gleich eine Passwort E-Mail
        if (benutzer.hatEmail()) {
            keycloakService.resetPassword(newBenutzer);
        }
        return newBenutzer;
    }

    @Secured(BENUTZERVERWALTUNG_WRITE)
    public Benutzer updateBenutzer(Benutzer benutzer) {
        stammdatenBerechtigungsContext.checkAccessToWrite(new BenutzerBerechtigungsAdapter(benutzer));
        pruefeFachmoduleUndThemen(benutzer);
        pruefeBetriebsstaetten(benutzer);
        // Datenberechtigung prüfen
        var existing = loadBenutzerById(benutzer.getId());
        stammdatenBerechtigungsContext.checkAccessToWrite(new BenutzerBerechtigungsAdapter(existing));
        pruefeRollen(benutzer, existing);
        return keycloakService.updateUser(benutzer);
    }

    @Secured(BENUTZERVERWALTUNG_WRITE)
    public void resetPassword(String benutzername) {
        var benutzer = loadBenutzerByName(benutzername);
        keycloakService.resetPassword(benutzer);
    }

    @SecuredAny
    public void resetOwnPassword() {
        // Write-Recht wird nicht benötigt; @Secured greift nur bei Aufrufen außerhalb der Klasse
        resetPassword(bearbeiter.getUsername());
    }

    @Secured(BENUTZERVERWALTUNG_WRITE)
    public void resetTwoFactor(String benutzername) {
        var benutzer = loadBenutzerByName(benutzername);
        keycloakService.resetTwoFactor(benutzer);
    }

    @SecuredAny
    public void resetOwnTwoFactor() {
        // Read-Recht wird nicht benötigt; @Secured greift nur bei Aufrufen außerhalb der Klasse
        resetTwoFactor(bearbeiter.getUsername());
    }

    @SecuredAny
    public Benutzer updateCurrentBenutzer(Benutzer b) {
        Assert.isTrue(b.getBenutzername().equals(bearbeiter.getUsername()),
                "Diese Methode darf nur vom eigenen Benutzer genutzt werden");
        // Write-Recht wird nicht benötigt; @Secured greift nur bei Aufrufen außerhalb der Klasse
        return updateBenutzer(b);
    }

    @Secured(BENUTZERVERWALTUNG_DELETE)
    public void deleteBenutzer(String benutzername) {
        if (benutzername.equals(bearbeiter.getUsername())) {
            throw new BenutzerException("Das eigene Benutzerprofil kann nicht gelöscht werden");
        }
        Benutzer benutzer = loadBenutzerByName(benutzername);
        keycloakService.deleteUser(benutzer.getId());
    }

    @SecuredAny
    public void updateProfile(String email) {
        var benutzer = loadCurrentBenutzer();
        benutzer.setEmail(email);
        updateCurrentBenutzer(benutzer);
    }

    @Secured(BENUTZERVERWALTUNG_DELETE)
    public boolean behoerdeStillUsed(Land land, String schluessel) {
        return listAllBehoerdenBenutzer().anyMatch(b -> b.hatBehoerde(land, schluessel));
    }

    @EventListener
    @Transactional
    public void handleBehoerdeGeloescht(BehoerdeGeloeschtEvent event) {
        if (behoerdeStillUsed(event.behoerde().getLand(), event.behoerde().getSchluessel())) {
            throw new ReferenzdatenException("Die Behörde wird noch verwendet und kann nicht gelöscht werden.");
        }
    }

    @Secured(BENUTZERVERWALTUNG_WRITE)
    public List<Benutzer> createTemporaryPasswordsForBenutzer(List<String> benutzerIds) {
        LOGGER.info("Creating temporary passwords for {} users", benutzerIds.size());
        var benutzerList = benutzerIds.stream().map(keycloakService::getUserById).toList();
        benutzerList.forEach(benutzer -> {
            if (!canRead(benutzer)) {
                throw new BubeEntityNotFoundException(Benutzer.class, "ID " + benutzer.getId());
            }
            if (!(benutzer.isBetreiberBenutzer() && benutzer.needsTempPassword())) {
                throw new BenutzerException("Passwort für Benutzer '%s' kann nicht zurückgesetzt werden".formatted(
                        benutzer.getBenutzername()));
            }
            benutzer.setTempPassword(passwordGenerator.generatePassword());
            keycloakService.updateTemporaryPassword(benutzer);
        });

        return benutzerList;
    }

    private void pruefeRollen(Benutzer benutzerUpdate, Benutzer existing) {
        if (benutzerUpdate.getZugewieseneRollen().isEmpty()) {
            throw new BenutzerException("Benutzer muss mindestens einer Rolle zugewiesen werden");
        }

        Set<Rolle> toAdd;
        Set<Rolle> toRemove;

        if (existing != null) {
            var currentRoles = EnumSet.copyOf(existing.getZugewieseneRollen());
            var updatedRoles = EnumSet.copyOf(benutzerUpdate.getZugewieseneRollen());
            toAdd = Sets.difference(updatedRoles, currentRoles);
            toRemove = Sets.difference(currentRoles, updatedRoles);
        } else {
            toAdd = Set.copyOf(benutzerUpdate.getZugewieseneRollen());
            toRemove = Collections.emptySet();
        }
        // TODO Rolle Betreiber darf nicht hinzugefügt oder entfernt werden
        toAdd.forEach(rolle -> {
            // Betreiber darf von allen zugewiesen werden, ansonsten müssen zugewiesene Rollen
            // Teilmenge der effektiven Rollen des Bearbeiter sein
            if (!Rolle.BETREIBER.equals(rolle) && !bearbeiter.hatRolle(rolle)) {
                throw new BenutzerException("Rolle " + rolle + " kann nicht zugewiesen werden");
            }
        });
        toRemove.forEach(rolle -> {
            // Betreiber darf von allen zugewiesen werden, ansonsten müssen zugewiesene Rollen
            // Teilmenge der effektiven Rollen des Bearbeiter sein
            if (!Rolle.BETREIBER.equals(rolle) && !bearbeiter.hatRolle(rolle)) {
                throw new BenutzerException("Rolle " + rolle + " kann nicht entfernt werden");
            }
        });
    }

    private void pruefeBetriebsstaetten(Benutzer benutzer) {
        var betriebsstaetten = benutzer.getDatenberechtigung().betriebsstaetten;
        if (benutzer.isBetreiberBenutzer() && betriebsstaetten.isEmpty()) {
            throw new BenutzerException("Betreiber muss mindestens einer Betriebsstätte zugeordnet werden");
        }

        if (!betriebsstaetten.isEmpty()) {
            Set<String> betriebsstaettenNummern = Set.copyOf(stammdatenService.allBetriebsstaettenNrForCurrentUser());
            if (!benutzer.allBstBerechtigungenContainedInIgnoreCase(betriebsstaettenNummern)) {
                throw new BenutzerException(String.format("Betriebsstätte(n) %s nicht gefunden oder keine Berechtigung",
                        String.join(", ", Sets.difference(betriebsstaetten, betriebsstaettenNummern))));
            }
        }
    }

    /**
     * Prüft, ob Fachmodule und Themen dem Benutzer vom bearbeiter zugewiesen werden dürfen.
     * Der Bearbeiter darf nur eine Teilmenge seiner Fachmodule und Themen zuweisen.
     * Leere Listen entsprechen der Berechtigung für alle Fachmodule bzw. Themen.
     *
     * @param benutzer
     */
    private void pruefeFachmoduleUndThemen(Benutzer benutzer) {
        var bearbeiterBerechtigung = this.bearbeiter.getBerechtigung();
        if (!bearbeiterBerechtigung.fachmodule.isEmpty()) {
            var benutzerDatenberechtigung = benutzer.getDatenberechtigung();
            if (benutzerDatenberechtigung.fachmodule.isEmpty()) {
                throw new BenutzerException("Es muss ein Fachmodul festgelegt werden");
            }
            benutzerDatenberechtigung.fachmodule.forEach(fachmodul -> {
                if (!bearbeiterBerechtigung.fachmodule.contains(fachmodul)) {
                    throw new BenutzerException("Fachmodul " + fachmodul + " kann nicht zugewiesen werden");
                }
            });
        }
        if (!bearbeiterBerechtigung.themen.isEmpty()) {
            var benutzerDatenberechtigung = benutzer.getDatenberechtigung();
            if (benutzerDatenberechtigung.themen.isEmpty()) {
                throw new BenutzerException("Es muss ein Thema vergeben werden");
            }
            benutzerDatenberechtigung.themen.forEach(thema -> {
                if (!bearbeiterBerechtigung.themen.contains(thema)) {
                    throw new BenutzerException("Thema " + thema + " kann nicht zugewiesen werden");
                }
            });
        }
    }

    private boolean canRead(Benutzer b) {
        return stammdatenBerechtigungsContext.hasAccessToRead(new BenutzerBerechtigungsAdapter(b));
    }

}
