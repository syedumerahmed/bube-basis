package de.wps.bube.basis.jobs.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import de.wps.bube.basis.jobs.domain.JobStatus;

public interface JobStatusRepository extends JpaRepository<JobStatus, String> {

    @Modifying
    String deleteByUsername(String username);

}
