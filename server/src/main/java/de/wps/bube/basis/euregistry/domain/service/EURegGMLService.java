package de.wps.bube.basis.euregistry.domain.service;

import java.io.OutputStream;
import javax.annotation.PostConstruct;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import de.wps.bube.basis.base.xml.ClasspathSchemaResolver;
import de.wps.bube.basis.base.xml.XMLExportException;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.EURegGMLWriter;
import de.wps.bube.basis.euregistry.xml.bericht.EURegMapper;
import de.wps.bube.basis.euregistry.xml.bericht.ReportDataMapper;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Service
@EnableConfigurationProperties(XeurProperties.class)
public class EURegGMLService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EURegGMLService.class);

    private final EURegistryService euRegistryService;
    private final EURegMapper euRegBerichtMapper;
    private final ReportDataMapper reportDataMapper;
    private final XeurProperties config;

    private Schema schema;

    public EURegGMLService(EURegistryService euRegistryService,
            EURegMapper euRegBerichtMapper, ReportDataMapper reportDataMapper, XeurProperties config) {
        this.euRegistryService = euRegistryService;
        this.euRegBerichtMapper = euRegBerichtMapper;
        this.reportDataMapper = reportDataMapper;
        this.config = config;
    }

    @PostConstruct
    void postConstruct() {
        LOGGER.info("Initializing EU-Schema");
        try {
            var schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            var resourceResolver = new ClasspathSchemaResolver("/xsd/xeur/");
            schemaFactory.setResourceResolver(resourceResolver);
            var source = this.getClass().getResourceAsStream("/xsd/EUReg2019-03-08.xsd");
            schema = schemaFactory.newSchema(new StreamSource(source));
        } catch (SAXException e) {
            throw new XMLExportException("Failed to initialize EUReg schema", e);
        }
        LOGGER.info("Initialized EU-Schema");
    }

    public Schema getSchema() {
        return schema;
    }

    public EURegGMLWriter createWriter(OutputStream outputStream, Referenz jahr, InspireParam inspireParam) {
        return new EURegGMLWriter(config.getUba(), euRegBerichtMapper, reportDataMapper, outputStream, jahr, schema,
                inspireParam, euRegistryService::findAllAnlagenberichteForBstBericht);
    }
}
