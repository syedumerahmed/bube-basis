package de.wps.bube.basis.datei.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class DeleteTempFilesJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteTempFilesJob.class);

    private final DateiService dateiService;

    public DeleteTempFilesJob(DateiService dateiService) {
        this.dateiService = dateiService;
    }

    // Täglich um 4 Uhr morgens
    @Scheduled(cron = "0 0 4 * * *")
    public void run() {
        LOGGER.info("Nightly Job to remove all temp files started.");
        dateiService.deleteTemporaryFiles();
    }

}
