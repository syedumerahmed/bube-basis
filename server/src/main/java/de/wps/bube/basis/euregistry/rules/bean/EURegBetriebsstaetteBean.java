package de.wps.bube.basis.euregistry.rules.bean;

import java.time.Instant;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;
import de.wps.bube.basis.stammdaten.rules.bean.StammdatenObjektBean;

@RegelApi.Type(name = "EURegBetriebsstätte")
public class EURegBetriebsstaetteBean implements PruefObjekt {

    private final Long id;
    private final BetriebsstaetteBean betriebsstaette;
    private final BerichtsdatenBean berichtsdaten;
    private final Instant ersteErfassung;
    private final Instant letzteAenderung;
    private final Supplier<EURegBetriebsstaetteBean> vorgaenger;
    private final Supplier<EURegBetriebsstaetteBean> nachfolger;
    private final Supplier<TeilberichtBean> teilbericht;

    public EURegBetriebsstaetteBean(Long id, BetriebsstaetteBean betriebsstaette, BerichtsdatenBean berichtsdaten,
            Instant ersteErfassung, Instant letzteAenderung, Supplier<EURegBetriebsstaetteBean> vorgaenger,
            Supplier<EURegBetriebsstaetteBean> nachfolger, Supplier<TeilberichtBean> teilbericht) {
        this.id = id;
        this.betriebsstaette = betriebsstaette;
        this.berichtsdaten = berichtsdaten;
        this.ersteErfassung = ersteErfassung;
        this.letzteAenderung = letzteAenderung;
        this.vorgaenger = Suppliers.memoize(vorgaenger::get);
        this.nachfolger = Suppliers.memoize(nachfolger::get);
        this.teilbericht = Suppliers.memoize(teilbericht::get);
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.EUREG_BST;
    }

    @RegelApi.Member(description = "Betriebsstätte")
    public BetriebsstaetteBean getBetriebsstaette() {
        return betriebsstaette;
    }

    public BerichtsdatenBean getBerichtsdaten() {
        return berichtsdaten;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public StammdatenObjektBean<?> getStammdatenObjekt() {
        return betriebsstaette;
    }

    @RegelApi.Member(description = "EuRegBst-Bericht des jeweiligen Vorjahres; muss in Land und LocalId übereinstimmen.")
    public EURegBetriebsstaetteBean getVorgaenger() {
        return vorgaenger.get();
    }

    @RegelApi.Member(description = "EuRegBst-Bericht des jeweiligen Folgejahres; muss in Land und LocalId übereinstimmen.")
    public EURegBetriebsstaetteBean getNachfolger() {
        return nachfolger.get();
    }

    @RegelApi.Member(description = """
            Aktueller EU-Registry-Teilbericht des Bundeslandes des aktuellen Betriebsstätten-Berichts, sofern letzterer
            im Teilbericht enthalten ist (anderenfalls ist das Attribut nicht gesetzt).""")
    public TeilberichtBean getTeilbericht() {
        return teilbericht.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        EURegBetriebsstaetteBean that = (EURegBetriebsstaetteBean) o;
        return Objects.equals(id, that.id) && Objects.equals(betriebsstaette,
                that.betriebsstaette) && Objects.equals(berichtsdaten,
                that.berichtsdaten) && Objects.equals(ersteErfassung,
                that.ersteErfassung) && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betriebsstaette, berichtsdaten, ersteErfassung, letzteAenderung);
    }

    @Override
    public String toString() {
        return "EURegBetriebsstaetteBean{" +
               "id=" + id +
               ", betriebsstaette=" + betriebsstaette +
               ", berichtsdaten=" + berichtsdaten +
               ", ersteErfassung=" + ersteErfassung +
               ", letzteAenderung=" + letzteAenderung +
               '}';
    }

}
