package de.wps.bube.basis.euregistry.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.euregistry.domain.event.BerichtPruefungEvent;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Component
public class BerichtPruefungListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BerichtPruefungListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final BerichtPruefungJob job;

    public BerichtPruefungListener(BenutzerJobRegistry jobRegistry, BerichtPruefungJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleBerichtPruefungEvent(BerichtPruefungEvent event) {
        LOGGER.info("Event handling started for '{}'", BerichtPruefungEvent.EVENT_NAME);
        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(BerichtPruefungJob.JOB_NAME));
        job.runAsync(event.ids(), event.username());
    }
}
