package de.wps.bube.basis.stammdaten.domain.vo;

import java.util.stream.Stream;

public enum Leistungsklasse {

    INSTALLIERT("installiert"),
    BETRIEBEN("betrieben"),
    GENEHMIGT("genehmigt");

    private final String bezeichner;

    Leistungsklasse(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static Leistungsklasse of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültige Leistungsklasse: " + bezeichner));
    }
}
