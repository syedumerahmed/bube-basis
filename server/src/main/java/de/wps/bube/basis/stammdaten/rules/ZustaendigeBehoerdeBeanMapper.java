package de.wps.bube.basis.stammdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.rules.BehoerdeBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.rules.bean.ZustaendigeBehoerdeBean;

@Mapper(uses = { BehoerdeBeanMapper.class, ReferenzBeanMapper.class })
public abstract class ZustaendigeBehoerdeBeanMapper {

    public abstract ZustaendigeBehoerdeBean toBean(ZustaendigeBehoerde zustaendigeBehoerde);

}
