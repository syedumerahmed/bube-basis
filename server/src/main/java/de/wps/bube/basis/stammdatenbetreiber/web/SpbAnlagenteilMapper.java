package de.wps.bube.basis.stammdatenbetreiber.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbAnlagenteilDto;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlagenteilListItemDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.web.VorschriftMapper;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlagenteilListItem;

@Mapper(uses = { ReferenzMapper.class, VorschriftMapper.class })
public abstract class SpbAnlagenteilMapper {

    @Mapping(target = "anlageNr", source = "anlagenteilNr")
    public abstract SpbAnlagenteilDto toDto(SpbAnlagenteil spbAnlagenteil);

    @Mapping(target = "anlagenteilNr", source = "anlageNr")
    public abstract SpbAnlagenteil fromDto(SpbAnlagenteilDto spbAnlagenteilDto);

    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    public abstract SpbAnlagenteilListItemDto toListItemDto(SpbAnlagenteilListItem spbAnlagenteilListItem);
}
