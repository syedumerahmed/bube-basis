package de.wps.bube.basis.benutzerverwaltung.domain.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerImportJobStatus;

public interface BenutzerImportJobStatusRepository extends JpaRepository<BenutzerImportJobStatus, String> {
}
