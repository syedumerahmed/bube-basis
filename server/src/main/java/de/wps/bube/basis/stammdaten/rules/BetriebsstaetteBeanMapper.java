package de.wps.bube.basis.stammdaten.rules;

import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.rules.AdresseBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.BehoerdeBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.KommunikationsverbindungBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;

@Mapper(uses = { ReferenzBeanMapper.class, BehoerdeBeanMapper.class, BetreiberBeanMapper.class, AdresseBeanMapper.class,
        AnsprechpartnerBeanMapper.class, KommunikationsverbindungBeanMapper.class, GeoPunktBeanMapper.class,
        ZustaendigeBehoerdeBeanMapper.class, QuelleBeanMapper.class, AnlageBeanMapper.class })
public abstract class BetriebsstaetteBeanMapper {

    @Autowired
    protected StammdatenService stammdatenService;

    @Mapping(target = "vorgaenger", expression = "java(vorgaenger(betriebsstaette, context, mappers))")
    @Mapping(target = "nachfolger", expression = "java(nachfolger(betriebsstaette, context, mappers))")
    public abstract BetriebsstaetteBean toBean(Betriebsstaette betriebsstaette, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers);

    public String map(EinleiterNr einleiterNr) {
        return einleiterNr.getEinleiterNr();
    }

    public String map(AbfallerzeugerNr abfallerzeugerNr) {
        return abfallerzeugerNr.getAbfallerzeugerNr();
    }

    protected Supplier<BetriebsstaetteBean> vorgaenger(Betriebsstaette betriebsstaette,
            BeanMappingContext context, StammdatenBeanMappers mappers) {
        return () -> {
            BeanMappingContext vorgaengerContext = context.vorgaenger();
            return getBetriebsstaetteBean(betriebsstaette, vorgaengerContext.getJahr(), vorgaengerContext, mappers);
        };
    }

    protected Supplier<BetriebsstaetteBean> nachfolger(Betriebsstaette betriebsstaette,
            BeanMappingContext context, StammdatenBeanMappers mappers) {
        return () -> {
            BeanMappingContext nachfolgerContext = context.nachfolger();
            return getBetriebsstaetteBean(betriebsstaette, nachfolgerContext.getJahr(), nachfolgerContext, mappers);
        };
    }

    private BetriebsstaetteBean getBetriebsstaetteBean(Betriebsstaette betriebsstaette, Gueltigkeitsjahr jahr,
            BeanMappingContext context, StammdatenBeanMappers mappers) {
        Betriebsstaette result = null;
        if (betriebsstaette.getLocalId() != null) {
            result = stammdatenService.findBetriebsstaetteByJahrLandLocalId(String.valueOf(jahr.getJahr()),
                    betriebsstaette.getLand(), betriebsstaette.getLocalId()).orElse(null);
        }
        if (result == null) {
            result = stammdatenService.findBetriebsstaetteByJahrLandNummer(String.valueOf(jahr.getJahr()),
                    betriebsstaette.getLand(), betriebsstaette.getBetriebsstaetteNr()).orElse(null);
        }
        return mappers.getBetriebsstaetteBeanMapper().toBean(result, context, mappers);
    }

}
