package de.wps.bube.basis.stammdatenuebernahme.domain.vo;

public class BetreiberUebernahme {
    private Long id;
    private Long spbId;
    private AdresseUebernahme adresse;
    private Boolean name;

    public BetreiberUebernahme() {
    }

    public BetreiberUebernahme(Long id, Long spbId, AdresseUebernahme adresse, Boolean name) {
        this.id = id;
        this.spbId = spbId;
        this.adresse = adresse;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public Boolean getName() {
        return name;
    }

    public void setName(Boolean name) {
        this.name = name;
    }

    public AdresseUebernahme getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseUebernahme adresse) {
        this.adresse = adresse;
    }
}
