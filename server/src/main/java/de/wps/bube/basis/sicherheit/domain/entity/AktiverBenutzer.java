package de.wps.bube.basis.sicherheit.domain.entity;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BETREIBER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static java.lang.String.format;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

import org.springframework.util.Assert;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

public class AktiverBenutzer {
    private final String username;
    private final Datenberechtigung berechtigung;
    private final Set<Rolle> rollen;

    public AktiverBenutzer(String username, Set<Rolle> rollen, Datenberechtigung berechtigung) {
        Assert.notNull(username, "username == null");
        Assert.notEmpty(rollen, "rollen.isEmpty()");
        Assert.notNull(berechtigung, "berechtigung == null");

        this.username = username;
        this.rollen = Collections.unmodifiableSet(EnumSet.copyOf(rollen));
        this.berechtigung = berechtigung;

        // CR für Prüfung im Frontend: BUBE-150
        Assert.isTrue(isGesamtadmin() == Land.DEUTSCHLAND.equals(getLand()),
                "Ausschließlich Gesamtadmins dürfen und müssen Land=Deutschland zugeordnet sein");
        Assert.isTrue(rollen.contains(BEHOERDENBENUTZER) || rollen.contains(BETREIBER),
                format("Eine der Rollen %s oder %s muss gewählt sein. Benutzername: '%s'",
                        BETREIBER, BEHOERDENBENUTZER, username));
    }

    public String getUsername() {
        return username;
    }

    public Datenberechtigung getBerechtigung() {
        return berechtigung;
    }

    public Land getLand() {
        return berechtigung.land;
    }

    public boolean hatRolle(Rolle rolle) {
        return rollen.contains(rolle);
    }

    public boolean hatEineDerRollen(Rolle... rollen) {
        return hatEineDerRollen(Arrays.asList(rollen));
    }

    public boolean hatEineDerRollen(Collection<Rolle> rollen) {
        return rollen.stream().anyMatch(this.rollen::contains);
    }

    public boolean hatAlleRollen(Rolle... rollen) {
        return hatAlleRollen(Set.of(rollen));
    }

    public boolean hatAlleRollen(Collection<Rolle> rollen) {
        return this.rollen.containsAll(rollen);
    }

    public boolean isBehoerdennutzer() {
        return hatRolle(BEHOERDENBENUTZER);
    }

    public boolean isGesamtadmin() {
        return hatRolle(GESAMTADMIN);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AktiverBenutzer aktiverBenutzer)) {
            return false;
        }
        return username.equals(aktiverBenutzer.username);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AktiverBenutzer.class.getSimpleName() + "[", "]")
                .add("name='" + username + "'")
                .add("rollen=" + rollen)
                .add("berechtigung=" + berechtigung)
                .toString();
    }
}
