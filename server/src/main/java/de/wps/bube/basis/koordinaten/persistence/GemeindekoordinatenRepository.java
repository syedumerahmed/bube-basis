package de.wps.bube.basis.koordinaten.persistence;

import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.koordinaten.domain.entity.Gemeindekoordinaten;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public interface GemeindekoordinatenRepository extends JpaRepository<Gemeindekoordinaten, String> {

    default boolean intersectsPointByAgs(Point point, int buffer, String ags, Long berichtsjahrId) {
        if (!sridExists(point.getSRID())) {
            throw new KoordinatenException("Unbekannte SRID %s".formatted(point.getSRID()));
        }
        Coordinate coordinate = point.getCoordinate();
        return this.intersectsCoordinatesByAgs(Double.valueOf(coordinate.y).longValue(),
                Double.valueOf(coordinate.x).longValue(), point.getSRID(), buffer, ags, berichtsjahrId);
    }

    @Query(nativeQuery = true, value = "SELECT EXISTS(SELECT srid FROM spatial_ref_sys WHERE srid = :srid)")
    boolean sridExists(int srid);

    /* Die Koordinatentransformation funktioniert bei Gauß-Krüger nur mit dem EPSG-Code 5677 nicht mit 31467. */
    @Query(nativeQuery = true, value = "SELECT EXISTS(SELECT * FROM Gemeindekoordinaten WHERE ST_DWithin( ST_Transform( ST_SetSRID( ST_Point(:ost, :nord), :srid), 25832), geom, :buffer) AND ags = :ags AND berichtsjahr_id = :berichtsjahrId)")
    boolean intersectsCoordinatesByAgs(Long nord, Long ost, int srid, int buffer, String ags, Long berichtsjahrId);

    boolean existsByAgs(String ags);

    @Query(value = "SELECT g.berichtsjahr FROM Gemeindekoordinaten g WHERE g.ags = :ags ORDER BY g.berichtsjahr.schluessel DESC")
    List<Referenz> findAllBerichtsjahrByAgs(String ags);

    void deleteAllByBerichtsjahr(Referenz berichtsjahr);
}
