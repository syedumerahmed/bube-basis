package de.wps.bube.basis.stammdaten.domain.service;

import static java.lang.String.format;

import java.io.IOException;
import java.io.Writer;

import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungenMap;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;

public class BetriebsstaettePruefungProtokollGenerator {

    public static final int EINZUG = 2;

    private final Betriebsstaette betriebsstaette;
    private final Komplexpruefung komplexpruefung;
    private final KomplexpruefungService komplexpruefungService;

    private boolean protokolliereHeader = true;
    private RegelPruefungenMap regelPruefungenMap;

    public BetriebsstaettePruefungProtokollGenerator(Betriebsstaette betriebsstaette, Komplexpruefung komplexpruefung,
            KomplexpruefungService komplexpruefungService) {
        this.betriebsstaette = betriebsstaette;
        this.komplexpruefung = komplexpruefung;
        this.komplexpruefungService = komplexpruefungService;
    }

    public void setProtokolliereHeader(boolean protokolliereHeader) {
        this.protokolliereHeader = protokolliereHeader;
    }

    public void protokolliereBetriebsstaette(Writer out) throws IOException {
        regelPruefungenMap = komplexpruefungService.readRegelPruefungen(komplexpruefung);

        if (protokolliereHeader) {
            protokolliereHeader(out, betriebsstaette.getBerichtsjahr());
        }

        out.write(format("Betriebsstätte: %s | %s", betriebsstaette.getBetriebsstaetteNr(), betriebsstaette.getName()));
        out.write("\n");
        protokolliereRegelPruefung(out, betriebsstaette.getId(), RegelObjekt.BST, 0);

        if (betriebsstaette.getBetreiber() != null) {
            protokolliereBetreiber(out, betriebsstaette.getBetreiber());
        }

        protokolliereBetriebsstaetteAbhaengigkeiten(out);
    }

    protected void protokolliereBetriebsstaetteAbhaengigkeiten(Writer out) throws IOException {
        for (Anlage anlage : betriebsstaette.getAnlagen()) {
            protokolliereAnlage(out, anlage);
        }

        for (Quelle quelle : betriebsstaette.getQuellen()) {
            protokolliereQuelle(out, quelle, false);
        }
    }

    protected void protokolliereHeader(Writer out, Referenz berichtsjahr) throws IOException {
        out.write(format("Komplexprüfung Start: %s Jahr: %s",
                DateTime.atCET(komplexpruefung.getAusfuehrung()),
                berichtsjahr.getSchluessel()));
        out.write("\n");
        out.write(format("Das Protokoll enthält %d Fehler und %d Warnungen.", komplexpruefung.getAnzahlFehler(),
                komplexpruefung.getAnzahlWarnungen()));
        out.write("\n");
        out.write("\n");
    }

    protected void protokolliereBetreiber(Writer out, Betreiber betreiber) throws IOException {
        out.write(format("Betreiber: %s | %s", betreiber.getBetreiberNummer(), betreiber.getName()).indent(EINZUG));
        protokolliereRegelPruefung(out, betreiber.getId(), RegelObjekt.BETREIBER, EINZUG);
    }

    protected void protokolliereAnlage(Writer out, Anlage anlage) throws IOException {
        out.write(format("Anlage: %s | %s", anlage.getAnlageNr(), anlage.getName()).indent(EINZUG));
        protokolliereRegelPruefung(out, anlage.getId(), RegelObjekt.ANLAGE, EINZUG);

        protokolliereAnlageAbhaengigkeiten(out, anlage);
    }

    protected void protokolliereAnlageAbhaengigkeiten(Writer out, Anlage anlage) throws IOException {
        for (Anlagenteil anlagenteil : anlage.getAnlagenteile()) {
            protokolliereAnlagenteil(out, anlagenteil);
        }

        for (Quelle quelle : anlage.getQuellen()) {
            protokolliereQuelle(out, quelle, true);
        }
    }

    protected void protokolliereAnlagenteil(Writer out, Anlagenteil anlagenteil) throws IOException {
        int einzug = 2 * EINZUG;
        out.write(format("Anlagenteil: %s | %s", anlagenteil.getAnlagenteilNr(), anlagenteil.getName())
                .indent(einzug));
        protokolliereRegelPruefung(out, anlagenteil.getId(), RegelObjekt.ANLAGENTEIL, einzug);
    }

    protected void protokolliereQuelle(Writer out, Quelle quelle, boolean anlQuelle) throws IOException {
        int einzug = anlQuelle ? 2 * EINZUG : EINZUG;
        out.write(format("Quelle: %s | %s", quelle.getQuelleNr(), quelle.getName()).indent(einzug));
        protokolliereRegelPruefung(out, quelle.getId(), RegelObjekt.QUELLE, einzug);
    }

    protected void protokolliereRegelPruefung(Writer out, Long pruefObjektId, RegelObjekt regelObjekt,
            int einzug) throws IOException {

        if (!regelPruefungenMap.has(pruefObjektId, regelObjekt)) {
            return;
        }

        for (RegelPruefung regelPruefung : regelPruefungenMap.get(pruefObjektId, regelObjekt)) {
            out.write(regelPruefung.getFormattedMessage().indent(einzug));
        }
    }

}
