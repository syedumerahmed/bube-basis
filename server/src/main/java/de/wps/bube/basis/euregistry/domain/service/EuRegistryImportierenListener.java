package de.wps.bube.basis.euregistry.domain.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.euregistry.domain.event.EuRegistryImportTriggeredEvent;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Component
@Transactional
public class EuRegistryImportierenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EuRegistryImportierenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final EuRegistryImportierenJob job;

    public EuRegistryImportierenListener(BenutzerJobRegistry jobRegistry, EuRegistryImportierenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleEuRegistryImportieren(EuRegistryImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", EuRegistryImportierenJob.JOB_NAME);

        jobRegistry.jobStarted(new JobStatus(EuRegistryImportierenJob.JOB_NAME));

        job.runAsync(event.berichtsjahr());
    }
}
