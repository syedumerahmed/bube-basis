package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import java.util.Collections;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlagenteilVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class AnlagenteilVergleichService {
    private final CommonVergleichService vergleichService;

    public AnlagenteilVergleichService(
            CommonVergleichService vergleichService) {
        this.vergleichService = vergleichService;
    }

    public SpbAnlagenteilVergleich vergleiche(Anlagenteil spl, SpbAnlagenteil spb) {
        SpbAnlagenteilVergleich vergleich = new SpbAnlagenteilVergleich();

        vergleich.setId(spl == null ? null : spl.getId());
        vergleich.setSpbId(spb == null ? null : spb.getId());
        vergleich.setName(new StringVector(spl == null ? null : spl.getName(),
                spb == null ? null : spb.getName()));

        if (spl == null || !spl.isBetreiberdatenUebernommen()) {
            vergleich.setAnlagenteilNr(new StringVector(spl == null ? null : spl.getAnlagenteilNr(),
                    spb == null ? null : spb.getAnlagenteilNr()));

            vergleich.setGeoPunkt(new GeoPunktVector(spl == null ? null : spl.getGeoPunkt(),
                    spb == null ? null : spb.getGeoPunkt()));

            vergleich.setLeistungen(vergleichService.mapLeistungen(
                    spl == null ? Collections.emptyList() : spl.getLeistungen(),
                    spb == null ? Collections.emptyList() : spb.getLeistungen()));

            vergleich.setBetriebsstatus(
                    new ReferenzVector(spl == null ? null : spl.getBetriebsstatus(),
                            spb == null ? null : spb.getBetriebsstatus()));

            vergleich.setBetriebsstatusSeit(
                    new LocalDateVector(spl == null ? null : spl.getBetriebsstatusSeit(),
                            spb == null ? null : spb.getBetriebsstatusSeit()));
            vergleich.setInbetriebnahme(
                    new LocalDateVector(spl == null ? null : spl.getInbetriebnahme(),
                            spb == null ? null : spb.getInbetriebnahme()));

        }

        if (spb == null) {
            vergleich.setHasChanges(false);
        } else {
            vergleich.setHasChanges(
                    spl == null || (!spl.isBetreiberdatenUebernommen() && !allIsEqual(vergleich)));
        }
        return vergleich;
    }

    private boolean allIsEqual(SpbAnlagenteilVergleich vergleich) {
        return vergleich.getAnlagenteilNr().isEqual()
                && vergleich.getBetriebsstatus().isEqual()
                && vergleich.getBetriebsstatusSeit().isEqual()
                && vergleich.getGeoPunkt().isEqual()
                && vergleich.getInbetriebnahme().isEqual()
                && vergleich.getLeistungen().stream().allMatch(LeistungVector::isEqual)
                && vergleich.getName().isEqual();
    }
}
