package de.wps.bube.basis.referenzdaten.persistence;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import de.wps.bube.basis.base.vo.Land;

@Converter(autoApply = true)
public class LandConverter implements AttributeConverter<Land, String> {

    @Override
    public String convertToDatabaseColumn(final Land land) {
        if (land == null) {
            return null;
        }
        return land.getNr();
    }

    @Override
    public Land convertToEntityAttribute(final String landNr) {
        if (landNr == null) {
            return null;
        }

        return Land.of(landNr);
    }
}
