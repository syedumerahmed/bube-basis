package de.wps.bube.basis.euregistry.web.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteInfoDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Mapper(uses = { BerichtsdatenMapper.class })
public abstract class EuRegBetriebsstaetteMapper {

    @Autowired
    private EURegistryService service;

    @Mapping(target = "betriebsstaetteInfo", source = "betriebsstaette")
    @Mapping(target = "canWrite", ignore = true)
    public abstract EuRegBetriebsstaetteDto toDto(EURegBetriebsstaette euRegBetriebsstaette);

    @Mapping(target = "betriebsstaette", ignore = true)
    @Mapping(target = "komplexpruefung", ignore = true)
    public abstract EURegBetriebsstaette fromDto(EuRegBetriebsstaetteDto dto);

    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    @Mapping(target = "ort", source = "adresse.ort")
    @Mapping(target = "vorschriften", qualifiedByName = "vorschriftToKText")
    @Mapping(target = "land", source = "land.nr")
    @Mapping(target = "berichtsjahr", source = "berichtsjahr.schluessel")
    public abstract BetriebsstaetteInfoDto toDto(Betriebsstaette bst);

    @AfterMapping
    void canWrite(EURegBetriebsstaette source, @MappingTarget EuRegBetriebsstaetteDto dto) {
        try {
            dto.setCanWrite(service.canWrite(source));
        } catch (AccessDeniedException e) {
            dto.setCanWrite(false);
        }
    }

    @Named("vorschriftToKText")
    String vorschriftToKText(Referenz vorschrift) {
        return vorschrift != null ? vorschrift.getKtext() : null;
    }
}
