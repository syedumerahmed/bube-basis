package de.wps.bube.basis.referenzdaten.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.api.ParameterApi;
import de.wps.bube.basis.base.web.openapi.model.ParameterDto;
import de.wps.bube.basis.base.web.openapi.model.ParameterWertDto;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Controller
class ParameterRestController implements ParameterApi {

    private final ParameterService parameterService;
    private final ParameterMapper parameterMapper;
    private final ParameterWertMapper mapper;

    ParameterRestController(ParameterService parameterService, ParameterMapper parameterMapper,
            ParameterWertMapper mapper) {
        this.parameterService = parameterService;
        this.parameterMapper = parameterMapper;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<ParameterDto>> readParameterliste() {
        return ResponseEntity.ok(parameterService.readParameterliste()
                                                 .stream()
                                                 .map(parameterMapper::toDto)
                                                 .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<ParameterDto> readParameter(String schluessel) {
        return ResponseEntity.ok(parameterMapper.toDto(parameterService.readParameter(schluessel)));
    }

    @Override
    public ResponseEntity<List<ParameterWertDto>> readParameterwerteForEdit(String schluessel) {
        return ResponseEntity.ok(parameterService.readParameterwerteForEdit(schluessel)
                                                 .stream()
                                                 .map(mapper::toDto)
                                                 .toList());
    }

    @Override
    public ResponseEntity<ParameterWertDto> createParameterWert(ParameterWertDto parameterWertDto) {
        ParameterWert parameterWert = parameterService.createParameterWert(mapper.fromDto(parameterWertDto));
        return ResponseEntity.ok(mapper.toDto(parameterWert));
    }

    @Override
    public ResponseEntity<ParameterWertDto> updateParameterWert(ParameterWertDto parameterWertDto) {
        ParameterWert parameterWert = parameterService.updateParameterWert(mapper.fromDto(parameterWertDto));
        return ResponseEntity.ok(mapper.toDto(parameterWert));
    }

    @Override
    public ResponseEntity<Void> deleteParameterWert(Long id) {
        parameterService.deleteParameterWert(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<ParameterWertDto> readParameterWert(String schluessel, String jahr,
            String landNr) {
        return ResponseEntity.ok(mapper.toDto(
                parameterService.readParameterWert(schluessel, Gueltigkeitsjahr.of(jahr), Land.of(landNr))));
    }

    @Override
    public ResponseEntity<ParameterWertDto> readParameterWertOptional(String schluessel, String jahr, String landNr) {
        return ResponseEntity.ok(
                mapper.toDto(parameterService.findParameterWert(schluessel, Gueltigkeitsjahr.of(jahr), Land.of(landNr))
                                             .orElse(null)));

    }

    @Override
    public ResponseEntity<ParameterWertDto> readParameterWertBehoerdeOptional(String schluessel, String jahr,
            String landNr, Long behoerdeId) {
        return ResponseEntity.ok(
                mapper.toDto(
                        parameterService.findParameterWert(schluessel, Gueltigkeitsjahr.of(jahr), Land.of(landNr),
                                                behoerdeId)
                                        .orElse(null)));

    }

}
