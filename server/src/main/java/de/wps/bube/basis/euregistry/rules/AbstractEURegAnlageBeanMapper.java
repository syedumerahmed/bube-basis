package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Context;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.rules.bean.EURegAnlageBean;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;

public abstract class AbstractEURegAnlageBeanMapper<B extends EURegAnlageBean> {

    @Autowired
    protected EURegistryService euRegistryService;

    protected void afterMapping(EURegAnlage euRegAnlage, @MappingTarget B euRegAnlageBean,
            @Context BeanMappingContext context, @Context EURegBeanMappers mappers) {
        euRegAnlageBean.setEuRegBetriebsstaette(
                () -> {
                    AnlageBean anlage = euRegAnlageBean.getAnlage() != null ?
                            euRegAnlageBean.getAnlage() :
                            euRegAnlageBean.getAnlagenteil().getAnlage();
                    BetriebsstaetteBean betriebsstaette = anlage.getBetriebsstaette();
                    return euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())
                                            .map(b -> mappers.getEuRegBetriebsstaetteBeanMapper().toBean(b,
                                                    context, mappers))
                                            .orElse(null);
                }
        );
        if (euRegAnlage.getAnlage() != null) {
            euRegAnlageBean.setAnlage(mappers.getAnlageBeanMapper().toBean(euRegAnlage.getAnlage(), context, mappers));
        }
        if (euRegAnlage.getAnlagenteil() != null) {
            euRegAnlageBean.setAnlagenteil(mappers.getAnlagenteilBeanMapper().toBean(euRegAnlage.getAnlagenteil(),
                    context, mappers));
        }
    }

}
