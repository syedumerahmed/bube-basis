package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDE_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlageVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlagenteilVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetreiberVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetriebsstaetteVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbQuelleVergleich;

@Service
public class StammdatenVergleichService {
    private final StammdatenService stammdatenService;
    private final StammdatenBetreiberService stammdatenBetreiberService;
    private final AnlageVergleichService anlageVergleichService;
    private final AnlagenteilVergleichService anlagenteilVergleichService;
    private final QuelleVergleichService quelleVergleichService;
    private final BetreiberVergleichService betreiberVergleichService;
    private final BetriebsstaetteVergleichService betriebsstaetteVergleichService;

    public StammdatenVergleichService(StammdatenService stammdatenService,
            StammdatenBetreiberService stammdatenBetreiberService,
            AnlageVergleichService anlageVergleichService,
            AnlagenteilVergleichService anlagenteilVergleichService,
            QuelleVergleichService quelleVergleichService,
            BetreiberVergleichService betreiberVergleichService,
            BetriebsstaetteVergleichService betriebsstaetteVergleichService) {
        this.stammdatenService = stammdatenService;
        this.stammdatenBetreiberService = stammdatenBetreiberService;
        this.anlageVergleichService = anlageVergleichService;
        this.anlagenteilVergleichService = anlagenteilVergleichService;
        this.quelleVergleichService = quelleVergleichService;
        this.betreiberVergleichService = betreiberVergleichService;
        this.betriebsstaetteVergleichService = betriebsstaetteVergleichService;
    }

    @Transactional(readOnly = true)
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbAnlageVergleich getAnlageVergleich(Long id) {
        Anlage anlage = stammdatenService.loadAnlage(id);
        SpbAnlage spbAnlage = stammdatenBetreiberService.findSpbAnlageByAnlageId(id)
                .orElse(null);

        return anlageVergleichService.vergleiche(anlage, spbAnlage);
    }

    @Transactional(readOnly = true)
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbAnlagenteilVergleich getAnlagenteilVergleich(Long id) {
        Anlagenteil anlagenteil = stammdatenService.loadAnlagenteil(id);
        SpbAnlagenteil spbAnlagenteil = stammdatenBetreiberService.findSpbAnlagenteilByAnlagenteilId(anlagenteil.getId()).orElse(null);

        return anlagenteilVergleichService.vergleiche(anlagenteil, spbAnlagenteil);
    }

    @Transactional(readOnly = true)
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbQuelleVergleich getQuelleVergleich(Long id) {
        Quelle quelle = stammdatenService.loadQuelle(id);
        SpbQuelle spbQuelle = stammdatenBetreiberService.findSpbQuelleByQuelleId(id)
                                                        .orElse(null);

        return quelleVergleichService.vergleiche(quelle, spbQuelle);
    }

    @Transactional(readOnly = true)
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbBetreiberVergleich getBetreiberVergleich(Long id) {
        Betreiber betreiber = stammdatenService.loadBetreiber(id);
        SpbBetreiber spbBetreiber = stammdatenBetreiberService.findSpbBetreiberByBetreiberId(id)
                                                              .orElse(null);

        return betreiberVergleichService.vergleiche(betreiber, spbBetreiber);

    }

    @Transactional(readOnly = true)
    @Secured({ LAND_READ, BEHOERDE_READ })
    public SpbBetriebsstaetteVergleich getBetriebsstaettenVergleich(Long id) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(id);
        SpbBetriebsstaette spbBetriebsstaette = stammdatenBetreiberService.findSpbBetriebsstaetteByBetriebsstaetteId(
                id).orElse(null);

        return betriebsstaetteVergleichService.vergleiche(betriebsstaette, spbBetriebsstaette);
    }
}
