package de.wps.bube.basis.jobs.web;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.BenutzerJobApi;
import de.wps.bube.basis.base.web.openapi.model.JobStatusDto;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;

@Controller
class BenutzerJobRestController implements BenutzerJobApi {

    private final BenutzerJobRegistry benutzerJobRegistry;
    private final JobStatusMapper mapper;

    public BenutzerJobRestController(BenutzerJobRegistry benutzerJobRegistry, JobStatusMapper mapper) {
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<JobStatusDto> readJobStatus() {
        return ResponseEntity.ok(mapper.toDto(benutzerJobRegistry.getJobStatus().orElse(null)));
    }

}
