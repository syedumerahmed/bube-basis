package de.wps.bube.basis.koordinaten.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Component
public class VG250ImportListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(VG250ImportListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final VG250ImportJob job;

    public VG250ImportListener(BenutzerJobRegistry jobRegistry, VG250ImportJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleVG250Import(VG250ImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", VG250ImportTriggeredEvent.EVENT_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(VG250ImportJob.JOB_NAME));

        job.runAsync(event.downloadLink(), event.berichtsjahr());
    }
}
