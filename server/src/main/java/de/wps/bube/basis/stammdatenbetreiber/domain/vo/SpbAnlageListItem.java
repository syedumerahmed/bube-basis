package de.wps.bube.basis.stammdatenbetreiber.domain.vo;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class SpbAnlageListItem {

    private final Long spbAnlageId;
    private final String anlageNr;
    private final String name;
    private final Referenz betriebsstatus;
    private final boolean anlageVorhanden;
    private final Long anlageId;

    public SpbAnlageListItem(Long spbAnlageId, String anlageNr, String name, Referenz betriebsstatus,
            boolean anlageVorhanden, Long anlageId) {
        this.spbAnlageId = spbAnlageId;
        this.anlageNr = anlageNr;
        this.name = name;
        this.betriebsstatus = betriebsstatus;
        this.anlageVorhanden = anlageVorhanden;
        this.anlageId = anlageId;
    }

    public Long getSpbAnlageId() {
        return spbAnlageId;
    }

    public String getAnlageNr() {
        return anlageNr;
    }

    public String getName() {
        return name;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public boolean isAnlageVorhanden() {
        return anlageVorhanden;
    }

    public Long getAnlageId() {
        return anlageId;
    }

    public boolean isVeraendert() {
        return spbAnlageId != null && anlageVorhanden;
    }

    public boolean isNeu() {
        return spbAnlageId != null && !anlageVorhanden;
    }
}
