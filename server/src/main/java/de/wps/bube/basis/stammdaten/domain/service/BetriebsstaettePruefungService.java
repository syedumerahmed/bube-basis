package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe.GRUPPE_A;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingException;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.rules.StammdatenBeanMappers;
import de.wps.bube.basis.stammdaten.rules.bean.AnlagenteilBean;
import de.wps.bube.basis.stammdaten.rules.bean.QuelleBean;

@Service
public class BetriebsstaettePruefungService {

    private final StammdatenService stammdatenService;
    private final KomplexpruefungService komplexpruefungService;
    private final StammdatenBeanMappers stammdatenBeanMappers;

    public BetriebsstaettePruefungService(StammdatenService stammdatenService,
            KomplexpruefungService komplexpruefungService,
            StammdatenBeanMappers stammdatenBeanMappers) {
        this.stammdatenService = stammdatenService;
        this.komplexpruefungService = komplexpruefungService;
        this.stammdatenBeanMappers = stammdatenBeanMappers;
    }

    @Transactional(noRollbackFor = BeanMappingException.class)
    @Secured(BEHOERDENBENUTZER)
    public void komplexpruefung(Betriebsstaette betriebsstaette) {
        if (betriebsstaette.getKomplexpruefung() != null) {
            komplexpruefungService.delete(betriebsstaette.getKomplexpruefung());
        }

        Komplexpruefung komplexpruefung = komplexpruefungService.save(new Komplexpruefung());
        betriebsstaette.setKomplexpruefung(komplexpruefung);

        EnumMap<RegelObjekt, List<? extends PruefObjekt>> pruefObjektCache = new EnumMap<>(RegelObjekt.class);

        komplexpruefungService.komplexpruefung(List.of(GRUPPE_A), komplexpruefung,
                betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(),
                regelObjekt -> getPruefObjekte(betriebsstaette, regelObjekt, pruefObjektCache)
        );

        stammdatenService.updateKomplexpruefung(betriebsstaette.getId(), komplexpruefung.hatFehler());
    }

    public List<PruefObjekt> getPruefObjekte(Betriebsstaette betriebsstaette, RegelObjekt regelObjekt,
            Map<RegelObjekt, List<? extends PruefObjekt>> pruefObjektCache) {

        LinkedList<PruefObjekt> pruefObjekte = new LinkedList<>();
        BeanMappingContext beanMappingContext = new BeanMappingContext(betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        switch (regelObjekt) {
        case BST -> pruefObjekte.addAll(pruefObjektCache.computeIfAbsent(regelObjekt,
                r -> List.of(stammdatenBeanMappers.getBetriebsstaetteBeanMapper().toBean(betriebsstaette,
                        beanMappingContext, stammdatenBeanMappers))));
        case ANLAGE -> pruefObjekte.addAll(pruefObjektCache.computeIfAbsent(regelObjekt,
                r -> betriebsstaette.getAnlagen()
                                    .stream()
                                    .map(anlage -> stammdatenBeanMappers.getAnlageBeanMapper()
                                                                        .toBean(anlage, beanMappingContext,
                                                                                stammdatenBeanMappers))
                                    .toList()
        ));
        case ANLAGENTEIL -> {
            if (!pruefObjektCache.containsKey(regelObjekt)) {
                List<AnlagenteilBean> anlagenteile = new ArrayList<>();
                for (Anlage anlage : betriebsstaette.getAnlagen()) {
                    for (Anlagenteil anlagenteil : anlage.getAnlagenteile()) {
                        AnlagenteilBean anlagenteilBean = stammdatenBeanMappers.getAnlagenteilBeanMapper().toBean(
                                anlagenteil, beanMappingContext, stammdatenBeanMappers);
                        anlagenteile.add(anlagenteilBean);
                    }
                }
                pruefObjektCache.put(regelObjekt, anlagenteile);
            }
            pruefObjekte.addAll(pruefObjektCache.get(regelObjekt));
        }
        case QUELLE -> {
            if (!pruefObjektCache.containsKey(regelObjekt)) {
                List<QuelleBean> quellen = new ArrayList<>();
                for (Quelle quelle : betriebsstaette.getQuellen()) {
                    QuelleBean quelleBean = stammdatenBeanMappers.getQuelleBeanMapper()
                                                                 .toBean(quelle, beanMappingContext,
                                                                         stammdatenBeanMappers);
                    quellen.add(quelleBean);
                }
                for (Anlage anlage : betriebsstaette.getAnlagen()) {
                    for (Quelle quelle : anlage.getQuellen()) {
                        QuelleBean quelleBean = stammdatenBeanMappers.getQuelleBeanMapper().toBean(quelle,
                                beanMappingContext, stammdatenBeanMappers);
                        quellen.add(quelleBean);
                    }
                }
                pruefObjektCache.put(regelObjekt, quellen);
            }
            pruefObjekte.addAll(pruefObjektCache.get(regelObjekt));
        }
        case BETREIBER -> {
            if (betriebsstaette.hatBetreiber()) {
                pruefObjekte.addAll(pruefObjektCache.computeIfAbsent(regelObjekt,
                        r -> List.of(stammdatenBeanMappers.getBetreiberBeanMapper()
                                                          .toBean(betriebsstaette.getBetreiber()))));
            }
        }
        default -> throw new IllegalArgumentException(
                "RegelObjekt " + regelObjekt + " darf von Regeln der Gruppe A nicht verwendet werden.");
        }
        return pruefObjekte;
    }

}
