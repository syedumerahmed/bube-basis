package de.wps.bube.basis.euregistry.web;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.XeurBerichtApi;
import de.wps.bube.basis.base.web.openapi.model.XeurTeilberichtErstellenCommandDto;
import de.wps.bube.basis.base.web.openapi.model.XeurTeilberichtListItemDto;
import de.wps.bube.basis.euregistry.domain.service.XeurTeilberichtCrudService;
import de.wps.bube.basis.euregistry.domain.service.XeurTeilberichtService;
import de.wps.bube.basis.euregistry.web.mapper.EuRegCommandMapper;
import de.wps.bube.basis.euregistry.web.mapper.XeurTeilberichtListItemMapper;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;

@Controller
class XeurBerichtController implements XeurBerichtApi {

    private final EuRegCommandMapper euregCommandMapper;
    private final XeurTeilberichtCrudService xeurTeilberichtCrudService;
    private final XeurTeilberichtService xeurTeilberichtService;
    private final XeurTeilberichtListItemMapper xeurTeilberichtListItemMapper;
    private final ReferenzenService referenzenService;

    XeurBerichtController(EuRegCommandMapper euregCommandMapper,
            XeurTeilberichtCrudService xeurTeilberichtCrudService,
            XeurTeilberichtService xeurTeilberichtService,
            XeurTeilberichtListItemMapper xeurTeilberichtListItemMapper,
            ReferenzenService referenzenService) {
        this.euregCommandMapper = euregCommandMapper;
        this.xeurTeilberichtCrudService = xeurTeilberichtCrudService;
        this.xeurTeilberichtService = xeurTeilberichtService;
        this.xeurTeilberichtListItemMapper = xeurTeilberichtListItemMapper;
        this.referenzenService = referenzenService;
    }

    @Override
    public ResponseEntity<List<XeurTeilberichtListItemDto>> getAll(String jahr) {
        return ResponseEntity.ok(xeurTeilberichtCrudService
                .getAllTeilberichte(referenzenService.getJahr(jahr))
                .stream()
                .map(xeurTeilberichtListItemMapper::toDto)
                .toList());
    }

    @Override
    public ResponseEntity<Void> erstelleXeurTeilbericht(XeurTeilberichtErstellenCommandDto xeurCommand) {
        xeurTeilberichtService.erstelleXeurTeilbericht(euregCommandMapper.fromDto(xeurCommand));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> downloadTeilbericht(Long xeurTeilberichtId) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        xeurTeilberichtCrudService.downloadTeilbericht(outputStream, xeurTeilberichtId);

        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType("application/zip"))
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"teilbericht.zip\"")
                             .body(byteArrayResource);
    }

    @Override
    public ResponseEntity<Void> teilberichtFreigeben(Long xeurTeilberichtId) {
        xeurTeilberichtService.teilberichtFreigeben(xeurTeilberichtId);
        return ResponseEntity.ok().build();
    }
}
