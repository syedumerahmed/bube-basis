package de.wps.bube.basis.referenzdaten.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.ParameterDto;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;

@Mapper
public interface ParameterMapper {

    ParameterDto toDto(Parameter parameter);

    Parameter fromDto(ParameterDto parameterDto);

}
