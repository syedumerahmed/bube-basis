package de.wps.bube.basis.stammdaten.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.annotation.Secured;

import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Secured({ Rollen.BEHOERDE_DELETE, Rollen.LAND_DELETE })
public @interface SecuredStammdatenDelete {
}
