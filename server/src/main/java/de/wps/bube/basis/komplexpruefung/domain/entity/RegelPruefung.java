package de.wps.bube.basis.komplexpruefung.domain.entity;

import static javax.persistence.EnumType.STRING;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;

@Entity
@Table(name = "regel_pruefung")
public class RegelPruefung {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "regel_pruefung_seq")
    @SequenceGenerator(name = "regel_pruefung_seq", allocationSize = 50)
    private Long id;

    @ManyToOne
    private Komplexpruefung komplexpruefung;

    private Long regelNummer;

    @Enumerated(STRING)
    private RegelObjekt regelObjekt;

    private Long pruefObjektId;

    @NotNull
    @Enumerated(STRING)
    private RegelTestStatus status;

    @Column(name = "message")
    private String message;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RegelTyp typ;

    @Deprecated
    protected RegelPruefung() {
    }

    public RegelPruefung(Komplexpruefung komplexpruefung, Long regelNummer, RegelTestStatus status) {
        this.komplexpruefung = komplexpruefung;
        this.regelNummer = regelNummer;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Komplexpruefung getKomplexpruefung() {
        return komplexpruefung;
    }

    public void setKomplexpruefung(Komplexpruefung komplexpruefung) {
        this.komplexpruefung = komplexpruefung;
    }

    public Long getRegelNummer() {
        return regelNummer;
    }

    public void setRegelNummer(Long regelNummer) {
        this.regelNummer = regelNummer;
    }

    public RegelObjekt getRegelObjekt() {
        return regelObjekt;
    }

    public void setRegelObjekt(RegelObjekt regelObjekt) {
        this.regelObjekt = regelObjekt;
    }

    public Long getPruefObjektId() {
        return pruefObjektId;
    }

    public void setPruefObjektId(Long pruefObjektId) {
        this.pruefObjektId = pruefObjektId;
    }

    public RegelTestStatus getStatus() {
        return status;
    }

    public void setStatus(RegelTestStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFormattedMessage() {
        return "%s %d: %s".formatted(typ, regelNummer, message);
    }

    public RegelTyp getTyp() {
        return typ;
    }

    public void setTyp(RegelTyp typ) {
        this.typ = typ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegelPruefung that = (RegelPruefung) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "RegelPruefung{" +
               "id=" + id +
               ", komplexpruefung=" + komplexpruefung +
               ", regelNummber=" + regelNummer +
               ", regelObjekt=" + regelObjekt +
               ", pruefObjektId=" + pruefObjektId +
               ", status='" + status + '\'' +
               ", message='" + message + '\'' +
               ", typ='" + typ + '\'' +
               '}';
    }
}
