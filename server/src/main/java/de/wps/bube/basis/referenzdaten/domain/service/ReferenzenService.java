package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RJAHR;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.REFERENZLISTEN_BUND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.REFERENZLISTEN_LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.REFERENZLISTEN_USE;
import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenSecurityException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzlisteMetadata;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzlisteMetadataRepository;
import de.wps.bube.basis.referenzdaten.security.ReferenzBerechtigungsAcessor;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenDelete;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenRead;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenUse;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenWrite;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;

@Service
public class ReferenzenService {

    public static final String KEINE_DATENBERECHTIGUNG_FUER_SCHREIBEN_DER_REFERENZ = "Keine Datenberechtigung für Schreiben der Referenz";
    public static final String KEINE_DATENBERECHTIGUNG_FUER_LÖSCHEN_DER_REFERENZ = "Keine Datenberechtigung für Löschen der Referenz";
    private final ReferenzRepository repository;
    private final ReferenzlisteMetadataRepository referenzlisteRepository;
    private final AktiverBenutzer aktiverBenutzer;
    private final ReferenzenBerechtigungsContext referenzenBerechtigungsContext;

    public ReferenzenService(ReferenzRepository repository, ReferenzlisteMetadataRepository referenzlisteRepository,
            AktiverBenutzer aktiverBenutzer,
            ReferenzenBerechtigungsContext referenzenBerechtigungsContext) {
        this.repository = repository;
        this.referenzlisteRepository = referenzlisteRepository;
        this.aktiverBenutzer = aktiverBenutzer;
        this.referenzenBerechtigungsContext = referenzenBerechtigungsContext;
    }

    private static Referenz preferLand(Referenz r1, Referenz r2) {
        Assert.isTrue(r1.isBundesreferenz() != r2.isBundesreferenz(),
                format("Schlüssel kann nicht doppelt für Land existieren. %s %s", r1, r2));
        return r1.isBundesreferenz() ? r2 : r1;
    }

    @Transactional(readOnly = true)
    @Secured({ REFERENZLISTEN_BUND_READ, REFERENZLISTEN_LAND_READ })
    public List<ReferenzlisteMetadata> readAllReferenzlistenMetadata() {
        return referenzlisteRepository.findAll();
    }

    @Transactional
    @Secured({ REFERENZLISTEN_USE })
    public Optional<Referenz> findById(Long id) {
        return this.repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Secured({ REFERENZLISTEN_BUND_READ, REFERENZLISTEN_LAND_READ })
    public ReferenzlisteMetadata readReferenzlisteMetadata(Referenzliste name) {
        Assert.notNull(name, "name == null");
        return referenzlisteRepository.findById(name)
                                      .orElseThrow(() -> new BubeEntityNotFoundException(Referenzliste.class));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenRead
    public <T> List<T> readReferenzlisteForEdit(Referenzliste name, Function<Referenz, T> mappingFunction) {
        return streamReferenzlisteForEdit(name).map(mappingFunction).collect(toList());
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenRead
    public Stream<Referenz> streamReferenzlisteForEdit(Referenzliste name) {
        // Gesamtadmin sieht alle Referenzen.
        // Sonst nur Referenzen aus dem Land des Benutzers und bundeseinheitliche Referenzen.
        if (aktiverBenutzer.isGesamtadmin()) {
            return repository.findByReferenzlisteOrderBySortierAsc(name);
        }
        return repository.findByReferenzlisteAndLandInOrderBySortierAsc(name,
                List.of(Land.DEUTSCHLAND, aktiverBenutzer.getLand()));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public List<Referenz> readReferenzliste(Referenzliste referenzliste, Gueltigkeitsjahr jahr, Land land) {
        return readReferenzliste(referenzliste, jahr, land, identity());
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public <T> List<T> readReferenzliste(Referenzliste referenzliste, Gueltigkeitsjahr jahr, Land land,
            Function<Referenz, T> mappingFunction) {
        return streamReferenzliste(referenzliste, jahr, land).map(mappingFunction).collect(toList());
    }

    private Stream<Referenz> streamReferenzliste(Referenzliste referenzliste, Gueltigkeitsjahr jahr, Land land) {
        Assert.notNull(referenzliste, "referenzliste == null");
        Assert.notNull(jahr, "jahr == null");
        Assert.notNull(land, "land == null");
        if (aktiverBenutzer.isGesamtadmin() && Land.DEUTSCHLAND.equals(land)) {
            return repository.findAll(referenzliste, jahr);
        }

        var bundEintraege = repository.findForLand(referenzliste, Land.DEUTSCHLAND, jahr);

        if (!referenzliste.isLaenderspezifisch()) {
            return bundEintraege;
        }

        if (!referenzenBerechtigungsContext.checkLandZugriff(land)) {
            throw new ReferenzdatenSecurityException(
                    "Es wird versucht auf Referenzdaten eines anderen Landes zuzugreifen. Dies darf nur der Gesamtadmin.");
        }
        var landEintraege = repository.findForLand(referenzliste, land, jahr);

        // Bund und Land Einträge mit übereinstimmendem Schlüssel werden auf den jeweiligen Landeseintrag gemappt
        return Stream.concat(bundEintraege, landEintraege)
                     .collect(toMap(Referenz::getSchluessel, identity(), ReferenzenService::preferLand))
                     .values()
                     .stream()
                     .sorted(comparing(Referenz::getSortier));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Optional<Referenz> findReferenz(Referenzliste referenzliste, String schluessel, Gueltigkeitsjahr jahr,
            Land land) {
        Assert.notNull(referenzliste, "name == null");
        Assert.notNull(schluessel, "schluessel == null");
        Assert.notNull(jahr, "berichtsjahrSchluessel == null");
        Assert.notNull(land, "land == null");

        if (!referenzliste.isLaenderspezifisch()) {
            // Bundeseinheitliche Liste
            return repository.findByKey(referenzliste, Land.DEUTSCHLAND, schluessel, jahr);
        }
        // Länderspezifische Liste
        if (!referenzenBerechtigungsContext.checkLandZugriff(land)) {
            throw new ReferenzdatenSecurityException(
                    "Es wird versucht auf Referenzdaten eines anderen Landes zuzugreifen. Dies darf nur der Gesamtadmin.");
        }

        return repository.findByKey(referenzliste, land, schluessel, jahr)
                         .or(() -> repository.findByKey(referenzliste, Land.DEUTSCHLAND, schluessel, jahr));

    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Referenz readReferenz(Referenzliste referenzliste, String schluessel, Gueltigkeitsjahr jahr, Land land) {
        return findReferenz(referenzliste, schluessel, jahr, land).orElseThrow(
                () -> new ReferenzdatenNotFoundException(referenzliste, schluessel, land, jahr));
    }

    /* Gesucht wird nach Referenzen mit dem gleichen Schlüssel über alle Berichtsjahre hinweg
     * und es wird die neuste mit dem gleichen Schlüssel genommen
     */
    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Optional<Referenz> sucheReferenzUeberAlleJahre(Referenzliste referenzliste, String schluessel,
            Land land) {
        Assert.notNull(referenzliste, "name == null");
        Assert.notNull(schluessel, "schluessel == null");
        Assert.notNull(land, "land == null");

        if (!referenzliste.isLaenderspezifisch()) {
            // Bundeseinheitliche Liste
            return repository.findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc(referenzliste,
                    Land.DEUTSCHLAND, schluessel);
        }
        // Länderspezifische Liste
        if (!referenzenBerechtigungsContext.checkLandZugriff(land)) {
            throw new ReferenzdatenSecurityException(
                    "Es wird versucht auf Referenzdaten eines anderen Landes zuzugreifen. Dies darf nur der Gesamtadmin.");
        }

        return repository.findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc(referenzliste, land,
                                 schluessel)
                         .or(() -> repository.findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc(
                                 referenzliste, Land.DEUTSCHLAND, schluessel));

    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Referenz getAktuellesBerichtsjahr() {
        return repository.findFirstByReferenzlisteOrderBySortierDesc(RJAHR)
                         .orElseThrow(() -> new BubeEntityNotFoundException(Referenz.class, RJAHR.toString()));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Referenz getJahr(String schluessel) {
        return repository.findJahrBySchluessel(schluessel)
                         .orElseThrow(() -> new BubeEntityNotFoundException(Referenz.class, schluessel));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Optional<Referenz> findByReferenznummer(String referenznummer) {
        return repository.findByReferenznummer(referenznummer);
    }

    @Transactional
    @SecuredReferenzlistenWrite
    public Referenz createReferenz(Referenz referenz) {
        Assert.isTrue(referenz.isNew(), "Referenz bereits vorhanden");
        if (referenz.getReferenznummer() == null) {
            referenz.setReferenznummer(Referenz.generateNewReferenznummer());
        }
        if (!referenzenBerechtigungsContext.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(referenz))) {
            throw new ReferenzdatenException(KEINE_DATENBERECHTIGUNG_FUER_SCHREIBEN_DER_REFERENZ);
        }
        ;
        pruefeSchluessel(referenz);
        referenz.pruefeLand();
        referenz.pruefeGueltigkeit();
        return repository.save(referenz);
    }

    @Transactional
    @SecuredReferenzlistenWrite
    public Referenz updateReferenz(Referenz referenz) {
        Assert.isTrue(!referenz.isNew(), "Referenz nicht vorhanden");
        Assert.isTrue(referenz.getReferenznummer() != null, "Referenznummer nicht vorhanden");

        //Prüfe auch die Datenberechtigung der existierenden Referenz (BUBE-424)
        Referenz referenzPreUpdate = repository.findById(referenz.getId())
                                               .orElseThrow(() -> new BubeEntityNotFoundException(Referenz.class,
                                                       "ID " + referenz.getId()));
        if (!referenzenBerechtigungsContext.checkBerechtigungWrite(
                new ReferenzBerechtigungsAcessor(referenzPreUpdate)) ||
                !referenzenBerechtigungsContext.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(referenz))) {
            throw new ReferenzdatenException(KEINE_DATENBERECHTIGUNG_FUER_SCHREIBEN_DER_REFERENZ);
        }
        pruefeSchluessel(referenz);
        referenz.pruefeLand();
        referenz.pruefeGueltigkeit();
        return repository.save(referenz);
    }

    @Transactional
    @SecuredReferenzlistenDelete
    public void deleteReferenz(Long id) {
        Referenz ref = repository.findById(id)
                                 .orElseThrow(() -> new BubeEntityNotFoundException(Referenz.class, "ID " + id));
        if (!referenzenBerechtigungsContext.checkBerechtigungDelete(new ReferenzBerechtigungsAcessor(ref))) {
            throw new ReferenzdatenException(KEINE_DATENBERECHTIGUNG_FUER_LÖSCHEN_DER_REFERENZ);
        }

        try {
            repository.deleteById(id);
            repository.flush(); // Damit hier die Exception ausgelöst wird
        } catch (DataIntegrityViolationException e) {
            throw new ReferenzdatenException("Referenz kann nicht gelöscht werden, da sie bereits verwendet wird.");
        } catch (DataAccessException e) {
            throw new ReferenzdatenException(
                    "Referenz kann nicht gelöscht werden, obwohl sie nicht mehr verwendet wird.");
        }
    }

    @Transactional
    @SecuredReferenzlistenDelete
    public void deleteMultipleReferenzen(List<Long> ids) {
        ids.forEach(this::deleteReferenz);
    }

    private void pruefeSchluessel(Referenz ref) {
        if (repository.functionalKeyDuplicate(ref.getId(), ref.getReferenzliste().name(), ref.getLand().getNr(),
                ref.getSchluessel(), ref.getGueltigVon().getJahr(), ref.getGueltigBis().getJahr())) {
            throw new ReferenzdatenException(
                    format("Referenz %s ist für Schlüssel %s, Land %s und Berichtsjahr %d-%d Zeitraum nicht eindeutig",
                            ref.getReferenzliste().name(), ref.getSchluessel(), ref.getLand().getNr(),
                            ref.getGueltigVon().getJahr(), ref.getGueltigBis().getJahr()));
        }
    }
}
