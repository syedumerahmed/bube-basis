package de.wps.bube.basis.euregistry.web.mapper;

import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.*;
import static de.wps.bube.basis.stammdaten.domain.vo.ZustaendigkeitenSchluessel.GEN_BEHOERDE;
import static de.wps.bube.basis.stammdaten.domain.vo.ZustaendigkeitenSchluessel.UEBERW_BEHOERDE;
import static java.lang.String.format;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.AnlagenInfoDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.domain.vo.ZustaendigkeitenSchluessel;

@Mapper(uses = { BerichtsdatenMapper.class, ReferenzMapper.class, BehoerdenMapper.class })
public abstract class EuRegAnlagenMapper {

    @Autowired
    private StammdatenService stammdatenService;

    // Bei diesem Mapping wird die Methode AnlagenInfoDto toDto(Anlage anlage) aufgerufen und dabei die Daten
    // der Anlage auf die Anlagen Infos gemappt
    @Mapping(target = "anlagenInfo", source = "euRegAnlage", qualifiedByName = "anlagenInfo")
    @Mapping(target = "canWrite", source = "euRegAnlage", qualifiedByName = "canWrite")
    public abstract EuRegAnlageDto toDto(EURegAnlage euRegAnlage);

    @Mapping(target = "berichtsart", ignore = true)
    @Mapping(target = "anlage", ignore = true)
    @Mapping(target = "anlagenteil", ignore = true)
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    public abstract EURegAnlage fromDto(EuRegAnlageDto dto);

    @Mapping(target = "anlagenId", source = "id")
    @Mapping(target = "prtrTaetigkeit", source = "vorschriften", qualifiedByName = "PRTR")
    @Mapping(target = "iedTaetigkeit", source = "vorschriften", qualifiedByName = "IED")
    @Mapping(target = "ueberwachungsBehoerde", source = "zustaendigeBehoerden", qualifiedByName = "UEBERW_BEH")
    @Mapping(target = "genehmigungsBehoerde", source = "zustaendigeBehoerden", qualifiedByName = "GEN_BEH")
    @Mapping(target = "bimschv4", source = "vorschriften", qualifiedByName = "R4BV")
    @Mapping(target = "bimschv17", source = "vorschriften", qualifiedByName = "hat17BV")
    @Mapping(target = "bimschv13", source = "vorschriften", qualifiedByName = "hat13BV")
    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    @Mapping(target = "parentAnlageId", ignore = true)
    @Mapping(target = "parentAnlageHatEuRegistryBericht", constant = "false")
    @Mapping(target = "fwl", source = "gesamtwaermeleistung")
    abstract AnlagenInfoDto toDto(Anlage anlage);

    @Mapping(target = "anlagenId", source = "id")
    @Mapping(target = "prtrTaetigkeit", source = "vorschriften", qualifiedByName = "PRTR")
    @Mapping(target = "iedTaetigkeit", source = "vorschriften", qualifiedByName = "IED")
    @Mapping(target = "ueberwachungsBehoerde", source = "zustaendigeBehoerden", qualifiedByName = "UEBERW_BEH")
    @Mapping(target = "genehmigungsBehoerde", source = "zustaendigeBehoerden", qualifiedByName = "GEN_BEH")
    @Mapping(target = "bimschv4", source = "vorschriften", qualifiedByName = "R4BV")
    @Mapping(target = "bimschv17", source = "vorschriften", qualifiedByName = "hat17BV")
    @Mapping(target = "bimschv13", source = "vorschriften", qualifiedByName = "hat13BV")
    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    @Mapping(target = "anlageNr", source = "anlagenteilNr")
    @Mapping(target = "parentBetriebsstaetteId", ignore = true)
    @Mapping(target = "parentAnlageHatEuRegistryBericht", source = "parentAnlageId", qualifiedByName = "parentHasBericht")
    @Mapping(target = "fwl", source = "gesamtwaermeleistung")
    abstract AnlagenInfoDto toDto(Anlagenteil anlage);

    @Named("canWrite")
    boolean canWrite(EURegAnlage eURegAnlage) {
        if (eURegAnlage.getAnlagenteil() != null) {
            return stammdatenService.canWriteAnlage(eURegAnlage.getAnlagenteil().getParentAnlageId());
        } else {
            return stammdatenService.canWriteBetriebsstaette(eURegAnlage.getAnlage().getParentBetriebsstaetteId());
        }
    }

    @Named("parentHasBericht")
    boolean parentHasBericht(Long parentId) {
        return stammdatenService.loadAnlage(parentId).hatEURegAnl();
    }

    @Named("anlagenInfo")
    AnlagenInfoDto anlagenInfo(EURegAnlage euRegAnlage) {
        if (euRegAnlage.getAnlagenteil() != null) {
            return toDto(euRegAnlage.getAnlagenteil());
        } else {
            return toDto(euRegAnlage.getAnlage());
        }
    }

    @Named("GEN_BEH")
    String genehmigungsBehoerde(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        return behoerdeDisplayFromZustaendigkeit(zustaendigeBehoerden, GEN_BEHOERDE);
    }

    @Named("UEBERW_BEH")
    String ueberwachungsBehoerde(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        return behoerdeDisplayFromZustaendigkeit(zustaendigeBehoerden, UEBERW_BEHOERDE);
    }

    private String behoerdeDisplayFromZustaendigkeit(List<ZustaendigeBehoerde> zustaendigeBehoerden,
            ZustaendigkeitenSchluessel schluessel) {
        if (zustaendigeBehoerden != null) {
            return zustaendigeBehoerden.stream()
                                       .filter(zb -> zb.getZustaendigkeit()
                                                       .getSchluessel()
                                                       .equals(schluessel.getSchluessel()))
                                       .findFirst()
                                       .map(zb -> format("%s - %s", zb.getBehoerde().getSchluessel(),
                                               zb.getBehoerde().getBezeichnung()))
                                       .orElse(null);
        }
        return null;
    }

    @Named("R4BV")
    String bimschv4ToKtext(List<Vorschrift> vorschriften) {
        return haupttaetigkeitFromVorschrift(vorschriften, V_R4BV.getSchluessel());
    }

    @Named("IED")
    String iedToKtext(List<Vorschrift> vorschriften) {
        return haupttaetigkeitFromVorschrift(vorschriften, V_IE_RL.getSchluessel());
    }

    @Named("PRTR")
    String prtrToKtext(List<Vorschrift> vorschriften) {
        return haupttaetigkeitFromVorschrift(vorschriften, V_RPRTR.getSchluessel());
    }

    @Named("hat17BV")
    boolean hat17BV(List<Vorschrift> vorschriften) {
        return hatVorschrift(vorschriften, V_17BV.getSchluessel());
    }

    @Named("hat13BV")
    boolean hat13BV(List<Vorschrift> vorschriften) {
        return hatVorschrift(vorschriften, V_13BV.getSchluessel());
    }

    private boolean hatVorschrift(List<Vorschrift> vorschriften, String schluessel) {
        if (vorschriften != null) {
            return vorschriften.stream().anyMatch(vorschrift -> vorschrift.getArt().getSchluessel().equals(schluessel));
        }
        return false;
    }

    private String haupttaetigkeitFromVorschrift(List<Vorschrift> vorschriften, String schluessel) {
        if (vorschriften != null) {
            return vorschriften.stream()
                               .filter(v -> v.getArt().getSchluessel().equals(schluessel))
                               .findFirst()
                               .map(v -> v.getHaupttaetigkeit() != null ? v.getHaupttaetigkeit().getSchluessel() : null)
                               .orElse(null);
        }
        return null;
    }

}
