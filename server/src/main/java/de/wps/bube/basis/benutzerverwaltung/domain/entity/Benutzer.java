package de.wps.bube.basis.benutzerverwaltung.domain.entity;

import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerDoppeltFehler;
import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerExistiertFehler;
import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerExistiertHinweis;
import static de.wps.bube.basis.base.vo.Validierungshinweis.betriebsstaetteNichtVorhandenFehler;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BETREIBER;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

public class Benutzer {

    //Keycloak-ID
    private String id;
    private final String benutzername;
    private String vorname;
    private String nachname;
    private String email;
    // phone-Attribut
    private String telefonnummer;

    private boolean aktiv;

    private List<Rolle> zugewieseneRollen;

    private Datenberechtigung datenberechtigung;

    private String tempPassword;

    private boolean needsTempPassword;

    public Benutzer(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public List<Rolle> getZugewieseneRollen() {
        return zugewieseneRollen;
    }

    public void setZugewieseneRollen(List<Rolle> zugewieseneRollen) {
        this.zugewieseneRollen = zugewieseneRollen;
    }

    public Datenberechtigung getDatenberechtigung() {
        return datenberechtigung;
    }

    public void setDatenberechtigung(Datenberechtigung datenberechtigung) {
        this.datenberechtigung = datenberechtigung;
    }

    public boolean isAktiv() {
        return aktiv;
    }

    public void setAktiv(boolean aktiv) {
        this.aktiv = aktiv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Benutzer benutzer = (Benutzer) o;
        return benutzername.equals(benutzer.benutzername);
    }

    @Override
    public int hashCode() {
        return Objects.hash(benutzername);
    }

    public boolean isBetreiberBenutzer() {
        return zugewieseneRollen.contains(BETREIBER);
    }

    public Land getLand() {
        return datenberechtigung.land;
    }

    public Set<String> getBehoerden() {
        return isBetreiberBenutzer() ? null : datenberechtigung.behoerden;
    }

    public Set<String> getAkzSet() {
        return isBetreiberBenutzer() ? null : datenberechtigung.akz;
    }

    public Set<String> getVerwaltungsgebiete() {
        return isBetreiberBenutzer() ? null : datenberechtigung.verwaltungsgebiete;
    }

    public Set<String> getBetriebsstaetten() {
        return datenberechtigung.betriebsstaetten;
    }

    public List<Validierungshinweis> validiereFuerImport(Set<String> allBetreiberBenutzer, Set<String> allUsers,
            Set<String> betriebsstaettenNummern, Set<String> usernames, Land land) {
        var hinweise = new ArrayList<Validierungshinweis>();
        var username = benutzername.toLowerCase();

        // Doppelt in der Datei
        if (usernames.contains(username)) {
            hinweise.add(benutzerDoppeltFehler(username));
        }

        // Schon im System vorhanden
        if (allBetreiberBenutzer.contains(username)) {
            hinweise.add(benutzerExistiertHinweis(username));
        } else if (allUsers.contains(username)) {
            hinweise.add(benutzerExistiertFehler(username));
        }

        // Betriebsstätte nicht vorhanden
        datenberechtigung.betriebsstaetten.stream()
                                          .filter(nummer -> betriebsstaettenNummern.stream().noneMatch(nummer::equalsIgnoreCase))
                                          .map(nummer -> betriebsstaetteNichtVorhandenFehler(username, nummer))
                                          .forEach(hinweise::add);

        // Es können nur Benutzer des eigenen Landes importiert werden.
        if (!land.equals(Land.DEUTSCHLAND)) {
            if (!datenberechtigung.land.equals(land)) {
                hinweise.add(Validierungshinweis.of("Der Benutzer hat ein abweichendes Land: " + land.getNr(),
                        Validierungshinweis.Severity.FEHLER));
            }
        }
        return hinweise;
    }

    public boolean hatEmail() {
        return isNotBlank(email);
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getTempPassword() {
        return tempPassword;
    }

    public boolean needsTempPassword() {
        return needsTempPassword;
    }

    public void setNeedsTempPassword(boolean needsTempPassword) {
        this.needsTempPassword = needsTempPassword;
    }

    public boolean hatBehoerde(Land land, String schluessel) {
        return this.datenberechtigung.land == land && this.datenberechtigung.behoerden.contains(schluessel);
    }

    public boolean allBstBerechtigungenContainedInIgnoreCase(Collection<String> betriebsstaettenNummern) {
        return datenberechtigung.betriebsstaetten
                .stream()
                .allMatch(str -> betriebsstaettenNummern.stream().anyMatch(str::equalsIgnoreCase));
    }
}
