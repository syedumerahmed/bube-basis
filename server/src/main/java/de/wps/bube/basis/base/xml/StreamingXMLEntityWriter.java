package de.wps.bube.basis.base.xml;

import static java.lang.Boolean.TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.bind.Marshaller.JAXB_ENCODING;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static javax.xml.bind.Marshaller.JAXB_FRAGMENT;

import java.io.OutputStream;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.validation.Schema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

public class StreamingXMLEntityWriter<E, X> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamingXMLEntityWriter.class);

    private final QName containerElement;
    private final Function<E, X> entityMapper;
    private final Function<X, JAXBElement<X>> xdtoWrapper;
    private final Marshaller marshaller;

    private ValidationErrorHandler<E> errorHandler = (entity, exception) -> {
        throw new XMLExportException(exception);
    };

    StreamingXMLEntityWriter(Class<X> elementClass, QName containerElement, Function<E, X> entityMapper,
            Function<X, JAXBElement<X>> xdtoWrapper) {
        this.containerElement = containerElement;
        this.entityMapper = entityMapper;
        this.xdtoWrapper = xdtoWrapper;
        try {
            marshaller = JAXBContext.newInstance(elementClass).createMarshaller();
            marshaller.setProperty(JAXB_ENCODING, UTF_8.toString());
            marshaller.setProperty(JAXB_FRAGMENT, TRUE);
            marshaller.setProperty(JAXB_FORMATTED_OUTPUT, TRUE);
        } catch (JAXBException e) {
            throw new XMLExportException(e);
        }
    }

    public StreamingXMLEntityWriter<E, X> withSchema(Schema schema) {
        marshaller.setSchema(schema);
        return this;
    }

    public StreamingXMLEntityWriter<E, X> withErrorHandler(ValidationErrorHandler<E> errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public void write(Stream<E> entityStream, OutputStream outputStream) {
        try {
            XMLStreamWriter xsw = new IndentingXMLStreamWriter(
                    XMLOutputFactory.newInstance().createXMLStreamWriter(outputStream, UTF_8.toString()));

            xsw.writeStartDocument(UTF_8.toString(), null);
            xsw.writeStartElement(containerElement.getLocalPart());

            var namespace = containerElement.getNamespaceURI();
            if (namespace != null) {
                xsw.setDefaultNamespace(namespace);
                xsw.writeDefaultNamespace(namespace);
            }

            entityStream.map(entity -> {
                            var xDto = entityMapper.apply(entity);
                            if (xdtoWrapper != null) {
                                return new XmlElement<>(entity, xdtoWrapper.apply(xDto));
                            }
                            return new XmlElement<>(entity, xDto);
                        })
                        .forEach(element -> {
                            try {
                                marshaller.marshal(element.xdto, xsw);
                            } catch (JAXBException e) {
                                LOGGER.error(e.toString(), e);
                                errorHandler.onError(element.entity, e);
                            }
                        });

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();
            xsw.close();
        } catch (XMLStreamException e) {
            LOGGER.error(e.toString(), e);
            throw new XMLExportException(e);
        }
    }

    private record XmlElement<E, X>(E entity, X xdto) {
    }
}
