package de.wps.bube.basis.base.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.WebappPropertiesDto;

@Mapper
public interface WebappPropertiesMapper {
    WebappPropertiesDto mapToDto(WebappProperties properties);
}
