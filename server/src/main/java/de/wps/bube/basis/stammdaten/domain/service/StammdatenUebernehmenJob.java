package de.wps.bube.basis.stammdaten.domain.service;

import static java.lang.String.format;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;

@Service
public class StammdatenUebernehmenJob implements StammdatenJob {

    public final static JobStatusEnum JOB_NAME = JobStatusEnum.STAMMDATEN_UEBERNEHEMEN;
    private final Logger logger = LoggerFactory.getLogger(StammdatenUebernehmenJob.class);

    private final BetriebsstaetteRepository betriebsstaetteRepository;
    private final StammdatenService stammdatenService;
    private final BenutzerJobRegistry benutzerJobRegistry;

    public StammdatenUebernehmenJob(BenutzerJobRegistry benutzerJobRegistry,
            BetriebsstaetteRepository betriebsstaetteRepository, StammdatenService stammdatenService) {
        this.betriebsstaetteRepository = betriebsstaetteRepository;
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.stammdatenService = stammdatenService;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(List<Long> betriebsstaettenIds, Referenz zieljahr, String username) {
        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(
                format("%s Start: %s Zieljahr: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), zieljahr.getSchluessel(),
                        username));

        try {
            stammdatenService.streamAllById(betriebsstaettenIds)
                             .filter(b -> doesNotExistInZieljahr(b, zieljahr.getId(), jobProtokoll))
                             .forEach(b -> {
                                 stammdatenService.copyBetriebsstaette(b, zieljahr, jobProtokoll);
                                 benutzerJobRegistry.jobCountChanged(1);
                             });

            benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Betriebsstätten: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }

    private boolean doesNotExistInZieljahr(Betriebsstaette b, Long zieljahrId, JobProtokoll jobProtokoll) {

        var exists = betriebsstaetteRepository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(zieljahrId,
                b.getLand(), b.getBetriebsstaetteNr());

        if (exists) {
            jobProtokoll.addWarnung(
                    "Betriebsstätte " + b.getName() + " Nr. " + b.getBetriebsstaetteNr() + ". Existiert bereits und wird nicht übernommen");
        }

        return !exists;
    }
}
