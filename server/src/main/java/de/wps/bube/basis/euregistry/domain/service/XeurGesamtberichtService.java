package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.output.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.base.sftp.SftpProperties;
import de.wps.bube.basis.base.sftp.SftpService;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.event.XeurTeilberichtFreigegebenEvent;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import net.lingala.zip4j.io.outputstream.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.AesVersion;
import net.lingala.zip4j.model.enums.EncryptionMethod;

@Service
public class XeurGesamtberichtService {

    private static final Logger LOG = LoggerFactory.getLogger(XeurGesamtberichtService.class);

    private static final String XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?>\n";
    private static final String GESAMTBERICHT_START_ELEMENT
            = "<gml:FeatureCollection xmlns:base2=\"http://inspire.ec.europa.eu/schemas/base2/2.0\"" +
              " xmlns:base=\"http://inspire.ec.europa.eu/schemas/base/3.3\"" +
              " xmlns:xlink=\"http://www.w3.org/1999/xlink\"" +
              " xmlns:EUReg=\"http://dd.eionet.europa.eu/schemaset/euregistryonindustrialsites\"" +
              " xmlns:pf=\"http://inspire.ec.europa.eu/schemas/pf/4.0\"" +
              " xmlns:gml=\"http://www.opengis.net/gml/3.2\"" +
              " xmlns:act_core=\"http://inspire.ec.europa.eu/schemas/act-core/4.0\"" +
              " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
    private static final String META_CONTENT = """
            <EURegistry-Gesamtbericht>
              <Berichtsjahr>%1$s</Berichtsjahr>
              <Dateiname>%2$s</Dateiname>
              <SHA3-512>%3$s</SHA3-512>
              <!--
                To verify checksum, run
            
                  openssl dgst -sha3-512 -hmac <HMAC_KEY> %2$s
            
              -->
            </EURegistry-Gesamtbericht>
            """;
    private static final String MAC_ALGORITHM = "HmacSHA3-512";

    private static class GesamtberichtWriter {

        private final OutputStream out;

        private boolean reportDataWritten = false;
        private boolean reportDataReading = false;

        public GesamtberichtWriter(OutputStream out) {
            this.out = out;
        }

        public boolean isRelevant(String line) {
            if (line == null || line.isBlank() || line.startsWith("<?xml ")
                || line.startsWith("<gml:FeatureCollection") || line.startsWith("</gml:FeatureCollection")) {
                return false;
            }
            if (line.matches("\\s*<EUReg:ReportData gml:id=.*")) {
                reportDataReading = true;
            }
            boolean result = !reportDataReading || !reportDataWritten;
            if (reportDataReading && line.matches("\\s*<gml:featureMember.*")) {
                reportDataWritten = true;
                reportDataReading = false;
            }
            return result;
        }

        public void writeLine(String line) {
            try {
                out.write(line.getBytes(UTF_8));
                out.write("\n".getBytes(UTF_8));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");

    private final XeurTeilberichtRepository xeurTeilberichtRepository;
    private final EURegGMLService euRegGMLService;
    private final EmailService emailService;
    private final SftpService sftpService;
    private final BenutzerverwaltungService benutzerverwaltungService;
    private final SftpProperties sftpProperties;

    @Value("${bube.mail.adresse.uba}")
    private String ubaMailAdresse;
    @Value("${bube.mail.adresse.from}")
    private String fromMailAdresse;
    @Value("${bube.xeur.gesamtbericht.hmacKey}")
    private String gesamtberichtWrapperHmacKey;
    @Value("${bube.xeur.gesamtbericht.password}")
    private String gesamtberichtEncryptionPassword;

    public XeurGesamtberichtService(
            XeurTeilberichtRepository xeurTeilberichtRepository, EURegGMLService euRegGMLService,
            EmailService emailService, SftpService sftpService, BenutzerverwaltungService benutzerverwaltungService,
            Optional<SftpProperties> sftpProperties) {
        this.xeurTeilberichtRepository = xeurTeilberichtRepository;
        this.euRegGMLService = euRegGMLService;
        this.emailService = emailService;
        this.sftpService = sftpService;
        this.benutzerverwaltungService = benutzerverwaltungService;
        this.sftpProperties = sftpProperties.orElse(null);
    }

    @Secured({ LAND_WRITE })
    @EventListener
    public void handleTeilberichtFreigegeben(XeurTeilberichtFreigegebenEvent event) {

        File gesamtbericht = null;
        File gesamtberichtXml = null;
        String fehlertext = "Bei der Erstellung des Gesamtberichts ist ein Fehler aufgetreten";

        try {

            String tmpDir = System.getProperty("java.io.tmpdir");

            Referenz berichtsjahr = event.berichtsjahr();
            String timestamp = DateTime.formatForFile(ZonedDateTime.now(ZoneId.of("Europe/Berlin")));
            String jahr = berichtsjahr.getSchluessel();
            String fileName = "%s-UBA_eureg_XEUR_%s".formatted(jahr, timestamp);
            String metaFileName = "%s-UBA_eureg_XEUR-wrapper_%s.xml".formatted(jahr, timestamp);

            gesamtbericht = new File(tmpDir, fileName + ".zip");
            gesamtberichtXml = new File(tmpDir, fileName + ".xml");

            fehlertext = "Beim Erzeugen der Gesamtberichtsdatei '%s' ist ein Fehler aufgetreten"
                    .formatted(gesamtbericht.getAbsolutePath());

            List<Land> laenderListe;
            try (FileOutputStream out = new FileOutputStream(gesamtbericht);
                    FileOutputStream xml = new FileOutputStream(gesamtberichtXml);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ZipOutputStream zip = new ZipOutputStream(out, gesamtberichtEncryptionPassword.toCharArray())) {

                ZipParameters zipParameters = getZipParameters();
                zipParameters.setFileNameInZip(fileName + ".xml");
                zipParameters.setEncryptionMethod(EncryptionMethod.AES);
                zipParameters.setAesVersion(AesVersion.TWO);
                zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
                zip.putNextEntry(zipParameters);

                laenderListe = writeGesamtberichtAndReturnLaenderListe(berichtsjahr, new TeeOutputStream(zip, bytes));
                xml.write(bytes.toByteArray());
                zip.closeEntry();

                zipParameters.setFileNameInZip(metaFileName);
                zip.putNextEntry(zipParameters);

                Mac mac = Mac.getInstance(MAC_ALGORITHM);
                mac.init(new SecretKeySpec(gesamtberichtWrapperHmacKey.getBytes(UTF_8), MAC_ALGORITHM));
                String sha3 = String.valueOf(Hex.encode(mac.doFinal(bytes.toByteArray())));
                zip.write(XML_HEADER.getBytes(UTF_8));
                zip.write(META_CONTENT.formatted(jahr, fileName + ".xml", sha3).getBytes(UTF_8));
                zip.closeEntry();

            }

            fehlertext = "Der erstellte Gesamtbericht ist nicht schemakonform";
            euRegGMLService.getSchema().newValidator().validate(new StreamSource(gesamtberichtXml));

            fehlertext = "Beim Upload des Gesamtberichts '%s' ist ein Fehler aufgetreten"
                    .formatted(gesamtbericht.getAbsolutePath());
            sftpService.uploadFile(gesamtbericht);

            if (sftpProperties != null) {
                fehlertext = ("Beim Versenden der Benachrichtigung über die Erstellung des Gesamtberichts '%s'" +
                             " ist ein Fehler aufgetreten").formatted(gesamtbericht.getName());
                String subject = "Bereitstellung der EU-Registry-Daten-Bereitstellung für das Berichtsjahr " + jahr;
                String text = getSuccessMessage(jahr, gesamtbericht.getName(), laenderListe);
                emailService.sendSimpleMessage(fromMailAdresse, subject, text, ubaMailAdresse);
            }

        } catch (Exception e) {
            LOG.error("Fehler bei Erstellung des Gesamtberichts", e);
            LOG.info("Fehlerbericht wird per E-Mail versandt an Benutzer und {}", ubaMailAdresse);
            try {
                sendErrorMail(fehlertext, e);
            } catch (Exception e2) {
                LOG.error("Fehler beim Versenden eines Fehlerberichts", e2);
            }
        } finally {
            Stream.of(gesamtbericht, gesamtberichtXml).forEach(file -> {
                if (file != null && file.exists()) {
                    try {
                        file.delete();
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    }
                }
            });
        }
    }

    private ZipParameters getZipParameters() {
        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(EncryptionMethod.AES);
        zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
        return zipParameters;
    }

    List<Land> writeGesamtberichtAndReturnLaenderListe(Referenz berichtsjahr, OutputStream out) throws IOException {

        out.write(XML_HEADER.getBytes(UTF_8));
        out.write(GESAMTBERICHT_START_ELEMENT.getBytes(UTF_8));


        GesamtberichtWriter gesamtberichtWriter = new GesamtberichtWriter(out);

        List<Land> laenderListe = new ArrayList<>();
        xeurTeilberichtRepository.findAllLatestFreigegeben(berichtsjahr.getId())
                                 .peek(teilbericht -> laenderListe.add(teilbericht.getLand()))
                                 .map(XeurTeilbericht::getXeurFile)
                                 .map(StringReader::new)
                                 .map(BufferedReader::new)
                                 .flatMap(BufferedReader::lines)
                                 .filter(gesamtberichtWriter::isRelevant)
                                 .forEach(gesamtberichtWriter::writeLine);

        out.write("</gml:FeatureCollection>\n".getBytes(UTF_8));

        return laenderListe;
    }

    private void sendErrorMail(String fehlertext, Exception e) {
        StringWriter content = new StringWriter();
        content.append(fehlertext).append(":\n\n");
        e.printStackTrace(new PrintWriter(content));
        Benutzer benutzer = benutzerverwaltungService.loadCurrentBenutzer();
        emailService.sendSimpleMessage(
                fromMailAdresse,
                "Fehler bei BUBE-Gesamtberichterstellung",
                content.toString(),
                benutzer != null ? benutzer.getEmail() : null,
                ubaMailAdresse);
    }

    private String getSuccessMessage(String jahr, String fileName, List<Land> laenderListe) {
        return """
                Die EU-Registry-Daten für das Berichtsjahr %s wurden auf dem SFTP-Server "%s" im Verzeichnis "%s" unter dem Namen "%s" am %s  bereitgestellt.

                Der Gesamtbericht beinhaltet Teilberichte folgender Bundesländer:
                %s""".formatted(jahr, sftpProperties.getHost(), sftpProperties.getUploadDir(), fileName,
                df.format(new Date()), laenderListe.stream().map(Land::name).collect(Collectors.joining(", ")));

    }
}
