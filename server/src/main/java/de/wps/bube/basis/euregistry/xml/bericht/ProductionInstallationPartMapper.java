package de.wps.bube.basis.euregistry.xml.bericht;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionInstallationPart;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.SpecificConditions;

@Mapper(uses = { CommonEURegMapper.class })
public abstract class ProductionInstallationPartMapper {

    @Mapping(target = "inspireID", source = ".")
    @Mapping(target = "thematicID.identifier", source = "berichtsdaten.thematicId")
    @Mapping(target = "thematicID.identifierScheme", source = "berichtsdaten.schema")
    @Mapping(target = "geometry", source = "geoPunkt")
    @Mapping(target = "status.statusType.schema", constant = SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES)
    @Mapping(target = "status.statusType.code", source = "betriebsstatus")
    @Mapping(target = "status.validFrom", source = "betriebsstatusSeit")
    @Mapping(target = "installationPartName.nameOfFeature", source = "name")
    @Mapping(target = "installationPartName.confidentialityReason", source = "vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    @Mapping(target = "plantType.schema", constant = SchemaSet.PLANT_TYPE_VALUE_CODE)
    @Mapping(target = "plantType.code", source = "feuerungsanlage.feuerungsanlageTyp.bezeichner")
    @Mapping(target = "totalRatedThermalInput", source = "gesamtwaermeleistung")
    @Mapping(target = "derogations", source = "feuerungsanlage.ieAusnahmen")
    @Mapping(target = "remarks", source = "berichtsdaten.bemerkung")
    @Mapping(target = "nominalCapacity.totalNormalCapacity", source = "feuerungsanlage.gesamtKapazitaetAbfall")
    @Mapping(target = "nominalCapacity.permittedCapacityHazardous", source = "feuerungsanlage.kapazitaetGefAbfall")
    @Mapping(target = "nominalCapacity.permittedCapacityNonHazardous", source = "feuerungsanlage.kapazitaetNichtGefAbfall")
    @Mapping(target = "dateOfStartOfOperation", source = "inbetriebnahme")
    @Mapping(target = "specificConditions", source = "feuerungsanlage.spezielleBedingungen")
    @Mapping(target = "heatReleaseHazardousWaste", source = "feuerungsanlage.mehrWaermeVonGefAbfall")
    @Mapping(target = "untreatedMunicipalWaste", source = "feuerungsanlage.mitverbrennungVonUnbehandeltemAbfall")
    @Mapping(target = "publicDisclosure", source = "feuerungsanlage.oeffentlicheBekanntmachung")
    @Mapping(target = "publicDisclosureURL", source = "feuerungsanlage.oeffentlicheBekanntmachungUrl")
    @Mapping(target = "gmlId", expression = "java(context.getProductionSite().getGmlId() + euRegAnlage.getLocalId() + \"_PART\")")
    public abstract ProductionInstallationPart toXDto(EURegAnlage euRegAnlage,
            @Context ProductionInstallationMapperContext context);

    // Komischerweise haben wir eine Liste und im Bericht wird nur eine Spezielle-Bedingung erwartet
    // Wir nehmen daher für den Bericht das erste Element aus der Liste
    public SpecificConditions mapListToElement(List<SpezielleBedingungIE51> spezielleBedingungen) {
        if (spezielleBedingungen == null || spezielleBedingungen.isEmpty()) {
            return null;
        }
        return toXDto(spezielleBedingungen.get(0));
    }

    @Mapping(target = "specificConditionsPermitURL", source = "genehmigungUrl")
    @Mapping(target = "conditionsInformation", source = "info")
    @Mapping(target = "specificConditions", source = "art")
    public abstract SpecificConditions toXDto(SpezielleBedingungIE51 spezielleBedingung);

}
