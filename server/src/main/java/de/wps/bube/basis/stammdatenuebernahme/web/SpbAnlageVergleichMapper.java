package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbAnlageVergleichDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlageVergleich;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbAnlageVergleichMapper {

    @Mapping(target = "allgemein", source = ".")
    public abstract SpbAnlageVergleichDto toDto(SpbAnlageVergleich spbAnlageVergleich);
}
