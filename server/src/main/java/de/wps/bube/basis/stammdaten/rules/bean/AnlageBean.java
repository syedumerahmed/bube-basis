package de.wps.bube.basis.stammdaten.rules.bean;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class AnlageBean extends StammdatenObjektBean<AnlageBean> {

    private final String anlageNr;
    private final Long parentBetriebsstaetteId;
    private final List<VorschriftBean> vorschriften;
    private final List<LeistungBean> leistungen;
    private final List<QuelleBean> quellen;
    private final List<AnlagenteilBean> anlagenteile;
    private final Supplier<BetriebsstaetteBean> betriebsstaette;

    public AnlageBean(Long id, String localId, String referenzAnlagenkataster, String anlageNr,
            Long parentBetriebsstaetteId, GeoPunktBean geoPunkt, List<VorschriftBean> vorschriften,
            List<LeistungBean> leistungen, List<ZustaendigeBehoerdeBean> zustaendigeBehoerden, String name,
            ReferenzBean vertraulichkeitsgrund, ReferenzBean betriebsstatus, LocalDate betriebsstatusSeit,
            LocalDate inbetriebnahme, String bemerkung, boolean betreiberdatenUebernommen, List<QuelleBean> quellen,
            List<AnlagenteilBean> anlagenteile, Instant ersteErfassung, Instant letzteAenderung,
            Supplier<AnlageBean> vorgaenger, Supplier<AnlageBean> nachfolger,
            Supplier<BetriebsstaetteBean> betriebsstaette) {
        super(id, localId, referenzAnlagenkataster, geoPunkt, zustaendigeBehoerden, name, vertraulichkeitsgrund,
                betriebsstatus, betriebsstatusSeit, inbetriebnahme, bemerkung, betreiberdatenUebernommen,
                ersteErfassung, letzteAenderung, vorgaenger, nachfolger);
        this.anlageNr = anlageNr;
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        this.vorschriften = vorschriften;
        this.leistungen = leistungen;
        this.quellen = quellen;
        this.anlagenteile = anlagenteile;
        this.betriebsstaette = Suppliers.memoize(betriebsstaette::get);
    }

    @RegelApi.Member(description = "Anlagennummer")
    public String getAnlageNr() {
        return anlageNr;
    }

    @RegelApi.Member(exclude = true)
    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    @RegelApi.Member(description = "Vorschriften")
    public List<VorschriftBean> getVorschriften() {
        return vorschriften;
    }

    @RegelApi.Member(description = "Leistungen")
    public List<LeistungBean> getLeistungen() {
        return leistungen;
    }

    @RegelApi.Member(description = "Quellen")
    public List<QuelleBean> getQuellen() {
        return quellen;
    }

    @RegelApi.Member(description = "Anlagenteile")
    public List<AnlagenteilBean> getAnlagenteile() {
        return anlagenteile;
    }

    @RegelApi.Member(description = "Die Betriebsstätte der Anlage")
    public BetriebsstaetteBean getBetriebsstaette() {
        return betriebsstaette.get();
    }

    // Wird für Annotation überschrieben
    @Override
    public AnlageBean getVorgaenger() {
        return super.getVorgaenger();
    }

    // Wird für Annotation überschrieben
    @Override
    public AnlageBean getNachfolger() {
        return super.getNachfolger();
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.ANLAGE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnlageBean that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        return Objects.equals(anlageNr, that.anlageNr) && Objects.equals(parentBetriebsstaetteId,
                that.parentBetriebsstaetteId) && Objects.equals(vorschriften, that.vorschriften) && Objects.equals(
                leistungen, that.leistungen) && Objects.equals(quellen, that.quellen) && Objects.equals(anlagenteile,
                that.anlagenteile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), anlageNr, parentBetriebsstaetteId, vorschriften, leistungen, quellen,
                anlagenteile);
    }

    @Override
    public String toString() {
        return "AnlageBean{" +
                "anlageNr='" + anlageNr + '\'' +
                ", parentBetriebsstaetteId=" + parentBetriebsstaetteId +
                ", vorschriften=" + vorschriften +
                ", leistungen=" + leistungen +
                ", quellen=" + quellen +
                ", anlagenteile=" + anlagenteile +
                " // " + super.toString() +
                '}';
    }

}
