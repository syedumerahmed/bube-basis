package de.wps.bube.basis.datei.domain.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.datei.domain.UploadException;
import de.wps.bube.basis.datei.domain.entity.Datei;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.persistence.DateiRepository;

@Service
@Transactional
public class DateiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateiService.class);

    private final DateiRepository dateiRepository;
    private final EntityManager entityManager;

    public DateiService(DateiRepository dateiRepository, EntityManager entityManager) {
        this.dateiRepository = dateiRepository;
        this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    public InputStream getDateiStream(String username, DateiScope scope) {
        return dateiRepository.findByUsernameAndDateiScope(username, scope)
                              .map(Datei::getStream)
                              .orElseThrow(() -> new BubeEntityNotFoundException(Datei.class));
    }

    public void dateiSpeichern(MultipartFile file, String username, DateiScope scope) {
        loescheDatei(username, scope);
        try {
            Blob blob = entityManager.unwrap(Session.class)
                                     .getLobHelper()
                                     .createBlob(file.getInputStream(), file.getSize());
            dateiRepository.save(new Datei(file.getOriginalFilename(), username, blob, scope));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new UploadException();
        }
    }

    public void loescheDatei(String username, DateiScope scope) {
        dateiRepository.deleteAllByUsernameAndDateiScope(username, scope);
    }

    @Transactional(readOnly = true)
    public String existingDatei(String username, DateiScope scope) {
        return dateiRepository.findByUsernameAndDateiScope(username, scope).map(Datei::getDateiName).orElse(null);
    }

    public void deleteTemporaryFiles() {
        dateiRepository.deleteAllByTempTrue();
        dateiRepository.deleteLargeObjectOrphans();
    }
}
