package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.NumberVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbQuelleVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class QuelleVergleichService {

    public SpbQuelleVergleich vergleiche(Quelle spl, SpbQuelle spb) {
        SpbQuelleVergleich vergleich = new SpbQuelleVergleich();

        vergleich.setId(spl == null ? null : spl.getId());
        vergleich.setSpbId(spb == null ? null : spb.getId());

        vergleich.setName(new StringVector(spl == null ? null : spl.getName(), spb == null ? null : spb.getName()));
        if (spl == null || !spl.isBetreiberdatenUebernommen()) {
            vergleich.setQuelleNr(
                    new StringVector(spl == null ? null : spl.getQuelleNr(), spb == null ? null : spb.getQuelleNr()));

            vergleich.setGeoPunkt(
                    new GeoPunktVector(spl == null ? null : spl.getGeoPunkt(), spb == null ? null : spb.getGeoPunkt()));
            vergleich.setQuelleArt(
                    new ReferenzVector(spl == null ? null : spl.getQuellenArt(),
                            spb == null ? null : spb.getQuellenArt()));

            vergleich.setFlaeche(
                    new NumberVector(spl == null ? null : spl.getFlaeche(), spb == null ? null : spb.getFlaeche()));
            vergleich.setLaenge(
                    new NumberVector(spl == null ? null : spl.getLaenge(), spb == null ? null : spb.getLaenge()));
            vergleich.setBreite(
                    new NumberVector(spl == null ? null : spl.getBreite(), spb == null ? null : spb.getBreite()));
            vergleich.setDurchmesser(
                    new NumberVector(spl == null ? null : spl.getDurchmesser(),
                            spb == null ? null : spb.getDurchmesser()));
            vergleich.setBauhoehe(
                    new NumberVector(spl == null ? null : spl.getBauhoehe(), spb == null ? null : spb.getBauhoehe()));
        }

        if (spb == null) {
            vergleich.setHasChanges(false);
        } else {

            vergleich.setHasChanges(spl == null || !spl.isBetreiberdatenUebernommen() &&
                    (!vergleich.getName().isEqual()
                            || !vergleich.getGeoPunkt().isEqual()
                            || !vergleich.getQuelleArt().isEqual()
                            || !vergleich.getQuelleNr().isEqual()
                            || !vergleich.getFlaeche().isEqual()
                            || !vergleich.getLaenge().isEqual()
                            || !vergleich.getBreite().isEqual()
                            || !vergleich.getDurchmesser().isEqual()
                            || !vergleich.getBauhoehe().isEqual()
                    ));
        }
        return vergleich;
    }
}
