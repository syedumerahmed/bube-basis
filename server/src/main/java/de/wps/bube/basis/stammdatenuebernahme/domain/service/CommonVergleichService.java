package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAdresse;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.AdresseVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class CommonVergleichService {

    public List<LeistungVector> mapLeistungen(List<Leistung> spl, List<Leistung> spb) {
        List<LeistungVector> leistungen = new ArrayList<>();
        List<Leistung> matched = new ArrayList<>();

        spl.forEach(v -> {
            Optional<Leistung> first = spb.stream().filter(v1 -> v1.equals(v)).findFirst();
            leistungen.add(new LeistungVector(v, first.orElse(null)));
            first.ifPresent(matched::add);
        });

        spb.forEach(v -> {
            if (!matched.contains(v)) {
                leistungen.add(new LeistungVector(null, v));
            }
        });

        return leistungen;
    }

    public AdresseVergleich adressVergleich(Adresse spl, SpbAdresse spb) {
        AdresseVergleich vergleich = new AdresseVergleich();

        vergleich.setStrasse(
                new StringVector(spl == null ? null : spl.getStrasse(), spb == null ? null : spb.getStrasse()));
        vergleich.setHausNr(
                new StringVector(spl == null ? null : spl.getHausNr(), spb == null ? null : spb.getHausNr()));
        vergleich.setPlz(new StringVector(spl == null ? null : spl.getPlz(), spb == null ? null : spb.getPlz()));
        vergleich.setOrt(new StringVector(spl == null ? null : spl.getOrt(), spb == null ? null : spb.getOrt()));
        vergleich.setOrtsteil(
                new StringVector(spl == null ? null : spl.getOrtsteil(), spb == null ? null : spb.getOrtsteil()));
        vergleich.setPostfach(
                new StringVector(spl == null ? null : spl.getPostfach(), spb == null ? null : spb.getPostfach()));
        vergleich.setPostfachPlz(
                new StringVector(spl == null ? null : spl.getPostfachPlz(), spb == null ? null : spb.getPostfachPlz()));
        vergleich.setLandIsoCode(
                new ReferenzVector(spl == null ? null : spl.getLandIsoCode(),
                        spb == null ? null : spb.getLandIsoCode()));

        return vergleich;
    }

    public List<ReferenzVector> mapReferenzListen(List<Referenz> spl, List<Referenz> spb) {
        List<ReferenzVector> maps = new ArrayList<>();
        List<Referenz> doubled = new ArrayList<>();

        spl.forEach(ref -> {
            Optional<Referenz> first = spb.stream().filter(r -> r.equals(ref)).findFirst();
            first.ifPresent(doubled::add);

            maps.add(new ReferenzVector(ref, first.orElse(null)));
        });

        spb.stream().filter(r -> !doubled.contains(r)).forEach(r -> maps.add(new ReferenzVector(null, r))
        );
        return maps;
    }

    public List<StringVector> vergleicheStringLists(List<String> spl, List<String> spb) {

        List<StringVector> maps = new ArrayList<>();
        List<String> doubled = new ArrayList<>();

        spl.forEach(str -> {
            Optional<String> first = spb.stream().filter(s -> s.equals(str)).findFirst();
            first.ifPresent(doubled::add);

            maps.add(new StringVector(str, first.orElse(null)));
        });

        spb.stream().filter(s -> !doubled.contains(s)).forEach(s -> maps.add(new StringVector(null, s))
        );
        return maps;
    }

    public boolean adresseEqual(AdresseVergleich vergleich) {
        return vergleich.getStrasse().isEqual()
                && vergleich.getHausNr().isEqual()
                && vergleich.getPlz().isEqual()
                && vergleich.getOrt().isEqual()
                && vergleich.getOrtsteil().isEqual()
                && vergleich.getPostfach().isEqual()
                && vergleich.getPostfachPlz().isEqual()
                && vergleich.getLandIsoCode().isEqual();
    }
}
