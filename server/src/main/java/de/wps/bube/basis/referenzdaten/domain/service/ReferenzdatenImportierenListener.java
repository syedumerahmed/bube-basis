package de.wps.bube.basis.referenzdaten.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.event.ReferenzdatenImportTriggeredEvent;

@Component
@Transactional
public class ReferenzdatenImportierenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReferenzdatenImportierenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final ReferenzdatenImportierenJob job;

    public ReferenzdatenImportierenListener(BenutzerJobRegistry jobRegistry, ReferenzdatenImportierenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleReferenzdatendatenImportieren(ReferenzdatenImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", ReferenzdatenImportierenJob.JOB_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(ReferenzdatenImportierenJob.JOB_NAME));

        job.runAsync(event.username);
    }
}
