package de.wps.bube.basis.euregistry.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public interface EuRegAnlageRepository
        extends JpaRepository<EURegAnlage, Long>, QuerydslPredicateProjectionRepository<EURegAnlage> {
    
    boolean existsByAnlageId(Long anlageId);

    Long deleteByAnlageId(Long anlageId);

    boolean existsByAnlagenteilId(Long anlagenteilId);

    Long deleteByAnlagenteilId(Long anlagenteilId);

    @Query("from EURegAnlage euRegAnl" +
            " left join Betriebsstaette bst with euRegAnl.anlage.parentBetriebsstaetteId = bst.id" +
            " where euRegAnl.anlage.localId = :localId and bst.berichtsjahr = :jahr")
    EURegAnlage findByLocalIdAndBerichtsjahrForAnlage(String localId, Referenz jahr);

    @Query("from EURegAnlage euRegAnl" +
            " left join Anlage anl with euRegAnl.anlagenteil.parentAnlageId = anl.id" +
            " left join Betriebsstaette bst with anl.parentBetriebsstaetteId = bst.id" +
            " where euRegAnl.anlagenteil.localId = :localId and bst.berichtsjahr = :jahr")
    EURegAnlage findByLocalIdAndBerichtsjahrForAnlagenTeil(String localId, Referenz jahr);

}
