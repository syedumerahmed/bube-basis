package de.wps.bube.basis.euregistry.domain.event;

import java.util.List;

public record BerichtPruefungEvent(List<Long> ids, String username) {

    public static final String EVENT_NAME = "Komplexprüfung";

}
