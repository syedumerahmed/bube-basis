package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.base.vo.Land;

public class AnsprechpartnerLoeschenTriggerdEvent {
    public static final String EVENT_NAME = "Ansprechpartner löschen";

    public final Referenz berichtsjahr;
    public final String username;
    public final Land land;

    public AnsprechpartnerLoeschenTriggerdEvent(Referenz berichtsjahr, String username, Land land) {
        this.berichtsjahr = berichtsjahr;
        this.username = username;
        this.land = land;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AnsprechpartnerLoeschenTriggerdEvent that = (AnsprechpartnerLoeschenTriggerdEvent) o;
        return Objects.equals(berichtsjahr, that.berichtsjahr) && Objects.equals(username,
                that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(berichtsjahr, username);
    }
}
