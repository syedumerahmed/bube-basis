package de.wps.bube.basis.euregistry.domain.entity;

import static javax.persistence.EnumType.STRING;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;

// In Arbeit hier fehlen noch viele Attribute
@Entity
@EntityListeners(AuditingEntityListener.class)

public class EURegAnlage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eureg_anlage_seq")
    @SequenceGenerator(name = "eureg_anlage_seq", allocationSize = 50)
    private Long id;

    // Die Anlage, für die der Bericht erstellt wird
    @OneToOne(fetch = FetchType.LAZY)
    private Anlage anlage;

    // Das Anlagenteil, für die der Bericht erstellt wird
    @OneToOne(fetch = FetchType.LAZY)
    private Anlagenteil anlagenteil;

    @Embedded
    private Berichtsdaten berichtsdaten;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    private String localId2;
    @NotNull
    @Enumerated(STRING)
    private Ausgangszustandsbericht report;
    private int visits;
    @NotNull
    private String visitUrl;
    private String eSpirsNr;
    private String tehgNr;
    private String monitor;
    private String monitorUrl;

    // Relevantes Kapitel der IE-RL, das auf die Anlage bzw. den Anlagenteil anwendbar ist
    @ManyToMany
    private List<Referenz> kapitelIeRl;

    // Anwendbare Durchführungsbeschlüsse, Auswahl
    @ManyToMany
    private List<Referenz> anwendbareBvt;

    @ElementCollection
    private List<BVTAusnahme> bvtAusnahmen;

    @ElementCollection
    private List<Auflage> auflagen;

    @Embedded
    @NotNull
    @AttributeOverride(column = @Column(name = "genehmigung_erteilt"), name = "erteilt")
    @AttributeOverride(column = @Column(name = "genehmigung_neu_betrachtet"), name = "neuBetrachtet")
    @AttributeOverride(column = @Column(name = "genehmigung_geaendert"), name = "geaendert")
    @AttributeOverride(column = @Column(name = "genehmigung_erteilungsdatum"), name = "erteilungsdatum")
    @AttributeOverride(column = @Column(name = "genehmigung_neu_betrachtet_datum"), name = "neuBetrachtetDatum")
    @AttributeOverride(column = @Column(name = "genehmigung_aenderungsdatum"), name = "aenderungsdatum")
    @AttributeOverride(column = @Column(name = "genehmigung_url"), name = "url")
    @AttributeOverride(column = @Column(name = "genehmigung_durchsetzungsmassnahmen"), name = "durchsetzungsmassnahmen")
    private Genehmigung genehmigung;

    @Embedded
    private FeuerungsanlageBerichtsdaten feuerungsanlage;

    @Deprecated
    protected EURegAnlage() {
    }

    public EURegAnlage(Anlage anlage, Anlagenteil anlagenteil, Berichtsdaten berichtsdaten) {
        this.anlage = anlage;
        this.anlagenteil = anlagenteil;
        this.berichtsdaten = berichtsdaten;
        this.report = Ausgangszustandsbericht.NICHT_ERFORDERLICH;
        this.genehmigung = new Genehmigung(false, false, false, null, null, null, null, null);
        this.feuerungsanlage = new FeuerungsanlageBerichtsdaten(0.0, 0.0, 0.0);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Anlage getAnlage() {
        return anlage;
    }

    public void setAnlage(Anlage anlage) {
        this.anlage = anlage;
    }

    public Anlagenteil getAnlagenteil() {
        return anlagenteil;
    }

    public void setAnlagenteil(Anlagenteil anlagenteil) {
        this.anlagenteil = anlagenteil;
    }

    public Berichtsdaten getBerichtsdaten() {
        return berichtsdaten;
    }

    public void setBerichtsdaten(Berichtsdaten berichtsdaten) {
        this.berichtsdaten = berichtsdaten;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public String getLocalId2() {
        return localId2;
    }

    public void setLocalId2(String localId2) {
        this.localId2 = localId2;
    }

    public Ausgangszustandsbericht getReport() {
        return report;
    }

    public void setReport(Ausgangszustandsbericht report) {
        this.report = report;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public String getVisitUrl() {
        return visitUrl;
    }

    public void setVisitUrl(String visitUrl) {
        this.visitUrl = visitUrl;
    }

    public String geteSpirsNr() {
        return eSpirsNr;
    }

    public void seteSpirsNr(String eSpirsNr) {
        this.eSpirsNr = eSpirsNr;
    }

    public String getTehgNr() {
        return tehgNr;
    }

    public void setTehgNr(String tehgNr) {
        this.tehgNr = tehgNr;
    }

    public String getMonitor() {
        return monitor;
    }

    public void setMonitor(String monitor) {
        this.monitor = monitor;
    }

    public String getMonitorUrl() {
        return monitorUrl;
    }

    public void setMonitorUrl(String monitorUrl) {
        this.monitorUrl = monitorUrl;
    }

    public FeuerungsanlageBerichtsdaten getFeuerungsanlage() {
        return feuerungsanlage;
    }

    public void setFeuerungsanlage(FeuerungsanlageBerichtsdaten feuerungsanlage) {
        this.feuerungsanlage = feuerungsanlage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EURegAnlage that = (EURegAnlage) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean isEURegAnl() {
        return berichtsdaten.isEURegAnl();
    }

    public boolean isEURegF() {
        return berichtsdaten.isEURegF();
    }

    public Double getGesamtwaermeleistung() {
        return anlage != null ? anlage.getGesamtwaermeleistung() : anlagenteil.getGesamtwaermeleistung();
    }

    public String getLocalId() {
        return anlage != null ? anlage.getLocalId() : anlagenteil.getLocalId();
    }

    public String getName() {
        return anlage != null ? anlage.getName() : anlagenteil.getName();
    }

    public Referenz getVertraulichkeitsgrund() {
        return anlage != null ? anlage.getVertraulichkeitsgrund() : anlagenteil.getVertraulichkeitsgrund();
    }

    public GeoPunkt getGeoPunkt() {
        return anlage != null ? anlage.getGeoPunkt() : anlagenteil.getGeoPunkt();
    }

    public Vorschrift getIERLVorschrift() {
        return anlage != null ? anlage.getIERLVorschrift() : anlagenteil.getIERLVorschrift();
    }

    public LocalDate getInbetriebnahme() {
        return anlage != null ? anlage.getInbetriebnahme() : anlagenteil.getInbetriebnahme();
    }

    public Referenz getBetriebsstatus() {
        return anlage != null ? anlage.getBetriebsstatus() : anlagenteil.getBetriebsstatus();
    }

    public LocalDate getBetriebsstatusSeit() {
        return anlage != null ? anlage.getBetriebsstatusSeit() : anlagenteil.getBetriebsstatusSeit();
    }

    public List<Behoerde> getGenehmigungsbehoerden() {
        return anlage != null ? anlage.getGenehmigungsbehoerden() : anlagenteil.getGenehmigungsbehoerden();
    }

    public List<Behoerde> getUeberwachungsbehoerden() {
        return anlage != null ? anlage.getUeberwachungsbehoerden() : anlagenteil.getUeberwachungsbehoerden();
    }

    public EURegAnlage getEURegF() {
        if (isEURegF()) {
            return this;
        } else {
            return null;
        }
    }

    public void setBerichtsart(Referenz berichtsart) {
        berichtsdaten.setBerichtsart(berichtsart);
    }

    public List<Referenz> getKapitelIeRl() {
        return kapitelIeRl;
    }

    public void setKapitelIeRl(List<Referenz> kapitelIeRl) {
        this.kapitelIeRl = kapitelIeRl;
    }

    public List<Referenz> getAnwendbareBvt() {
        return anwendbareBvt;
    }

    public void setAnwendbareBvt(List<Referenz> anwendbareBvt) {
        this.anwendbareBvt = anwendbareBvt;
    }

    public List<BVTAusnahme> getBvtAusnahmen() {
        return bvtAusnahmen;
    }

    public void setBvtAusnahmen(List<BVTAusnahme> bvtAusnahmen) {
        this.bvtAusnahmen = bvtAusnahmen;
    }

    public Genehmigung getGenehmigung() {
        return genehmigung;
    }

    public void setGenehmigung(Genehmigung genehmigung) {
        this.genehmigung = genehmigung;
    }

    public List<Auflage> getAuflagen() {
        return auflagen;
    }

    public void setAuflagen(List<Auflage> auflagen) {
        this.auflagen = auflagen;
    }

}
