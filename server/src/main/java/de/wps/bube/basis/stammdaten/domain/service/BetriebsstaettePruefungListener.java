package de.wps.bube.basis.stammdaten.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaettePruefungEvent;

@Component
public class BetriebsstaettePruefungListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BetriebsstaettePruefungListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final BetriebsstaettePruefungJob job;

    public BetriebsstaettePruefungListener(BenutzerJobRegistry jobRegistry, BetriebsstaettePruefungJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleBetriebsstaettePruefungEvent(BetriebsstaettePruefungEvent event) {
        LOGGER.info("Event handling started for '{}'", BetriebsstaettePruefungEvent.EVENT_NAME);
        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(BetriebsstaettePruefungJob.JOB_NAME));

        job.runAsync(event.ids(), event.username());
    }
}
