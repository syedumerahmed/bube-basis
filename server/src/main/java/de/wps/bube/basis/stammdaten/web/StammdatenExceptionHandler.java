package de.wps.bube.basis.stammdaten.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.stammdaten.domain.BetreiberInUseException;

@ControllerAdvice("de.wps.bube.basis.stammdaten.web")
public class StammdatenExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ BetreiberInUseException.class })
    protected ResponseEntity<Object> handleBetreiberInUseException(BetreiberInUseException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

}
