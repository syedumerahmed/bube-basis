package de.wps.bube.basis.referenzdaten.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;

public interface ParameterRepository extends JpaRepository<Parameter, String> {

}
