package de.wps.bube.basis.stammdaten.rules;

import org.springframework.stereotype.Component;

@Component
public class StammdatenBeanMappers {

    private final BetriebsstaetteBeanMapper betriebsstaetteBeanMapper;
    private final AnlageBeanMapper anlageBeanMapper;
    private final AnlagenteilBeanMapper anlagenteilBeanMapper;
    private final QuelleBeanMapper quelleBeanMapper;
    private final BetreiberBeanMapper betreiberBeanMapper;

    public StammdatenBeanMappers(BetriebsstaetteBeanMapper betriebsstaetteBeanMapper,
            AnlageBeanMapper anlageBeanMapper,
            AnlagenteilBeanMapper anlagenteilBeanMapper,
            QuelleBeanMapper quelleBeanMapper,
            BetreiberBeanMapper betreiberBeanMapper) {
        this.betriebsstaetteBeanMapper = betriebsstaetteBeanMapper;
        this.anlageBeanMapper = anlageBeanMapper;
        this.anlagenteilBeanMapper = anlagenteilBeanMapper;
        this.quelleBeanMapper = quelleBeanMapper;
        this.betreiberBeanMapper = betreiberBeanMapper;
    }

    public BetriebsstaetteBeanMapper getBetriebsstaetteBeanMapper() {
        return betriebsstaetteBeanMapper;
    }

    public AnlageBeanMapper getAnlageBeanMapper() {
        return anlageBeanMapper;
    }

    public AnlagenteilBeanMapper getAnlagenteilBeanMapper() {
        return anlagenteilBeanMapper;
    }

    public QuelleBeanMapper getQuelleBeanMapper() {
        return quelleBeanMapper;
    }

    public BetreiberBeanMapper getBetreiberBeanMapper() {
        return betreiberBeanMapper;
    }

}
