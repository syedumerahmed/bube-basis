package de.wps.bube.basis.euregistry.xml.bericht;

import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;

public interface ProductionInstallationMapperContext {
    InspireParam getInspireParam();

    ProductionSite getProductionSite();
}
