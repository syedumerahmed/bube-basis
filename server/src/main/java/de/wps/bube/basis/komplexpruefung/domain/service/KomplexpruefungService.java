package de.wps.bube.basis.komplexpruefung.domain.service;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.TEILBERICHT;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus.BST_UNGUELTIG;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus.SEMANTISCHER_FEHLER;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus.SYNTAX_FEHLER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ParseException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.komplexpruefung.persistence.KomplexpruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelPruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Service
@Transactional
@Secured(BEHOERDENBENUTZER)
public class KomplexpruefungService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KomplexpruefungService.class);

    private final RegelRepository regelRepository;
    private final KomplexpruefungRepository komplexpruefungRepository;
    private final RegelPruefungRepository regelPruefungRepository;
    private final RegelPruefungService regelPruefungService;

    public KomplexpruefungService(RegelRepository regelRepository, KomplexpruefungRepository komplexpruefungRepository,
            RegelPruefungRepository regelPruefungRepository, RegelPruefungService regelPruefungService) {
        this.regelRepository = regelRepository;
        this.komplexpruefungRepository = komplexpruefungRepository;
        this.regelPruefungRepository = regelPruefungRepository;
        this.regelPruefungService = regelPruefungService;
    }

    public void komplexpruefung(List<RegelGruppe> gruppen, Komplexpruefung komplexpruefung,
            Land land, Gueltigkeitsjahr jahr, Long behoerdeId,
            Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector) {

        List<RegelPruefung> regelPruefungList = regelRepository
                .streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(land, gruppen, jahr)
                .filter(regel -> !regel.getObjekte().contains(TEILBERICHT))
                .flatMap(regel -> komplexpruefung(regel, komplexpruefung, land, jahr,
                        behoerdeId, pruefObjektCollector))
                .toList();

        komplexpruefungRepository.save(komplexpruefung);
        regelPruefungRepository.saveAll(regelPruefungList);
    }

    private Stream<RegelPruefung> komplexpruefung(Regel regel, Komplexpruefung komplexpruefung,
            Land land, Gueltigkeitsjahr jahr, Long behoerdeId,
            Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector) {
        try {

            RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, land, jahr, behoerdeId,
                    pruefObjektCollector);

            // Zähle gültige Objekte
            komplexpruefung.addAnzahlGueltig((long) ergebnis.getGueltigeObjekte().size());

            // Für die anderen Status-Werte, lege RegelPrüfung-Objekte an UND inkrementiere die entsprechenden Zähler
            Stream<RegelPruefung> ungueltigStream = ergebnis.getUngueltigeObjekte().stream().map(ungueltig -> {
                RegelPruefung regelPruefung = new RegelPruefung(komplexpruefung, regel.getNummer(), BST_UNGUELTIG);
                regelPruefung.setRegelObjekt(ungueltig.getRegelObjekt());
                regelPruefung.setPruefObjektId(ungueltig.getId());
                regelPruefung.setMessage(regel.getFehlertext());
                regelPruefung.setTyp(regel.getTyp(land));
                return regelPruefung;
            });
            RegelTyp typ = regel.getTyp(land);
            if (typ == RegelTyp.FEHLER) {
                komplexpruefung.addAnzahlFehler((long) ergebnis.getUngueltigeObjekte().size());
            } else if (typ == RegelTyp.WARNUNG) {
                komplexpruefung.addAnzahlWarnungen((long) ergebnis.getUngueltigeObjekte().size());
            }

            Stream<RegelPruefung> fehlerStream = ergebnis.getPruefungsFehler().entrySet().stream().map(entry -> {
                RegelPruefung regelPruefung = new RegelPruefung(komplexpruefung, regel.getNummer(),
                        SEMANTISCHER_FEHLER);
                regelPruefung.setRegelObjekt(entry.getKey().getRegelObjekt());
                regelPruefung.setPruefObjektId(entry.getKey().getId());
                regelPruefung.setMessage(entry.getValue().getMessage());
                regelPruefung.setTyp(regel.getTyp(land));
                return regelPruefung;
            });
            komplexpruefung.addAnzahlSemantischUngueltig((long) ergebnis.getPruefungsFehler().size());

            return Stream.concat(ungueltigStream, fehlerStream);

        } catch (ParseException e) {
            RegelPruefung regelPruefung = new RegelPruefung(komplexpruefung, regel.getNummer(), SYNTAX_FEHLER);
            regelPruefung.setMessage(e.getMessage());
            return Stream.of(regelPruefung);
        }
    }

    public void delete(Komplexpruefung komplexpruefung) {
        regelPruefungRepository.deleteByKomplexpruefung(komplexpruefung);
        komplexpruefungRepository.delete(komplexpruefung);
    }

    public RegelPruefungenMap readRegelPruefungen(Komplexpruefung komplexpruefung) {
        RegelPruefungenMap map = new RegelPruefungenMap();
        regelPruefungRepository.findByKomplexpruefung(komplexpruefung)
                               .forEach(map::put);
        return map;
    }

    public Komplexpruefung save(Komplexpruefung komplexpruefung) {
        return komplexpruefungRepository.save(komplexpruefung);
    }

}
