package de.wps.bube.basis.komplexpruefung.domain.vo;

import java.util.stream.Stream;

public enum RegelTyp {
    FEHLER("Fehler"), WARNUNG("Warnung"), IGNORIERT("Ignoriert");

    private final String bezeichner;

    RegelTyp(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

}
