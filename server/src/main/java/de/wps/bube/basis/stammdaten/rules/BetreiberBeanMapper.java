package de.wps.bube.basis.stammdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.rules.AdresseBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.rules.bean.BetreiberBean;

@Mapper(uses = { ReferenzBeanMapper.class, AdresseBeanMapper.class })
public abstract class BetreiberBeanMapper {

    public abstract BetreiberBean toBean(Betreiber betreiber);

}
