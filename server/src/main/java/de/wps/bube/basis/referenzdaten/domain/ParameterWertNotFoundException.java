package de.wps.bube.basis.referenzdaten.domain;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

public class ParameterWertNotFoundException extends RuntimeException {

    public final String schluessel;
    public final Gueltigkeitsjahr jahr;
    public final Land land;

    public ParameterWertNotFoundException(String schluessel, Land land, Gueltigkeitsjahr jahr) {
        super();
        this.schluessel = schluessel;
        this.land = land;
        this.jahr = jahr;
    }

    @Override
    public String getMessage() {
        return String.format("Kein Parameterwert gefunden für Schlüssel=%s Land=%s Jahr=%d", schluessel, land,
                jahr.getJahr());
    }
}
