package de.wps.bube.basis.koordinaten.domain;

public class KoordinatenException extends RuntimeException {
    public KoordinatenException(final String message) {
        super(message);
    }

    public KoordinatenException(String message, Throwable cause) {
        super(message, cause);
    }
}
