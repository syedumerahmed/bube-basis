package de.wps.bube.basis.base.persistence;

import static java.lang.String.format;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.QBean;

/**
 * Eigene Implementierung der QBean, die eine Exception wirft, wenn das Mapping nicht korrekt implementiert wird
 * @param <T>
 */
public class QBeanWithPropertyCheck<T> extends QBean<T> {
    private QBeanWithPropertyCheck(Class<T> type, boolean fieldAccess, Expression<?>... args) {
        super(type, fieldAccess, args);
    }

    public static <T> QBeanWithPropertyCheck<T> fields(Class<T> type, Expression<?>... args) {
        return new QBeanWithPropertyCheck<>(type, true, args);
    }

    @Override
    protected void propertyNotFound(Expression<?> expr, String property) {
        throw new IllegalArgumentException(
                format("Property '%s' not found for expression %s", property, expr));
    }
}
