package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.REPSG;

import java.util.Objects;
import java.util.function.Function;

import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class GeoPunktBean {

    private final long nord;
    private final long ost;
    private final ReferenzBean epsgCode;
    private final Function<String, Boolean> isInAgs;

    public GeoPunktBean(long nord, long ost, ReferenzBean epsgCode,
            Function<String, Boolean> isInAgs) {
        this.nord = nord;
        this.ost = ost;
        this.epsgCode = epsgCode;
        this.isInAgs = isInAgs;
    }

    @RegelApi.Member(description = "Nord-Koordinate")
    public long getNord() {
        return nord;
    }

    @RegelApi.Member(description = "Ost-Koordinate")
    public long getOst() {
        return ost;
    }

    @RegelApi.Member(description = "EPSG-Code", referenzliste = REPSG)
    public ReferenzBean getEpsgCode() {
        return epsgCode;
    }

    @RegelApi.Member(description = "Prüft, ob der GeoPunkt in der durch die AGS-Kennziffer angegebenen Gemeinde liegt" +
            " (bis auf einen durch den Parameter 'geo_buff' spezifizierten Puffer)")
    public boolean isInAgs(String ags) {
        return isInAgs.apply(ags);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        GeoPunktBean that = (GeoPunktBean) o;
        return nord == that.nord && ost == that.ost && epsgCode.equals(that.epsgCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nord, ost, epsgCode);
    }

    @Override
    public String toString() {
        return "GeoPunktBean{" +
                "nord=" + nord +
                ", ost=" + ost +
                ", epsgCode=" + epsgCode +
                '}';
    }

}
