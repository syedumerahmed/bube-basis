package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDE_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.stammdaten.domain.AnlageDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.AnlagenteilDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.QuelleDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAdresse;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AdresseUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlageUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlagenteilUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetreiberUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetriebsstaetteUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.QuelleUebernahme;

@Service
public class StammdatenUebernahmeService {

    private final StammdatenService stammdatenService;
    private final StammdatenBetreiberService stammdatenBetreiberService;

    public StammdatenUebernahmeService(StammdatenService stammdatenService,
            StammdatenBetreiberService stammdatenBetreiberService) {
        this.stammdatenService = stammdatenService;
        this.stammdatenBetreiberService = stammdatenBetreiberService;
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Anlage uebernehmeNeueAnlage(Long spbAnlageId, Anlage anlage) {
        // Überprüfung, ob Anlagenummer in SPB bereits vergeben
        stammdatenBetreiberService
                .findSpbAnlage(anlage.getParentBetriebsstaetteId(), anlage.getAnlageNr())
                .filter(spbAnlage -> !spbAnlage.getId().equals(spbAnlageId))
                .ifPresent(a -> {
                    throw new AnlageDuplicateKeyException(
                            ("Eine andere Anlage mit der Nummer '%s' existiert bereits in den Betreiberdaten. " +
                             "Bitte diese erst übernehmen oder löschen.").formatted(anlage.getAnlageNr()));
                });

        var uebernommeneAnlage = stammdatenService.createAnlage(anlage);
        stammdatenBetreiberService.aktualisiereSpbNachAnlageUebernahme(anlage.getId(), spbAnlageId);
        uebernommeneAnlage.markiereAlsUebernommen();
        return uebernommeneAnlage;
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Anlage uebernehmeAnlage(AnlageUebernahme uebernahme) {
        Anlage anlage = this.stammdatenService.loadAnlage(uebernahme.getId());
        SpbAnlage spbAnlage = this.stammdatenBetreiberService.loadSpbAnlage(uebernahme.getSpbId());

        if (uebernahme.getName()) {
            anlage.setName(spbAnlage.getName());
        }
        if (uebernahme.getAnlageNr()) {
            anlage.setAnlageNr(spbAnlage.getAnlageNr());
        }
        if (uebernahme.getBetriebsstatus()) {
            anlage.setBetriebsstatus(spbAnlage.getBetriebsstatus());
        }
        if (uebernahme.getBetriebsstatusSeit()) {
            anlage.setBetriebsstatusSeit(spbAnlage.getBetriebsstatusSeit());
        }
        if (uebernahme.getInbetriebnahme()) {
            anlage.setInbetriebnahme(spbAnlage.getInbetriebnahme());
        }
        if (uebernahme.getGeoPunkt()) {
            anlage.setGeoPunkt(spbAnlage.getGeoPunkt());
        }
        if (uebernahme.getLeistungen()) {
            anlage.setLeistungen(new ArrayList<>(spbAnlage.getLeistungen()));
        }
        anlage.setBetreiberdatenUebernommen(true);

        stammdatenBetreiberService.deleteUebernommeneSpbAnlage(spbAnlage);
        return stammdatenService.updateAnlageForUebernahme(anlage);
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Anlagenteil uebernehmeNeuesAnlagenteil(Long spbAnlagenteilId, Anlagenteil anlagenteil) {
        // Überprüfung, ob Anlagenteilnummer in SPB bereits vergeben
        stammdatenBetreiberService
                .findSpbAnlagenteil(anlagenteil.getParentAnlageId(), anlagenteil.getAnlagenteilNr())
                .filter(spbAnlagenteil -> !spbAnlagenteil.getId().equals(spbAnlagenteilId))
                .ifPresent(a -> {
                    throw new AnlagenteilDuplicateKeyException(
                            ("Eine anderes Anlagenteil mit der Nummer '%s' existiert bereits in den Betreiberdaten. " +
                             "Bitte dieses erst übernehmen oder löschen.").formatted(anlagenteil.getAnlagenteilNr()));
                });
        var parentBetriebsstaetteId = stammdatenService.loadAnlage(anlagenteil.getParentAnlageId())
                                                       .getParentBetriebsstaetteId();
        var uebernommenesAnlagenteil = stammdatenService.createAnlagenteil(parentBetriebsstaetteId, anlagenteil);
        stammdatenBetreiberService.aktualisiereSpbNachAnlagenteilUebernahme(spbAnlagenteilId);
        uebernommenesAnlagenteil.markiereAlsUebernommen();
        return uebernommenesAnlagenteil;
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Anlagenteil uebernehmeAnlagenteil(AnlagenteilUebernahme uebernahme) {
        Anlagenteil anlagenteil = this.stammdatenService.loadAnlagenteil(uebernahme.getId());
        Anlage anlage = this.stammdatenService.loadAnlage(anlagenteil.getParentAnlageId());
        SpbAnlagenteil spbAnlagenteil = this.stammdatenBetreiberService.loadSpbAnlagenteilById(uebernahme.getSpbId());

        if (uebernahme.getName()) {
            anlagenteil.setName(spbAnlagenteil.getName());
        }
        if (uebernahme.getAnlagenteilNr()) {
            anlagenteil.setAnlagenteilNr(spbAnlagenteil.getAnlagenteilNr());
        }
        if (uebernahme.getBetriebsstatus()) {
            anlagenteil.setBetriebsstatus(spbAnlagenteil.getBetriebsstatus());
        }
        if (uebernahme.getBetriebsstatusSeit()) {
            anlagenteil.setBetriebsstatusSeit(spbAnlagenteil.getBetriebsstatusSeit());
        }
        if (uebernahme.getInbetriebnahme()) {
            anlagenteil.setInbetriebnahme(spbAnlagenteil.getInbetriebnahme());
        }
        if (uebernahme.getGeoPunkt()) {
            anlagenteil.setGeoPunkt(spbAnlagenteil.getGeoPunkt());
        }
        if (uebernahme.getLeistungen()) {
            anlagenteil.setLeistungen(new ArrayList<>(spbAnlagenteil.getLeistungen()));
        }
        anlagenteil.setBetreiberdatenUebernommen(true);
        anlagenteil.setLetzteAenderungBetreiber(null);

        stammdatenBetreiberService.deleteSpbAnlagenteil(anlage.getParentBetriebsstaetteId(), spbAnlagenteil.getId());
        return stammdatenService.updateAnlagenteil(anlage.getParentBetriebsstaetteId(), anlagenteil);
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Betreiber uebernehmeBetreiber(BetreiberUebernahme uebernahme) {
        Betreiber betreiber = stammdatenService.loadBetreiber(uebernahme.getId());
        SpbBetreiber spbBetreiber = stammdatenBetreiberService.loadSpbBetreiber(uebernahme.getSpbId());

        if (uebernahme.getName()) {
            betreiber.setName(spbBetreiber.getName());
        }

        betreiber.setAdresse(updateAdresse(uebernahme.getAdresse(), spbBetreiber.getAdresse(), betreiber.getAdresse()));
        betreiber.setBetreiberdatenUebernommen(true);
        betreiber.setLetzteAenderungBetreiber(null);

        stammdatenBetreiberService.deleteSpbBetreiber(spbBetreiber.getId());
        return stammdatenService.updateBetreiber(betreiber);
    }

    private Adresse updateAdresse(AdresseUebernahme uebernahme, SpbAdresse spbAdresse, Adresse adresse) {
        if (uebernahme.getStrasse()) {
            adresse.setStrasse(spbAdresse.getStrasse());
        }
        if (uebernahme.getHausNr()) {
            adresse.setHausNr(spbAdresse.getHausNr());
        }
        if (uebernahme.getPlz()) {
            adresse.setPlz(spbAdresse.getPlz());
        }
        if (uebernahme.getOrt()) {
            adresse.setOrt(spbAdresse.getOrt());
        }
        if (uebernahme.getOrtsteil()) {
            adresse.setOrtsteil(spbAdresse.getOrtsteil());
        }
        if (uebernahme.getPostfach()) {
            adresse.setPostfach(spbAdresse.getPostfach());
        }
        if (uebernahme.getPostfachPlz()) {
            adresse.setPostfachPlz(spbAdresse.getPostfachPlz());
        }
        if (uebernahme.getLandIsoCode()) {
            adresse.setLandIsoCode(spbAdresse.getLandIsoCode());
        }
        return adresse;
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Betriebsstaette uebernehmeBetriebsstaette(BetriebsstaetteUebernahme uebernahme) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForWrite(uebernahme.getId());
        SpbBetriebsstaette spbBetriebsstaette = stammdatenBetreiberService.loadSpbBetriebsstaette(
                uebernahme.getSpbId());

        if (uebernahme.getName()) {
            betriebsstaette.setName(spbBetriebsstaette.getName());
        }
        if (uebernahme.getAbfallerzeugerNummern()) {
            betriebsstaette.setAbfallerzeugerNummern(new ArrayList<>(spbBetriebsstaette.getAbfallerzeugerNummern()));
        }
        if (uebernahme.getEinleiterNummern()) {
            betriebsstaette.setEinleiterNummern(new ArrayList<>(spbBetriebsstaette.getEinleiterNummern()));
        }
        if (uebernahme.getFlusseinzugsgebiet()) {
            betriebsstaette.setFlusseinzugsgebiet(spbBetriebsstaette.getFlusseinzugsgebiet());
        }
        if (uebernahme.getNaceCode()) {
            betriebsstaette.setNaceCode(spbBetriebsstaette.getNaceCode());
        }
        if (uebernahme.getDirekteinleiter()) {
            betriebsstaette.setDirekteinleiter(spbBetriebsstaette.isDirekteinleiter());
        }
        if (uebernahme.getIndirekteinleiter()) {
            betriebsstaette.setIndirekteinleiter(spbBetriebsstaette.isIndirekteinleiter());
        }
        if (uebernahme.getGeoPunkt()) {
            betriebsstaette.setGeoPunkt(spbBetriebsstaette.getGeoPunkt());
        }
        if (uebernahme.getBetriebsstatusSeit()) {
            betriebsstaette.setBetriebsstatusSeit(spbBetriebsstaette.getBetriebsstatusSeit());
        }
        if (uebernahme.getBetriebsstatus()) {
            betriebsstaette.setBetriebsstatus(spbBetriebsstaette.getBetriebsstatus());
        }
        if (uebernahme.getInbetriebnahme()) {
            betriebsstaette.setInbetriebnahme(spbBetriebsstaette.getInbetriebnahme());
        }
        if (uebernahme.getAnsprechpartner()) {
            betriebsstaette.getAnsprechpartner().clear();
            betriebsstaette.getAnsprechpartner().addAll(new ArrayList<>(spbBetriebsstaette.getAnsprechpartner()));
        }
        if (uebernahme.getKommunikationsverbindungen()) {
            betriebsstaette.setKommunikationsverbindungen(
                    new ArrayList<>(spbBetriebsstaette.getKommunikationsverbindungen()));
        }

        betriebsstaette.setAdresse(
                updateAdresse(uebernahme.getAdresse(), spbBetriebsstaette.getAdresse(), betriebsstaette.getAdresse()));
        betriebsstaette.setBetreiberdatenUebernommen(true);

        stammdatenBetreiberService.deleteSpbBetriebsstaetteUebernahme(spbBetriebsstaette);
        return stammdatenService.updateBetriebsstaetteForUebernahme(betriebsstaette);
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Quelle uebernehmeNeueQuelle(Long spbQuelleId, Quelle quelle) {
        // Überprüfung, ob Quellennummer in SPB bereits vergeben
        Optional<SpbQuelle> otherSpbQuelle = quelle.getParentAnlageId() != null ?
                stammdatenBetreiberService
                        .findSpbQuelleOfAnlage(quelle.getParentAnlageId(), quelle.getQuelleNr()) :
                stammdatenBetreiberService
                        .findSpbQuelleOfBetriebsstaette(quelle.getParentBetriebsstaetteId(), quelle.getQuelleNr());
        otherSpbQuelle
                .filter(spbQuelle -> !spbQuelle.getId().equals(spbQuelleId))
                .ifPresent(q -> {
                    throw new QuelleDuplicateKeyException(
                            ("Eine andere Quelle mit der Nummer '%s' existiert bereits in den Betreiberdaten. " +
                             "Bitte diese erst übernehmen oder löschen.").formatted(quelle.getQuelleNr()));
                });
        var uebernommeneQuelle = stammdatenService.createQuelle(quelle);
        stammdatenBetreiberService.aktualisiereSpbNachQuelleUebernahme(spbQuelleId);
        uebernommeneQuelle.markiereAlsUebernommen();
        return uebernommeneQuelle;
    }

    @Transactional
    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Quelle uebernehmeQuelle(QuelleUebernahme uebernahme) {
        Quelle quelle = stammdatenService.loadQuelle(uebernahme.getId());
        SpbQuelle spbQuelle = stammdatenBetreiberService.loadSpbQuelle(uebernahme.getSpbId());

        if (uebernahme.getName()) {
            quelle.setName(spbQuelle.getName());
        }
        if (uebernahme.getQuelleNr()) {
            quelle.setQuelleNr(spbQuelle.getQuelleNr());
        }
        if (uebernahme.getGeoPunkt()) {
            quelle.setGeoPunkt(spbQuelle.getGeoPunkt());
        }
        if (uebernahme.getFlaeche()) {
            quelle.setFlaeche(spbQuelle.getFlaeche());
        }
        if (uebernahme.getLaenge()) {
            quelle.setLaenge(spbQuelle.getLaenge());
        }
        if (uebernahme.getBreite()) {
            quelle.setBreite(spbQuelle.getBreite());
        }
        if (uebernahme.getDurchmesser()) {
            quelle.setDurchmesser(spbQuelle.getDurchmesser());
        }
        if (uebernahme.getBauhoehe()) {
            quelle.setBauhoehe(spbQuelle.getBauhoehe());
        }
        quelle.setBetreiberdatenUebernommen(true);
        quelle.setLetzteAenderungBetreiber(null);

        stammdatenBetreiberService.deleteSpbQuelle(spbQuelle.getId());
        return stammdatenService.updateQuelle(quelle);
    }
}
