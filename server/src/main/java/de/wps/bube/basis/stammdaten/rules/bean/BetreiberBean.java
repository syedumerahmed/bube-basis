package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RCONF;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RJAHR;

import java.time.Instant;
import java.util.Objects;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.rules.bean.AdresseBean;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class BetreiberBean implements PruefObjekt {

    private final Long id;
    private final String betreiberNummer;
    private final ReferenzBean berichtsjahr;
    private final Land land;
    private final String referenzAnlagenkataster;
    private final AdresseBean adresse;
    private final String name;
    private final ReferenzBean vertraulichkeitsgrund;
    private final String url;
    private final String bemerkung;
    private final boolean betreiberdatenUebernommen;
    private final Instant ersteErfassung;
    private final Instant letzteAenderung;

    public BetreiberBean(Long id, String betreiberNummer, ReferenzBean berichtsjahr, Land land,
            String referenzAnlagenkataster, AdresseBean adresse, String name, ReferenzBean vertraulichkeitsgrund,
            String url, String bemerkung, boolean betreiberdatenUebernommen, Instant ersteErfassung,
            Instant letzteAenderung) {
        this.id = id;
        this.betreiberNummer = betreiberNummer;
        this.berichtsjahr = berichtsjahr;
        this.land = land;
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        this.adresse = adresse;
        this.name = name;
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        this.url = url;
        this.bemerkung = bemerkung;
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        this.ersteErfassung = ersteErfassung;
        this.letzteAenderung = letzteAenderung;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.BETREIBER;
    }

    public String getBetreiberNummer() {
        return betreiberNummer;
    }

    @RegelApi.Member(referenzliste = RJAHR)
    public ReferenzBean getBerichtsjahr() {
        return berichtsjahr;
    }

    public Land getLand() {
        return land;
    }

    @RegelApi.Member(description = "Anlagenkataster")
    public String getReferenzAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    @RegelApi.Member(description = "Adresse")
    public AdresseBean getAdresse() {
        return adresse;
    }

    @RegelApi.Member(description = "Name")
    public String getName() {
        return name;
    }

    @RegelApi.Member(description = "Vertraulichkeitsgrund", referenzliste = RCONF)
    public ReferenzBean getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    @RegelApi.Member(description = "URL")
    public String getUrl() {
        return url;
    }

    @RegelApi.Member(description = "Bemerkung")
    public String getBemerkung() {
        return bemerkung;
    }

    @RegelApi.Member(description = "Betreiberdaten übernommen")
    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BetreiberBean that = (BetreiberBean) o;
        return betreiberdatenUebernommen == that.betreiberdatenUebernommen && Objects.equals(id,
                that.id) && Objects.equals(betreiberNummer, that.betreiberNummer) && Objects.equals(
                berichtsjahr, that.berichtsjahr) && land == that.land && Objects.equals(referenzAnlagenkataster,
                that.referenzAnlagenkataster) && Objects.equals(adresse,
                that.adresse) && Objects.equals(name, that.name) && Objects.equals(
                vertraulichkeitsgrund, that.vertraulichkeitsgrund) && Objects.equals(url,
                that.url) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(ersteErfassung,
                that.ersteErfassung) && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betreiberNummer, berichtsjahr, land, referenzAnlagenkataster, adresse, name,
                vertraulichkeitsgrund, url, bemerkung, betreiberdatenUebernommen, ersteErfassung, letzteAenderung);
    }

    @Override
    public String toString() {
        return "BetreiberBean{" +
                "id=" + id +
                ", betreiberNummer='" + betreiberNummer + '\'' +
                ", berichtsjahr=" + berichtsjahr +
                ", land=" + land +
                ", referenzAnlagenkataster='" + referenzAnlagenkataster + '\'' +
                ", adresse=" + adresse +
                ", name='" + name + '\'' +
                ", vertraulichkeitsgrund=" + vertraulichkeitsgrund +
                ", url='" + url + '\'' +
                ", bemerkung='" + bemerkung + '\'' +
                ", betreiberdatenUebernommen=" + betreiberdatenUebernommen +
                ", ersteErfassung=" + ersteErfassung +
                ", letzteAenderung=" + letzteAenderung +
                '}';
    }

}
