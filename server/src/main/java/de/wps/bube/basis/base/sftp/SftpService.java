package de.wps.bube.basis.base.sftp;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.OpenSSHKnownHosts;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import net.schmizz.sshj.userauth.password.PasswordUtils;

public interface SftpService {

    @Service
    @EnableConfigurationProperties(SftpProperties.class)
    @ConditionalOnProperty("bube.sftp.host-key")
    class Remote implements SftpService {

        private final SftpProperties sftpProperties;
        private final SSHClientProvider sshClientProvider;

        public Remote(SftpProperties sftpProperties, SSHClientProvider sshClientProvider) {
            this.sftpProperties = sftpProperties;
            this.sshClientProvider = sshClientProvider;
        }

        public void uploadFile(File file) throws IOException {

            try (SSHClient ssh = sshClientProvider.newSSHClient()) {

                ssh.addHostKeyVerifier(new OpenSSHKnownHosts(new StringReader(sftpProperties.getHostKey())));

                if (sftpProperties.getPort() != null) {
                    ssh.connect(sftpProperties.getHost(), sftpProperties.getPort());
                } else {
                    ssh.connect(sftpProperties.getHost());
                }


                File keyFile = new File(sftpProperties.getKeyDir(), sftpProperties.getKeyName());
                KeyProvider keyProvider = ssh.loadKeys(keyFile.toString(),
                        PasswordUtils.createOneOff(sftpProperties.getPassphrase().toCharArray()));
                ssh.authPublickey(sftpProperties.getUsername(), keyProvider);

                try (SFTPClient sftp = ssh.newSFTPClient()) {
                    sftp.mkdirs(sftpProperties.getUploadDir());
                    sftp.put(file.getAbsolutePath(), sftpProperties.getUploadDir() + file.getName());
                }

            }

        }

    }

    @Service
    @ConditionalOnMissingBean(Remote.class)
    class Local implements SftpService {
        private static final Logger LOGGER = LoggerFactory.getLogger(Local.class);
        @Override
        public void uploadFile(File file) throws IOException {
            String targetFolder = "target/sftp/";
            new File(targetFolder).mkdirs();
            var target = Path.of(targetFolder, file.getName());
            LOGGER.info("Saving to local directory instead of SFTP-Server. File: " + target);
            Files.copy(file.toPath(), target);
        }
    }

    void uploadFile(File file) throws IOException;

}
