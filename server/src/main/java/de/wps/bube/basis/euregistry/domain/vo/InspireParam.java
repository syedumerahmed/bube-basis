package de.wps.bube.basis.euregistry.domain.vo;

public class InspireParam {

    // insp_ns
    private final String namespace;

    // insp_site
    private final String site;

    // insp_extension
    private final String extension;

    public InspireParam(String namespace, String site, String extension) {
        this.namespace = namespace;
        this.site = site;
        this.extension = extension;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getSite() {
        return site;
    }

    public String getExtension() {
        return extension;
    }
}
