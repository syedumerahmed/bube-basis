package de.wps.bube.basis.base.vo;

import java.util.stream.Stream;

/**
 * RLAND Bundesland
 */
public enum Land {

    DEUTSCHLAND("00"),
    SCHLESWIG_HOLSTEIN("01"),
    HAMBURG("02"),
    NIEDERSACHSEN("03"),
    BREMEN("04"),
    NORDRHEIN_WESTFALEN("05"),
    HESSEN("06"),
    RHEINLAND_PFALZ("07"),
    BADEN_WUERTTEMBERG("08"),
    BAYERN("09"),
    SAARLAND("10"),
    BERLIN("11"),
    BRANDENBURG("12"),
    MECKLENBURG_VORPOMMERN("13"),
    SACHSEN("14"),
    SACHSEN_ANHALT("15"),
    THUERINGEN("16");

    private final String nr;

    Land(final String nr) {
        this.nr = nr;
    }

    public String getNr() {
        return nr;
    }

    public static Land of(final String nr) {
        return Stream.of(values())
                     .filter(p -> p.getNr().equals(nr))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Landesschlüssel: " + nr));
    }
}


