package de.wps.bube.basis.stammdatenuebernahme.domain.vo;

public class AdresseUebernahme {
    private Boolean strasse;
    private Boolean hausNr;
    private Boolean plz;
    private Boolean ort;
    private Boolean ortsteil;
    private Boolean postfachPlz;
    private Boolean postfach;
    private Boolean landIsoCode;

    public AdresseUebernahme() {
    }

    public AdresseUebernahme(Boolean strasse, Boolean hausNr, Boolean plz, Boolean ort, Boolean ortsteil,
            Boolean postfachPlz, Boolean postfach, Boolean landIsoCode) {
        this.strasse = strasse;
        this.hausNr = hausNr;
        this.plz = plz;
        this.ort = ort;
        this.ortsteil = ortsteil;
        this.postfachPlz = postfachPlz;
        this.postfach = postfach;
        this.landIsoCode = landIsoCode;
    }

    public Boolean getLandIsoCode() {
        return landIsoCode;
    }

    public void setLandIsoCode(Boolean landIsoCode) {
        this.landIsoCode = landIsoCode;
    }

    public Boolean getPostfach() {
        return postfach;
    }

    public void setPostfach(Boolean postfach) {
        this.postfach = postfach;
    }

    public Boolean getPostfachPlz() {
        return postfachPlz;
    }

    public void setPostfachPlz(Boolean postfachPlz) {
        this.postfachPlz = postfachPlz;
    }

    public Boolean getOrtsteil() {
        return ortsteil;
    }

    public void setOrtsteil(Boolean ortsteil) {
        this.ortsteil = ortsteil;
    }

    public Boolean getOrt() {
        return ort;
    }

    public void setOrt(Boolean ort) {
        this.ort = ort;
    }

    public Boolean getPlz() {
        return plz;
    }

    public void setPlz(Boolean plz) {
        this.plz = plz;
    }

    public Boolean getHausNr() {
        return hausNr;
    }

    public void setHausNr(Boolean hausNr) {
        this.hausNr = hausNr;
    }

    public Boolean getStrasse() {
        return strasse;
    }

    public void setStrasse(Boolean strasse) {
        this.strasse = strasse;
    }
}
