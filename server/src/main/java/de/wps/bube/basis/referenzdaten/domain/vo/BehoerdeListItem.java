package de.wps.bube.basis.referenzdaten.domain.vo;

import de.wps.bube.basis.base.vo.Land;

public class BehoerdeListItem {
    public Long id;
    public Land land;
    public String schluessel;
    public String bezeichnung;
    public String kreis;
    public Gueltigkeitsjahr gueltigVon;
    public Gueltigkeitsjahr gueltigBis;
}
