package de.wps.bube.basis.referenzdaten.domain.entity;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class GeoPunkt {

    private long nord;
    private long ost;
    @ManyToOne
    private Referenz epsgCode;

    @Deprecated
    protected GeoPunkt() {
    }

    public GeoPunkt(long nord, long ost, Referenz epsgCode) {
        this.nord = nord;
        this.ost = ost;
        this.epsgCode = epsgCode;
    }

    public long getNord() {
        return nord;
    }

    public long getOst() {
        return ost;
    }

    public Referenz getEpsgCode() {
        return epsgCode;
    }

    public void setEpsgCode(Referenz r) {
        this.epsgCode = r;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GeoPunkt geoPunkt = (GeoPunkt) o;
        return nord == geoPunkt.nord && ost == geoPunkt.ost && Objects.equals(epsgCode, geoPunkt.epsgCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nord, ost, epsgCode);
    }
}
