package de.wps.bube.basis.euregistry.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATC;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRELCH;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.AnlagenteilBean;
import de.wps.bube.basis.stammdaten.rules.bean.StammdatenObjektBean;

public class EURegAnlageBean implements PruefObjekt {

    private final Long id;
    private AnlageBean anlage;
    private AnlagenteilBean anlagenteil;
    private final BerichtsdatenBean berichtsdaten;
    private final Instant ersteErfassung;
    private final Instant letzteAenderung;
    private final String localId2;
    private final Ausgangszustandsbericht report;
    private final int visits;
    private final String visitUrl;
    private final String eSpirsNr;
    private final String tehgNr;
    private final String monitor;
    private final String monitorUrl;
    private final List<ReferenzBean> kapitelIeRl;
    private final List<ReferenzBean> anwendbareBvt;
    private final List<BVTAusnahmeBean> bvtAusnahmen;
    private final List<AuflageBean> auflagen;
    private final GenehmigungBean genehmigung;
    private Supplier<EURegBetriebsstaetteBean> euRegBetriebsstaette;

    public EURegAnlageBean(Long id, BerichtsdatenBean berichtsdaten, Instant ersteErfassung, Instant letzteAenderung,
            String localId2, Ausgangszustandsbericht report, int visits, String visitUrl, String eSpirsNr,
            String tehgNr, String monitor, String monitorUrl, List<ReferenzBean> kapitelIeRl,
            List<ReferenzBean> anwendbareBvt, List<BVTAusnahmeBean> bvtAusnahmen, List<AuflageBean> auflagen,
            GenehmigungBean genehmigung) {
        this.id = id;
        this.berichtsdaten = berichtsdaten;
        this.ersteErfassung = ersteErfassung;
        this.letzteAenderung = letzteAenderung;
        this.localId2 = localId2;
        this.report = report;
        this.visits = visits;
        this.visitUrl = visitUrl;
        this.eSpirsNr = eSpirsNr;
        this.tehgNr = tehgNr;
        this.monitor = monitor;
        this.monitorUrl = monitorUrl;
        this.kapitelIeRl = kapitelIeRl;
        this.anwendbareBvt = anwendbareBvt;
        this.bvtAusnahmen = bvtAusnahmen;
        this.auflagen = auflagen;
        this.genehmigung = genehmigung;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.EUREG_ANLAGE;
    }

    public AnlageBean getAnlage() {
        return anlage;
    }

    public void setAnlage(AnlageBean anlage) {
        this.anlage = anlage;
    }

    public AnlagenteilBean getAnlagenteil() {
        return anlagenteil;
    }

    public void setAnlagenteil(AnlagenteilBean anlagenteil) {
        this.anlagenteil = anlagenteil;
    }

    public BerichtsdatenBean getBerichtsdaten() {
        return berichtsdaten;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @RegelApi.Member(description = "Local ID für InspireId eines fallweise generierten Objektes ProductionInstallation oder ProductionInstallationPart")
    public String getLocalId2() {
        return localId2;
    }

    @RegelApi.Member(description = "Ausgangszustandsbericht")
    public Ausgangszustandsbericht getReport() {
        return report;
    }

    @RegelApi.Member(description = "Anzahl der im Berichtsjahr durchgeführten Vor-Ort-Besichtigungen")
    public int getVisits() {
        return visits;
    }

    @RegelApi.Member(description = "URL, wo die Kurzberichte zur Vor-Ort-Besichtigung einsehbar sind")
    public String getVisitUrl() {
        return visitUrl;
    }

    @RegelApi.Member(description = "Identifikator für Störfallanlagen nach Seveso Plant Information Retrieval System (SPIRS), Richtlinie 2012/18/EU (eSPIRS-Nummer)")
    public String getESpirsNr() {
        return eSpirsNr;
    }

    @RegelApi.Member(description = "Identifikator Emissionshandel, wie auf EU-Ebene geführt; Richtlinie 2003/87/EG")
    public String getTehgNr() {
        return tehgNr;
    }

    @RegelApi.Member(description = "Beschreibung des öffentlichen Emissions-Monitorings nach Art. 24(3)(b) 2010-75-EU")
    public String getMonitor() {
        return monitor;
    }

    @RegelApi.Member(description = "Adresse für weitere Informationen zum Emissionsmonitoring")
    public String getMonitorUrl() {
        return monitorUrl;
    }

    @RegelApi.Member(description = "Relevantes Kapitel der IE-RL, das auf die Anlage bzw. den Anlagenteil anwendbar ist", referenzliste = RRELCH)
    public List<ReferenzBean> getKapitelIeRl() {
        return kapitelIeRl;
    }

    @RegelApi.Member(description = "Anwendbare Durchführungsbeschlüsse", referenzliste = RBATC)
    public List<ReferenzBean> getAnwendbareBvt() {
        return anwendbareBvt;
    }

    @RegelApi.Member(description = "BVT-Ausnahmen")
    public List<BVTAusnahmeBean> getBvtAusnahmen() {
        return bvtAusnahmen;
    }

    @RegelApi.Member(description = "Strengere Genehmigungsauflagen")
    public List<AuflageBean> getAuflagen() {
        return auflagen;
    }

    @RegelApi.Member(description = "Genehmigung")
    public GenehmigungBean getGenehmigung() {
        return genehmigung;
    }

    @RegelApi.Member(description = "Betriebsstättenbericht")
    public EURegBetriebsstaetteBean getEURegBetriebsstaette() {
        return euRegBetriebsstaette.get();
    }

    public void setEuRegBetriebsstaette(Supplier<EURegBetriebsstaetteBean> euRegBetriebsstaette) {
        this.euRegBetriebsstaette = Suppliers.memoize(euRegBetriebsstaette::get);
    }

    @RegelApi.Member(description = "Zugehöriges Stammdaten-Objekt (entweder vom Typ 'Anlage' oder 'Anlagenteil')")
    public StammdatenObjektBean<?> getStammdatenObjekt() {
        return anlage != null ? anlage : anlagenteil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EURegAnlageBean that))
            return false;
        return visits == that.visits
                && Objects.equals(id, that.id)
                && Objects.equals(anlage, that.anlage)
                && Objects.equals(anlagenteil, that.anlagenteil)
                && Objects.equals(berichtsdaten, that.berichtsdaten)
                && Objects.equals(ersteErfassung, that.ersteErfassung)
                && Objects.equals(letzteAenderung, that.letzteAenderung)
                && Objects.equals(localId2, that.localId2)
                && report == that.report
                && Objects.equals(visitUrl, that.visitUrl)
                && Objects.equals(eSpirsNr, that.eSpirsNr)
                && Objects.equals(tehgNr, that.tehgNr)
                && Objects.equals(monitor, that.monitor)
                && Objects.equals(monitorUrl, that.monitorUrl)
                && Objects.equals(kapitelIeRl, that.kapitelIeRl)
                && Objects.equals(anwendbareBvt, that.anwendbareBvt)
                && Objects.equals(bvtAusnahmen, that.bvtAusnahmen)
                && Objects.equals(auflagen, that.auflagen)
                && Objects.equals(genehmigung, that.genehmigung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, anlage, anlagenteil, berichtsdaten, ersteErfassung, letzteAenderung, localId2, report,
                visits, visitUrl, eSpirsNr, tehgNr, monitor, monitorUrl, kapitelIeRl, anwendbareBvt, bvtAusnahmen,
                auflagen, genehmigung);
    }

    @Override
    public String toString() {
        return "EURegAnlageBean{" +
                "id=" + id +
                ", anlage=" + anlage +
                ", anlagenteil=" + anlagenteil +
                ", berichtsdaten=" + berichtsdaten +
                ", ersteErfassung=" + ersteErfassung +
                ", letzteAenderung=" + letzteAenderung +
                ", localId2='" + localId2 + '\'' +
                ", report=" + report +
                ", visits=" + visits +
                ", visitUrl='" + visitUrl + '\'' +
                ", eSpirsNr='" + eSpirsNr + '\'' +
                ", tehgNr='" + tehgNr + '\'' +
                ", monitor='" + monitor + '\'' +
                ", monitorUrl='" + monitorUrl + '\'' +
                ", kapitelIeRl=" + kapitelIeRl +
                ", anwendbareBvt=" + anwendbareBvt +
                ", bvtAusnahmen=" + bvtAusnahmen +
                ", auflagen=" + auflagen +
                ", genehmigung=" + genehmigung +
                '}';
    }

}
