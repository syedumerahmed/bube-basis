package de.wps.bube.basis.stammdatenbetreiber.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbAnlageDto;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlageListItemDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.web.VorschriftMapper;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlageListItem;

@Mapper(uses = { ReferenzMapper.class, VorschriftMapper.class })
public abstract class SpbAnlageMapper {

    public abstract SpbAnlageDto toDto(SpbAnlage spbAnlage);

    public abstract SpbAnlage fromDto(SpbAnlageDto spbAnlageDto);

    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    public abstract SpbAnlageListItemDto toListItemDto(SpbAnlageListItem spbAnlageListItem);
}
