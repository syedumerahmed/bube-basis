package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.euregistry.rules.bean.BVTAusnahmeBean;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class BVTAusnahmeBeanMapper {

    public abstract BVTAusnahmeBean toBean(BVTAusnahme bvtAusnahme);

}
