package de.wps.bube.basis.euregistry.xml.bericht.dto.base2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;

@XmlAccessorType(XmlAccessType.FIELD)
public class ThematicIdentifier {
    @XmlElement(name = "identifier", namespace = SchemaSet.SCHEMA_BASE2)
    private String identifier;

    @XmlElement(name = "identifierScheme", namespace = SchemaSet.SCHEMA_BASE2)
    private String identifierScheme;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierScheme() {
        return identifierScheme;
    }

    public void setIdentifierScheme(String identifierScheme) {
        this.identifierScheme = identifierScheme;
    }

}
