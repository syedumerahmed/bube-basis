package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureNameContainer {

    @XmlElement(required = true, name = "FeatureName")
    private final FeatureName featureName = new FeatureName();

    public String getNameOfFeature() {
        return featureName.getNameOfFeature();
    }

    public void setNameOfFeature(String nameOfFeature) {
        featureName.setNameOfFeature(nameOfFeature);
    }

    public ReferenceType getConfidentialityReason() {
        return featureName.getConfidentialityReason();
    }

    public void setConfidentialityReason(ReferenceType confidentialityReason) {
        featureName.setConfidentialityReason(confidentialityReason);
    }
}
