package de.wps.bube.basis.referenzdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.rules.bean.BehoerdeBean;

@Mapper(uses = { AdresseBeanMapper.class, KommunikationsverbindungBeanMapper.class })
public abstract class BehoerdeBeanMapper {

    public abstract BehoerdeBean toBean(Behoerde behoerde);

    int map(Gueltigkeitsjahr gueltigkeitsjahr) {
        return gueltigkeitsjahr.getJahr();
    }

}
