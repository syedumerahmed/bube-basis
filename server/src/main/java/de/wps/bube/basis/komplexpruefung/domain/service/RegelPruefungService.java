package de.wps.bube.basis.komplexpruefung.domain.service;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.TEILBERICHT;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.ReflectivePropertyAccessor;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingException;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Service
public class RegelPruefungService {

    public static class StopWatch {

        private long mapping;
        private long evaluation;

        private StopWatch() {
        }

        public StopWatch reset() {
            mapping = 0;
            evaluation = 0;
            return this;
        }

        public void startMapping() {
            mapping -= System.currentTimeMillis();
        }

        public void stopMapping() {
            mapping += System.currentTimeMillis();
        }

        public void startEvaluation() {
            evaluation -= System.currentTimeMillis();
        }

        public void stopEvaluation() {
            evaluation += System.currentTimeMillis();
        }

        public long getMapping() {
            return mapping;
        }

        public long getEvaluation() {
            return evaluation;
        }
    }

    public static class RegelPruefungEvaluationContext extends StandardEvaluationContext implements AutoCloseable {

        private static final ThreadLocal<RegelPruefungEvaluationContext> INSTANCE = new ThreadLocal<>();

        private final Gueltigkeitsjahr jahr;
        private final Land land;
        private final Long behoerdeId;
        private final ParameterService parameterService;

        public RegelPruefungEvaluationContext(Gueltigkeitsjahr jahr, Land land, Long behoerdeId,
                ParameterService parameterService) {
            setPropertyAccessors(List.of(new ReflectivePropertyAccessor(false)));
            this.jahr = jahr;
            this.land = land;
            this.behoerdeId = behoerdeId;
            this.parameterService = parameterService;
            INSTANCE.set(this);
        }

        private String getParameterWert(String schluessel) {
            Optional<ParameterWert> parameterWert = behoerdeId != null ?
                    parameterService.findParameterWert(schluessel, jahr, land, behoerdeId) :
                    parameterService.findParameterWert(schluessel, jahr, land);
            return parameterWert.map(ParameterWert::getWert).orElse(null);
        }

        @Override
        public void close() {
            INSTANCE.remove();
        }

    }

    private static final ThreadLocal<StopWatch> STOP_WATCH = ThreadLocal.withInitial(StopWatch::new);

    public static StopWatch getStopWatch() {
        return STOP_WATCH.get();
    }

    public static Number number(String numberString) throws java.text.ParseException {
        return NumberFormat.getNumberInstance().parse(numberString);
    }

    public static Temporal date(String dateString) throws java.text.ParseException {
        Date date = DateUtils.parseDate(dateString, Locale.getDefault(),
                "dd.MM.yyyy HH:mm:ss", "dd.MM.yyyy", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("Europe/Berlin"));
        return dateString.length() < 12 ? zonedDateTime.toLocalDate() : zonedDateTime.toLocalDateTime();
    }

    public static Number abs(Number number) {
        if (number instanceof Integer i) {
            return Math.abs(i);
        } else if (number instanceof Long l) {
            return Math.abs(l);
        } else if (number.doubleValue() % 1 != 0) {
            return Math.abs(number.doubleValue());
        } else if (number.longValue() <= Integer.MAX_VALUE && number.longValue() >= Integer.MIN_VALUE) {
            return Math.abs(number.intValue());
        } else {
            return Math.abs(number.longValue());
        }
    }

    public static String para(String schluessel) {
        return RegelPruefungEvaluationContext.INSTANCE.get().getParameterWert(schluessel);
    }

    private final ParameterService parameterService;

    public RegelPruefungService(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    public RegelPruefungErgebnis pruefeRegeln(Regel regel, Land land, Gueltigkeitsjahr jahr,
            Long behoerdeId, Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector)
            throws ParseException {
        // Betriebsstätten-Regeln dürfen nicht den Objekttyp "Teilbericht" beinhalten, diese Regeln werden
        // separat bei der Teilbericht-Erstellung geprüft: RegelPruefungService.pruefeTeilberichtRegeln
        if (regel.getObjekte().contains(TEILBERICHT)) {
            throw new IllegalArgumentException("Regel %d ist ungültig: Darf nicht Objekt %s enthalten"
                    .formatted(regel.getNummer(), TEILBERICHT.getBezeichner()));
        }
        SpelExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(regel.getCode());

        return pruefeRegeln(expression, regel.getObjekte(), land, jahr, behoerdeId, false,
                pruefObjektCollector);
    }

    public RegelPruefungEvaluationContext setupContext(Gueltigkeitsjahr jahr, Land land, Long behoerdeId) {
        RegelPruefungEvaluationContext context = new RegelPruefungEvaluationContext(jahr, land, behoerdeId,
                parameterService);
        context.setVariable("JAHR", jahr.getJahr());
        context.setVariable("LAND", land);
        try {
            context.setVariable("NUMBER", this.getClass().getDeclaredMethod("number", String.class));
            context.setVariable("DATE", this.getClass().getDeclaredMethod("date", String.class));
            context.setVariable("ABS", this.getClass().getDeclaredMethod("abs", Number.class));
            context.setVariable("PARA", this.getClass().getDeclaredMethod("para", String.class));
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
        return context;
    }

    public RegelPruefungErgebnis pruefeRegeln(Expression expression, List<RegelObjekt> regelObjekte,
            Land land, Gueltigkeitsjahr jahr, Long behoerdeId, boolean optimize,
            Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector) {

        StopWatch stopWatch = STOP_WATCH.get();

        RegelPruefungErgebnis ergebnis = new RegelPruefungErgebnis();
        Map<RegelObjekt, List<PruefObjekt>> pruefObjekteMap = getPruefObjekte(regelObjekte, pruefObjektCollector);

        if (pruefObjekteMap.isEmpty()) {
            return ergebnis;
        }

        try (RegelPruefungEvaluationContext context = setupContext(jahr, land, behoerdeId)) {
            //  Prüfe jedes Element auf Gültigkeit.
            for (List<PruefObjekt> pruefObjekte : pruefObjekteMap.values()) {
                for (PruefObjekt pruefObjekt : pruefObjekte) {
                    context.setVariable("OBJ", pruefObjekt);
                    try {
                        Boolean value;
                        stopWatch.startEvaluation();
                        try {
                            value = expression.getValue(context, Boolean.class);
                        } catch (EvaluationException e) {
                            Throwable t = e;
                            // ist irgendwo eine BeanMappingContext eingepackt?
                            do {
                                t = t.getCause();
                                if (t instanceof BeanMappingException bme) {
                                    // Wenn ja, dann wirf sie
                                    throw bme;
                                }
                            } while (t != null && !t.equals(t.getCause()));
                            // Wenn nein, wirf die Original-Exception
                            throw e;
                        } finally {
                            stopWatch.stopEvaluation();
                        }
                        boolean result = Boolean.TRUE.equals(value);
                        ergebnis.add(pruefObjekt, result);
                        if (!result && optimize) {
                            return ergebnis;
                        }
                    } catch (EvaluationException e) {
                        ergebnis.addFehler(pruefObjekt, e);
                    }
                }
            }
        }

        return ergebnis;
    }

    private Map<RegelObjekt, List<PruefObjekt>> getPruefObjekte(List<RegelObjekt> regelObjekte,
            Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector) {
        StopWatch stopWatch = STOP_WATCH.get();
        EnumMap<RegelObjekt, List<PruefObjekt>> result = new EnumMap<>(RegelObjekt.class);
        for (RegelObjekt regelObjekt : regelObjekte) {
            List<PruefObjekt> pruefObjekte;
            stopWatch.startMapping();
            try {
                pruefObjekte = pruefObjektCollector.apply(regelObjekt);
            } finally {
                stopWatch.stopMapping();
            }
            if (pruefObjekte != null && !pruefObjekte.isEmpty()) {
                result.put(regelObjekt, pruefObjekte);
            }
        }
        return result;
    }

}
