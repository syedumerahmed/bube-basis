package de.wps.bube.basis.stammdatenbetreiber.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;

public interface SpbQuelleRepository extends JpaRepository<SpbQuelle, Long>,
        QuerydslPredicateProjectionRepository<SpbQuelle> {

    List<SpbQuelle> findByParentAnlageId(Long parentAnlageId);

    List<SpbQuelle> findByParentAnlageIdAndQuelleIdIsNull(Long parentAnlageId);

    List<SpbQuelle> findByParentSpbAnlageId(Long parentSpbAnlageId);

    List<SpbQuelle> findByParentBetriebsstaetteId(Long betriebsstaetteId);

    List<SpbQuelle> findByParentBetriebsstaetteIdAndQuelleIdIsNull(Long betriebsstaetteId);

    Optional<SpbQuelle> findOneByParentBetriebsstaetteIdAndQuelleId(Long betriebsstaetteId, Long quelleId);

    Optional<SpbQuelle> findOneByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(Long betriebsstaetteId, String quelleNr);

    Optional<SpbQuelle> findOneByParentAnlageIdAndQuelleId(Long parentAnlageId, Long quelleId);

    Optional<SpbQuelle> findOneByParentAnlageIdAndQuelleNrIgnoreCase(Long parentAnlageId, String quelleNr);

    Optional<SpbQuelle> findOneByParentSpbAnlageIdAndQuelleNrIgnoreCase(Long parentSpbAnlageId, String quelleNr);

    Optional<SpbQuelle> findByQuelleId(Long id);

    void deleteAllByParentSpbAnlageId(Long spbAnlageId);

    void deleteAllByParentAnlageId(Long anlageId);

    boolean existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(Long parentBetriebsstaetteId, String quelleNr);

    boolean existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCaseAndIdNot(Long parentBetriebsstaetteId, String quelleNr,
            Long exceptedSpbQuelleId);

    boolean existsByParentAnlageIdAndQuelleNrIgnoreCase(Long parentAnlageId, String quelleNr);

    boolean existsByParentAnlageIdAndQuelleNrIgnoreCaseAndIdNot(Long parentAnlageId, String quelleNr,
            Long exceptedSpbQuelleId);

}
