package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.QReferenz.referenz;
import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QSort;
import org.springframework.data.repository.query.Param;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;

import de.wps.bube.basis.base.persistence.JoinProperties;
import de.wps.bube.basis.base.persistence.QBeanWithPropertyCheck;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;

public interface BetriebsstaetteRepository
        extends JpaRepository<Betriebsstaette, Long>, QuerydslPredicateProjectionRepository<Betriebsstaette> {

    static Predicate berichtsjahrIdEquals(long berichtsjahrId) {
        return betriebsstaette.berichtsjahr.id.eq(berichtsjahrId);
    }

    static Predicate byStammdatenKey(String jahrSchluessel, Land land, String nummer) {
        return betriebsstaette.land.eq(land)
                                   .and(betriebsstaette.berichtsjahr.schluessel.eq(jahrSchluessel))
                                   .and(betriebsstaette.betriebsstaetteNr.equalsIgnoreCase(nummer));
    }

    List<Betriebsstaette> findAll(Predicate predicate);

    long countAllByBetreiberId(Long id);

    boolean existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(Long berichtsjahrId, Land land,
            String betriebsstaetteNr);

    @Query("select count (distinct land) from Betriebsstaette where id in :ids")
    long countDistinctByLandAndIdIn(@Param("ids") Collection<Long> ids);

    @Query(value = """
            select exists(select b.local_id, a.local_id, an.local_id from betriebsstaette b
            left join anlage a on b.id = a.parent_betriebsstaette_id
            left join anlagenteil an on a.id = an.parent_anlage_id
                where b.land = :landNr
                  and b.berichtsjahr_id = :berichtsjahrId
                  and :localId in (b.local_id, a.local_id, an.local_id))""", nativeQuery = true)
    boolean localIdExists(String localId, String landNr, Long berichtsjahrId);

    @Query(value = """
            select exists(select zustaendige_behoerde_id as behoerde_id from betriebsstaette where zustaendige_behoerde_id = :id
                       union select behoerde_id from betriebsstaette_zustaendige_behoerden where behoerde_id = :id
                       union select behoerde_id from anlage_zustaendige_behoerden where behoerde_id = :id
                       union select behoerde_id from anlagenteil_zustaendige_behoerden where behoerde_id = :id) as a""",
            nativeQuery = true)
    boolean behoerdeUsed(Long id);

    Stream<Betriebsstaette> findAllByBerichtsjahr(Referenz berichtsjahr);

    Stream<Betriebsstaette> streamAllByIdIn(List<Long> ids);

    Stream<Betriebsstaette> findByBetreiberId(Long id);

    default List<String> findAllBetriebsstaettenNr(Predicate predicate) {
        return findAll(predicate, Projections.constructor(String.class, betriebsstaette.betriebsstaetteNr));
    }

    default List<Long> findAllIds(Predicate predicate) {
        return findAll(predicate, Projections.constructor(Long.class, betriebsstaette.id));
    }

    default Page<BetriebsstaetteListItemView> findAllAsListItems(Predicate predicate, Pageable page) {
        return findAll(predicate, page, QBeanWithPropertyCheck.fields(BetriebsstaetteListItemView.class,
                        betriebsstaette.id,
                        betriebsstaette.zustaendigeBehoerde,
                        betriebsstaette.name,
                        betriebsstaette.land,
                        betriebsstaette.betriebsstaetteNr,
                        betriebsstaette.localId,
                        betriebsstaette.betriebsstatus,
                        betriebsstaette.adresse.ort,
                        betriebsstaette.adresse.plz,
                        betriebsstaette.adresse.strasse,
                        betriebsstaette.adresse.hausNr,
                        betriebsstaette.akz,
                        betriebsstaette.gemeindekennziffer,
                        betriebsstaette.pruefungsfehler,
                        betriebsstaette.pruefungVorhanden,
                        betriebsstaette.letzteAenderungBetreiber,
                        betriebsstaette.betreiberdatenUebernommen),
                new JoinProperties<>(betriebsstaette.zustaendigeBehoerde, behoerde),
                new JoinProperties<>(betriebsstaette.betriebsstatus, referenz),
                new JoinProperties<>(betriebsstaette.gemeindekennziffer, referenz));
    }

    default List<String> findAllZustaendigeBehoerdenSchluessel(List<String> betriebsstaettenNummern, Land land) {
        List<String> upper = betriebsstaettenNummern.stream().map(String::toUpperCase).toList();
        return findDistinct(
                betriebsstaette.land.eq(land).and(betriebsstaette.betriebsstaetteNr.upper().in(upper)),
                QSort.by(betriebsstaette.zustaendigeBehoerde.schluessel.asc()),
                Projections.constructor(String.class, betriebsstaette.zustaendigeBehoerde.schluessel));
    }

    @Modifying
    @Query("update Betriebsstaette bst set bst.pruefungsfehler = :fehler, bst.pruefungVorhanden = true where bst.id = :bstId")
    void updateKomplexpruefung(@Param("bstId") Long betriebsstaetteId, @Param("fehler") boolean pruefungsfehler);

    @Modifying
    @Query("update Betriebsstaette bst set bst.schreibsperreBetreiber = :sperre where bst.id = :bstId")
    void updateSchreibsperreBetreiber(@Param("bstId") Long betriebsstaetteId, @Param("sperre") boolean sperre);

    @Modifying
    @Query("""
            update Betriebsstaette bst
                     set bst.letzteAenderungBetreiber  = current_timestamp,
                         bst.betreiberdatenUebernommen = false
                     where bst.id = :betriebsstaetteId""")
    void aenderungDurchBetreiber(Long betriebsstaetteId);

}
