package de.wps.bube.basis.base.mapping;

import static java.lang.String.format;

public class MappingError {

    public final String entityIdentifier;
    private final String fehlertext;

    private boolean istWarnung = false;

    public MappingError(String entityIdentifier, String fehlertext) {
        this.entityIdentifier = entityIdentifier;
        this.fehlertext = fehlertext;
    }

    public MappingError(String entityIdentifier, String fehlertext, boolean istWarnung) {
        this.entityIdentifier = entityIdentifier;
        this.fehlertext = fehlertext;
        this.istWarnung = istWarnung;
    }

    public boolean istWarnung() {
        return istWarnung;
    }

    @Override
    public String toString() {
        return format("%s: %s", entityIdentifier, fehlertext);
    }
}
