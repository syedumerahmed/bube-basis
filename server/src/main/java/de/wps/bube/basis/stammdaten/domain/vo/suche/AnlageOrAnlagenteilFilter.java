package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;
import java.util.function.Function;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.QReferenz;
import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public class AnlageOrAnlagenteilFilter implements Filter {
    private final Filter anlageFilter;
    private final Filter anlagenteilFilter;

    private AnlageOrAnlagenteilFilter(Filter anlageFilter, Filter anlagenteilFilter) {
        this.anlageFilter = anlageFilter;
        this.anlagenteilFilter = anlagenteilFilter;
    }

    public static AnlageOrAnlagenteilFilter forStringExpression(Function<QBetriebsstaette, StringExpression> anlagePath,
            Function<QBetriebsstaette, StringExpression> anlageteilPath,
            Function<Suchattribute, String> attributExtractor) {
        return new AnlageOrAnlagenteilFilter(new StringFilter(anlagePath, attributExtractor),
                new StringFilter(anlageteilPath, attributExtractor));
    }

    public static AnlageOrAnlagenteilFilter forBooleanExpression(
            Function<QBetriebsstaette, BooleanExpression> anlagePath,
            Function<QBetriebsstaette, BooleanExpression> anlageteilPath,
            Function<Suchattribute, Boolean> attributExtractor) {
        return new AnlageOrAnlagenteilFilter(new BooleanFilter(anlagePath, attributExtractor),
                new BooleanFilter(anlageteilPath, attributExtractor));
    }

    public static AnlageOrAnlagenteilFilter forReferenz(Function<QBetriebsstaette, QReferenz> anlageReferenz,
            Function<QBetriebsstaette, QReferenz> anlageteilReferenz,
            Function<Suchattribute, List<Long>> attributExtractor) {
        return new AnlageOrAnlagenteilFilter(new ReferenzFilter(anlageReferenz, attributExtractor),
                new ReferenzFilter(anlageteilReferenz, attributExtractor));
    }

    public static AnlageOrAnlagenteilFilter forBehoerde(Function<QBetriebsstaette, QBehoerde> anlageBehoerde,
            Function<QBetriebsstaette, QBehoerde> anlageteilBehoerde,
            Function<Suchattribute, List<Long>> attributExtractor) {
        return new AnlageOrAnlagenteilFilter(new BehoerdeFilter(anlageBehoerde, attributExtractor),
                new BehoerdeFilter(anlageteilBehoerde, attributExtractor));
    }

    @Override
    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        var builder = new BooleanBuilder(anlageFilter.createPredicate(suchattribute, bstRoot));
        if (suchattribute.anlage.searchAnlagenteile) {
            builder.or(anlagenteilFilter.createPredicate(suchattribute, bstRoot));
        }
        return builder.getValue();
    }
}
