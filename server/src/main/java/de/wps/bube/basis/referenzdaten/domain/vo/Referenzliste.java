package de.wps.bube.basis.referenzdaten.domain.vo;

import java.util.EnumSet;
import java.util.Set;

public enum Referenzliste {

    /**
     * Durchführungsbeschluss
     * <p>
     * Anwendbare BVT-Schlussfolgerung
     */
    RBATC,

    /**
     * BVT-assoziierter Emissionswert
     */
    RBATE,

    /**
     * Bearbeitungszustand Bericht, ähnlich R0002
     */
    RBEA,

    /**
     * Betriebsstatus der Betriebsstätte
     */
    RBETZ,

    /**
     * Ausgangszustandsbericht
     * <p>
     * Wert aus {liegt vor, liegt nicht vor, nicht erforderlich}
     */
    RCONF,

    /**
     * Ausnahmen nach Art. 31 bis 35 IE-RL
     */
    RDERO,

    /**
     * Maßeinheiten, anwendbar z.B. für Leistung.EINHEIT
     */
    REINH,

    /**
     * Koordinatenreferenzsystem
     * <p>
     * EPSG, anwendbare Codes
     */
    REPSG,

    /**
     * Typisierte Funktionen von Ansprechpartnern
     */
    RFKN,

    /**
     * Flusseinzugsgebiete
     */
    RFLEZ,

    /**
     * Gewässer
     */
    RGMD,

    /**
     * IE-Tätigkeiten
     */
    RIET,

    /**
     * Land
     */
    RLAND,

    /**
     * Wirtschaftsszweig
     */
    RNACE,

    /**
     * PRTR-Tätigkeiten
     */
    RPRTR,

    /**
     * Typ der Feuerungsanlage
     * <p>
     * aus {LCP, WI, co-WI}
     */
    RQUEA,

    /**
     * relevante Kapitel aus III,IV,V,VI der 2010-75-EU
     */
    RRELCH,

    /**
     * Berichtstypen
     * <p>
     * {EURegBst, EURegAnl, EURegF, PRTR, GFA, EEBst, EEAnl, ...}
     **/
    RRTYP,

    /**
     * Art der Änderungen
     * <p>
     * Wert aus Artikel 51({1, 2, 3}) oder Kombinationen
     */
    RSPECC,

    /**
     * zweistelliger Code aus ISO 3166-1, ISO-2
     */
    RSTAAT,

    /**
     * Tätigkeitstypen
     * <p>
     * {IED, PRTR, 4BV, ...}
     */
    RVORS,

    /**
     * Zuständigkeit Behörde
     * <p>
     * {Genehmigung, Überwachung, Genehmigung und Überwachung, ...}
     */
    RZUST,

    /**
     * 4.BImSchV-Nummern
     */
    R4BV,

    /**
     * Berichtsjahr
     */
    RJAHR,

    /**
     * Art der Kommunikationsverbindung
     */
    RKOM;

    private static final Set<Referenzliste> LAENDER_SPEZIFISCH = EnumSet.of(REINH, RFKN, RGMD, REPSG);

    public boolean isLaenderspezifisch() {
        return LAENDER_SPEZIFISCH.contains(this);
    }

}
