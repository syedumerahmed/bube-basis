package de.wps.bube.basis.stammdaten.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;

public interface BetreiberRepository extends JpaRepository<Betreiber, Long>, QuerydslPredicateExecutor<Betreiber> {

    Optional<Betreiber> findByBerichtsjahrAndLandAndBetreiberNummer(Referenz jahr, Land land, String nummer);

    @Modifying
    @Query("""
            update Betreiber b
                     set b.letzteAenderungBetreiber  = current_timestamp,
                         b.betreiberdatenUebernommen = false
                     where b.id = :quelleId""")
    void aenderungDurchBetreiber(Long quelleId);
}
