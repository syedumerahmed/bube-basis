package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.time.Instant;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class BetriebsstaetteListItemView {
    public Long id;
    public Behoerde zustaendigeBehoerde;
    public String name;
    public String localId;
    public Land land;
    public String betriebsstaetteNr;
    public Referenz betriebsstatus;
    public String ort;
    public String plz;
    public String strasse;
    public String hausNr;
    public String akz;
    public Referenz gemeindekennziffer;
    public boolean pruefungsfehler;
    public boolean pruefungVorhanden;
    public Instant letzteAenderungBetreiber;
    public boolean betreiberdatenUebernommen;
}
