package de.wps.bube.basis.euregistry.web.mapper;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.XeurTeilberichtListItemDto;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;

@Mapper(uses = ReferenzMapper.class)
public interface XeurTeilberichtListItemMapper {

    XeurTeilberichtListItemDto toDto(XeurTeilbericht xeurTeilbericht);
}
