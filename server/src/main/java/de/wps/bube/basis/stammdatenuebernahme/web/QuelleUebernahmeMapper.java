package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.QuelleUebernahmeDto;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.QuelleUebernahme;

@Mapper
public abstract class QuelleUebernahmeMapper {
    public abstract QuelleUebernahme fromDto(QuelleUebernahmeDto quelleUebernahmeDto);
}
