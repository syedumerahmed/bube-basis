package de.wps.bube.basis.stammdaten.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import de.wps.bube.basis.stammdaten.domain.entity.Quelle;

public interface QuelleRepository extends JpaRepository<Quelle, Long>, QuerydslPredicateExecutor<Quelle> {

    Optional<Quelle> findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(Long betriebsstaetteId, String quellenNummer);

    Optional<Quelle> findByParentAnlageIdAndQuelleNrIgnoreCase(Long anlageId, String quellenNummer);

    boolean existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(Long parentBetriebsstaetteId, String quelleNr);

    boolean existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCaseAndIdNot(Long parentBetriebsstaetteId, String quelleNr,
            Long exceptedQuelleId);

    boolean existsByParentAnlageIdAndQuelleNrIgnoreCase(Long parentAnlageId, String quelleNr);

    boolean existsByParentAnlageIdAndQuelleNrIgnoreCaseAndIdNot(Long parentAnlageId, String quelleNr,
            Long exceptedQuelleId);

    void deleteAllByIdIn(List<Long> ids);

    List<Quelle> findAllByParentBetriebsstaetteId(Long betriebsstaetteId);

    List<Quelle> findAllByParentAnlageId(Long anlageId);

    @Modifying
    @Query("""
            update Quelle q
                     set q.letzteAenderungBetreiber  = current_timestamp,
                         q.betreiberdatenUebernommen = false
                     where q.id = :quelleId""")
    void aenderungDurchBetreiber(Long quelleId);
}
