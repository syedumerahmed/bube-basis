package de.wps.bube.basis.referenzdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

@Mapper
public abstract class ReferenzBeanMapper {

    public abstract ReferenzBean toBean(Referenz referenz);

    int map(Gueltigkeitsjahr gueltigkeitsjahr) {
        return gueltigkeitsjahr.getJahr();
    }

}
