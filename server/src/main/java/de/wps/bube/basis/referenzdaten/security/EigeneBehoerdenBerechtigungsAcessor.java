package de.wps.bube.basis.referenzdaten.security;

import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Predicate;

@Component
public class EigeneBehoerdenBerechtigungsAcessor extends BehoerdeBerechtigungsAccessor {

    @Override
    public Predicate behoerdenIn(Set<String> behoerden) {
        return behoerden.isEmpty() ?
                null :
                behoerde.schluessel.in(behoerden);
    }
}
