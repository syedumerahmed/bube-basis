package de.wps.bube.basis.euregistry.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATE;

import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class AuflageBean {

    private final ReferenzBean emissionswert;
    private final boolean nachArt18;
    private final boolean nachArt14;

    public AuflageBean(ReferenzBean emissionswert, boolean nachArt18, boolean nachArt14) {
        this.emissionswert = emissionswert;
        this.nachArt18 = nachArt18;
        this.nachArt14 = nachArt14;
    }

    @RegelApi.Member(description = "BVT-assoziierter Emissionswert", referenzliste = RBATE)
    public ReferenzBean getEmissionswert() {
        return emissionswert;
    }

    @RegelApi.Member(description = "Gibt es strengere Auflagen nach Art. 18 IE-RL?")
    public boolean isNachArt18() {
        return nachArt18;
    }

    @RegelApi.Member(description = "Gibt es strengere Auflagen nach Art. 14 (4) IE-RL?")
    public boolean isNachArt14() {
        return nachArt14;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuflageBean that)) {
            return false;
        }
        return nachArt18 == that.nachArt18 && nachArt14 == that.nachArt14 && Objects.equals(emissionswert,
                that.emissionswert);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emissionswert, nachArt18, nachArt14);
    }

    @Override
    public String toString() {
        return "AuflageBean{" +
                "emissionswert=" + emissionswert +
                ", nachArt18=" + nachArt18 +
                ", nachArt14=" + nachArt14 +
                '}';
    }

}
