package de.wps.bube.basis.euregistry.domain;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.base.vo.Land;

public final class XeurTeilberichtErstellenCommand {
    public final List<Long> berichtsIds;
    public final Referenz berichtsjahr;
    public final Land land;

    public XeurTeilberichtErstellenCommand(List<Long> berichtsIds,
            Referenz berichtsjahr,
            Land land) {
        this.berichtsIds = berichtsIds;
        this.berichtsjahr = berichtsjahr;
        this.land = land;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        var that = (XeurTeilberichtErstellenCommand) obj;
        return Objects.equals(this.berichtsIds, that.berichtsIds) &&
                Objects.equals(this.berichtsjahr, that.berichtsjahr) &&
                Objects.equals(this.land, that.land);
    }

    @Override
    public int hashCode() {
        return Objects.hash(berichtsIds, berichtsjahr, land);
    }

    @Override
    public String toString() {
        return "XeurTeilberichtErstellenCommand[" +
                "berichtsIds=" + berichtsIds + ", " +
                "berichtsjahr=" + berichtsjahr + ", " +
                "land=" + land + ']';
    }

}
