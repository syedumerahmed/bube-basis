package de.wps.bube.basis.euregistry.domain.entity;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class BVTAusnahme {

    // Referenz auf RBATE
    @NotNull
    @ManyToOne
    private Referenz emissionswertAusnahme;

    @NotNull
    private LocalDate beginntAm;
    private LocalDate endetAm;

    @NotNull
    private String oeffentlicheBegruendungUrl;

    @Deprecated
    protected BVTAusnahme() {
    }

    public BVTAusnahme(Referenz emissionswertAusnahme, LocalDate beginntAm, String oeffentlicheBegruendungUrl) {
        this.emissionswertAusnahme = emissionswertAusnahme;
        this.beginntAm = beginntAm;
        this.oeffentlicheBegruendungUrl = oeffentlicheBegruendungUrl;
    }

    public Referenz getEmissionswertAusnahme() {
        return emissionswertAusnahme;
    }

    public void setEmissionswertAusnahme(Referenz emissionswertAusnahme) {
        this.emissionswertAusnahme = emissionswertAusnahme;
    }

    public LocalDate getBeginntAm() {
        return beginntAm;
    }

    public void setBeginntAm(LocalDate beginntAm) {
        this.beginntAm = beginntAm;
    }

    public LocalDate getEndetAm() {
        return endetAm;
    }

    public void setEndetAm(LocalDate endetAm) {
        this.endetAm = endetAm;
    }

    public String getOeffentlicheBegruendungUrl() {
        return oeffentlicheBegruendungUrl;
    }

    public void setOeffentlicheBegruendungUrl(String oeffentlicheBegruendungUrl) {
        this.oeffentlicheBegruendungUrl = oeffentlicheBegruendungUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BVTAusnahme that = (BVTAusnahme) o;
        return emissionswertAusnahme.equals(that.emissionswertAusnahme) && beginntAm.equals(that.beginntAm) && Objects
                .equals(endetAm, that.endetAm) && oeffentlicheBegruendungUrl.equals(that.oeffentlicheBegruendungUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emissionswertAusnahme, beginntAm, endetAm, oeffentlicheBegruendungUrl);
    }
}


