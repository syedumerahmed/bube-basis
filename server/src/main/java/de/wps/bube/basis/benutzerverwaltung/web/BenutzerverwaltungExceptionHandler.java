package de.wps.bube.basis.benutzerverwaltung.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerException;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerImportException;
import de.wps.bube.basis.benutzerverwaltung.domain.KeycloakException;

@ControllerAdvice
public class BenutzerverwaltungExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BenutzerException.class)
    protected ResponseEntity<Object> handleBenutzerException(BenutzerException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(KeycloakException.class)
    protected ResponseEntity<Object> handleKeycloakException(KeycloakException ex) {
        return new ResponseEntity<>(ex.getMessage(), ex.getStatus());
    }

    @ExceptionHandler(BenutzerImportException.class)
    protected ResponseEntity<Object> handleBenutzerImportException(BenutzerImportException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
