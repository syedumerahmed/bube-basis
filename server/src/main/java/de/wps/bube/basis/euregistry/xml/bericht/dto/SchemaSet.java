package de.wps.bube.basis.euregistry.xml.bericht.dto;

public class SchemaSet {

    public static final String SCHEMA_NET = "http://inspire.ec.europa.eu/schemas/net/4.0";
    public static final String SCHEMA_EUREG = "http://dd.eionet.europa.eu/schemaset/euregistryonindustrialsites";
    public static final String SCHEMA_EUREG_XSD = "http://dd.eionet.europa.eu/schemas/euregistryonindustrialsites/EUReg.xsd";
    public static final String SCHEMA_PF = "http://inspire.ec.europa.eu/schemas/pf/4.0";
    public static final String SCHEMA_GML = "http://www.opengis.net/gml/3.2";
    public static final String SCHEMA_BASE = "http://inspire.ec.europa.eu/schemas/base/3.3";
    public static final String SCHEMA_XLINK = "http://www.w3.org/1999/xlink";
    public static final String SCHEMA_SC = "http://www.interactive-instruments.de/ShapeChange/AppInfo";
    public static final String SCHEMA_XS = "http://www.w3.org/2001/XMLSchema";
    public static final String SCHEMA_GCO = "http://www.isotc211.org/2005/gco";
    public static final String SCHEMA_CP = "http://inspire.ec.europa.eu/schemas/cp/4.0";
    public static final String SCHEMA_HFP = "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty";
    public static final String SCHEMA_BU_CORE2D = "http://inspire.ec.europa.eu/schemas/bu-core2d/4.0";
    public static final String SCHEMA_AD = "http://inspire.ec.europa.eu/schemas/ad/4.0";
    public static final String SCHEMA_AU = "http://inspire.ec.europa.eu/schemas/au/4.0";
    public static final String SCHEMA_BASE2 = "http://inspire.ec.europa.eu/schemas/base2/2.0";
    public static final String SCHEMA_ACT_CORE = "http://inspire.ec.europa.eu/schemas/act-core/4.0";
    public static final String SCHEMA_TN = "http://inspire.ec.europa.eu/schemas/tn/4.0";
    public static final String SCHEMA_GN = "http://inspire.ec.europa.eu/schemas/gn/4.0";
    public static final String SCHEMA_GMD = "http://www.isotc211.org/2005/gmd";
    public static final String SCHEMA_BU_BASE = "http://inspire.ec.europa.eu/schemas/bu-base/4.0";
    public static final String SCHEMA_GSR = "http://www.isotc211.org/2005/gsr";
    public static final String SCHEMA_GTS = "http://www.isotc211.org/2005/gts";
    public static final String SCHEMA_GSS = "http://www.isotc211.org/2005/gss";
    public static final String SCHEMA_XSI = "http://www.w3.org/2001/XMLSchema-instance";

    public static final String COUNTRY_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/CountryCodeValue/";
    public static final String REASON_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/ReasonValue/";
    public static final String FACILITY_TYPE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/FacilityTypeValue/";
    public static final String EPRTR_ACTIVITY_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/EPRTRAnnexIActivityValue/";
    public static final String IED_ACTIVITY_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/IEDAnnexIActivityValue/";
    public static final String NACE_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/NACEValue/";
    public static final String CONDITION_OF_FACILITY_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/ConditionOfFacilityValue/";
    public static final String ARTICLE51_VALUE_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/Article51Value/";
    public static final String DEROGATIONS_VALUE_CODE = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/DerogationValue/";
    public static final String PLANT_TYPE_VALUE_CODE = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/PlantTypeValue/";
    public static final String BATAEL_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/BATAELValue/";
    public static final String BASELINE_REPORT_INDICATOR_CODES = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/BaselineReportValue/";
    public static final String OTHER_RELEVANT_CHAPTERS_CODE = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/RelevantChapterValue/";
    public static final String BAT_CONCLUSION_CODE = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/BATConclusionValue/";
    public static final String INSTALLATION_TYPE_CODE = "http://dd.eionet.europa.eu/vocabulary/euregistryonindustrialsites/InstallationTypeValue/";

    private SchemaSet() {}
}
