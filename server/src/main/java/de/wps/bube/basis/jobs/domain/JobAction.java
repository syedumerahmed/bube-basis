package de.wps.bube.basis.jobs.domain;

import java.util.HashMap;
import java.util.Map;

public class JobAction {

    private final String label;
    private final String route;
    private final Map<String, Object> payload = new HashMap<>();

    public JobAction(String label, String route) {
        this.label = label;
        this.route = route;
    }

    public String getLabel() {
        return label;
    }

    public String getRoute() {
        return route;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void addPayloadData(String key, Object data) {
        payload.put(key, data);
    }

}
