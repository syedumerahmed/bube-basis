package de.wps.bube.basis.referenzdaten.xml;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.function.Consumer;

import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.AdresseType;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.KommunikationsverbindungType;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.ZustaendigkeitEnum;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapper;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverContext;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

@Mapper(uses = { CommonXMLMapper.class, ReferenzResolverService.class })
public abstract class BehoerdeXMLMapper implements XDtoMapper<BehoerdeType, Behoerde> {

    @Autowired
    private ReferenzenService referenzenService;

    @Override
    @Mapping(target = "kommunikationsverbindungen.kommunikationsverbindung", source = "kommunikationsverbindungen")
    @Mapping(target = "zustaendigkeiten.zustaendigkeit", source = "zustaendigkeiten")
    public abstract BehoerdeType toXDto(Behoerde behoerde);

    @Override
    public EntityWithError<Behoerde> fromXDto(BehoerdeType xDto) {
        var errors = new ArrayList<MappingError>();
        var b = fromXDto(xDto, new BehoerdeXMLMapper.MappingContext(errors::add));
        return new EntityWithError<>(b, errors);
    }

    @Mapping(target = "kommunikationsverbindungen", source = "kommunikationsverbindungen.kommunikationsverbindung")
    @Mapping(target = "zustaendigkeiten", source = "zustaendigkeiten.zustaendigkeit")
    public abstract Behoerde fromXDto(BehoerdeType BehoerdeType, @Context BehoerdeXMLMapper.MappingContext context);

    static String schluessel(Referenz referenz) {
        return referenz != null ? referenz.getSchluessel() : null;
    }

    @Mapping(target = "typ", source = "rkom", qualifiedByName = "RKOM")
    @Mapping(target = "verbindung", source = "value")
    abstract Kommunikationsverbindung KTfromXDto(KommunikationsverbindungType kommunikationsverbindungType,
                                               @Context BehoerdeXMLMapper.MappingContext context);

    @Mapping(target = "value", source = "verbindung")
    @Mapping(target = "rkom", source = "typ")
    abstract KommunikationsverbindungType KTtoXDto(Kommunikationsverbindung kommunikationsverbindung);

    @Mapping(target = "postfachPlz", source = "plzPostfach")
    @Mapping(target = "landIsoCode", source = "rstaat", qualifiedByName = "RSTAAT")
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    abstract Adresse AdressefromXDto(AdresseType adresseType, @Context BehoerdeXMLMapper.MappingContext context);

    @Mapping(target = "plzPostfach", source = "postfachPlz")
    @Mapping(target = "rstaat", source = "landIsoCode")
    abstract AdresseType AdressetoXDto(Adresse adresse);

    public static Gueltigkeitsjahr map(int jahr) {
        return Gueltigkeitsjahr.of(jahr);
    }

    public static int map(Gueltigkeitsjahr gueltigkeitsjahr) {
        return gueltigkeitsjahr.getJahr();
    }

    public static Land map(String land) {
        return Land.of(land);
    }

    public static String map(Land land) {
        return land != null ? land.getNr() : null;
    }

    class MappingContext implements ReferenzResolverContext {

        public Land land;
        public Gueltigkeitsjahr jahr;
        private String behoerdeBezeichnung, behoerdeSchluessel;
        private final Consumer<MappingError> errorConsumer;

        MappingContext(Consumer<MappingError> errorConsumer) {
            this.errorConsumer = errorConsumer;
        }

        @BeforeMapping
        void beforeFromDto(BehoerdeType xdto) {
            this.land = Land.of(xdto.getLand());
            var jahrSchluessel = referenzenService.getAktuellesBerichtsjahr().getSchluessel();
            this.jahr = Gueltigkeitsjahr.of(jahrSchluessel);

            this.behoerdeBezeichnung = xdto.getBezeichnung();
            this.behoerdeSchluessel = xdto.getSchluessel();
        }

        @Override
        public Land getLand() {
            return land;
        }

        @Override
        public Gueltigkeitsjahr getJahr() {
            return jahr;
        }

        @Override
        public void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Behörde %s Schlüssel %s", behoerdeBezeichnung, behoerdeSchluessel),
                    format("Referenz %s mit Schlüssel %s wurde nicht gefunden", referenzliste.name(), schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Behörde %s Nr. %s", behoerdeBezeichnung, behoerdeSchluessel),
                    format("Referenz %s mit Schlüssel %s ist ungültig", referenzliste.name(), schluessel), true);
            errorConsumer.accept(error);
        }

    }

    @ValueMapping(source = "BIMSCHV11", target = "BIMSCHV_11")
    @ValueMapping(source = "BIMSCHV42", target = "BIMSCHV_42")
    abstract ZustaendigkeitEnum mapZustaendigkeit(Zustaendigkeit zustaendigkeit);


    @ValueMapping(source = "BIMSCHV_11", target = "BIMSCHV11")
    @ValueMapping(source = "BIMSCHV_42", target = "BIMSCHV42")
    abstract Zustaendigkeit mapZustaendigkeitBack(ZustaendigkeitEnum zustaendigkeitEnum);
}
