package de.wps.bube.basis.stammdaten.domain.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Mapper
public interface BetriebsstaettenUpdater {

    @Mapping(target = "quellen", ignore = true)
    @Mapping(target = "anlagen", ignore = true)
    @Mapping(target = "berichtsobjekte", ignore = true)
    @Mapping(target = "prtrVorschriften", ignore = true)
    @Mapping(target = "prtrTaetigkeitenOhneHaupttaetigkeit", ignore = true)
    @Mapping(target = "komplexpruefung", ignore = true)
    void update(@MappingTarget Betriebsstaette targetBetriebsstaette, Betriebsstaette sourceBetriebsstaette);

}
