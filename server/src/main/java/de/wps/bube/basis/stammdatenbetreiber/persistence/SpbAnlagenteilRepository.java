package de.wps.bube.basis.stammdatenbetreiber.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;

public interface SpbAnlagenteilRepository
        extends JpaRepository<SpbAnlagenteil, Long> {

    Optional<SpbAnlagenteil> findOneByAnlagenteilId(Long anlagenteilId);

    Optional<SpbAnlagenteil> findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(Long parentAnlageId,
            String anlagenteilNr);

    Optional<SpbAnlagenteil> findOneByParentSpbAnlageIdAndAnlagenteilNrIgnoreCase(Long parentSpbAnlageId,
            String anlagenteilNr);

    List<SpbAnlagenteil> findByParentAnlageId(Long parentAnlageId);

    List<SpbAnlagenteil> findByParentAnlageIdAndAnlagenteilIdIsNull(Long parentAnlageId);

    List<SpbAnlagenteil> findByParentSpbAnlageId(Long parentSpbAnlageId);

    boolean existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(Long parentAnlageId, String anlagenteilNr);

    boolean existsByParentAnlageIdAndAnlagenteilNrIgnoreCaseAndIdNot(Long parentAnlageId, String anlagenteilNr,
            Long exceptedSpbAnlagenteilId);

    void deleteAllByParentSpbAnlageId(Long id);

    void deleteAllByParentAnlageId(Long anlageId);
}
