package de.wps.bube.basis.koordinaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;

import java.util.Set;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Coordinate;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

@Service
public class KoordinatenTransformationService {

    public static final String EPSG_4647 = "4647";
    public static final String EPSG_25832 = "25832";
    public static final String EPSG_25833 = "25833";

    private static final Set<String> ALLOWED_SOURCE_EPSG = Set.of(EPSG_4647, EPSG_25832, EPSG_25833);

    @Secured(BEHOERDENBENUTZER)
    public double[] transformToEPSG4258(GeoPunkt geoPunkt) throws KoordinatenException {

        String epsgSource = geoPunkt.getEpsgCode().getSchluessel();
        Coordinate coord = new Coordinate(geoPunkt.getOst(), geoPunkt.getNord());

        if (!ALLOWED_SOURCE_EPSG.contains(epsgSource)) {
            throw new KoordinatenException(
                    "Fehler bei der Koordinatentransformation. " +
                    "EPSG-Code '%s' ist nicht 4647, 25832, oder 25833".formatted(epsgSource));
        }

        try {
            CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:" + epsgSource);
            CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:4258");

            MathTransform mTrans = CRS.findMathTransform(sourceCRS, targetCRS);
            JTS.transform(coord, coord, mTrans);
            return new double[] { round(coord.x, 5), round(coord.y, 5) };
        } catch (FactoryException | TransformException e) {
            throw new KoordinatenException("Fehler bei der Koordinatentransformation zu EPSG:4258", e);
        }
    }

    private double round(double value, int decimalPoints) {
        double d = Math.pow(10, decimalPoints);
        return Math.round(value * d) / d;
    }
}
