package de.wps.bube.basis.sicherheit.domain.security;

public interface ReferenzBerechtigungsInMemoryAccess extends BerechtigungsInMemoryAccessor {
    boolean isBundesreferenz();
}
