package de.wps.bube.basis.komplexpruefung.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

public interface RegelRepository extends JpaRepository<Regel, Long>, QuerydslPredicateExecutor<Regel> {

    Optional<Regel> findByNummer(Long nummer);

    @Query(nativeQuery = true, value = """
            SELECT r.*
              FROM regel r
              LEFT OUTER JOIN regel_typ_aenderungen a
                ON r.nummer = a.regel_nummer
               AND (a.land = :land OR a.land IS NULL)
             WHERE (r.gruppe IN :gruppen)
               AND (:jahr BETWEEN r.gueltig_von AND r.gueltig_bis)
               AND ((r.land = :land AND r.typ <> 'IGNORIERT')
                   OR (r.land = '00' AND a.land IS NULL AND r.typ <> 'IGNORIERT')
                   OR (r.land = '00' AND a.land = :land AND a.typ <> 'IGNORIERT'))""")
    Stream<Regel> streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeitNative(String land, List<String> gruppen,
            int jahr);

    default Stream<Regel> streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(Land land, List<RegelGruppe> gruppen,
            Gueltigkeitsjahr jahr) {
        return streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeitNative(
                land.getNr(), gruppen.stream().map(RegelGruppe::name).toList(), jahr.getJahr());
    }

    Stream<Regel> findByObjekteContaining(RegelObjekt regelObjekt);

}
