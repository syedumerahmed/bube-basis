package de.wps.bube.basis.euregistry.xml.bericht.dto.actcore;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Function {

    @XmlElement(required = true, name = "Function", namespace = SchemaSet.SCHEMA_ACT_CORE)
    private final Data data = new Data();

    public ReferenceType getActivity() {
        return data.activity;
    }

    public void setActivity(ReferenceType activity) {
        this.data.activity = activity;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class Data {
        @XmlElement(required = true, name = "activity", namespace = SchemaSet.SCHEMA_ACT_CORE)
        ReferenceType activity; // --laut EU-Schema eine Liste, aber laut UBA-Vorgabe ein Wert
    }
}
