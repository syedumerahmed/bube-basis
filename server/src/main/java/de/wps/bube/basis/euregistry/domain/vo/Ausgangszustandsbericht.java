package de.wps.bube.basis.euregistry.domain.vo;

import java.util.stream.Stream;

public enum Ausgangszustandsbericht {

    LIEGT_VOR("liegt vor"), LIEGT_NICHT_VOR("liegt nicht vor"), NICHT_ERFORDERLICH("nicht erforderlich");

    private final String bezeichner;

    Ausgangszustandsbericht(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static Ausgangszustandsbericht of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(
                             () -> new IllegalArgumentException("Ungültiger Ausgangszustandsbericht: " + bezeichner));
    }
}
