package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.ThematicId;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.Status;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProductionInstallation {

    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    private String gmlId;

    @XmlElement(required = true, name = "inspireId", namespace = SchemaSet.SCHEMA_PF)
    private InspireID inspireID;

    @XmlElement(required = false, name = "thematicId", namespace = SchemaSet.SCHEMA_PF)
    private ThematicId thematicID;

    @XmlElement(required = true, name = "pointGeometry", namespace = SchemaSet.SCHEMA_PF)
    private final PointLocation geometry = new PointLocation();

    @XmlElement(required = true, name = "status", namespace = SchemaSet.SCHEMA_PF)
    private Status status;

    @XmlElement(required = true, name = "type", namespace = SchemaSet.SCHEMA_PF, nillable = true)
    private String type;

    @XmlTransient
    private ProductionInstallationPart productionInstallationPart;

    @XmlElement(name = "groupedInstallationPart", namespace = SchemaSet.SCHEMA_PF)
    private GroupedInstallationPart groupedInstallationPart;

    @XmlElement(required = true, name = "installationName", namespace = SchemaSet.SCHEMA_EUREG)
    private FeatureNameContainer installationName;

    /**
     * wurde ein Bericht nach Art.22 2010-75-EU erstellt?
     */
    @XmlElement(name = "baselineReportIndicator", namespace = SchemaSet.SCHEMA_EUREG)
    private ReferenceType baselineReportIndicator;

    /**
     * Ausnahmen nach Art.15 IV 2010-75-EU
     */
    @XmlElement(name = "BATDerogation", namespace = SchemaSet.SCHEMA_EUREG)
    private List<BATDerogation> batDerogation;

    /**
     * Genehmigungsbehörden
     */
    @XmlElement(name = "competentAuthorityPermits", namespace = SchemaSet.SCHEMA_EUREG)
    private List<CompetentAuthorityEPRTR> competentAuthorityPermits;

    /**
     * Überwachungsbehörden
     */
    @XmlElement(name = "competentAuthorityInspections", namespace = SchemaSet.SCHEMA_EUREG)
    private List<CompetentAuthorityEPRTR> competentAuthorityInspections;

    /**
     * Vor-Ort-Besichtigungen nach Art. 23(6) 2010-75-EU
     */
    @XmlElement(required = false, name = "siteVisits", namespace = SchemaSet.SCHEMA_EUREG)
    private SiteVisits siteVisits;

    /**
     * andere relevante Kapitel aus II,IV,V,VI der 2010-75-EU anwendbar auf die
     * Anlage
     */
    @XmlElement(name = "otherRelevantChapters", namespace = SchemaSet.SCHEMA_EUREG)
    private List<ReferenceType> otherRelevantChapters;

    /**
     * Genehmigung
     */
    @XmlElement(required = true, name = "permit", namespace = SchemaSet.SCHEMA_EUREG)
    private List<PermitDetails> permitDetails;

    /**
     * IED-Tätigkeit
     */
    @XmlElement(required = true, name = "IEDAnnexIActivity", namespace = SchemaSet.SCHEMA_EUREG)
    private IEDAnnexIActivity iedAnnexIActivity;

    /**
     * Identifikator für Störanfallanlagen nach Seveso Plant Information Retrieval
     * System (SPIRS), Richtlinie 2012/18/EU
     */
    @XmlElement(name = "eSPIRSIdentifier", namespace = SchemaSet.SCHEMA_EUREG)
    private List<String> eSPIRSIdentifier;

    /**
     * Identifikator Emissionshandel, wie auf EU-Ebene geführt; Richtlinie
     * 2003/87/EG
     */
    @XmlElement(name = "ETSIdentifier", namespace = SchemaSet.SCHEMA_EUREG)
    private List<String> etsIdentifier;

    @XmlElement(name = "remarks", namespace = SchemaSet.SCHEMA_EUREG)
    private String remarks;

    /**
     * strengere Genehmigungsauflagen nach 2018/1135/EU, Art.21/3
     */
    @XmlElement(name = "stricterPermitConditions", namespace = SchemaSet.SCHEMA_EUREG)
    private List<StricterPermitConditionsType> stricterPermitConditions;

    /**
     * öffentliches Emissions-Monitoring nach Art. 24(3)(b) 2010-75-EU (Freitext)
     */
    @XmlElement(name = "publicEmissionMonitoring", namespace = SchemaSet.SCHEMA_EUREG)
    private String publicEmissionMonitoring;

    /**
     * URL zum Monitoring
     */
    @XmlElement(name = "publicEmissionMonitoringURL", namespace = SchemaSet.SCHEMA_EUREG)
    private String publicEmissionMonitoringURL;

    /**
     * anwendbare Durchführungsbeschlüsse aus Referenzliste BATConclusionValue
     */
    @XmlElement(name = "BATConclusion", namespace = SchemaSet.SCHEMA_EUREG)
    private List<ReferenceType> batConclusion;

    @XmlElement(required = true, name = "installationType", namespace = SchemaSet.SCHEMA_EUREG)
    private ReferenceType installationType;

    @XmlElement(required = true, name = "dateOfStartOfOperation", nillable = true)
    private Nillable dateOfStartOfOperation;

    public InspireID getInspireID() {
        return inspireID;
    }

    public void setInspireID(InspireID inspireID) {
        this.inspireID = inspireID;
    }

    public ThematicId getThematicID() {
        return thematicID;
    }

    public void setThematicID(ThematicId thematicID) {
        this.thematicID = thematicID;
    }

    public Point getGeometry() {
        return geometry.getPoint();
    }

    public void setGeometry(Point point) {
        geometry.setPoint(point);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GroupedInstallationPart getGroupedInstallationPart() {
        return groupedInstallationPart;
    }

    public void setGroupedInstallationPart(GroupedInstallationPart groupedInstallationPart) {
        this.groupedInstallationPart = groupedInstallationPart;
    }

    public FeatureNameContainer getInstallationName() {
        return installationName;
    }

    public void setInstallationName(FeatureNameContainer installationName) {
        this.installationName = installationName;
    }

    public ReferenceType getBaselineReportIndicator() {
        return baselineReportIndicator;
    }

    public void setBaselineReportIndicator(
            ReferenceType baselineReportIndicator) {
        this.baselineReportIndicator = baselineReportIndicator;
    }

    public List<BATDerogation> getBatDerogation() {
        return batDerogation;
    }

    public void setBatDerogation(List<BATDerogation> batDerogation) {
        this.batDerogation = batDerogation;
    }

    public List<CompetentAuthorityEPRTR> getCompetentAuthorityPermits() {
        return competentAuthorityPermits;
    }

    public void setCompetentAuthorityPermits(List<CompetentAuthorityEPRTR> competentAuthorityPermits) {
        this.competentAuthorityPermits = competentAuthorityPermits;
    }

    public List<CompetentAuthorityEPRTR> getCompetentAuthorityInspections() {
        return competentAuthorityInspections;
    }

    public void setCompetentAuthorityInspections(List<CompetentAuthorityEPRTR> competentAuthorityInspections) {
        this.competentAuthorityInspections = competentAuthorityInspections;
    }

    public SiteVisits getSiteVisits() {
        return siteVisits;
    }

    public void setSiteVisits(SiteVisits siteVisits) {
        this.siteVisits = siteVisits;
    }

    public List<String> getOtherRelevantChapters() {
        if (otherRelevantChapters == null || otherRelevantChapters.isEmpty()) {
            return new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        for (ReferenceType type : otherRelevantChapters) {
            list.add(type.getCode());
        }
        return list;
    }

    public void setOtherRelevantChapters(List<String> otherRelevantChaptersCodes) {
        otherRelevantChapters = new ArrayList<>();
        for (String code : otherRelevantChaptersCodes) {
            otherRelevantChapters.add(new ReferenceType(SchemaSet.OTHER_RELEVANT_CHAPTERS_CODE, code));
        }
    }

    public List<PermitDetails> getPermitDetails() {
        return permitDetails;
    }

    public void setPermitDetails(List<PermitDetails> permitDetails) {
        this.permitDetails = permitDetails;
    }

    public IEDAnnexIActivity getIEDAnnexIActivity() {
        return iedAnnexIActivity;
    }

    public void setIEDAnnexIActivity(IEDAnnexIActivity iEDAnnexIActivity) {
        iedAnnexIActivity = iEDAnnexIActivity;
    }

    public List<String> geteSPIRSIdentifier() {
        return eSPIRSIdentifier;
    }

    public void seteSPIRSIdentifier(List<String> eSPIRSIdentifier) {
        this.eSPIRSIdentifier = eSPIRSIdentifier;
    }

    public List<String> getETSIdentifier() {
        return etsIdentifier;
    }

    public void setETSIdentifier(List<String> eTSIdentifier) {
        etsIdentifier = eTSIdentifier;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<StricterPermitConditionsType> getStricterPermitConditions() {
        return stricterPermitConditions;
    }

    public void setStricterPermitConditions(List<StricterPermitConditionsType> stricterPermitConditions) {
        this.stricterPermitConditions = stricterPermitConditions;
    }

    public String getPublicEmissionMonitoring() {
        return publicEmissionMonitoring;
    }

    public void setPublicEmissionMonitoring(String publicEmissionMonitoring) {
        this.publicEmissionMonitoring = publicEmissionMonitoring;
    }

    public String getPublicEmissionMonitoringURL() {
        return publicEmissionMonitoringURL;
    }

    public void setPublicEmissionMonitoringURL(String publicEmissionMonitoringURL) {
        this.publicEmissionMonitoringURL = publicEmissionMonitoringURL;
    }

    public List<String> getBATConclusion() {
        if (batConclusion == null || batConclusion.isEmpty()) {
            return new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        for (ReferenceType type : batConclusion) {
            list.add(type.getCode());
        }
        return list;
    }

    public void setBATConclusion(List<String> bATConclusionCodes) {
        if (bATConclusionCodes == null) {
            return;
        }
        batConclusion = new ArrayList<>();
        for (String code : bATConclusionCodes) {
            batConclusion.add(new ReferenceType(SchemaSet.BAT_CONCLUSION_CODE, code));
        }
    }

    public void addBATConclusion(String derogationsCode) {
        if (batConclusion == null) {
            batConclusion = new ArrayList<>();
        }

        batConclusion.add(new ReferenceType(SchemaSet.BAT_CONCLUSION_CODE, derogationsCode));
    }

    public ReferenceType getInstallationType() {
        return installationType;
    }

    public void setInstallationType(ReferenceType installationType) {
        this.installationType = installationType;
    }

    public String getDateOfStartOfOperation() {
        return Nillable.getValueOfNillable(dateOfStartOfOperation);
    }

    public void setDateOfStartOfOperation(String dateOfStartOfOperationValue) {
        dateOfStartOfOperation = Nillable.getUpdatedNillable(dateOfStartOfOperation, dateOfStartOfOperationValue);
    }

    public String getGmlId() {
        return gmlId;
    }

    public void setGmlId(String gmlId) {
        this.gmlId = gmlId;
    }

    public ProductionInstallationPart getProductionInstallationPart() {
        return productionInstallationPart;
    }

    public void setProductionInstallationPart(ProductionInstallationPart productionInstallationPart) {
        this.productionInstallationPart = productionInstallationPart;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class GroupedInstallationPart {

        @XmlAttribute(name = AttributeNames.HREF, namespace = SchemaSet.SCHEMA_XLINK)
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

}
