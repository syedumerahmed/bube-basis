package de.wps.bube.basis.benutzerverwaltung.domain;

import org.keycloak.representations.idm.ErrorRepresentation;
import org.springframework.http.HttpStatus;

public class KeycloakException extends RuntimeException {
    private final HttpStatus status;
    private final ErrorRepresentation error;

    public KeycloakException(String message, int status, ErrorRepresentation error) {
        super(String.format("%s (Keycloak-Nachricht = %s)", message, error.getErrorMessage()));
        this.status = HttpStatus.valueOf(status);
        this.error = error;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
