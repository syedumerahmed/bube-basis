package de.wps.bube.basis.euregistry.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;

public class EuRegBstBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {
    private final EURegBetriebsstaette euRegBst;

    public EuRegBstBerechtigungsAdapter(EURegBetriebsstaette euRegBst) {
        this.euRegBst = euRegBst;
    }

    @Override
    public Land getLand() {
        return euRegBst.getBetriebsstaette().getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return Set.of(euRegBst.getBerichtsdaten().getZustaendigeBehoerde().getSchluessel());
    }

    @Override
    public Set<String> getAkzSet() {
        if (euRegBst.getBerichtsdaten().getAkz() != null) {
            return Set.of(euRegBst.getBerichtsdaten().getAkz());
        }
        return null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return Set.of(euRegBst.getBetriebsstaette().getGemeindekennziffer().getSchluessel());
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return Set.of(euRegBst.getBetriebsstaette().getBetriebsstaetteNr());
    }
}
