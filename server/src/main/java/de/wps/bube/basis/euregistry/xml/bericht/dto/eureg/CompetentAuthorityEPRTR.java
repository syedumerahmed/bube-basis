package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CompetentAuthorityEPRTR {
    @XmlElement(required = true, name = "CompetentAuthority")
    private final CompetentAuthority competentAuthority = new CompetentAuthority();

    public String getOrganisationName() {
        return competentAuthority.organisationName;
    }

    public void setOrganisationName(String orgasationName) {
        this.competentAuthority.organisationName = orgasationName;
    }

    public String getIndividualName() {
        return competentAuthority.individualName;
    }

    public void setIndividualName(String individualName) {
        this.competentAuthority.individualName = individualName;
    }

    public String getElectronicMailAddress() {
        return competentAuthority.electronicMailAddress;
    }

    public void setElectronicMailAddress(String electronicMailAddress) {
        this.competentAuthority.electronicMailAddress = electronicMailAddress;
    }

    public Address getAddress() {
        return competentAuthority.address;
    }

    public void setAddress(Address address) {
        this.competentAuthority.address = address;
    }

    public String getTelephoneNo() {
        return competentAuthority.telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.competentAuthority.telephoneNo = telephoneNo;
    }

    public String getFaxNo() {
        return competentAuthority.faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.competentAuthority.faxNo = faxNo;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class CompetentAuthority {
        @XmlElement(required = true, name = "organisationName")
        String organisationName;
        @XmlElement(required = true, name = "individualName")
        String individualName;
        @XmlElement(required = true, name = "electronicMailAddress")
        String electronicMailAddress;
        @XmlElement(required = true, name = "address")
        Address address;
        @XmlElement(required = true, name = "telephoneNo")
        String telephoneNo;
        @XmlElement(name = "faxNo")
        String faxNo;
    }
}
