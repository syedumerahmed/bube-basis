package de.wps.bube.basis.stammdaten.domain.vo;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;

import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum BetriebsstaetteColumn implements Column {

    BETRIEBSSTAETTEN_NUMMER(betriebsstaette.betriebsstaetteNr),
    NAME(betriebsstaette.name),
    STRASSE_HAUSNUMMER(emptyIfNull(betriebsstaette.adresse.strasse)
            .concat(" ")
            .concat(emptyIfNull(betriebsstaette.adresse.hausNr))),
    PLZ(betriebsstaette.adresse.plz),
    ORT(betriebsstaette.adresse.ort),
    ZUSTAENDIGE_BEHOERDE(betriebsstaette.zustaendigeBehoerde.bezeichnung),
    AKZ(betriebsstaette.akz),
    BETRIEBSSTATUS(betriebsstaette.betriebsstatus.ktext),
    NACE(betriebsstaette.naceCode.ktext),
    GEMEINDE(betriebsstaette.gemeindekennziffer.ktext),
    UEBERNOMMEN(betriebsstaette.betreiberdatenUebernommen),
    PRUEFUNGSFEHLER(betriebsstaette.pruefungsfehler),
    AENDERUNG_BETREIBER(betriebsstaette.letzteAenderungBetreiber);

    private static StringExpression emptyIfNull(StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final ComparableExpression<?> expression;

    BetriebsstaetteColumn(ComparableExpression<?> expression) {
        this.expression = expression;
    }

    @Override
    public ComparableExpression<?> getExpression() {
        return expression;
    }
}
