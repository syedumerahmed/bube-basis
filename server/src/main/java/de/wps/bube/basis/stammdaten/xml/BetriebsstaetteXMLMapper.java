package de.wps.bube.basis.stammdaten.xml;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.EnumUtils.isValidEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverContext;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.xml.dto.AdresseType;
import de.wps.bube.basis.stammdaten.xml.dto.AnlageType;
import de.wps.bube.basis.stammdaten.xml.dto.AnlagenteilType;
import de.wps.bube.basis.stammdaten.xml.dto.AnsprechpartnerType;
import de.wps.bube.basis.stammdaten.xml.dto.BetreiberType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.GeoPunktType;
import de.wps.bube.basis.stammdaten.xml.dto.KommunikationsverbindungType;
import de.wps.bube.basis.stammdaten.xml.dto.LeistungType;
import de.wps.bube.basis.stammdaten.xml.dto.QuelleType;
import de.wps.bube.basis.stammdaten.xml.dto.VorschriftType;
import de.wps.bube.basis.stammdaten.xml.dto.ZustaendigeBehoerdeType;

@Mapper(uses = { CommonXMLMapper.class, ReferenzResolverService.class })
public abstract class BetriebsstaetteXMLMapper implements XDtoMapper<BetriebsstaetteType, Betriebsstaette> {

    @Autowired
    private ReferenzResolverService referenzResolverService;

    @Override
    @Mapping(target = "nr", source = "betriebsstaetteNr")
    @Mapping(target = "ansprechpartnerListe.ansprechpartner", source = "ansprechpartner")
    @Mapping(target = "kommunikationsverbindungen.kommunikationsverbindung", source = "kommunikationsverbindungen")
    @Mapping(target = "quellen.quelle", source = "quellen")
    @Mapping(target = "anlagen.anlage", source = "anlagen")
    @Mapping(target = "zustaendigeBehoerden.zustaendigeBehoerde", source = "zustaendigeBehoerden")
    @Mapping(target = "abfallerzeugerNummern.abfallerzeugerNr", source = "abfallerzeugerNummern")
    @Mapping(target = "einleiterNummern.einleiterNr", source = "einleiterNummern")
    @Mapping(target = "rnace", source = "naceCode")
    @Mapping(target = "rland", source = "land")
    @Mapping(target = "rjahr", source = "berichtsjahr")
    @Mapping(target = "rgmd", source = "gemeindekennziffer")
    @Mapping(target = "rflez", source = "flusseinzugsgebiet")
    @Mapping(target = "rbetz", source = "betriebsstatus")
    @Mapping(target = "zustaendigeBehoerde", source = "zustaendigeBehoerde.schluessel")
    @Mapping(target = "vorschriften.schluessel", source = "vorschriften")
    public abstract BetriebsstaetteType toXDto(Betriebsstaette betriebsstaette);

    @Override
    public EntityWithError<Betriebsstaette> fromXDto(BetriebsstaetteType type) {
        var errors = new ArrayList<MappingError>();
        var bst = fromXDto(type, new MappingContext(errors::add));
        return new EntityWithError<>(bst, errors);
    }

    @Mapping(target = "betriebsstaetteNr", source = "nr")
    @Mapping(target = "ansprechpartner", source = "ansprechpartnerListe.ansprechpartner")
    @Mapping(target = "kommunikationsverbindungen", source = "kommunikationsverbindungen.kommunikationsverbindung")
    @Mapping(target = "quellen", source = "quellen.quelle")
    @Mapping(target = "anlagen", source = "anlagen.anlage")
    @Mapping(target = "zustaendigeBehoerden", source = "zustaendigeBehoerden.zustaendigeBehoerde")
    @Mapping(target = "abfallerzeugerNummern", source = "abfallerzeugerNummern.abfallerzeugerNr")
    @Mapping(target = "einleiterNummern", source = "einleiterNummern.einleiterNr")
    @Mapping(target = "naceCode", source = "rnace", qualifiedByName = "RNACE")
    @Mapping(target = "land", source = "rland")
    @Mapping(target = "berichtsjahr", source = "rjahr", qualifiedByName = "RJAHR")
    @Mapping(target = "gemeindekennziffer", source = "rgmd", qualifiedByName = "RGMD")
    @Mapping(target = "flusseinzugsgebiet", source = "rflez", qualifiedByName = "RFLEZ")
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    @Mapping(target = "betriebsstatus", source = "rbetz", qualifiedByName = "RBETZ")
    @Mapping(target = "vorschriften", source = "vorschriften.schluessel", qualifiedByName = "RVORS")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "pruefungsfehler", constant = "false")
    @Mapping(target = "pruefungVorhanden", constant = "false")
    @Mapping(target = "letzteAenderungBetreiber", ignore = true)
    @Mapping(target = "komplexpruefung", ignore = true)
    @Mapping(target = "schreibsperreBetreiber", ignore = true)
    @Mapping(target = "berichtsobjekte", ignore = true)
    @Mapping(target = "prtrVorschriften", ignore = true)
    @Mapping(target = "prtrTaetigkeitenOhneHaupttaetigkeit", ignore = true)
    abstract Betriebsstaette fromXDto(BetriebsstaetteType bsType, @Context MappingContext context);

    static String toXDto(Referenz referenz) {
        return referenz != null ? referenz.getSchluessel() : null;
    }

    @Mapping(target = "typ", source = "rkom", qualifiedByName = "RKOM")
    @Mapping(target = "verbindung", source = "value")
    abstract Kommunikationsverbindung fromXDto(KommunikationsverbindungType kommunikationsverbindungType,
            @Context MappingContext context);

    @Mapping(target = "value", source = "verbindung")
    @Mapping(target = "rkom", source = "typ")
    abstract KommunikationsverbindungType toXDto(Kommunikationsverbindung kommunikationsverbindung);

    @Mapping(target = "kommunikationsverbindungen", source = "kommunikationsverbindung")
    @Mapping(target = "funktion", source = "rfkn", qualifiedByName = "RFKN")
    @Mapping(target = "id", ignore = true)
    abstract Ansprechpartner fromXDto(AnsprechpartnerType ansprechpartnerType, @Context MappingContext context);

    @Mapping(target = "kommunikationsverbindung", source = "kommunikationsverbindungen")
    @Mapping(target = "rfkn", source = "funktion")
    abstract AnsprechpartnerType toXDto(Ansprechpartner ansprechpartner);

    @Mapping(target = "postfachPlz", source = "plzPostfach")
    @Mapping(target = "landIsoCode", source = "rstaat", qualifiedByName = "RSTAAT")
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    abstract Adresse fromXDto(AdresseType adresseType, @Context MappingContext context);

    @Mapping(target = "plzPostfach", source = "postfachPlz")
    @Mapping(target = "rstaat", source = "landIsoCode")
    abstract AdresseType toXDto(Adresse adresse);

    @Mapping(target = "betreiberNummer", source = "nr")
    @Mapping(target = "referenzAnlagenkataster", source = "refAnlagenkataster")
    @Mapping(target = "land", ignore = true)
    @Mapping(target = "berichtsjahr", ignore = true)
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "letzteAenderungBetreiber", ignore = true)
    abstract Betreiber fromXDto(BetreiberType betreiberType, @Context MappingContext context);

    @Mapping(target = "nr", source = "betreiberNummer")
    @Mapping(target = "refAnlagenkataster", source = "referenzAnlagenkataster")
    abstract BetreiberType toXDto(Betreiber betreiber);

    @Mapping(target = "quelleNr", source = "nr")
    @Mapping(target = "quellenArt", source = "rquea", qualifiedByName = "RQUEA")
    @Mapping(target = "bauhoehe", source = "hoehe")
    @Mapping(target = "referenzAnlagenkataster", source = "refAnlagenkataster")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "parentBetriebsstaetteId", ignore = true)
    @Mapping(target = "parentAnlageId", ignore = true)
    @Mapping(target = "letzteAenderungBetreiber", ignore = true)
    abstract Quelle fromXDto(QuelleType quelleType, @Context MappingContext context);

    @Mapping(target = "nr", source = "quelleNr")
    @Mapping(target = "rquea", source = "quellenArt")
    @Mapping(target = "hoehe", source = "bauhoehe")
    @Mapping(target = "refAnlagenkataster", source = "referenzAnlagenkataster")
    abstract QuelleType toXDto(Quelle quelle);

    @Mapping(target = "quellen", source = "quellen.quelle")
    @Mapping(target = "referenzAnlagenkataster", source = "refAnlagenkataster")
    @Mapping(target = "anlageNr", source = "nr")
    @Mapping(target = "leistungen", source = "leistung")
    @Mapping(target = "anlagenteile", source = "anlagenteile.anlagenteil")
    @Mapping(target = "zustaendigeBehoerden", source = "zustaendigeBehoerden.zustaendigeBehoerde")
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    @Mapping(target = "betriebsstatus", source = "rbetz", qualifiedByName = "RBETZ")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "parentBetriebsstaetteId", ignore = true)
    @Mapping(target = "vorschriften", source = "vorschriften.vorschrift")
    @Mapping(target = "letzteAenderungBetreiber", ignore = true)
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    abstract Anlage fromXDto(AnlageType anlageType, @Context MappingContext context);

    @Mapping(target = "quellen.quelle", source = "quellen")
    @Mapping(target = "refAnlagenkataster", source = "referenzAnlagenkataster")
    @Mapping(target = "nr", source = "anlageNr")
    @Mapping(target = "leistung", source = "leistungen")
    @Mapping(target = "anlagenteile.anlagenteil", source = "anlagenteile")
    @Mapping(target = "zustaendigeBehoerden.zustaendigeBehoerde", source = "zustaendigeBehoerden")
    @Mapping(target = "rbetz", source = "betriebsstatus")
    @Mapping(target = "vorschriften.vorschrift", source = "vorschriften")
    abstract AnlageType toXDto(Anlage anlage);

    @Mapping(target = "anlagenteilNr", source = "nr")
    @Mapping(target = "referenzAnlagenkataster", source = "refAnlagenkataster")
    @Mapping(target = "leistungen", source = "leistung")
    @Mapping(target = "zustaendigeBehoerden", source = "zustaendigeBehoerden.zustaendigeBehoerde")
    @Mapping(target = "vertraulichkeitsgrund", qualifiedByName = "RCONF")
    @Mapping(target = "betriebsstatus", source = "rbetz", qualifiedByName = "RBETZ")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "parentAnlageId", ignore = true)
    @Mapping(target = "vorschriften", source = "vorschriften.vorschrift")
    @Mapping(target = "letzteAenderungBetreiber", ignore = true)
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    abstract Anlagenteil fromXDto(AnlagenteilType anlagenteilType, @Context MappingContext context);

    @Mapping(target = "nr", source = "anlagenteilNr")
    @Mapping(target = "refAnlagenkataster", source = "referenzAnlagenkataster")
    @Mapping(target = "leistung", source = "leistungen")
    @Mapping(target = "zustaendigeBehoerden.zustaendigeBehoerde", source = "zustaendigeBehoerden")
    @Mapping(target = "rbetz", source = "betriebsstatus")
    @Mapping(target = "vorschriften.vorschrift", source = "vorschriften")
    abstract AnlagenteilType toXDto(Anlagenteil anlagenteil);

    static EinleiterNr stringToEinleiterNr(String einleiterNr) {
        return new EinleiterNr(einleiterNr);
    }

    static String einleiterNrToString(EinleiterNr einleiterNr) {
        return einleiterNr != null ? einleiterNr.getEinleiterNr() : null;
    }

    static AbfallerzeugerNr stringToAbfallerzeugerNr(String abfallerzeugerNr) {
        return new AbfallerzeugerNr(abfallerzeugerNr);
    }

    static String abfallerzeugerNrToString(AbfallerzeugerNr abfallerzeugerNr) {
        return abfallerzeugerNr != null ? abfallerzeugerNr.getAbfallerzeugerNr() : null;
    }

    static String landToDto(Land land) {
        return land.getNr();
    }

    public static Land landFromDto(String landNr) {
        return Land.of(landNr);
    }

    @Mapping(target = "leistungseinheit", source = "reinh", qualifiedByName = "REINH")
    @Mapping(target = "leistungsklasse", source = "klasse")
    abstract Leistung fromXDto(LeistungType leistungType, @Context MappingContext context);

    @Mapping(target = "reinh", source = "leistungseinheit")
    @Mapping(target = "klasse", source = "leistungsklasse")
    abstract LeistungType toXDto(Leistung leistung);

    @Mapping(target = "zustaendigkeit", source = "rzust", qualifiedByName = "RZUST")
    abstract ZustaendigeBehoerde fromXDto(ZustaendigeBehoerdeType zustaendigeBehoerdeType,
            @Context MappingContext context);

    @Mapping(target = "rzust", source = "zustaendigkeit")
    @Mapping(target = "behoerde", source = "behoerde.schluessel")
    abstract ZustaendigeBehoerdeType toXDto(ZustaendigeBehoerde zustaendigeBehoerde);

    @Mapping(target = "epsgCode", source = "repsg", qualifiedByName = "REPSG")
    abstract GeoPunkt fromXDto(GeoPunktType geoPunkt, @Context MappingContext context);

    @Mapping(target = "repsg", source = "epsgCode")
    abstract GeoPunktType toXDto(GeoPunkt geoPunkt);

    @Mapping(target = "taetigkeiten.schluessel", source = "taetigkeiten")
    abstract VorschriftType toXDto(Vorschrift vorschrift);

    Vorschrift fromXDto(VorschriftType vorschriftType, @Context MappingContext context) {
        if (vorschriftType == null) {
            return null;
        }
        var vorschriftArt = referenzResolverService.vorschriftFromDto(vorschriftType.getArt(), context);
        if (vorschriftArt == null) {
            return null;
        }
        Referenz haupttaetigkeit = null;
        List<Referenz> taetigkeiten = null;

        if (isValidEnum(Referenzliste.class, vorschriftArt.getStrg())) {
            var liste = Referenzliste.valueOf(vorschriftArt.getStrg());
            haupttaetigkeit = referenzResolverService.loadReferenz(liste, vorschriftType.getHaupttaetigkeit(), context);
            if (vorschriftType.getTaetigkeiten() != null && vorschriftType.getTaetigkeiten().getSchluessel() != null) {
                taetigkeiten = vorschriftType
                        .getTaetigkeiten().getSchluessel().stream()
                        .map(schluessel -> referenzResolverService.loadReferenz(liste, schluessel, context))
                        .toList();
            }
        }
        try {
            return new Vorschrift(vorschriftArt, taetigkeiten, haupttaetigkeit);
        } catch (IllegalArgumentException e){
            context.addDefaultError(e.getMessage());
            return null;
        }
    }

    static class MappingContext implements ReferenzResolverContext {
        public Land land;
        public Gueltigkeitsjahr jahr;
        private String bstName, bstNummer;
        private final Consumer<MappingError> errorConsumer;

        MappingContext(Consumer<MappingError> errorConsumer) {
            this.errorConsumer = errorConsumer;
        }

        @BeforeMapping
        void beforeFromDto(BetriebsstaetteType bsType) {
            land = Land.of(bsType.getRland());
            jahr = Gueltigkeitsjahr.of(bsType.getRjahr());
            bstName = bsType.getName();
            bstNummer = bsType.getNr();
        }

        @AfterMapping
        void afterFromDto(@MappingTarget Betriebsstaette betriebsstaette) {
            var betreiber = betriebsstaette.getBetreiber();
            if (betreiber != null) {
                betreiber.setLand(land);
                betreiber.setBerichtsjahr(betriebsstaette.getBerichtsjahr());
            }
        }

        @Override
        public Land getLand() {
            return land;
        }

        @Override
        public Gueltigkeitsjahr getJahr() {
            return jahr;
        }

        @Override
        public void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s wurde nicht gefunden", referenzliste.name(), schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s ist ungültig", referenzliste.name(), schluessel), true);
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeNotFound(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s wurde nicht gefunden", schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeUngueltig(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s ist ungültig", schluessel), true);
            errorConsumer.accept(error);
        }

        public void addDefaultError(String message) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer), message);
            errorConsumer.accept(error);
        }

    }
}
