package de.wps.bube.basis.stammdaten.domain.vo.suche;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public interface Filter {
    Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot);
}
