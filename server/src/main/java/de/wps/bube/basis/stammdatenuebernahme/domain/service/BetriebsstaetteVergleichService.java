package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.AnsprechpartnerVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.BooleanVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.KommunikationsverbindungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetriebsstaetteVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class BetriebsstaetteVergleichService {
    private final CommonVergleichService vergleichService;

    public BetriebsstaetteVergleichService(
            CommonVergleichService vergleichService) {
        this.vergleichService = vergleichService;
    }

    public SpbBetriebsstaetteVergleich vergleiche(Betriebsstaette spl, SpbBetriebsstaette spb) {
        SpbBetriebsstaetteVergleich vergleich = new SpbBetriebsstaetteVergleich();

        vergleich.setId(spl == null ? null : spl.getId());
        vergleich.setSpbId(spb == null ? null : spb.getId());

        vergleich.setName(new StringVector(spl == null ? null : spl.getName(), spb == null ? null : spb.getName()));
        if (spl != null && !spl.isBetreiberdatenUebernommen() && spb != null) {
            vergleich.setGeoPunkt(
                    new GeoPunktVector(spl.getGeoPunkt(), spb.getGeoPunkt()));

            vergleich.setBetriebsstatus(new ReferenzVector(spl.getBetriebsstatus(),
                    spb.getBetriebsstatus()));

            vergleich.setBetriebsstatusSeit(new LocalDateVector(spl.getBetriebsstatusSeit(),
                    spb.getBetriebsstatusSeit()));
            vergleich.setInbetriebnahme(new LocalDateVector(spl.getInbetriebnahme(),
                    spb.getInbetriebnahme()));

            vergleich.setAdresse(vergleichService.adressVergleich(spl.getAdresse(),
                    spb.getAdresse()));
            vergleich.setAnsprechpartner(
                    vergleicheAnsprechpartner(spl.getAnsprechpartner(),
                            spb.getAnsprechpartner()));
            vergleich.setKommunikationsverbindung(vergleicheKommunikationsverbindungen(
                    spl.getKommunikationsverbindungen(),
                    spb.getKommunikationsverbindungen()));

            vergleich.setEinleiterNummern(vergleichService.vergleicheStringLists(
                    spl.getEinleiterNummern().stream().map(
                            EinleiterNr::getEinleiterNr).toList(),
                    spb.getEinleiterNummern().stream().map(EinleiterNr::getEinleiterNr).toList()));
            vergleich.setAbfallerzeugerNummern(vergleichService.vergleicheStringLists(
                    spl.getAbfallerzeugerNummern().stream().map(
                            AbfallerzeugerNr::getAbfallerzeugerNr).toList(),
                    spb.getAbfallerzeugerNummern().stream().map(AbfallerzeugerNr::getAbfallerzeugerNr).toList()));

            vergleich.setFlusseinzuggebiet(new ReferenzVector(spl.getFlusseinzugsgebiet(),
                    spb.getFlusseinzugsgebiet()));
            vergleich.setNaceCode(
                    new ReferenzVector(spl.getNaceCode(), spb.getNaceCode()));

            vergleich.setDirekteinleiter(new BooleanVector(spl.isDirekteinleiter(),
                    spb.isDirekteinleiter()));
            vergleich.setIndirekteinleiter(new BooleanVector(spl.isIndirekteinleiter(),
                    spb.isIndirekteinleiter()));
        }

        if (spb == null) {
            vergleich.setHasChanges(false);
        } else {

            vergleich.setHasChanges(spl == null || !spl.isBetreiberdatenUebernommen() && (!vergleich.getName().isEqual()
                    || !vergleich.getBetriebsstatus().isEqual()
                    || !vergleich.getBetriebsstatusSeit().isEqual()
                    || !vergleich.getInbetriebnahme().isEqual()
                    || !vergleich.getDirekteinleiter().isEqual()
                    || !vergleich.getIndirekteinleiter().isEqual()
                    || !vergleich.getFlusseinzuggebiet().isEqual()
                    || !vergleich.getGeoPunkt().isEqual()
                    || !vergleich.getNaceCode().isEqual()
                    || !vergleich.getAbfallerzeugerNummern().stream().allMatch(StringVector::isEqual)
                    || !vergleich.getAnsprechpartner().stream().allMatch(AnsprechpartnerVector::isEqual)
                    || !vergleich.getKommunikationsverbindung()
                                 .stream()
                                 .allMatch(KommunikationsverbindungVector::isEqual)
                    || !vergleichService.adresseEqual(vergleich.getAdresse()))
            );
        }

        return vergleich;
    }

    private List<KommunikationsverbindungVector> vergleicheKommunikationsverbindungen(
            List<Kommunikationsverbindung> spl, List<Kommunikationsverbindung> spb) {
        List<KommunikationsverbindungVector> maps = new ArrayList<>();
        List<Kommunikationsverbindung> doubled = new ArrayList<>();

        spl.forEach(kommunikationsverbindung -> {
            Optional<Kommunikationsverbindung> first = spb.stream()
                                                          .filter(k -> k.equals(kommunikationsverbindung))
                                                          .findFirst();
            first.ifPresent(doubled::add);

            KommunikationsverbindungVector map = new KommunikationsverbindungVector(kommunikationsverbindung,
                    first.orElse(null));
            maps.add(map);
        });

        spb.stream().filter(k -> !doubled.contains(k)).forEach(kommunikationsverbindung -> {
                    KommunikationsverbindungVector map = new KommunikationsverbindungVector(null, kommunikationsverbindung);

                    maps.add(map);
                }
        );
        return maps;
    }

    private List<AnsprechpartnerVector> vergleicheAnsprechpartner(List<Ansprechpartner> spl,
            List<Ansprechpartner> spb) {
        List<AnsprechpartnerVector> maps = new ArrayList<>();
        List<Ansprechpartner> doubled = new ArrayList<>();

        spl.forEach(ansprechpartner -> {
            Optional<Ansprechpartner> first = spb.stream().filter(a -> a.equalValues(ansprechpartner)).findFirst();
            first.ifPresent(doubled::add);

            maps.add(new AnsprechpartnerVector(ansprechpartner, first.orElse(null)));
        });

        spb.stream()
           .filter(a -> !doubled.contains(a))
           .forEach(ansprechpartner -> maps.add(new AnsprechpartnerVector(null, ansprechpartner))
           );
        return maps;
    }
}
