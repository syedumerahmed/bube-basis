package de.wps.bube.basis.euregistry.domain.entity;

import static java.util.Collections.emptyList;
import static javax.persistence.EnumType.STRING;

import java.util.List;
import java.util.Objects;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.euregistry.domain.vo.FeuerungsanlageTyp;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class FeuerungsanlageBerichtsdaten {

    @Enumerated(STRING)
    private FeuerungsanlageTyp feuerungsanlageTyp;

    @NotNull
    private Double gesamtKapazitaetAbfall = 0.0;
    @NotNull
    private Double kapazitaetGefAbfall = 0.0;
    @NotNull
    private Double kapazitaetNichtGefAbfall = 0.0;

    @NotNull
    private Boolean mehrWaermeVonGefAbfall = false;
    @NotNull
    private Boolean mitverbrennungVonUnbehandeltemAbfall = false;

    private String oeffentlicheBekanntmachung;
    private String oeffentlicheBekanntmachungUrl;

    @ElementCollection
    private List<SpezielleBedingungIE51> spezielleBedingungen;

    // IE-Ausnahmen nach Art. 31 bis 35 IE-RL (Derogation RDERO)
    @ManyToMany
    private List<Referenz> ieAusnahmen;

    @Deprecated
    protected FeuerungsanlageBerichtsdaten() {
    }

    public FeuerungsanlageBerichtsdaten(Double gesamtKapazitaetAbfall, Double kapazitaetGefAbfall,
            Double kapazitaetNichtGefAbfall) {
        this.gesamtKapazitaetAbfall = gesamtKapazitaetAbfall;
        this.kapazitaetGefAbfall = kapazitaetGefAbfall;
        this.kapazitaetNichtGefAbfall = kapazitaetNichtGefAbfall;
    }

    public FeuerungsanlageTyp getFeuerungsanlageTyp() {
        return feuerungsanlageTyp;
    }

    public void setFeuerungsanlageTyp(FeuerungsanlageTyp feuerungsanlageTyp) {
        this.feuerungsanlageTyp = feuerungsanlageTyp;
    }

    public Double getGesamtKapazitaetAbfall() {
        return gesamtKapazitaetAbfall;
    }

    public void setGesamtKapazitaetAbfall(Double gesamtKapazitaetAbfall) {
        this.gesamtKapazitaetAbfall = gesamtKapazitaetAbfall;
    }

    public Double getKapazitaetGefAbfall() {
        return kapazitaetGefAbfall;
    }

    public void setKapazitaetGefAbfall(Double kapazitaetGefAbfall) {
        this.kapazitaetGefAbfall = kapazitaetGefAbfall;
    }

    public Double getKapazitaetNichtGefAbfall() {
        return kapazitaetNichtGefAbfall;
    }

    public void setKapazitaetNichtGefAbfall(Double kapazitaetNichtGefAbfall) {
        this.kapazitaetNichtGefAbfall = kapazitaetNichtGefAbfall;
    }

    public Boolean isMehrWaermeVonGefAbfall() {
        return mehrWaermeVonGefAbfall;
    }

    public void setMehrWaermeVonGefAbfall(Boolean mehrWaermeVonGefAbfall) {
        this.mehrWaermeVonGefAbfall = mehrWaermeVonGefAbfall;
    }

    public Boolean isMitverbrennungVonUnbehandeltemAbfall() {
        return mitverbrennungVonUnbehandeltemAbfall;
    }

    public void setMitverbrennungVonUnbehandeltemAbfall(Boolean mitverbrennungVonUnbehandeltemAbfall) {
        this.mitverbrennungVonUnbehandeltemAbfall = mitverbrennungVonUnbehandeltemAbfall;
    }

    public String getOeffentlicheBekanntmachung() {
        return oeffentlicheBekanntmachung;
    }

    public void setOeffentlicheBekanntmachung(String oeffentlicheBekanntmachung) {
        this.oeffentlicheBekanntmachung = oeffentlicheBekanntmachung;
    }

    public String getOeffentlicheBekanntmachungUrl() {
        return oeffentlicheBekanntmachungUrl;
    }

    public void setOeffentlicheBekanntmachungUrl(String oeffentlicheBekanntmachungUrl) {
        this.oeffentlicheBekanntmachungUrl = oeffentlicheBekanntmachungUrl;
    }

    public List<Referenz> getIeAusnahmen() {
        return ieAusnahmen;
    }

    public void setIeAusnahmen(List<Referenz> ieAusnahmen) {
        this.ieAusnahmen = ieAusnahmen;
    }

    public List<SpezielleBedingungIE51> getSpezielleBedingungen() {
        return spezielleBedingungen;
    }

    public void setSpezielleBedingungen(List<SpezielleBedingungIE51> spezielleBedingungen) {
        this.spezielleBedingungen = spezielleBedingungen;
    }

    public void clear17BVData() {
        spezielleBedingungen = emptyList();
        gesamtKapazitaetAbfall = 0D;
        kapazitaetGefAbfall = 0D;
        kapazitaetNichtGefAbfall = 0D;
        mehrWaermeVonGefAbfall = false;
        mitverbrennungVonUnbehandeltemAbfall = false;
        oeffentlicheBekanntmachung = null;
        oeffentlicheBekanntmachungUrl = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeuerungsanlageBerichtsdaten that = (FeuerungsanlageBerichtsdaten) o;
        return mehrWaermeVonGefAbfall == that.mehrWaermeVonGefAbfall && mitverbrennungVonUnbehandeltemAbfall == that.mitverbrennungVonUnbehandeltemAbfall && feuerungsanlageTyp == that.feuerungsanlageTyp && Objects
                .equals(gesamtKapazitaetAbfall, that.gesamtKapazitaetAbfall) && Objects.equals(kapazitaetGefAbfall,
                that.kapazitaetGefAbfall) && Objects.equals(kapazitaetNichtGefAbfall,
                that.kapazitaetNichtGefAbfall) && Objects.equals(oeffentlicheBekanntmachung,
                that.oeffentlicheBekanntmachung) && Objects.equals(oeffentlicheBekanntmachungUrl,
                that.oeffentlicheBekanntmachungUrl) && Objects.equals(ieAusnahmen, that.ieAusnahmen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feuerungsanlageTyp, gesamtKapazitaetAbfall, kapazitaetGefAbfall, kapazitaetNichtGefAbfall,
                mehrWaermeVonGefAbfall, mitverbrennungVonUnbehandeltemAbfall, oeffentlicheBekanntmachung,
                oeffentlicheBekanntmachungUrl, ieAusnahmen);
    }
}
