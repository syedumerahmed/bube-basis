package de.wps.bube.basis.euregistry.rules.bean;

import java.time.LocalDate;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class GenehmigungBean {

    private final boolean erteilt;
    private final boolean neuBetrachtet;
    private final boolean geaendert;
    private final LocalDate erteilungsdatum;
    private final LocalDate neuBetrachtetDatum;
    private final LocalDate aenderungsdatum;
    private final String url;
    private final String durchsetzungsmassnahmen;

    public GenehmigungBean(boolean erteilt, boolean neuBetrachtet, boolean geaendert, LocalDate erteilungsdatum,
            LocalDate neuBetrachtetDatum, LocalDate aenderungsdatum, String url, String durchsetzungsmassnahmen) {
        this.erteilt = erteilt;
        this.neuBetrachtet = neuBetrachtet;
        this.geaendert = geaendert;
        this.erteilungsdatum = erteilungsdatum;
        this.neuBetrachtetDatum = neuBetrachtetDatum;
        this.aenderungsdatum = aenderungsdatum;
        this.url = url;
        this.durchsetzungsmassnahmen = durchsetzungsmassnahmen;
    }

    @RegelApi.Member(description = "Wurde die Genehmigung erteilt nach Art. 5 IE-RL?")
    public boolean isErteilt() {
        return erteilt;
    }

    @RegelApi.Member(description = "Wurde die Genehmigung im Berichtszeitraum neu betrachtet?")
    public boolean isNeuBetrachtet() {
        return neuBetrachtet;
    }

    @RegelApi.Member(description = "Wurde die Genehmigung im Berichtszeitraum wesentlich geändert?")
    public boolean isGeaendert() {
        return geaendert;
    }

    @RegelApi.Member(description = "Erteilungsdatum")
    public LocalDate getErteilungsdatum() {
        return erteilungsdatum;
    }

    @RegelApi.Member(description = "Betrachtungsdatum")
    public LocalDate getNeuBetrachtetDatum() {
        return neuBetrachtetDatum;
    }

    @RegelApi.Member(description = "Änderungsdatum")
    public LocalDate getAenderungsdatum() {
        return aenderungsdatum;
    }

    @RegelApi.Member(description = "URL, unter der die Genehmigung veröffentlicht wurde")
    public String getUrl() {
        return url;
    }

    @RegelApi.Member(description = "Beschreibung der Durchsetzungsmaßnahmen, wenn keine Genehmigung nach Art. 5 IE-RL erteilt wurde")
    public String getDurchsetzungsmassnahmen() {
        return durchsetzungsmassnahmen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        GenehmigungBean that = (GenehmigungBean) o;
        return erteilt == that.erteilt && neuBetrachtet == that.neuBetrachtet && geaendert == that.geaendert && Objects.equals(
                erteilungsdatum, that.erteilungsdatum) && Objects.equals(neuBetrachtetDatum,
                that.neuBetrachtetDatum) && Objects.equals(aenderungsdatum,
                that.aenderungsdatum) && Objects.equals(url, that.url) && Objects.equals(
                durchsetzungsmassnahmen, that.durchsetzungsmassnahmen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(erteilt, neuBetrachtet, geaendert, erteilungsdatum, neuBetrachtetDatum, aenderungsdatum,
                url, durchsetzungsmassnahmen);
    }

    @Override
    public String toString() {
        return "GenehmigungBean{" +
                "erteilt=" + erteilt +
                ", neuBetrachtet=" + neuBetrachtet +
                ", geaendert=" + geaendert +
                ", erteilungsdatum=" + erteilungsdatum +
                ", neuBetrachtetDatum=" + neuBetrachtetDatum +
                ", aenderungsdatum=" + aenderungsdatum +
                ", url='" + url + '\'' +
                ", durchsetzungsmassnahmen='" + durchsetzungsmassnahmen + '\'' +
                '}';
    }

}