package de.wps.bube.basis.referenzdaten.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Mapper
public interface ReferenzMapper {

    ReferenzDto toDto(Referenz referenz);

    Referenz fromDto(ReferenzDto referenzDto);

    static Gueltigkeitsjahr gueltigkeitsjahrFromDto(int jahr) {
        return Gueltigkeitsjahr.of(jahr);
    }

    static int gueltigkeitsjahrToDto(Gueltigkeitsjahr jahr) {
        return jahr.getJahr();
    }

    static Land landFromDto(String landNr) {
        return Land.of(landNr);
    }

    static String landToDto(Land land) {
        return land.getNr();
    }

}
