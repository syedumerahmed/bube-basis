package de.wps.bube.basis.jobs.enums;

public enum JobStatusEnum {
    STAMMDATEN_IMPORTIEREN("Stammdaten importieren"),

    REFERENZDATEN_IMPORTIEREN("Referenzdaten importieren"),

    BENUTZER_IMPORTIEREN("Benutzer importieren"),

    BEHOERDEN_IMPORTIEREN("Behörden importieren"),

    KOMPLEXPRUEFUNG("Komplexprüfung"),

    XEUR_TEILBERICHT_ERSTELLEN("XEUR Teilbericht erstellen"),

    EUREGISRTY_IMPORTIEREN("EuRegistry importieren"),

    BERICHTSDATEN_UEBERNEHMEN("Berichtsdaten in Zieljahr übernehmen"),

    ANSPRECHPARTNER_LOESCHEN("Alle Ansprechpersonen Löschen"),

    GEMEINDEKOORDINATEN_IMPORTIEREN("Gemeindekoordinaten importieren"),

    STAMMDATEN_UEBERNEHEMEN("Stammdaten in Zieljahr übernehmen");

    private String value;

    JobStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static JobStatusEnum fromValue(String value) {
        for (JobStatusEnum b : JobStatusEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
