package de.wps.bube.basis.stammdaten.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

public interface AnlagenteilRepository
        extends JpaRepository<Anlagenteil, Long>, QuerydslPredicateExecutor<Anlagenteil> {

    boolean existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(Long parentAnlageId, String anlagenteilNr);

    boolean existsByParentAnlageIdAndAnlagenteilNrIgnoreCaseAndIdNot(Long parentAnlageId, String anlagenteilNr,
            Long exceptedAnlagenteilId);

    Optional<Anlagenteil> findByParentAnlageIdAndAnlagenteilNrIgnoreCase(Long parentAnlageId, String anlagenteilNr);

    Optional<Anlagenteil> findByParentAnlageIdAndLocalId(Long parentAnlageId, String localId);

    Optional<Anlagenteil> findByLocalId(String localId);

    void deleteAllByIdIn(List<Long> ids);

    List<Anlagenteil> findByParentAnlageId(Long anlageId);

    @Modifying
    @Query("""
            update Anlagenteil a
                     set a.letzteAenderungBetreiber  = current_timestamp,
                         a.betreiberdatenUebernommen = false
                     where a.id = :anlagenteilId""")
    void aenderungDurchBetreiber(Long anlagenteilId);
}
