package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.ThematicId;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.Status;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProductionInstallationPart {

    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    private String gmlId;

    @XmlElement(required = true, name = "inspireId", namespace = SchemaSet.SCHEMA_PF)
    private InspireID inspireID;

    @XmlElement(required = false, name = "thematicId", namespace = SchemaSet.SCHEMA_PF)
    private ThematicId thematicID;

    @XmlElement(required = true, name = "pointGeometry", namespace = SchemaSet.SCHEMA_PF)
    private final PointLocation geometry = new PointLocation();

    @XmlElement(required = true, name = "status", namespace = SchemaSet.SCHEMA_PF)
    private Status status;

    @XmlElement(required = true, name = "type", namespace = SchemaSet.SCHEMA_PF, nillable = true)
    private String type;

    @XmlElement(required = true, name = "technique", namespace = SchemaSet.SCHEMA_PF, nillable = true)
    private String technique;

    @XmlElement(required = true, name = "installationPartName")
    private FeatureNameContainer installationPartName;

    @XmlElement(required = true, name = "plantType")
    private ReferenceType plantType;

    @XmlElement(required = false, name = "derogations")
    private List<ReferenceType> derogations = new ArrayList<>();

    @XmlElement(required = false, name = "nominalCapacity")
    private NominalCapacity nominalCapacity;

    @XmlElement(required = false, name = "specificConditions")
    private SpecificConditions specificConditions;

    @XmlElement(required = false, name = "totalRatedThermalInput")
    private Double totalRatedThermalInput;

    @XmlElement(required = false, name = "remarks")
    private String remarks;

    @XmlElement(required = false, name = "heatReleaseHazardousWaste")
    private Boolean heatReleaseHazardousWaste;

    @XmlElement(required = false, name = "untreatedMunicipalWaste")
    private Boolean untreatedMunicipalWaste;

    @XmlElement(required = false, name = "publicDisclosure")
    private String publicDisclosure;

    @XmlElement(required = false, name = "publicDisclosureURL")
    private String publicDisclosureURL;

    @XmlElement(required = true, name = "dateOfStartOfOperation", nillable = true)
    private Nillable dateOfStartOfOperation;

    public InspireID getInspireID() {
        return inspireID;
    }

    public void setInspireID(InspireID inspireID) {
        this.inspireID = inspireID;
    }

    public ThematicId getThematicID() {
        return thematicID;
    }

    public void setThematicID(ThematicId thematicID) {
        this.thematicID = thematicID;
    }

    public Point getGeometry() {
        return geometry.getPoint();
    }

    public void setGeometry(Point point) {
        geometry.setPoint(point);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public FeatureNameContainer getInstallationPartName() {
        return installationPartName;
    }

    public void setInstallationPartName(FeatureNameContainer installationPartName) {
        this.installationPartName = installationPartName;
    }

    public ReferenceType getPlantType() {
        return plantType;
    }

    public void setPlantType(ReferenceType plantType) {
        this.plantType = plantType;
    }

    public List<String> getDerogations() {
        if (derogations == null || derogations.isEmpty()) {
            return new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        for (ReferenceType type : derogations) {
            list.add(type.getCode());
        }
        return list;
    }

    public void setDerogations(List<String> derogationsCodes) {
        if (derogationsCodes == null || derogationsCodes.isEmpty()) {
            return;
        }
        derogations = new ArrayList<>();
        for (String code : derogationsCodes) {
            derogations.add(new ReferenceType(SchemaSet.DEROGATIONS_VALUE_CODE, code));
        }
    }

    public void addDerogation(String derogationsCode) {
        if (derogations == null) {
            derogations = new ArrayList<>();
        }

        derogations.add(new ReferenceType(SchemaSet.DEROGATIONS_VALUE_CODE, derogationsCode));
    }

    public NominalCapacity getNominalCapacity() {
        return nominalCapacity;
    }

    public void setNominalCapacity(NominalCapacity nominalCapacity) {
        this.nominalCapacity = nominalCapacity;
    }

    public SpecificConditions getSpecificConditions() {
        return specificConditions;
    }

    public void setSpecificConditions(SpecificConditions specificConditions) {
        this.specificConditions = specificConditions;
    }

    public double getTotalRatedThermalInput() {
        return totalRatedThermalInput;
    }

    public void setTotalRatedThermalInput(Double totalRatedThermalInput) {
        this.totalRatedThermalInput = totalRatedThermalInput;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isHeatReleaseHazardousWaste() {
        return heatReleaseHazardousWaste;
    }

    public void setHeatReleaseHazardousWaste(Boolean heatReleaseHazardousWaste) {
        this.heatReleaseHazardousWaste = heatReleaseHazardousWaste;
    }

    public boolean isUntreatedMunicipalWaste() {
        return untreatedMunicipalWaste;
    }

    public void setUntreatedMunicipalWaste(Boolean untreatedMunicipalWaste) {
        this.untreatedMunicipalWaste = untreatedMunicipalWaste;
    }

    public String getPublicDisclosure() {
        return publicDisclosure;
    }

    public void setPublicDisclosure(String publicDisclosure) {
        this.publicDisclosure = publicDisclosure;
    }

    public String getPublicDisclosureURL() {
        return publicDisclosureURL;
    }

    public void setPublicDisclosureURL(String publicDisclosureURL) {
        this.publicDisclosureURL = publicDisclosureURL;
    }

    public String getDateOfStartOfOperation() {
        return Nillable.getValueOfNillable(dateOfStartOfOperation);
    }

    public void setDateOfStartOfOperation(String dateOfStartOfOperationValue) {
        dateOfStartOfOperation = Nillable.getUpdatedNillable(dateOfStartOfOperation, dateOfStartOfOperationValue);
    }

    public String getGmlId() {
        return gmlId;
    }

    public void setGmlId(String gmlId) {
        this.gmlId = gmlId;
    }
}
