package de.wps.bube.basis.euregistry.xml.bericht;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.FeatureMember;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

public class EURegGMLWriterProtokoll {

    public static class EURegBetriebsstaetteProtokoll {

        private final EURegBetriebsstaette euRegBetriebsstaette;
        private String productionFacilityLocalId;
        private String productionSiteLocalId;
        private final Deque<EURegAnlageProtokoll> euRegAnlageProtokolle = new LinkedList<>();
        private boolean hasMappingErrors = false;
        private boolean hasValidationErrors = false;
        private String mappingError;
        private final List<String> validationErrors = new ArrayList<>();

        public EURegBetriebsstaetteProtokoll(EURegBetriebsstaette euRegBetriebsstaette) {
            this.euRegBetriebsstaette = euRegBetriebsstaette;
        }
        
        private boolean hasMappingErrors() {
            return hasMappingErrors;
        }

        public boolean hasValidationErrors() {
            return hasValidationErrors;
        }

        private StringWriter displayIdentifier(StringWriter writer) {
            writer.append("Betriebsstätte ")
                  .append(euRegBetriebsstaette.getBetriebsstaette().getBetriebsstaetteNr())
                  .append(" | ")
                  .append(euRegBetriebsstaette.getBetriebsstaette().getName());
            if (productionFacilityLocalId != null) {
                writer.append(" | ").append(productionFacilityLocalId);
            }
            if (productionSiteLocalId != null) {
                writer.append(" | ").append(productionSiteLocalId);
            }
            return writer;
        }

        private void display(StringWriter writer) {
            displayIdentifier(writer).append("\n");
            euRegAnlageProtokolle.forEach(anl -> anl.display(writer).append("\n"));
            writer.append("\n");
        }

        public void displayMappingErrors(StringWriter writer) {
            displayIdentifier(writer);
            if (mappingError != null) {
                writer.append(" : ").append(mappingError);
            }
            writer.append("\n");
            euRegAnlageProtokolle.stream().filter(EURegAnlageProtokoll::hasMappingError).forEach(
                   anl ->  anl.display(writer.append("  ")).append(" : ").append(anl.mappingError).append("\n")
            );
            writer.append("\n");
        }

        public void displayValidationErrors(StringWriter writer) {
            displayIdentifier(writer).append(" :\n");
            for (String validationError : validationErrors) {
                writer.append("  ").append(validationError).append("\n");
            }
            euRegAnlageProtokolle.stream()
                                 .filter(EURegAnlageProtokoll::hasValidationErrors)
                                 .forEach(anl -> {
                                     anl.display(writer.append("  ")).append(" :\n");
                                     for (String validationError : anl.validationErrors) {
                                         writer.append("      ").append(validationError).append("\n");
                                     }
                                 }
            );
            writer.append("\n");
        }
    }

    public static class EURegAnlageProtokoll {

        private final EURegAnlage euRegAnlage;
        private String productionInstallationLocalId;
        private String productionInstallationPartLocalId;
        private String mappingError;
        private final List<String> validationErrors = new ArrayList<>();

        public EURegAnlageProtokoll(EURegAnlage euRegAnlage) {
            this.euRegAnlage = euRegAnlage;
        }

        private boolean hasMappingError() {
            return mappingError != null;
        }

        public boolean hasValidationErrors() {
            return !validationErrors.isEmpty();
        }

        private StringWriter display(StringWriter writer) {
            Anlage anlage = euRegAnlage.getAnlage();
            if (anlage != null) {
                writer.append("Anlage ").append(anlage.getAnlageNr()).append(" | ").append(anlage.getName());
            } else {
                Anlagenteil anlagenteil = euRegAnlage.getAnlagenteil();
                writer.append("Anlagenteil ").append(anlagenteil.getAnlagenteilNr()).append(" | ")
                      .append(anlagenteil.getName());
            }
            if (productionInstallationLocalId != null) {
                writer.append(" | ").append(productionInstallationLocalId);
            }
            if (productionInstallationPartLocalId != null) {
                writer.append(" | ").append(productionInstallationPartLocalId);
            }
            return writer;
        }
    }

    private final Deque<EURegBetriebsstaetteProtokoll> euRegBetriebsstaetteProtokolle = new LinkedList<>();
    private boolean hasMappingErrors = false;
    private boolean hasValidationErrors = false;
    private boolean globalValidation = true;
    private final List<String> globalErrors = new ArrayList<>();
    private final List<String> validationErrors = new ArrayList<>();

    public boolean hasMappingErrors() {
        return hasMappingErrors;
    }

    public boolean hasValidationErrors() {
        return hasValidationErrors;
    }

    public void start(EURegBetriebsstaette euRegBetriebsstaette) {
        globalValidation = false;
        euRegBetriebsstaetteProtokolle.add(new EURegBetriebsstaetteProtokoll(euRegBetriebsstaette));
    }

    public void start(EURegAnlage euRegAnlage) {
        euRegBetriebsstaetteProtokolle.getLast().euRegAnlageProtokolle.add(new EURegAnlageProtokoll(euRegAnlage));
    }

    public void setGlobalValidation() {
        this.globalValidation = true;
    }

    public void addMappingError(String error) {
        this.hasMappingErrors = true;
        if (globalValidation) {
            globalErrors.add(error);
        } else {
            EURegBetriebsstaetteProtokoll euRegBetriebsstaetteProtokoll = euRegBetriebsstaetteProtokolle.getLast();
            euRegBetriebsstaetteProtokoll.hasMappingErrors = true;
            if (euRegBetriebsstaetteProtokoll.euRegAnlageProtokolle.isEmpty()) {
                euRegBetriebsstaetteProtokoll.mappingError = error;
            } else {
                euRegBetriebsstaetteProtokoll.euRegAnlageProtokolle.getLast().mappingError = error;
            }
        }
    }

    public void addValidationError(String error) {
        this.hasValidationErrors = true;
        if (globalValidation) {
            validationErrors.add(error);
        } else {
            EURegBetriebsstaetteProtokoll euRegBetriebsstaetteProtokoll = euRegBetriebsstaetteProtokolle.getLast();
            euRegBetriebsstaetteProtokoll.hasValidationErrors = true;
            if (euRegBetriebsstaetteProtokoll.euRegAnlageProtokolle.isEmpty()) {
                euRegBetriebsstaetteProtokoll.validationErrors.add(error);
            } else {
                euRegBetriebsstaetteProtokoll.euRegAnlageProtokolle.getLast().validationErrors.add(error);
            }
        }
    }

    public void log(FeatureMember featureMember) {
        if (featureMember.getProductionSite() != null) {
            euRegBetriebsstaetteProtokolle.getLast().productionSiteLocalId
                    = featureMember.getProductionSite().getInspireID().getIdentifier().getLocalId();
        } else if (featureMember.getProductionFacility() != null) {
            euRegBetriebsstaetteProtokolle.getLast().productionFacilityLocalId
                    = featureMember.getProductionFacility().getInspireID().getIdentifier().getLocalId();
        } else if (featureMember.getProductionInstallation() != null) {
            euRegBetriebsstaetteProtokolle.getLast().euRegAnlageProtokolle.getLast().productionInstallationLocalId
                    = featureMember.getProductionInstallation().getInspireID().getIdentifier().getLocalId();
        } else if (featureMember.getProductionInstallationPart() != null) {
            euRegBetriebsstaetteProtokolle.getLast().euRegAnlageProtokolle.getLast().productionInstallationPartLocalId
                    = featureMember.getProductionInstallationPart().getInspireID().getIdentifier().getLocalId();
        }
    }
    
    public StringWriter display(StringWriter writer) {
        euRegBetriebsstaetteProtokolle.forEach(bst -> bst.display(writer));
        return writer;
    }

    public StringWriter displayMappingErrors(StringWriter writer) {
        globalErrors.forEach(globalError -> writer.append(globalError).append("\n"));
        euRegBetriebsstaetteProtokolle.stream()
                                      .filter(EURegBetriebsstaetteProtokoll::hasMappingErrors)
                                      .forEach(bst -> bst.displayMappingErrors(writer));
        return writer;
    }

    public StringWriter displayValidationErrors(StringWriter writer) {
        validationErrors.forEach(validationError -> writer.append(validationError).append("\n"));
        euRegBetriebsstaetteProtokolle.stream()
                                      .filter(EURegBetriebsstaetteProtokoll::hasValidationErrors)
                                      .forEach(bst -> bst.displayValidationErrors(writer));
        return writer;
    }

}
