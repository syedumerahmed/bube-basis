package de.wps.bube.basis.komplexpruefung.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;

public interface KomplexpruefungRepository
        extends JpaRepository<Komplexpruefung, Long>, JpaSpecificationExecutor<Komplexpruefung> {

}
