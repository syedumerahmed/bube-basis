package de.wps.bube.basis.stammdaten.domain;

import static java.lang.String.format;

import de.wps.bube.basis.base.persistence.DuplicateKeyException;

public class QuelleDuplicateKeyException extends DuplicateKeyException {
    public QuelleDuplicateKeyException(String message) {
        super(message);
    }

    public static QuelleDuplicateKeyException forQuelleNr(String nummer) {
        return new QuelleDuplicateKeyException(format("Eine Quelle mit der Nummer '%s' existiert bereits.", nummer));
    }
}
