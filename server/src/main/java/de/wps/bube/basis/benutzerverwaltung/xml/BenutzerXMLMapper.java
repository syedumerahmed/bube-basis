package de.wps.bube.basis.benutzerverwaltung.xml;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;

@Mapper(uses = DatenberechtigungXMLMapper.class)
public interface BenutzerXMLMapper extends XDtoMapper<BenutzerType, Benutzer> {

    @Override
    BenutzerType toXDto(Benutzer benutzer);

    @Override
    default EntityWithError<Benutzer> fromXDto(BenutzerType benutzerType) {
        return new EntityWithError<>(map(benutzerType));
    }

    @Mapping(target = "zugewieseneRollen", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "behoerden", ignore = true)
    @Mapping(target = "akzSet", ignore = true)
    @Mapping(target = "verwaltungsgebiete", ignore = true)
    @Mapping(target = "betriebsstaetten", ignore = true)
    @Mapping(target = "tempPassword", ignore = true)
    @Mapping(target = "needsTempPassword", ignore = true)
    Benutzer map(BenutzerType benutzerType);

}
