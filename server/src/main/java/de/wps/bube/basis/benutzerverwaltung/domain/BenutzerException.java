package de.wps.bube.basis.benutzerverwaltung.domain;

public class BenutzerException extends RuntimeException {
    public BenutzerException(String message) {
        super(message);
    }

    public BenutzerException(String message, Exception e) {
        super(message, e);
    }
}
