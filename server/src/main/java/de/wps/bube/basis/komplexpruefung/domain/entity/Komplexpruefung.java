package de.wps.bube.basis.komplexpruefung.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "komplexpruefung")
public class Komplexpruefung {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "komplexpruefung_seq")
    @SequenceGenerator(name = "komplexpruefung_seq", allocationSize = 50)
    private Long id;

    @NotNull
    private Instant ausfuehrung;

    @NotNull
    private Long anzahlGueltig = 0L;
    @NotNull
    private Long anzahlWarnungen = 0L;
    @NotNull
    private Long anzahlFehler = 0L;
    @NotNull
    private Long anzahlSemantischUngueltig = 0L;

    public Komplexpruefung() {
        this.ausfuehrung = Instant.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getAusfuehrung() {
        return ausfuehrung;
    }

    public void setAusfuehrung(Instant ausfuehrung) {
        this.ausfuehrung = ausfuehrung;
    }

    public Long getAnzahlGueltig() {
        return anzahlGueltig;
    }

    public void addAnzahlGueltig(Long anzahlGueltig) {
        this.anzahlGueltig += anzahlGueltig;
    }

    public Long getAnzahlWarnungen() {
        return anzahlWarnungen;
    }

    public void addAnzahlWarnungen(Long anzahlWarnungen) {
        this.anzahlWarnungen += anzahlWarnungen;
    }

    public Long getAnzahlFehler() {
        return anzahlFehler;
    }

    public void addAnzahlFehler(Long anzahlFehler) {
        this.anzahlFehler += anzahlFehler;
    }

    public Long getAnzahlSemantischUngueltig() {
        return anzahlSemantischUngueltig;
    }

    public void addAnzahlSemantischUngueltig(Long anzahlSemantischUngueltig) {
        this.anzahlSemantischUngueltig += anzahlSemantischUngueltig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Komplexpruefung that = (Komplexpruefung) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Komplexpruefung{" +
                "id=" + id +
                ", ausfuehrung=" + ausfuehrung +
                ", anzahlGueltig=" + anzahlGueltig +
                ", anzahlWarnungen=" + anzahlWarnungen +
                ", anzahlFehler=" + anzahlFehler +
                ", anzahlSemantischUngueltig=" + anzahlSemantischUngueltig +
                '}';
    }

    public boolean hatFehler() {
        return anzahlFehler + anzahlSemantischUngueltig > 0;
    }
}