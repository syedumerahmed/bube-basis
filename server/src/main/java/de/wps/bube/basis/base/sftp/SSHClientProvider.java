package de.wps.bube.basis.base.sftp;

import org.springframework.stereotype.Component;

import net.schmizz.sshj.SSHClient;

@Component
public class SSHClientProvider {

    public SSHClient newSSHClient() {
        return new SSHClient();
    }

}
