package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.R4BV;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RIET;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RPRTR;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RVORS;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class VorschriftBean {

    private final Long id;
    private final ReferenzBean art;
    private final List<ReferenzBean> taetigkeiten;
    private final ReferenzBean haupttaetigkeit;

    public VorschriftBean(Long id, ReferenzBean art, List<ReferenzBean> taetigkeiten, ReferenzBean haupttaetigkeit) {
        this.id = id;
        this.art = art;
        this.taetigkeiten = taetigkeiten;
        this.haupttaetigkeit = haupttaetigkeit;
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @RegelApi.Member(referenzliste = RVORS)
    public ReferenzBean getArt() {
        return art;
    }

    @RegelApi.Member(description = "Tätigkeiten", referenzliste = { R4BV, RIET, RPRTR})
    public List<ReferenzBean> getTaetigkeiten() {
        return taetigkeiten;
    }

    @RegelApi.Member(description = "Haupttätigkeit", referenzliste = { R4BV, RIET, RPRTR})
    public ReferenzBean getHaupttaetigkeit() {
        return haupttaetigkeit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        VorschriftBean that = (VorschriftBean) o;
        return Objects.equals(id, that.id) && Objects.equals(art, that.art) && Objects.equals(
                taetigkeiten, that.taetigkeiten) && Objects.equals(haupttaetigkeit, that.haupttaetigkeit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, art, taetigkeiten, haupttaetigkeit);
    }

    @Override
    public String toString() {
        return "VorschriftBean{" +
                "id=" + id +
                ", art=" + art +
                ", taetigkeiten=" + taetigkeiten +
                ", haupttaetigkeit=" + haupttaetigkeit +
                '}';
    }

}
