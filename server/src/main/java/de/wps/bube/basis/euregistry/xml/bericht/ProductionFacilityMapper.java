package de.wps.bube.basis.euregistry.xml.bericht;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.ThematicId;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.CompetentAuthorityEPRTR;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ParentCompany;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionFacility;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.Status;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Mapper(uses = { AddressMapper.class, CommonEURegMapper.class, CommonXMLMapper.class })
public abstract class ProductionFacilityMapper {
    @Mapping(target = "inspireID", source = "betriebsstaette")
    @Mapping(target = "thematicId", source = "berichtsdaten")
    @Mapping(target = "geometry", source = "betriebsstaette.geoPunkt")
    @Mapping(target = "facilityName.nameOfFeature", source = "betriebsstaette.name")
    @Mapping(target = "facilityName.confidentialityReason", source = "betriebsstaette.vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    @Mapping(target = "parentCompany", source = "betriebsstaette.betreiber")
    @Mapping(target = "facilityType.schema", constant = SchemaSet.FACILITY_TYPE_CODES)
    @Mapping(target = "facilityType.code", source = "berichtsdaten.berichtsart")
    @Mapping(target = "eprtrAnnexIActivity.mainActivity", source = "betriebsstaette.prtrHaupttaetigkeit", qualifiedByName = "prtrTaetigkeit")
    @Mapping(target = "eprtrAnnexIActivity.otherActivity", source = "betriebsstaette.prtrTaetigkeitenOhneHaupttaetigkeit", qualifiedByName = "prtrTaetigkeit")
    @Mapping(target = "dateOfStartOfOperation", source = "betriebsstaette.inbetriebnahme")
    @Mapping(target = "status", source = "betriebsstaette")
    @Mapping(target = "competentAuthorityEPRTR", expression = "java(createUbaAuthority(context.getUbaProperties()))")
    @Mapping(target = "riverBasinDistrict", source = "betriebsstaette.flusseinzugsgebiet")
    @Mapping(target = "remarks", source = "berichtsdaten.bemerkung")
    @Mapping(target = "address", source = "betriebsstaette.adresse")
    @Mapping(target = "function.activity.schema", constant = SchemaSet.NACE_VALUE_CODES)
    @Mapping(target = "function.activity.code", source = "betriebsstaette.naceCode")

    @Mapping(target = "hostingSite.schema", constant = "#")
    @Mapping(target = "hostingSite.code", expression = "java(context.getProductionSite().getGmlId())")
    // TODO so nicht spezifiziert (aber auch nicht anders)
    @Mapping(target = "gmlId", expression = "java(context.getProductionSite().getGmlId() + entity.getBetriebsstaette().getLocalId())")

    @Mapping(target = "groupedInstallation", expression = "java(createGroupedInstallation(context))")
    @Mapping(target = "productionInstallations", ignore = true)
    @Mapping(target = "validFrom", ignore = true)
    @Mapping(target = "beginLifespanVersion", ignore = true)
    public abstract ProductionFacility toXDto(EURegBetriebsstaette entity,
            @Context ProductionFacilityMapperContext context);

    @Mapping(target = "identifier.namespace", expression = "java(context.getInspireParam().getNamespace())")
    @Mapping(target = "identifier.localId", source = "localId")
    abstract InspireID mapInspireID(Betriebsstaette betriebsstaette, @Context ProductionFacilityMapperContext context);

    @Mapping(target = "identifierScheme", source = "schema")
    @Mapping(target = "identifier", source = "thematicId")
    abstract ThematicId mapThematicId(Berichtsdaten berichtsdaten);

    @Mapping(target = "parentCompanyName", source = "name")
    @Mapping(target = "parentCompanyURL", ignore = true)
    @Mapping(target = "confidentialityReason", source = "vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    abstract ParentCompany mapParentCompany(Betreiber betreiber);

    @Mapping(target = "statusType.schema", constant = SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES)
    @Mapping(target = "statusType.code", source = "betriebsstatus")
    @Mapping(target = "validFrom", source = "betriebsstatusSeit")
    @Mapping(target = "description", ignore = true)
    @Mapping(target = "validTo", ignore = true)
    abstract Status mapStatus(Betriebsstaette betriebsstaette);

    @Mapping(target = "schema", constant = SchemaSet.EPRTR_ACTIVITY_VALUE_CODES)
    @Mapping(target = "code", source = ".")
    @Named("prtrTaetigkeit")
    abstract ReferenceType mapPrtrTaetigkeit(Referenz prtrTaetigkeit);

    @Mapping(target = "address.confidentialityReason", ignore = true)
    abstract CompetentAuthorityEPRTR createUbaAuthority(XeurProperties.UbaProperties properties);

    protected List<ProductionFacility.GroupedInstallation> createGroupedInstallation(
            ProductionFacilityMapperContext context) {

        List<ProductionFacility.GroupedInstallation> list = new ArrayList<>();
        for (String anlageLocalId : context.getAnlageLocalIds()) {
            ProductionFacility.GroupedInstallation installation = new ProductionFacility.GroupedInstallation();
            installation.setHref("#" + context.getProductionSite().getGmlId() + anlageLocalId);
            list.add(installation);
        }
        return list;

    }

    interface ProductionFacilityMapperContext {
        ProductionSite getProductionSite();

        XeurProperties.UbaProperties getUbaProperties();

        InspireParam getInspireParam();

        List<String> getAnlageLocalIds();
    }

}
