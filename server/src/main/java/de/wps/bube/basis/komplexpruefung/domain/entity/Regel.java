package de.wps.bube.basis.komplexpruefung.domain.entity;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.komplexpruefung.domain.RegelException;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Regel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "regel_seq")
    @SequenceGenerator(name = "regel_seq", allocationSize = 50)
    private Long nummer;

    @NotNull
    private Land land;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<RegelObjekt> objekte;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RegelGruppe gruppe;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RegelTyp typ;

    private boolean typBundeseinheitlich;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @MapKeyColumn(name = "land")
    @Column(name = "typ")
    private final Map<Land, RegelTyp> typAenderungen = new HashMap<>();

    @NotNull
    private String beschreibung;

    @NotNull
    private String fehlertext;

    @NotNull
    private String code;

    // Gilt ab 1.1.yyyy bezogen auf Berichtsjahr, min. 1900
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_von"))
    private Gueltigkeitsjahr gueltigVon;

    // Gilt bis 31.12.yyyy bezogen auf Berichtsjahr, max. 9999
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_bis"))
    private Gueltigkeitsjahr gueltigBis;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected Regel() {
    }

    public Regel(Land land, List<RegelObjekt> objekte, RegelGruppe gruppe, RegelTyp typ, boolean typBundeseinheitlich,
            String beschreibung, String fehlertext, Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis,
            String code) {
        this.land = land;
        this.objekte = objekte;
        this.gruppe = gruppe;
        this.typ = typ;
        this.typBundeseinheitlich = typBundeseinheitlich;
        this.beschreibung = beschreibung;
        this.fehlertext = fehlertext;
        this.gueltigVon = gueltigVon;
        this.gueltigBis = gueltigBis;
        this.code = code;
    }

    public Long getNummer() {
        return nummer;
    }

    public void setNummer(Long nummer) {
        this.nummer = nummer;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public Gueltigkeitsjahr getGueltigVon() {
        return gueltigVon;
    }

    public void setGueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
    }

    public Gueltigkeitsjahr getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public List<RegelObjekt> getObjekte() {
        return objekte;
    }

    public void setObjekte(List<RegelObjekt> objekte) {
        this.objekte = objekte;
    }

    public RegelGruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(RegelGruppe gruppe) {
        this.gruppe = gruppe;
    }

    public RegelTyp getTyp() {
        return typ;
    }

    public void setTyp(RegelTyp typ) {
        this.typ = typ;
    }

    public boolean isTypBundeseinheitlich() {
        return typBundeseinheitlich;
    }

    public void setTypBundeseinheitlich(boolean typBundeseinheitlich) {
        this.typBundeseinheitlich = typBundeseinheitlich;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getFehlertext() {
        return fehlertext;
    }

    public void setFehlertext(String fehlertext) {
        this.fehlertext = fehlertext;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<Land, RegelTyp> getTypAenderungen() {
        return typAenderungen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Regel regel = (Regel) o;
        return Objects.equals(nummer, regel.nummer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nummer);
    }

    @Override
    public String toString() {
        return "Regel{" + "nummer=" + nummer + ", land=" + land + ", gruppe=" + gruppe + ", typ=" + typ + ", gueltigVon=" + gueltigVon + ", gueltigBis=" + gueltigBis + '}';
    }

    public void pruefeGueltigkeit() {
        if (gueltigBis.liegtVor(gueltigVon)) {
            throw new RegelException("Gültig bis liegt zeitlich vor Gültig von");
        }
    }

    public boolean hatDatenberechtigung(boolean gesamtadmin, Land land, boolean landRolle) {
        if (gruppe == RegelGruppe.GRUPPE_B && !gesamtadmin) {
            return false;
        }
        return gesamtadmin || isOfLand(land) && landRolle;
    }

    public boolean isOfLand(Land land) {
        return this.land == land;
    }

    public void pruefeObjekteInGruppe() {
        // Für B ist alles erlaubt
        if (gruppe == RegelGruppe.GRUPPE_B) {
            return;
        }

        for (RegelObjekt regelObjekt : objekte) {
            if (regelObjekt.isEURegObjekt()) {
                throw new RegelException("Ein oder mehrere falsche Objekte für Gruppe A");
            }
        }
    }

    public RegelTyp getTyp(Land einLand) {
        if (typAenderungen.containsKey(einLand)) {
            return typAenderungen.get(einLand);
        } else {
            return typ;
        }
    }

    public void aendereTyp(Land einLand, RegelTyp typ) {
        if (typBundeseinheitlich) {
            throw new RegelException("Der Typ kann bei bundeseinheitlichen Regeln nicht geändert werden");
        }
        if (einLand == Land.DEUTSCHLAND) {
            throw new RegelException("Der Typ kann für das Land Deutschland nicht geändert werden");
        }
        if (this.land != Land.DEUTSCHLAND) {
            throw new RegelException("Der Typ kann nur bei Regeln mit Land=Deutschland geändert werden");
        }
        typAenderungen.put(einLand, typ);
    }

    public void pruefeTypBundeseinheitlich() {
        if (typBundeseinheitlich && (land != Land.DEUTSCHLAND)) {
            throw new RegelException("Typ bundeseinheitlich kann nur bei Regeln mit Land=Deutschland gesetzt werden");
        }
    }

}
