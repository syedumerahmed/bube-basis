package de.wps.bube.basis.referenzdaten.domain.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.ObjectFactory;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.BehoerdeImportException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdenImportTriggeredEvent;
import de.wps.bube.basis.referenzdaten.xml.BehoerdeXMLMapper;
import de.wps.bube.basis.referenzdaten.xml.dto.BehoerdenlisteXDto;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Service
@Transactional
public class BehoerdenImportExportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BehoerdenImportExportService.class);

    private final BehoerdenService behoerdenService;
    private final XMLService xmlService;
    private final BehoerdeXMLMapper behoerdeXMLMapper;
    private final DateiService dateiService;
    private final ApplicationEventPublisher eventPublisher;

    public BehoerdenImportExportService(BehoerdenService behoerdenService, XMLService xmlService,
            BehoerdeXMLMapper behoerdeXMLMapper, DateiService dateiService,
            ApplicationEventPublisher eventPublisher) {
        this.behoerdenService = behoerdenService;
        this.xmlService = xmlService;
        this.behoerdeXMLMapper = behoerdeXMLMapper;
        this.dateiService = dateiService;
        this.eventPublisher = eventPublisher;
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_READ)
    @Transactional(readOnly = true)
    public void exportBehoerden(ByteArrayOutputStream outputStream, List<Long> behoerdenList) {
        var objectFactory = new ObjectFactory();
        Stream<Behoerde> stream = behoerdenList.stream().map(behoerdenService::readBehoerde);
        var writer = xmlService.createWriter(BehoerdeType.class, BehoerdenlisteXDto.QUALIFIED_NAME,
               behoerdeXMLMapper::toXDto, objectFactory::createBehoerde);

        writer.write(stream , outputStream);
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    @Transactional(readOnly = true)
    public String existierendeDatei(String username) {
        return dateiService.existingDatei(username, DateiScope.BEHOERDE);
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public void dateiSpeichern(MultipartFile file, String username) {
        dateiService.dateiSpeichern(file, username, DateiScope.BEHOERDE);
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    @Transactional(readOnly = true)
    public List<Validierungshinweis> validiere(String username) {
        List<Validierungshinweis> hinweise = new ArrayList<>();

        try {
            // Das XML-Schema Validieren
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.BEHOERDE);
            xmlService.validate(inputStream, BehoerdenlisteXDto.XSD_SCHEMA_URL, hinweise);

        } catch (XMLImportException e) {
            hinweise.add(Validierungshinweis.of(e.getLocalizedMessage(), Validierungshinweis.Severity.FEHLER));
        } catch (Exception e) {
            throw new ReferenzdatenException("Es ist ein Fehler bei der Validierung aufgetreten");
        }

        return hinweise;
    }

    /**
     * Wird vom Behörden-Importieren-Job Aufgerufen
     */
    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public void readXml(String username, Consumer<EntityWithError<Behoerde>> consumer) {
        InputStream inputStream = dateiService.getDateiStream(username, DateiScope.BEHOERDE);
        xmlService.read(inputStream, BehoerdeType.class, behoerdeXMLMapper, BehoerdenlisteXDto.XML_ROOT_ELEMENT_NAME,
                null, consumer);
    }

    /**
     * Wird vom Behörden-Importieren-Job Aufgerufen
     */
    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public void importiereBehoerde(EntityWithError<Behoerde> behoerdeWithErrors, JobProtokoll jobProtokoll) {
        Behoerde behoerde = behoerdeWithErrors.entity;

        if (!behoerdeWithErrors.errorsOrEmpty().isEmpty()) {
            behoerdeWithErrors.errorsOrEmpty().forEach(jobProtokoll::addEintrag);
            throw new BehoerdeImportException();
        }

        // Behörde anlegen oder updaten
        if (behoerde.getId() == null) {
            behoerdenService.createBehoerde(behoerde);
            jobProtokoll.addEintrag(behoerde + ". Fehlerfrei");
        } else {
            behoerdenService.updateBehoerde(behoerde);
            jobProtokoll.addEintrag(behoerde + ". Aktualisiert");
        }
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public void triggerImport(String username) {
        eventPublisher.publishEvent(new BehoerdenImportTriggeredEvent(username));
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public void dateiLoeschen(String username) {
        dateiService.loescheDatei(username, DateiScope.BEHOERDE);
    }

}
