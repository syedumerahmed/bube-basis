package de.wps.bube.basis.referenzdaten.domain.vo;

import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class Gueltigkeitsjahr {

    private int jahr;

    private Gueltigkeitsjahr(int jahr) {
        this.jahr = jahr;
    }

    @Deprecated
    public Gueltigkeitsjahr() {
    }

    public static Gueltigkeitsjahr of(int jahr) {
        if (isInvalid(jahr)) {
            throw new IllegalArgumentException();
        }
        return new Gueltigkeitsjahr(jahr);
    }

    public static Gueltigkeitsjahr of(String jahrSchluessel) {
        return of(Integer.parseInt(jahrSchluessel));
    }

    public static boolean isInvalid(int jahr) {
        return jahr < 1900 || jahr > 9999;
    }

    public int getJahr() {
        return jahr;
    }

    public boolean liegtVor(Gueltigkeitsjahr gueltigkeitsjahr) {
        return this.jahr < gueltigkeitsjahr.jahr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Gueltigkeitsjahr that = (Gueltigkeitsjahr) o;
        return jahr == that.jahr;
    }

    @Override
    public int hashCode() {
        return Objects.hash(jahr);
    }

    @Override
    public String toString() {
        return String.valueOf(jahr);
    }

}
