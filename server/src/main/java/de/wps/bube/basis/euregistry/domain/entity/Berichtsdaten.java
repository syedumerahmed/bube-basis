package de.wps.bube.basis.euregistry.domain.entity;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class Berichtsdaten {

    public static final String EUREG_BST_SCHLUESSEL = "EURegBST";
    public static final String EUREG_ANL_SCHLUESSEL = "EURegAnl";
    public static final String EUREG_F_SCHLUESSEL = "EURegF";

    @NotNull
    @ManyToOne
    private Behoerde zustaendigeBehoerde;

    // Aufgabenbereichskennzahl
    private String akz;

    //Berichttyp RRTYP = Berichtsart
    @NotNull
    @ManyToOne
    private Referenz berichtsart;

    //Bearbeitungsstatus Bericht RBEA
    @NotNull
    @ManyToOne
    private Referenz bearbeitungsstatus;

    @NotNull
    private LocalDate bearbeitungsstatusSeit;

    private String bemerkung;

    private String thematicId = "-";
    private String schema = "-";

    @Deprecated
    protected Berichtsdaten() {
    }

    public Berichtsdaten(Behoerde zustaendigeBehoerde, String akz, Referenz berichtsart, Referenz bearbeitungsstatus,
            LocalDate bearbeitungsstatusSeit) {
        Assert.notNull(bearbeitungsstatusSeit,
                "Bearbeitungsstatus seit muss als Berichtsdatenattribut angegeben werden");

        this.zustaendigeBehoerde = zustaendigeBehoerde;
        this.berichtsart = berichtsart;
        this.bearbeitungsstatus = bearbeitungsstatus;
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
        this.akz = akz;
    }

    public Behoerde getZustaendigeBehoerde() {
        return zustaendigeBehoerde;
    }

    public void setZustaendigeBehoerde(Behoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerde = zustaendigeBehoerde;
    }

    public String getAkz() {
        return akz;
    }

    public void setAkz(String akz) {
        this.akz = akz;
    }

    public Referenz getBerichtsart() {
        return berichtsart;
    }

    public void setBerichtsart(Referenz berichtsart) {
        this.berichtsart = berichtsart;
    }

    public Referenz getBearbeitungsstatus() {
        return bearbeitungsstatus;
    }

    public void setBearbeitungsstatus(Referenz bearbeitungsstatus) {
        this.bearbeitungsstatus = bearbeitungsstatus;
    }

    public LocalDate getBearbeitungsstatusSeit() {
        return bearbeitungsstatusSeit;
    }

    public void setBearbeitungsstatusSeit(LocalDate bearbeitungsstatusSeit) {
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public String getThematicId() {
        return thematicId;
    }

    public void setThematicId(String thematicId) {
        this.thematicId = thematicId;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Berichtsdaten that = (Berichtsdaten) o;
        return zustaendigeBehoerde.equals(that.zustaendigeBehoerde) && Objects.equals(akz,
                that.akz) && berichtsart.equals(that.berichtsart) && bearbeitungsstatus.equals(
                that.bearbeitungsstatus) && bearbeitungsstatusSeit.equals(
                that.bearbeitungsstatusSeit) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(thematicId,
                that.thematicId) && Objects.equals(schema, that.schema);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zustaendigeBehoerde, akz, berichtsart, bearbeitungsstatus, bearbeitungsstatusSeit,
                bemerkung, thematicId, schema);
    }

    public boolean isEURegF() {
        return EUREG_F_SCHLUESSEL.equals(berichtsart.getSchluessel());
    }

    public boolean isEURegAnl() {
        return EUREG_ANL_SCHLUESSEL.equals(berichtsart.getSchluessel());
    }

    public boolean isEURegBst() {
        return EUREG_BST_SCHLUESSEL.equals(berichtsart.getSchluessel());
    }
}
