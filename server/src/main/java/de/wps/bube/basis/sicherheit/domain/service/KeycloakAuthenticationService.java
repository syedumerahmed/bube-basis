package de.wps.bube.basis.sicherheit.domain.service;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toUnmodifiableSet;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Thema;

@Service
public class KeycloakAuthenticationService implements AuthenticationService {

    @Override
    public AktiverBenutzer getCurrentBenutzer() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Assert.isInstanceOf(KeycloakAuthenticationToken.class, authentication);
        final KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;

        Assert.isInstanceOf(SimpleKeycloakAccount.class, authenticationToken.getDetails());
        return mapAccountToBenutzer((SimpleKeycloakAccount) authenticationToken.getDetails());
    }

    private AktiverBenutzer mapAccountToBenutzer(SimpleKeycloakAccount account) {
        return new AktiverBenutzer(account.getPrincipal().getName(), mapRoles(account.getRoles()),
                parseClaims(account.getKeycloakSecurityContext().getToken().getOtherClaims()));
    }

    private Set<Rolle> mapRoles(Set<String> roles) {
        return roles.stream()
                    .map(s -> EnumUtils.getEnumIgnoreCase(Rolle.class, s))
                    .filter(Objects::nonNull)
                    .collect(() -> EnumSet.noneOf(Rolle.class), EnumSet::add, EnumSet::addAll);
    }

    private Datenberechtigung parseClaims(Map<String, Object> claims) {
        return Datenberechtigung.DatenberechtigungBuilder
                .land(Land.of((String) claims.get(Datenberechtigung.LAND)))
                .behoerden(getStringSet(claims.get(Datenberechtigung.BEHOERDEN)))
                .akz(getStringSet(claims.get(Datenberechtigung.AKZ)))
                .verwaltungsgebiete(getStringSet(claims.get(Datenberechtigung.VERWALTUNGSGEBIETE)))
                .betriebsstaetten(getStringSet(claims.get(Datenberechtigung.BETRIEBSSTAETTEN)))
                .fachmodule(getStringSet(claims.get(Datenberechtigung.FACHMODULE))
                        .stream()
                        .map(Fachmodul::of)
                        .collect(toSet()))
                .themen(getStringSet(claims.get(Datenberechtigung.THEMEN))
                        .stream()
                        .map(Thema::of)
                        .collect(toSet()))
                .build();
    }

    private Set<String> getStringSet(Object stringCollection) {
        if (!(stringCollection instanceof Collection)) {
            return Collections.emptySet();
        }
        return ((Collection<?>) stringCollection).stream()
                                                 .filter(o -> o instanceof String)
                                                 .map(String.class::cast)
                                                 .collect(toUnmodifiableSet());
    }

}
