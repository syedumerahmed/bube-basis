package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;

public class KommunikationsverbindungVector {
    private Kommunikationsverbindung spl;
    private Kommunikationsverbindung spb;
    private boolean equal;

    public KommunikationsverbindungVector(Kommunikationsverbindung spl,
            Kommunikationsverbindung spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public Kommunikationsverbindung getSpl() {
        return spl;
    }

    public void setSpl(Kommunikationsverbindung spl) {
        this.spl = spl;
    }

    public Kommunikationsverbindung getSpb() {
        return spb;
    }

    public void setSpb(Kommunikationsverbindung spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
