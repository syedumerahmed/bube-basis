package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository.berichtsjahrIdEquals;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDE_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteListItem;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.service.SuchattributePredicateFactory;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;

@Service
@Transactional(readOnly = true)
@Secured({ BEHOERDE_READ, LAND_READ })
public class EURegistrySucheService {
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final EURegBetriebsstaetteRepository repository;
    private final EuRegBstBerechtigungsAccessor berechtigungsAccessor;
    private final SuchattributePredicateFactory suchattributePredicatefactory;

    public EURegistrySucheService(StammdatenBerechtigungsContext stammdatenBerechtigungsContext, EURegBetriebsstaetteRepository repository,
            EuRegBstBerechtigungsAccessor berechtigungsAccessor,
            SuchattributePredicateFactory suchattributePredicatefactory) {
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.repository = repository;
        this.berechtigungsAccessor = berechtigungsAccessor;
        this.suchattributePredicatefactory = suchattributePredicatefactory;
    }

    public Page<EuRegBetriebsstaetteListItem> readAllEuRegBetriebsstaette(Long berichtsjahr,
            Suchattribute suchattribute, Pageable pageable) {
        var predicate = createSuchPredicate(berichtsjahr, suchattribute);
        return repository.findAllAsListItems(predicate, pageable);
    }

    public List<Long> readAllEuRegBetriebsstaetteIds(Long berichtsjahr, Suchattribute suchattribute) {
        var predicate = createSuchPredicate(berichtsjahr, suchattribute);
        return repository.findAllIds(predicate);
    }

    private Predicate createSuchPredicate(Long berichtsjahr, Suchattribute suchattribute) {
        var suchPredicate = suchattributePredicatefactory.createPredicate(suchattribute,
                eURegBetriebsstaette.betriebsstaette);
        return stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                             .and(berichtsjahrIdEquals(berichtsjahr))
                                             .and(suchPredicate);
    }
}
