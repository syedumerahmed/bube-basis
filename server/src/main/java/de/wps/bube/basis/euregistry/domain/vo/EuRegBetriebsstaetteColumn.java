package de.wps.bube.basis.euregistry.domain.vo;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;

import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum EuRegBetriebsstaetteColumn implements Column {

    LAND(eURegBetriebsstaette.betriebsstaette.land),
    ZUSTAENDIGE_BEHOERDE(eURegBetriebsstaette.berichtsdaten.zustaendigeBehoerde.bezeichnung),
    BETRIEBSSTAETTEN_NUMMER(eURegBetriebsstaette.betriebsstaette.betriebsstaetteNr),
    NAME(eURegBetriebsstaette.betriebsstaette.name),
    PLZ(eURegBetriebsstaette.betriebsstaette.adresse.plz),
    ORT(eURegBetriebsstaette.betriebsstaette.adresse.ort),
    BETRIEBSSTATUS(eURegBetriebsstaette.betriebsstaette.betriebsstatus.ktext),
    PRUEFUNGSFEHLER(eURegBetriebsstaette.pruefungsfehler);

    private static StringExpression emptyIfNull(StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final ComparableExpression<?> expression;

    EuRegBetriebsstaetteColumn(ComparableExpression<?> expression) {
        this.expression = expression;
    }

    @Override
    public ComparableExpression<?> getExpression() {
        return expression;
    }
}
