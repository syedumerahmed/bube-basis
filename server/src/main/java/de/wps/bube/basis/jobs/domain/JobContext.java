package de.wps.bube.basis.jobs.domain;

import java.util.function.Consumer;

public record JobContext<T>(JobProtokoll jobProtokoll, Consumer<T> elementConsumer) {
}
