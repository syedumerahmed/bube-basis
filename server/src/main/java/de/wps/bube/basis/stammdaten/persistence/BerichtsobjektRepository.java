package de.wps.bube.basis.stammdaten.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Berichtsobjekt;

public interface BerichtsobjektRepository extends JpaRepository<Berichtsobjekt, Long> {

    Optional<Berichtsobjekt> findByBetriebsstaetteIdAndBerichtsart(Long betriebsstaetteId, Referenz berichtsart);

    @Modifying
    @Query(value = "DELETE FROM Berichtsobjekt b WHERE b.id = :berichtsobjektId")
    void forceDelete(Long berichtsobjektId);

}
