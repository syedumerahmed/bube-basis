package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDE_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BETREIBER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;
import static de.wps.bube.basis.stammdaten.domain.entity.QBetreiber.betreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository.byStammdatenKey;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.BooleanBuilder;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdeGeloeschtEvent;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;
import de.wps.bube.basis.stammdaten.domain.AnlageDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.AnlagenteilDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.BetreiberInUseException;
import de.wps.bube.basis.stammdaten.domain.BetriebsstaetteDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.InvalidTaetigkeitException;
import de.wps.bube.basis.stammdaten.domain.LocalIdBereitsVerwendetException;
import de.wps.bube.basis.stammdaten.domain.QuelleDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageBearbeitetEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageWurdeGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilBearbeitetEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilWurdeGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnsprechpartnerLoeschenTriggerdEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetreiberGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaetteGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.StammdatenUebernehmenTriggeredEvent;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.AnlagenteilRepository;
import de.wps.bube.basis.stammdaten.persistence.BetreiberRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.persistence.QuelleRepository;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAdapter;
import de.wps.bube.basis.stammdaten.security.BetriebsstaetteBerechtigungsAdapter;
import de.wps.bube.basis.stammdaten.security.BetriebsstaetteListItemBerechtigungsAdapter;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.SecuredAdmins;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenDelete;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenRead;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenWrite;

@Service
@Transactional
public class StammdatenService {

    private final BetriebsstaetteRepository betriebsstaetteRepository;
    private final BetreiberRepository betreiberRepository;
    private final AnlageRepository anlageRepository;
    private final AnlagenteilRepository anlagenteilRepository;
    private final QuelleRepository quelleRepository;
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final BetriebsstaettenBerechtigungsAccessor betriebsstaetteBerechtigungen;
    private final BetreiberBerechtigungsAccessor betreiberBerechtigungen;
    private final StammdatenBerichtsjahrwechselMapper stammdatenBerichtsjahrwechselMapper;

    private final AktiverBenutzer aktiverBenutzer;

    private final AnlagenUpdater anlagenUpdater;
    private final BetriebsstaettenUpdater betriebsstaettenUpdater;

    private final ApplicationEventPublisher eventPublisher;

    public StammdatenService(StammdatenBerechtigungsContext stammdatenBerechtigungsContext,
            BetriebsstaettenBerechtigungsAccessor betriebsstaetteBerechtigungen,
            BetreiberBerechtigungsAccessor betreiberBerechtigungen,
            BetriebsstaetteRepository betriebsstaetteRepository,
            BetreiberRepository betreiberRepository,
            AnlageRepository anlagenRepository,
            AnlagenteilRepository anlagenteilRepository,
            QuelleRepository quelleRepository,
            StammdatenBerichtsjahrwechselMapper stammdatenBerichtsjahrwechselMapper,
            AnlagenUpdater anlagenUpdater,
            BetriebsstaettenUpdater betriebsstaettenUpdater,
            ApplicationEventPublisher eventPublisher,
            AktiverBenutzer aktiverBenutzer) {
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.betriebsstaetteBerechtigungen = betriebsstaetteBerechtigungen;
        this.betreiberBerechtigungen = betreiberBerechtigungen;
        this.betriebsstaetteRepository = betriebsstaetteRepository;
        this.anlageRepository = anlagenRepository;
        this.betreiberRepository = betreiberRepository;
        this.anlagenteilRepository = anlagenteilRepository;
        this.quelleRepository = quelleRepository;
        this.stammdatenBerichtsjahrwechselMapper = stammdatenBerichtsjahrwechselMapper;
        this.anlagenUpdater = anlagenUpdater;
        this.betriebsstaettenUpdater = betriebsstaettenUpdater;
        this.eventPublisher = eventPublisher;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    /**
     * Betriebsstätte
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<String> allBetriebsstaettenNrForCurrentUser() {
        return betriebsstaetteRepository.findAllBetriebsstaettenNr(
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
    }

    @Transactional
    @SecuredStammdatenRead
    public Stream<Referenz> getAllYearsForBetriebsstaetteId(Long id) {
        Betriebsstaette bst = this.loadBetriebsstaetteForRead(id);

        Stream<Referenz> referenzStream = getJahresReferenzenByNummer(bst);
        if (bst.getLocalId() == null) {
            return referenzStream;
        } else {
            Stream<Referenz> berichtsjahrePerLocalId = this.betriebsstaetteRepository.findAll(
                    stammdatenBerechtigungsContext.createPredicateRead(this.betriebsstaetteBerechtigungen)
                                                  .and(betriebsstaette.localId.eq(bst.getLocalId()))
            ).stream().map(Betriebsstaette::getBerichtsjahr);
            return Stream.concat(referenzStream, berichtsjahrePerLocalId).distinct();
        }
    }

    @Transactional
    @SecuredStammdatenRead
    public Stream<Referenz> getJahresReferenzenByNummer(Betriebsstaette bst) {
        return this.betriebsstaetteRepository.findAll(
                stammdatenBerechtigungsContext.createPredicateRead(this.betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.betriebsstaetteNr.equalsIgnoreCase(
                                                      bst.getBetriebsstaetteNr()))
        ).stream().map(Betriebsstaette::getBerichtsjahr);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean haveSameLand(List<Long> betriebsstaetten) {
        return betriebsstaetteRepository.countDistinctByLandAndIdIn(betriebsstaetten) == 1;
    }

    private Betriebsstaette loadBetriebsstaette(Long id, BooleanBuilder predicate) {
        return betriebsstaetteRepository.findOne(predicate.and(betriebsstaette.id.eq(id)))
                                        .orElseThrow(() -> new BubeEntityNotFoundException(Betriebsstaette.class,
                                                "ID " + id));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betriebsstaette loadBetriebsstaetteForRead(Long id) {
        return loadBetriebsstaette(id,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betriebsstaette loadBetriebsstaetteForWrite(Long id) {
        return loadBetriebsstaette(id,
                stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betriebsstaette loadBetriebsstaetteForDelete(Long id) {
        return loadBetriebsstaette(id,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Page<Betriebsstaette> findBetriebsstaetteByNummerNameOrtLimitBy10(String suchstring) {
        return betriebsstaetteRepository.findAll(
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.betriebsstaetteNr.concat(" ")
                                                                                    .concat(betriebsstaette.name)
                                                                                    .concat(" ")
                                                                                    .concat(betriebsstaette.adresse.ort)
                                                                                    .containsIgnoreCase(
                                                                                            suchstring)),
                PageRequest.of(0, 10));

    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Betriebsstaette> findBetriebsstaette(Long id) {
        return betriebsstaetteRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.id.eq(id)));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betriebsstaette loadBetriebsstaetteByJahrLandNummer(String jahrSchluessel, Land land, String nummer) {
        return findBetriebsstaetteByJahrLandNummer(jahrSchluessel, land, nummer)
                .orElseThrow(() -> new BubeEntityNotFoundException(Betriebsstaette.class, "Nummer " + nummer));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Betriebsstaette> findBetriebsstaetteByJahrLandNummer(String jahrSchluessel, Land land,
            String nummer) {
        return betriebsstaetteRepository
                .findOne(stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                                       .and(byStammdatenKey(jahrSchluessel, land, nummer)));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<String> findAllZustaendigeBehoerdenSchluessel(List<String> betriebsstaettenNummern, Land land) {
        Assert.notEmpty(betriebsstaettenNummern, "Keine Betriebsstaettennummern ausgewählt");
        Assert.notNull(land, "Kein Land ausgewählt");
        return betriebsstaetteRepository.findAllZustaendigeBehoerdenSchluessel(betriebsstaettenNummern, land);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betriebsstaette loadBetriebsstaetteByJahrLandLocalId(String jahrSchluessel, Land land, String localId) {
        return findBetriebsstaetteByJahrLandLocalId(jahrSchluessel, land, localId)
                .orElseThrow(() -> new BubeEntityNotFoundException(Betriebsstaette.class, "Local-ID " + localId));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Betriebsstaette> findBetriebsstaetteByJahrLandLocalId(String jahrSchluessel, Land land,
            String localId) {
        return betriebsstaetteRepository
                .findOne(stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                                       .and(betriebsstaette.land.eq(land))
                                                       .and(betriebsstaette.berichtsjahr.schluessel.eq(jahrSchluessel))
                                                       .and(betriebsstaette.localId.eq(localId)));
    }

    @SecuredStammdatenWrite
    public Betriebsstaette createBetriebsstaette(Betriebsstaette betriebsstaette) {
        Assert.isTrue(betriebsstaette.isNew(), "Betriebsstätte bereits vorhanden");
        Assert.isTrue(!betriebsstaette.getLand().equals(Land.DEUTSCHLAND),
                "Land der Betriebsstätte darf nicht Deutschland sein");

        this.stammdatenBerechtigungsContext.checkAccessToWrite(
                new BetriebsstaetteBerechtigungsAdapter(betriebsstaette));
        checkBetriebsstaetteFunctionalKeyExists(betriebsstaette);

        // Wenn Kindobjekte vorhanden sind, müssen hier auch die LocalIds geprüft werden
        Stream.concat(Stream.of(betriebsstaette.getLocalId()), betriebsstaette.getAnlagen()
                                                                              .stream()
                                                                              .flatMap(a -> Stream.concat(
                                                                                      Stream.of(a.getLocalId()),
                                                                                      a.getAnlagenteile()
                                                                                       .stream()
                                                                                       .map(Anlagenteil::getLocalId))))
              .filter(Objects::nonNull)
              .forEach(localId -> checkLocalIdAlreadyExists(localId, betriebsstaette.getLand().getNr(),
                      betriebsstaette.getBerichtsjahr().getId()));
        var savedBetriebsstaette = betriebsstaetteRepository.save(betriebsstaette);
        savedBetriebsstaette.getAnlagen().forEach(anlage -> {
            anlage.setParentBetriebsstaetteId(savedBetriebsstaette.getId());
            eventPublisher.publishEvent(new AnlageAngelegtEvent(anlage));
            anlage.getAnlagenteile().forEach(anlagenteil -> {
                anlagenteil.setParentAnlageId(anlage.getId());
                eventPublisher.publishEvent(new AnlagenteilAngelegtEvent(anlagenteil));
            });
        });
        return savedBetriebsstaette;
    }

    /**
     * Aktualisiert die Betriebsstaette ohne Anlagen (ink. Anlagenteilen) und Quellen
     */
    @SecuredStammdatenWrite
    public Betriebsstaette updateBetriebsstaette(Betriebsstaette betriebsstaette) {
        Assert.isTrue(!betriebsstaette.isNew(), "Betriebsstätte nicht vorhanden");
        stammdatenBerechtigungsContext.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette));
        var existing = loadBetriebsstaetteForWrite(betriebsstaette.getId());

        if (!existing.hasSameKey(betriebsstaette)) {
            checkBetriebsstaetteFunctionalKeyExists(betriebsstaette);
        }

        if (!existing.hasSameLocalIdOrNull(betriebsstaette)) {
            checkLocalIdAlreadyExists(betriebsstaette.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }

        betriebsstaettenUpdater.update(existing, betriebsstaette);

        return betriebsstaetteRepository.save(existing);
    }

    @SecuredStammdatenWrite
    public Betriebsstaette updateBetriebsstaetteForUebernahme(Betriebsstaette betriebsstaette) {
        Assert.isTrue(!betriebsstaette.isNew(), "Betriebsstätte nicht vorhanden");
        var existing = loadBetriebsstaetteForWrite(betriebsstaette.getId());

        if (!existing.hasSameKey(betriebsstaette)) {
            checkBetriebsstaetteFunctionalKeyExists(betriebsstaette);
        }

        if (!existing.hasSameLocalIdOrNull(betriebsstaette)) {
            checkLocalIdAlreadyExists(betriebsstaette.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }

        return betriebsstaetteRepository.save(betriebsstaette);
    }

    @Secured(BEHOERDENBENUTZER)
    public void updateKomplexpruefung(Long betriebsstaetteId, boolean pruefungsfehler) {
        betriebsstaetteRepository.updateKomplexpruefung(betriebsstaetteId, pruefungsfehler);
    }

    @Transactional
    @SecuredStammdatenWrite
    public void updateSchreibsperreBetreiber(Long betriebsstaetteId, boolean sperre) {
        if (!canReadBetriebsstaette(betriebsstaetteId)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
        betriebsstaetteRepository.updateSchreibsperreBetreiber(betriebsstaetteId, sperre);
    }

    @SecuredStammdatenDelete
    public void deleteBetriebsstaette(Long id) {
        Betriebsstaette betriebsstaette = loadBetriebsstaetteForDelete(id);

        betriebsstaette.getAnlagen().forEach(anlage -> deleteAnlage(anlage.getId()));

        betriebsstaette.getQuellen().forEach(quelle -> this.deleteQuelle(betriebsstaette.getId(), quelle.getId()));

        eventPublisher.publishEvent(new BetriebsstaetteGeloeschtEvent(id));
        // event is handled in eureg module which in turn deletes the Berichtsobjekt instances, so in order to
        // make Hibernate happy, we need to throw them out of the list of the Betriebsstaette:
        if (betriebsstaette.getBerichtsobjekte() != null) {
            betriebsstaette.getBerichtsobjekte().clear();
        }
        betriebsstaetteRepository.delete(betriebsstaette);

        if (betriebsstaette.hatBetreiber()) {
            if (betriebsstaetteRepository.countAllByBetreiberId(betriebsstaette.getBetreiber().getId()) == 0) {
                eventPublisher.publishEvent(new BetreiberGeloeschtEvent(betriebsstaette.getBetreiber().getId()));
                betreiberRepository.delete(betriebsstaette.getBetreiber());
            }
        }

    }

    @SecuredStammdatenDelete
    @Transactional
    public void deleteMultipleBetriebsstaetten(List<Long> ids) {
        ids.forEach(this::deleteBetriebsstaette);
    }

    @Secured(Rollen.LAND_WRITE)
    public void uebernimmBetriebsstaetten(List<Long> betriebsstaettenIds, Referenz zieljahr) {
        // Darf der Nutzer die Betriebsstätten sehen/bearbeiten
        checkBetriebsstaetteDatenberechtigung(betriebsstaettenIds,
                stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen));
        // Publish ein Event, welches den Job aufruft
        eventPublisher.publishEvent(
                new StammdatenUebernehmenTriggeredEvent(betriebsstaettenIds, zieljahr, aktiverBenutzer.getUsername()));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Stream<Betriebsstaette> streamAllById(List<Long> ids) {
        if (!canReadAllBetriebsstaette(ids)) {
            throw new AccessDeniedException("Keine Berechtigung");
        }
        return betriebsstaetteRepository.streamAllByIdIn(ids);
    }

    // Wird vom Stammdaten übernehmen Job aufgerufen
    @Secured(Rollen.LAND_WRITE)
    public void copyBetriebsstaette(Betriebsstaette betriebsstaette, Referenz zieljahr, JobProtokoll jobProtokoll) {
        try {
            Betreiber neuerBetreiber = null;
            if (betriebsstaette.hatBetreiber()) {
                neuerBetreiber = betreiberRepository.findByBerichtsjahrAndLandAndBetreiberNummer(zieljahr,
                                                            betriebsstaette.getLand(), betriebsstaette.betreiberNummerOrNull())
                                                    .orElseGet(() -> createBetreiber(
                                                            copyBetreiber(betriebsstaette.getBetreiber(), zieljahr)));
            }

            var copyWithError = stammdatenBerichtsjahrwechselMapper.copyBetriebsstaetteToBerichtsjahr(betriebsstaette,
                    zieljahr,
                    neuerBetreiber);

            createBetriebsstaette(copyWithError.entity);

            copyWithError.errorsOrEmpty().forEach(jobProtokoll::addEintrag);
            jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s. Fehlerfrei", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()));
        } catch (Exception e) {
            jobProtokoll.addFehler(format("Betriebsstätte %s Nr. %s. Fehler", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()), e.getMessage());
            throw e;
        }
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadBetriebsstaette(Long id) {
        return this.betriebsstaetteRepository.exists(
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.id.eq(id)));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadAllBetriebsstaette(List<Long> ids) {
        return ids.size() == this.betriebsstaetteRepository.count(
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.id.in(ids)));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canWriteBetriebsstaette(Long id) {
        if (this.aktiverBenutzer.hatEineDerRollen(Rolle.BEHOERDE_WRITE, Rolle.LAND_WRITE)) {
            return this.betriebsstaetteRepository.exists(
                    stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen)
                                                  .and(betriebsstaette.id.eq(id)));
        }
        return false;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenWrite
    public boolean canWriteAllBetriebsstaetten(List<Long> ids) {
        return ids.size() == this.betriebsstaetteRepository.count(
                stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen)
                                              .and(betriebsstaette.id.in(ids)));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canDeleteBetriebsstaette(Long id) {
        if (aktiverBenutzer.hatEineDerRollen(Rolle.LAND_DELETE, Rolle.BEHOERDE_DELETE)) {
            return this.betriebsstaetteRepository.exists(
                    stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen)
                                                  .and(betriebsstaette.id.eq(id)));
        }
        return false;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canDeleteBetriebsstaette(BetriebsstaetteListItemView bst) {
        return stammdatenBerechtigungsContext.hasAccessToDelete(new BetriebsstaetteListItemBerechtigungsAdapter(bst));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean behoerdeUsed(Long id) {
        return betriebsstaetteRepository.behoerdeUsed(id);
    }

    /**
     * Betreiber
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<Betreiber> loadAllBetreiberByJahrAndLand(String jahrSchluessel, Land land) {
        return ImmutableList.copyOf(betreiberRepository.findAll(
                stammdatenBerechtigungsContext.createPredicateRead(betreiberBerechtigungen)
                                              .and(betreiber.berichtsjahr.schluessel.eq(jahrSchluessel))
                                              .and(betreiber.land.eq(land))));
    }

    @SecuredStammdatenWrite
    public Betreiber createBetreiberZuBetriebsstaette(Betreiber betreiber, String betriebsstaettenNummer) {
        Betreiber savedBetreiber = createBetreiber(betreiber);

        // Solange wir keinen echten fachlichen Key haben, wird die ID als Betreibernummer verwendet
        savedBetreiber.setBetreiberNummer(String.valueOf(savedBetreiber.getId()));
        // Setze diesen Betreiber an die Betriebsstätte
        var betriebsstaette = loadBetriebsstaetteByJahrLandNummer(savedBetreiber.getBerichtsjahr().getSchluessel(),
                savedBetreiber.getLand(), betriebsstaettenNummer);
        checkBetriebsstaetteDatenberechtigung(betriebsstaette.getId(),
                stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen));
        betriebsstaette.setBetreiber(savedBetreiber);

        betriebsstaetteRepository.save(betriebsstaette);
        return betreiberRepository.save(savedBetreiber);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betreiber loadBetreiberByJahrLandNummer(String jahrSchluessel, Land land, String nummer) {
        return findBetreiberByJahrLandNummer(jahrSchluessel, land, nummer).orElseThrow(
                () -> new BubeEntityNotFoundException(Betreiber.class));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Betreiber> findBetreiberByJahrLandNummer(String jahrSchluessel, Land land, String nummer) {
        return betreiberRepository.findOne(stammdatenBerechtigungsContext.createPredicateRead(betreiberBerechtigungen)
                                                                         .and(betreiber.land.eq(land)
                                                                                            .and(betreiber.berichtsjahr.schluessel
                                                                                                    .eq(jahrSchluessel))
                                                                                            .and(betreiber.betreiberNummer.eq(
                                                                                                    nummer))));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Betreiber loadBetreiber(Long betreiberId) {
        Assert.notNull(betreiberId, "betreiberId");
        return betreiberRepository
                .findOne(stammdatenBerechtigungsContext.createPredicateRead(betreiberBerechtigungen)
                                                       .and(betreiber.id.eq(betreiberId)))
                .orElseThrow(() -> new BubeEntityNotFoundException(Betreiber.class, "ID " + betreiberId));
    }

    @SecuredStammdatenWrite
    public Betreiber updateBetreiber(Betreiber betreiber) {
        Assert.isTrue(!betreiber.isNew(), "Betreiber nicht vorhanden");
        stammdatenBerechtigungsContext.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber));
        checkBetreiberDatenberechtigung(betreiber.getId(),
                stammdatenBerechtigungsContext.createPredicateWrite(betreiberBerechtigungen));
        return betreiberRepository.save(betreiber);
    }

    @SecuredStammdatenDelete
    public void deleteBetreiber(Long id) {
        checkBetreiberDatenberechtigung(id,
                stammdatenBerechtigungsContext.createPredicateDelete(betreiberBerechtigungen));
        eventPublisher.publishEvent(new BetreiberGeloeschtEvent(id));
        if (betriebsstaetteRepository.countAllByBetreiberId(id) > 1) {
            throw new BetreiberInUseException();
        }
        betriebsstaetteRepository.findByBetreiberId(id).forEach(betriebsstaette -> {
            checkBetriebsstaetteDatenberechtigung(betriebsstaette.getId(),
                    stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen));
            betriebsstaette.setBetreiber(null);
            betriebsstaetteRepository.save(betriebsstaette);
        });
        // TODO Event to delete SpbBetreiber
        betreiberRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadBetreiber(Long... ids) {
        return canReadAllBetreiber(List.of(ids));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadAllBetreiber(List<Long> ids) {
        return ids.size() == betreiberRepository.count(
                stammdatenBerechtigungsContext.createPredicateRead(betreiberBerechtigungen)
                                              .and(betreiber.id.in(ids)));
    }

    private static Betreiber copyBetreiber(Betreiber betreiber, Referenz zieljahr) {
        Betreiber clone = betreiber.clone();
        clone.copyToZieljahr(zieljahr);
        return clone;
    }

    @SecuredStammdatenWrite
    public Betreiber createBetreiber(Betreiber betreiber) {
        Assert.isTrue(betreiber.isNew(), "Betreiber bereits vorhanden");
        stammdatenBerechtigungsContext.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber));
        return betreiberRepository.save(betreiber);
    }

    /**
     * Quelle
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<Quelle> loadAllQuellenByBetriebsstaettenId(Long betriebsstaetteId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return quelleRepository.findAllByParentBetriebsstaetteId(betriebsstaetteId);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<Quelle> loadAllQuellenByAnlagenId(Long anlageId, Long betriebsstaetteId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));

        return quelleRepository.findAllByParentAnlageId(anlageId);
    }

    @SecuredStammdatenWrite
    public Quelle createQuelle(Quelle quelle) {
        Assert.isTrue(quelle.isNew(), "Quelle bereits vorhanden");
        if (quelle.istKindVonAnlage()) {
            checkAnlageDatenberechtigungWrite(quelle.getParentAnlageId());
        } else {
            checkBetriebsstaetteDatenberechtigungWrite(quelle.getParentBetriebsstaetteId());
        }
        checkQuelleFunctionalKeyExists(quelle);

        Quelle q = quelleRepository.saveAndFlush(quelle);

        eventPublisher.publishEvent(new QuelleAngelegtEvent(q));

        return q;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Quelle loadQuelleByQuelleNummer(Long betriebsstaettenId, String quellenNummer) {
        return findQuelle(betriebsstaettenId, quellenNummer)
                .orElseThrow(() -> new BubeEntityNotFoundException(Quelle.class));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Quelle loadQuelle(Long id) {
        Quelle quelle = quelleRepository.findById(id).orElseThrow(() -> new BubeEntityNotFoundException(Quelle.class));

        if (quelle.getParentAnlageId() != null) {
            loadAnlage(quelle.getParentAnlageId());
        } else {
            checkBetriebsstaetteDatenberechtigungRead(quelle.getParentBetriebsstaetteId());
        }
        return quelle;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Quelle> findQuelle(Long betriebsstaettenId, String quellenNummer) {
        checkBetriebsstaetteDatenberechtigungRead(betriebsstaettenId);
        return quelleRepository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(betriebsstaettenId, quellenNummer);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Quelle loadQuelleOfAnlage(Long anlageId, String quellenNummer) {
        loadAnlage(anlageId);
        return quelleRepository.findByParentAnlageIdAndQuelleNrIgnoreCase(anlageId, quellenNummer)
                               .orElseThrow(() -> new BubeEntityNotFoundException(Quelle.class));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Quelle> findQuelleOfAnlage(Long anlageId, String quellenNummer) {
        loadAnlage(anlageId);
        return quelleRepository.findByParentAnlageIdAndQuelleNrIgnoreCase(anlageId, quellenNummer);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Quelle> findQuelleOfBst(Long betriebsstaetteId, String quellenNummer) {
        checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);
        return quelleRepository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(betriebsstaetteId, quellenNummer);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadQuelle(Long id) {
        var quelle = quelleRepository.getById(id);
        if (quelle.istKindVonBetriebsstaette()) {
            return canReadBetriebsstaette(quelle.getParentBetriebsstaetteId());
        }
        return canReadAnlage(quelle.getParentAnlageId());
    }

    @SecuredStammdatenWrite
    @Transactional
    public Quelle updateQuelle(Quelle quelle) {
        Assert.isTrue(!quelle.isNew(), "Quelle nicht vorhanden");
        if (quelle.istKindVonAnlage()) {
            checkAnlageDatenberechtigungWrite(quelle.getParentAnlageId());
        } else {
            checkBetriebsstaetteDatenberechtigungWrite(quelle.getParentBetriebsstaetteId());
        }
        var existing = quelleRepository.findById(quelle.getId())
                                       .orElseThrow(() -> new BubeEntityNotFoundException(Quelle.class));
        if (!existing.hasSameKey(quelle)) {
            checkQuelleFunctionalKeyExists(quelle);
        }
        return quelleRepository.save(quelle);
    }

    @SecuredStammdatenDelete
    @Transactional
    public void deleteQuelle(Long betriebsstaetteId, Long id) {
        eventPublisher.publishEvent(new QuelleGeloeschtEvent(id));
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
        quelleRepository.deleteById(id);
    }

    @SecuredStammdatenDelete
    @Transactional
    public void deleteMultipleQuellen(Long betriebsstaetteId, List<Long> ids) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
        quelleRepository.deleteAllByIdIn(ids);
    }

    /**
     * Anlage
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<Anlage> loadAllAnlagenByBetriebsstaettenId(Long betriebsstaetteId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));

        return anlageRepository.findByParentBetriebsstaetteId(betriebsstaetteId);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenWrite
    public Anlage checkAnlageDatenberechtigungWrite(Long anlageId) {
        return loadAnlage(anlageId);
    }

    @SecuredStammdatenWrite
    public Anlage createAnlage(Anlage anlage) {
        Assert.isTrue(anlage.isNew(), "Anlage bereits vorhanden");
        var betriebsstaette = loadBetriebsstaetteForWrite(anlage.getParentBetriebsstaetteId());
        checkAnlageFunctionalKeyExists(anlage);
        if (anlage.getLocalId() != null) {
            checkLocalIdAlreadyExists(anlage.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }
        checkValidVorschriftenTaetigkeiten(anlage.getVorschriften());

        // Hier wird Flush benötigt, damit das Event die gespeicherte Anlage auch laden kann (z.B. über die ParentBST)
        Anlage a = anlageRepository.saveAndFlush(anlage);

        eventPublisher.publishEvent(new AnlageAngelegtEvent(a));

        return a;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlage loadAnlage(Long id) {
        Anlage anlage = anlageRepository.findById(id)
                                        .orElseThrow(() -> new BubeEntityNotFoundException(Anlage.class, "ID " + id));
        checkBetriebsstaetteDatenberechtigung(anlage.getParentBetriebsstaetteId(),
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return anlage;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlage loadAnlageByAnlageNummer(Long betriebsstaettenId, String anlagenNummer) {
        return this.findAnlage(betriebsstaettenId, anlagenNummer)
                   .orElseThrow(() -> new BubeEntityNotFoundException(Anlage.class, "Nummer " + anlagenNummer));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Anlage> findAnlage(Long betriebsstaettenId, String anlagenNummer) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaettenId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return anlageRepository.findByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(betriebsstaettenId, anlagenNummer);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Anlage> findAnlageByLocalId(Long betriebsstaettenId, String localId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaettenId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return anlageRepository.findByParentBetriebsstaetteIdAndLocalId(betriebsstaettenId, localId);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlage readAnlageByLocalId(Long betriebsstaettenId, String localId) {
        return findAnlageByLocalId(betriebsstaettenId, localId).orElseThrow(
                () -> new BubeEntityNotFoundException(Anlage.class, "Local-ID " + localId));
    }

    @SecuredStammdatenWrite
    public Anlage updateAnlage(Anlage anlage) {
        Assert.isTrue(!anlage.isNew(), "Anlage nicht vorhanden");
        var betriebsstaette = loadBetriebsstaetteForWrite(anlage.getParentBetriebsstaetteId());
        var existing = loadAnlage(anlage.getId());

        if (!existing.hasSameKey(anlage)) {
            checkAnlageFunctionalKeyExists(anlage);
        }

        if (!existing.hasSameLocalIdOrNull(anlage)) {
            checkLocalIdAlreadyExists(anlage.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }
        checkValidVorschriftenTaetigkeiten(anlage.getVorschriften());

        anlagenUpdater.update(existing, anlage);
        Anlage a = anlageRepository.save(existing);

        eventPublisher.publishEvent(new AnlageBearbeitetEvent(a));

        return a;
    }

    @SecuredStammdatenWrite
    public Anlage updateAnlageForUebernahme(Anlage anlage) {
        Assert.isTrue(!anlage.isNew(), "Anlage nicht vorhanden");
        var betriebsstaette = loadBetriebsstaetteForWrite(anlage.getParentBetriebsstaetteId());
        var existing = loadAnlage(anlage.getId());

        if (!existing.hasSameKey(anlage)) {
            checkAnlageFunctionalKeyExists(anlage);
        }

        if (!existing.hasSameLocalIdOrNull(anlage)) {
            checkLocalIdAlreadyExists(anlage.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }
        checkValidVorschriftenTaetigkeiten(anlage.getVorschriften());

        Anlage a = anlageRepository.save(anlage);
        eventPublisher.publishEvent(new AnlageBearbeitetEvent(a));

        return a;
    }

    @SecuredStammdatenDelete
    public void deleteAnlage(Long id) {        // Zur Prüfung der Datenberechtigungen
        var anlage = loadAnlage(id);
        checkBetriebsstaetteDatenberechtigung(anlage.getParentBetriebsstaetteId(),
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));

        anlage.getQuellen().forEach(quelle -> this.deleteQuelle(anlage.getParentBetriebsstaetteId(), quelle.getId()));

        anlage.getAnlagenteile()
              .forEach(anlagenteil -> eventPublisher.publishEvent(
                      new AnlagenteilGeloeschtEvent(anlagenteil.getId(), anlage.getParentBetriebsstaetteId())));
        eventPublisher.publishEvent(new AnlageGeloeschtEvent(anlage.getId(), anlage.getParentBetriebsstaetteId()));
        anlageRepository.deleteById(id);
        // Das Flush wird benötigt, damit das Löschen und die nachfolgende Event Verarbeitung funktionieren
        anlageRepository.flush();

        //Wird nur für die Anlage aufgerufen. Dies ist Absicht um die Performance zu verbessern.
        eventPublisher.publishEvent(new AnlageWurdeGeloeschtEvent(anlage.getId(), anlage.getParentBetriebsstaetteId()));
    }

    // Das Nutzen von deleteAnlage löscht nur die erste Anlage in der Liste,
    // wahrschienlich aufgrund der vielen Events die geworfen werden im Zusammenhang mit Transactional
    @SecuredStammdatenDelete
    @Transactional
    public void deleteMultipleAnlagen(List<Long> ids) {
        var anlagen = new ArrayList<Anlage>();
        ids.forEach(id -> {
            var anlage = loadAnlage(id);

            checkBetriebsstaetteDatenberechtigung(anlage.getParentBetriebsstaetteId(),
                    stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));

            anlage.getQuellen()
                  .forEach(quelle -> this.deleteQuelle(anlage.getParentBetriebsstaetteId(), quelle.getId()));

            anlage.getAnlagenteile()
                  .forEach(anlagenteil -> eventPublisher.publishEvent(
                          new AnlagenteilGeloeschtEvent(anlagenteil.getId(), anlage.getParentBetriebsstaetteId())));
            eventPublisher.publishEvent(new AnlageGeloeschtEvent(anlage.getId(), anlage.getParentBetriebsstaetteId()));

            anlagen.add(anlage);
        });

        anlageRepository.deleteAllById(ids);
        anlageRepository.flush();

        anlagen.forEach(anlage -> eventPublisher.publishEvent(
                new AnlageWurdeGeloeschtEvent(anlage.getId(), anlage.getParentBetriebsstaetteId())));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canReadAnlage(Long anlageId) {
        var anlage = anlageRepository.findById(anlageId)
                                     .orElseThrow(
                                             () -> new BubeEntityNotFoundException(Anlage.class, "ID " + anlageId));
        return canReadBetriebsstaette(anlage.getParentBetriebsstaetteId());
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public boolean canWriteAnlage(Long anlageId) {
        var anlage = anlageRepository.findById(anlageId)
                                     .orElseThrow(
                                             () -> new BubeEntityNotFoundException(Anlage.class, "ID " + anlageId));
        return canWriteBetriebsstaette(anlage.getParentBetriebsstaetteId());
    }

    /**
     * Anlagenteil
     */
    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlagenteil loadAnlagenteil(Long id) {
        Anlagenteil anlagenteil = anlagenteilRepository
                .findById(id)
                .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class, "ID " + id));
        loadAnlage(anlagenteil.getParentAnlageId());
        return anlagenteil;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Optional<Anlagenteil> findAnlagenteilByLocalId(Long anlageId, Long betriebsstaetteId, String localId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return anlagenteilRepository.findByParentAnlageIdAndLocalId(anlageId, localId);
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlagenteil readAnlagenteilByLocalId(Long betriebsstaetteId, String localId) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
        return anlagenteilRepository.findByLocalId(localId)
                                    .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class));
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public List<Anlagenteil> loadAllAnlagenteileByAnlageId(Long anlageId, Long betriebsstaetteId) {
        // Zur Prüfung der Datenberechtigungen
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));

        return anlagenteilRepository.findByParentAnlageId(anlageId);
    }

    @SecuredStammdatenWrite
    public Anlagenteil createAnlagenteil(Long betriebsstaetteId, Anlagenteil anlagenteil) {
        Assert.isTrue(anlagenteil.isNew(), "Anlagenteil bereits vorhanden");
        var betriebsstaette = loadBetriebsstaetteForWrite(betriebsstaetteId);
        checkAnlagenteilFunctionalKeyExists(anlagenteil);
        if (anlagenteil.getLocalId() != null) {
            checkLocalIdAlreadyExists(anlagenteil.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }
        checkValidVorschriftenTaetigkeiten(anlagenteil.getVorschriften());
        // Hier wird Flush benötigt, damit das Event das Anlagenteil auch laden kann (z.B. über die ParentBST)
        Anlagenteil a = anlagenteilRepository.saveAndFlush(anlagenteil);

        eventPublisher.publishEvent(new AnlagenteilAngelegtEvent(a));

        return a;
    }

    @Transactional(readOnly = true)
    @SecuredStammdatenRead
    public Anlagenteil loadAnlagenteilByAnlagenteilNummer(Long betriebsstaetteId, Long parentAnlageId,
            String anlagenteilNummer) {
        checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);

        return anlagenteilRepository.findByParentAnlageIdAndAnlagenteilNrIgnoreCase(parentAnlageId, anlagenteilNummer)
                                    .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class));
    }

    @SecuredStammdatenWrite
    public Anlagenteil updateAnlagenteil(Long betriebsstaetteId, Anlagenteil anlagenteil) {
        Assert.isTrue(!anlagenteil.isNew(), "Anlagenteil nicht vorhanden");
        var betriebsstaette = loadBetriebsstaetteForWrite(betriebsstaetteId);
        var existing = anlagenteilRepository.findById(anlagenteil.getId())
                                            .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class));

        if (!existing.hasSameKey(anlagenteil)) {
            checkAnlagenteilFunctionalKeyExists(anlagenteil);
        }

        if (!existing.hasSameLocalIdOrNull(anlagenteil)) {
            checkLocalIdAlreadyExists(anlagenteil.getLocalId(), betriebsstaette.getLand().getNr(),
                    betriebsstaette.getBerichtsjahr().getId());
        }
        checkValidVorschriftenTaetigkeiten(anlagenteil.getVorschriften());

        Anlagenteil a = anlagenteilRepository.save(anlagenteil);

        eventPublisher.publishEvent(new AnlagenteilBearbeitetEvent(a));

        return a;
    }

    @SecuredStammdatenDelete
    public void deleteAnlagenteil(Long betriebsstaetteId, Long id) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
        eventPublisher.publishEvent(new AnlagenteilGeloeschtEvent(id, betriebsstaetteId));
        anlagenteilRepository.deleteById(id);
        // Das Flush wird benötigt, damit das Löschen und die nachfolgende Event Verarbeitung funktionieren
        anlagenteilRepository.flush();
        eventPublisher.publishEvent(new AnlagenteilWurdeGeloeschtEvent(id, betriebsstaetteId));
    }

    @SecuredStammdatenDelete
    public void deleteMultipleAnlagenteile(Long betriebsstaetteId, List<Long> ids) {
        checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
        ids.forEach(anlagenteilId -> eventPublisher.publishEvent(
                new AnlagenteilGeloeschtEvent(anlagenteilId, betriebsstaetteId)));
        anlagenteilRepository.deleteAllByIdIn(ids);
        anlagenteilRepository.flush();
        ids.forEach(anlagenteilId -> eventPublisher.publishEvent(
                new AnlagenteilWurdeGeloeschtEvent(anlagenteilId, betriebsstaetteId)));
    }

    /**
     * Ansprechpartner
     */
    @SecuredAdmins
    public void deleteAllAnsprechpartner(Referenz berichtsjahr, String username) {
        var event = new AnsprechpartnerLoeschenTriggerdEvent(berichtsjahr, username, aktiverBenutzer.getLand());
        eventPublisher.publishEvent(event);
    }

    /**
     * Betriebsstättendatenberechtigung
     */
    private void checkBetriebsstaetteDatenberechtigung(List<Long> ids, BooleanBuilder predicate) {
        if (betriebsstaetteRepository.count(predicate.and(betriebsstaette.id.in(ids))) != ids.size()) {
            throw new BubeEntityNotFoundException(Betriebsstaette.class);
        }
    }

    private Betriebsstaette checkBetriebsstaetteDatenberechtigung(Long betriebsstaetteId, BooleanBuilder predicate) {
        return this.betriebsstaetteRepository.findOne(predicate.and(betriebsstaette.id.eq(betriebsstaetteId)))
                                             .orElseThrow(() -> new BubeEntityNotFoundException(Betriebsstaette.class,
                                                     "ID " + betriebsstaetteId));
    }

    public Betriebsstaette checkBetriebsstaetteDatenberechtigungRead(Long betriebsstaetteId) {
        return checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateRead(betriebsstaetteBerechtigungen));
    }

    public Betriebsstaette checkBetriebsstaetteDatenberechtigungWrite(Long betriebsstaetteId) {
        return checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateWrite(betriebsstaetteBerechtigungen));
    }

    public Betriebsstaette checkBetriebsstaetteDatenberechtigungDelete(Long betriebsstaetteId) {
        return checkBetriebsstaetteDatenberechtigung(betriebsstaetteId,
                stammdatenBerechtigungsContext.createPredicateDelete(betriebsstaetteBerechtigungen));
    }

    private void checkBetreiberDatenberechtigung(Long id, BooleanBuilder predicate) {
        if (!betreiberRepository.exists(predicate.and(betreiber.id.eq(id)))) {
            throw new BubeEntityNotFoundException(Betreiber.class, "ID " + id);
        }
    }

    private void checkBetriebsstaetteFunctionalKeyExists(Betriebsstaette betriebsstaette) {
        if (this.betriebsstaetteRepository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(
                betriebsstaette.getBerichtsjahr().getId(), betriebsstaette.getLand(),
                betriebsstaette.getBetriebsstaetteNr())) {
            throw new BetriebsstaetteDuplicateKeyException(betriebsstaette.getBerichtsjahr().getKtext(),
                    betriebsstaette.getBetriebsstaetteNr());
        }
    }

    private void checkQuelleFunctionalKeyExists(Quelle quelle) {
        if (quelle.istKindVonAnlage()) {
            checkAnlageQuelleFunctionalKeyExists(quelle.getParentAnlageId(), quelle.getQuelleNr(), null);
        }
        if (quelle.istKindVonBetriebsstaette()) {
            checkBstQuelleFunctionalKeyExists(quelle.getParentBetriebsstaetteId(), quelle.getQuelleNr(), null);
        }
    }

    public void checkAnlageQuelleFunctionalKeyExists(Long parentAnlageId, String quelleNr, Long exceptedQuelleId) {
        boolean exists = exceptedQuelleId != null ?
                quelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCaseAndIdNot(parentAnlageId, quelleNr,
                        exceptedQuelleId) :
                quelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCase(parentAnlageId, quelleNr);
        if (exists) {
            throw QuelleDuplicateKeyException.forQuelleNr(quelleNr);
        }
    }

    public void checkBstQuelleFunctionalKeyExists(Long parentBetriebsstaetteId, String quelleNr,
            Long exceptedQuelleId) {
        boolean exists = exceptedQuelleId != null ?
                quelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCaseAndIdNot(parentBetriebsstaetteId,
                        quelleNr, exceptedQuelleId) :
                quelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(parentBetriebsstaetteId,
                        quelleNr);
        if (exists) {
            throw QuelleDuplicateKeyException.forQuelleNr(quelleNr);
        }
    }

    private void checkAnlageFunctionalKeyExists(Anlage anlage) {
        Long parentBetriebsstaetteId = anlage.getParentBetriebsstaetteId();
        String anlageNr = anlage.getAnlageNr();
        checkAnlageFunctionalKeyExists(parentBetriebsstaetteId, anlageNr, null);
    }

    public void checkAnlageFunctionalKeyExists(Long parentBetriebsstaetteId, String anlageNr, Long exceptedAnlageId) {
        boolean exists = exceptedAnlageId != null ?
                anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCaseAndIdNot(
                        parentBetriebsstaetteId, anlageNr, exceptedAnlageId) :
                anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(
                        parentBetriebsstaetteId, anlageNr);
        if (exists) {
            throw AnlageDuplicateKeyException.forAnlageNr(anlageNr);
        }
    }

    private void checkAnlagenteilFunctionalKeyExists(Anlagenteil anlagenteil) {
        Long parentAnlageId = anlagenteil.getParentAnlageId();
        String anlagenteilNr = anlagenteil.getAnlagenteilNr();
        checkAnlagenteilFunctionalKeyExists(parentAnlageId, anlagenteilNr, null);
    }

    public void checkAnlagenteilFunctionalKeyExists(Long parentAnlageId, String anlagenteilNr,
            Long exceptedAnlagenteilId) {
        boolean exists = exceptedAnlagenteilId != null ?
                this.anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCaseAndIdNot(parentAnlageId,
                        anlagenteilNr, exceptedAnlagenteilId) :
                this.anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(parentAnlageId,
                        anlagenteilNr);
        if (exists) {
            throw AnlagenteilDuplicateKeyException.forAnlagenteilNr(anlagenteilNr);
        }
    }

    private void checkLocalIdAlreadyExists(String localId, String landSchluessel, Long berichtsjahrId) {
        if (betriebsstaetteRepository.localIdExists(localId, landSchluessel, berichtsjahrId)) {
            throw new LocalIdBereitsVerwendetException(localId);
        }
    }

    private void checkValidVorschriftenTaetigkeiten(List<Vorschrift> vorschriften) {
        if (vorschriften == null) {
            return;
        }

        vorschriften.stream().filter(Vorschrift::hasTaetigkeiten).forEach(vorschrift -> {
            if (vorschrift.invalidTaetigkeiten()) {
                throw new InvalidTaetigkeitException(vorschrift.getArt().getSchluessel());
            }
        });
    }

    /**
     * Änderungen durch Betreiber
     */
    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void aenderungBetriebsstaetteDurchBetreiber(Long betriebsstaetteId) {
        checkBetriebsstaetteDatenberechtigungRead(betriebsstaetteId);
        betriebsstaetteRepository.aenderungDurchBetreiber(betriebsstaetteId);
    }

    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void aenderungAnlageDurchBetreiber(Long anlageId) {
        if (!canReadAnlage(anlageId)) {
            throw new BubeEntityNotFoundException(Anlage.class, "ID " + anlageId);
        }
        anlageRepository.aenderungDurchBetreiber(anlageId);
    }

    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void aenderungAnlagenteilDurchBetreiber(Long anlagenteilId) {
        anlagenteilRepository
                .findById(anlagenteilId)
                .map(Anlagenteil::getParentAnlageId)
                .filter(this::canReadAnlage)
                .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class, "ID " + anlagenteilId));

        anlagenteilRepository.aenderungDurchBetreiber(anlagenteilId);
    }

    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void aenderungQuelleDurchBetreiber(Long quelleId) {
        if (!canReadQuelle(quelleId)) {
            throw new BubeEntityNotFoundException(Quelle.class, "ID " + quelleId);
        }
        quelleRepository.aenderungDurchBetreiber(quelleId);
    }

    @Secured({ BETREIBER, BEHOERDE_WRITE, LAND_WRITE })
    public void aenderungBetreiberDurchBetreiber(Long betreiberId) {
        if (!canReadBetreiber(betreiberId)) {
            throw new BubeEntityNotFoundException(Betreiber.class, "ID " + betreiberId);
        }
        betreiberRepository.aenderungDurchBetreiber(betreiberId);
    }

    @Secured({ BEHOERDE_WRITE, LAND_WRITE })
    public Betriebsstaette updateStammdatenObjekteForAbschlussUebernahme(Betriebsstaette betriebsstaette,
            List<Anlage> anlagen, List<Quelle> quellen, List<Anlagenteil> anlagenteile, Betreiber betreiber) {

        quellen.forEach(quelle -> {
            quelle.setLetzteAenderungBetreiber(null);
            quelleRepository.save(quelle);
        });

        anlagenteile.forEach(anlagenteil -> {
            anlagenteil.setLetzteAenderungBetreiber(null);
            anlagenteilRepository.save(anlagenteil);
        });

        anlagen.forEach(anlage -> {
            anlage.setLetzteAenderungBetreiber(null);
            anlageRepository.save(anlage);
        });

        if (betreiber != null) {
            betreiber.setLetzteAenderungBetreiber(null);
            betreiberRepository.save(betreiber);
        }

        betriebsstaette.setLetzteAenderungBetreiber(null);
        return betriebsstaetteRepository.save(betriebsstaette);

    }

    @EventListener
    public void handleBehoerdeGeloescht(BehoerdeGeloeschtEvent event) {
        if (behoerdeUsed(event.behoerde().getId())) {
            throw new ReferenzdatenException("Die Behörde wird noch verwendet und kann nicht gelöscht werden.");
        }
    }
}
