package de.wps.bube.basis.stammdaten.domain.service;

public interface StammdatenJob {

    String PROTOKOLL_OPENING = "Bitte Protokoll bis zum Ende durchlesen, um das Ergebnis der Übertragung bzw. Nicht-Übertragung von bestehenden Betriebsstätten zu erfahren.";

}
