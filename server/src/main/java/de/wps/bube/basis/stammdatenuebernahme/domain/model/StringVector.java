package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

public class StringVector {
    private String spl;
    private String spb;
    private boolean equal;

    public StringVector(String spl, String spb) {
        this.spb = spb;
        this.spl = spl;
        this.equal = Objects.equal(spl, spb);
    }

    public String getSpl() {
        return spl;
    }

    public void setSpl(String spl) {
        this.spl = spl;
    }

    public String getSpb() {
        return spb;
    }

    public void setSpb(String spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
