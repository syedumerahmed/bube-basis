package de.wps.bube.basis.koordinaten.domain.entity;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.locationtech.jts.geom.MultiPolygon;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Entity
public class Gemeindekoordinaten {

    public static final int SRID_UTM32 = 25832;
    private static final String COLUMN_DEFINITION_MULTI_POLYGON = "geometry(MultiPolygon, 25832)";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gemeindekoordinaten_seq")
    @SequenceGenerator(name = "gemeindekoordinaten_seq", allocationSize = 50)
    private Long id;

    // Amtlicher Gemeindeschlüssel
    private String ags;

    // Geografischer Name
    @Column(name = "gen")
    private String name;
    // Bezeichnung der Verwaltungseinheit (Gemeinde, Stadt ...)
    @Column(name = "bez")
    private String verwaltungseinheit;

    @NotNull
    @Column(columnDefinition = COLUMN_DEFINITION_MULTI_POLYGON)
    private MultiPolygon geom;

    @NotNull
    @ManyToOne
    private Referenz berichtsjahr;

    @Deprecated
    protected Gemeindekoordinaten() {
    }

    public Gemeindekoordinaten(String ags, String name, String verwaltungseinheit, MultiPolygon geom, Referenz berichtsjahr) {
        this.ags = ags;
        this.name = name;
        this.verwaltungseinheit = verwaltungseinheit;
        this.geom = geom;
        this.berichtsjahr = berichtsjahr;
    }

    public String getAgs() {
        return ags;
    }

    public void setAgs(String ags) {
        this.ags = ags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerwaltungseinheit() {
        return verwaltungseinheit;
    }

    public void setVerwaltungseinheit(String verwaltungseinheit) {
        this.verwaltungseinheit = verwaltungseinheit;
    }

    public MultiPolygon getGeom() {
        return geom;
    }

    public void setGeom(MultiPolygon geom) {
        this.geom = geom;
    }

    @Override
    public String toString() {
        return "Gemeindekoordinaten{" +
                "ags='" + ags + '\'' +
                ", name='" + name + '\'' +
                ", verwaltungseinheit='" + verwaltungseinheit + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Gemeindekoordinaten that = (Gemeindekoordinaten) o;
        return ags.equals(that.ags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ags);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Referenz getBerichtsjahr() {
        return berichtsjahr;
    }

    public void setBerichtsjahr(Referenz berichtsjahr) {
        this.berichtsjahr = berichtsjahr;
    }
}
