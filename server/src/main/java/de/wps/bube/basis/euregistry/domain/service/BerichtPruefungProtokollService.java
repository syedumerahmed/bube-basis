package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;
import static java.lang.String.format;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Optional;
import javax.transaction.Transactional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Service
public class BerichtPruefungProtokollService {

    private final EURegistryService eURegistryService;
    private final StammdatenService stammdatenService;
    private final KomplexpruefungService komplexpruefungService;

    public BerichtPruefungProtokollService(EURegistryService eURegistryService, StammdatenService stammdatenService,
            KomplexpruefungService komplexpruefungService) {
        this.eURegistryService = eURegistryService;
        this.stammdatenService = stammdatenService;
        this.komplexpruefungService = komplexpruefungService;
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional
    public void protokolliere(ByteArrayOutputStream outputStream, Long betriebsstaetteId) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);

            Optional<EURegBetriebsstaette> berichtOpt = eURegistryService.findBetriebsstaettenBerichtByBstId(
                    betriebsstaetteId);
            if (berichtOpt.isEmpty()) {
                out.write(format("Der Bericht für die Betriebsstätte %s wurde nicht gefunden",
                        bst.getBetriebsstaetteNr()));
            } else {
                EURegBetriebsstaette bericht = berichtOpt.get();
                Komplexpruefung komplexpruefung = bericht.getKomplexpruefung();
                if (komplexpruefung != null) {
                    BerichtPruefungProtokollGenerator generator = new BerichtPruefungProtokollGenerator(bst, bericht, komplexpruefung,
                            komplexpruefungService, eURegistryService);
                    generator.protokolliereBetriebsstaette(out);
                } else {
                    out.write(format("Die Komplexprüfung für Betriebsstätte %s wurde nicht gefunden",
                            bst.getBetriebsstaetteNr()));
                }
            }
        }

    }

}
