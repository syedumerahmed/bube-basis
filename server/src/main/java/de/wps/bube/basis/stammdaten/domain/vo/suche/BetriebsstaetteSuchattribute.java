package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.List;

public class BetriebsstaetteSuchattribute {
    public String land;
    public String name;
    public String nummer;
    public String localId;
    public String plz;
    public String ort;
    public String akz;
    public List<Long> gemeindeIn;
    public List<Long> behoerdeIn;
    public List<Long> naceIn;
    public List<Long> statusIn;
    public Boolean uebernommen;
}
