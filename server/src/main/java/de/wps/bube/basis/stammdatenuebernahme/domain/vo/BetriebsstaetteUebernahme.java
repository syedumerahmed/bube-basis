package de.wps.bube.basis.stammdatenuebernahme.domain.vo;

public class BetriebsstaetteUebernahme {
    private Long id;
    private Long spbId;
    private AdresseUebernahme adresse;
    private Boolean einleiterNummern;
    private Boolean abfallerzeugerNummern;
    private Boolean flusseinzugsgebiet;
    private Boolean naceCode;
    private Boolean direkteinleiter;
    private Boolean indirekteinleiter;
    private Boolean name;
    private Boolean geoPunkt;
    private Boolean betriebsstatus;
    private Boolean betriebsstatusSeit;
    private Boolean inbetriebnahme;
    private Boolean ansprechpartner;
    private Boolean kommunikationsverbindungen;

    public BetriebsstaetteUebernahme() {
    }

    public BetriebsstaetteUebernahme(Long id, Long spbId, AdresseUebernahme adresse,
            Boolean einleiterNummern, Boolean abfallerzeugerNummern, Boolean flusseinzugsgebiet, Boolean naceCode,
            Boolean direkteinleiter, Boolean indirekteinleiter, Boolean name, Boolean geoPunkt,
            Boolean betriebsstatus, Boolean betriebsstatusSeit, Boolean inbetriebnahme, Boolean ansprechpartner,
            Boolean kommunikationsverbindungen) {
        this.id = id;
        this.spbId = spbId;
        this.adresse = adresse;
        this.einleiterNummern = einleiterNummern;
        this.abfallerzeugerNummern = abfallerzeugerNummern;
        this.flusseinzugsgebiet = flusseinzugsgebiet;
        this.naceCode = naceCode;
        this.direkteinleiter = direkteinleiter;
        this.indirekteinleiter = indirekteinleiter;
        this.name = name;
        this.geoPunkt = geoPunkt;
        this.betriebsstatus = betriebsstatus;
        this.betriebsstatusSeit = betriebsstatusSeit;
        this.inbetriebnahme = inbetriebnahme;
        this.ansprechpartner = ansprechpartner;
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public Boolean getEinleiterNummern() {
        return einleiterNummern;
    }

    public void setEinleiterNummern(Boolean einleiterNummern) {
        this.einleiterNummern = einleiterNummern;
    }

    public Boolean getAbfallerzeugerNummern() {
        return abfallerzeugerNummern;
    }

    public void setAbfallerzeugerNummern(Boolean abfallerzeugerNummern) {
        this.abfallerzeugerNummern = abfallerzeugerNummern;
    }

    public Boolean getFlusseinzugsgebiet() {
        return flusseinzugsgebiet;
    }

    public void setFlusseinzugsgebiet(Boolean flusseinzugsgebiet) {
        this.flusseinzugsgebiet = flusseinzugsgebiet;
    }

    public Boolean getNaceCode() {
        return naceCode;
    }

    public void setNaceCode(Boolean naceCode) {
        this.naceCode = naceCode;
    }

    public Boolean getDirekteinleiter() {
        return direkteinleiter;
    }

    public void setDirekteinleiter(Boolean direkteinleiter) {
        this.direkteinleiter = direkteinleiter;
    }

    public Boolean getIndirekteinleiter() {
        return indirekteinleiter;
    }

    public void setIndirekteinleiter(Boolean indirekteinleiter) {
        this.indirekteinleiter = indirekteinleiter;
    }

    public Boolean getName() {
        return name;
    }

    public void setName(Boolean name) {
        this.name = name;
    }

    public Boolean getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(Boolean geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public Boolean getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Boolean betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public Boolean getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(Boolean betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public Boolean getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(Boolean inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public Boolean getAnsprechpartner() {
        return ansprechpartner;
    }

    public void setAnsprechpartner(Boolean ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
    }

    public Boolean getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    public void setKommunikationsverbindungen(Boolean kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    public AdresseUebernahme getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseUebernahme adresse) {
        this.adresse = adresse;
    }
}
