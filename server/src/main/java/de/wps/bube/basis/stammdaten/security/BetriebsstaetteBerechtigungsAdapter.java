package de.wps.bube.basis.stammdaten.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

public class BetriebsstaetteBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {
    private final Betriebsstaette betriebsstaette;

    public BetriebsstaetteBerechtigungsAdapter(Betriebsstaette betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
    }

    @Override
    public Land getLand() {
        return betriebsstaette.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return Set.of(betriebsstaette.getZustaendigeBehoerde().getSchluessel());
    }

    @Override
    public Set<String> getAkzSet() {
        return betriebsstaette.getAkz() != null ? Set.of(betriebsstaette.getAkz()) : null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return Set.of(betriebsstaette.getGemeindekennziffer().getSchluessel());
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return Set.of(betriebsstaette.getBetriebsstaetteNr());
    }
}
