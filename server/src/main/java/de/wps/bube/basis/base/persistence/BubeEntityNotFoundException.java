package de.wps.bube.basis.base.persistence;

import javax.persistence.EntityNotFoundException;

public class BubeEntityNotFoundException extends EntityNotFoundException {

    public BubeEntityNotFoundException(String message) {
        super(message);
    }

    public BubeEntityNotFoundException(Class<?> entityClass) {
        super(entityClass.getSimpleName() + " wurde nicht gefunden oder keine Berechtigung zum Öffnen des Objektes");
    }

    public BubeEntityNotFoundException(Class<?> entityClass, String key) {
        super(entityClass.getSimpleName() + " mit " + key + " wurde nicht gefunden oder keine Berechtigung zum Öffnen des Objektes");
    }
}
