package de.wps.bube.basis.euregistry.domain.vo;

import de.wps.bube.basis.base.vo.Land;

public class EuRegBetriebsstaetteListItem {

    public Long id;
    public Long bstId;
    public String zustaendigeBehoerde;
    public String betriebsstaetteNr;
    public String name;
    public String localId;
    public String plz;
    public String ort;
    public String betriebsstatus;
    public Land land;
    public boolean pruefungsfehler;
    public boolean pruefungVorhanden;
}
