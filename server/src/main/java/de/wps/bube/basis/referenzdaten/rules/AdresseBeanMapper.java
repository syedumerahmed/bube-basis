package de.wps.bube.basis.referenzdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.rules.bean.AdresseBean;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class AdresseBeanMapper {

    public abstract AdresseBean toBean(Adresse adresse);

}
