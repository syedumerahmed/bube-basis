package de.wps.bube.basis.benutzerverwaltung.web;

import static java.util.function.Predicate.not;

import java.util.Set;
import java.util.TreeSet;

import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.BenutzerDto;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.security.BenutzerBerechtigungsAdapter;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;

@Mapper
public abstract class BenutzerMapper {

    @Autowired
    StammdatenBerechtigungsContext stammdatenBerechtigungsContext;

    @Mapping(target = "canDelete", source = ".", qualifiedByName = "canDelete")
    @Mapping(target = "canWrite", source = ".", qualifiedByName = "canWrite")
    @Mapping(target = "behoerdenSchluessel", ignore = true)
    @Mapping(target = "needsTempPassword", expression = "java(benutzer.needsTempPassword())")
    public abstract BenutzerDto toDto(Benutzer benutzer);

    @Mapping(target = "behoerden", ignore = true)
    @Mapping(target = "akzSet", ignore = true)
    @Mapping(target = "verwaltungsgebiete", ignore = true)
    @Mapping(target = "betriebsstaetten", ignore = true)
    public abstract Benutzer fromDto(BenutzerDto benutzerDto);

    @BeforeMapping
    protected void stripBetriebsstaettenIgnoreCase(BenutzerDto benutzerDto) {
        Set<String> betriebsstaetten = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        benutzerDto.getDatenberechtigung().getBetriebsstaetten().removeIf(not(betriebsstaetten::add));
    }

    static Land landFromDto(final String landNr) {
        return Land.of(landNr);
    }

    static String landToDto(final Land land) {
        return land.getNr();
    }

    @Named("canDelete")
    boolean canDelete(Benutzer benutzer) {
        return stammdatenBerechtigungsContext.hasAccessToDelete(new BenutzerBerechtigungsAdapter(benutzer));
    }

    @Named("canWrite")
    boolean canWrite(Benutzer benutzer) {
        return stammdatenBerechtigungsContext.hasAccessToWrite(new BenutzerBerechtigungsAdapter(benutzer));
    }

}
