package de.wps.bube.basis.stammdaten.domain.event;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class StammdatenUebernehmenTriggeredEvent {

    public static final String EVENT_NAME = "Stammdaten übernehmen";

    public final List<Long> betriebsstaettenIds;
    public final Referenz zieljahr;
    public final String username;

    public StammdatenUebernehmenTriggeredEvent(List<Long> betriebsstaettenIds, Referenz zieljahr, String username) {
        this.betriebsstaettenIds = betriebsstaettenIds;
        this.zieljahr = zieljahr;
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        StammdatenUebernehmenTriggeredEvent that = (StammdatenUebernehmenTriggeredEvent) o;
        return Objects.equals(betriebsstaettenIds, that.betriebsstaettenIds) && Objects.equals(zieljahr,
                that.zieljahr) && Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(betriebsstaettenIds, zieljahr, username);
    }
}
