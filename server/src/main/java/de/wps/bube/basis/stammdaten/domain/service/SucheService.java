package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository.berichtsjahrIdEquals;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenRead;

@Service
@Transactional(readOnly = true)
@SecuredStammdatenRead
public class SucheService {
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final BetriebsstaetteRepository repository;
    private final BetriebsstaettenBerechtigungsAccessor betriebsstaettenBerechtigungsAccessor;
    private final SuchattributePredicateFactory suchattributePredicatefactory;
    private final AktiverBenutzer aktiverBenutzer;

    public SucheService(StammdatenBerechtigungsContext stammdatenBerechtigungsContext, BetriebsstaetteRepository repository,
            BetriebsstaettenBerechtigungsAccessor betriebsstaettenBerechtigungsAccessor,
            SuchattributePredicateFactory suchattributePredicatefactory, AktiverBenutzer aktiverBenutzer) {
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.repository = repository;
        this.betriebsstaettenBerechtigungsAccessor = betriebsstaettenBerechtigungsAccessor;
        this.suchattributePredicatefactory = suchattributePredicatefactory;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    public Page<BetriebsstaetteListItemView> suche(Long berichtsjahr, Suchattribute suchattribute, Pageable page) {
        var predicate = createSuchePredicate(berichtsjahr, suchattribute);

        return repository.findAllAsListItems(predicate, page);
    }

    public List<Long> sucheIds(Long berichtsjahr, Suchattribute suchattribute) {
        var predicate = createSuchePredicate(berichtsjahr, suchattribute);

        return repository.findAllIds(predicate);
    }

    private Predicate createSuchePredicate(Long berichtsjahr, Suchattribute suchattribute) {
        BooleanBuilder predicate = stammdatenBerechtigungsContext.createPredicateRead(betriebsstaettenBerechtigungsAccessor)
                                                                 .and(berichtsjahrIdEquals(berichtsjahr));
        if (aktiverBenutzer.isBehoerdennutzer()) {
            predicate.and(suchattributePredicatefactory.createPredicate(suchattribute, betriebsstaette));
        }
        return predicate;
    }
}
