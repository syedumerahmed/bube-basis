package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;
import static java.lang.String.format;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.XMLExportException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtErstellenCommand;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtException;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.event.XeurTeilberichtErstellenTriggeredEvent;
import de.wps.bube.basis.euregistry.domain.event.XeurTeilberichtFreigegebenEvent;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.euregistry.xml.bericht.EURegGMLWriterProtokoll;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobContext;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelService;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Service
public class XeurTeilberichtService {

    private static final Logger LOGGER = LoggerFactory.getLogger(XeurTeilberichtService.class);

    private final XeurTeilberichtRepository xeurTeilberichtRepository;
    private final EURegistryService euRegistryService;
    private final EURegGMLService euRegGMLService;
    private final BerichtPruefungService berichtPruefungService;
    private final RegelService regelService;
    private final KomplexpruefungService komplexpruefungService;
    private final ApplicationEventPublisher eventPublisher;
    private final ParameterService parameterService;
    private final AktiverBenutzer aktiverBenutzer;
    private final EmailService emailService;
    private final BenutzerverwaltungService benutzerverwaltungService;

    @Value("${bube.mail.adresse.uba}")
    private String ubaMailAdresse;
    @Value("${bube.mail.adresse.from}")
    private String fromMailAdresse;

    public XeurTeilberichtService(
            XeurTeilberichtRepository xeurTeilberichtRepository,
            EURegistryService euRegistryService,
            EURegGMLService euRegGMLService,
            BerichtPruefungService berichtPruefungService,
            RegelService regelService,
            KomplexpruefungService komplexpruefungService,
            ApplicationEventPublisher eventPublisher,
            ParameterService parameterService,
            AktiverBenutzer aktiverBenutzer, EmailService emailService,
            BenutzerverwaltungService benutzerverwaltungService) {
        this.xeurTeilberichtRepository = xeurTeilberichtRepository;
        this.euRegistryService = euRegistryService;
        this.euRegGMLService = euRegGMLService;
        this.berichtPruefungService = berichtPruefungService;
        this.regelService = regelService;
        this.komplexpruefungService = komplexpruefungService;
        this.eventPublisher = eventPublisher;
        this.parameterService = parameterService;
        this.aktiverBenutzer = aktiverBenutzer;
        this.emailService = emailService;
        this.benutzerverwaltungService = benutzerverwaltungService;
    }

    @Secured({ LAND_WRITE })
    @Transactional
    public void erstellXeurTeilbericht(List<Long> berichtIds, Referenz jahr, Land land,
            JobContext<EURegBetriebsstaette> jobContext) {
        LOGGER.info(
                format("Starte Erstellung von XEUR-Teilbericht für Land %s mit %d Berichten", land, berichtIds.size()));
        var jobProtokoll = jobContext.jobProtokoll();

        // XeurTeilbericht instanziieren
        XeurTeilbericht xeurTeilbericht = new XeurTeilbericht(jahr, land);
        LOGGER.info("Teilbericht erfolgreich erstellt");

        // Protokoll anlegen
        TeilberichtProtokoll teilberichtProtokoll = new TeilberichtProtokoll(land.name(), jahr.getSchluessel(),
                komplexpruefungService, euRegistryService);

        // Komplexprüfung starten
        List<Long> fehlerfreieIds = new ArrayList<>(berichtIds);
        List<String> globaleFehler = new ArrayList<>();
        euRegistryService.streamAllByBerichtIdIn(berichtIds).forEach(euRegBetriebsstaette -> {
            jobProtokoll.addEintrag("Starte Komplexprüfung für Bericht zu BST "
                                    + euRegBetriebsstaette.getBetriebsstaette().getBetriebsstaetteNr());
            try {
                Komplexpruefung komplexpruefung = berichtPruefungService.komplexpruefung(euRegBetriebsstaette);
                if (komplexpruefung.hatFehler()) {
                    fehlerfreieIds.remove(euRegBetriebsstaette.getId());
                }
                teilberichtProtokoll.addKomplexpruefung(euRegBetriebsstaette, komplexpruefung);
            } catch (Exception e) {
                LOGGER.error("Fehler bei der Ausführung der Komplexprüfung", e);
                jobProtokoll.addWarnung("Fehler bei der Ausführung der Komplexprüfung: " + e.getMessage());
                globaleFehler.add(e.getMessage());
            }
        });

        // Teilbericht Regeln prüfen
        regelService.getTeilberichtRegeln().forEach(regel -> {
            jobProtokoll.addEintrag("Starte Prüfung der Teilbericht-Regel " + regel.getNummer());
            try {
                List<BerichtPruefungService.TestErgebnis> ergebnisse
                        = berichtPruefungService.pruefeTeilberichtRegeln(regel, xeurTeilbericht);
                teilberichtProtokoll.addTeilberichtPruefungErgebnisse(ergebnisse);
                if (regel.getNummer() == 36) {
                    teilberichtProtokoll.setKomplexpruefungenFehlend(ergebnisse.size());
                }
            } catch (Exception e) {
                LOGGER.error("Fehler bei der Ausführung der Regel-Prüfung", e);
                jobProtokoll.addWarnung("Fehler bei der Ausführung der Regel-Prüfung: " + e.getMessage());
                globaleFehler.add(e.getMessage());
            }
        });

        // Inhalt des Teilberichts erstellen
        erstelleTeilberichtInhalt(fehlerfreieIds, globaleFehler, xeurTeilbericht, teilberichtProtokoll, jobContext);

        xeurTeilbericht.setProtokoll(teilberichtProtokoll.getProtokoll());
        boolean abgabefaehig = teilberichtProtokoll.getStatus() == TeilberichtProtokoll.Status.ABGABEFAEHIG;
        xeurTeilbericht.setAbgabefaehig(abgabefaehig);
        if (abgabefaehig) {
            jobProtokoll.addEintrag("Der XEUR-Teilbericht wurde erfolgreich erstellt; Download und Freigabe unter \"XEUR Berichte\"");
        } else  {
            jobProtokoll.addWarnung("Der XEUR-Teilbericht wurde erstellt, ist jedoch nicht abgabefähig. Teilbericht-Download unter \"XEUR Berichte\"");
        }
        jobProtokoll.addEintrag("Das Komplexprüfungsprotokoll wurde angelegt oder aktualisiert.");

        //  TODO Metadatei erstellen

        this.xeurTeilberichtRepository.save(xeurTeilbericht);
    }

    private void erstelleTeilberichtInhalt(List<Long> berichtIds, List<String> globaleFehler,
            XeurTeilbericht teilbericht, TeilberichtProtokoll teilberichtProtokoll,
            JobContext<EURegBetriebsstaette> jobContext) {
        LOGGER.info("Starte Erstellung des XML für Teilbericht {} {}", teilbericht.getLand(),
                teilbericht.getJahr().getKtext());
        var jobProtokoll = jobContext.jobProtokoll();

        jobProtokoll.addEintrag("Starte Erstellung des Teilberichts");
        var localIds = new ArrayList<String>();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        // Inspire Parameter aus der Datenbank lesen
        ParameterWert namespace = parameterService.readParameterWert(Parameter.INSP_NS,
                Gueltigkeitsjahr.of(teilbericht.getJahr().getSchluessel()), teilbericht.getLand());
        ParameterWert site = parameterService.readParameterWert(Parameter.INSP_SITE,
                Gueltigkeitsjahr.of(teilbericht.getJahr().getSchluessel()), teilbericht.getLand());
        Optional<ParameterWert> extension = parameterService.findParameterWert(Parameter.INSP_EXTENSION,
                Gueltigkeitsjahr.of(teilbericht.getJahr().getSchluessel()), teilbericht.getLand());

        var inspireParam = new InspireParam(namespace.getWert(), site.getWert(),
                extension.map(ParameterWert::getWert).orElse(null));

        var gmlWriter = euRegGMLService.createWriter(outputStream, teilbericht.getJahr(), inspireParam);
        try {
            EURegGMLWriterProtokoll euRegGMLWriterProtokoll = gmlWriter.writeFeatureStream(
                    euRegistryService.streamAllByBerichtIdIn(berichtIds)
                                     .peek(b -> jobContext.elementConsumer().accept(b))
                                     .peek(b -> localIds.add(b.getLocalId())));
            euRegGMLWriterProtokoll.setGlobalValidation();
            globaleFehler.forEach(euRegGMLWriterProtokoll::addMappingError);
            teilberichtProtokoll.setEURegGMLWriterProtokoll(euRegGMLWriterProtokoll);
        } catch (XMLStreamException | IOException e) {
            jobProtokoll.addFehler("Fehler beim Erstellen des XEUR-Teilberichts", e.getMessage());
            throw new XMLExportException(e);
        }

        teilbericht.setXeurFile(outputStream.toString());
        teilbericht.setLocalIds(localIds);
        LOGGER.info("Teilbericht-Inhalt erfolgreich erstellt");
        LOGGER.debug("Teilbericht XML:\n{}", teilbericht.getXeurFile());
    }

    @Secured({ LAND_WRITE })
    @Transactional
    public void erstelleXeurTeilbericht(XeurTeilberichtErstellenCommand command) {
        // Synchrone Überprüfung: Darf der Nutzer die EuReg sehen/bearbeiten + Land/Jahr korrekt
        euRegistryService.checkDatenberechtigungForBerichtIds(command.berichtsIds, command.land, command.berichtsjahr);

        this.eventPublisher.publishEvent(
                new XeurTeilberichtErstellenTriggeredEvent(command.berichtsIds, command.berichtsjahr, command.land));
    }

    @Secured({ LAND_WRITE })
    @Transactional
    public void teilberichtFreigeben(Long xeurTeilberichtId) {

        Benutzer benutzer = benutzerverwaltungService.loadCurrentBenutzer();
        if (benutzer.getEmail() == null) {
            throw new XeurTeilberichtException(
                    "Der Freigebende hat keine E-Mail. Daher kann der Teilbericht nicht freigegeben werden.");
        }

        XeurTeilbericht teilbericht = xeurTeilberichtRepository.getById(xeurTeilberichtId);
        datenberechtigungPruefen(teilbericht);

        if (teilbericht.isFreigegeben()) {
            throw new XeurTeilberichtException("Der Teilbericht ist bereits freigegeben.");
        }

        if (!teilbericht.isAbgabefaehig()) {
            throw new XeurTeilberichtException(
                    "Der Teilbericht ist nicht abgabefähig und kann daher nicht freigegeben werden.");
        }

        teilbericht.setFreigegebenAt(Instant.now());
        xeurTeilberichtRepository.save(teilbericht);

        String betreff = String.format("Freigabe der EU-Registry-Daten für das Berichtsjahr %s aus %s",
                teilbericht.getJahr().getSchluessel(), teilbericht.getLand());

        String nachricht = String.format(
                "Die EU-Registry-Daten des Landes %s für das Berichtsjahr %s wurden freigegeben. Freigabezeitpunkt ist %s.",
                teilbericht.getLand(), teilbericht.getJahr().getSchluessel(),
                DateTime.atCET(teilbericht.getFreigegebenAt()));
        emailService.sendSimpleMessage(fromMailAdresse, betreff, nachricht, benutzer.getEmail(), ubaMailAdresse);

        this.eventPublisher.publishEvent(new XeurTeilberichtFreigegebenEvent(teilbericht.getJahr()));
    }

    private void datenberechtigungPruefen(XeurTeilbericht teilbericht) {
        if (!aktiverBenutzer.isGesamtadmin() && !teilbericht.getLand().equals(aktiverBenutzer.getLand())) {
            throw new AccessDeniedException("Zugriff auf Teilbericht verweigert");
        }
    }

}
