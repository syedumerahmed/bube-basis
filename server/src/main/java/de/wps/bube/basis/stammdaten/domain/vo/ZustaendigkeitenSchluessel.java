package de.wps.bube.basis.stammdaten.domain.vo;

import java.util.stream.Stream;

public enum ZustaendigkeitenSchluessel {
    GEN_BEHOERDE("04"), UEBERW_BEHOERDE("05");

    private final String schluessel;

    ZustaendigkeitenSchluessel(String schluessel) {
        this.schluessel = schluessel;
    }

    public String getSchluessel() {
        return schluessel;
    }

    public static boolean schluesselExist(String schluessel) {
        return Stream.of(values()).map(ZustaendigkeitenSchluessel::getSchluessel).anyMatch(schluessel::equals);
    }

    public static ZustaendigkeitenSchluessel ofSchluessel(String schluessel) {
        return Stream.of(values())
                     .filter(v -> v.schluessel.equals(schluessel))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Zuständigkeitenschlüssel: " + schluessel));
    }
}
