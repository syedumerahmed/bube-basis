package de.wps.bube.basis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;

@SpringBootApplication
@EnableJpaRepositories(repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
public class BasisApplication {

    public static void main(final String[] args) {
        SpringApplication.run(BasisApplication.class, args);
    }

}
