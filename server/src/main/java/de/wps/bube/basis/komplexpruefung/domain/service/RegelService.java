package de.wps.bube.basis.komplexpruefung.domain.service;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.komplexpruefung.domain.RegelException;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Service
public class RegelService {

    private final RegelRepository repository;
    private final AktiverBenutzer aktiverBenutzer;

    public RegelService(RegelRepository repository, AktiverBenutzer aktiverBenutzer) {
        this.repository = repository;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Transactional(readOnly = true)
    @Secured(Rollen.LAND_READ)
    public List<Regel> readAllRegeln() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Secured(Rollen.LAND_READ)
    public Regel loadRegelByNummer(Long nummer) {
        Assert.notNull(nummer, "nummer == null");
        return repository.findByNummer(nummer).orElseThrow(() -> new BubeEntityNotFoundException(Regel.class, "Nummer " + nummer));
    }

    @Transactional
    @Secured(Rollen.LAND_WRITE)
    public Regel createRegel(Regel regel) {
        checkBerechtigungWrite(regel);
        regel.pruefeGueltigkeit();
        regel.pruefeObjekteInGruppe();
        regel.pruefeTypBundeseinheitlich();
        return repository.save(regel);
    }

    @Transactional
    @Secured(Rollen.LAND_WRITE)
    public Regel updateRegel(Regel regel) {
        checkBerechtigungWrite(regel);
        regel.pruefeGueltigkeit();
        regel.pruefeObjekteInGruppe();
        regel.pruefeTypBundeseinheitlich();
        return repository.save(regel);
    }

    @Transactional
    @Secured(Rollen.LAND_DELETE)
    public void deleteRegel(Long nummer) {
        Regel regel = repository.findByNummer(nummer)
                                .orElseThrow(() -> new BubeEntityNotFoundException(Regel.class, "Nummer " + nummer));
        checkBerechtigungDelete(regel);
        repository.delete(regel);
    }

    private void checkBerechtigungWrite(Regel regel) {
        if (!regel.hatDatenberechtigung(aktiverBenutzer.isGesamtadmin(), aktiverBenutzer.getLand(),
                aktiverBenutzer.hatRolle(Rolle.LAND_WRITE))) {
            throw new RegelException("Keine Datenberechtigung für Schreiben der Regel");
        }
    }

    private void checkBerechtigungDelete(Regel regel) {
        if (!regel.hatDatenberechtigung(aktiverBenutzer.isGesamtadmin(), aktiverBenutzer.getLand(),
                aktiverBenutzer.hatRolle(Rolle.LAND_DELETE))) {
            throw new RegelException("Keine Datenberechtigung für Löschen der Regel");
        }
    }

    @Transactional
    @Secured(Rollen.ADMIN)
    public void typAendern(List<Long> regeln, RegelTyp typ) {
        regeln.forEach(nummer -> {
            Regel regel = loadRegelByNummer(nummer);
            regel.aendereTyp(aktiverBenutzer.getLand(), typ);
            repository.save(regel);
        });
    }

    public Stream<Regel> getTeilberichtRegeln() {
        return repository.findByObjekteContaining(RegelObjekt.TEILBERICHT);
    }
}
