package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

import de.wps.bube.basis.stammdaten.domain.entity.Leistung;

public class LeistungVector {
    private Leistung spl;
    private Leistung spb;
    private boolean equal;

    public LeistungVector(Leistung spl, Leistung spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public Leistung getSpl() {
        return spl;
    }

    public void setSpl(Leistung spl) {
        this.spl = spl;
    }

    public Leistung getSpb() {
        return spb;
    }

    public void setSpb(Leistung spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
