package de.wps.bube.basis.base.vo;

import java.util.Objects;

public class Validierungshinweis {

    public enum Severity {WARNUNG, FEHLER}

    public final String hinweis;
    public final Severity severity;

    private Validierungshinweis(String hinweis, Severity severity) {
        this.hinweis = hinweis;
        this.severity = severity;
    }

    public static Validierungshinweis of(String hinweis, Severity severity) {
        return new Validierungshinweis(hinweis, severity);
    }

    public static Validierungshinweis benutzerDoppeltFehler(String username) {
        return new Validierungshinweis("Der Benutzername '" + username + "' kommt mehrfach in der Datei vor.",
                Severity.FEHLER);
    }

    public static Validierungshinweis benutzerExistiertHinweis(String username) {
        return new Validierungshinweis(
                "Der Benutzer '" + username + "' existiert bereits und wird beim Importieren überschrieben.",
                Severity.WARNUNG);
    }

    public static Validierungshinweis benutzerExistiertFehler(String username) {
        return new Validierungshinweis(
                "Der Benutzer '" + username + "' existiert bereits und kann nicht überschrieben werden.",
                Severity.FEHLER);
    }

    public static Validierungshinweis betriebsstaetteNichtVorhandenFehler(String username, String nummer) {
        return new Validierungshinweis(
                "Die Betriebsstätte '" + nummer + "' existiert nicht oder darf dem Benutzer '" + username + "' nicht zugewiesen werden.",
                Severity.FEHLER);
    }

    public boolean isError() {
        return severity == Severity.FEHLER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Validierungshinweis that = (Validierungshinweis) o;
        return hinweis.equals(that.hinweis) && severity == that.severity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hinweis, severity);
    }

    @Override
    public String toString() {
        return String.format("Validierungshinweis{%s: %s}", severity, hinweis);
    }

}
