package de.wps.bube.basis.stammdaten.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;

public interface AnlageRepository extends JpaRepository<Anlage, Long>, QuerydslPredicateExecutor<Anlage> {

    Optional<Anlage> findByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(Long betriebsstaetteId, String anlagenNummer);

    Optional<Anlage> findByParentBetriebsstaetteIdAndLocalId(Long betriebsstaetteId, String localId);

    boolean existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(Long parentBetriebsstaetteId, String anlageNr);

    boolean existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCaseAndIdNot(Long parentBetriebsstaetteId, String anlageNr,
            Long exceptedAnlageId);

    List<Anlage> findByParentBetriebsstaetteId(Long betriebsstaetteId);

    @Modifying
    @Query("""
            update Anlage a
                     set a.letzteAenderungBetreiber  = current_timestamp,
                         a.betreiberdatenUebernommen = false
                     where a.id = :anlageId""")
    void aenderungDurchBetreiber(Long anlageId);

}
