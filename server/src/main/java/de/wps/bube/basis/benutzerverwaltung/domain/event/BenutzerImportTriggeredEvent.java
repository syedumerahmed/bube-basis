package de.wps.bube.basis.benutzerverwaltung.domain.event;

public class BenutzerImportTriggeredEvent {

    public static final String EVENT_NAME = "Benutzer importieren";

    public final String username;

    public BenutzerImportTriggeredEvent(String username) {
        this.username = username;
    }
}
