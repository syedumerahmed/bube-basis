package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Berichtsobjekt implements Comparable<Berichtsobjekt> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "berichtsobjekt_seq")
    @SequenceGenerator(name = "berichtsobjekt_seq", allocationSize = 50)
    private Long id;

    @NotNull
    private Long betriebsstaetteId;

    @NotNull
    @ManyToOne
    private Referenz berichtsart;

    @NotNull
    @ManyToOne
    private Referenz bearbeitungsstatus;

    @NotNull
    private LocalDate bearbeitungsstatusSeit;

    private String url;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected Berichtsobjekt() {
    }

    public Berichtsobjekt(Long betriebsstaetteId, Referenz berichtsart, Referenz bearbeitungsstatus,
            LocalDate bearbeitungsstatusSeit, String url) {
        this.betriebsstaetteId = betriebsstaetteId;
        this.berichtsart = berichtsart;
        this.bearbeitungsstatus = bearbeitungsstatus;
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBetriebsstaetteId() {
        return betriebsstaetteId;
    }

    public void setBetriebsstaetteId(Long betriebsstaetteId) {
        this.betriebsstaetteId = betriebsstaetteId;
    }

    public Referenz getBerichtsart() {
        return berichtsart;
    }

    public void setBerichtsart(Referenz berichtsart) {
        this.berichtsart = berichtsart;
    }

    public Referenz getBearbeitungsstatus() {
        return bearbeitungsstatus;
    }

    public void setBearbeitungsstatus(Referenz bearbeitungsstatus) {
        this.bearbeitungsstatus = bearbeitungsstatus;
    }

    public LocalDate getBearbeitungsstatusSeit() {
        return bearbeitungsstatusSeit;
    }

    public void setBearbeitungsstatusSeit(LocalDate bearbeitungsstatusSeit) {
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    @Override
    public int compareTo(Berichtsobjekt that) {
        return Integer.parseInt(this.berichtsart.getSortier()) - Integer.parseInt(that.berichtsart.getSortier());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Berichtsobjekt that = (Berichtsobjekt) o;
        return Objects.equals(id, that.id)
                && Objects.equals(betriebsstaetteId, that.betriebsstaetteId)
                && Objects.equals(berichtsart, that.berichtsart)
                && Objects.equals(bearbeitungsstatus, that.bearbeitungsstatus)
                && Objects.equals(bearbeitungsstatusSeit, that.bearbeitungsstatusSeit)
                && Objects.equals(url, that.url)
                && Objects.equals(ersteErfassung, that.ersteErfassung)
                && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betriebsstaetteId, berichtsart, bearbeitungsstatus, bearbeitungsstatusSeit, url,
                ersteErfassung, letzteAenderung);
    }

    @Override
    public String toString() {
        return "Berichtsobjekt{" +
                "id=" + id +
                ", betriebsstaetteId=" + betriebsstaetteId +
                ", berichtsart=" + berichtsart +
                ", bearbeitungsstatus=" + bearbeitungsstatus +
                ", bearbeitungsstatusSeit=" + bearbeitungsstatusSeit +
                ", url='" + url + '\'' +
                ", ersteErfassung=" + ersteErfassung +
                ", letzteAenderung=" + letzteAenderung +
                '}';
    }
}
