package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;

public class AnlageBearbeitetEvent {
    public static final String EVENT_NAME = "Anlage bearbeitet";

    public final Anlage anlage;

    public AnlageBearbeitetEvent(Anlage a) {
        this.anlage = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AnlageBearbeitetEvent that = (AnlageBearbeitetEvent) o;
        return Objects.equals(anlage, that.anlage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anlage);
    }
}
