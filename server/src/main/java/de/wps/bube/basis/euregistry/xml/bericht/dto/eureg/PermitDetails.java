package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;

@XmlAccessorType(XmlAccessType.FIELD)
public class PermitDetails {
    @XmlElement(required = true, name = "PermitDetails")
    private final PermitDetailsType data = new PermitDetailsType();

    public Boolean getPermitGranted() {
        return data.permitGranted;
    }

    public void setPermitGranted(Boolean permitGranted) {
        data.permitGranted = permitGranted;
    }

    public Boolean getPermitReconsidered() {
        return data.permitReconsidered;
    }

    public void setPermitReconsidered(Boolean permitReconsidered) {
        data.permitReconsidered = permitReconsidered;
    }

    public Boolean getPermitUpdated() {
        return data.permitUpdated;
    }

    public void setPermitUpdated(Boolean permitUpdated) {
        data.permitUpdated = permitUpdated;
    }

    public String getDateOfGranting() {
        return data.dateOfGranting;
    }

    public void setDateOfGranting(String dateOfGranting) {
        data.dateOfGranting = dateOfGranting;
    }

    public String getDateOfLastUpdate() {
        return data.dateOfLastUpdate;
    }

    public void setDateOfLastUpdate(String dateOfLastUpdate) {
        data.dateOfLastUpdate = dateOfLastUpdate;
    }

    public String getPermitURL() {
        return data.permitURL;
    }

    public void setPermitURL(String permitURL) {
        data.permitURL = permitURL;
    }

    public String getEnforcementAction() {
        return data.enforcementAction;
    }

    public void setEnforcementAction(String enforcementAction) {
        data.enforcementAction = enforcementAction;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class PermitDetailsType {
        @XmlElement(required = true, name = "permitGranted", namespace = SchemaSet.SCHEMA_EUREG)
        Boolean permitGranted;

        @XmlElement(required = true, name = "permitReconsidered", namespace = SchemaSet.SCHEMA_EUREG)
        Boolean permitReconsidered;

        @XmlElement(required = true, name = "permitUpdated", namespace = SchemaSet.SCHEMA_EUREG)
        Boolean permitUpdated;

        @XmlElement(required = false, name = "dateOfGranting", namespace = SchemaSet.SCHEMA_EUREG)
        String dateOfGranting;

        @XmlElement(required = false, name = "dateOfLastUpdate", namespace = SchemaSet.SCHEMA_EUREG)
        String dateOfLastUpdate;

        @XmlElement(required = false, name = "permitURL", namespace = SchemaSet.SCHEMA_EUREG)
        String permitURL;

        @XmlElement(required = false, name = "enforcementAction", namespace = SchemaSet.SCHEMA_EUREG)
        String enforcementAction;
    }
}
