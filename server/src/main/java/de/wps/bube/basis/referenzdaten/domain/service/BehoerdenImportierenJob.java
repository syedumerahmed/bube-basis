package de.wps.bube.basis.referenzdaten.domain.service;

import static java.lang.String.format;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.xml.BehoerdeXMLMapper;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;
import de.wps.bube.basis.referenzdaten.xml.dto.BehoerdenlisteXDto;

@Service
@Transactional
public class BehoerdenImportierenJob implements ReferenzdatenJob {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.BEHOERDEN_IMPORTIEREN;

    private static final Logger LOGGER = LoggerFactory.getLogger(BehoerdenImportierenJob.class);

    private static final int COUNT_SIZE = 10;

    private final BehoerdenImportExportService importExportService;
    private final BenutzerJobRegistry benutzerJobRegistry;
    private final DateiService dateiService;
    private final XMLService xmlService;
    private final BehoerdeXMLMapper behoerdeXMLMapper;

    public BehoerdenImportierenJob(BehoerdenImportExportService importExportService,
            BenutzerJobRegistry benutzerJobRegistry, DateiService dateiService,
            XMLService xmlService, BehoerdeXMLMapper behoerdeXMLMapper) {
        this.importExportService = importExportService;
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.dateiService = dateiService;
        this.xmlService = xmlService;
        this.behoerdeXMLMapper = behoerdeXMLMapper;
    }

    @Async
    public void runAsync(String username) {
        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(format("%s Start: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), username));

        try {
            AtomicInteger count = new AtomicInteger();
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.BEHOERDE);
            xmlService.read(inputStream, BehoerdeType.class, behoerdeXMLMapper,
                    BehoerdenlisteXDto.XML_ROOT_ELEMENT_NAME,
                    null, behoerde -> {
                        importExportService.importiereBehoerde(behoerde, jobProtokoll);
                        if (count.incrementAndGet() % COUNT_SIZE == 0) {
                            benutzerJobRegistry.jobCountChanged(COUNT_SIZE);
                        }
                    });

            benutzerJobRegistry.jobCountChanged(count.get() % COUNT_SIZE);
            benutzerJobRegistry.jobCompleted(false);
            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Behörden: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            // Datei wird gelöscht, egal ob der Job durchlief oder nicht
            importExportService.dateiLoeschen(username);

            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }
}
