package de.wps.bube.basis.euregistry.xml.bericht.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum EUConfidentiality {
    @XmlEnumValue("Art4-2b") ART4_2B("Article4(2)(b)"),
    @XmlEnumValue("Art4-2c") ART4_2C("Article4(2)(c)"),
    @XmlEnumValue("Art4-2d") ART4_2D("Article4(2)(d)"),
    @XmlEnumValue("Art4-2e") ART4_2E("Article4(2)(e)"),
    @XmlEnumValue("Art4-2f") ART4_2F("Article4(2)(f)");

    private final String id;

    EUConfidentiality(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
