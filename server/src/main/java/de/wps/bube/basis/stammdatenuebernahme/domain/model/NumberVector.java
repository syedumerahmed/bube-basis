package de.wps.bube.basis.stammdatenuebernahme.domain.model;

public class NumberVector {
    private Double spl;
    private Double spb;
    private boolean equal;
    final double epsilon = 0.000001d;

    public NumberVector(Double spl, Double spb) {
        this.spl = spl;
        this.spb = spb;
        // Double.equals vergleicht bitweise, wir wollen mit Tolleranz vergleichen
        this.equal = (spl == null && spb == null) || (spl != null && spb != null && (Math.abs(spl - spb) < epsilon));
    }

    public Double getSpl() {
        return spl;
    }

    public void setSpl(Double spl) {
        this.spl = spl;
    }

    public Double getSpb() {
        return spb;
    }

    public void setSpb(Double spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
