package de.wps.bube.basis.komplexpruefung.rules.bean;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;

public interface PruefObjekt {

    Long getId();

    RegelObjekt getRegelObjekt();

}
