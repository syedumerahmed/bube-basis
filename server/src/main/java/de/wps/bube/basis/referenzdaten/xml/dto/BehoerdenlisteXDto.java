package de.wps.bube.basis.referenzdaten.xml.dto;

import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

@XmlRootElement(name = BehoerdenlisteXDto.XML_ROOT_ELEMENT_NAME, namespace = BehoerdenlisteXDto.XSD_NAMESPACE)
public class BehoerdenlisteXDto {

    public static final String XML_ROOT_ELEMENT_NAME = "behoerdenliste";
    public static final String XSD_SCHEMA_URL = "classpath:xsd/XREBH_1.29.xsd";
    public static final String XSD_NAMESPACE = "http://basis.bube.wps.de/behoerden";
    public static final QName QUALIFIED_NAME = new QName(XSD_NAMESPACE, XML_ROOT_ELEMENT_NAME);

    @XmlElement
    public List<BehoerdeType> behoerde;

}
