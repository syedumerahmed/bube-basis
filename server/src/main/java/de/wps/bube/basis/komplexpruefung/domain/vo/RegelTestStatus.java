package de.wps.bube.basis.komplexpruefung.domain.vo;

public enum RegelTestStatus {
    BST_GUELTIG, BST_UNGUELTIG, KEINE_OBJEKTE, SEMANTISCHER_FEHLER, SYNTAX_FEHLER, BST_NICHT_GEFUNDEN
}
