package de.wps.bube.basis.stammdaten.domain.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.stammdaten.domain.event.StammdatenImportTriggeredEvent;

@Component
public class StammdatenImportierenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(StammdatenImportierenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final StammdatenImportierenJob job;

    public StammdatenImportierenListener(BenutzerJobRegistry jobRegistry, StammdatenImportierenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    @Transactional
    public void handleStammdatenImportieren(StammdatenImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", StammdatenImportierenJob.JOB_NAME);

        jobRegistry.jobStarted(new JobStatus(StammdatenImportierenJob.JOB_NAME));

        job.runAsync(event.berichtsjahr);
    }
}
