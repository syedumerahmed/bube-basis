package de.wps.bube.basis.stammdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.rules.bean.LeistungBean;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class LeistungBeanMapper {

    public abstract LeistungBean toBean(Leistung leistung);

}
