package de.wps.bube.basis.stammdaten.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.stammdaten.domain.event.AnsprechpartnerLoeschenTriggerdEvent;

@Component
public class AnsprechpartnerLoeschenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnsprechpartnerLoeschenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final AnsprechpartnerLoeschenJob job;

    public AnsprechpartnerLoeschenListener(BenutzerJobRegistry jobRegistry, AnsprechpartnerLoeschenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleAnsprechpartnerLoeschen(AnsprechpartnerLoeschenTriggerdEvent event) {
        LOGGER.info("Event handling started for '{}'", AnsprechpartnerLoeschenTriggerdEvent.EVENT_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(AnsprechpartnerLoeschenJob.JOB_NAME));

        job.runAsync(event.berichtsjahr, event.username, event.land);
    }
}
