package de.wps.bube.basis.euregistry.xml.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

@XmlRootElement(name = EuRegBstListXDto.XML_ROOT_ELEMENT_NAME)
public class EuRegBstListXDto {

    public static final String XML_ROOT_ELEMENT_NAME = "euregistry";
    public static final String XSD_SCHEMA_URL = "classpath:xsd/XREG_1.4.xsd";
    public static final String XSD_NAMESPACE = "http://basis.bube.wps.de/euregistry";
    public static final QName QUALIFIED_NAME = new QName(XSD_NAMESPACE, XML_ROOT_ELEMENT_NAME);

    public List<EuRegBst> euRegBst;

}
