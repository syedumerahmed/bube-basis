package de.wps.bube.basis.euregistry.domain.entity;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class Genehmigung {

    @NotNull
    private boolean erteilt;
    @NotNull
    private boolean neuBetrachtet;
    @NotNull
    private boolean geaendert;

    private LocalDate erteilungsdatum;
    private LocalDate neuBetrachtetDatum;
    private LocalDate aenderungsdatum;

    private String url;

    // Durchsetzungsmaßnahmen Freitext
    private String durchsetzungsmassnahmen;

    @Deprecated
    protected Genehmigung() {
    }

    public Genehmigung(
            boolean erteilt,
            boolean neuBetrachtet,
            boolean geaendert,
            LocalDate erteilungsdatum,
            LocalDate neuBetrachtetDatum,
            LocalDate aenderungsdatum,
            String url,
            String durchsetzungsmassnahmen) {
        this.erteilt = erteilt;
        this.neuBetrachtet = neuBetrachtet;
        this.geaendert = geaendert;
        this.erteilungsdatum = erteilungsdatum;
        this.neuBetrachtetDatum = neuBetrachtetDatum;
        this.aenderungsdatum = aenderungsdatum;
        this.url = url;
        this.durchsetzungsmassnahmen = durchsetzungsmassnahmen;
    }

    public boolean isErteilt() {
        return erteilt;
    }

    public void setErteilt(boolean erteilt) {
        this.erteilt = erteilt;
    }

    public boolean isNeuBetrachtet() {
        return neuBetrachtet;
    }

    public void setNeuBetrachtet(boolean neu_betrachtet) {
        this.neuBetrachtet = neu_betrachtet;
    }

    public boolean isGeaendert() {
        return geaendert;
    }

    public void setGeaendert(boolean geaendert) {
        this.geaendert = geaendert;
    }

    public LocalDate getErteilungsdatum() {
        return erteilungsdatum;
    }

    public void setErteilungsdatum(LocalDate erteilungsdatum) {
        this.erteilungsdatum = erteilungsdatum;
    }

    public LocalDate getNeuBetrachtetDatum() {
        return neuBetrachtetDatum;
    }

    public void setNeuBetrachtetDatum(LocalDate neu_betrachtet_datum) {
        this.neuBetrachtetDatum = neu_betrachtet_datum;
    }

    public LocalDate getAenderungsdatum() {
        return aenderungsdatum;
    }

    public void setAenderungsdatum(LocalDate aenderungsdatum) {
        this.aenderungsdatum = aenderungsdatum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDurchsetzungsmassnahmen() {
        return durchsetzungsmassnahmen;
    }

    public void setDurchsetzungsmassnahmen(String durchsetzungsmassnahmen) {
        this.durchsetzungsmassnahmen = durchsetzungsmassnahmen;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(o == null || getClass() != o.getClass()) {
            return false;
        }
        Genehmigung that = (Genehmigung) o;
        return erteilt == that.erteilt
                && neuBetrachtet == that.neuBetrachtet
                && geaendert == that.geaendert
                && erteilungsdatum.equals(that.erteilungsdatum)
                && neuBetrachtetDatum.equals(that.neuBetrachtetDatum)
                && aenderungsdatum.equals(that.aenderungsdatum)
                && url.equals(that.url)
                && durchsetzungsmassnahmen.equals(that.durchsetzungsmassnahmen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                erteilt,
                geaendert,
                neuBetrachtet,
                erteilungsdatum,
                neuBetrachtetDatum,
                aenderungsdatum,
                url,
                durchsetzungsmassnahmen);
    }
}
