package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProductionSite {

    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    private String gmlId;

    @XmlElement(name = "inspireId", namespace = SchemaSet.SCHEMA_PF)
    private InspireID inspireID;

    /**
     * Pflichtelement, darf leer sein
     */
    @XmlElement(required = true, name = "status", namespace = SchemaSet.SCHEMA_PF, nillable = true)
    private Nillable status;

    @XmlElement(required = true, name = "siteName")
    private FeatureNameContainer siteName;

    @XmlElement(required = true, name = "location")
    private final PointLocation pointLocation = new PointLocation();

    @XmlElement(required = true, name = "reportData")
    private ReferenceType reportData;

    public InspireID getInspireID() {
        return inspireID;
    }

    public void setInspireID(InspireID inspireID) {
        this.inspireID = inspireID;
    }

    public FeatureNameContainer getSiteName() {
        return siteName;
    }

    public void setSiteName(FeatureNameContainer siteName) {
        this.siteName = siteName;
    }

    public Point getLocation() {
        return pointLocation.getPoint();
    }

    public void setLocation(Point point) {
        pointLocation.setPoint(point);
    }

    public ReferenceType getReportData() {
        return reportData;
    }

    public void setReportData(ReferenceType reportData) {
        this.reportData = reportData;
    }

    public String getGmlId() {
        return gmlId;
    }

    public void setGmlId(String gmlId) {
        this.gmlId = gmlId;
    }
}
