package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

public class StammdatenImportTriggeredEvent {

    public final String username;
    public final String berichtsjahr;

    public StammdatenImportTriggeredEvent(String username, String berichtsjahr) {
        this.username = username;
        this.berichtsjahr = berichtsjahr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        StammdatenImportTriggeredEvent that = (StammdatenImportTriggeredEvent) o;
        return Objects.equals(username, that.username) && Objects.equals(berichtsjahr,
                that.berichtsjahr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, berichtsjahr);
    }
}
