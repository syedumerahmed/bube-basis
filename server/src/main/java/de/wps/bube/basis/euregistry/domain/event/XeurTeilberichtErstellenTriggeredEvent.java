package de.wps.bube.basis.euregistry.domain.event;

import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.base.vo.Land;

public record XeurTeilberichtErstellenTriggeredEvent(List<Long> berichtIds, Referenz berichtsjahr, Land land) {

    public static final String EVENT_NAME = "XEUR Teilbericht erstellen";
}
