package de.wps.bube.basis.stammdatenbetreiber.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.SpbBetreiberDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbBetreiberMapper {

    @Autowired
    StammdatenService stammdatenService;

    @Mapping(target = "betriebsstaetteId", ignore = true)
    public abstract SpbBetreiberDto toDto (SpbBetreiber spbBetreiber);

    @Mapping(target = "betreiber", source = "betreiberId", qualifiedByName = "getBetreiber")
    public abstract SpbBetreiber fromDto(SpbBetreiberDto spbBetreiberDto);

    @Named("getBetreiber")
    public Betreiber getBetreiber(Long betreiberId) {
        return betreiberId != null ? this.stammdatenService.loadBetreiber(betreiberId) : null;
    }
}