package de.wps.bube.basis.desktop.web;

import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.ANLAGE;
import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.BETRIEBSSTAETTE;
import static de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum.QUELLE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTagsEnum;
import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlageListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlagenteilListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbQuelleListItem;

@Component
public class SpbBestriebsstaetteTreeNodeMapper implements TreeNodeMapper {

    private final StammdatenBetreiberService stammdatenBetreiberService;
    private final StammdatenService stammdatenService;

    public SpbBestriebsstaetteTreeNodeMapper(
            StammdatenBetreiberService stammdatenBetreiberService,
            StammdatenService stammdatenService) {
        this.stammdatenBetreiberService = stammdatenBetreiberService;
        this.stammdatenService = stammdatenService;
    }

    @Override
    public DesktopTyp getTyp() {
        return DesktopTyp.SPB_STAMMDATEN;
    }

    @Override
    public Optional<TreeNodeDto> map(DesktopItem item) {
        Optional<SpbBetriebsstaette> bstOpt = stammdatenBetreiberService.findSpbBetriebsstaetteByBetriebsstaetteId(
                item.getDesktopItemId().getItemId());

        if (bstOpt.isPresent()) {
            return bstOpt.map(b -> mapBetriebsstaette(b));
        } else {
            return stammdatenService.findBetriebsstaette(item.getDesktopItemId().getItemId())
                                    .map(SpbBetriebsstaette::new)
                                    .map(this::mapBetriebsstaette);
        }
    }

    public TreeNodeDto mapBetriebsstaette(SpbBetriebsstaette b) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setId(b.getBetriebsstaette().getId());
        dto.setName(b.getName());
        dto.setNummer(b.getBetriebsstaette().getBetriebsstaetteNr());
        dto.setTyp(BETRIEBSSTAETTE);
        String basePath = "betriebsstaette/land/" + b.getBetriebsstaette()
                                                     .getLand()
                                                     .getNr() + "/nummer/" + b.getBetriebsstaette()
                                                                              .getBetriebsstaetteNr();
        dto.setLink(basePath);
        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (b.getBetriebsstaette().getLetzteAenderungBetreiber() != null) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        for (SpbAnlageListItem a : stammdatenBetreiberService.listAllSpbAnlagenAsListItemByBetriebsstaettenId(
                b.getBetriebsstaette().getId())) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(mapAnlage(a, basePath, b.getBetriebsstaette().getId()));
        }

        for (SpbQuelleListItem q : stammdatenBetreiberService.listAllSpbQuellenAsListItemByBetriebsstaettenId(
                b.getBetriebsstaette().getId())) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(mapQuelle(q, basePath));
        }

        return dto;
    }

    private TreeNodeDto mapAnlage(SpbAnlageListItem a, String basePath, Long betriebsstaetteId) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(a.getName());
        dto.setNummer(a.getAnlageNr());
        dto.setTyp(ANLAGE);
        dto.setLink(basePath + "/anlage/nummer/" + a.getAnlageNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (a.isVeraendert()) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        } else if (a.isNeu()) {
            tags.add(TreeNodeTagsEnum.NEU_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        // Ist es eine Anlage oder eine neue Anlage des Betreibers
        List<SpbAnlagenteilListItem> anlagenteile;
        if (a.getAnlageId() != null) {
            anlagenteile = stammdatenBetreiberService.listAllSpbAnlagenteileAsListItemByAnlageId(
                    a.getAnlageId(), betriebsstaetteId);
        } else {
            anlagenteile = stammdatenBetreiberService.listAllSpbAnlagenteileAsListItemBySpbAnlageId(
                    a.getSpbAnlageId(), betriebsstaetteId);
        }

        for (SpbAnlagenteilListItem t : anlagenteile) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(mapAnlagenteil(t, dto.getLink()));
        }

        for (SpbQuelleListItem q : stammdatenBetreiberService.listAllSpbQuellenAsListItemByAnlageId(a.getAnlageId(),
                a.getSpbAnlageId(), betriebsstaetteId)) {
            if (dto.getChildren() == null) {
                dto.setChildren(new ArrayList<>());
            }
            dto.getChildren().add(mapQuelle(q, dto.getLink()));
        }

        return dto;
    }

    private static TreeNodeDto mapAnlagenteil(SpbAnlagenteilListItem a, String basePath) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(a.getName());
        dto.setNummer(a.getAnlagenteilNr());
        dto.setTyp(ANLAGE);
        dto.setLink(basePath + "/anlagenteil/nummer/" + a.getAnlagenteilNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (a.isVeraendert()) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        } else if (a.isNeu()) {
            tags.add(TreeNodeTagsEnum.NEU_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        return dto;
    }

    private static TreeNodeDto mapQuelle(SpbQuelleListItem q, String basePath) {
        TreeNodeDto dto = new TreeNodeDto();

        dto.setName(q.getName());
        dto.setNummer(q.getQuelleNr());
        dto.setTyp(QUELLE);
        dto.setLink(basePath + "/quelle/nummer/" + q.getQuelleNr());

        var tags = new ArrayList<TreeNodeTagsEnum>();
        if (q.isVeraendert()) {
            tags.add(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER);
        } else if (q.isNeu()) {
            tags.add(TreeNodeTagsEnum.NEU_DURCH_BETREIBER);
        }
        dto.setTags(tags);

        return dto;
    }
}
