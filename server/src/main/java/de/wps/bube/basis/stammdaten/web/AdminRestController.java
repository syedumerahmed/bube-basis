package de.wps.bube.basis.stammdaten.web;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.AdminApi;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class AdminRestController implements AdminApi {

    private final StammdatenService stammdatenService;
    private final ReferenzenService referenzenService;
    private final AktiverBenutzer aktiverBenutzer;

    public AdminRestController(StammdatenService stammdatenService,
        ReferenzenService referenzenService,
        AktiverBenutzer aktiverBenutzer) {
        this.stammdatenService = stammdatenService;
        this.referenzenService = referenzenService;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Override public ResponseEntity<Void> deleteAllAnsprechpartner(String jahr) {
        Referenz berichtsjahr = referenzenService.readReferenz(Referenzliste.RJAHR, jahr, Gueltigkeitsjahr.of(jahr), Land.DEUTSCHLAND);
        stammdatenService.deleteAllAnsprechpartner(berichtsjahr, aktiverBenutzer.getUsername());
        return ResponseEntity.ok().build();
    }
}
