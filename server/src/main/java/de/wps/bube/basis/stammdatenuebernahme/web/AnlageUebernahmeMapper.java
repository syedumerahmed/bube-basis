package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.AnlageUebernahmeDto;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlageUebernahme;

@Mapper
public abstract class AnlageUebernahmeMapper {

    public abstract AnlageUebernahme fromDto(AnlageUebernahmeDto anlageUebernahmeDto);
}
