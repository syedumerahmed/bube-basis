package de.wps.bube.basis.komplexpruefung.domain.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;

public class RegelPruefungenMap {

    final Map<String, List<RegelPruefung>> regelPruefungenMap = new HashMap<>();

    public List<RegelPruefung> get(Long pruefObjektId, RegelObjekt regelObjekt) {
        return regelPruefungenMap.get(pruefObjektId + regelObjekt.getBezeichner());
    }

    public boolean has(Long pruefObjektId, RegelObjekt regelObjekt) {
        return regelPruefungenMap.containsKey(pruefObjektId + regelObjekt.getBezeichner());
    }

    public void put(RegelPruefung regelPruefung) {
        var id = regelPruefung.getPruefObjektId();
        var rObjekt = regelPruefung.getRegelObjekt();

        // Ist der Code der Regel  nicht valide kann es vorkommen, dass PrüfObjekt und RegelObjekt null sind
        if ((id == null) || (rObjekt == null)) {
            return;
        }

        if (has(id, rObjekt)) {
            List<RegelPruefung> list = get(id, rObjekt);
            list.add(regelPruefung);
        } else {
            List<RegelPruefung> list = new ArrayList<>();
            list.add(regelPruefung);
            regelPruefungenMap.put(id + rObjekt.getBezeichner(), list);
        }

    }

}
