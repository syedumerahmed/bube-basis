package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe.GRUPPE_A;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe.GRUPPE_B;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.TEILBERICHT;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus.BST_UNGUELTIG;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.rules.EURegBeanMappers;
import de.wps.bube.basis.euregistry.rules.bean.EURegAnlageBean;
import de.wps.bube.basis.euregistry.rules.bean.EURegFBean;
import de.wps.bube.basis.euregistry.rules.bean.TeilberichtBean;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungErgebnis;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingException;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettePruefungService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;

@Service
public class BerichtPruefungService {

    public static class TestErgebnis {
        public RegelTestStatus status;
        public String message;
        public Betriebsstaette betriebsstaette;
    }

    private final EURegistryService euRegistryService;
    private final BetriebsstaettePruefungService betriebsstaettePruefungService;
    private final StammdatenService stammdatenService;
    private final KomplexpruefungService komplexpruefungService;
    private final RegelPruefungService regelPruefungService;
    private final ReferenzenService referenzenService;
    private final EURegBeanMappers euRegBeanMappers;

    public BerichtPruefungService(EURegistryService euRegistryService,
            BetriebsstaettePruefungService betriebsstaettePruefungService,
            StammdatenService stammdatenService,
            KomplexpruefungService komplexpruefungService,
            RegelPruefungService regelPruefungService,
            ReferenzenService referenzenService,
            EURegBeanMappers euRegBeanMappers) {
        this.euRegistryService = euRegistryService;
        this.betriebsstaettePruefungService = betriebsstaettePruefungService;
        this.stammdatenService = stammdatenService;
        this.komplexpruefungService = komplexpruefungService;
        this.regelPruefungService = regelPruefungService;
        this.referenzenService = referenzenService;
        this.euRegBeanMappers = euRegBeanMappers;
    }

    @Transactional(noRollbackFor = BeanMappingException.class)
    @Secured(BEHOERDENBENUTZER)
    public Komplexpruefung komplexpruefung(EURegBetriebsstaette euRegBetriebsstaette) {
        if (euRegBetriebsstaette.getKomplexpruefung() != null) {
            komplexpruefungService.delete(euRegBetriebsstaette.getKomplexpruefung());
        }

        Komplexpruefung komplexpruefung = komplexpruefungService.save(new Komplexpruefung());
        euRegBetriebsstaette.setKomplexpruefung(komplexpruefung);
        Betriebsstaette betriebsstaette = euRegBetriebsstaette.getBetriebsstaette();
        EnumMap<RegelObjekt, List<? extends PruefObjekt>> pruefObjektCache = new EnumMap<>(RegelObjekt.class);
        komplexpruefungService.komplexpruefung(List.of(GRUPPE_A, GRUPPE_B),
                komplexpruefung, betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(),
                regelObjekt -> getPruefObjekte(betriebsstaette, regelObjekt, pruefObjektCache)
        );

        euRegistryService.updateKomplexpruefung(euRegBetriebsstaette, komplexpruefung.hatFehler());

        return komplexpruefung;
    }

    @Transactional(readOnly = true)
    @Secured(Rollen.LAND_READ)
    public TestErgebnis regelTesten(String code, List<RegelObjekt> regelObjekte,
            Long berichtsjahrId, Land land,
            String betriebsstaetteNr) {

        TestErgebnis testErgebnis = new TestErgebnis();

        // Stelle die syntaktische Korrektheit sicher
        Expression expression;
        try {
            SpelExpressionParser parser = new SpelExpressionParser();
            expression = parser.parseExpression(code);
        } catch (ParseException e) {
            testErgebnis.status = RegelTestStatus.SYNTAX_FEHLER;
            testErgebnis.message = e.getMessage();
            return testErgebnis;
        }

        // Suche die Betriebsstaette
        Referenz jahr = referenzenService
                .findById(berichtsjahrId)
                .orElseThrow(() -> new BubeEntityNotFoundException(Referenz.class, "ID " + berichtsjahrId));
        Betriebsstaette betriebsstaette =
                stammdatenService.loadBetriebsstaetteByJahrLandNummer(jahr.getSchluessel(), land, betriebsstaetteNr);

        testErgebnis.betriebsstaette = betriebsstaette;

        EnumMap<RegelObjekt, List<? extends PruefObjekt>> pruefObjektCache = new EnumMap<>(RegelObjekt.class);

        // Führe die eigentliche Auswertung durch
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(expression, regelObjekte,
                land, betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(),
                true, regelObjekt -> getPruefObjekte(betriebsstaette, regelObjekt, pruefObjektCache));

        if (!ergebnis.getPruefungsFehler().isEmpty()) {
            testErgebnis.status = RegelTestStatus.SEMANTISCHER_FEHLER;
            testErgebnis.message = ergebnis.getPruefungsFehler().values().iterator().next().getMessage();
        } else if (!ergebnis.getUngueltigeObjekte().isEmpty()) {
            testErgebnis.status = BST_UNGUELTIG;
        } else if (!ergebnis.getGueltigeObjekte().isEmpty()) {
            testErgebnis.status = RegelTestStatus.BST_GUELTIG;
        } else {
            testErgebnis.status = RegelTestStatus.KEINE_OBJEKTE;
        }

        return testErgebnis;
    }

    @Transactional(noRollbackFor = BeanMappingException.class)
    @Secured(BEHOERDENBENUTZER)
    public List<TestErgebnis> pruefeTeilberichtRegeln(Regel regel, XeurTeilbericht bericht) throws ParseException {
        if (!List.of(TEILBERICHT).equals(regel.getObjekte())) {
            throw new IllegalArgumentException("Regel %d ist ungültig: Darf nur Objekt %s enthalten"
                    .formatted(regel.getNummer(), TEILBERICHT.getBezeichner()));
        }

        SpelExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(regel.getCode());

        BeanMappingContext beanMappingContext = new BeanMappingContext(bericht.getLand(),
                Gueltigkeitsjahr.of(bericht.getJahr().getSchluessel()));
        TeilberichtBean pruefObjekt = euRegBeanMappers.getTeilberichtBeanMapper().toBean(bericht,
                beanMappingContext, euRegBeanMappers);
        try (RegelPruefungService.RegelPruefungEvaluationContext context = regelPruefungService.setupContext(
                Gueltigkeitsjahr.of(bericht.getJahr().getSchluessel()), bericht.getLand(), null)) {
            context.setVariable("OBJ", pruefObjekt);
            // Regeln mit Objekt TEILBERICHT müssen eine Liste von BetriebsstaetteBeans (= Regelverstößen) zurückgeben.
            try {
                @SuppressWarnings({ "RedundantExplicitVariableType", "unchecked" }) List<BetriebsstaetteBean> result
                        = (List<BetriebsstaetteBean>) expression.getValue(context);
                return result == null
                        ? Collections.emptyList()
                        : result.stream().map(bstBean -> {
                    TestErgebnis ergebnis = new TestErgebnis();
                    ergebnis.status = BST_UNGUELTIG;
                    ergebnis.message = "%s %d: %s".formatted(regel.getTyp(bericht.getLand()), regel.getNummer(),
                            regel.getFehlertext());
                    ergebnis.betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(bstBean.getId());
                    return ergebnis;
                }).toList();
            } catch (EvaluationException e) {
                Throwable t = e;
                // ist irgendwo eine BeanMappingContext eingepackt?
                do {
                    t = t.getCause();
                    if (t instanceof BeanMappingException bme) {
                        // Wenn ja, dann wirf sie
                        throw bme;
                    }
                } while (t != null && !t.equals(t.getCause()));
                // Wenn nein, wirf die Original-Exception
                throw e;
            }
        }
    }

    public List<PruefObjekt> getPruefObjekte(Betriebsstaette betriebsstaette, RegelObjekt regelObjekt,
            Map<RegelObjekt, List<? extends PruefObjekt>> pruefObjektCache) {
        LinkedList<PruefObjekt> pruefObjekte = new LinkedList<>();
        BeanMappingContext beanMappingContext = new BeanMappingContext(betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        switch (regelObjekt) {
        case BST, ANLAGE, ANLAGENTEIL, QUELLE, BETREIBER -> pruefObjekte.addAll(
                betriebsstaettePruefungService.getPruefObjekte(betriebsstaette, regelObjekt, pruefObjektCache));
        case EUREG_BST -> pruefObjekte.addAll(pruefObjektCache.computeIfAbsent(regelObjekt,
                r -> euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())
                                      .map(b -> euRegBeanMappers.getEuRegBetriebsstaetteBeanMapper().toBean(b,
                                              beanMappingContext, euRegBeanMappers)).stream().toList()));
        case EUREG_ANLAGE -> {
            if (!pruefObjektCache.containsKey(regelObjekt)) {
                List<EURegAnlageBean> euRegAnlagen = new ArrayList<>();
                for (Anlage anlage : betriebsstaette.getAnlagen()) {
                    euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())
                                     .map(a -> euRegBeanMappers.getEuRegAnlageBeanMapper().toBean(a, beanMappingContext,
                                             euRegBeanMappers))
                                     .ifPresent(euRegAnlagen::add);
                    for (Anlagenteil anlagenteil : anlage.getAnlagenteile()) {
                        euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())
                                         .map(a -> euRegBeanMappers.getEuRegAnlageBeanMapper()
                                                                   .toBean(a, beanMappingContext,
                                                                           euRegBeanMappers))
                                         .ifPresent(euRegAnlagen::add);
                    }
                }
                pruefObjektCache.put(regelObjekt, euRegAnlagen);
            }
            pruefObjekte.addAll(pruefObjektCache.get(regelObjekt));
        }
        case EUREG_F -> {
            if (!pruefObjektCache.containsKey(regelObjekt)) {
                List<EURegFBean> euRegFs = new ArrayList<>();
                for (Anlage anlage : betriebsstaette.getAnlagen()) {
                    euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())
                                     .filter(EURegAnlage::isEURegF)
                                     .map(a -> euRegBeanMappers.getEuRegFBeanMapper().toBean(a, beanMappingContext,
                                             euRegBeanMappers))
                                     .ifPresent(euRegFs::add);
                    for (Anlagenteil anlagenteil : anlage.getAnlagenteile()) {
                        euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())
                                         .filter(EURegAnlage::isEURegF)
                                         .map(a -> euRegBeanMappers.getEuRegFBeanMapper().toBean(a, beanMappingContext,
                                                 euRegBeanMappers))
                                         .ifPresent(euRegFs::add);
                    }
                }
                pruefObjektCache.put(regelObjekt, euRegFs);
            }
            pruefObjekte.addAll(pruefObjektCache.get(regelObjekt));
        }
        case TEILBERICHT -> throw new IllegalArgumentException();
        }
        return pruefObjekte;

    }

}
