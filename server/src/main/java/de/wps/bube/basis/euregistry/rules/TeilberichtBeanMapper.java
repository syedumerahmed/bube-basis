package de.wps.bube.basis.euregistry.rules;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.domain.service.XeurTeilberichtCrudService;
import de.wps.bube.basis.euregistry.rules.bean.EURegBetriebsstaetteBean;
import de.wps.bube.basis.euregistry.rules.bean.TeilberichtBean;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingException;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;

@Mapper()
public abstract class TeilberichtBeanMapper {

    @Autowired
    protected ReferenzenService referenzenService;
    @Autowired
    protected EURegistryService euRegistryService;
    @Autowired
    protected XeurTeilberichtCrudService xeurTeilberichtCrudService;
    @Autowired
    protected ReferenzBeanMapper referenzBeanMapper;
    @Autowired
    protected EURegBetriebsstaetteBeanMapper euRegBetriebsstaetteBeanMapper;

    @Mapping(target = "berichte", expression = "java(berichte(teilbericht, context, mappers))")
    @Mapping(target = "vorgaenger", expression = "java(vorgaenger(teilbericht, context, mappers))")
    @Mapping(target = "nachfolger", expression = "java(nachfolger(teilbericht, context, mappers))")
    public abstract TeilberichtBean toBean(XeurTeilbericht teilbericht, @Context BeanMappingContext context,
            @Context EURegBeanMappers mappers);

    protected Supplier<List<EURegBetriebsstaetteBean>> berichte(XeurTeilbericht teilbericht, BeanMappingContext context,
            EURegBeanMappers mappers) {
        return () -> {
            var jahr = referenzenService.getJahr(String.valueOf(context.getJahr().getJahr()));
            return xeurTeilberichtCrudService.getAllTeilberichte(jahr, context.getLand())
                                             .stream()
                                             .map(XeurTeilbericht::getLocalIds)
                                             .flatMap(Collection::stream)
                                             .distinct()
                                             .map(localId -> {
                                                 Optional<EURegBetriebsstaette> berichtOptional
                                                         = euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(
                                                         jahr.getSchluessel(), localId);
                                                 return berichtOptional.orElseThrow(()
                                                         -> new BeanMappingException(
                                                                 "Betriebsstätte mit LocalID '%s' in Berichtsjahr %s nicht gefunden"
                                                                         .formatted(localId, jahr.getSchluessel())));
                                             })
                                             .map(b -> mappers.getEuRegBetriebsstaetteBeanMapper()
                                                              .toBean(b, context, mappers))
                                             .toList();
        };
    }

    protected Supplier<TeilberichtBean> vorgaenger(XeurTeilbericht teilbericht, BeanMappingContext context,
            EURegBeanMappers mappers) {
        return () -> mappers.getTeilberichtBeanMapper().toBean(teilbericht, context.vorgaenger(), mappers);
    }

    protected Supplier<TeilberichtBean> nachfolger(XeurTeilbericht teilbericht, BeanMappingContext context,
            EURegBeanMappers mappers) {
        return () -> mappers.getTeilberichtBeanMapper().toBean(teilbericht, context.nachfolger(), mappers);
    }

}
