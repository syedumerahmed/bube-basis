package de.wps.bube.basis.euregistry.persistence;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public interface XeurTeilberichtRepository
        extends JpaRepository<XeurTeilbericht, Long>, QuerydslPredicateProjectionRepository<XeurTeilbericht> {

    Stream<XeurTeilbericht> findAllByLandAndJahr(Land land, Referenz jahr);

    Stream<XeurTeilbericht> findAllByJahr(Referenz jahr);

    @Query(nativeQuery = true, value = """
            SELECT b.*
              FROM eureg_xeur_teilbericht b,
                   (SELECT land, MAX(freigegeben_at) AS freigegeben_at
                      FROM eureg_xeur_teilbericht
                     WHERE jahr_id = :jahrId
                       AND freigegeben_at IS NOT NULL
                       AND xeur_file IS NOT NULL
                     GROUP BY land) b2
             WHERE b.land = b2.land
               AND b.freigegeben_at = b2.freigegeben_at
             ORDER BY b.land;
            """)
    Stream<XeurTeilbericht> findAllLatestFreigegeben(Long jahrId);

    @Query(nativeQuery = true, value = """
            SELECT b.*
              FROM eureg_xeur_teilbericht b,
                   (SELECT t.land, MAX(t.freigegeben_at) AS freigegeben_at
                      FROM eureg_xeur_teilbericht t,
                           eureg_xeur_teilbericht_local_ids l
                     WHERE t.jahr_id = :jahrId
                       AND t.land = :land
                       AND t.freigegeben_at IS NOT NULL
                       AND t.xeur_file IS NOT NULL
                       AND t.id = l.eureg_xeur_teilbericht_id
                       AND l.local_ids = :localId
                     GROUP BY t.land) b2
             WHERE 1 = 1
               AND b.land = :land
               AND b.freigegeben_at = b2.freigegeben_at
             ORDER BY b.land
            """)
    Optional<XeurTeilbericht> findLatestFreigegeben(Long jahrId, String land, String localId);

}
