package de.wps.bube.basis.referenzdaten.domain.entity;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

/**
 * Basis für alle Refrenzlisten
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Referenz implements IReferenz {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "referenz_seq")
    @SequenceGenerator(name = "referenz_seq", allocationSize = 50)
    private Long id;

    private String referenznummer;

    @NotNull
    private Land land;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Referenzliste referenzliste;

    @NotNull
    private String schluessel;

    // Legt die Reihenfolge der Anzeige fest.
    @NotNull
    private String sortier;

    @NotNull
    private String ktext;

    private String ltext;

    // Ggf. Bezeichnung, die an die EU berichtet wird
    private String eu;

    // Steuerungsparameter
    private String strg;

    // Gilt ab 1.1.yyyy bezogen auf Berichtsjahr, min. 1900
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_von"))
    private Gueltigkeitsjahr gueltigVon;

    // Gilt bis 31.12.yyyy bezogen auf Berichtsjahr, max. 9999
    @NotNull
    @Embedded
    @AttributeOverride(name = "jahr", column = @Column(name = "gueltig_bis"))
    private Gueltigkeitsjahr gueltigBis;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected Referenz() {
    }

    public Referenz(Referenzliste referenzliste, Land land, String schluessel, String sortier, String ktext,
            Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis) {
        this.referenzliste = referenzliste;
        this.land = land;
        this.schluessel = schluessel;
        this.sortier = sortier;
        this.ktext = ktext;
        this.gueltigVon = gueltigVon;
        this.gueltigBis = gueltigBis;
    }

    public static String generateNewReferenznummer() {
        return UUID.randomUUID().toString();
    }

    public boolean isNew() {
        return id == null;
    }

    public boolean isLaenderspezifisch() {
        return this.referenzliste.isLaenderspezifisch();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenznummer() {
        return referenznummer;
    }

    public void setReferenznummer(String referenznummer) {
        this.referenznummer = referenznummer;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public String getSchluessel() {
        return schluessel;
    }

    public void setSchluessel(String schluessel) {
        this.schluessel = schluessel;
    }

    public String getSortier() {
        return sortier;
    }

    public void setSortier(String sortier) {
        this.sortier = sortier;
    }

    public Gueltigkeitsjahr getGueltigVon() {
        return gueltigVon;
    }

    public void setGueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
    }

    public Gueltigkeitsjahr getGueltigBis() {
        return gueltigBis;
    }

    public void setGueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public String getKtext() {
        return ktext;
    }

    public void setKtext(String ktext) {
        this.ktext = ktext;
    }

    public String getLtext() {
        return ltext;
    }

    public void setLtext(String ltext) {
        this.ltext = ltext;
    }

    public Referenzliste getReferenzliste() {
        return referenzliste;
    }

    public void setReferenzliste(Referenzliste referenzliste) {
        this.referenzliste = referenzliste;
    }

    public String getEu() {
        return eu;
    }

    public void setEu(String eu) {
        this.eu = eu;
    }

    public String getStrg() {
        return strg;
    }

    public void setStrg(String strg) {
        this.strg = strg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Referenz referenz = (Referenz) o;

        return Objects.equals(id, referenz.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public void pruefeLand() {
        if (land != Land.DEUTSCHLAND) {
            if (!isLaenderspezifisch()) {
                throw new ReferenzdatenException("Keine Länderreferenzen in bundeseinheitlichen Listen erlaubt");
            }
        }
    }

    @Override
    public void pruefeGueltigkeit() {
        if (gueltigBis.liegtVor(gueltigVon)) {
            throw new ReferenzdatenException("Gültig bis liegt zeitlich vor Gültig von");
        }
    }

    @Override
    public boolean isOfLand(Land land) {
        return this.land == land;
    }

    @Override
    public String toString() {
        return "Referenz " + "Referenznummer=" + referenznummer + ", Land=" + land.getNr() + ", Referenzliste=" + referenzliste + ", Schlüssel='" + schluessel + '\'';
    }

    public boolean gueltigFuer(Gueltigkeitsjahr jahr) {
        return gueltigVon.getJahr() <= jahr.getJahr() && jahr.getJahr() <= gueltigBis.getJahr();
    }
}
