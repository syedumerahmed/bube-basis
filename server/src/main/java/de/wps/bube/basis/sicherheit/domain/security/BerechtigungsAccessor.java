package de.wps.bube.basis.sicherheit.domain.security;

import java.util.Set;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;

/**
 * Interface zum Zugriff auf die konkreten Datenfelder einzelner Entities. Siehe
 * dazu:
 * https://wps-bube.atlassian.net/wiki/spaces/BB/pages/140115969/Sicherheitskonzept#Absicherung-auf-Serverseite
 * <p>
 * Ist einer dieser Sicherheitsaspekte für eine Entität nicht relevant, muss die
 * implementierende Methode {@code null} zurückgeben. Dieser Aspekt wird bei
 * Datenbankabfragen dann ignoriert.
 */
public interface BerechtigungsAccessor {

    Predicate land(Land land);

    Predicate behoerdenIn(Set<String> behoerden);

    Predicate akzIn(Set<String> akz);

    Predicate verwaltungsgebieteIn(Set<String> verwaltungsgebiete);

    Predicate betriebsstaettenIn(Set<String> betriebsstaetten);
}
