package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.AnlagenteilUebernahmeDto;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlagenteilUebernahme;

@Mapper
public abstract class AnlagenteilUebernahmeMapper {

    public abstract AnlagenteilUebernahme fromDto(AnlagenteilUebernahmeDto anlageUebernahmeDto);
}
