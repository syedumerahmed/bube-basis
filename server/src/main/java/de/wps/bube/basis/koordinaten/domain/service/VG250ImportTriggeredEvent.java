package de.wps.bube.basis.koordinaten.domain.service;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public record VG250ImportTriggeredEvent(String downloadLink, Referenz berichtsjahr) {

    public static final String EVENT_NAME = "Gemeindekoordinaten importieren";

}
