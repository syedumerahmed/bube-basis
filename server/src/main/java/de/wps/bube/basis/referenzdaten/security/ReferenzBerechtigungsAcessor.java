package de.wps.bube.basis.referenzdaten.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzBerechtigungsInMemoryAccess;

public class ReferenzBerechtigungsAcessor implements ReferenzBerechtigungsInMemoryAccess {
    private final Referenz referenz;

    public ReferenzBerechtigungsAcessor(Referenz referenz) {
        this.referenz = referenz;
    }

    @Override
    public boolean isBundesreferenz() {
        return referenz.isBundesreferenz();
    }

    @Override
    public Land getLand() {
        return referenz.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return null;
    }

    @Override
    public Set<String> getAkzSet() {
        return null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return null;
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return null;
    }
}
