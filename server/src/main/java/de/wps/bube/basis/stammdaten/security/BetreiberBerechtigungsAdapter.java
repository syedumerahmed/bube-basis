package de.wps.bube.basis.stammdaten.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;

public class BetreiberBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {
    private final Betreiber betreiber;

    public BetreiberBerechtigungsAdapter(Betreiber betreiber) {
        this.betreiber = betreiber;
    }

    @Override
    public Land getLand() {
        return betreiber.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return null;
    }

    @Override
    public Set<String> getAkzSet() {
        return null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return null;
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return null;
    }
}
