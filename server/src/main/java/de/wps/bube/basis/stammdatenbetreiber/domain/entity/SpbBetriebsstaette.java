package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;

/**
 * Betriebsstättendaten der Betreiber
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class SpbBetriebsstaette {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "spb_betriebsstaette_seq")
    @SequenceGenerator(name = "spb_betriebsstaette_seq", allocationSize = 50)
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    private Betriebsstaette betriebsstaette;

    @OneToOne
    private SpbBetreiber spbBetreiber;

    @Embedded
    private SpbAdresse adresse;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ansprechpartner> ansprechpartner;

    @ElementCollection
    private List<Kommunikationsverbindung> kommunikationsverbindungen;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    // hat EinleiterNr-n 0..n
    @ElementCollection
    private List<EinleiterNr> einleiterNummern;

    // hat AbfallENr-n // Abfallerzeugernr. 0..n
    @ElementCollection
    private List<AbfallerzeugerNr> abfallerzeugerNummern;

    private String name;

    @ManyToOne
    private Referenz flusseinzugsgebiet;
    @ManyToOne
    private Referenz naceCode;
    private boolean direkteinleiter;
    private boolean indirekteinleiter;

    @ManyToOne
    private Referenz betriebsstatus;

    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;

    @NotNull
    private Instant ersteErfassung;

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected SpbBetriebsstaette() {
    }

    public SpbBetriebsstaette(Betriebsstaette betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
        this.initializeFromBetriebsstaette();
    }

    public Betriebsstaette getBetriebsstaette() {
        return betriebsstaette;
    }

    public void setBetriebsstaette(Betriebsstaette betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SpbBetreiber getSpbBetreiber() {
        return spbBetreiber;
    }

    public void setSpbBetreiber(SpbBetreiber spbBetreiber) {
        this.spbBetreiber = spbBetreiber;
    }

    public SpbAdresse getAdresse() {
        return adresse;
    }

    public void setAdresse(SpbAdresse adresse) {
        this.adresse = adresse;
    }

    public List<Ansprechpartner> getAnsprechpartner() {
        return ansprechpartner;
    }

    public void setAnsprechpartner(List<Ansprechpartner> ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
    }

    public List<Kommunikationsverbindung> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    public void setKommunikationsverbindungen(List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public List<EinleiterNr> getEinleiterNummern() {
        return einleiterNummern;
    }

    public void setEinleiterNummern(List<EinleiterNr> einleiterNummern) {
        this.einleiterNummern = einleiterNummern;
    }

    public List<AbfallerzeugerNr> getAbfallerzeugerNummern() {
        return abfallerzeugerNummern;
    }

    public void setAbfallerzeugerNummern(List<AbfallerzeugerNr> abfallerzeugerNummern) {
        this.abfallerzeugerNummern = abfallerzeugerNummern;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Referenz getFlusseinzugsgebiet() {
        return flusseinzugsgebiet;
    }

    public void setFlusseinzugsgebiet(Referenz flusseinzugsgebiet) {
        this.flusseinzugsgebiet = flusseinzugsgebiet;
    }

    public Referenz getNaceCode() {
        return naceCode;
    }

    public void setNaceCode(Referenz naceCode) {
        this.naceCode = naceCode;
    }

    public boolean isDirekteinleiter() {
        return direkteinleiter;
    }

    public void setDirekteinleiter(boolean istDirekteinleiter) {
        this.direkteinleiter = istDirekteinleiter;
    }

    public boolean isIndirekteinleiter() {
        return indirekteinleiter;
    }

    public void setIndirekteinleiter(boolean istIndirekteinleiter) {
        this.indirekteinleiter = istIndirekteinleiter;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDate getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDate getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpbBetriebsstaette that = (SpbBetriebsstaette) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    private void initializeFromBetriebsstaette() {
        this.name = betriebsstaette.getName();
        this.inbetriebnahme = betriebsstaette.getInbetriebnahme();
        this.betriebsstatus = betriebsstaette.getBetriebsstatus();
        this.betriebsstatusSeit = betriebsstaette.getBetriebsstatusSeit();
        this.naceCode = betriebsstaette.getNaceCode();
        this.flusseinzugsgebiet = betriebsstaette.getFlusseinzugsgebiet();
        this.geoPunkt = betriebsstaette.getGeoPunkt();
        this.direkteinleiter = betriebsstaette.isDirekteinleiter();
        this.indirekteinleiter = betriebsstaette.isIndirekteinleiter();
        this.ersteErfassung = betriebsstaette.getErsteErfassung();
        this.letzteAenderung = betriebsstaette.getLetzteAenderung();

        Adresse adresse = betriebsstaette.getAdresse();
        this.adresse = new SpbAdresse(adresse.getStrasse(), adresse.getHausNr(), adresse.getOrt(), adresse.getPlz());
        this.adresse.setLandIsoCode(adresse.getLandIsoCode());
        this.adresse.setOrtsteil(adresse.getOrtsteil());
        this.adresse.setPostfach(adresse.getPostfach());
        this.adresse.setPostfachPlz(adresse.getPostfachPlz());

        this.ansprechpartner = new ArrayList<>(betriebsstaette.getAnsprechpartner());
        this.kommunikationsverbindungen = new ArrayList<>(betriebsstaette.getKommunikationsverbindungen());
        this.einleiterNummern = new ArrayList<>(betriebsstaette.getEinleiterNummern());
        this.abfallerzeugerNummern = new ArrayList<>(betriebsstaette.getAbfallerzeugerNummern());
    }

}
