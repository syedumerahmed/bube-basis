package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlageVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class AnlageVergleichService {

    private final CommonVergleichService vergleichService;

    public AnlageVergleichService(
            CommonVergleichService vergleichService) {
        this.vergleichService = vergleichService;
    }

    public SpbAnlageVergleich vergleiche(Anlage spl, SpbAnlage spb) {
        SpbAnlageVergleich vergleich = new SpbAnlageVergleich();

        vergleich.setId(spl == null ? null : spl.getId());
        vergleich.setSpbId(spb == null ? null : spb.getId());

        vergleich.setName(new StringVector(spl == null ? null : spl.getName(), spb == null ? null : spb.getName()));
        if (spl != null && !spl.isBetreiberdatenUebernommen() && spb != null) {
            vergleich.setAnlageNr(
                    new StringVector(spl.getAnlageNr(), spb.getAnlageNr()));

            vergleich.setGeoPunkt(
                    new GeoPunktVector(spl.getGeoPunkt(), spb.getGeoPunkt()));

            vergleich.setLeistungen(
                    vergleichService.mapLeistungen(spl.getLeistungen(), spb.getLeistungen()));

            vergleich.setBetriebsstatus(
                    new ReferenzVector(spl.getBetriebsstatus(),
                            spb.getBetriebsstatus()));

            vergleich.setBetriebsstatusSeit(new LocalDateVector(spl.getBetriebsstatusSeit(),
                    spb.getBetriebsstatusSeit()));
            vergleich.setInbetriebnahme(new LocalDateVector(spl.getInbetriebnahme(),
                    spb.getInbetriebnahme()));
        }
        if (spb == null) {
            vergleich.setHasChanges(false);
        } else {
            vergleich.setHasChanges(
                    (spl == null || (!spl.isBetreiberdatenUebernommen() && !allIsEqual(vergleich))));
        }

        return vergleich;
    }

    private boolean allIsEqual(SpbAnlageVergleich vergleich) {
        return vergleich.getAnlageNr().isEqual()
                && vergleich.getBetriebsstatus().isEqual()
                && vergleich.getBetriebsstatusSeit().isEqual()
                && vergleich.getGeoPunkt().isEqual()
                && vergleich.getInbetriebnahme().isEqual()
                && vergleich.getLeistungen().stream().allMatch(LeistungVector::isEqual)
                && vergleich.getName().isEqual();
    }
}
