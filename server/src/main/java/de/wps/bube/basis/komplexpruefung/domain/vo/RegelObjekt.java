package de.wps.bube.basis.komplexpruefung.domain.vo;

import java.util.EnumSet;

public enum RegelObjekt {
    BST("BST"),
    ANLAGE("Anlage"),
    ANLAGENTEIL("Anlagenteil"),
    QUELLE("Quelle"),
    BETREIBER("Betreiber"),
    EUREG_BST("EURegBST"),
    EUREG_ANLAGE("EURegAnl"),
    EUREG_F("EURegF"),
    TEILBERICHT("Teilbericht");

    private static final EnumSet<RegelObjekt> EUREG_OBJEKTE = EnumSet.of(EUREG_ANLAGE, EUREG_BST, EUREG_F, TEILBERICHT);

    private final String bezeichner;

    RegelObjekt(String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public boolean isEURegObjekt() {
        return EUREG_OBJEKTE.contains(this);
    }

}
