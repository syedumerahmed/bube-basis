package de.wps.bube.basis.stammdaten.security;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDE_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BENUTZERVERWALTUNG_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BETREIBER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.annotation.Secured;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Secured({ BEHOERDE_READ, LAND_READ, BETREIBER, BENUTZERVERWALTUNG_READ })
public @interface SecuredStammdatenRead {
}
