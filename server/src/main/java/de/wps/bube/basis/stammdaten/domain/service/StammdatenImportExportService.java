package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;
import static java.lang.String.format;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobsException;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.event.StammdatenImportTriggeredEvent;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapper;
import de.wps.bube.basis.stammdaten.xml.StammdatenImportException;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;
import de.wps.bube.basis.stammdaten.xml.dto.ObjectFactory;

@Service
@Transactional
@Secured(LAND_READ)
public class StammdatenImportExportService {

    private final StammdatenService stammdatenService;
    private final XMLService xmlService;
    private final BetriebsstaetteXMLMapper betriebsstaetteXMLMapper;
    private final ApplicationEventPublisher eventPublisher;
    private final DateiService dateiService;
    private final AktiverBenutzer aktiverBenutzer;
    private final ParameterService parameterService;
    private final ReferenzenService referenzenService;

    public StammdatenImportExportService(StammdatenService stammdatenService, XMLService xmlService,
                                         BetriebsstaetteXMLMapper betriebsstaetteXMLMapper, ApplicationEventPublisher eventPublisher,
                                         DateiService dateiService, AktiverBenutzer aktiverBenutzer, ParameterService parameterService, ReferenzenService referenzenService) {
        this.stammdatenService = stammdatenService;
        this.xmlService = xmlService;
        this.betriebsstaetteXMLMapper = betriebsstaetteXMLMapper;
        this.eventPublisher = eventPublisher;
        this.dateiService = dateiService;
        this.aktiverBenutzer = aktiverBenutzer;
        this.parameterService = parameterService;
        this.referenzenService = referenzenService;
    }

    @Transactional(readOnly = true)
    public void exportStammdaten(ByteArrayOutputStream outputStream, List<Long> betriebsstaetten) {
        if (Land.DEUTSCHLAND.equals(aktiverBenutzer.getLand()) && !stammdatenService.haveSameLand(
                betriebsstaetten)) {
            throw new JobsException("Ausgewählte Betriebsstätten sind unterschiedlichen Bundesländern zugeordnet");
        }
        var stammdatenStream = betriebsstaetten.stream()
                .map(stammdatenService::loadBetriebsstaetteForRead);

        var objectFactory = new ObjectFactory();

        var writer = xmlService.createWriter(BetriebsstaetteType.class, BetriebsstaettenXDto.QUALIFIED_NAME,
                (Betriebsstaette betriebsstaette) -> betriebsstaetteXMLMapper.toXDto(betriebsstaette), objectFactory::createBetriebsstaette);

        writer.write(stammdatenStream, outputStream);
    }

    @Secured(LAND_WRITE)
    @Transactional(readOnly = true)
    public String existierendeDatei() {
        return dateiService.existingDatei(aktiverBenutzer.getUsername(), DateiScope.STAMMDATEN);
    }

    @Secured(LAND_DELETE)
    public void dateiSpeichern(MultipartFile file) {
        dateiService.dateiSpeichern(file, aktiverBenutzer.getUsername(), DateiScope.STAMMDATEN);
    }

    @Secured(LAND_DELETE)
    @Transactional(readOnly = true)
    public List<Validierungshinweis> validiere() {
        List<Validierungshinweis> hinweise = new ArrayList<>();

        try {
            // Das XML-Schema Validieren
            InputStream inputStream = dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.STAMMDATEN);
            xmlService.validate(inputStream, BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);

        } catch (XMLImportException e) {
            hinweise.add(Validierungshinweis.of(e.getLocalizedMessage(), Validierungshinweis.Severity.FEHLER));
        } catch (Exception e) {
            throw new StammdatenImportException("Es ist ein Fehler bei der Validierung aufgetreten", e);
        }

        return hinweise;
    }

    /**
     * Wird vom Stammdaten-Importieren-Job Aufgerufen
     */
    @Secured(LAND_DELETE)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importiereBetriebsstaette(EntityWithError<Betriebsstaette> betriebsstaette, String berichtsjahr,
                                          JobProtokoll jobProtokoll) {
        if (!validiereImport(betriebsstaette, berichtsjahr, jobProtokoll)) {
            throw new StammdatenImportException(betriebsstaette.entity.getBetriebsstaetteNr(),
                    betriebsstaette.entity.getName());
        }
        importiere(betriebsstaette.entity, jobProtokoll);
    }

    private boolean validiereImport(EntityWithError<Betriebsstaette> bstWithErrors, String berichtsjahr,
                                    JobProtokoll jobProtokoll) {
        var erfolg = true;
        var betriebsstaette = bstWithErrors.entity;

        ParameterWert pw = parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                Gueltigkeitsjahr.of(berichtsjahr), betriebsstaette.getLand());

        // Prüft die Zuordung der Quellen an einer Betriebsstätte
        if (pw.getWert().equals(ParameterWert.QUELLE_ZUORDNUNG_ANLAGE) && !betriebsstaette.getQuellen().isEmpty()) {
            jobProtokoll.addEintrag(
                    format("Betriebsstätte %s Nr. %s sollte keine Quellen haben", betriebsstaette.getName(),
                            betriebsstaette.getBetriebsstaetteNr()));
            erfolg = false;
        }

        // Prüft die Zuordung der Quellen an den Anlagen einer Betriebsstätte
        if (pw.getWert().equals(ParameterWert.QUELLE_ZUORDNUNG_BST)) {
            Optional<Anlage> optAnlage = betriebsstaette.getAnlagen()
                    .stream()
                    .filter(anlage -> !anlage.getQuellen().isEmpty())
                    .findFirst();

            if (optAnlage.isPresent()) {
                jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s sollte an den Anlagen keine Quellen haben",
                        betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr()));
                erfolg = false;
            }
        }

        // Der Import gilt immer für das ausgewählte Berichtsjahr.
        if (betriebsstaette.getBerichtsjahr() == null || !betriebsstaette.getBerichtsjahr().getSchluessel().equals(berichtsjahr)) {
            jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s falsches Berichtsjahr", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()));
            erfolg = false;
        }

        // Es werden immer nur die Daten des angemeldeten Benutzer-Landes importiert.
        // Nur der Gesamtadmin kann Daten anderer Länder importieren
        if (!aktiverBenutzer.isGesamtadmin() && !betriebsstaette.getLand().equals(aktiverBenutzer.getLand())) {
            jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s falsches Land", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()));
            erfolg = false;
        }

        // Die Anlagen und Anlagenteile sollen keine doppelten Vorschriften haben.
        if (betriebsstaette.hasDoppelteVorschriften()) {
            jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s: Import nicht möglich, da Vorschrift mehrfach erfasst.", betriebsstaette.getName(),
                    betriebsstaette.getBetriebsstaetteNr()));
            erfolg = false;
        }

        for (MappingError mappingError : bstWithErrors.errorsOrEmpty()) {
            if (!mappingError.istWarnung()) {
                erfolg = false;
            }
            jobProtokoll.addEintrag(mappingError);
        }

        return erfolg;
    }

    private void importiere(Betriebsstaette betriebsstaette, JobProtokoll jobProtokoll) {

        // Ist die Betriebsstätte schon vorhanden dann vor dem Import löschen
        Betriebsstaette read = stammdatenService.findBetriebsstaetteByJahrLandNummer(
                betriebsstaette.getBerichtsjahr().getSchluessel(), betriebsstaette.getLand(),
                betriebsstaette.getBetriebsstaetteNr()).orElse(null);
        if (read != null) {

            //Wird die Betriebsstätte noch durch den Betreiber bearbeitet, dann darf sie nicht neu importiert werden
            if (read.getLetzteAenderungBetreiber() != null) {
                jobProtokoll.addWarnung(
                        format("Betriebsstätte %s Nr. %s. Der Import konnte nicht durchgeführt werden, da Daten im SPB nicht geprüft und übernommen wurden.",
                                read.getName(),
                                read.getBetriebsstaetteNr()));
                return;
            }

            stammdatenService.deleteBetriebsstaette(read.getId());
            jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s bereits vorhanden. Gelöscht", read.getName(),
                    read.getBetriebsstaetteNr()));
        }

        // Betreiber prüfen und falls schon vorhanden updaten
        Betreiber betreiber = betriebsstaette.getBetreiber();
        if (betreiber != null) {
            Optional<Betreiber> existing = stammdatenService.findBetreiberByJahrLandNummer(
                    betreiber.getBerichtsjahr().getSchluessel(), betreiber.getLand(), betreiber.getBetreiberNummer());
            if (existing.isPresent()) {
                betreiber.setId(existing.get().getId());
                stammdatenService.updateBetreiber(betreiber);
            } else {
                stammdatenService.createBetreiber(betreiber);
            }
        }

        // Betriebsstätte anlegen
        stammdatenService.createBetriebsstaette(betriebsstaette);
        jobProtokoll.addEintrag(format("Betriebsstätte %s Nr. %s. Fehlerfrei und angelegt", betriebsstaette.getName(),
                betriebsstaette.getBetriebsstaetteNr()));
    }

    @Secured(LAND_DELETE)
    public void triggerImport(String berichtsjahr) {
        eventPublisher.publishEvent(new StammdatenImportTriggeredEvent(aktiverBenutzer.getUsername(), berichtsjahr));
    }

    @Secured(LAND_DELETE)
    public void dateiLoeschen() {
        dateiService.loescheDatei(aktiverBenutzer.getUsername(), DateiScope.STAMMDATEN);
    }
}
