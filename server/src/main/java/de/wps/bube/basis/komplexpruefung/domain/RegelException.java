package de.wps.bube.basis.komplexpruefung.domain;

public class RegelException extends RuntimeException {
    public RegelException(String message) {
        super(message);
    }

    public RegelException(String message, Exception e) {
        super(message, e);
    }
}
