package de.wps.bube.basis.euregistry.persistence;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.QReferenz.referenz;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;

import de.wps.bube.basis.base.persistence.JoinProperties;
import de.wps.bube.basis.base.persistence.QBeanWithPropertyCheck;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteListItem;

public interface EURegBetriebsstaetteRepository
        extends JpaRepository<EURegBetriebsstaette, Long>, QuerydslPredicateProjectionRepository<EURegBetriebsstaette> {

    static Predicate berichtsjahrIdEquals(long berichtsjahrId) {
        return eURegBetriebsstaette.betriebsstaette.berichtsjahr.id.eq(berichtsjahrId);
    }

    static Predicate byStammdatenKey(String jahrSchluessel, Land land, String nummer) {
        var bst = eURegBetriebsstaette.betriebsstaette;
        return bst.land.eq(land)
                       .and(bst.berichtsjahr.schluessel.eq(jahrSchluessel))
                       .and(bst.betriebsstaetteNr.equalsIgnoreCase(nummer));
    }


    List<EURegBetriebsstaette> findAll(Predicate predicate);

    static Predicate byJahrAndLocalId(String jahrSchluessel, String localId) {
        var bst = eURegBetriebsstaette.betriebsstaette;
        return bst.berichtsjahr.schluessel.eq(jahrSchluessel).and(bst.localId.eq(localId));
    }

    default Page<EuRegBetriebsstaetteListItem> findAllAsListItems(Predicate predicate, Pageable page) {
        return findAll(predicate, page, QBeanWithPropertyCheck.fields(EuRegBetriebsstaetteListItem.class,
                eURegBetriebsstaette.id,
                eURegBetriebsstaette.betriebsstaette.id.as("bstId"),
                eURegBetriebsstaette.berichtsdaten.zustaendigeBehoerde.bezeichnung.as("zustaendigeBehoerde"),
                eURegBetriebsstaette.betriebsstaette.betriebsstaetteNr, eURegBetriebsstaette.betriebsstaette.name,
                eURegBetriebsstaette.betriebsstaette.adresse.plz, eURegBetriebsstaette.betriebsstaette.adresse.ort,
                eURegBetriebsstaette.betriebsstaette.betriebsstatus.ktext.as("betriebsstatus"),
                eURegBetriebsstaette.betriebsstaette.land, eURegBetriebsstaette.betriebsstaette.localId,
                eURegBetriebsstaette.pruefungsfehler, eURegBetriebsstaette.pruefungVorhanden),
                new JoinProperties<>(eURegBetriebsstaette.berichtsdaten.zustaendigeBehoerde, behoerde),
                new JoinProperties<>(eURegBetriebsstaette.betriebsstaette.betriebsstatus, referenz));
    }

    boolean existsByBetriebsstaetteId(Long betriebsstaetteId);

    Long deleteByBetriebsstaetteId(Long betriebsstaetteId);

    default List<Long> findAllBstIds(Predicate predicate) {
        return findAll(predicate, Projections.constructor(Long.class, eURegBetriebsstaette.betriebsstaette.id));
    }

    default List<Long> findAllIds(Predicate predicate) {
        return findAll(predicate, Projections.constructor(Long.class, eURegBetriebsstaette.id));
    }

    Stream<EURegBetriebsstaette> streamAllByBetriebsstaetteIdIn(Collection<Long> ids);

    Stream<EURegBetriebsstaette> streamAllByIdIn(Collection<Long> ids);

    @Query("select count (distinct betriebsstaette.land) from EURegBetriebsstaette where id in :ids")
    long countDistinctByLandAndIdIn(Collection<Long> ids);

    @Query("select count (distinct betriebsstaette.berichtsjahr) from EURegBetriebsstaette where id in :ids")
    long countDistinctByJahrAndIdIn(Collection<Long> ids);

}
