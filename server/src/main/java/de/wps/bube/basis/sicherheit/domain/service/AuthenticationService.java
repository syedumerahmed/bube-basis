package de.wps.bube.basis.sicherheit.domain.service;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

public interface AuthenticationService {
    AktiverBenutzer getCurrentBenutzer();
}
