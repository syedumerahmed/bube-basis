package de.wps.bube.basis.base.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.base.xml.XMLExportException;
import de.wps.bube.basis.base.xml.XMLImportException;

@ControllerAdvice
public class XMLExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ XMLImportException.class })
    protected ResponseEntity<Object> handleXmlException(XMLImportException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ XMLExportException.class })
    protected ResponseEntity<Object> handleXmlException(XMLExportException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }
}
