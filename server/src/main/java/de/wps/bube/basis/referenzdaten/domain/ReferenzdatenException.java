package de.wps.bube.basis.referenzdaten.domain;

public class ReferenzdatenException extends RuntimeException {
    public ReferenzdatenException(String message) {
        super(message);
    }

    public ReferenzdatenException(String message, Exception e) {
        super(message, e);
    }
}
