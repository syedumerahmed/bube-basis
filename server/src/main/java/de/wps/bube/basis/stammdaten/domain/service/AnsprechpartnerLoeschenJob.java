package de.wps.bube.basis.stammdaten.domain.service;

import java.util.stream.Stream;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;

@Service
public class AnsprechpartnerLoeschenJob {

    public final static JobStatusEnum JOB_NAME = JobStatusEnum.ANSPRECHPARTNER_LOESCHEN;
    private final Logger logger = LoggerFactory.getLogger(AnsprechpartnerLoeschenJob.class);

    private final BetriebsstaetteRepository betriebsstaetteRepository;
    private final EntityManager entityManager;
    private final BenutzerJobRegistry jobController;

    public AnsprechpartnerLoeschenJob(final BenutzerJobRegistry jobController,
            final BetriebsstaetteRepository betriebsstaetteRepository, final EntityManager entityManager) {
        this.betriebsstaetteRepository = betriebsstaetteRepository;
        this.entityManager = entityManager;
        this.jobController = jobController;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(final Referenz berichtsjahr, final String username, final Land land) {

        try {
            final Stream<Betriebsstaette> stream = betriebsstaetteRepository.findAllByBerichtsjahr(berichtsjahr);

            stream.filter(betriebsstaette -> filterForLand(land, betriebsstaette))
                  .forEach(betriebsstaette -> {
                      betriebsstaette.removeAllAnsprechpartner();
                      //save alleine reicht nicht aus
                      betriebsstaetteRepository.saveAndFlush(betriebsstaette);
                      // Detach the entity so garbage collector can reclaim the memory.
                      entityManager.detach(betriebsstaette);
                      jobController.jobCountChanged(1);
                  });

            jobController.jobCompleted(false);

        } catch (final Exception e) {
            jobController.jobError(e);
        }
    }

    private boolean filterForLand(Land land, Betriebsstaette betriebsstaette) {
        if (land.equals(Land.DEUTSCHLAND)) {
            return true;
        } else {
            return betriebsstaette.getLand().equals(land);
        }
    }
}
