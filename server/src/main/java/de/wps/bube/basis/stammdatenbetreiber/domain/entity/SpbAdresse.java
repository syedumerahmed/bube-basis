package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

/**
 * Adressdaten der Betreiber
 */
@Embeddable
public class SpbAdresse {

    private String strasse;
    private String hausNr;
    @NotEmpty
    private String plz;
    @NotEmpty
    private String ort;
    private String ortsteil;
    private String postfachPlz;
    private String postfach;
    @ManyToOne
    private Referenz landIsoCode;

    @Deprecated
    protected SpbAdresse() {
    }

    public SpbAdresse(String strasse, String hausNr, String ort, String plz) {
        this.strasse = strasse;
        this.hausNr = hausNr;
        this.ort = ort;
        this.plz = plz;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausNr() {
        return hausNr;
    }

    public void setHausNr(String strasseNr) {
        this.hausNr = strasseNr;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getPostfachPlz() {
        return postfachPlz;
    }

    public void setPostfachPlz(String plzPostfach) {
        this.postfachPlz = plzPostfach;
    }

    public String getPostfach() {
        return postfach;
    }

    public void setPostfach(String postfach) {
        this.postfach = postfach;
    }

    public Referenz getLandIsoCode() {
        return landIsoCode;
    }

    public void setLandIsoCode(Referenz landISOCode) {
        this.landIsoCode = landISOCode;
    }

    public String getOrtsteil() {
        return ortsteil;
    }

    public void setOrtsteil(String ortsteil) {
        this.ortsteil = ortsteil;
    }
}
