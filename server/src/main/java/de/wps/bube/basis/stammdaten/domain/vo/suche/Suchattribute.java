package de.wps.bube.basis.stammdaten.domain.vo.suche;

public class Suchattribute {

    public BetriebsstaetteSuchattribute betriebsstaette = new BetriebsstaetteSuchattribute();

    public BetreiberSuchattribute betreiber = new BetreiberSuchattribute();

    public AnlageSuchattribute anlage = new AnlageSuchattribute();
}
