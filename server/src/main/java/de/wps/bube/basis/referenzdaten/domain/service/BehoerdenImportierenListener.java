package de.wps.bube.basis.referenzdaten.domain.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdenImportTriggeredEvent;

@Component
@Transactional
public class BehoerdenImportierenListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BehoerdenImportierenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final BehoerdenImportierenJob job;

    public BehoerdenImportierenListener(BenutzerJobRegistry jobRegistry, BehoerdenImportierenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleBehoerdenImportieren(BehoerdenImportTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", BehoerdenImportierenJob.JOB_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(BehoerdenImportierenJob.JOB_NAME));

        job.runAsync(event.username);
    }
}
