package de.wps.bube.basis.euregistry.xml.bericht.dto;

public class AttributeNames {
    public static final String HREF = "href";
    public static final String GML_ID = "id";

    private AttributeNames() {}
}
