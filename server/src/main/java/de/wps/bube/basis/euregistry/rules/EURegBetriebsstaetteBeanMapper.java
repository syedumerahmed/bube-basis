package de.wps.bube.basis.euregistry.rules;

import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.domain.service.XeurTeilberichtCrudService;
import de.wps.bube.basis.euregistry.rules.bean.EURegBetriebsstaetteBean;
import de.wps.bube.basis.euregistry.rules.bean.TeilberichtBean;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.stammdaten.rules.BetriebsstaetteBeanMapper;

@Mapper(uses = { BetriebsstaetteBeanMapper.class, BerichtsdatenBeanMapper.class })
public abstract class EURegBetriebsstaetteBeanMapper {

    @Autowired
    @Lazy
    protected EURegistryService euRegistryService;
    @Autowired
    @Lazy
    protected XeurTeilberichtCrudService xeurTeilberichtCrudService;

    @Mapping(target = "vorgaenger", expression = "java(vorgaenger(euRegBetriebsstaette, context, mappers))")
    @Mapping(target = "nachfolger", expression = "java(nachfolger(euRegBetriebsstaette, context, mappers))")
    @Mapping(target = "teilbericht", expression = "java(teilbericht(euRegBetriebsstaette, context, mappers))")
    public abstract EURegBetriebsstaetteBean toBean(EURegBetriebsstaette euRegBetriebsstaette,
            @Context BeanMappingContext context, @Context EURegBeanMappers mappers);

    protected Supplier<EURegBetriebsstaetteBean> vorgaenger(EURegBetriebsstaette euRegBetriebsstaette,
            @Context BeanMappingContext context, @Context EURegBeanMappers mappers) {
        return () -> {
            var betriebsstaette = euRegBetriebsstaette.getBetriebsstaette();
            var vorgaengerJahr = String.valueOf(Integer.parseInt(betriebsstaette.getBerichtsjahr().getSchluessel()) - 1);
            return euRegistryService
                    .findBetriebsstaettenBerichtByJahrAndLocalId(vorgaengerJahr, betriebsstaette.getLocalId())
                    .map(b -> mappers.getEuRegBetriebsstaetteBeanMapper().toBean(b, context, mappers))
                    .orElse(null);
        };
    }

    protected Supplier<EURegBetriebsstaetteBean> nachfolger(EURegBetriebsstaette euRegBetriebsstaette,
            @Context BeanMappingContext context, @Context EURegBeanMappers mappers) {
        return () -> {
            var betriebsstaette = euRegBetriebsstaette.getBetriebsstaette();
            var nachfolgerJahr = String.valueOf(Integer.parseInt(betriebsstaette.getBerichtsjahr().getSchluessel()) + 1);
            return euRegistryService
                    .findBetriebsstaettenBerichtByJahrAndLocalId(nachfolgerJahr, betriebsstaette.getLocalId())
                    .map(b -> mappers.getEuRegBetriebsstaetteBeanMapper().toBean(b, context, mappers))
                    .orElse(null);
        };
    }

    protected Supplier<TeilberichtBean> teilbericht(EURegBetriebsstaette euRegBetriebsstaette,
            @Context BeanMappingContext context, @Context EURegBeanMappers mappers) {
        return () -> xeurTeilberichtCrudService.findLatestFreigegeben(
                euRegBetriebsstaette.getBetriebsstaette().getBerichtsjahr().getId(),
                context.getLand().getNr(),
                euRegBetriebsstaette.getLocalId()
        ).map(t -> mappers.getTeilberichtBeanMapper().toBean(t, context, mappers)).orElse(null);
    }

}
