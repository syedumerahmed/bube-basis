package de.wps.bube.basis.stammdatenbetreiber.web;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.web.openapi.api.SpbAnlagenteilApi;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlagenteilDto;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlagenteilListItemDto;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;

@RestController
class SpbAnlagenteilRestController implements SpbAnlagenteilApi {

    private final StammdatenBetreiberService service;
    private final SpbAnlagenteilMapper mapper;

    SpbAnlagenteilRestController(
            StammdatenBetreiberService service, SpbAnlagenteilMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<SpbAnlagenteilListItemDto>> listAllSpbAnlagenteile(Long anlageId,
            Long betriebsstaetteId) {
        return ResponseEntity.ok(
                service.listAllSpbAnlagenteileAsListItemByAnlageId(anlageId, betriebsstaetteId).stream()
                       .map(mapper::toListItemDto)
                       .collect(
                               Collectors.toList()));
    }

    @Override
    public ResponseEntity<List<SpbAnlagenteilListItemDto>> listAllNeueSpbAnlagenteile(Long anlageId) {
        return ResponseEntity.ok(
                service.listAllNeueSpbAnlagenteileAsListItemByAnlageId(anlageId).stream()
                       .map(mapper::toListItemDto)
                       .collect(
                               Collectors.toList()));
    }

    @Override
    public ResponseEntity<List<SpbAnlagenteilListItemDto>> listAllSpbAnlagenteileAnSpbAnlage(Long spbAnlageId,
            Long betriebsstaetteId) {
        return ResponseEntity.ok(
                service.listAllSpbAnlagenteileAsListItemBySpbAnlageId(spbAnlageId, betriebsstaetteId).stream()
                       .map(mapper::toListItemDto)
                       .collect(
                               Collectors.toList()));
    }

    @Override
    public ResponseEntity<SpbAnlagenteilDto> readSpbAnlagenteil(Long betriebsstaetteId, Long parentAnlageId,
            String anlagenteilNummer) {
        return ResponseEntity.ok(
                mapper.toDto(service.loadSpbAnlagenteilByAnlageteilNummer(betriebsstaetteId, parentAnlageId, anlagenteilNummer, false)));
    }

    @Override
    public ResponseEntity<SpbAnlagenteilDto> readSpbAnlagenteilAnSpbAnlage(Long betriebsstaetteId,
            Long parentSpbAnlageId, String anlagenteilNummer) {
        return ResponseEntity.ok(mapper.toDto(
                service.loadSpbAnlagenteilByAnlageteilNummer(betriebsstaetteId, parentSpbAnlageId, anlagenteilNummer, true)));
    }

    @Override
    public ResponseEntity<SpbAnlagenteilDto> createSpbAnlagenteil(@Valid SpbAnlagenteilDto spbAnlagenteilDto) {
        return ResponseEntity.ok(mapper.toDto(service.createSpbAnlagenteil(mapper.fromDto(spbAnlagenteilDto))));
    }

    @Override
    public ResponseEntity<SpbAnlagenteilDto> updateSpbAnlagenteil(@Valid SpbAnlagenteilDto spbAnlagenteilDto) {
        return ResponseEntity.ok(mapper.toDto(service.updateSpbAnlagenteil(mapper.fromDto(spbAnlagenteilDto))));
    }

    @Override
    public ResponseEntity<Void> deleteSpbAnlagenteil(Long betriebsstaetteId, Long id) {
        service.deleteSpbAnlagenteil(betriebsstaetteId, id);
        return ResponseEntity.ok().build();
    }

}
