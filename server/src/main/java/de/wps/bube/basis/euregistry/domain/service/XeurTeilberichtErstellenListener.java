package de.wps.bube.basis.euregistry.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.euregistry.domain.event.XeurTeilberichtErstellenTriggeredEvent;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Component
public class XeurTeilberichtErstellenListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(XeurTeilberichtErstellenListener.class);

    private final BenutzerJobRegistry jobRegistry;
    private final XeurTeilberichtErstellenJob job;

    public XeurTeilberichtErstellenListener(BenutzerJobRegistry jobRegistry, XeurTeilberichtErstellenJob job) {
        this.jobRegistry = jobRegistry;
        this.job = job;
    }

    @EventListener
    public void handleXeurTeilberichtErstellen(XeurTeilberichtErstellenTriggeredEvent event) {
        LOGGER.info("Event handling started for '{}'", XeurTeilberichtErstellenTriggeredEvent.EVENT_NAME);

        // Job muss vor dem asynchronen Job-Aufruf gestartet werden.
        // Evtl. landet der Job erstmal in der Warteschlange.
        jobRegistry.jobStarted(new JobStatus(XeurTeilberichtErstellenJob.JOB_NAME));

        job.runAsync(event.berichtIds(), event.berichtsjahr(), event.land());
    }
}
