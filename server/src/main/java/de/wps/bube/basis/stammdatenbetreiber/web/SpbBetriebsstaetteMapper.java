package de.wps.bube.basis.stammdatenbetreiber.web;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteListItemDto;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;

@Mapper(uses = { ReferenzMapper.class, BehoerdenMapper.class })
public abstract class SpbBetriebsstaetteMapper {

    @Autowired
    private StammdatenService stammdatenService;

    @Mapping(target = "betriebsstaetteId", source = "betriebsstaette.id")
    @Mapping(target = "id", source = "id")
    @Mapping(target = "berichtsjahr", source = "betriebsstaette.berichtsjahr")
    @Mapping(target = "land", source = "betriebsstaette.land")
    @Mapping(target = "zustaendigeBehoerde", source = "betriebsstaette.zustaendigeBehoerde")
    @Mapping(target = "akz", source = "betriebsstaette.akz")
    @Mapping(target = "betriebsstaetteNr", source = "betriebsstaette.betriebsstaetteNr")
    @Mapping(target = "schreibsperreBetreiber", source = "betriebsstaette.schreibsperreBetreiber")
    public abstract SpbBetriebsstaetteDto toDto(SpbBetriebsstaette betriebsstaette);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "betriebsstaette", source = "betriebsstaetteId")
    @Mapping(target = "spbBetreiber", ignore = true)
    public abstract SpbBetriebsstaette fromDto(SpbBetriebsstaetteDto betriebsstaetteDto);

    @Mapping(target = "adresseStrasse", source = "strasse")
    @Mapping(target = "adressePlz", source = "plz")
    @Mapping(target = "adresseOrt", source = "ort")
    @Mapping(target = "adresseHausNr", source = "hausNr")
    @Mapping(target = "zustaendigeBehoerde", source = "zustaendigeBehoerde.bezeichnung")
    @Mapping(target = "nummer", source = "betriebsstaetteNr")
    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    abstract SpbBetriebsstaetteListItemDto toListDto(BetriebsstaetteListItemView listItem);

    protected Betriebsstaette betriebsstaetteFromDto(Long betriebsstaetteId) {
        return stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);
    }

    static String einleiterNrToDto(EinleiterNr einleiterNr) {
        return einleiterNr.getEinleiterNr();
    }

    static EinleiterNr einleiterNrFromDto(String einleiterNr) {
        return new EinleiterNr(einleiterNr);
    }

    static String abfallerzeugerNummerToDto(AbfallerzeugerNr abfallerzeugerNummer) {
        return abfallerzeugerNummer.getAbfallerzeugerNr();
    }

    static AbfallerzeugerNr abfallerzeugerNummerFromDto(String abfallerzeugerNummer) {
        return new AbfallerzeugerNr(abfallerzeugerNummer);
    }

    protected String behoerdeToDto(Behoerde behoerde) {
        if (behoerde == null) {
            return "";
        }
        if (behoerde.getSchluessel() == null) {
            return behoerde.getBezeichnung();
        } else if (behoerde.getBezeichnung() == null) {
            return behoerde.getSchluessel();
        } else {
            return behoerde.getSchluessel() + " - " + behoerde.getBezeichnung();
        }
    }

    @AfterMapping
    protected void clearHiddenFields(@MappingTarget SpbBetriebsstaetteDto dto) {
        dto.getAnsprechpartner().forEach(ansprechpartnerDto -> ansprechpartnerDto.setBemerkung(null));
    }

}
