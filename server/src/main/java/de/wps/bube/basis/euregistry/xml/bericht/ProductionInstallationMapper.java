package de.wps.bube.basis.euregistry.xml.bericht;

import java.util.List;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ValueMapping;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.Auflage;
import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.euregistry.domain.entity.Genehmigung;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.BATDerogation;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.CompetentAuthorityEPRTR;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.IEDAnnexIActivity;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.PermitDetails;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionInstallation;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.StricterPermitConditionsType;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;

@Mapper(uses = { CommonEURegMapper.class, ProductionInstallationPartMapper.class })
public abstract class ProductionInstallationMapper {

    @Mapping(target = "inspireID", source = ".")
    @Mapping(target = "thematicID.identifier", source = "berichtsdaten.thematicId")
    @Mapping(target = "thematicID.identifierScheme", source = "berichtsdaten.schema")
    @Mapping(target = "geometry", source = "geoPunkt")
    @Mapping(target = "installationName.nameOfFeature", source = "name")
    @Mapping(target = "installationName.confidentialityReason", source = "vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    @Mapping(target = "baselineReportIndicator.schema", constant = SchemaSet.BASELINE_REPORT_INDICATOR_CODES)
    @Mapping(target = "baselineReportIndicator.code", source = "report")
    @Mapping(target = "batDerogation", source = "bvtAusnahmen")
    @Mapping(target = "IEDAnnexIActivity", source = "IERLVorschrift")
    @Mapping(target = "permitDetails", source = "genehmigung")
    @Mapping(target = "otherRelevantChapters", source = "kapitelIeRl")
    @Mapping(target = "siteVisits.siteVisitNumber", source = "visits")
    @Mapping(target = "siteVisits.siteVisitURL", source = "visitUrl")
    @Mapping(target = "dateOfStartOfOperation", source = "inbetriebnahme")
    @Mapping(target = "status.statusType.schema", constant = SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES)
    @Mapping(target = "status.statusType.code", source = "betriebsstatus")
    @Mapping(target = "status.validFrom", source = "betriebsstatusSeit")
    @Mapping(target = "remarks", source = "berichtsdaten.bemerkung")
    @Mapping(target = "competentAuthorityPermits", source = "genehmigungsbehoerden")
    @Mapping(target = "competentAuthorityInspections", source = "ueberwachungsbehoerden")
    @Mapping(target = "eSPIRSIdentifier", source = "eSpirsNr")
    @Mapping(target = "ETSIdentifier", source = "tehgNr")
    @Mapping(target = "stricterPermitConditions", source = "auflagen")
    @Mapping(target = "publicEmissionMonitoring", source = "monitor")
    @Mapping(target = "publicEmissionMonitoringURL", source = "monitorUrl")
    @Mapping(target = "BATConclusion", source = "anwendbareBvt")
    @Mapping(target = "installationType.schema", constant = SchemaSet.INSTALLATION_TYPE_CODE)
    @Mapping(target = "installationType.code", expression = "java(eURegAnlage.getIERLVorschrift() != null ? \"IED\" : \"NONIED\")")
    @Mapping(target = "gmlId", expression = "java(context.getProductionSite().getGmlId() + euRegAnlage.getLocalId())")
    @Mapping(target = "productionInstallationPart", source = "EURegF")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "groupedInstallationPart", ignore = true)
    public abstract ProductionInstallation toXDto(EURegAnlage euRegAnlage,
            @Context ProductionInstallationMapperContext context);

    @AfterMapping
    protected void setGroupedInstallationPart(@MappingTarget ProductionInstallation productionInstallation) {
        if (productionInstallation.getProductionInstallationPart() != null) {
            ProductionInstallation.GroupedInstallationPart groupedInstallationPart
                    = new ProductionInstallation.GroupedInstallationPart();
            groupedInstallationPart.setHref("#" + productionInstallation.getProductionInstallationPart().getGmlId());
            productionInstallation.setGroupedInstallationPart(groupedInstallationPart);
        }
    }

    @Mapping(target = "derogationDurationEndDate", source = "endetAm")
    @Mapping(target = "derogationDurationStartDate", source = "beginntAm")
    @Mapping(target = "batael.schema", constant = SchemaSet.BATAEL_CODES)
    @Mapping(target = "batael.code", source = "emissionswertAusnahme")
    @Mapping(target = "publicReasonURL", source = "oeffentlicheBegruendungUrl")
    @Mapping(target = "batDerogationIndicator", constant = "true")
    public abstract BATDerogation toXDto(BVTAusnahme bvtAusnahme);

    @Mapping(target = "mainActivity", source = "haupttaetigkeit")
    @Mapping(target = "otherActivity", source = "taetigkeitenOhneHaupttaetigkeit")
    public abstract IEDAnnexIActivity toXDto(Vorschrift IERLVorschrift);

    @ValueMapping(target = "REPORT", source = "LIEGT_VOR")
    @ValueMapping(target = "NOREPORT", source = "LIEGT_NICHT_VOR")
    @ValueMapping(target = "NOTREQUIRED", source = "NICHT_ERFORDERLICH")
    abstract String mapBaselineReportIndicatorCode(Ausgangszustandsbericht ausgangszustandsbericht);

    public List<PermitDetails> mapToList(Genehmigung genehmigung) {
        return List.of(toXDto(genehmigung));
    }

    public List<String> mapToList(String value) {
        if (value == null) {
            return null;
        }
        return List.of(value);
    }

    @Mapping(target = "permitGranted", source = "erteilt")
    @Mapping(target = "permitReconsidered", source = "neuBetrachtet")
    @Mapping(target = "permitUpdated", source = "geaendert")
    @Mapping(target = "dateOfGranting", source = "erteilungsdatum")
    @Mapping(target = "dateOfLastUpdate", source = "aenderungsdatum")
    @Mapping(target = "permitURL", source = "url")
    @Mapping(target = "enforcementAction", source = "durchsetzungsmassnahmen")
    public abstract PermitDetails toXDto(Genehmigung genehmigung);

    @Mapping(target = "article18", source = "nachArt18")
    @Mapping(target = "article14_4", source = "nachArt14")
    @Mapping(target = "batael", source = "emissionswert")
    @Mapping(target = "stricterPermitConditionsIndicator", constant = "true")
    public abstract StricterPermitConditionsType toXDto(Auflage auflage);

    @Mapping(target = "organisationName", source = "bezeichnung")
    @Mapping(target = "individualName", source = "bezeichnung2")
    @Mapping(target = "electronicMailAddress", source = "email")
    @Mapping(target = "telephoneNo", source = "telefon")
    @Mapping(target = "faxNo", source = "fax")
    @Mapping(target = "address.streetName", source = "adresse.strasse")
    @Mapping(target = "address.buildingNumber", source = "adresse.hausNr")
    @Mapping(target = "address.city", source = "adresse.ort")
    @Mapping(target = "address.postalCode", source = "adresse.plz")
    @Mapping(target = "address.confidentialityReason", ignore = true)
    public abstract CompetentAuthorityEPRTR toXDto(Behoerde behoerde);

}
