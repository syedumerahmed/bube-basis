package de.wps.bube.basis.referenzdaten.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

public interface ParameterWertRepository extends JpaRepository<ParameterWert, Long> {

    List<ParameterWert> findByPschluessel(String pschluessel);

    List<ParameterWert> findByPschluesselAndLandIn(String pschluessel, List<Land> landList);

    @Query("from ParameterWert where land = :land and pschluessel = :schluessel and :jahr between gueltigVon and gueltigBis and behoerde_id is null ")
    Optional<ParameterWert> findByKeyWithoutBehoerde(Land land, String schluessel, Gueltigkeitsjahr jahr);

    @Query("from ParameterWert where land = :land and pschluessel = :schluessel and :jahr between gueltigVon and gueltigBis and behoerde_id = :behoerdeId")
    Optional<ParameterWert> findByKeyWithBehoerde(Land land, String schluessel, Gueltigkeitsjahr jahr, Long behoerdeId);

    boolean existsParameterwertByIdNotAndLandAndPschluesselAndGueltigVonBetweenAndBehoerde(Long id, Land land,
            String pschluessel,
            Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis, Behoerde behoerde);

    boolean existsParameterwertByIdNotAndLandAndPschluesselAndGueltigBisBetweenAndBehoerde(Long id, Land land,
            String pschluessel,
            Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis, Behoerde behoerde);

    boolean existsParameterwertByLandAndPschluesselAndGueltigVonBetweenAndBehoerde(Land land, String pschluessel,
            Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis, Behoerde behoerde);

    boolean existsParameterwertByLandAndPschluesselAndGueltigBisBetweenAndBehoerde(Land land, String pschluessel,
            Gueltigkeitsjahr gueltigVon, Gueltigkeitsjahr gueltigBis, Behoerde behoerde);

    boolean existsParameterWertByBehoerde(Behoerde behoerde);
}
