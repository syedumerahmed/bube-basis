package de.wps.bube.basis.stammdaten.domain.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

@Embeddable
public class ZustaendigeBehoerde {

    public static final String GENEHMIGUNG_SCHLUESSEL = "04";
    public static final String UEBERWACHUNG_SCHLUESSEL = "05";

    @NotNull
    @ManyToOne
    private Behoerde behoerde;

    @NotNull
    @ManyToOne
    private Referenz zustaendigkeit;

    @Deprecated
    protected ZustaendigeBehoerde() {
    }

    public ZustaendigeBehoerde(Behoerde behoerde, Referenz zustaendigkeit) {
        this.behoerde = behoerde;
        this.zustaendigkeit = zustaendigkeit;
    }

    public Behoerde getBehoerde() {
        return behoerde;
    }

    @NotNull
    public Referenz getZustaendigkeit() {
        return zustaendigkeit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ZustaendigeBehoerde that = (ZustaendigeBehoerde) o;

        if (!behoerde.equals(that.behoerde)) {
            return false;
        }
        return zustaendigkeit.equals(that.zustaendigkeit);
    }

    @Override
    public int hashCode() {
        int result = behoerde.hashCode();
        result = 31 * result + zustaendigkeit.hashCode();
        return result;
    }

    public void setZustaendigkeit(Referenz r) {
        Assert.notNull(r, "zustaendigkeit");
        this.zustaendigkeit = r;
    }

    public void setBehoerde(Behoerde b) {
        Assert.notNull(b, "behoerde");
        this.behoerde = b;
    }

    public boolean isGenehmigungsbehoerde() {
        return zustaendigkeit.getSchluessel().equals(GENEHMIGUNG_SCHLUESSEL);
    }

    public boolean isUeberwachungsbehoerde() {
        return zustaendigkeit.getSchluessel().equals(UEBERWACHUNG_SCHLUESSEL);
    }

}
