package de.wps.bube.basis.base.xml;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TimeZone;
import javax.xml.datatype.XMLGregorianCalendar;

import org.mapstruct.Mapper;

@Mapper
public abstract class CommonXMLMapper {
    public static ZonedDateTime map(Instant value) {
        return value != null ? ZonedDateTime.ofInstant(value, ZoneId.of("Europe/Berlin")) : null;
    }

    public static Instant map(XMLGregorianCalendar value) {
        if (value == null) {
            return null;
        }
        var gregorianCalendar = value.toGregorianCalendar();
        if (gregorianCalendar.getTimeZone() == null) {
            gregorianCalendar.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        }
        return gregorianCalendar.toInstant();
    }
}
