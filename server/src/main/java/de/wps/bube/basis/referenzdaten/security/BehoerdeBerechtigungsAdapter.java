package de.wps.bube.basis.referenzdaten.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;

public class BehoerdeBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {

    private final Behoerde behoerde;

    public BehoerdeBerechtigungsAdapter(Behoerde behoerde) {
        this.behoerde = behoerde;
    }

    @Override
    public Land getLand() {
        return behoerde.getLand();
    }

    @Override
    public Set<String> getBehoerden() {
        return null;
    }

    @Override
    public Set<String> getAkzSet() {
        return null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return null;
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return null;
    }
}
