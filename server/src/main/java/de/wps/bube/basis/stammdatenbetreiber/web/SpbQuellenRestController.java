package de.wps.bube.basis.stammdatenbetreiber.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.web.openapi.api.SpbQuellenApi;
import de.wps.bube.basis.base.web.openapi.model.ListAllSpbQuellenForAnlageDto;
import de.wps.bube.basis.base.web.openapi.model.SpbQuelleDto;
import de.wps.bube.basis.base.web.openapi.model.SpbQuelleListItemDto;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;

@RestController
public class SpbQuellenRestController implements SpbQuellenApi {

    private final StammdatenBetreiberService service;
    private final SpbQuelleMapper mapper;

    public SpbQuellenRestController(
            StammdatenBetreiberService service, SpbQuelleMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<SpbQuelleDto> createSpbQuelle(SpbQuelleDto spbQuelleDto) {
        return ResponseEntity.ok(mapper.toDto(service.createSpbQuelle(mapper.fromDto(spbQuelleDto))));
    }

    @Override
    public ResponseEntity<Void> deleteSpbQuelle(Long betriebsstaetteId, Long id) {
        this.service.deleteSpbQuelleFromRest(betriebsstaetteId, id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<SpbQuelleListItemDto>> listAllSpbQuellenForAnlage(ListAllSpbQuellenForAnlageDto dto) {

        return ResponseEntity.ok(service.listAllSpbQuellenAsListItemByAnlageId(dto.getAnlageId(), dto.getSpbAnlageId(),
                dto.getBetriebsstaetteId())
                                        .stream()
                                        .map(mapper::toListItemDto)
                                        .toList());
    }

    @Override
    public ResponseEntity<List<SpbQuelleListItemDto>> listAllNeueSpbQuellenForAnlage(Long anlageId) {

        return ResponseEntity.ok(
                service.listAllNeueSpbQuellenAsListItemByAnlagenId(anlageId)
                       .stream()
                       .map(mapper::toListItemDto)
                       .toList());
    }

    @Override
    public ResponseEntity<List<SpbQuelleListItemDto>> listAllSpbQuellenForBetriebsstaette(Long betriebsstaetteId) {

        return ResponseEntity.ok(service.listAllSpbQuellenAsListItemByBetriebsstaettenId(betriebsstaetteId)
                                        .stream()
                                        .map(mapper::toListItemDto)
                                        .toList());
    }

    @Override
    public ResponseEntity<List<SpbQuelleListItemDto>> listAllNeueSpbQuellenForBetriebsstaette(Long betriebsstaetteId) {

        return ResponseEntity.ok(service.listAllNeueSpbQuellenAsListItemByBetriebsstaettenId(betriebsstaetteId)
                                        .stream()
                                        .map(mapper::toListItemDto)
                                        .toList());
    }

    @Override
    public ResponseEntity<SpbQuelleDto> readSpbQuelleByBetriebsstaette(Long betriebsstaetteId, String quellenNummer) {
        return ResponseEntity.ok(
                mapper.toDto(service.loadSpbQuelleByBetriebsstaette(betriebsstaetteId, quellenNummer)));
    }

    @Override
    public ResponseEntity<SpbQuelleDto> readSpbQuelleByAnlage(Long betriebsstaetteId, Long anlageId,
            String quellenNummer) {
        return ResponseEntity.ok(
                mapper.toDto(service.loadSpbQuelleByAnlage(anlageId, quellenNummer)));
    }

    @Override
    public ResponseEntity<SpbQuelleDto> readSpbQuelleBySpbAnlage(Long betriebsstaetteId, Long spbAnlageId,
            String quellenNummer) {
        return ResponseEntity.ok(
                mapper.toDto(service.loadSpbQuelleBySpbAnlage(spbAnlageId, betriebsstaetteId, quellenNummer)));
    }

    @Override
    public ResponseEntity<SpbQuelleDto> updateSpbQuelle(SpbQuelleDto spbQuelleDto) {
        return ResponseEntity.ok(mapper.toDto(service.updateSpbQuelle(mapper.fromDto(spbQuelleDto))));
    }
}
