package de.wps.bube.basis.stammdaten.rules.bean;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class AnlagenteilBean extends StammdatenObjektBean<AnlagenteilBean> {

    private final String anlagenteilNr;
    private final Long parentAnlageId;
    private final List<VorschriftBean> vorschriften;
    private final List<LeistungBean> leistungen;
    private final Supplier<AnlageBean> anlage;

    public AnlagenteilBean(Long id, String localId, String referenzAnlagenkataster, String anlagenteilNr,
            Long parentAnlageId, GeoPunktBean geoPunkt, List<VorschriftBean> vorschriften,
            List<LeistungBean> leistungen, List<ZustaendigeBehoerdeBean> zustaendigeBehoerden, String name,
            ReferenzBean vertraulichkeitsgrund, ReferenzBean betriebsstatus, LocalDate betriebsstatusSeit,
            LocalDate inbetriebnahme, String bemerkung, boolean betreiberdatenUebernommen, Instant ersteErfassung,
            Instant letzteAenderung, Supplier<AnlagenteilBean> vorgaenger, Supplier<AnlagenteilBean> nachfolger,
            Supplier<AnlageBean> anlage) {
        super(id, localId, referenzAnlagenkataster, geoPunkt, zustaendigeBehoerden, name, vertraulichkeitsgrund,
                betriebsstatus, betriebsstatusSeit, inbetriebnahme, bemerkung, betreiberdatenUebernommen,
                ersteErfassung, letzteAenderung, vorgaenger, nachfolger);
        this.anlagenteilNr = anlagenteilNr;
        this.parentAnlageId = parentAnlageId;
        this.vorschriften = vorschriften;
        this.leistungen = leistungen;
        this.anlage = Suppliers.memoize(anlage::get);
    }

    @RegelApi.Member(description = "Anlagenteil-Nummer")
    public String getAnlagenteilNr() {
        return anlagenteilNr;
    }

    @RegelApi.Member(exclude = true)
    public Long getParentAnlageId() {
        return parentAnlageId;
    }

    @RegelApi.Member(description = "Vorschriften")
    public List<VorschriftBean> getVorschriften() {
        return vorschriften;
    }

    @RegelApi.Member(description = "Leistungen")
    public List<LeistungBean> getLeistungen() {
        return leistungen;
    }

    // Wird für Annotation überschrieben
    @Override
    public AnlagenteilBean getNachfolger() {
        return super.getNachfolger();
    }

    // Wird für Annotation überschrieben
    @Override
    public AnlagenteilBean getVorgaenger() {
        return super.getVorgaenger();
    }

    public AnlageBean getAnlage() {
        return anlage.get();
    }

    @Override
    @RegelApi.Member(description = "Die Betriebsstätte des Anlagenteils")
    public BetriebsstaetteBean getBetriebsstaette() {
        return getAnlage().getBetriebsstaette();
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.ANLAGENTEIL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnlagenteilBean that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        return Objects.equals(anlagenteilNr, that.anlagenteilNr) && Objects.equals(parentAnlageId,
                that.parentAnlageId) && Objects.equals(vorschriften,
                that.vorschriften) && Objects.equals(leistungen, that.leistungen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), anlagenteilNr, parentAnlageId, vorschriften, leistungen);
    }

    @Override
    public String toString() {
        return "AnlagenteilBean{" +
                "anlagenteilNr='" + anlagenteilNr + '\'' +
                ", parentAnlageId=" + parentAnlageId +
                ", vorschriften=" + vorschriften +
                ", leistungen=" + leistungen +
                " // " + super.toString() +
                '}';
    }

}
