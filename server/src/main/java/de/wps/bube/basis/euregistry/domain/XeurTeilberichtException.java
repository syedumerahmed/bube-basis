package de.wps.bube.basis.euregistry.domain;

public class XeurTeilberichtException extends EuRegistryException {
    public XeurTeilberichtException(String message) {
        super(message);
    }

    public XeurTeilberichtException(String message, Exception e) {
        super(message, e);
    }
}
