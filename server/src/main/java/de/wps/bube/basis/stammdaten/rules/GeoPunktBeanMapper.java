package de.wps.bube.basis.stammdaten.rules;

import java.util.function.Function;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationException;

import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenPruefungService;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdaten.rules.bean.GeoPunktBean;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
@Mapper(uses = ReferenzBeanMapper.class)
public abstract class GeoPunktBeanMapper {

    /*
     TODO: Im Rahmen von https://wps-bube.atlassian.net/browse/BUBE-699 wurde die Klasse GeoPunkt aus dem Modul
      Stammdaten nach Referenzdaten verschoben.
      Fachlich wäre es sinnvoll, auch die Klassen GeoPunktBean und GeoPunktBeanMapper zu verschieben, aber aufgrund
      der Abhängigkeit des Mappers zum KoordinatenPruefungsService und zum BeanMappingContext würde dies neue
      Architekturverletzungen verursachen.
     */

    @Autowired
    protected ReferenzenService referenzenService;
    @Autowired
    protected KoordinatenPruefungService koordinatenPruefungService;

    @Mapping(target = "isInAgs", expression = "java(isInAgs(geoPunkt, context))")
    public abstract GeoPunktBean toBean(GeoPunkt geoPunkt, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers);

    protected Function<String, Boolean> isInAgs(GeoPunkt geoPunkt, BeanMappingContext context) {
        return ags -> {
            Referenz berichtsjahr = referenzenService.getJahr(context.getJahr().toString());
            try {
                return koordinatenPruefungService.isGeoPunktInAgsGeometrie(ags, geoPunkt, context.getJahr(),
                        context.getLand(), berichtsjahr);
            } catch (KoordinatenException e) {
                throw new EvaluationException(
                        "Fehler bei Koordinatenprüfung für GeoPunkt mit EPSG-Code %s: %s".formatted(
                                geoPunkt.getEpsgCode().getKtext(), e.getMessage()), e);
            }
        };
    }

}
