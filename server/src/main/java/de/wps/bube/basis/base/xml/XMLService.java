package de.wps.bube.basis.base.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.xerces.impl.XMLErrorReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Validierungshinweis;

@Service
public class XMLService {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLService.class);

    public void validate(InputStream stream, String xsdPath, List<Validierungshinweis> hinweise) {

        try {
            Schema schema = this.getSchema(xsdPath);
            Validator validator = schema.newValidator();

            // Eigenen Message Formatter setzten
            XMLErrorReporter property = (XMLErrorReporter) validator.getProperty(
                    "http://apache.org/xml/properties/internal/error-reporter");
            property.putMessageFormatter(XMLSchemaMessageFormatter.DOMAIN, new XMLSchemaMessageFormatter());

            validator.setErrorHandler(new ErrorHandler() {
                @Override
                public void warning(SAXParseException pe) {
                    hinweise.add(Validierungshinweis.of(
                            "XML-Import Warnung Zeile " + pe.getLineNumber() + ": " + pe.getLocalizedMessage(),
                            Validierungshinweis.Severity.WARNUNG));
                }

                @Override
                public void error(SAXParseException pe) {
                    hinweise.add(Validierungshinweis.of(
                            "XML-Import Fehler Zeile " + pe.getLineNumber() + ": " + pe.getLocalizedMessage(),
                            Validierungshinweis.Severity.FEHLER));
                }

                @Override
                public void fatalError(SAXParseException pe) {
                    hinweise.add(Validierungshinweis.of(
                            "XML-Import fataler Fehler Zeile " + pe.getLineNumber() + ": " + pe.getLocalizedMessage(),
                            Validierungshinweis.Severity.FEHLER));
                }
            });
            validator.validate(new StreamSource(stream));
        } catch (IOException | SAXException e) {
            if (e.getClass() == SAXParseException.class) {
                SAXParseException pe = (SAXParseException) e;
                throw new XMLImportException(
                        "XML-Import Fehler Zeile %d: %s".formatted(pe.getLineNumber(), pe.getMessage()), e);
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new XMLImportException(e);
            }
        }
    }

    private Schema getSchema(String xsdPath) throws IOException, SAXException {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource xsdResource = resourceLoader.getResource(xsdPath);
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return sf.newSchema(new StreamSource(xsdResource.getInputStream()));
    }

    public <T, S> void read(InputStream stream, Class<T> clazz, XDtoMapper<T, S> mapper, String startElementName,
            String xsdPath, Consumer<EntityWithError<S>> consumer) {
        read(stream, clazz, mapper::fromXDto, startElementName, xsdPath, consumer);
    }

    public <T, S> void read(
            InputStream stream, Class<T> clazz, Function<T, EntityWithError<S>> mapper, String startElementName,
            String xsdPath, Consumer<EntityWithError<S>> consumer) {
        try {

            // StAX:
            XMLInputFactory staxFactory = XMLInputFactory.newInstance();
            XMLEventReader staxReader = staxFactory.createXMLEventReader(stream);
            XMLEventReader staxFiltRd = staxFactory.createFilteredReader(staxReader, XMLEvent::isStartElement);

            // Validiere StartElement:
            StartElement startElement = (StartElement) staxFiltRd.nextEvent();
            if (!startElementName.equals(startElement.getName().getLocalPart())) {
                String message = String.format("Fehler beim Import: Erwartet: <%s>; gefunden: <%s>.",
                        startElementName, startElement.getName());
                throw new XMLImportException(message);
            }
            // JAXB:
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            // soll man aus Performance Gründen nicht machen besser über validate() Prüfen
            if (xsdPath != null) {
                unmarshaller.setSchema(this.getSchema(xsdPath));
            }
            // Parsing:
            while (staxFiltRd.peek() != null) {
                JAXBElement<T> jaxbElement = unmarshaller.unmarshal(staxReader, clazz);
                consumer.accept(mapper.apply(jaxbElement.getValue()));
            }

        } catch (JAXBException | XMLStreamException | IOException | SAXException e) {
            LOGGER.error(e.getMessage(), e);
            throw new XMLImportException(e);
        }
    }

    public <E, X> StreamingXMLEntityWriter<E, X> createWriter(Class<X> elementClass,
            QName containerElement, Function<E, X> entityMapper, Function<X, JAXBElement<X>> xdtoWrapper) {
        return new StreamingXMLEntityWriter<>(elementClass, containerElement, entityMapper, xdtoWrapper);
    }
}
