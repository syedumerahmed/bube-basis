package de.wps.bube.basis.base.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.Validierungshinweis;

@Mapper
public interface ValidierungshinweisMapper {
    Validierungshinweis mapToDto(de.wps.bube.basis.base.vo.Validierungshinweis validierungshinweis);
}
