package de.wps.bube.basis.euregistry.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.querydsl.core.annotations.QueryInit;

import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class EURegBetriebsstaette {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eureg_betriebsstaette_seq")
    @SequenceGenerator(name = "eureg_betriebsstaette_seq", allocationSize = 50)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    // @QueryInit sorgt dafür, dass QueryDSL bei der Pfaderzeugung hier tiefer als standardmäßig (2 Ebenen)
    // den EntityBaum hinabsteigt. S. http://www.querydsl.com/static/querydsl/2.2.0/reference/html/ch03s02.html
    @QueryInit("*.*")
    private Betriebsstaette betriebsstaette;

    @Embedded
    @QueryInit("*.*")
    private Berichtsdaten berichtsdaten;

    @OneToOne
    private Komplexpruefung komplexpruefung;

    private boolean pruefungsfehler;
    private boolean pruefungVorhanden;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    @Deprecated
    protected EURegBetriebsstaette() {
    }

    public EURegBetriebsstaette(Betriebsstaette betriebsstaette, Berichtsdaten berichtsdaten) {
        this.betriebsstaette = betriebsstaette;
        this.berichtsdaten = berichtsdaten;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Betriebsstaette getBetriebsstaette() {
        return betriebsstaette;
    }

    public void setBetriebsstaette(Betriebsstaette betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
    }

    public Berichtsdaten getBerichtsdaten() {
        return berichtsdaten;
    }

    public void setBerichtsdaten(Berichtsdaten berichtsdaten) {
        this.berichtsdaten = berichtsdaten;
    }

    public boolean isPruefungsfehler() {
        return pruefungsfehler;
    }

    public void setPruefungsfehler(boolean pruefungsfehler) {
        this.pruefungsfehler = pruefungsfehler;
    }

    public boolean isPruefungVorhanden() {
        return pruefungVorhanden;
    }

    public void setPruefungVorhanden(boolean pruefungVorhanden) {
        this.pruefungVorhanden = pruefungVorhanden;
    }

    public Komplexpruefung getKomplexpruefung() {
        return komplexpruefung;
    }

    public void setKomplexpruefung(Komplexpruefung komplexpruefung) {
        this.komplexpruefung = komplexpruefung;
    }

    public String getLocalId() {
        return betriebsstaette.getLocalId();
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EURegBetriebsstaette that = (EURegBetriebsstaette) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
