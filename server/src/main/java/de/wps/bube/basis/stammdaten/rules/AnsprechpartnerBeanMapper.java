package de.wps.bube.basis.stammdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.rules.KommunikationsverbindungBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.rules.bean.AnsprechpartnerBean;

@Mapper(uses = { ReferenzBeanMapper.class, KommunikationsverbindungBeanMapper.class })
public abstract class AnsprechpartnerBeanMapper {

    public abstract AnsprechpartnerBean toBean(Ansprechpartner ansprechpartner);

}
