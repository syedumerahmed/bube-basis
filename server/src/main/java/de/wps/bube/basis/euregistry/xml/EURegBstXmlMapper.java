package de.wps.bube.basis.euregistry.xml;

import java.util.List;
import java.util.stream.Stream;

import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.xml.dto.*;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.xml.CommonXMLMapper;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.referenzdaten.xml.ReferenzXMLMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Mapper(uses = { CommonXMLMapper.class, ReferenzXMLMapper.class })
public abstract class EURegBstXmlMapper implements XDtoMapper<EuRegBst, EURegBetriebsstaette> {

    @Autowired
    EURegistryService euRegistryService;

    @Override
    public EntityWithError<EURegBetriebsstaette> fromXDto(EuRegBst xdto){
        throw new UnsupportedOperationException();
    }

    @Override
    @Mapping(target = ".", source = "berichtsdaten")
    @Mapping(target = "zustaendigeBehoerde", source = "berichtsdaten.zustaendigeBehoerde.schluessel")
    @Mapping(target = "inspire", source = "berichtsdaten")
    @Mapping(target = "inspire.localId", source = "betriebsstaette.localId")
    @Mapping(target = "jahr", source = "betriebsstaette.berichtsjahr")
    @Mapping(target = "land", source = "betriebsstaette.land")
    @Mapping(target = "betriebsstaetteNr", source = "betriebsstaette.betriebsstaetteNr")
    @Mapping(target = "euRegAnl", source = "betriebsstaette", qualifiedByName = "euRegAnl")
    @Mapping(target = "euRegF", source = "betriebsstaette", qualifiedByName = "euRegF")
    public abstract EuRegBst toXDto(EURegBetriebsstaette entity);

    @Named("euRegAnl")
    List<EURegAnlage> getEuRegAnl(Betriebsstaette betriebsstaette) {
        return getEuRegBerichte(betriebsstaette)
                .filter(EURegAnlage::isEURegAnl)
                .toList();
    }

    @Named("euRegAnl")
    @IterableMapping(qualifiedByName = "euRegAnl")
    abstract List<EuRegAnl> mapEuRegAnlList(List<EURegAnlage> euRegAnlage);

    @Named("euRegAnl")
    @Mapping(target = ".", source = "berichtsdaten")
    @Mapping(target = "zustaendigeBehoerde", source = "berichtsdaten.zustaendigeBehoerde.schluessel")
    @Mapping(target = "inspire", source = "berichtsdaten")
    @Mapping(target = "inspire.localId", source = "localId")
    @Mapping(target = "anlagenNr", source = "anlage.anlageNr")
    @Mapping(target = "anlagenteilNr", source = "anlagenteil.anlagenteilNr")
    @Mapping(target = "ESpirs", source = "eSpirsNr")
    @Mapping(target = "tehg", source = "tehgNr")
    @Mapping(target = "oeffentlichesMonitoring", source = "monitor")
    @Mapping(target = "monitoringUrl", source = "monitorUrl")
    @Mapping(target = "strengereGenehmigungsauflage", source = "auflagen")
    @Mapping(target = "bvtAusnahme", source = "bvtAusnahmen")
    @Mapping(target = "ausgangszustandsbericht", source = "report")
    abstract EuRegAnl mapEuRegAnl(EURegAnlage euRegAnlage);

    @Named("euRegF")
    List<EURegAnlage> getEuRegF(Betriebsstaette betriebsstaette) {
        return getEuRegBerichte(betriebsstaette)
                .filter(EURegAnlage::isEURegF)
                .toList();
    }

    @Named("euRegF")
    @IterableMapping(qualifiedByName = "euRegF")
    abstract List<EuRegF> mapEuRegFList(List<EURegAnlage> euRegAnlage);

    @Named("euRegF")
    @Mapping(target = ".", source = "berichtsdaten")
    @Mapping(target = "zustaendigeBehoerde", source = "berichtsdaten.zustaendigeBehoerde.schluessel")
    @Mapping(target = "inspire", source = "berichtsdaten")
    @Mapping(target = "inspire.localId", source = "localId")
    @Mapping(target = "anlagenNr", source = "anlage.anlageNr")
    @Mapping(target = "anlagenteilNr", source = "anlagenteil.anlagenteilNr")
    @Mapping(target = "ESpirs", source = "eSpirsNr")
    @Mapping(target = "tehg", source = "tehgNr")
    @Mapping(target = "oeffentlichesMonitoring", source = "monitor")
    @Mapping(target = "monitoringUrl", source = "monitorUrl")
    @Mapping(target = "strengereGenehmigungsauflage", source = "auflagen")
    @Mapping(target = "bvtAusnahme", source = "bvtAusnahmen")
    @Mapping(target = "ausgangszustandsbericht", source = "report")
    @Mapping(target = "feuerungsanlage", source = ".")
    abstract EuRegF mapEuRegF(EURegAnlage euRegAnlage);

    Feuerungsanlage mapFeuerungsanlage( EURegAnlage euRegAnlage){
        if (euRegAnlage == null){
            return null;
        }
        FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten = euRegAnlage.getFeuerungsanlage();

        if(feuerungsanlageBerichtsdaten == null){
            return null;
        }

        Feuerungsanlage feuerungsanlage = new Feuerungsanlage();
        if(feuerungsanlageBerichtsdaten.getIeAusnahmen() != null) {
            feuerungsanlage.getAusnahmeNachIeRl().addAll(mapIeAusnahmen(feuerungsanlageBerichtsdaten.getIeAusnahmen()));
        }
        feuerungsanlage.setFeuerungsanlageTyp(mapFeuerungsanlagenTyp(feuerungsanlageBerichtsdaten.getFeuerungsanlageTyp()));


        if(euRegAnlage.getAnlage() != null) {
            if (euRegAnlage.getAnlage().isFeuerungsanlageMit17BV()) {
                feuerungsanlage.setBImSchV17(map17BImSchV(euRegAnlage));
            }
        } else if (euRegAnlage.getAnlagenteil().isFeuerungsanlageMit17BV()) {
                feuerungsanlage.setBImSchV17(map17BImSchV(euRegAnlage));
            }

        return feuerungsanlage;
    }

    @Mapping(target = ".", source = "feuerungsanlage")
    @Mapping(target = "spezielleBedingung", source = "feuerungsanlage.spezielleBedingungen")
    @Mapping(target = "fwl", source = "gesamtwaermeleistung")

    abstract BImSchV17 map17BImSchV(EURegAnlage euRegAnlage);

    abstract List<String> mapIeAusnahmen(List<Referenz> ieAusnahmen);

    abstract FeuerungsanlageTyp mapFeuerungsanlagenTyp(de.wps.bube.basis.euregistry.domain.vo.FeuerungsanlageTyp feuerungsanlageTyp);

    @Mapping(target = "aenderungArt", source = "art")
    abstract SpezielleBedingung mapSpezielleBedingung(SpezielleBedingungIE51 spezielleBedingung);

    private Stream<EURegAnlage> getEuRegBerichte(Betriebsstaette betriebsstaette) {
        return betriebsstaette.getAnlagen()
                              .stream()
                              .flatMap(anlage -> Stream.concat(
                                      euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId()).stream(),
                                      anlage.getAnlagenteile().stream()
                                            .flatMap(anlagenteil -> euRegistryService.findAnlagenBerichtByAnlagenteilId(
                                                    anlagenteil.getId()).stream())));
    }
}
