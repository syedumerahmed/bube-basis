package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

public class BooleanVector {
    private Boolean spl;
    private Boolean spb;
    private boolean equal;

    public BooleanVector(Boolean spl, Boolean spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public Boolean getSpl() {
        return spl;
    }

    public void setSpl(Boolean spl) {
        this.spl = spl;
    }

    public Boolean getSpb() {
        return spb;
    }

    public void setSpb(Boolean spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
