package de.wps.bube.basis.euregistry.persistence.predicates;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.euregistry.domain.vo.EuRegAnlageColumn;

public class EuRegAnlagePredicates {

    public static Predicate mapFiltersToPredicate(
            List<ListStateDto.FilterStateDto<EuRegAnlageColumn>> filters) {
        if (filters == null) {
            return null;
        }
        return createPredicateByColumns(
                filters.stream().collect(Collectors.toMap(f -> f.property, f -> f.value)));
    }

    public static Predicate createPredicateByColumns(Map<EuRegAnlageColumn, String> columns) {
        if (columns == null || columns.isEmpty()) {
            return null;
        }
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        columns.forEach((property, value) -> booleanBuilder.and(property.getExpression().containsIgnoreCase(value)));
        return booleanBuilder;
    }
}
