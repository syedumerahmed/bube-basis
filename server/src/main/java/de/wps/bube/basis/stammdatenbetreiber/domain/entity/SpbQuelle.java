package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import java.time.Instant;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

@Entity
@EntityListeners({AuditingEntityListener.class})
public class SpbQuelle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "spb_quelle_seq")
    @SequenceGenerator(name = "spb_quelle_seq", allocationSize = 50)
    private Long id;

    // Eine SpbQuelle verändert diese Quelle
    private Long quelleId;

    @NotNull
    private String quelleNr;

    @NotNull
    private String name;

    // Eine Quelle kann eine Eltern Betriebsstätte haben
    private Long parentBetriebsstaetteId;

    // Eine Quelle kann eine Eltern Anlage habnen
    private Long parentAnlageId;

    // Eine Quelle kann eine Eltern Anlage habnen
    private Long parentSpbAnlageId;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    @ManyToOne
    private Referenz quellenArt;

    private Double flaeche;
    private Double laenge;
    private Double breite;
    private Double durchmesser;
    private Double bauhoehe;

    @NotNull
    private Instant ersteErfassung = Instant.now();

    @NotNull
    @LastModifiedDate
    private Instant letzteAenderung;

    public SpbQuelle() {
    }

    public SpbQuelle(Quelle quelle) {
        this.quelleId = quelle.getId();
        this.parentAnlageId = quelle.getParentAnlageId();
        this.parentBetriebsstaetteId = quelle.getParentBetriebsstaetteId();
        this.quelleNr = quelle.getQuelleNr();
        var geo = quelle.getGeoPunkt();
        if (geo != null) {
            this.geoPunkt = new GeoPunkt(geo.getNord(), geo.getOst(), geo.getEpsgCode());
        }
        this.name = quelle.getName();
        this.quellenArt = quelle.getQuellenArt();
        this.ersteErfassung = quelle.getErsteErfassung();
        this.letzteAenderung = quelle.getLetzteAenderung();

        this.flaeche = quelle.getFlaeche();
        this.laenge = quelle.getLaenge();
        this.breite = quelle.getBreite();
        this.durchmesser = quelle.getDurchmesser();
        this.bauhoehe = quelle.getBauhoehe();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuelleId() {
        return quelleId;
    }

    public void setQuelleId(Long quelleId) {
        this.quelleId = quelleId;
    }

    public String getQuelleNr() {
        return quelleNr;
    }

    public void setQuelleNr(String quelleNr) {
        this.quelleNr = quelleNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    public void setParentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
    }

    public Long getParentAnlageId() {
        return parentAnlageId;
    }

    public void setParentAnlageId(Long parentAnlageId) {
        this.parentAnlageId = parentAnlageId;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public Referenz getQuellenArt() {
        return quellenArt;
    }

    public void setQuellenArt(Referenz quellenArt) {
        this.quellenArt = quellenArt;
    }

    public Double getFlaeche() {
        return flaeche;
    }

    public void setFlaeche(Double flaeche) {
        this.flaeche = flaeche;
    }

    public Double getLaenge() {
        return laenge;
    }

    public void setLaenge(Double laenge) {
        this.laenge = laenge;
    }

    public Double getBreite() {
        return breite;
    }

    public void setBreite(Double breite) {
        this.breite = breite;
    }

    public Double getDurchmesser() {
        return durchmesser;
    }

    public void setDurchmesser(Double durchmesser) {
        this.durchmesser = durchmesser;
    }

    public Double getBauhoehe() {
        return bauhoehe;
    }

    public void setBauhoehe(Double bauhoehe) {
        this.bauhoehe = bauhoehe;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SpbQuelle spbQuelle = (SpbQuelle) o;
        return id.equals(spbQuelle.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getParentSpbAnlageId() {
        return parentSpbAnlageId;
    }

    public void setParentSpbAnlageId(Long parentSpbAnlageId) {
        this.parentSpbAnlageId = parentSpbAnlageId;
    }

    public boolean istKindVonAnlage() {
        return parentAnlageId != null || parentSpbAnlageId != null;
    }

    public boolean istKindVonBetriebsstaette() {
        return parentAnlageId == null && parentSpbAnlageId == null;
    }

    public boolean hasQuelle() {
        return quelleId != null;
    }
}
