package de.wps.bube.basis.euregistry.domain.event;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public record XeurTeilberichtFreigegebenEvent(Referenz berichtsjahr) {

    public static final String EVENT_NAME = "XEUR Teilbericht freigegeben";

}
