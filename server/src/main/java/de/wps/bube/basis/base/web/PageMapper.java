package de.wps.bube.basis.base.web;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.PageDtoBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.PageDtoEuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.PageDtoSpbBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteListItemDto;

@Mapper
public abstract class PageMapper {

    public PageDtoBetriebsstaetteListItemDto mapBetriebsstaetteListItemToDto(Page<BetriebsstaetteListItemDto> page) {
        PageDtoBetriebsstaetteListItemDto dto = new PageDtoBetriebsstaetteListItemDto();
        dto.setTotalElements(page.getTotalElements());
        dto.setContent(page.getContent());
        return dto;
    }

    public PageDtoSpbBetriebsstaetteListItemDto mapSpbBetriebsstaetteListItemDto(
            Page<SpbBetriebsstaetteListItemDto> page) {
        PageDtoSpbBetriebsstaetteListItemDto dto = new PageDtoSpbBetriebsstaetteListItemDto();
        dto.setTotalElements(page.getTotalElements());
        dto.setContent(page.getContent());
        return dto;
    }

    public PageDtoEuRegBetriebsstaetteListItemDto mapEuRegBetriebsstaetteListItemToDto(
            Page<EuRegBetriebsstaetteListItemDto> page) {
        PageDtoEuRegBetriebsstaetteListItemDto dto = new PageDtoEuRegBetriebsstaetteListItemDto();
        dto.setTotalElements(page.getTotalElements());
        dto.setContent(page.getContent());
        return dto;
    }

    public abstract ListStateDto.PageStateDto mapQuellenFromDto(
            de.wps.bube.basis.base.web.openapi.model.PageStateDto page);
}
