package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.ErrorRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.Sets;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.benutzerverwaltung.domain.KeycloakException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

/* Service für die Keycloak Admin REST API
    https://www.keycloak.org/docs-api/11.0/rest-api/index.html
 */
@Service
public class KeycloakService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakService.class);

    private static final int USER_LIMIT = Integer.MAX_VALUE;
    public static final String CREDENTIAL_TYPE_OTP = "otp";
    public static final String CREDENTIAL_TYPE_PW = "password";
    public static final String ACTION_CONFIGURE_TOTP = "CONFIGURE_TOTP";
    public static final String ACTION_UPDATE_PASSWORD = "UPDATE_PASSWORD";

    private final KeycloakServiceMapper mapper;
    private final UsersResource users;
    private final RolesResource roles;
    private final Keycloak keycloakClient;

    public KeycloakService(KeycloakServiceMapper mapper, Keycloak keycloakClient,
            @Value("${keycloak.realm}") String realm) {
        this.mapper = mapper;
        this.keycloakClient = keycloakClient;
        var realmResource = keycloakClient.realm(realm);
        users = realmResource.users();
        roles = realmResource.roles();
    }

    public Stream<Benutzer> listAllUsers() {
        return this.users.list(0, USER_LIMIT)
                         .stream()
                         .map(user -> mapper.mapUserToBenutzer(this.users.get(user.getId())));
    }

    public Stream<Benutzer> listAllUsers(String rollenname) {
        return roles.get(rollenname)
                    .getRoleUserMembers(0, USER_LIMIT)
                    .stream()
                    .map(user -> mapper.mapUserToBenutzer(this.users.get(user.getId())));
    }

    public Benutzer getUserById(String id) {
        Assert.notNull(id, "id == null");
        try {
            return mapper.mapUserToBenutzer(users.get(id));
        } catch (NotFoundException e) {
            throw new BubeEntityNotFoundException(Benutzer.class, "ID " + id);
        }
    }

    public Optional<Benutzer> findUserByUsername(String benutzername) {
        Assert.notNull(benutzername, "benutzername == null");
        List<UserRepresentation> results = users.search(benutzername, true);
        Assert.isTrue(results.size() <= 1, "benutzer results.size > 1");
        return results.stream()
                      .findFirst()
                      .map(UserRepresentation::getId)
                      .map(users::get)
                      .map(mapper::mapUserToBenutzer);
    }

    public Benutzer createUser(Benutzer b) {
        Assert.notNull(b, "benutzer == null");
        Assert.isNull(b.getId(), "id != null");

        LOGGER.info("Creating user {}", b.getBenutzername());

        UserResource user = createKeycloakUser(mapper.mapBenutzerToUser(b));
        LOGGER.info("Created user {} successfully.", b.getBenutzername());

        mapRollenAndDatenberechtigung(b, user);

        var benutzer = mapper.mapUserToBenutzer(user);

        if (b.getTempPassword() != null) {
            LOGGER.info("Assigning temporary password for user {}", b.getBenutzername());
            user.resetPassword(buildTemporaryPwCredential(b.getTempPassword()));
            benutzer.setTempPassword(b.getTempPassword());
        }
        return benutzer;
    }

    public Benutzer updateUser(Benutzer b) {
        Assert.notNull(b, "benutzer == null");
        Assert.notNull(b.getId(), "id == null");
        try {
            var user = users.get(b.getId());
            user.update(mapper.mapBenutzerToUser(b));
            var currentRoles = user.roles()
                                   .realmLevel()
                                   .listAll()
                                   .stream()
                                   .map(RoleRepresentation::getName)
                                   .filter(Rolle::isValid)
                                   .map(Rolle::of)
                                   .collect(toSet());

            var newRoles = Set.copyOf(b.getZugewieseneRollen());
            var toRemove = Sets.difference(currentRoles, newRoles);
            if (!toRemove.isEmpty()) {
                user.roles().realmLevel().remove(toRemove.stream().map(this::mapRolle).collect(toList()));
            }

            var toAdd = Sets.difference(newRoles, currentRoles);
            if (!toAdd.isEmpty()) {
                user.roles().realmLevel().add(toAdd.stream().map(this::mapRolle).collect(toList()));
            }

            return this.getUserById(b.getId());
        } catch (WebApplicationException ex) {
            throw new KeycloakException("Fehler beim Speichern", ex.getResponse().getStatus(),
                    ex.getResponse().readEntity(ErrorRepresentation.class));
        }
    }

    public void deleteUser(String id) {
        Assert.notNull(id, "id == null");
        LOGGER.info("Deleting user with ID {}", id);
        users.delete(id);
    }

    public void resetPassword(Benutzer b) {
        Assert.notNull(b.getId(), "id == null");
        LOGGER.info("Resetting password for user {}", b.getBenutzername());

        try {
            var user = users.get(b.getId());

            // Verlange Benutzeraktion "Update Passwort" setzen
            UserRepresentation userRepresentation = user.toRepresentation();
            userRepresentation.getRequiredActions().add(ACTION_UPDATE_PASSWORD);
            user.update(userRepresentation);

            user.executeActionsEmail(List.of(ACTION_UPDATE_PASSWORD));
        } catch (WebApplicationException ex) {
            throw new KeycloakException(
                    "Passwort für Benutzer " + b.getBenutzername() + " konnte nicht zurückgesetzt werden",
                    ex.getResponse().getStatus(), ex.getResponse().readEntity(ErrorRepresentation.class));
        }
    }

    public void resetTwoFactor(Benutzer b) {
        Assert.notNull(b.getId(), "id == null");
        LOGGER.info("Resetting 2FA for user {}", b.getBenutzername());

        try {
            var user = users.get(b.getId());

            // Vorhandene OTP-Credentials löschen.
            user.credentials()
                .stream()
                .filter(credentialRepresentation -> CREDENTIAL_TYPE_OTP.equals(credentialRepresentation.getType()))
                .map(CredentialRepresentation::getId)
                .forEach(user::removeCredential);

            // Verlange Benutzeraktion "Configure OTP" setzen
            UserRepresentation userRepresentation = user.toRepresentation();
            userRepresentation.getRequiredActions().add(ACTION_CONFIGURE_TOTP);
            user.update(userRepresentation);

            // E-Mail an Benutzer senden
            user.executeActionsEmail(List.of(ACTION_CONFIGURE_TOTP));
        } catch (WebApplicationException ex) {
            throw new KeycloakException(
                    "Zwei-Faktor-Authentifizierung für Benutzer " + b.getBenutzername() + " konnte nicht zurückgesetzt werden",
                    ex.getResponse().getStatus(), ex.getResponse().readEntity(ErrorRepresentation.class));
        }
    }

    public void updateTemporaryPassword(Benutzer b) {
        Assert.notNull(b, "benutzer == null");
        Assert.notNull(b.getId(), "id == null");

        var temporaryPwCredential = buildTemporaryPwCredential(b.getTempPassword());
        LOGGER.info("Creating temporary password for user {}", b.getBenutzername());
        var user = users.get(b.getId());
        user.resetPassword(temporaryPwCredential);
    }

    private static CredentialRepresentation buildTemporaryPwCredential(String tmpPassword) {
        Assert.notNull(tmpPassword, "tmpPassword == null");
        CredentialRepresentation cr = new CredentialRepresentation();
        cr.setTemporary(true);
        cr.setType(CREDENTIAL_TYPE_PW);
        cr.setValue(tmpPassword);
        return cr;
    }

    private UserResource createKeycloakUser(UserRepresentation user) {
        var response = users.create(user);
        if (response.getStatusInfo().getFamily() != SUCCESSFUL) {
            throw new KeycloakException("Benutzer konnte nicht erstellt werden", response.getStatus(),
                    response.readEntity(ErrorRepresentation.class));
        }

        return keycloakClient.proxy(UserResource.class, response.getLocation());
    }

    private void mapRollenAndDatenberechtigung(Benutzer b, UserResource user) {
        List<RoleRepresentation> rollen;
        if (b.isBetreiberBenutzer()) {
            rollen = List.of(roles.get("Betreiber").toRepresentation());
        } else {
            rollen = b.getZugewieseneRollen().stream().map(this::mapRolle).collect(toList());
            rollen.add(roles.get("Behoerdenbenutzer").toRepresentation());
        }
        user.roles().realmLevel().add(rollen);
    }

    private RoleRepresentation mapRolle(Rolle rolle) {
        return roles.get(rolle.toKeycloakRepresentation()).toRepresentation();
    }
}
