package de.wps.bube.basis.base.xml;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.xerces.util.MessageFormatter;

// Code is from Apache Xerces org.apache.xerces.impl.msg.MessageFormatter and customized for BUBE ResourceBundle XMLSchemaMessages
// DOMAIN customized
// ResourceBundle.getBundle line 40/45 code customized
// deleted msg==null part after line 71.
public class XMLSchemaMessageFormatter implements MessageFormatter {

    public static final String DOMAIN = "http://www.w3.org/TR/xml-schema-1";

    // private objects to cache the locale and resource bundle
    private Locale fLocale = null;
    private ResourceBundle fResourceBundle = null;

    /**
     * Formats a message with the specified arguments using the given
     * locale information.
     *
     * @param locale    The locale of the message.
     * @param key       The message key.
     * @param arguments The message replacement text arguments. The order
     *                  of the arguments must match that of the placeholders
     *                  in the actual message.
     *
     * @return Returns the formatted message.
     * @throws MissingResourceException Thrown if the message with the
     *                                  specified key cannot be found.
     */
    @Override
    public String formatMessage(Locale locale, String key, Object[] arguments) throws MissingResourceException {

        if (fResourceBundle == null || locale != fLocale) {
            if (locale != null) {
                fResourceBundle = ResourceBundle.getBundle("xml.XMLSchemaMessages", locale);
                // memorize the most-recent locale
                fLocale = locale;
            }
            if (fResourceBundle == null) {
                fResourceBundle = ResourceBundle.getBundle("xml.XMLSchemaMessages");
            }
        }

        // format message
        String msg;
        try {
            msg = fResourceBundle.getString(key);
            if (arguments != null) {
                try {
                    msg = java.text.MessageFormat.format(msg, arguments);
                } catch (Exception e) {
                    msg = fResourceBundle.getString("FormatFailed");
                    msg += " " + fResourceBundle.getString(key);
                }
            }
        }

        // error
        catch (MissingResourceException e) {
            msg = fResourceBundle.getString("BadMessageKey");
            throw new MissingResourceException(key, msg, key);
        }

        return msg;
    }

}
