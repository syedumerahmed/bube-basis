package de.wps.bube.basis.base.web;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.EnvironmentApi;
import de.wps.bube.basis.base.web.openapi.model.Version;
import de.wps.bube.basis.base.web.openapi.model.WebappPropertiesDto;

@Controller
@EnableConfigurationProperties(WebappProperties.class)
public class EnvironmentRestController implements EnvironmentApi {

    private final WebappProperties webappProperties;
    private final WebappPropertiesMapper propertiesMapper;
    private final BuildProperties properties;

    public EnvironmentRestController(WebappProperties webappProperties, WebappPropertiesMapper propertiesMapper,
            BuildProperties properties) {
        this.webappProperties = webappProperties;
        this.propertiesMapper = propertiesMapper;
        this.properties = properties;
    }

    @Override
    public ResponseEntity<WebappPropertiesDto> getEnvironment() {
        return ResponseEntity.ok(propertiesMapper.mapToDto(webappProperties));
    }

    @Override
    public ResponseEntity<Version> getVersion() {
        Version version = new Version();
        version.setVersion(properties.getVersion());
        version.setBuildDate(OffsetDateTime.ofInstant(properties.getTime(), ZoneId.of("Europe/Berlin")));
        return ResponseEntity.ok(version);
    }

}
