package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

import de.wps.bube.basis.euregistry.xml.bericht.dto.AttributeNames;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.Function;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.ThematicId;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.Status;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProductionFacility {

    @XmlAttribute(required = true, name = AttributeNames.GML_ID, namespace = SchemaSet.SCHEMA_GML)
    private String gmlId;

    @XmlElement(required = true, name = "inspireId", namespace = SchemaSet.SCHEMA_ACT_CORE)
    private InspireID inspireID;

    @XmlElement(name = "thematicId", namespace = SchemaSet.SCHEMA_ACT_CORE)
    private ThematicId thematicId;

    @XmlElement(name = "geometry", namespace = SchemaSet.SCHEMA_ACT_CORE, nillable = true)
    private final PointLocation geometry = new PointLocation();

    @XmlElement(required = true, name = "function", namespace = SchemaSet.SCHEMA_ACT_CORE)
    // laut EU-Schema eine Liste, aber laut UBA-Vorgabe ein Wert
    private Function function;

    @XmlElement(required = true, name = "validFrom", namespace = SchemaSet.SCHEMA_ACT_CORE, nillable = true)
    private XMLGregorianCalendar validFrom;

    @XmlElement(required = true,
            name = "beginLifespanVersion",
            namespace = SchemaSet.SCHEMA_ACT_CORE,
            nillable = true)
    private XMLGregorianCalendar beginLifespanVersion;

    @XmlElement(name = "riverBasinDistrict", namespace = SchemaSet.SCHEMA_PF)
    private String riverBasinDistrict;

    @XmlElement(required = true, name = "status", namespace = SchemaSet.SCHEMA_PF)
    private Status status;

    @XmlElement(required = true, name = "hostingSite", namespace = SchemaSet.SCHEMA_PF)
    private ReferenceType hostingSite;

    @XmlElement(name = "groupedInstallation", namespace = SchemaSet.SCHEMA_PF, nillable = true)
    private List<GroupedInstallation> groupedInstallation;

    @XmlElement(required = true, name = "facilityName")
    private FeatureNameContainer facilityName;

    @XmlElement(name = "competentAuthorityEPRTR")
    private CompetentAuthorityEPRTR competentAuthorityEPRTR;

    @XmlElement(name = "parentCompany")
    private ParentCompany parentCompany;

    @XmlElement(name = "EPRTRAnnexIActivity")
    private EPRTRAnnexIActivity eprtrAnnexIActivity;

    @XmlElement(name = "remarks")
    private String remarks;

    @XmlElement(name = "facilityType")
    private ReferenceType facilityType;

    @XmlElement(required = true, name = "dateOfStartOfOperation", nillable = true)
    private XMLGregorianCalendar dateOfStartOfOperation;

    @XmlElement(required = true, name = "address", nillable = true)
    private Address address;

    public InspireID getInspireID() {
        return inspireID;
    }

    public void setInspireID(InspireID inspireID) {
        this.inspireID = inspireID;
    }

    public ThematicId getThematicId() {
        return thematicId;
    }

    public void setThematicId(ThematicId thematicId) {
        this.thematicId = thematicId;
    }

    public Point getGeometry() {
        return geometry.getPoint();
    }

    public void setGeometry(Point point) {
        this.geometry.setPoint(point);
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public XMLGregorianCalendar getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(XMLGregorianCalendar validFrom) {
        this.validFrom = validFrom;
    }

    public XMLGregorianCalendar getBeginLifespanVersion() {
        return beginLifespanVersion;
    }

    public void setBeginLifespanVersion(XMLGregorianCalendar beginLifespanVersion) {
        this.beginLifespanVersion = beginLifespanVersion;
    }

    public String getRiverBasinDistrict() {
        return riverBasinDistrict;
    }

    public void setRiverBasinDistrict(String riverBasinDistrict) {
        this.riverBasinDistrict = riverBasinDistrict;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ReferenceType getHostingSite() {
        return hostingSite;
    }

    public void setHostingSite(ReferenceType hostingSite) {
        this.hostingSite = hostingSite;
    }

    public List<GroupedInstallation> getGroupedInstallation() {
        return groupedInstallation;
    }

    public void setGroupedInstallation(List<GroupedInstallation> groupedInstallation) {
        this.groupedInstallation = groupedInstallation;
    }

    public FeatureNameContainer getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(FeatureNameContainer facilityName) {
        this.facilityName = facilityName;
    }

    public CompetentAuthorityEPRTR getCompetentAuthorityEPRTR() {
        return competentAuthorityEPRTR;
    }

    public void setCompetentAuthorityEPRTR(CompetentAuthorityEPRTR competentAuthorityEPRTR) {
        this.competentAuthorityEPRTR = competentAuthorityEPRTR;
    }

    public ParentCompany getParentCompany() {
        return parentCompany;
    }

    public void setParentCompany(ParentCompany parentCompany) {
        this.parentCompany = parentCompany;
    }

    public EPRTRAnnexIActivity getEprtrAnnexIActivity() {
        return eprtrAnnexIActivity;
    }

    public void setEprtrAnnexIActivity(EPRTRAnnexIActivity eprtrAnnexIActivity) {
        this.eprtrAnnexIActivity = eprtrAnnexIActivity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ReferenceType getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(ReferenceType facilityType) {
        this.facilityType = facilityType;
    }

    public XMLGregorianCalendar getDateOfStartOfOperation() {
        return dateOfStartOfOperation;
    }

    public void setDateOfStartOfOperation(XMLGregorianCalendar dateOfStartOfOperation) {
        this.dateOfStartOfOperation = dateOfStartOfOperation;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getGmlId() {
        return gmlId;
    }

    public void setGmlId(String gmlId) {
        this.gmlId = gmlId;
    }

    public List<ProductionInstallation> getProductionInstallations() {
        List<ProductionInstallation> productionInstallations = new ArrayList<>();
        if (groupedInstallation != null) {
            for (GroupedInstallation gInstallation : groupedInstallation) {
                productionInstallations.add(gInstallation.getProductionInstallation());
            }
        }
        return productionInstallations;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class GroupedInstallation {
        @XmlElement(name = "ProductionInstallation",
                namespace = SchemaSet.SCHEMA_EUREG,
                type = ProductionInstallation.class)
        private ProductionInstallation productionInstallation;

        @XmlAttribute(name = AttributeNames.HREF, namespace = SchemaSet.SCHEMA_XLINK)
        private String href;

        public ProductionInstallation getProductionInstallation() {
            return productionInstallation;
        }

        public void setProductionInstallation(ProductionInstallation productionInstallation) {
            this.productionInstallation = productionInstallation;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }
}
