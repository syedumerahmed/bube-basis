package de.wps.bube.basis.euregistry.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.euregistry.domain.EuRegistryException;

@ControllerAdvice("de.wps.bube.basis.euregistry.web")
public class EuRegistryExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ EuRegistryException.class })
    protected ResponseEntity<Object> handleEuRegistryException(EuRegistryException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

}
