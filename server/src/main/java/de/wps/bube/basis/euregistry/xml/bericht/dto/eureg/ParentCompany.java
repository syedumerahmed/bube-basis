package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ParentCompany {
    @XmlElement(required = true, name = "ParentCompanyDetails")
    private final ParentCompanyDetails details = new ParentCompanyDetails();

    public String getParentCompanyName() {
        return details.parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.details.parentCompanyName = parentCompanyName;
    }

    public String getParentCompanyURL() {
        return details.parentCompanyURL;
    }

    public void setParentCompanyURL(String parentCompanyURL) {
        this.details.parentCompanyURL = parentCompanyURL;
    }

    public ReferenceType getConfidentialityReason() {
        return details.confidentialityReason;
    }

    public void setConfidentialityReason(ReferenceType confidentialityReason) {
        this.details.confidentialityReason = confidentialityReason;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class ParentCompanyDetails {
        @XmlElement(required = true, name = "parentCompanyName")
        String parentCompanyName;
        @XmlElement(name = "parentCompanyURL")
        String parentCompanyURL;
        @XmlElement(name = "confidentialityReason")
        ReferenceType confidentialityReason;
    }
}
