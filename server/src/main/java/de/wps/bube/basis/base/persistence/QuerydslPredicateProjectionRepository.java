package de.wps.bube.basis.base.persistence;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;

@NoRepositoryBean
public interface QuerydslPredicateProjectionRepository<T> extends QuerydslPredicateExecutor<T> {

    @SuppressWarnings("rawtypes")
    <P> Page<P> findAll(Predicate predicate, Pageable pageable, FactoryExpression<P> factoryExpression, JoinProperties... leftJoins);

    <P> List<P> findAll(Predicate predicate, Sort sort, FactoryExpression<P> factoryExpression);

    <P> List<P> findAll(Predicate predicate, FactoryExpression<P> factoryExpression);

    <P> List<P> findDistinct(Predicate predicate, Sort sort, FactoryExpression<P> factoryExpression);

}
