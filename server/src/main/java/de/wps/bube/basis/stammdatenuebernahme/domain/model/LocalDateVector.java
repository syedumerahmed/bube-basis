package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import java.time.LocalDate;

import com.google.common.base.Objects;

public class LocalDateVector {

    private LocalDate spl;
    private LocalDate spb;
    private boolean equal;

    public LocalDateVector(LocalDate spl, LocalDate spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public LocalDate getSpl() {
        return spl;
    }

    public void setSpl(LocalDate spl) {
        this.spl = spl;
    }

    public LocalDate getSpb() {
        return spb;
    }

    public void setSpb(LocalDate spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
