package de.wps.bube.basis.base.persistence;

import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.QuerydslJpaPredicateExecutor;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.support.PageableExecutionUtils;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.JPQLQuery;

/**
 * Dieses Repository ermöglicht die Nutzung von eigenen SELECT-Statements
 * (wenn z.B. nicht die vollständige Entity benötigt wird) und Fremdschlüssel werden direkt
 * über INNER JOINs abgebildet anstatt in einer separaten Query nachgeladen zu werden.
 * <p>
 * Repository basiert auf einer Idee von <a href="https://stackoverflow.com/a/18313290">StackOverflow</a>
 *
 * @param <T>
 */
public class QuerydslPredicateProjectionRepositoryImpl<T> extends QuerydslJpaPredicateExecutor<T>
        implements QuerydslPredicateProjectionRepository<T> {

    private final Querydsl querydsl;

    public QuerydslPredicateProjectionRepositoryImpl(JpaEntityInformation<T, ?> entityInformation,
            EntityManager entityManager, EntityPathResolver resolver) {
        super(entityInformation, entityManager, resolver, null);
        var path = resolver.createPath(entityInformation.getJavaType());
        var builder = new PathBuilder<T>(path.getType(), path.getMetadata());
        this.querydsl = new Querydsl(entityManager, builder);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public <P> Page<P> findAll(Predicate predicate, Pageable pageable, FactoryExpression<P> expression, JoinProperties... leftJoins) {
        JPQLQuery<?> countQuery = createCountQuery(predicate);
        JPQLQuery<P> query = querydsl.applyPagination(pageable,
                createQuery(predicate).select(expression));
        Arrays.stream(leftJoins).forEach(join -> query.leftJoin(join.entityPath, join.path));
        return PageableExecutionUtils.getPage(query.fetch(), pageable, countQuery::fetchCount);
    }

    @Override
    public <P> List<P> findAll(Predicate predicate, FactoryExpression<P> factoryExpression) {
        return createQuery(predicate).select(factoryExpression).fetch();
    }

    @Override
    public <P> List<P> findAll(Predicate predicate, Sort sort, FactoryExpression<P> factoryExpression) {
        JPQLQuery<P> query = createQuery(predicate).select(factoryExpression);
        querydsl.applySorting(sort, query);

        return query.fetch();
    }

    @Override
    public <P> List<P> findDistinct(Predicate predicate, Sort sort, FactoryExpression<P> factoryExpression) {
        JPQLQuery<P> query = createQuery(predicate).select(factoryExpression).distinct();
        querydsl.applySorting(sort, query);

        return query.fetch();
    }
}
