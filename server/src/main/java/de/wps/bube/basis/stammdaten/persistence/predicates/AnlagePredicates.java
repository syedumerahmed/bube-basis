package de.wps.bube.basis.stammdaten.persistence.predicates;

import static de.wps.bube.basis.stammdaten.domain.entity.QAnlage.anlage;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.stammdaten.domain.vo.AnlageColumn;

public class AnlagePredicates {

    public static Predicate parentBetriebsstaetteIdEquals(final long betriebsstaetteId) {
        return anlage.parentBetriebsstaetteId.eq(betriebsstaetteId);
    }

    public static Predicate mapAnlageFiltersToPredicate(
            final List<ListStateDto.FilterStateDto<AnlageColumn>> filters) {
        if (filters == null) {
            return null;
        }
        return createPredicateByAnlageColumns(
                filters.stream().collect(Collectors.toMap(f -> f.property, f -> f.value)));
    }

    public static Predicate createPredicateByAnlageColumns(final Map<AnlageColumn, String> columns) {
        if (columns == null || columns.isEmpty()) {
            return null;
        }
        final BooleanBuilder booleanBuilder = new BooleanBuilder();
        columns.forEach((property, value) -> booleanBuilder.and(property.getExpression().containsIgnoreCase(value)));
        return booleanBuilder;
    }
}
