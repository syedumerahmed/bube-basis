package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.REINH;

import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;

public class LeistungBean {

    private final Double leistung;
    private final ReferenzBean leistungseinheit;
    private final String bezug;
    private final Leistungsklasse leistungsklasse;

    public LeistungBean(Double leistung, ReferenzBean leistungseinheit, String bezug, Leistungsklasse leistungsklasse) {
        this.leistung = leistung;
        this.leistungseinheit = leistungseinheit;
        this.bezug = bezug;
        this.leistungsklasse = leistungsklasse;
    }

    @RegelApi.Member(description = "Leistung")
    public Double getLeistung() {
        return leistung;
    }

    @RegelApi.Member(description = "Einheit", referenzliste = REINH)
    public ReferenzBean getLeistungseinheit() {
        return leistungseinheit;
    }

    @RegelApi.Member(description = "Bezug")
    public String getBezug() {
        return bezug;
    }

    @RegelApi.Member(description = "Klasse")
    public Leistungsklasse getLeistungsklasse() {
        return leistungsklasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        LeistungBean that = (LeistungBean) o;
        return Objects.equals(leistung, that.leistung) && Objects.equals(leistungseinheit,
                that.leistungseinheit) && Objects.equals(bezug,
                that.bezug) && leistungsklasse == that.leistungsklasse;
    }

    @Override
    public int hashCode() {
        return Objects.hash(leistung, leistungseinheit, bezug, leistungsklasse);
    }

    @Override
    public String toString() {
        return "LeistungBean{" +
                "leistung=" + leistung +
                ", leistungseinheit=" + leistungseinheit +
                ", bezug='" + bezug + '\'' +
                ", leistungsklasse=" + leistungsklasse +
                '}';
    }

}
