package de.wps.bube.basis.base.xml;

public class XMLImportException extends RuntimeException {
    public XMLImportException(Exception e) {
        super("XML-Import Fehler: " + e.getMessage(), e);
    }

    public XMLImportException(String message, Exception e) {
        super(message, e);
    }

    public XMLImportException(String message) {
        super(message);
    }
}
