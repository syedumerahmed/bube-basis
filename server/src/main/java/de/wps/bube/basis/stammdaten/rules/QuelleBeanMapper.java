package de.wps.bube.basis.stammdaten.rules;

import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;
import de.wps.bube.basis.stammdaten.rules.bean.QuelleBean;

@Mapper(uses = { GeoPunktBeanMapper.class, ReferenzBeanMapper.class })
public abstract class QuelleBeanMapper {

    @Autowired
    protected StammdatenService stammdatenService;

    @Mapping(target = "betriebsstaette", expression = "java(betriebsstaette(quelle, context, mappers))")
    @Mapping(target = "anlage", expression = "java(anlage(quelle, context, mappers))")
    public abstract QuelleBean toBean(Quelle quelle, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers);

    protected Supplier<BetriebsstaetteBean> betriebsstaette(Quelle quelle, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(
                    quelle.getParentBetriebsstaetteId());
            return mappers.getBetriebsstaetteBeanMapper().toBean(betriebsstaette, context, mappers);
        };
    }

    protected Supplier< AnlageBean> anlage(Quelle quelle, BeanMappingContext context, StammdatenBeanMappers mappers) {
        return () -> {
            Anlage anlage = stammdatenService.loadAnlage(quelle.getParentAnlageId());
            return mappers.getAnlageBeanMapper().toBean(anlage, context, mappers);
        };
    }

}
