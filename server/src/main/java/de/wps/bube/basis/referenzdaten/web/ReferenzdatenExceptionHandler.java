package de.wps.bube.basis.referenzdaten.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.referenzdaten.domain.ParameterWertNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;

@ControllerAdvice
public class ReferenzdatenExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ReferenzdatenException.class)
    protected ResponseEntity<Object> handleReferenzdatenException(ReferenzdatenException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ParameterWertNotFoundException.class)
    protected ResponseEntity<Object> handleParameterWertNotFoundException(ParameterWertNotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
