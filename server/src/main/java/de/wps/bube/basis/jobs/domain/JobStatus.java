package de.wps.bube.basis.jobs.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;

@Entity
@Table(name = "job_status")
@Inheritance(strategy = InheritanceType.JOINED)
public class JobStatus {

    public static final long REMOVE_MILLIES = 5000;

    @Id
    private String username;

    @NotNull
    private String jobName;
    private boolean completed;
    private String error;
    private long count;
    private long startMillis;
    private long stopMillis;
    private String protokoll;
    private boolean warnungVorhanden;

    @Deprecated
    protected JobStatus() {
    }

    public JobStatus(JobStatusEnum jobName) {
        this.jobName = jobName.name();
        this.startMillis = System.currentTimeMillis();
        this.stopMillis = 0;
    }

    public String getJobName() {
        return jobName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isCompleted() {
        return completed;
    }

    public String getError() {
        return error;
    }

    public long getCount() {
        return count;
    }

    public void setCompleted() {
        this.completed = true;
        this.stopMillis = System.currentTimeMillis();
    }

    public void setError(String errorMessage) {
        this.error = errorMessage;
        this.stopMillis = System.currentTimeMillis();
    }

    public boolean isRunning() {
        return this.stopMillis == 0;
    }

    private boolean hasError() {
        return error != null;
    }

    public void addToCount(long add) {
        this.count += add;
    }

    public void resetCount() {
        this.count = 0;
    }

    public long getRuntimeSec() {
        long runtimeMillis;
        if (isCompleted() || hasError()) {
            runtimeMillis = stopMillis - startMillis;
        } else {
            runtimeMillis = System.currentTimeMillis() - startMillis;
        }
        return runtimeMillis / 1000;
    }

    public boolean isOld(long currentTimeMillis) {
        return (isCompleted() || hasError()) && currentTimeMillis - stopMillis > REMOVE_MILLIES;
    }

    public boolean isWarnungVorhanden() {
        return warnungVorhanden;
    }

    public void setWarnungVorhanden(boolean warnungVorhanden) {
        this.warnungVorhanden = warnungVorhanden;
    }

    @Override
    public String toString() {
        return "JobStatus{" + "jobName='" + jobName + '\'' + ", username='" + username + '\'' + ", completed=" + completed + ", error=" + error + ", runtimeSec=" + getRuntimeSec() + ", count=" + count + '}';
    }

    public void setProtokoll(String protokoll) {
        this.protokoll = protokoll;
    }

    public String getProtokoll() {
        return protokoll;
    }

    public JobAction getAction() {
        return null;
    }
}
