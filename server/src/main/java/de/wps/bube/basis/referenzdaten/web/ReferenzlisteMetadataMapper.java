package de.wps.bube.basis.referenzdaten.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.ReferenzlisteMetadataDto;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzlisteMetadata;

@Mapper
public interface ReferenzlisteMetadataMapper {

    ReferenzlisteMetadataDto toDto(ReferenzlisteMetadata referenzlisteMetadata);

    ReferenzlisteMetadata fromDto(ReferenzlisteMetadataDto referenzlisteMetadataDto);

}
