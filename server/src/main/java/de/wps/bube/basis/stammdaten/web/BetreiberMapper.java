package de.wps.bube.basis.stammdaten.web;

import java.util.StringJoiner;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import de.wps.bube.basis.base.web.openapi.model.BetreiberDto;
import de.wps.bube.basis.base.web.openapi.model.BetreiberListItemDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;

@Mapper(uses = { ReferenzMapper.class })
public abstract class BetreiberMapper {

    public abstract BetreiberDto toDto(Betreiber betreiber);

    @Mapping(target = "display", source = ".", qualifiedByName = "betreiberDisplay")
    public abstract BetreiberListItemDto toListDto(Betreiber betreiber);

    @Named("betreiberDisplay")
    String betreiberDisplay(Betreiber betreiber) {
        var adresse = betreiber.getAdresse();
        var display = new StringJoiner(" - ");
        display.add(betreiber.getName());
        display.add(adresse.getPlz());
        display.add(adresse.getOrt());
        return display.toString();
    }

    public abstract Betreiber fromDto(BetreiberDto betreiberDto);

}
