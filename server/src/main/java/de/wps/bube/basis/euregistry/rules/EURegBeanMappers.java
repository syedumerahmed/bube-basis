package de.wps.bube.basis.euregistry.rules;

import org.springframework.stereotype.Component;

import de.wps.bube.basis.stammdaten.rules.AnlageBeanMapper;
import de.wps.bube.basis.stammdaten.rules.AnlagenteilBeanMapper;
import de.wps.bube.basis.stammdaten.rules.BetreiberBeanMapper;
import de.wps.bube.basis.stammdaten.rules.BetriebsstaetteBeanMapper;
import de.wps.bube.basis.stammdaten.rules.QuelleBeanMapper;
import de.wps.bube.basis.stammdaten.rules.StammdatenBeanMappers;

@Component
public class EURegBeanMappers extends StammdatenBeanMappers {

    private final EURegBetriebsstaetteBeanMapper euRegBetriebsstaetteBeanMapper;
    private final EURegAnlageBeanMapper euRegAnlageBeanMapper;
    private final EURegFBeanMapper euRegFBeanMapper;
    private final TeilberichtBeanMapper teilberichtBeanMapper;

    public EURegBeanMappers(BetriebsstaetteBeanMapper betriebsstaetteBeanMapper,
            AnlageBeanMapper anlageBeanMapper,
            AnlagenteilBeanMapper anlagenteilBeanMapper,
            QuelleBeanMapper quelleBeanMapper,
            BetreiberBeanMapper betreiberBeanMapper,
            EURegBetriebsstaetteBeanMapper euRegBetriebsstaetteBeanMapper,
            EURegAnlageBeanMapper euRegAnlageBeanMapper,
            EURegFBeanMapper euRegFBeanMapper,
            TeilberichtBeanMapper teilberichtBeanMapper) {
        super(betriebsstaetteBeanMapper, anlageBeanMapper, anlagenteilBeanMapper, quelleBeanMapper,
                betreiberBeanMapper);
        this.euRegBetriebsstaetteBeanMapper = euRegBetriebsstaetteBeanMapper;
        this.euRegAnlageBeanMapper = euRegAnlageBeanMapper;
        this.euRegFBeanMapper = euRegFBeanMapper;
        this.teilberichtBeanMapper = teilberichtBeanMapper;
    }

    public EURegBetriebsstaetteBeanMapper getEuRegBetriebsstaetteBeanMapper() {
        return euRegBetriebsstaetteBeanMapper;
    }

    public EURegAnlageBeanMapper getEuRegAnlageBeanMapper() {
        return euRegAnlageBeanMapper;
    }

    public EURegFBeanMapper getEuRegFBeanMapper() {
        return euRegFBeanMapper;
    }

    public TeilberichtBeanMapper getTeilberichtBeanMapper() {
        return teilberichtBeanMapper;
    }

}
