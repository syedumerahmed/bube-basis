package de.wps.bube.basis.referenzdaten.domain.event;

public class ReferenzdatenImportTriggeredEvent {

    public final String username;

    public ReferenzdatenImportTriggeredEvent(String username) {
        this.username = username;
    }
}
