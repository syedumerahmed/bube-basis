package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RVORS;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

@Entity
public class Vorschrift {

    private static final Set<String> CAN_HAVE_TAETIGKEITEN = Set.of(Referenzliste.R4BV.toString(),
            Referenzliste.RPRTR.toString(), Referenzliste.RIET.toString());

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vorschrift_seq")
    @SequenceGenerator(name = "vorschrift_seq", allocationSize = 50)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "art_referenz_id")
    @NotNull
    private Referenz art;

    @ManyToMany
    private List<Referenz> taetigkeiten;

    @ManyToOne
    private Referenz haupttaetigkeit;

    @Deprecated
    protected Vorschrift() {
    }

    public Vorschrift(Referenz art, List<Referenz> taetigkeiten, Referenz haupttaetigkeit) {
        Assert.notNull(art, "art");
        Assert.isTrue(RVORS.equals(art.getReferenzliste()), "art muss vom Typ RVORS sein");
        if (haupttaetigkeit != null) {
            Assert.isTrue(taetigkeiten != null && taetigkeiten.contains(haupttaetigkeit),
                    "Haupttätigkeit: \"%s\" muss in Tätigkeiten vorhanden sein".formatted(haupttaetigkeit.getKtext()));
        }
        this.art = art;
        this.taetigkeiten = taetigkeiten;
        this.haupttaetigkeit = haupttaetigkeit;
    }

    public Long getId() {
        return id;
    }

    public List<Referenz> getTaetigkeiten() {
        return taetigkeiten;
    }

    public List<Referenz> getTaetigkeitenOhneHaupttaetigkeit() {
        if (taetigkeiten == null) {
            return new ArrayList<>();
        }

        var ohneHaupttaetigkeit = new ArrayList<>(taetigkeiten);
        ohneHaupttaetigkeit.remove(haupttaetigkeit);
        return ohneHaupttaetigkeit;
    }

    public Referenz getHaupttaetigkeit() {
        return haupttaetigkeit;
    }

    public Referenz getArt() {
        return art;
    }

    private boolean canNotHaveTaetigkeiten() {
        return art.getStrg() != null && !CAN_HAVE_TAETIGKEITEN.contains(art.getStrg());
    }

    private boolean hasInvalidTaetigkeit() {
        return taetigkeiten.stream().anyMatch(ref -> !ref.getReferenzliste().toString().equals(art.getStrg()));
    }

    public boolean invalidTaetigkeiten() {
        return canNotHaveTaetigkeiten() || hasInvalidTaetigkeit();
    }

    public boolean hasTaetigkeiten() {
        return taetigkeiten != null && !taetigkeiten.isEmpty();
    }

    public boolean isIERL() {
        return VorschriftSchluessel.ofSchluessel(art.getSchluessel()).isIERL();
    }

    public boolean isPRTR() {
        return VorschriftSchluessel.ofSchluessel(art.getSchluessel()).isPRTR();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vorschrift that = (Vorschrift) o;
        return art.equals(that.art);
    }

    @Override
    public int hashCode() {
        return Objects.hash(art);
    }
}
