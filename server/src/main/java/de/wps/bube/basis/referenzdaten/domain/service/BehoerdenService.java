package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.QBehoerde.behoerde;
import static java.lang.String.format;

import java.util.List;
import java.util.Optional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.querydsl.core.types.dsl.Expressions;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.BehoerdeListItem;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAdapter;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenUse;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Service
@Transactional
@SecuredReferenzlistenUse
public class BehoerdenService {

    private final BehoerdenRepository repository;
    private final StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    private final BehoerdeBerechtigungsAccessor behoerdenBerechtigungen;
    private final EigeneBehoerdenBerechtigungsAcessor eigeneBehoerdenBerechtigungen;

    public BehoerdenService(BehoerdenRepository repository,
            StammdatenBerechtigungsContext stammdatenBerechtigungsContext,
            BehoerdeBerechtigungsAccessor behoerdenBerechtigungsAccessor,
            EigeneBehoerdenBerechtigungsAcessor eigeneBehoerdenBerechtigungen) {
        this.repository = repository;
        this.stammdatenBerechtigungsContext = stammdatenBerechtigungsContext;
        this.behoerdenBerechtigungen = behoerdenBerechtigungsAccessor;
        this.eigeneBehoerdenBerechtigungen = eigeneBehoerdenBerechtigungen;
    }

    @Transactional(readOnly = true)
    public Behoerde readBehoerde(Long id) {
        return repository.findOne(
                                 stammdatenBerechtigungsContext.createPredicateRead(behoerdenBerechtigungen).and(behoerde.id.eq(id)))
                         .orElseThrow(() -> new BubeEntityNotFoundException(Behoerde.class, "ID " + id));
    }

    @Transactional(readOnly = true)
    @Secured(Rollen.REFERENZLISTEN_LAND_DELETE)
    public Behoerde readBehoerdeForDelete(Long id) {
        return repository.findOne(
                                 stammdatenBerechtigungsContext.createPredicateDelete(behoerdenBerechtigungen).and(behoerde.id.eq(id)))
                         .orElseThrow(() -> new BubeEntityNotFoundException(Behoerde.class, "ID " + id));
    }

    private void readBehoerdeForWrite(Long id) {
        repository.findOne(
                          stammdatenBerechtigungsContext.createPredicateWrite(behoerdenBerechtigungen).and(behoerde.id.eq(id)))
                  .orElseThrow(() -> new BubeEntityNotFoundException(Behoerde.class, "ID " + id));
    }

    @Transactional(readOnly = true)
    public Optional<Behoerde> findBehoerde(String schluessel, Gueltigkeitsjahr jahr, Land land) {
        var jahrExpression = Expressions.asNumber(jahr.getJahr());
        return repository.findOne(stammdatenBerechtigungsContext.createPredicateRead(behoerdenBerechtigungen)
                                                                .and(behoerde.land.eq(land))
                                                                .and(behoerde.schluessel.eq(schluessel))
                                                                .and(jahrExpression.between(behoerde.gueltigVon.jahr,
                                                                        behoerde.gueltigBis.jahr)));
    }

    @Transactional(readOnly = true)
    public Optional<Behoerde> sucheBehoerdeUeberAlleJahre(String schluessel, Land land) {
        return repository.findFirstByLandAndSchluesselOrderByGueltigBisDesc(land, schluessel);
    }

    /**
     * Wird von den Mappern genutzt, um aus dem Schlüssel im DTO die Entity zu holen
     */
    @Transactional(readOnly = true)
    public Behoerde loadBehoerde(String schluessel, Gueltigkeitsjahr jahr, Land land) {
        return findBehoerde(schluessel, jahr, land).orElseThrow(
                () -> new BubeEntityNotFoundException(Behoerde.class));
    }

    @Transactional(readOnly = true)
    @Secured(Rollen.REFERENZLISTEN_LAND_READ)
    public List<BehoerdeListItem> readAllBehoerden() {
        return repository.findAllAsListItems(
                stammdatenBerechtigungsContext.createPredicateRead(behoerdenBerechtigungen));
    }

    @Transactional(readOnly = true)
    public List<BehoerdeListItem> readBehoerden(Gueltigkeitsjahr jahr, Land land) {
        var jahrExpression = Expressions.asNumber(jahr.getJahr());
        var predicate = stammdatenBerechtigungsContext.createPredicateRead(eigeneBehoerdenBerechtigungen)
                                                      .and(jahrExpression.between(behoerde.gueltigVon.jahr,
                                                              behoerde.gueltigBis.jahr));
        if (null != land) {
            predicate.and(behoerde.land.eq(land));
        }
        return repository.findAllAsListItems(predicate);
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public Behoerde createBehoerde(Behoerde behoerde) {
        Assert.isNull(behoerde.getId(), "Keine neue Behörde");
        return saveBehoerde(behoerde);
    }

    @Secured(Rollen.REFERENZLISTEN_LAND_WRITE)
    public Behoerde updateBehoerde(Behoerde behoerde) {
        Assert.notNull(behoerde.getId(), "Keine existierende Behörde");
        // Datenberechtigung prüfen
        readBehoerdeForWrite(behoerde.getId());
        return saveBehoerde(behoerde);
    }

    private Behoerde saveBehoerde(Behoerde behoerde) {
        stammdatenBerechtigungsContext.checkAccessToWrite(new BehoerdeBerechtigungsAdapter(behoerde));
        pruefeSchluessel(behoerde);
        behoerde.pruefeGueltigkeit();
        return repository.save(behoerde);
    }

    private void pruefeSchluessel(Behoerde behoerde) {
        if (repository.functionalKeyDuplicate(behoerde.getId(), behoerde.getLand().getNr(), behoerde.getSchluessel(),
                behoerde.getGueltigVon().getJahr(), behoerde.getGueltigBis().getJahr())) {
            throw new ReferenzdatenException(
                    format("Behörde ist für Schlüssel %s, Land %s und Berichtsjahr Zeitraum %d-%d nicht eindeutig",
                            behoerde.getSchluessel(), behoerde.getLand().getNr(), behoerde.getGueltigVon().getJahr(),
                            behoerde.getGueltigBis().getJahr()));
        }
    }

}
