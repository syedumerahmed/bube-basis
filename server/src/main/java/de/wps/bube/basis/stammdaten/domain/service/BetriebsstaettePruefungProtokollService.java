package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.BEHOERDENBENUTZER;
import static java.lang.String.format;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.transaction.Transactional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

@Service
public class BetriebsstaettePruefungProtokollService {

    private final KomplexpruefungService komplexpruefungService;
    private final StammdatenService stammdatenService;

    public BetriebsstaettePruefungProtokollService(KomplexpruefungService komplexpruefungService,
            StammdatenService stammdatenService) {
        this.komplexpruefungService = komplexpruefungService;
        this.stammdatenService = stammdatenService;
    }

    @Secured(BEHOERDENBENUTZER)
    @Transactional
    public void protokolliere(OutputStream outputStream, Long betriebsstaetteId) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            Betriebsstaette bst = stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);
            Komplexpruefung komplexpruefung = bst.getKomplexpruefung();
            if (komplexpruefung != null) {
                BetriebsstaettePruefungProtokollGenerator generator =
                        new BetriebsstaettePruefungProtokollGenerator(bst, komplexpruefung, komplexpruefungService);
                generator.protokolliereBetriebsstaette(out);
            } else {
                out.write(format("Die Komplexprüfung für Betriebsstätte %s wurde nicht gefunden",
                        bst.getBetriebsstaetteNr()));
            }
        }
    }

}
