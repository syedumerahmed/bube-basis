package de.wps.bube.basis.stammdatenbetreiber.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.SpbQuelleDto;
import de.wps.bube.basis.base.web.openapi.model.SpbQuelleListItemDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbQuelleListItem;

@Mapper(uses = { ReferenzMapper.class })
public interface SpbQuelleMapper {

    SpbQuelleDto toDto(SpbQuelle spbQuelle);

    SpbQuelle fromDto(SpbQuelleDto spbQuelleDto);

    SpbQuelleListItemDto toListItemDto(SpbQuelleListItem spbQuelleListItem);
}
