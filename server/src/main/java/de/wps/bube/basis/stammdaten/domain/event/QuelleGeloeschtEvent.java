package de.wps.bube.basis.stammdaten.domain.event;

public class QuelleGeloeschtEvent {
    public static final String EVENT_NAME = "Quelle gelöscht";

    public final Long quelleId;

    public QuelleGeloeschtEvent(Long quelleId) {
        this.quelleId = quelleId;
    }
}
