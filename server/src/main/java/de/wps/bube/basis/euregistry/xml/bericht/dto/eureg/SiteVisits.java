package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;

@XmlAccessorType(XmlAccessType.FIELD)
public class SiteVisits {
    @XmlElement(required = false, name = "SiteVisitsType", namespace = SchemaSet.SCHEMA_EUREG)
    private final SiteVisitsType siteVisitsType = new SiteVisitsType();

    public String getSiteVisitURL() {
        return siteVisitsType.siteVisitURL;
    }

    public void setSiteVisitURL(String siteVisitURL) {
        siteVisitsType.siteVisitURL = siteVisitURL;
    }

    public Integer getSiteVisitNumber() {
        return siteVisitsType.siteVisitNumber;
    }

    public void setSiteVisitNumber(Integer siteVisitNumber) {
        siteVisitsType.siteVisitNumber = siteVisitNumber;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class SiteVisitsType {
        @XmlElement(required = false, name = "siteVisitURL", namespace = SchemaSet.SCHEMA_EUREG)
        String siteVisitURL;

        @XmlElement(required = true, name = "siteVisitNumber", namespace = SchemaSet.SCHEMA_EUREG)
        Integer siteVisitNumber;
    }
}
