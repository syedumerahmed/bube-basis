package de.wps.bube.basis.euregistry.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.PageMapper;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaettenListStateDto;
import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteColumn;

@Mapper
public class EuRegListStateMapper {

    @Autowired
    PageMapper pageMapper;

    public ListStateDto<EuRegBetriebsstaetteColumn> fromEuRegBetriebsstaettenListStateDto(
            EuRegBetriebsstaettenListStateDto state) {
        ListStateDto<EuRegBetriebsstaetteColumn> listState = new ListStateDto<>();

        ListStateDto.SortStateDto<EuRegBetriebsstaetteColumn> sortState = new ListStateDto.SortStateDto<>();
        sortState.by = EuRegBetriebsstaetteColumn.valueOf(state.getSort().getBy().getValue());
        sortState.reverse = state.getSort().getReverse();

        List<ListStateDto.FilterStateDto<EuRegBetriebsstaetteColumn>> filterStates = new ArrayList<>();
        if (state.getFilters() != null) {
            state.getFilters().forEach(column -> {
                ListStateDto.FilterStateDto<EuRegBetriebsstaetteColumn> filterState = new ListStateDto.FilterStateDto<>();
                filterState.property = EuRegBetriebsstaetteColumn.valueOf(column.getProperty().getValue());
                filterState.value = column.getValue();
                filterStates.add(filterState);
            });
        }

        listState.page = pageMapper.mapQuellenFromDto(state.getPage());
        listState.sort = sortState;
        listState.filters = filterStates;

        return listState;
    }
}
