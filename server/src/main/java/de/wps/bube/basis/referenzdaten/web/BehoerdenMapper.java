package de.wps.bube.basis.referenzdaten.web;

import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.BehoerdeDto;
import de.wps.bube.basis.base.web.openapi.model.BehoerdeListItemDto;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.vo.BehoerdeListItem;

@Mapper(uses = { ReferenzMapper.class })
public abstract class BehoerdenMapper {

    @Autowired
    private BehoerdenService behoerdenService;

    public abstract BehoerdeDto toDto(Behoerde behoerde);

    public abstract BehoerdeListItemDto toDto(BehoerdeListItem behoerdeListItem);

    public abstract Behoerde fromDto(BehoerdeDto behoerdeDto);

    public Behoerde fromDto(BehoerdeListItemDto dto) {
        return dto != null ? behoerdenService.readBehoerde(dto.getId()) : null;
    }

}
