package de.wps.bube.basis.jobs.domain;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.persistence.JobStatusRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Component
public class BenutzerJobRegistry {

    private final Logger logger = LoggerFactory.getLogger(BenutzerJobRegistry.class);

    private final JobStatusRepository jobStatusRepository;
    private final AktiverBenutzer aktiverBenutzer;

    public BenutzerJobRegistry(JobStatusRepository jobStatusRepository, AktiverBenutzer aktiverBenutzer) {
        this.jobStatusRepository = jobStatusRepository;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobStarted(JobStatus newStatus) {
        if (hasRunningJob()) {
            throw new JobsException("Benutzer hat bereits einen laufenden Job");
        }
        jobStatusRepository.deleteByUsername(aktiverBenutzer.getUsername());
        newStatus.setUsername(aktiverBenutzer.getUsername());
        jobStatusRepository.save(newStatus);
        logger.info("Job started: " + newStatus);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public boolean hasRunningJob() {
        return this.getJobStatus().map(JobStatus::isRunning).orElse(false);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public Optional<JobStatus> getJobStatus() {
        return jobStatusRepository.findById(aktiverBenutzer.getUsername());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobCountChanged(long addToCount) {
        this.getJobStatus().ifPresent(status -> {
            status.addToCount(addToCount);
            jobStatusRepository.save(status);
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobCompleted(boolean warnungVorhanden) {
        this.getJobStatus().ifPresent(status -> {
            status.setCompleted();
            status.setWarnungVorhanden(warnungVorhanden);
            jobStatusRepository.save(status);
            logger.info("Job completed: " + status);
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobProtokoll(String protokoll) {
        this.getJobStatus().ifPresent(status -> {
            status.setProtokoll(protokoll);
            jobStatusRepository.save(status);
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobError(Exception e) {
        logger.error("Job error: Exception = " + e.getMessage());

        this.getJobStatus().ifPresent(status -> {
            status.setError(e.getMessage() != null ?
                    e.getMessage() :
                    "Beim Ausführen des Jobs ist ein unerwarteter Fehler aufgetreten");
            status.resetCount();
            jobStatusRepository.save(status);
            logger.error("Job error: " + status);
        });
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void jobErrorMultiple(String e) {
        this.getJobStatus().ifPresent(status -> {
            status.setError(e != null ?
                    e :
                    "Beim Ausführen des Jobs ist ein unerwarteter Fehler aufgetreten");
            status.resetCount();
            jobStatusRepository.save(status);
            logger.error("Job error: " + status);
        });
    }

    //@Scheduled(fixedDelay = 10000)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void removeOldJobs() {
        long millies = System.currentTimeMillis();

        for (JobStatus status : jobStatusRepository.findAll()) {
            if (status.isOld(millies)) {
                jobStatusRepository.delete(status);
            }
        }
    }

}
