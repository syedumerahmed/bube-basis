package de.wps.bube.basis.stammdatenuebernahme.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.web.openapi.api.StammdatenUebernahmeApi;
import de.wps.bube.basis.base.web.openapi.model.*;
import de.wps.bube.basis.stammdaten.web.AnlageMapper;
import de.wps.bube.basis.stammdaten.web.AnlagenteilMapper;
import de.wps.bube.basis.stammdaten.web.BetreiberMapper;
import de.wps.bube.basis.stammdaten.web.BetriebsstaetteMapper;
import de.wps.bube.basis.stammdaten.web.QuelleMapper;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenuebernahme.domain.service.StammdatenUebernahmeService;
import de.wps.bube.basis.stammdatenuebernahme.domain.service.StammdatenVergleichService;

@RestController
public class StammdatenUebernahmeRestController implements StammdatenUebernahmeApi {

    private final StammdatenVergleichService stammdatenVergleichService;
    private final SpbAnlagenteilVergleichMapper anlagenteilVergleichMapper;
    private final SpbAnlageVergleichMapper anlageVergleichMapper;
    private final SpbBetreiberVergleichMapper betreiberVergleichMapper;
    private final SpbQuelleVergleichMapper quelleVergleichMapper;
    private final SpbBetriebsstaetteVergleichMapper betriebsstaetteVergleichMapper;
    private final StammdatenUebernahmeService stammdatenUebernahmeService;
    private final StammdatenBetreiberService stammdatenBetreiberService;
    private final BetreiberMapper betreiberMapper;
    private final BetriebsstaetteMapper betriebsstaetteMapper;
    private final AnlageMapper anlageMapper;
    private final AnlagenteilMapper anlagenteilMapper;
    private final QuelleMapper quelleMapper;
    private final AnlageUebernahmeMapper anlageUebernahmeMapper;
    private final AnlagenteilUebernahmeMapper anlagenteilUebernahmeMapper;
    private final BetreiberUebernahmeMapper betreiberUebernahmeMapper;
    private final BetriebsstaetteUebernahmeMapper betriebsstaetteUebernahmeMapper;
    private final QuelleUebernahmeMapper quelleUebernahmeMapper;

    public StammdatenUebernahmeRestController(
            StammdatenVergleichService stammdatenVergleichService,
            SpbAnlagenteilVergleichMapper anlagenteilVergleichMapper,
            SpbAnlageVergleichMapper anlageVergleichMapper,
            SpbBetreiberVergleichMapper betreiberVergleichMapper,
            SpbQuelleVergleichMapper quelleVergleichMapper,
            SpbBetriebsstaetteVergleichMapper spbBetriebsstaetteVergleichMapper,
            StammdatenUebernahmeService stammdatenUebernahmeService,
            StammdatenBetreiberService stammdatenBetreiberService,
            BetreiberMapper betreiberMapper,
            BetriebsstaetteMapper betriebsstaetteMapper, AnlageMapper anlageMapper,
            AnlagenteilMapper anlagenteilMapper, QuelleMapper quelleMapper,
            AnlageUebernahmeMapper anlageUebernahmeMapper,
            AnlagenteilUebernahmeMapper anlagenteilUebernahmeMapper,
            BetreiberUebernahmeMapper betreiberUebernahmeMapper,
            BetriebsstaetteUebernahmeMapper betriebsstaetteUebernahmeMapper,
            QuelleUebernahmeMapper quelleUebernahmeMapper) {
        this.stammdatenVergleichService = stammdatenVergleichService;
        this.anlagenteilVergleichMapper = anlagenteilVergleichMapper;
        this.anlageVergleichMapper = anlageVergleichMapper;
        this.betreiberVergleichMapper = betreiberVergleichMapper;
        this.quelleVergleichMapper = quelleVergleichMapper;
        this.betriebsstaetteVergleichMapper = spbBetriebsstaetteVergleichMapper;
        this.stammdatenUebernahmeService = stammdatenUebernahmeService;
        this.stammdatenBetreiberService = stammdatenBetreiberService;
        this.betreiberMapper = betreiberMapper;
        this.betriebsstaetteMapper = betriebsstaetteMapper;
        this.anlageMapper = anlageMapper;
        this.anlagenteilMapper = anlagenteilMapper;
        this.quelleMapper = quelleMapper;
        this.anlageUebernahmeMapper = anlageUebernahmeMapper;
        this.anlagenteilUebernahmeMapper = anlagenteilUebernahmeMapper;
        this.betreiberUebernahmeMapper = betreiberUebernahmeMapper;
        this.betriebsstaetteUebernahmeMapper = betriebsstaetteUebernahmeMapper;
        this.quelleUebernahmeMapper = quelleUebernahmeMapper;
    }

    @Override
    public ResponseEntity<SpbAnlageVergleichDto> readAnlageSpbVergleichsobjekt(Long id) {
        return ResponseEntity.ok(anlageVergleichMapper.toDto(
                stammdatenVergleichService.getAnlageVergleich(id)
        ));
    }

    @Override
    public ResponseEntity<SpbAnlagenteilVergleichDto> readAnlagenteilSpbVergleichsobjekt(Long id) {
        return ResponseEntity.ok(
                anlagenteilVergleichMapper.toDto(stammdatenVergleichService.getAnlagenteilVergleich(id)));
    }

    @Override
    public ResponseEntity<SpbQuelleVergleichDto> readQuelleSpbVergleichsobjekt(Long id) {
        return ResponseEntity.ok(quelleVergleichMapper.toDto(
                stammdatenVergleichService.getQuelleVergleich(id)
        ));
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> uebernahmeAbschliessen(Long betriebsstaetteId) {
        return ResponseEntity.ok(betriebsstaetteMapper.toDto(
                this.stammdatenBetreiberService.schliesseBetriebsstaetteUebernahmeAb(betriebsstaetteId)));
    }

    @Override
    public ResponseEntity<AnlageDto> uebernehmeNeueAnlage(Long spbAnlageId, AnlageDto anlageDto) {
        return ResponseEntity.ok(anlageMapper.toDto(
                stammdatenUebernahmeService.uebernehmeNeueAnlage(spbAnlageId, anlageMapper.fromDto(anlageDto))));
    }

    @Override
    public ResponseEntity<AnlageDto> uebernehmeAnlage(AnlageUebernahmeDto anlageUebernahmeDto) {
        return ResponseEntity.ok(anlageMapper.toDto(
                stammdatenUebernahmeService.uebernehmeAnlage(anlageUebernahmeMapper.fromDto(anlageUebernahmeDto))));
    }

    @Override
    public ResponseEntity<AnlagenteilDto> uebernehmeNeuesAnlagenteil(Long spbAnlagenteilId,
            AnlagenteilDto anlagenteilDto) {
        return ResponseEntity.ok(
                anlagenteilMapper.toDto(
                        stammdatenUebernahmeService.uebernehmeNeuesAnlagenteil(
                                spbAnlagenteilId,
                                anlagenteilMapper.fromDto(anlagenteilDto))));
    }

    @Override
    public ResponseEntity<AnlagenteilDto> uebernehmeAnlagenteil(AnlagenteilUebernahmeDto anlagenteilUebernahmeDto) {
        return ResponseEntity.ok(
                anlagenteilMapper.toDto(
                        stammdatenUebernahmeService.uebernehmeAnlagenteil(
                                anlagenteilUebernahmeMapper.fromDto(anlagenteilUebernahmeDto))));
    }

    @Override
    public ResponseEntity<BetreiberDto> uebernehmeBetreiber(BetreiberUebernahmeDto betreiberUebernahmeDto) {
        return ResponseEntity.ok(betreiberMapper.toDto(stammdatenUebernahmeService.uebernehmeBetreiber(
                betreiberUebernahmeMapper.fromDto(betreiberUebernahmeDto))));
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> uebernehmeBetriebsstaette(
            BetriebsstaetteUebernahmeDto betriebsstaetteUebernahmeDto) {
        return ResponseEntity.ok(betriebsstaetteMapper.toDto(stammdatenUebernahmeService.uebernehmeBetriebsstaette(
                betriebsstaetteUebernahmeMapper.fromDto(betriebsstaetteUebernahmeDto))));
    }

    @Override
    public ResponseEntity<QuelleDto> uebernehmeNeueQuelle(Long spbQuelleId, QuelleDto quelleDto) {
        return ResponseEntity.ok(quelleMapper.toDto(
                stammdatenUebernahmeService.uebernehmeNeueQuelle(spbQuelleId, quelleMapper.fromDto(quelleDto))));
    }

    @Override
    public ResponseEntity<QuelleDto> uebernehmeQuelle(QuelleUebernahmeDto quelleUebernahmeDto) {
        return ResponseEntity.ok(quelleMapper.toDto(
                stammdatenUebernahmeService.uebernehmeQuelle(quelleUebernahmeMapper.fromDto(quelleUebernahmeDto))));
    }

    @Override
    public ResponseEntity<SpbBetreiberVergleichDto> readBetreiberSpbVergleichsobjekt(Long id) {
        return ResponseEntity.ok(betreiberVergleichMapper.toDto(
                stammdatenVergleichService.getBetreiberVergleich(id)
        ));
    }

    @Override
    public ResponseEntity<SpbBetriebsstaetteVergleichDto> readBetriebsstaetteSpbVergleichsobjekt(Long id) {
        return ResponseEntity.ok(betriebsstaetteVergleichMapper.toDto(
                stammdatenVergleichService.getBetriebsstaettenVergleich(id)
        ));
    }

}
