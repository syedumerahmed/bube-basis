package de.wps.bube.basis.euregistry.web.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageListItemDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

@Component
public class EuRegAnlagenTreeToListMapper {

    private final EURegistryService eURegistryService;

    public EuRegAnlagenTreeToListMapper(EURegistryService eURegistryService) {
        this.eURegistryService = eURegistryService;
    }

    public List<EuRegAnlageListItemDto> mapAnlagen(List<Anlage> anlagen) {
        List<EuRegAnlageListItemDto> anlagenListe = new ArrayList<>();

        for (Anlage a : anlagen) {
            mapAnlage(a, anlagenListe);
        }

        return anlagenListe;
    }

    private void mapAnlage(Anlage anlage, List<EuRegAnlageListItemDto> anlagenListe) {
        EuRegAnlageListItemDto dto = new EuRegAnlageListItemDto();
        Optional<EURegAnlage> berichtOpt = eURegistryService.findAnlagenBerichtByAnlageId(anlage.getId());

        if (berichtOpt.isPresent()) {
            dto.setBerichtsart(berichtOpt.get().getBerichtsdaten().getBerichtsart().getKtext());
            dto.setName(anlage.getName());
            dto.setNummer(anlage.getAnlageNr());
            if (anlage.getBetriebsstatus() != null) {
                dto.setBetriebsstatus(anlage.getBetriebsstatus().getKtext());
            }
            dto.setId(anlage.getId());
            dto.setIsAnlage(true);
            anlagenListe.add(dto);
        }

        for (Anlagenteil anlagenteil : anlage.getAnlagenteile()) {
            mapAnlagenteil(anlagenteil, anlagenListe, anlage.getAnlageNr());
        }
    }

    private void mapAnlagenteil(Anlagenteil anlagenteil, List<EuRegAnlageListItemDto> anlagenListe,
            String parentAnlageNr) {
        EuRegAnlageListItemDto dto = new EuRegAnlageListItemDto();
        Optional<EURegAnlage> berichtOpt = eURegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId());

        if (berichtOpt.isPresent()) {
            dto.setBerichtsart(berichtOpt.get().getBerichtsdaten().getBerichtsart().getKtext());
            dto.setName(anlagenteil.getName());
            dto.setNummer(anlagenteil.getAnlagenteilNr());
            if (anlagenteil.getBetriebsstatus() != null) {
                dto.setBetriebsstatus(anlagenteil.getBetriebsstatus().getKtext());
            }
            dto.setId(anlagenteil.getId());
            dto.setIsAnlage(false);
            dto.setParentAnlageNr(parentAnlageNr);
            anlagenListe.add(dto);
        }
    }
}
