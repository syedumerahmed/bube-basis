package de.wps.bube.basis.stammdaten.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.BetreiberApi;
import de.wps.bube.basis.base.web.openapi.model.BetreiberDto;
import de.wps.bube.basis.base.web.openapi.model.BetreiberListItemDto;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class BetreiberRestController implements BetreiberApi {

    private final StammdatenService stammdatenService;
    private final BetreiberMapper mapper;

    public BetreiberRestController(StammdatenService stammdatenService, BetreiberMapper betreiberMapper) {
        this.stammdatenService = stammdatenService;
        this.mapper = betreiberMapper;
    }

    @Override
    public ResponseEntity<List<BetreiberListItemDto>> readAllBetreiberByJahrAndLand( String jahrSchluessel, final String landSchluessel) {
        return ResponseEntity.ok(stammdatenService.loadAllBetreiberByJahrAndLand(jahrSchluessel, Land.of(landSchluessel))
                                .stream()
                                .map(mapper::toListDto)
                                .toList());
    }

    @Override
    public ResponseEntity<BetreiberDto> createBetreiber(String betriebsstaettenNummer, BetreiberDto betreiberDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.createBetreiberZuBetriebsstaette(mapper.fromDto(betreiberDto),
                betriebsstaettenNummer)));
    }

    @Override
    public ResponseEntity<BetreiberDto> readBetreiberByJahrLandNummer(String jahrSchluessel, String landSchluessel, String nummer) {
        return ResponseEntity.ok(mapper.toDto(
                stammdatenService.loadBetreiberByJahrLandNummer(jahrSchluessel, Land.of(landSchluessel), nummer)));
    }

    @Override
    public ResponseEntity<BetreiberDto> updateBetreiber(BetreiberDto betreiberDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.updateBetreiber(mapper.fromDto(betreiberDto))));
    }

    @Override
    public ResponseEntity<Void> deleteBetreiber(final Long id) {
        stammdatenService.deleteBetreiber(id);
        return ResponseEntity.ok().build();
    }
}
