package de.wps.bube.basis.stammdaten.domain.service;

import java.time.LocalDate;
import java.util.Objects;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;
import de.wps.bube.basis.stammdaten.domain.entity.Berichtsobjekt;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.persistence.BerichtsobjektRepository;

@Service
public class BerichtsobjektService {

    private final BerichtsobjektRepository berichtsobjektRepository;
    private final ReferenzenService referenzenService;
    private final StammdatenService stammdatenService;

    public BerichtsobjektService(BerichtsobjektRepository berichtsobjektRepository, ReferenzenService referenzenService,
            StammdatenService stammdatenService) {
        this.berichtsobjektRepository = berichtsobjektRepository;
        this.referenzenService = referenzenService;
        this.stammdatenService = stammdatenService;
    }

    @Transactional
    @Secured({ Rollen.BEHOERDE_WRITE, Rollen.LAND_WRITE })
    public void addBerichtsobjekt(Long betriebsstaetteId, String berichtsart, String bearbeitungsstatus,
            LocalDate bearbeitungsstatusSeit, String url) {

        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);
        Referenz berichtsartRef = referenzenService.readReferenz(Referenzliste.RRTYP, berichtsart,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand());
        Referenz statusRef = referenzenService.readReferenz(Referenzliste.RBEA, bearbeitungsstatus,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand());

        Berichtsobjekt berichtsobjekt = new Berichtsobjekt(betriebsstaetteId, berichtsartRef, statusRef,
                bearbeitungsstatusSeit, url);
        berichtsobjektRepository.save(berichtsobjekt);

    }

    @Transactional
    @Secured({ Rollen.BEHOERDE_DELETE, Rollen.LAND_DELETE })
    public void deleteBerichtsobjekt(Long betriebsstaetteId, String berichtsart) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);
        Referenz berichtsartRef = referenzenService.readReferenz(Referenzliste.RRTYP, berichtsart,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand());
        Berichtsobjekt berichtsobjekt = berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(
                                                                        betriebsstaetteId, berichtsartRef)
                                                                .orElseThrow(() -> new BubeEntityNotFoundException(
                                                                        Berichtsobjekt.class));
        // You'd expect that forceDelete() does the same thing as deleteById(). It doesn't. Don't ask me why.
        berichtsobjektRepository.forceDelete(berichtsobjekt.getId());
    }

    @Transactional
    @Secured({ Rollen.BEHOERDE_WRITE, Rollen.LAND_WRITE })
    public void updateBerichtsobjekt(Long betriebsstaetteId, String berichtsart, String bearbeitungsstatus,
            LocalDate bearbeitungsstatusSeit) {
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(betriebsstaetteId);
        Referenz berichtsartRef = referenzenService.readReferenz(Referenzliste.RRTYP, berichtsart,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand());
        Berichtsobjekt berichtsobjekt = berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(
                                                                        betriebsstaetteId, berichtsartRef)
                                                                .orElseThrow(() -> new BubeEntityNotFoundException(
                                                                        Berichtsobjekt.class));

        Referenz statusRef = referenzenService.readReferenz(Referenzliste.RBEA, bearbeitungsstatus,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand());

        if (!Objects.equals(statusRef, berichtsobjekt.getBearbeitungsstatus())
                || !Objects.equals(bearbeitungsstatusSeit, berichtsobjekt.getBearbeitungsstatusSeit())) {
            berichtsobjekt.setBearbeitungsstatus(statusRef);
            berichtsobjekt.setBearbeitungsstatusSeit(bearbeitungsstatusSeit);
            berichtsobjektRepository.save(berichtsobjekt);
        }

    }

}
