package de.wps.bube.basis.stammdaten.domain.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

@Embeddable
public class AbfallerzeugerNr {

    // Erzeuger-Nummer Abfall + Prüfziffer
    @NotEmpty
    private String abfallerzeugerNr;

    protected AbfallerzeugerNr() {
    }

    public AbfallerzeugerNr(final String nr) {
        this.abfallerzeugerNr = nr;
    }

    public String getAbfallerzeugerNr() {
        return abfallerzeugerNr;
    }

}
