package de.wps.bube.basis.referenzdaten.domain.event;

public class BehoerdenImportTriggeredEvent {

    public final String username;

    public BehoerdenImportTriggeredEvent(String username) {
        this.username = username;
    }
}
