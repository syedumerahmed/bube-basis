package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class IEDAnnexIActivity {
    @XmlElement(required = true, name = "IEDAnnexIActivityType")
    private final IEDAnnexIActivityType details = new IEDAnnexIActivityType();

    public String getMainActivity() {
        if (details.mainActivity == null) {
            return null;
        }
        return details.mainActivity.getCode();
    }

    public void setMainActivity(String mainActivity) {
        if (details.mainActivity == null) {
            details.mainActivity = new ReferenceType(SchemaSet.IED_ACTIVITY_VALUE_CODES, mainActivity);
        } else {
            details.mainActivity.setCode(mainActivity);
        }
    }

    public List<String> getOtherActivity() {
        if (details.otherActivity == null || details.otherActivity.isEmpty()) {
            return new ArrayList<>();
        }

        List<String> activities = new ArrayList<>();
        details.otherActivity.forEach(type -> activities.add(type.getCode()));
        return activities;
    }

    public void setOtherActivity(List<String> otherActivity) {
        details.otherActivity = new ArrayList<>();
        if (otherActivity != null) {
            otherActivity.forEach(activity -> details.otherActivity
                    .add(new ReferenceType(SchemaSet.IED_ACTIVITY_VALUE_CODES, activity)));
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class IEDAnnexIActivityType {
        @XmlElement(required = true, name = "mainActivity")
        ReferenceType mainActivity;

        @XmlElement(name = "otherActivity")
        List<ReferenceType> otherActivity;
    }
}
