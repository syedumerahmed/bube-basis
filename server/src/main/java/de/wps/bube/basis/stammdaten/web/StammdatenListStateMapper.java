package de.wps.bube.basis.stammdaten.web;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.PageMapper;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaettenListStateDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteListStateDto;
import de.wps.bube.basis.stammdaten.domain.vo.BetriebsstaetteColumn;

@Mapper
public class StammdatenListStateMapper {
    @Autowired
    PageMapper pageMapper;

    public ListStateDto<BetriebsstaetteColumn> fromBetriebsstaetteListItemDto(BetriebsstaettenListStateDto state) {
        ListStateDto<BetriebsstaetteColumn> listState = new ListStateDto<>();

        ListStateDto.SortStateDto<BetriebsstaetteColumn> sortState = new ListStateDto.SortStateDto<>();
        sortState.by = BetriebsstaetteColumn.valueOf(state.getSort().getBy().getValue());
        sortState.reverse = state.getSort().getReverse();

        List<ListStateDto.FilterStateDto<BetriebsstaetteColumn>> filterStates = new ArrayList<>();
        if (state.getFilters() != null) {
            state.getFilters().forEach(column -> {
                ListStateDto.FilterStateDto<BetriebsstaetteColumn> filterState = new ListStateDto.FilterStateDto<>();
                filterState.property = BetriebsstaetteColumn.valueOf(column.getProperty().getValue());
                filterState.value = column.getValue();
                filterStates.add(filterState);
            });
        }

        listState.page = pageMapper.mapQuellenFromDto(state.getPage());
        listState.sort = sortState;
        listState.filters = filterStates;

        return listState;
    }

    public ListStateDto<BetriebsstaetteColumn> fromSpbBetriebsstaetteListItemDto(SpbBetriebsstaetteListStateDto state) {
        ListStateDto<BetriebsstaetteColumn> listState = new ListStateDto<>();

        ListStateDto.SortStateDto<BetriebsstaetteColumn> sortState = new ListStateDto.SortStateDto<>();
        if (state.getSort() != null) {
            sortState.by = BetriebsstaetteColumn.valueOf(state.getSort().getBy().getValue());
            sortState.reverse = state.getSort().getReverse();
        } else {
            sortState.by = BetriebsstaetteColumn.NAME;
            sortState.reverse = false;
        }
        List<ListStateDto.FilterStateDto<BetriebsstaetteColumn>> filterStates = new ArrayList<>();
        if (state.getFilters() != null) {
            state.getFilters().forEach(column -> {
                ListStateDto.FilterStateDto<BetriebsstaetteColumn> filterState = new ListStateDto.FilterStateDto<>();
                filterState.property = BetriebsstaetteColumn.valueOf(column.getProperty().getValue());
                filterState.value = column.getValue();
                filterStates.add(filterState);
            });
        }

        listState.page = pageMapper.mapQuellenFromDto(state.getPage());
        listState.sort = sortState;
        listState.filters = filterStates;

        return listState;
    }
}
