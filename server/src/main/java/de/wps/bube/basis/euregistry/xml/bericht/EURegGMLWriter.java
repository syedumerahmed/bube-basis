package de.wps.bube.basis.euregistry.xml.bericht;

import static de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet.SCHEMA_GML;
import static java.lang.Boolean.TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.bind.Marshaller.JAXB_ENCODING;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static javax.xml.bind.Marshaller.JAXB_FRAGMENT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.apache.commons.io.output.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.xml.SimpleNamespaceContext;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import de.wps.bube.basis.base.xml.XMLExportException;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.FeatureMember;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class EURegGMLWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(EURegGMLWriter.class);

    @FunctionalInterface
    public interface EURegGMLErrorHandler extends ErrorHandler {

        void handleException(SAXParseException exception);

        @Override
        default void warning(SAXParseException exception) {
            handleException(exception);
        }

        @Override
        default void error(SAXParseException exception) {
            handleException(exception);
        }

        @Override
        default void fatalError(SAXParseException exception) {
            handleException(exception);
        }
    }

    private static final QName FEATURE_MEMBER_NAME = new QName(SCHEMA_GML, "featureMember", "gml");
    private static final QName CONTAINER_ELEMENT_NAME = new QName(SCHEMA_GML, "FeatureCollection", "gml");
    private static final SimpleNamespaceContext NAMESPACE_CONTEXT;
    private static final Map<String, String> NAMESPACES;

    static {
        NAMESPACES = Map.of("pf", "http://inspire.ec.europa.eu/schemas/pf/4.0",
                "base2", "http://inspire.ec.europa.eu/schemas/base2/2.0",
                "act_core", "http://inspire.ec.europa.eu/schemas/act-core/4.0",
                "base", "http://inspire.ec.europa.eu/schemas/base/3.3",
                "xsi", "http://www.w3.org/2001/XMLSchema-instance",
                "xlink", "http://www.w3.org/1999/xlink",
                "EUReg", "http://dd.eionet.europa.eu/schemaset/euregistryonindustrialsites",
                "gml", "http://www.opengis.net/gml/3.2");
        NAMESPACE_CONTEXT = new SimpleNamespaceContext();
        NAMESPACE_CONTEXT.setBindings(NAMESPACES);
    }

    private final XeurProperties.UbaProperties ubaProperties;
    private final EURegMapper euRegBerichtMapper;
    private final ReportDataMapper reportDataMapper;

    private final OutputStream outputStream;
    private final Referenz jahr;
    private final Marshaller featureMemberMarshaller;
    private final Validator xmlValidator;
    private final InspireParam inspireParam;
    private final Function<EURegBetriebsstaette, Stream<EURegAnlage>> anlageLoader;

    public EURegGMLWriter(XeurProperties.UbaProperties ubaProperties, EURegMapper euRegBerichtMapper,
            ReportDataMapper reportDataMapper, OutputStream outputStream,
            Referenz jahr, Schema schema, InspireParam inspireParam,
            Function<EURegBetriebsstaette, Stream<EURegAnlage>> anlageLoader) {
        this.ubaProperties = ubaProperties;
        this.euRegBerichtMapper = euRegBerichtMapper;
        this.reportDataMapper = reportDataMapper;
        this.outputStream = outputStream;
        this.jahr = jahr;
        this.inspireParam = inspireParam;
        this.anlageLoader = anlageLoader;
        try {
            featureMemberMarshaller = JAXBContext.newInstance(FeatureMember.class).createMarshaller();
            featureMemberMarshaller.setProperty(JAXB_ENCODING, UTF_8.toString());
            featureMemberMarshaller.setProperty(JAXB_FRAGMENT, TRUE);
            featureMemberMarshaller.setProperty(JAXB_FORMATTED_OUTPUT, TRUE);
        } catch (JAXBException e) {
            throw new XMLExportException(e);
        }
        this.xmlValidator = schema.newValidator();
    }

    public EURegGMLWriterProtokoll writeFeatureStream(Stream<EURegBetriebsstaette> entities)
            throws XMLStreamException, IOException {
        EURegGMLWriterProtokoll protokoll = new EURegGMLWriterProtokoll();
        var reportData = reportDataMapper.mapReportData(jahr.getSchluessel());

        File tempFile = File.createTempFile("EURegGMLWriter-", ".xml");

        try {
            try (FileOutputStream tempOut = new FileOutputStream(tempFile);
                    TeeOutputStream tee = new TeeOutputStream(outputStream, tempOut)) {

                XMLStreamWriter xsw = new IndentingXMLStreamWriter(
                        XMLOutputFactory.newInstance().createXMLStreamWriter(tee, UTF_8.toString()));

                xsw.setNamespaceContext(NAMESPACE_CONTEXT);
                writeHeader(xsw);
                writeReportData(reportData, xsw);
                writeFeatureMembers(entities, reportData, xsw, protokoll);
                writeEnd(xsw);
                xsw.close();
            }

            protokoll.setGlobalValidation();
            try (FileInputStream tempIn = new FileInputStream(tempFile)) {
                try {
                    xmlValidator.validate(new StreamSource(tempIn));
                } catch (SAXException e) {
                    throw new IllegalStateException(e);
                }
            }

            return protokoll;
        } finally {
            if (tempFile.exists()) {
                try {
                    tempFile.delete();
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    private void writeHeader(XMLStreamWriter xsw) throws XMLStreamException {
        xsw.writeStartDocument(UTF_8.toString(), null);
        xsw.writeStartElement(CONTAINER_ELEMENT_NAME.getPrefix(), CONTAINER_ELEMENT_NAME.getLocalPart(),
                CONTAINER_ELEMENT_NAME.getNamespaceURI());
        for (var ns : NAMESPACES.entrySet()) {
            xsw.writeNamespace(ns.getKey(), ns.getValue());
        }
    }

    private void writeReportData(ReportData reportData, XMLStreamWriter xsw) {
        try {
            featureMemberMarshaller.marshal(wrapFeature(new FeatureMember(reportData)), xsw);
        } catch (JAXBException e) {
            throw new XMLExportException(e);
        }
    }

    private void writeFeatureMembers(Stream<EURegBetriebsstaette> entities, ReportData reportData,
            XMLStreamWriter xsw, EURegGMLWriterProtokoll protokoll) {

        var productionSiteIds = new AtomicLong();
        xmlValidator.setErrorHandler(
                (EURegGMLErrorHandler) exception -> protokoll.addValidationError(exception.getMessage()));

        entities.forEach(entity -> {
            try {
                List<EURegAnlage> euRegAnlageList = anlageLoader.apply(entity).toList();
                List<String> anlageLocalIds = euRegAnlageList.stream().map(EURegAnlage::getLocalId).toList();

                protokoll.start(entity);
                Stream<EURegAnlage> euRegAnlageStream = euRegAnlageList.stream().peek(protokoll::start);
                Stream<FeatureMember> featureMembers
                        = euRegBerichtMapper.toXDto(entity, euRegAnlageStream, productionSiteIds::incrementAndGet,
                                                    reportData, inspireParam, ubaProperties, anlageLocalIds)
                                            .peek(protokoll::log);
                featureMembers.map(EURegGMLWriter::wrapFeature)
                              .forEach(feature -> {
                                  DOMResult domResult = new DOMResult();
                                  try {
                                      featureMemberMarshaller.marshal(feature, xsw);
                                      featureMemberMarshaller.marshal(feature, domResult);
                                      xmlValidator.validate(new DOMSource(domResult.getNode()));
                                  } catch (JAXBException | SAXException | IOException e) {
                                      throw new XMLExportException(e);
                                  }
                              });
            } catch (ReferenceType.MissingCodeException e) {
                protokoll.addMappingError(e.getMessage());
            }
        });
    }

    private void writeEnd(XMLStreamWriter xsw) throws XMLStreamException {
        xsw.writeEndElement();
        xsw.writeEndDocument();
    }

    private static JAXBElement<FeatureMember> wrapFeature(FeatureMember feature) {
        return new JAXBElement<>(FEATURE_MEMBER_NAME, FeatureMember.class, feature);
    }
}
