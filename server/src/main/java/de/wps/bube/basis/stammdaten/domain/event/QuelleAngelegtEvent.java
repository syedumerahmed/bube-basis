package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

import de.wps.bube.basis.stammdaten.domain.entity.Quelle;

public class QuelleAngelegtEvent {
    public static final String EVENT_NAME = "Quelle angelegt";

    public final Quelle quelle;

    public QuelleAngelegtEvent(Quelle q) {
        this.quelle = q;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        QuelleAngelegtEvent that = (QuelleAngelegtEvent) o;
        return Objects.equals(quelle, that.quelle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quelle);
    }
}
