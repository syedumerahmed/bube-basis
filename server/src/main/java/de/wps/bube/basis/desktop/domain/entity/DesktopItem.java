package de.wps.bube.basis.desktop.domain.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class DesktopItem {

    @EmbeddedId
    private DesktopItemId desktopItemId;

    @NotNull
    private String jahrSchluessel;

    @Deprecated
    public DesktopItem() {
    }

    public DesktopItem(DesktopItemId desktopItemId, @NotNull String jahrSchluessel) {
        this.desktopItemId = desktopItemId;
        this.jahrSchluessel = jahrSchluessel;
    }

    public DesktopItemId getDesktopItemId() {
        return desktopItemId;
    }

    public String getJahrSchluessel() {
        return jahrSchluessel;
    }

}
