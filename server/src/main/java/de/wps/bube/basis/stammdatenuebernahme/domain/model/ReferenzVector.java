package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class ReferenzVector {
    private Referenz spl;
    private Referenz spb;
    private boolean equal;

    public ReferenzVector(Referenz spl, Referenz spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public Referenz getSpl() {
        return spl;
    }

    public void setSpl(Referenz spl) {
        this.spl = spl;
    }

    public Referenz getSpb() {
        return spb;
    }

    public void setSpb(Referenz spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }
}
