package de.wps.bube.basis.stammdatenbetreiber.web;

import java.util.List;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.PageMapper;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.api.SpbBetriebsstaettenApi;
import de.wps.bube.basis.base.web.openapi.model.PageDtoSpbBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteListStateDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.vo.BetriebsstaetteColumn;
import de.wps.bube.basis.stammdaten.web.StammdatenListStateMapper;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;

@RestController
public class SpbBetriebsstaettenRestController implements SpbBetriebsstaettenApi {

    private final StammdatenBetreiberService service;
    private final SpbBetriebsstaetteMapper mapper;
    private final PageMapper pageMapper;
    private final StammdatenListStateMapper listStateMapper;
    private final ReferenzMapper referenzMapper;

    SpbBetriebsstaettenRestController(StammdatenBetreiberService service, SpbBetriebsstaetteMapper mapper,
            PageMapper pageMapper, StammdatenListStateMapper listStateMapper, ReferenzMapper referenzMapper) {
        this.service = service;
        this.mapper = mapper;
        this.pageMapper = pageMapper;
        this.listStateMapper = listStateMapper;
        this.referenzMapper = referenzMapper;
    }

    @Override
    public ResponseEntity<PageDtoSpbBetriebsstaetteListItemDto> readAllSpbBetriebsstaette(Long berichtsjahrId,
            SpbBetriebsstaetteListStateDto state) {
        ListStateDto<BetriebsstaetteColumn> listState = listStateMapper.fromSpbBetriebsstaetteListItemDto(state);
        return ResponseEntity.ok(pageMapper.mapSpbBetriebsstaetteListItemDto(
                service.suche(berichtsjahrId, listState.toPageable())
                       .map(mapper::toListDto)));
    }

    @Override
    public ResponseEntity<List<Long>> readAllSpbBetriebsstaetteIds(Long berichtsjahrId) {
        return ResponseEntity.ok(service.sucheIds(berichtsjahrId));
    }

    @Override
    public ResponseEntity<SpbBetriebsstaetteDto> readSpbBetriebsstaetteByJahrLandNummer(String jahr, String land,
            String nummer) {
        return ResponseEntity.ok(mapper.toDto(
                service.loadSpbBetriebsstaetteByJahrLandNummer(jahr, Land.of(land), nummer)));
    }

    @Override
    public ResponseEntity<SpbBetriebsstaetteDto> createSpbBetriebsstaette(
            @Valid SpbBetriebsstaetteDto betriebsstaetteDto) {
        return ResponseEntity.ok(mapper.toDto(service.createSpbBetriebsstaette(mapper.fromDto(betriebsstaetteDto))));
    }

    @Override
    public ResponseEntity<List<ReferenzDto>> getAllYearsForBetriebsstaette(Long bstId) {
        return ResponseEntity.ok(service.getAllYearsForBetriebsstaette(bstId).map(referenzMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<SpbBetriebsstaetteDto> updateSpbBetriebsstaette(
            @Valid SpbBetriebsstaetteDto betriebsstaetteDto) {
        return ResponseEntity.ok(mapper.toDto(service.updateSpbBetriebsstaette(mapper.fromDto(betriebsstaetteDto))));
    }

}
