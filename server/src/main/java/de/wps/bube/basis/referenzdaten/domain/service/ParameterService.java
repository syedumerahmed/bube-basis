package de.wps.bube.basis.referenzdaten.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ParameterWertNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.persistence.ParameterRepository;
import de.wps.bube.basis.referenzdaten.persistence.ParameterWertRepository;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenDelete;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenRead;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenUse;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenWrite;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

@Service
public class ParameterService {

    private static final String PARAMETERWERT_NICHT_EINDEUTIG = "Parameterwert ist für Schlüssel, Land, Behörde und Berichtsjahr Zeitraum nicht eindeutig";
    private final ParameterRepository parameterRepository;
    private final ParameterWertRepository parameterWertRepository;
    private final AktiverBenutzer aktiverBenutzer;

    public ParameterService(ParameterRepository parameterRepository, ParameterWertRepository parameterWertRepository,
            AktiverBenutzer aktiverBenutzer) {
        this.parameterRepository = parameterRepository;
        this.parameterWertRepository = parameterWertRepository;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenRead
    public List<Parameter> readParameterliste() {
        return parameterRepository.findAll();
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenRead
    public Parameter readParameter(String schluessel) {
        return parameterRepository.findById(schluessel)
                                  .orElseThrow(() -> new BubeEntityNotFoundException(Parameter.class, schluessel));
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenRead
    public List<ParameterWert> readParameterwerteForEdit(String pschluessel) {
        // Gesamtadmin sieht alle Paramterwerte.
        // Sonst nur Paramterwerte aus dem Land des Benutzers und Deutschland.
        if (aktiverBenutzer.isGesamtadmin()) {
            return parameterWertRepository.findByPschluessel(pschluessel);
        } else {
            return parameterWertRepository.findByPschluesselAndLandIn(pschluessel,
                    List.of(Land.DEUTSCHLAND, aktiverBenutzer.getLand()));
        }
    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Optional<ParameterWert> findParameterWert(String schluessel, Gueltigkeitsjahr jahr, Land land) {
        Assert.notNull(schluessel, "schluessel == null");
        Assert.notNull(jahr, "berichtsjahrSchluessel == null");
        Assert.notNull(land, "land == null");

        return parameterWertRepository.findByKeyWithoutBehoerde(land, schluessel, jahr)
                                      .or(() -> parameterWertRepository.findByKeyWithoutBehoerde(Land.DEUTSCHLAND,
                                              schluessel, jahr));

    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public Optional<ParameterWert> findParameterWert(String schluessel, Gueltigkeitsjahr jahr, Land land,
            Long behoerdeId) {
        Assert.notNull(schluessel, "schluessel == null");
        Assert.notNull(jahr, "berichtsjahrSchluessel == null");
        Assert.notNull(land, "land == null");
        Assert.notNull(behoerdeId, "behoerdeId == null");

        return parameterWertRepository.findByKeyWithBehoerde(land, schluessel, jahr, behoerdeId)
                                      .or(() -> findParameterWert(schluessel, jahr, land));

    }

    @Transactional(readOnly = true)
    @SecuredReferenzlistenUse
    public ParameterWert readParameterWert(String schluessel, Gueltigkeitsjahr jahr, Land land) {
        return findParameterWert(schluessel, jahr, land).orElseThrow(
                () -> new ParameterWertNotFoundException(schluessel, land, jahr));
    }

    @Transactional
    @SecuredReferenzlistenWrite
    public ParameterWert createParameterWert(ParameterWert parameterWert) {
        hatDatenberechtigungWrite(parameterWert);
        pruefeSchluesselCreate(parameterWert);
        parameterWert.pruefeDaten();
        return parameterWertRepository.save(parameterWert);
    }

    @Transactional
    @SecuredReferenzlistenWrite
    public ParameterWert updateParameterWert(ParameterWert parameterWert) {
        hatDatenberechtigungWrite(parameterWert);
        pruefeSchluesselUpdate(parameterWert);
        parameterWert.pruefeDaten();
        return parameterWertRepository.save(parameterWert);
    }

    @Transactional
    @SecuredReferenzlistenDelete
    public void deleteParameterWert(Long id) {
        hatDatenberechtigungDelete(this.parameterWertRepository.findById(id)
                                                               .orElseThrow(() -> new BubeEntityNotFoundException(
                                                                       ParameterWert.class)));
        parameterWertRepository.deleteById(id);
    }

    @Transactional
    @SecuredReferenzlistenUse
    public boolean behoerdeUsedForBehoerdeLoeschen(Behoerde behoerde) {
        return parameterWertRepository.existsParameterWertByBehoerde(behoerde);
    }

    private void hatDatenberechtigungWrite(ParameterWert parameterWert) {
        if (aktiverBenutzer.isGesamtadmin()) {
            return;
        }
        if (parameterWert.getLand().equals(Land.DEUTSCHLAND) && aktiverBenutzer.hatRolle(
                Rolle.REFERENZLISTEN_BUND_WRITE)) {
            return;
        }
        if (parameterWert.getLand().equals(aktiverBenutzer.getLand()) && aktiverBenutzer.hatRolle(
                Rolle.REFERENZLISTEN_LAND_WRITE)) {
            return;
        }
        throw new ReferenzdatenException("Keine Datenberechtigung für Schreiben des Parameterwertes");
    }

    private void hatDatenberechtigungDelete(ParameterWert parameterWert) {
        if (aktiverBenutzer.isGesamtadmin()) {
            return;
        }
        if (parameterWert.getLand().equals(Land.DEUTSCHLAND) && aktiverBenutzer.hatRolle(
                Rolle.REFERENZLISTEN_BUND_DELETE)) {
            return;
        }
        if (parameterWert.getLand().equals(aktiverBenutzer.getLand()) && aktiverBenutzer.hatRolle(
                Rolle.REFERENZLISTEN_LAND_DELETE)) {
            return;
        }
        throw new ReferenzdatenException("Keine Datenberechtigung für Löschen der Parameterwertes");
    }

    private void pruefeSchluesselUpdate(ParameterWert parameterWert) {
        Assert.notNull(parameterWert.getId(), "id != null");

        // Werte sind eindeutig für Schlüssel + Land + Berichtsjahr Zeitraum + Behörde

        // Prüfe Gültig Von bei Update
        if (parameterWertRepository.existsParameterwertByIdNotAndLandAndPschluesselAndGueltigVonBetweenAndBehoerde(
                parameterWert.getId(), parameterWert.getLand(), parameterWert.getPschluessel(),
                parameterWert.getGueltigVon(), parameterWert.getGueltigBis(), parameterWert.getBehoerde())) {
            throw new ReferenzdatenException(PARAMETERWERT_NICHT_EINDEUTIG);
        }

        // Prüfe Gültig Bis bei Update
        if (parameterWertRepository.existsParameterwertByIdNotAndLandAndPschluesselAndGueltigBisBetweenAndBehoerde(
                parameterWert.getId(), parameterWert.getLand(), parameterWert.getPschluessel(),
                parameterWert.getGueltigVon(), parameterWert.getGueltigBis(), parameterWert.getBehoerde())) {
            throw new ReferenzdatenException(PARAMETERWERT_NICHT_EINDEUTIG);
        }
    }

    private void pruefeSchluesselCreate(ParameterWert parameterWert) {
        Assert.isNull(parameterWert.getId(), "id == null");

        // Werte sind eindeutig für Name + Schlüssel + Land + Berichtsjahr Zeitraum + Behörde

        // Prüfe Gültig Von bei Create
        if (parameterWertRepository.existsParameterwertByLandAndPschluesselAndGueltigVonBetweenAndBehoerde(
                parameterWert.getLand(),
                parameterWert.getPschluessel(), parameterWert.getGueltigVon(), parameterWert.getGueltigBis(),
                parameterWert.getBehoerde())) {
            throw new ReferenzdatenException(PARAMETERWERT_NICHT_EINDEUTIG);
        }

        // Prüfe Gültig Bis bei Create
        if (parameterWertRepository.existsParameterwertByLandAndPschluesselAndGueltigBisBetweenAndBehoerde(
                parameterWert.getLand(),
                parameterWert.getPschluessel(), parameterWert.getGueltigVon(), parameterWert.getGueltigBis(),
                parameterWert.getBehoerde())) {
            throw new ReferenzdatenException(PARAMETERWERT_NICHT_EINDEUTIG);
        }

    }
}
