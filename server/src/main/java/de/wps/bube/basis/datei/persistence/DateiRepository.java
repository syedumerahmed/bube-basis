package de.wps.bube.basis.datei.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.datei.domain.entity.Datei;
import de.wps.bube.basis.datei.domain.entity.DateiScope;

public interface DateiRepository extends JpaRepository<Datei, Long> {

    Optional<Datei> findByUsernameAndDateiScope(String username, DateiScope dateiScope);

    void deleteAllByUsernameAndDateiScope(String username, DateiScope dateiScope);

    void deleteAllByTempTrue();

    @Modifying
    @Query(value = "DELETE FROM pg_largeobject WHERE loid NOT IN (SELECT DISTINCT data FROM datei)", nativeQuery = true)
    void deleteLargeObjectOrphans();
}
