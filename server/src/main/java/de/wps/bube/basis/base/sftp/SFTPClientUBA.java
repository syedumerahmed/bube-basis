package de.wps.bube.basis.base.sftp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.OpenSSHKnownHosts;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import net.schmizz.sshj.userauth.password.PasswordUtils;
import net.schmizz.sshj.xfer.FileSystemFile;

public class SFTPClientUBA {

    //Mit "ssh-keyscan -H 194.29.233.175" erzeugt. Der vom UBE gelieferte Key hat nicht funktionert.
    private final String known_hosts = "|1|CwJOZYyA0LD6d9qn8L46UwMsNP8=|5EFI2Wk0sv42w+pjbMJRBQrJkZc= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPYiAq/wl8+o/iPCpN9ZkqvuirwLtRRc+dS2m7SdBZjM";

    private final String remoteHost = "194.29.233.175";
    private final String username = "uba";
    private final String privateKey = "C:\\Users\\tim\\Downloads\\id_ecdsa_WPS";
    private final String passphrase = "aloha5guru";

    public void uploadFile() throws IOException {
        final SSHClient ssh = new SSHClient();

        InputStream entry = new ByteArrayInputStream(known_hosts.getBytes(Charset.defaultCharset()));
        ssh.addHostKeyVerifier(new OpenSSHKnownHosts(new InputStreamReader(entry, Charset.defaultCharset())));

        ssh.connect(remoteHost);

        try {
            File keyFile = new File(privateKey);
            KeyProvider keyProvider = ssh.loadKeys(keyFile.toString(),
                    PasswordUtils.createOneOff(passphrase.toCharArray()));
            ssh.authPublickey(username, keyProvider);

            try (SFTPClient sftp = ssh.newSFTPClient()) {
                String localFile = "C://projekte/bube-basis/server/src/main/resources/database/changelog/changelog-0.0.0.xml";
                sftp.put(new FileSystemFile(localFile), "wps-test.xml");
            }
        } finally {
            ssh.disconnect();
        }
    }
}
