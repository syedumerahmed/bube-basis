package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class NominalCapacity {
    @XmlElement(required = true, name = "CapacityWasteIncinerationType")
    private final CapacityWasteIncinerationType capacityWasteIncinerationType = new CapacityWasteIncinerationType();

    public double getTotalNormalCapacity() {
        return capacityWasteIncinerationType.totalNormalCapacity;
    }

    public void setTotalNormalCapacity(Double totalNormalCapacity) {
        capacityWasteIncinerationType.totalNormalCapacity = totalNormalCapacity;
    }

    public double getPermittedCapacityHazardous() {
        return capacityWasteIncinerationType.permittedCapacityHazardous;
    }

    public void setPermittedCapacityHazardous(Double permittedCapacityHazardous) {
        capacityWasteIncinerationType.permittedCapacityHazardous = permittedCapacityHazardous;
    }

    public double getPermittedCapacityNonHazardous() {
        return capacityWasteIncinerationType.permittedCapacityNonHazardous;
    }

    public void setPermittedCapacityNonHazardous(Double permittedCapacityNonHazardous) {
        capacityWasteIncinerationType.permittedCapacityNonHazardous = permittedCapacityNonHazardous;
    }

    private static class CapacityWasteIncinerationType {
        @XmlElement(required = true, name = "totalNominalCapacityAnyWasteType")
        Double totalNormalCapacity;
        @XmlElement(required = true, name = "permittedCapacityHazardous")
        Double permittedCapacityHazardous;
        @XmlElement(required = true, name = "permittedCapacityNonHazardous")
        Double permittedCapacityNonHazardous;
    }
}
