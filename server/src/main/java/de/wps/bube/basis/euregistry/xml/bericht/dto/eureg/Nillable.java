package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class Nillable {
    @XmlValue
    private String value;

    @XmlAttribute(name = "nilreason")
    private String nilReason;

    public static Nillable getUpdatedNillable(Nillable oldNillable, String value) {
        if (value == null) {
            return null;
        }
        Nillable newNillable = oldNillable;
        if (oldNillable == null) {
            newNillable = new Nillable();
        }
        newNillable.setValue(value);
        return newNillable;
    }

    public static String getValueOfNillable(Nillable nillable) {
        if (nillable == null) {
            return null;
        } else {
            return nillable.getValue();
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNilReason() {
        return nilReason;
    }

    public void setNilReason(String nilReason) {
        this.nilReason = nilReason;
    }
}
