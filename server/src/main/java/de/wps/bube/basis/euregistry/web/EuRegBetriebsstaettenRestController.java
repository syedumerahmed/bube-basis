package de.wps.bube.basis.euregistry.web;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.web.ValidierungshinweisMapper;
import de.wps.bube.basis.base.web.openapi.api.EuregBetriebsstaettenApi;
import de.wps.bube.basis.base.web.openapi.model.DateiDto;
import de.wps.bube.basis.base.web.openapi.model.Validierungshinweis;
import de.wps.bube.basis.euregistry.domain.service.EURegistryImportExportService;

@Controller
public class EuRegBetriebsstaettenRestController implements EuregBetriebsstaettenApi {

    private final EURegistryImportExportService euRegistryImportExportService;
    private final ValidierungshinweisMapper validierungshinweisMapper;

    public EuRegBetriebsstaettenRestController(
        EURegistryImportExportService euRegistryImportExportService,
        ValidierungshinweisMapper validierungshinweisMapper) {
        this.euRegistryImportExportService = euRegistryImportExportService;
        this.validierungshinweisMapper = validierungshinweisMapper;
    }

    @Override
    public ResponseEntity<Void> euregDateiLoeschen() {
        euRegistryImportExportService.dateiLoeschen();
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<DateiDto> existierendeEuregDatei() {
        DateiDto dateiDto = new DateiDto();
        dateiDto.setDateiName(euRegistryImportExportService.existierendeDatei());
        return ResponseEntity.ok(dateiDto);
    }

    @Override
    public ResponseEntity<Void> importEureg(String jahrSchluessel) {
        euRegistryImportExportService.triggerImport(jahrSchluessel);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> uploadEureg(MultipartFile file) {
        euRegistryImportExportService.dateiSpeichern(file);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Validierungshinweis>> validiereEureg(String jahrSchluessel) {
        return ResponseEntity.ok(euRegistryImportExportService.validiere(jahrSchluessel)
                                                              .stream()
                                                              .map(validierungshinweisMapper::mapToDto)
                                                              .toList());
    }
}
