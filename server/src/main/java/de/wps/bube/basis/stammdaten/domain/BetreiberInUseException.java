package de.wps.bube.basis.stammdaten.domain;

public class BetreiberInUseException extends RuntimeException {
    public BetreiberInUseException() {
        super("Betreiber kann nicht gelöscht werden, da er mind. einer weiteren Betriebsstätte zugewiesen ist.");
    }
}
