package de.wps.bube.basis.stammdaten.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.PageMapper;
import de.wps.bube.basis.base.web.ValidierungshinweisMapper;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.api.BetriebsstaettenApi;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteDto;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaettenListStateDto;
import de.wps.bube.basis.base.web.openapi.model.DateiDto;
import de.wps.bube.basis.base.web.openapi.model.DownloadKomplexpruefungCommand;
import de.wps.bube.basis.base.web.openapi.model.ExportiereStammdatenCommand;
import de.wps.bube.basis.base.web.openapi.model.KomplexpruefungDurchfuehrenCommand;
import de.wps.bube.basis.base.web.openapi.model.PageDtoBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.base.web.openapi.model.Suchattribute;
import de.wps.bube.basis.base.web.openapi.model.UebernimmBetriebsstaettenInJahr;
import de.wps.bube.basis.base.web.openapi.model.Validierungshinweis;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaettePruefungEvent;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettePruefungProtokollService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenImportExportService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.service.SucheService;
import de.wps.bube.basis.stammdaten.domain.vo.BetriebsstaetteColumn;

@Controller
class BetriebsstaettenRestController implements BetriebsstaettenApi {

    private final StammdatenService stammdatenService;
    private final StammdatenImportExportService importExportService;
    private final SucheService sucheService;
    private final BetriebsstaetteMapper mapper;
    private final ReferenzMapper referenzMapper;
    private final ValidierungshinweisMapper validierungshinweisMapper;
    private final PageMapper pageMapper;
    private final SuchattributeMapper suchattributeMapper;
    private final StammdatenListStateMapper listStateMapper;
    private final BetriebsstaettePruefungProtokollService betriebsstaettePruefungProtokollService;
    private final AktiverBenutzer aktiverBenutzer;
    private final ApplicationEventPublisher eventPublisher;

    public BetriebsstaettenRestController(StammdatenService stammdatenService,
            StammdatenImportExportService importExportService, SucheService sucheService,
            BetriebsstaetteMapper betriebsstaetteMapper, ReferenzMapper referenzMapper,
            ValidierungshinweisMapper validierungshinweisMapper,
            PageMapper pageMapper,
            SuchattributeMapper suchattributeMapper,
            StammdatenListStateMapper listStateMapper,
            BetriebsstaettePruefungProtokollService betriebsstaettePruefungProtokollService,
            AktiverBenutzer aktiverBenutzer, ApplicationEventPublisher eventPublisher) {
        this.stammdatenService = stammdatenService;
        this.importExportService = importExportService;
        this.sucheService = sucheService;
        this.mapper = betriebsstaetteMapper;
        this.referenzMapper = referenzMapper;
        this.validierungshinweisMapper = validierungshinweisMapper;
        this.pageMapper = pageMapper;
        this.suchattributeMapper = suchattributeMapper;
        this.listStateMapper = listStateMapper;
        this.betriebsstaettePruefungProtokollService = betriebsstaettePruefungProtokollService;
        this.aktiverBenutzer = aktiverBenutzer;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public ResponseEntity<PageDtoBetriebsstaetteListItemDto> readAllBetriebsstaette(Long berichtsjahrId,
            BetriebsstaettenListStateDto state) {
        ListStateDto<BetriebsstaetteColumn> listState = listStateMapper.fromBetriebsstaetteListItemDto(state);

        return ResponseEntity.ok(
                pageMapper.mapBetriebsstaetteListItemToDto(
                        sucheService.suche(berichtsjahrId,
                                suchattributeMapper.fromDto(state.getSuche()), listState.toPageable())
                                    .map(mapper::toListDto)));
    }

    @Override
    public ResponseEntity<List<Long>> readAllBetriebsstaetteIds(Long berichtsjahrId, Suchattribute suchattribute) {
        return ResponseEntity.ok(sucheService.sucheIds(berichtsjahrId, suchattributeMapper.fromDto(suchattribute)));
    }

    @Override
    public ResponseEntity<Resource> exportStammdaten(ExportiereStammdatenCommand exportiereStammdatenCommand) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        this.importExportService.exportStammdaten(outputStream,
                exportiereStammdatenCommand.getBetriebsstaetten());

        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType("application/octet-stream"))
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"stammdaten.xml\"")
                             .body(byteArrayResource);
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> createBetriebsstaette(BetriebsstaetteDto betriebsstaetteDto) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.createBetriebsstaette(mapper.fromDto(betriebsstaetteDto))));
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> readBetriebsstaette(Long id) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.loadBetriebsstaetteForRead(id)));
    }

    @Override
    public ResponseEntity<Boolean> canWriteBst(Long id) {
        return ResponseEntity.ok(stammdatenService.canWriteBetriebsstaette(id));
    }

    @Override
    public ResponseEntity<Boolean> canDeleteBst(Long id) {
        return ResponseEntity.ok(stammdatenService.canDeleteBetriebsstaette(id));
    }

    @Override
    public ResponseEntity<List<BetriebsstaetteDto>> findBetriebsstaetteByNummerNameOrt(String suchstring) {
        return ResponseEntity.ok(stammdatenService.findBetriebsstaetteByNummerNameOrtLimitBy10(suchstring)
                                                  .map(mapper::toDto)
                                                  .getContent());
    }

    @Override
    public ResponseEntity<List<ReferenzDto>> getAllYearsForBetriebsstaette(Long id) {
        return ResponseEntity.ok(stammdatenService.getAllYearsForBetriebsstaetteId(id).map(referenzMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> readBetriebsstaetteByJahrLandNummer(String jahrSchluessel,
            String landSchluessel, String nummer) {
        return ResponseEntity.ok(mapper.toDto(
                stammdatenService.loadBetriebsstaetteByJahrLandNummer(jahrSchluessel, Land.of(landSchluessel),
                        nummer)));
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> readBetriebsstaetteByJahrLandLocalId(String jahrSchluessel,
            String landSchluessel, String localId) {
        return ResponseEntity.ok(mapper.toDto(
                stammdatenService.loadBetriebsstaetteByJahrLandLocalId(jahrSchluessel, Land.of(landSchluessel),
                        localId)));
    }

    @Override
    public ResponseEntity<BetriebsstaetteDto> updateBetriebsstaette(BetriebsstaetteDto betriebsstaetteDto) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.updateBetriebsstaette(mapper.fromDto(betriebsstaetteDto))));
    }

    @Override
    public ResponseEntity<Void> deleteBetriebsstaette(Long id) {
        stammdatenService.deleteBetriebsstaette(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleBetriebsstaetten(List<Long> ids) {
        stammdatenService.deleteMultipleBetriebsstaetten(ids);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> betriebsstaettenUebernehmen(UebernimmBetriebsstaettenInJahr uebernimmCommand) {
        stammdatenService.uebernimmBetriebsstaetten(uebernimmCommand.getBetriebsstaettenIds(),
                referenzMapper.fromDto(uebernimmCommand.getZieljahr()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<DateiDto> existierendeStammdatenDatei() {
        DateiDto dateiDto = new DateiDto();
        dateiDto.setDateiName(importExportService.existierendeDatei());
        return ResponseEntity.ok(dateiDto);
    }

    @Override
    public ResponseEntity<Void> uploadStammdaten(MultipartFile file) {
        importExportService.dateiSpeichern(file);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Validierungshinweis>> validiereStammdaten() {
        return ResponseEntity.ok(
                importExportService.validiere().stream().map(validierungshinweisMapper::mapToDto).toList());
    }

    @Override
    public ResponseEntity<Void> importStammdaten(String jahrSchluessel) {
        importExportService.triggerImport(jahrSchluessel);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> stammdatenDateiLoeschen() {
        importExportService.dateiLoeschen();
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> runKomplexpruefung(KomplexpruefungDurchfuehrenCommand command) {
        eventPublisher.publishEvent(new BetriebsstaettePruefungEvent(command.getIds(), aktiverBenutzer.getUsername()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> komplexpruefungDownload(DownloadKomplexpruefungCommand command) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (!stammdatenService.canReadBetriebsstaette(command.getBstId())) {
            outputStream.writeBytes("Betriebsstätte nicht gefunden oder keine Berechtigung zum Öffnen".getBytes());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(byteArrayResource);
        }
        try {
            betriebsstaettePruefungProtokollService.protokolliere(outputStream, command.getBstId());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

            return ResponseEntity.ok()
                                 .contentType(MediaType.TEXT_PLAIN)
                                 .header(HttpHeaders.CONTENT_DISPOSITION,
                                         "attachment; filename=\"komplexpruefung.txt\"")
                                 .body(byteArrayResource);
        } catch (IOException e) {
            outputStream.writeBytes("Betriebsstätte nicht gefunden oder keine Berechtigung zum Öffnen".getBytes());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(byteArrayResource);
        }
    }

    @Override
    public ResponseEntity<Void> schreibsperreBetreiberdaten(Long id, Boolean sperre) {
        stammdatenService.updateSchreibsperreBetreiber(id, sperre);
        return ResponseEntity.ok().build();
    }
}
