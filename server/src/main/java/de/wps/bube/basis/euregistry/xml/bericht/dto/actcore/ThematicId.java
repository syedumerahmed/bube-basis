package de.wps.bube.basis.euregistry.xml.bericht.dto.actcore;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base2.ThematicIdentifier;

@XmlAccessorType(XmlAccessType.FIELD)
public class ThematicId {

    @XmlElement(required = true, name = "ThematicIdentifier", namespace = SchemaSet.SCHEMA_BASE2)
    private final ThematicIdentifier thematicIdentifier = new ThematicIdentifier();

    public String getIdentifier() {
        return thematicIdentifier.getIdentifier();
    }

    public void setIdentifier(String identifier) {
        thematicIdentifier.setIdentifier(identifier);
    }

    public String getIdentifierScheme() {
        return thematicIdentifier.getIdentifierScheme();
    }

    public void setIdentifierScheme(String identifierScheme) {
        thematicIdentifier.setIdentifierScheme(identifierScheme);
    }
}
