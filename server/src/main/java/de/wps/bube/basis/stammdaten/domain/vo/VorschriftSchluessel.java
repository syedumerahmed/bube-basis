package de.wps.bube.basis.stammdaten.domain.vo;

import java.util.EnumSet;
import java.util.stream.Stream;

public enum VorschriftSchluessel {
    V_11BV("11BV"), V_13BV("13BV"), V_17BV("17BV"), V_IE_RL("IE-RL"), V_R4BV("R4BV"), V_RPRTR("RPRTR");

    private static final EnumSet<VorschriftSchluessel> BST_PFLICHTIG = EnumSet.of(V_13BV, V_17BV, V_IE_RL, V_RPRTR);
    private static final EnumSet<VorschriftSchluessel> ANL_PFLICHTIG = EnumSet.of(V_13BV, V_17BV, V_IE_RL);
    private static final EnumSet<VorschriftSchluessel> FEUERUNGSANL_PFLICHTIG = EnumSet.of(V_13BV, V_17BV);

    private final String schluessel;

    VorschriftSchluessel(String schluessel) {
        this.schluessel = schluessel;
    }

    public String getSchluessel() {
        return schluessel;
    }

    public boolean brauchtBetriebsstaetteBericht() {
        return BST_PFLICHTIG.contains(this);
    }

    public boolean brauchtAnlageBericht() {
        return ANL_PFLICHTIG.contains(this);
    }

    public boolean brauchtFeuerungsanlageBericht() {
        return FEUERUNGSANL_PFLICHTIG.contains(this);
    }

    public boolean is17BV() {
        return this == V_17BV;
    }

    public boolean is13BV() {
        return this == V_13BV;
    }

    public boolean isIERL() {
        return this == V_IE_RL;
    }

    public boolean isPRTR() {
        return this == V_RPRTR;
    }

    public static boolean schluesselExist(String schluessel) {
        return Stream.of(values()).map(VorschriftSchluessel::getSchluessel).anyMatch(schluessel::equals);
    }

    public static VorschriftSchluessel ofSchluessel(String schluessel) {
        return Stream.of(values())
                     .filter(v -> v.schluessel.equals(schluessel))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiger Vorschriftschlüssel: " + schluessel));
    }
}
