package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rollen.LAND_WRITE;
import static java.lang.String.format;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.vo.Validierungshinweis.Severity;
import de.wps.bube.basis.base.xml.ValidationErrorHandler;
import de.wps.bube.basis.base.xml.XMLExportException;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.event.EuRegistryImportTriggeredEvent;
import de.wps.bube.basis.euregistry.xml.EURegBstXmlMapper;
import de.wps.bube.basis.euregistry.xml.EuRegistryImportException;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBetriebsstaetteImportDto;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBst;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBstListXDto;
import de.wps.bube.basis.euregistry.xml.dto.ObjectFactory;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobsException;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@Service
@Transactional
@Secured(LAND_READ)
public class EURegistryImportExportService {

    private final EURegistryService euRegistryService;
    private final XMLService xmlService;
    private final EURegBstXmlMapper euRegBstXmlMapper;
    private final ApplicationEventPublisher eventPublisher;
    private final AktiverBenutzer aktiverBenutzer;
    private final DateiService dateiService;

    public EURegistryImportExportService(EURegistryService euRegistryService, XMLService xmlService,
            EURegBstXmlMapper euRegBstXmlMapper, ApplicationEventPublisher eventPublisher,
            AktiverBenutzer aktiverBenutzer, DateiService dateiService) {
        this.euRegistryService = euRegistryService;
        this.xmlService = xmlService;
        this.euRegBstXmlMapper = euRegBstXmlMapper;
        this.eventPublisher = eventPublisher;
        this.aktiverBenutzer = aktiverBenutzer;
        this.dateiService = dateiService;
    }

    @Transactional(readOnly = true)
    public void export(OutputStream outputStream, List<Long> betriebsstaettenBerichtIds) {
        // Überprüfe Land Eindeutigkeit
        if (aktiverBenutzer.isGesamtadmin() && !euRegistryService.haveSameLand(betriebsstaettenBerichtIds)) {
            throw new JobsException("Ausgewählte Berichtsdaten sind unterschiedlichen Bundesländern zugeordnet");
        }

        // Jahr muss eindeutig sein
        if (!euRegistryService.haveSameJahr(betriebsstaettenBerichtIds)) {
            throw new JobsException("Ausgewählte Berichtsdaten stammen aus unterschiedlichen Berichtsjahren");
        }

        // Wird für bessere Fehlermeldungen benötigt, da XML-Service fachfremd ist
        ValidationErrorHandler<EURegBetriebsstaette> errorHandler = (euRegBst, error) -> {
            throw new XMLExportException(
                    "Berichtsdaten für Betriebsstätte %s ungültig. Technische Ursache: %s".formatted(
                            euRegBst.getBetriebsstaette().getBetriebsstaetteNr(),
                            error.getLinkedException().getMessage()),
                    error);
        };

        var objectFactory = new ObjectFactory();

        var writer = xmlService.createWriter(EuRegBst.class, EuRegBstListXDto.QUALIFIED_NAME,
                                       euRegBstXmlMapper::toXDto, objectFactory::createEuRegBst)
                               .withErrorHandler(errorHandler);

        writer.write(euRegistryService.streamAllByBerichtIdIn(betriebsstaettenBerichtIds), outputStream);
    }

    @Secured(LAND_WRITE)
    @Transactional(readOnly = true)
    public String existierendeDatei() {
        return dateiService.existingDatei(aktiverBenutzer.getUsername(), DateiScope.EUREG);
    }

    @Secured(LAND_DELETE)
    public void dateiSpeichern(MultipartFile file) {
        dateiService.dateiSpeichern(file, aktiverBenutzer.getUsername(), DateiScope.EUREG);
    }

    @Secured(LAND_DELETE)
    public void triggerImport(String berichtsjahr) {
        eventPublisher.publishEvent(new EuRegistryImportTriggeredEvent(berichtsjahr));
    }

    @Secured(LAND_DELETE)
    public void dateiLoeschen() {
        dateiService.loescheDatei(aktiverBenutzer.getUsername(), DateiScope.EUREG);
    }

    @Secured(LAND_DELETE)
    @Transactional(readOnly = true)
    public List<Validierungshinweis> validiere(String berichtsjahr) {
        List<Validierungshinweis> hinweise = new ArrayList<>();

        try {
            // Das XML-Schema Validieren
            InputStream inputStream = dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG);
            xmlService.validate(inputStream, EuRegBstListXDto.XSD_SCHEMA_URL, hinweise);

            hinweise.addAll(validateImportFile(berichtsjahr));
        } catch (XMLImportException e) {
            hinweise.add(Validierungshinweis.of(e.getLocalizedMessage(), Severity.FEHLER));
        } catch (Exception e) {
            throw new EuRegistryImportException("Es ist ein Fehler bei der Validierung aufgetreten", e);
        }

        return hinweise;
    }

    @Secured(LAND_DELETE)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importiereEuRegBetriebsstaette(EntityWithError<EuRegBetriebsstaetteImportDto> importDtoEntityWithError,
            String berichtsjahr, JobProtokoll jobProtokoll) {
        if (!validiereImport(importDtoEntityWithError, berichtsjahr, jobProtokoll)) {
            var betriebsstaette = importDtoEntityWithError.entity.getEuRegBetriebsstaette().getBetriebsstaette();
            throw new EuRegistryImportException(
                    "Berichtsdaten für Betriebsstätte %s Nr. %s mit Local-ID %s konnten nicht importiert werden"
                            .formatted(betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr(),
                                    betriebsstaette.getLocalId()));
        }
        importiere(importDtoEntityWithError.entity, jobProtokoll);
    }

    private List<Validierungshinweis> validateImportFile(String berichtsjahr) {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        // Fachliche Validierung: Jahr und Land ist eindeutig
        Set<String> uniqueLaender = new HashSet<>();
        InputStream inputStream = dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG);
        xmlService.read(inputStream, EuRegBst.class,
                (Function<EuRegBst, EntityWithError<EuRegBst>>) EntityWithError::new,
                EuRegBstListXDto.XML_ROOT_ELEMENT_NAME, null, euRegBst -> {
                    var rawXmlElement = euRegBst.entity;
                    uniqueLaender.add(rawXmlElement.getLand());
                    if (!berichtsjahr.equals(rawXmlElement.getJahr())) {
                        hinweise.add(Validierungshinweis.of(
                                "Ungültiges Berichtsjahr '%s' in Datensatz".formatted(rawXmlElement.getJahr()),
                                Severity.FEHLER));
                    }
                });
        if (uniqueLaender.size() != 1) {
            hinweise.add(Validierungshinweis.of(
                    "Es können nur die Daten eines Landes importiert werden, vorhanden sind Berichtsdaten für die Länder " +
                    String.join(", ", uniqueLaender), Severity.FEHLER));
        }
        return hinweise;
    }

    private boolean validiereImport(EntityWithError<EuRegBetriebsstaetteImportDto> euregBstWithErrors,
            String berichtsjahr, JobProtokoll jobProtokoll) {
        var erfolg = true;
        var euregBst = euregBstWithErrors.entity;
        var betriebsstaette = euregBst.getEuRegBetriebsstaette().getBetriebsstaette();

        if (!berichtsjahr.equals(betriebsstaette.getBerichtsjahr().getSchluessel())) {
            jobProtokoll.addEintrag(
                    "EuRegBetriebsstaette für Betriebsstätte %s (Local-ID: %s) für falsches Jahr angegeben: %s".formatted(
                            betriebsstaette.getName(), betriebsstaette.getLocalId(),
                            betriebsstaette.getBerichtsjahr().getKtext()));
            erfolg = false;
        }

        for (var mappingError : euregBstWithErrors.errorsOrEmpty()) {
            if (!mappingError.istWarnung()) {
                erfolg = false;
            }
            jobProtokoll.addEintrag(mappingError);
        }

        return erfolg;
    }

    private void importiere(EuRegBetriebsstaetteImportDto importDto, JobProtokoll jobProtokoll) {
        updateBstBericht(importDto.getEuRegBetriebsstaette(), jobProtokoll);
        importDto.getAnlagen().forEach(euRegAnlage -> updateAnlagenBericht(euRegAnlage, jobProtokoll));
    }

    private void updateBstBericht(EURegBetriebsstaette euRegBetriebsstaette, JobProtokoll jobProtokoll) {
        var betriebsstaette = euRegBetriebsstaette.getBetriebsstaette();
        var existingBerichtId = euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(
                                                         betriebsstaette.getBerichtsjahr().getSchluessel(), betriebsstaette.getLocalId())
                                                 .map(EURegBetriebsstaette::getId);
        if (existingBerichtId.isEmpty()) {
            jobProtokoll.addEintrag(
                    format("EuRegBetriebsstaette für Betriebsstätte %s (Nr. %s) mit Local-ID %s wurde nicht gefunden",
                            betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr(),
                            betriebsstaette.getLocalId()));
            return;
        }
        euRegBetriebsstaette.setId(existingBerichtId.get());
        euRegistryService.updateBetriebsstaettenBericht(euRegBetriebsstaette);

        jobProtokoll.addEintrag(
                format("EuRegBetriebsstaette für Betriebsstätte %s (Nr. %s) mit Local-ID %s wurde aktualisiert.",
                        betriebsstaette.getName(), betriebsstaette.getBetriebsstaetteNr(),
                        betriebsstaette.getLocalId()));
    }

    private void updateAnlagenBericht(EURegAnlage euRegAnlage, JobProtokoll jobProtokoll) {
        String description;
        Optional<EURegAnlage> anlagenBericht;
        if (euRegAnlage.getAnlage() != null) {
            description = format("EuRegAnl für Anlage %s (Nr. %s) mit Local-ID %s",
                    euRegAnlage.getAnlage().getName(), euRegAnlage.getAnlage().getAnlageNr(),
                    euRegAnlage.getAnlage().getLocalId());
            anlagenBericht = euRegistryService.findAnlagenBerichtByAnlageId(euRegAnlage.getAnlage().getId());
        } else {
            description = format("EuRegAnl für Anlagenteil %s (Nr. %s) mit Local-ID %s",
                    euRegAnlage.getAnlagenteil().getName(), euRegAnlage.getAnlagenteil().getAnlagenteilNr(),
                    euRegAnlage.getAnlagenteil().getLocalId());
            anlagenBericht = euRegistryService.findAnlagenBerichtByAnlagenteilId(euRegAnlage.getAnlagenteil().getId());
        }
        if (anlagenBericht.isEmpty()) {
            jobProtokoll.addEintrag(description + " wurde nicht gefunden");
            return;
        }
        euRegAnlage.setId(anlagenBericht.get().getId());
        euRegistryService.updateAnlagenBericht(euRegAnlage);

        jobProtokoll.addEintrag(description + " wurde aktualisiert.");
    }
}
