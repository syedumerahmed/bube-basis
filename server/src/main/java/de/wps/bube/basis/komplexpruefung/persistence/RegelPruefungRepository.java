package de.wps.bube.basis.komplexpruefung.persistence;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;

public interface RegelPruefungRepository
        extends JpaRepository<RegelPruefung, Long>, JpaSpecificationExecutor<RegelPruefung> {

    void deleteByKomplexpruefung(Komplexpruefung komplexpruefung);

    Stream<RegelPruefung> findByKomplexpruefung(Komplexpruefung komplexpruefung);
}