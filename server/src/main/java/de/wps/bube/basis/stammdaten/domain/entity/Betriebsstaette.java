package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

/**
 * Die Betriebsstätte hat Anlagen und Quellen.
 * Die Kombination land, zustaendigeBehoerde, betriebsstaetteNr) ist eindeutig.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Betriebsstaette {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "betriebsstaette_seq")
    @SequenceGenerator(name = "betriebsstaette_seq", allocationSize = 50)
    private Long id;

    private String localId;
    private String referenzAnlagenkataster;

    @NotNull
    @ManyToOne
    private Referenz berichtsjahr;

    @NotNull
    private Land land;

    @NotNull
    @ManyToOne
    private Behoerde zustaendigeBehoerde;

    // Aufgabenbereichskennzahl
    private String akz;

    @NotEmpty
    private String betriebsstaetteNr;

    @ManyToOne
    private Betreiber betreiber;

    // hat immer genau eine Adresse
    @Embedded
    @AssociationOverride(name = "vertraulichkeitsgrund", joinColumns = @JoinColumn(name = "adresse_vertraulichkeitsgrund_id"))
    private Adresse adresse;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ansprechpartner> ansprechpartner;

    @ElementCollection
    private List<Kommunikationsverbindung> kommunikationsverbindungen;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    @NotNull
    @ManyToOne
    private Referenz gemeindekennziffer;

    // hat EinleiterNr-n 0..n
    @ElementCollection
    private List<EinleiterNr> einleiterNummern;

    // hat Vorschriften 0..n
    @ManyToMany
    private List<Referenz> vorschriften;

    // hat AbfallENr-n // Abfallerzeugernr. 0..n
    @ElementCollection
    private List<AbfallerzeugerNr> abfallerzeugerNummern;

    // hat ZBehoerde // zuständige Bhd. 0..n
    @ElementCollection
    private List<ZustaendigeBehoerde> zustaendigeBehoerden;

    @NotEmpty
    private String name;

    @ManyToOne
    private Referenz vertraulichkeitsgrund;
    @ManyToOne
    private Referenz flusseinzugsgebiet;
    @ManyToOne
    private Referenz naceCode;
    private boolean direkteinleiter;
    private boolean indirekteinleiter;
    @NotNull
    @ManyToOne
    private Referenz betriebsstatus;

    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;

    private String bemerkung;
    private boolean betreiberdatenUebernommen;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentBetriebsstaetteId", referencedColumnName = "id")
    @OrderBy("quelleNr ASC")
    private List<Quelle> quellen;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentBetriebsstaetteId", referencedColumnName = "id")
    @OrderBy("anlageNr ASC")
    private List<Anlage> anlagen;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "betriebsstaetteId", referencedColumnName = "id")
    private List<Berichtsobjekt> berichtsobjekte;

    @OneToOne
    private Komplexpruefung komplexpruefung;

    private boolean pruefungsfehler;
    private boolean pruefungVorhanden;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    private Instant letzteAenderungBetreiber;

    private boolean schreibsperreBetreiber;

    public Betriebsstaette() {
    }

    @PostLoad
    private void sortBerichtsobjekte() {
        // Sortieren der Liste mittels @OrderBy funktioniert nicht, da nach einem
        // abhängigen Attribut ("berichtsart.sortier") sortiert werden müsste.
        // Daher wird die (kurze) Liste der Berichtsobjekte Java-seitig sortiert.
        if (berichtsobjekte != null) {
            Collections.sort(berichtsobjekte);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    public void setRefAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public Referenz getBerichtsjahr() {
        return berichtsjahr;
    }

    public void setBerichtsjahr(Referenz berichtsjahr) {
        this.berichtsjahr = berichtsjahr;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public Behoerde getZustaendigeBehoerde() {
        return zustaendigeBehoerde;
    }

    public void setZustaendigeBehoerde(Behoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerde = zustaendigeBehoerde;
    }

    public String getBetriebsstaetteNr() {
        return betriebsstaetteNr;
    }

    public void setBetriebsstaetteNr(String betriebsstaetteNr) {
        this.betriebsstaetteNr = betriebsstaetteNr;
    }

    public String getAkz() {
        return akz;
    }

    public void setAkz(String akz) {
        this.akz = akz;
    }

    public Betreiber getBetreiber() {
        return betreiber;
    }

    public void setBetreiber(Betreiber betreiber) {
        this.betreiber = betreiber;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<Ansprechpartner> getAnsprechpartner() {
        return ansprechpartner;
    }

    public void setAnsprechpartner(List<Ansprechpartner> ansprechpartner) {
        this.ansprechpartner = ansprechpartner;
    }

    public List<Kommunikationsverbindung> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    public void setKommunikationsverbindungen(List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public Referenz getGemeindekennziffer() {
        return gemeindekennziffer;
    }

    public void setGemeindekennziffer(Referenz gemeindeKennziffer) {
        this.gemeindekennziffer = gemeindeKennziffer;
    }

    public List<EinleiterNr> getEinleiterNummern() {
        return einleiterNummern;
    }

    public void setEinleiterNummern(List<EinleiterNr> einleiterNummern) {
        this.einleiterNummern = einleiterNummern;
    }

    public List<Referenz> getVorschriften() {
        if (vorschriften == null) {
            return Collections.emptyList();
        }
        return vorschriften;
    }

    public void setVorschriften(List<Referenz> vorschriften) {
        this.vorschriften = vorschriften;
    }

    public List<AbfallerzeugerNr> getAbfallerzeugerNummern() {
        return abfallerzeugerNummern;
    }

    public void setAbfallerzeugerNummern(List<AbfallerzeugerNr> abfallerzeugerNummern) {
        this.abfallerzeugerNummern = abfallerzeugerNummern;
    }

    public List<ZustaendigeBehoerde> getZustaendigeBehoerden() {
        return zustaendigeBehoerden;
    }

    public void setZustaendigeBehoerden(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        this.zustaendigeBehoerden = zustaendigeBehoerden;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Referenz getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    public void setVertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
    }

    public Referenz getFlusseinzugsgebiet() {
        return flusseinzugsgebiet;
    }

    public void setFlusseinzugsgebiet(Referenz flusseinzugsgebiet) {
        this.flusseinzugsgebiet = flusseinzugsgebiet;
    }

    public Referenz getNaceCode() {
        return naceCode;
    }

    public void setNaceCode(Referenz naceCode) {
        this.naceCode = naceCode;
    }

    public boolean isDirekteinleiter() {
        return direkteinleiter;
    }

    public void setDirekteinleiter(boolean istDirekteinleiter) {
        this.direkteinleiter = istDirekteinleiter;
    }

    public boolean isIndirekteinleiter() {
        return indirekteinleiter;
    }

    public void setIndirekteinleiter(boolean istIndirekteinleiter) {
        this.indirekteinleiter = istIndirekteinleiter;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDate getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDate getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public void setBetreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
    }

    public List<Quelle> getQuellen() {
        if (quellen == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(quellen);
    }

    public void setQuellen(List<Quelle> quellen) {
        this.quellen = quellen;
    }

    public List<Anlage> getAnlagen() {
        if (anlagen == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(anlagen);
    }

    public void setAnlagen(List<Anlage> anlagen) {
        this.anlagen = anlagen;
    }

    public List<Berichtsobjekt> getBerichtsobjekte() {
        return berichtsobjekte;
    }

    public void setBerichtsobjekte(List<Berichtsobjekt> berichtsobjekte) {
        this.berichtsobjekte = berichtsobjekte;
    }

    public Komplexpruefung getKomplexpruefung() {
        return komplexpruefung;
    }

    public void setKomplexpruefung(Komplexpruefung komplexpruefung) {
        this.komplexpruefung = komplexpruefung;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public boolean isPruefungsfehler() {
        return pruefungsfehler;
    }

    public void setPruefungsfehler(boolean pruefungsfehler) {
        this.pruefungsfehler = pruefungsfehler;
    }

    public boolean isPruefungVorhanden() {
        return pruefungVorhanden;
    }

    public void setPruefungVorhanden(boolean pruefungVorhanden) {
        this.pruefungVorhanden = pruefungVorhanden;
    }

    public Instant getLetzteAenderungBetreiber() {
        return letzteAenderungBetreiber;
    }

    public void setLetzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
    }

    public boolean isSchreibsperreBetreiber() {
        return schreibsperreBetreiber;
    }

    public void setSchreibsperreBetreiber(boolean schreibsperreBetreiber) {
        this.schreibsperreBetreiber = schreibsperreBetreiber;
    }

    /**
     * Prüft erst die id und wenn die null ist den fachlichen Key
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Betriebsstaette that = (Betriebsstaette) o;

        return id != null ? id.equals(that.id) : this.hasSameKey(that);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isNew() {
        return id == null;
    }

    public boolean hasSameKey(Betriebsstaette other) {
        return berichtsjahr.equals(other.berichtsjahr) && land.equals(other.land) && betriebsstaetteNr.equals(
                other.betriebsstaetteNr);
    }

    public boolean hasSameLocalIdOrNull(Betriebsstaette other) {
        return other.localId == null || Objects.equals(localId, other.localId);
    }

    public boolean hatBetreiber() {
        return betreiber != null;
    }

    public String betreiberNummerOrNull() {
        return hatBetreiber() ? betreiber.getBetreiberNummer() : null;
    }

    public void removeAllAnsprechpartner() {
        this.ansprechpartner.clear();
    }

    public Gueltigkeitsjahr getGueltigkeitsjahrFromBerichtsjahr() {
        return Gueltigkeitsjahr.of(berichtsjahr.getSchluessel());
    }

    public Optional<Anlage> sucheAnlage(Predicate<Anlage> filterPredicate) {
        return getAnlagen().stream().filter(filterPredicate).findAny();
    }

    public Optional<Anlagenteil> sucheAnlagenteil(Predicate<Anlagenteil> filterPredicate) {
        return getAnlagen().stream().flatMap(a -> a.getAnlagenteile().stream()).filter(filterPredicate).findAny();
    }

    public boolean hatEURegBst() {
        return getAnlagen().stream()
                           .flatMap(Anlage::alleVorschriften)
                           .map(Vorschrift::getArt)
                           .map(Referenz::getSchluessel)
                           .filter(VorschriftSchluessel::schluesselExist)
                           .map(VorschriftSchluessel::ofSchluessel)
                           .anyMatch(VorschriftSchluessel::brauchtBetriebsstaetteBericht);
    }

    public Stream<Vorschrift> getPrtrVorschriften() {
        return getAnlagen().stream().flatMap(Anlage::alleVorschriften).filter(Vorschrift::isPRTR);
    }

    public Referenz getPrtrHaupttaetigkeit() {
        return getPrtrVorschriften().map(Vorschrift::getHaupttaetigkeit).findFirst().orElse(null);
    }

    public List<Referenz> getPrtrTaetigkeitenOhneHaupttaetigkeit() {
        return getPrtrVorschriften().map(Vorschrift::getTaetigkeitenOhneHaupttaetigkeit)
                                    .flatMap(Collection::stream)
                                    .distinct()
                                    .toList();
    }

    public boolean hasDoppelteVorschriften() {
        for ( Anlage anlage : getAnlagen() ){
            if(anlage.hasDoppelteVorschriften()){
                return true;
            }
        }
        return false;
    }
}
