package de.wps.bube.basis.benutzerverwaltung.domain.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.ObjectFactory;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerException;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerImportException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.event.BenutzerImportTriggeredEvent;
import de.wps.bube.basis.benutzerverwaltung.xml.BenutzerXMLMapper;
import de.wps.bube.basis.benutzerverwaltung.xml.dto.BenutzerListeXDto;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Service
public class BenutzerverwaltungImportExportService {
    private final BenutzerverwaltungService benutzerverwaltungService;
    private final StammdatenService stammdatenService;
    private final AktiverBenutzer aktiverBenutzer;
    private final KeycloakService keycloakService;
    private final ApplicationEventPublisher eventPublisher;
    private final DateiService dateiService;
    private final XMLService xmlService;
    private final BenutzerXMLMapper benutzerXMLMapper;

    public BenutzerverwaltungImportExportService(BenutzerverwaltungService benutzerverwaltungService,
            StammdatenService stammdatenService, AktiverBenutzer aktiverBenutzer,
            KeycloakService keycloakService, ApplicationEventPublisher eventPublisher, DateiService dateiService,
            XMLService xmlService, BenutzerXMLMapper benutzerXMLMapper) {
        this.benutzerverwaltungService = benutzerverwaltungService;
        this.stammdatenService = stammdatenService;
        this.aktiverBenutzer = aktiverBenutzer;
        this.keycloakService = keycloakService;
        this.eventPublisher = eventPublisher;
        this.dateiService = dateiService;
        this.xmlService = xmlService;
        this.benutzerXMLMapper = benutzerXMLMapper;
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    public void triggerImport(String username) {
        eventPublisher.publishEvent(new BenutzerImportTriggeredEvent(username));
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    public void dateiSpeichern(MultipartFile file, String username) {
        dateiService.dateiSpeichern(file, username, DateiScope.BENUTZER);
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    public String existierendeDatei(String username) {
        return dateiService.existingDatei(username, DateiScope.BENUTZER);
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    public void dateiLoeschen(String username) {
        dateiService.loescheDatei(username, DateiScope.BENUTZER);
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    @Transactional(readOnly = true)
    public List<Validierungshinweis> validiere(String username) {

        List<Validierungshinweis> hinweise = new ArrayList<>();

        try {

            // Erstmal des XML-Schema Validieren
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.BENUTZER);
            xmlService.validate(inputStream, BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
            if (!hinweise.isEmpty()) {
                return hinweise;
            }

            Set<String> allBetreiberBenutzer = benutzerverwaltungService.listAllBetreiberBenutzer()
                                                                        .map(Benutzer::getBenutzername)
                                                                        .collect(Collectors.toSet());
            Set<String> allUsers = allExistingUsernames();
            Set<String> betriebsstaettenNummern = Set.copyOf(stammdatenService.allBetriebsstaettenNrForCurrentUser());
            Set<String> usernames = new HashSet<>();

            inputStream = dateiService.getDateiStream(username, DateiScope.BENUTZER);
            xmlService.read(inputStream, BenutzerType.class, benutzerXMLMapper, BenutzerListeXDto.XML_ROOT_ELEMENT_NAME,
                    null, benutzer -> {
                        hinweise.addAll(
                                benutzer.entity.validiereFuerImport(allBetreiberBenutzer, allUsers,
                                        betriebsstaettenNummern,
                                        usernames, aktiverBenutzer.getLand()));
                        usernames.add(benutzer.entity.getBenutzername().toLowerCase());
                    });
        } catch (XMLImportException e) {
            hinweise.add(Validierungshinweis.of(e.getLocalizedMessage(), Validierungshinweis.Severity.FEHLER));
        } catch (Exception e) {
            throw new BenutzerException("Es ist ein Fehler bei der Validierung aufgetreten");
        }

        return hinweise;
    }

    /**
     * Wird vom Benutzer-Importieren-Job Aufgerufen
     */
    @Secured(Rollen.BENUTZERVERWALTUNG_WRITE)
    public Benutzer importiereBenutzer(Benutzer benutzer, Set<String> betriebsstaetten,
            Set<String> allBetreiberBenutzer, JobProtokoll jobProtokoll) {
        if (!benutzer.allBstBerechtigungenContainedInIgnoreCase(betriebsstaetten)) {
            throw new BenutzerImportException(
                    "Eine oder mehrere Betriebsstätten von Benutzer " + benutzer.getBenutzername() + " existieren nicht mehr");
        }
        benutzer.setZugewieseneRollen(List.of(Rolle.BETREIBER));

        Benutzer imported;
        if (!allBetreiberBenutzer.contains(benutzer.getBenutzername().toLowerCase())) {
            imported = benutzerverwaltungService.createBenutzer(benutzer);
            jobProtokoll.addEintrag("Benutzer " + imported.getBenutzername() + " erfolgreich angelegt");
        } else {
            benutzer.setId(
                    benutzerverwaltungService.loadBenutzerByName(benutzer.getBenutzername().toLowerCase()).getId());
            imported = benutzerverwaltungService.updateBenutzer(benutzer);
            jobProtokoll.addEintrag("Benutzer " + imported.getBenutzername() + " erfolgreich aktualisiert");
        }
        return imported;
    }

    Set<String> allExistingUsernames() {
        return keycloakService.listAllUsers().map(Benutzer::getBenutzername).collect(Collectors.toSet());
    }

    @Secured(Rollen.BENUTZERVERWALTUNG_READ)
    public void exportBenutzer(ByteArrayOutputStream outputStream, List<String> benutzerIds) {
        var objectFactory = new ObjectFactory();
        var benutzerStream = benutzerIds.stream()
                                        .map(benutzerverwaltungService::loadBenutzerById);
        var writer = xmlService.createWriter(BenutzerType.class, BenutzerListeXDto.QUALIFIED_NAME,
                benutzerXMLMapper::toXDto, objectFactory::createBenutzer);

        writer.write(benutzerStream, outputStream);
    }
}
