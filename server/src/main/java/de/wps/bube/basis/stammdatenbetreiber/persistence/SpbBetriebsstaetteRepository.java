package de.wps.bube.basis.stammdatenbetreiber.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;

public interface SpbBetriebsstaetteRepository
        extends JpaRepository<SpbBetriebsstaette, Long>, QuerydslPredicateProjectionRepository<SpbBetriebsstaette> {

    boolean existsByBetriebsstaetteId(Long betriebsstaetteId);

    Optional<SpbBetriebsstaette> findOneByBetriebsstaetteId(Long betriebsstaetteId);

    Optional<SpbBetriebsstaette> findOneBySpbBetreiberId(Long spbBetreiberId);

}
