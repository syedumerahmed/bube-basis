package de.wps.bube.basis.sicherheit.config;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.context.annotation.RequestScope;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.service.AuthenticationService;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@KeycloakConfiguration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        SimpleAuthorityMapper authorityMapper = new SimpleAuthorityMapper();
        authorityMapper.setPrefix(Rollen.ROLE_PREFIX);
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(authorityMapper);
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public static KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    public Keycloak keycloakClient(@Value("${keycloak.auth-server-url}") String serverUrl,
            @Value("${service.keycloak.config.username}") String username,
            @Value("${service.keycloak.config.password}") String password,
            @Value("${service.keycloak.config.connection-pool-size}") int connectionPoolSize) {
        return KeycloakBuilder.builder()
                              .serverUrl(serverUrl)
                              .realm("master")
                              .username(username)
                              .password(password)
                              .clientId("admin-cli")
                              .resteasyClient(
                                      new ResteasyClientBuilder().connectionPoolSize(connectionPoolSize)
                                                                 .build())
                              .build();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        super.configure(http);

        http.authorizeRequests()
            .antMatchers("/env")
            .permitAll()
            .antMatchers("/admin/**")
            .hasAnyRole(Rolle.ADMIN.toKeycloakRepresentation(), Rolle.GESAMTADMIN.toKeycloakRepresentation())
            .antMatchers("/anlagen/**", "/anlagenteil/**", "/betreiber/**", "/betriebsstaetten/**", "/quellen/**")
            .hasAnyRole(Rolle.LAND_READ.toKeycloakRepresentation(), Rolle.BEHOERDE_READ.toKeycloakRepresentation())
            .antMatchers("/eureg/**", "/euregbetriebsstaetten/**")
            .hasAnyRole(Rolle.LAND_READ.toKeycloakRepresentation(), Rolle.BEHOERDE_READ.toKeycloakRepresentation())
            .antMatchers("/koordinaten/**")
            .hasRole(Rolle.GESAMTADMIN.toKeycloakRepresentation())
            .antMatchers("/regel/**")
            .hasRole(Rolle.LAND_READ.toKeycloakRepresentation())
            .antMatchers("/spbanlagen/**", "/spbanlagenteil/**", "/spbbetreiber/**", "/spbbetriebsstaetten/**",
                    "/spbquellen/**")
            .hasAnyRole(Rolle.BETREIBER.toKeycloakRepresentation(), Rolle.BEHOERDE_READ.toKeycloakRepresentation(),
                    Rolle.LAND_READ.toKeycloakRepresentation())
            .anyRequest()
            .authenticated();
        http.csrf().disable();
    }

    @Bean
    @RequestScope
    public AktiverBenutzer benutzer(AuthenticationService authenticationService) {
        return authenticationService.getCurrentBenutzer();
    }

    @Bean
    @RequestScope
    public StammdatenBerechtigungsContext stammdatenBerechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        return new StammdatenBerechtigungsContext(aktiverBenutzer);
    }

    @Bean
    @RequestScope
    public ReferenzenBerechtigungsContext referenzenBerechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        return new ReferenzenBerechtigungsContext(aktiverBenutzer);
    }
}
