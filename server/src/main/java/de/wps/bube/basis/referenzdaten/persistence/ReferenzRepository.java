package de.wps.bube.basis.referenzdaten.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public interface ReferenzRepository extends JpaRepository<Referenz, Long> {

    Stream<Referenz> findByReferenzlisteOrderBySortierAsc(Referenzliste referenzliste);

    Stream<Referenz> findByReferenzlisteAndLandInOrderBySortierAsc(Referenzliste referenzliste, List<Land> landList);

    @Query(value = "select exists(select 1 from referenz where (:id is null or id != CAST(CAST(:id AS TEXT) AS BIGINT))" +
            "                and land = :land" +
            "                and referenzliste = :liste" +
            "                and schluessel = :schluessel" +
            "                and (gueltig_von between :von and :bis" +
            "                   or :von between gueltig_von and gueltig_bis))", nativeQuery = true)
    boolean functionalKeyDuplicate(Long id, String liste, String land, String schluessel, int von, int bis);

    @Query("from Referenz where referenzliste = :referenzliste" +
            "                and land = :land" +
            "                and schluessel = :schluessel" +
            "                and :jahr between gueltigVon and gueltigBis")
    Optional<Referenz> findByKey(Referenzliste referenzliste, Land land, String schluessel, Gueltigkeitsjahr jahr);

    Optional<Referenz> findFirstByReferenzlisteOrderBySortierDesc(Referenzliste referenzliste);

    @Query("from Referenz where referenzliste = :referenzliste and land = :land and :jahr between gueltigVon and gueltigBis order by sortier")
    Stream<Referenz> findForLand(Referenzliste referenzliste, Land land, Gueltigkeitsjahr jahr);

    @Query("from Referenz where referenzliste = :referenzliste and :jahr between gueltigVon and gueltigBis order by sortier")
    Stream<Referenz> findAll(Referenzliste referenzliste, Gueltigkeitsjahr jahr);

    Optional<Referenz> findByReferenznummer(String referenznummer);

    @Query("from Referenz where referenzliste = 'RJAHR' and schluessel = :schluessel")
    Optional<Referenz> findJahrBySchluessel(String schluessel);

    Optional<Referenz> findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc(Referenzliste referenzliste,
            Land land, String schluessel);
}
