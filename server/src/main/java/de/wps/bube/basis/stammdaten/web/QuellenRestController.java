package de.wps.bube.basis.stammdaten.web;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.QuellenApi;
import de.wps.bube.basis.base.web.openapi.model.QuelleDto;
import de.wps.bube.basis.base.web.openapi.model.QuelleListItemDto;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class QuellenRestController implements QuellenApi {

    private final StammdatenService stammdatenService;
    private final QuelleMapper mapper;

    public QuellenRestController(StammdatenService stammdatenService, QuelleMapper mapper) {
        this.stammdatenService = stammdatenService;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<QuelleListItemDto>> readAllQuellenFromBetriebsstaette(Long betriebsstaetteId) {
        return ResponseEntity.ok(stammdatenService.loadAllQuellenByBetriebsstaettenId(betriebsstaetteId)
                                                  .stream()
                                                  .map(this::mapQuelleListItemDto)
                                                  .collect(toList()));
    }

    private QuelleListItemDto mapQuelleListItemDto(Quelle quelle) {
        QuelleListItemDto dto = new QuelleListItemDto();
        dto.setNummer(quelle.getQuelleNr());
        dto.setId(quelle.getId());
        dto.setName(quelle.getName());
        dto.setBetreiberdatenUebernommen(quelle.isBetreiberdatenUebernommen());
        dto.setLetzteAenderungBetreiber(
                quelle.getLetzteAenderungBetreiber() == null ? null : quelle.getLetzteAenderungBetreiber().toString());
        return dto;
    }

    @Override
    public ResponseEntity<List<QuelleListItemDto>> readAllQuellenFromAnlage(Long anlageId, Long betriebsstaetteId) {
        return ResponseEntity.ok(stammdatenService.loadAllQuellenByAnlagenId(anlageId, betriebsstaetteId)
                                                  .stream()
                                                  .map(this::mapQuelleListItemDto)
                                                  .collect(toList()));
    }

    @Override
    public ResponseEntity<QuelleDto> createQuelle(QuelleDto quelleDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.createQuelle(mapper.fromDto(quelleDto))));
    }

    @Override
    public ResponseEntity<QuelleDto> readQuelle(Long betriebsstaetteId, String quellenNummer) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.loadQuelleByQuelleNummer(betriebsstaetteId, quellenNummer)));
    }

    @Override
    public ResponseEntity<QuelleDto> readQuelleOfAnlage(Long anlageId, String quellenNummer) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.loadQuelleOfAnlage(anlageId, quellenNummer)));
    }

    @Override
    public ResponseEntity<QuelleDto> updateQuelle(QuelleDto quelleDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.updateQuelle(mapper.fromDto(quelleDto))));
    }

    @Override
    public ResponseEntity<Void> deleteQuelle(Long betriebsstaetteId, Long id) {
        stammdatenService.deleteQuelle(betriebsstaetteId, id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleQuellen(Long betriebsstaetteId, List<Long> ids) {
        stammdatenService.deleteMultipleQuellen(betriebsstaetteId, ids);
        return ResponseEntity.ok().build();
    }
}
