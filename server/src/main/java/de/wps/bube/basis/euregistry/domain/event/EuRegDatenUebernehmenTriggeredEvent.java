package de.wps.bube.basis.euregistry.domain.event;

import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public record EuRegDatenUebernehmenTriggeredEvent(Referenz zieljahr, List<Long> betriebsstaettenIds) {
    public static final String EVENT_NAME = "EuRegDaten übernehmen";
}
