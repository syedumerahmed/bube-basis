package de.wps.bube.basis.datei.domain;

public class DateiException extends RuntimeException {
    public DateiException(Exception e) {
        super("Fehler beim Laden der Datei.", e);
    }
}
