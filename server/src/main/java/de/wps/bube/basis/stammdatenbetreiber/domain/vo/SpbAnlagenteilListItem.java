package de.wps.bube.basis.stammdatenbetreiber.domain.vo;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class SpbAnlagenteilListItem {

    private final Long spbAnlagenteilId;
    private final String anlagenteilNr;
    private final String name;
    private final Referenz betriebsstatus;
    private final boolean anlagenteilVorhanden;

    public SpbAnlagenteilListItem(Long spbAnlagenteilId, String anlagenteilNr, String name,
            Referenz betriebsstatus, boolean anlagenteilVorhanden) {
        this.spbAnlagenteilId = spbAnlagenteilId;
        this.anlagenteilNr = anlagenteilNr;
        this.name = name;
        this.betriebsstatus = betriebsstatus;
        this.anlagenteilVorhanden = anlagenteilVorhanden;
    }

    public Long getSpbAnlagenteilId() {
        return spbAnlagenteilId;
    }

    public String getAnlagenteilNr() {
        return anlagenteilNr;
    }

    public String getName() {
        return name;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public boolean isAnlagenteilVorhanden() {
        return anlagenteilVorhanden;
    }

    public boolean isVeraendert() {
        return spbAnlagenteilId != null && anlagenteilVorhanden;
    }

    public boolean isNeu() {
        return spbAnlagenteilId != null && !anlagenteilVorhanden;
    }
}
