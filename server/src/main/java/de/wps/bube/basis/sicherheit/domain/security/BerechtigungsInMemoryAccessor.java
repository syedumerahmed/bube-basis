package de.wps.bube.basis.sicherheit.domain.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;

/**
 * Interface zum Zugriff auf die konkreten Datenfelder einzelner Entities im Speicher. Siehe dazu:
 * <a href="https://wps-bube.atlassian.net/wiki/spaces/BB/pages/140115969/Sicherheitskonzept#Absicherung-auf-Serverseite">
 * Sicherheitskonzept
 * </a>
 * <p>
 * Wird vom {@link StammdatenBerechtigungsContext} genutzt, um Datenberechtigungen von Entitäten zu überprüfen.
 */
public interface BerechtigungsInMemoryAccessor {

    Land getLand();

    Set<String> getBehoerden();

    Set<String> getAkzSet();

    Set<String> getVerwaltungsgebiete();

    Set<String> getBetriebsstaetten();
}
