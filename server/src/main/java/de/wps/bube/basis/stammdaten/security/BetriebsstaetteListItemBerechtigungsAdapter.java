package de.wps.bube.basis.stammdaten.security;

import java.util.Set;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;

public class BetriebsstaetteListItemBerechtigungsAdapter implements BerechtigungsInMemoryAccessor {
    private final BetriebsstaetteListItemView betriebsstaette;

    public BetriebsstaetteListItemBerechtigungsAdapter(BetriebsstaetteListItemView betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
    }

    @Override
    public Land getLand() {
        return betriebsstaette.land;
    }

    @Override
    public Set<String> getBehoerden() {
        return Set.of(betriebsstaette.zustaendigeBehoerde.getSchluessel());
    }

    @Override
    public Set<String> getAkzSet() {
        return betriebsstaette.akz != null ? Set.of(betriebsstaette.akz) : null;
    }

    @Override
    public Set<String> getVerwaltungsgebiete() {
        return Set.of(betriebsstaette.gemeindekennziffer.getSchluessel());
    }

    @Override
    public Set<String> getBetriebsstaetten() {
        return Set.of(betriebsstaette.betriebsstaetteNr);
    }
}
