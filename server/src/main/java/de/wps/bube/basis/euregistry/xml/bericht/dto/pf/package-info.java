@javax.xml.bind.annotation.XmlSchema(
        namespace = SchemaSet.SCHEMA_PF,
        elementFormDefault = XmlNsForm.QUALIFIED)
package de.wps.bube.basis.euregistry.xml.bericht.dto.pf;

import javax.xml.bind.annotation.XmlNsForm;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
