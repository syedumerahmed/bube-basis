package de.wps.bube.basis.komplexpruefung.rules;

/**
 * Diese Klasse dient zum Signalisieren von Fehlerzuständen, die in den von den *BeanMapping-Klassen bereitgestellten
 * Hilfsmethoden auftreten.
 * Durch die Verwendung dieser spezifischen Exception-Klasse kann das Zurücksetzen einer geschachtelten Transaktion
 * auf den Rollback-Modus kontrolliert unterbunden werden.
 */
public class BeanMappingException extends RuntimeException {

    public BeanMappingException(String message) {
        super(message);
    }

    public BeanMappingException(String message, Throwable cause) {
        super(message, cause);
    }

}
