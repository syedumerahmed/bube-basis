package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Component;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Thema;

@Component
public class KeycloakServiceMapper {

    private static final String PHONE_ATTRIBUTE = "phone";

    public UserRepresentation mapBenutzerToUser(Benutzer b) {

        UserRepresentation user = new UserRepresentation();
        user.setAttributes(mapDatenberechtigung(b.getDatenberechtigung()));

        user.setId(b.getId());
        user.setUsername(b.getBenutzername());
        user.setLastName(b.getNachname());
        user.setFirstName(b.getVorname());
        user.setEmail(b.getEmail());
        user.setEnabled(b.isAktiv());
        user.setAttributes(mapDatenberechtigung(b.getDatenberechtigung()));
        user.setRealmRoles(
                b.getZugewieseneRollen().stream().map(Rolle::toKeycloakRepresentation).collect(Collectors.toList()));
        if (b.getTelefonnummer() != null) {
            List<String> liste = new ArrayList<>();
            liste.add(b.getTelefonnummer());

            user.getAttributes().put(PHONE_ATTRIBUTE, liste);
        }

        return user;
    }

    public Benutzer mapUserToBenutzer(UserResource user) {
        var representation = user.toRepresentation();
        Benutzer b = new Benutzer(representation.getUsername());
        b.setId(representation.getId());
        b.setNachname(representation.getLastName());
        b.setVorname(representation.getFirstName());
        b.setEmail(representation.getEmail());
        b.setAktiv(representation.isEnabled());
        b.setTelefonnummer(getAttribute(representation, PHONE_ATTRIBUTE));

        b.setDatenberechtigung(mapDatenberechtigung(representation));
        b.setZugewieseneRollen(mapRollen(user.roles().realmLevel().listAll()));

        boolean needsTempPassword = representation.getEmail() == null
                                    && representation.getRequiredActions() != null
                                    && representation.getRequiredActions().contains("UPDATE_PASSWORD");
        b.setNeedsTempPassword(needsTempPassword);

        return b;
    }

    private List<Rolle> mapRollen(List<RoleRepresentation> roleRepresentations) {
        return roleRepresentations.stream()
                                  .map(RoleRepresentation::getName)
                                  .filter(Rolle::isValid)
                                  .map(Rolle::of)
                                  .collect(Collectors.toList());
    }

    private String getAttribute(UserRepresentation user, String attributeName) {
        String attribute = null;
        List<String> list = user.getAttributes().get(attributeName);
        if (list != null && !list.isEmpty()) {
            attribute = list.get(0);
        }
        return attribute;
    }

    private Set<String> getAttributeSet(UserRepresentation user, String attributeName) {
        List<String> list = user.getAttributes().get(attributeName);
        if (list == null) {
            return Collections.emptySet();
        }
        return Set.copyOf(list);
    }

    private Datenberechtigung mapDatenberechtigung(UserRepresentation user) {
        return Datenberechtigung.DatenberechtigungBuilder
                .land(Land.of(getAttribute(user, Datenberechtigung.LAND)))
                .behoerden(getAttributeSet(user, Datenberechtigung.BEHOERDEN))
                .akz(getAttributeSet(user, Datenberechtigung.AKZ))
                .verwaltungsgebiete(getAttributeSet(user, Datenberechtigung.VERWALTUNGSGEBIETE))
                .betriebsstaetten(getAttributeSet(user, Datenberechtigung.BETRIEBSSTAETTEN))
                .fachmodule(getAttributeSet(user, Datenberechtigung.FACHMODULE)
                        .stream()
                        .map(Fachmodul::of)
                        .collect(toSet()))
                .themen(getAttributeSet(user, Datenberechtigung.THEMEN)
                        .stream()
                        .map(Thema::of)
                        .collect(toSet()))
                .build();
    }

    private Map<String, List<String>> mapDatenberechtigung(Datenberechtigung d) {
        Map<String, List<String>> attributes = new HashMap<>();
        attributes.put(Datenberechtigung.LAND, List.of(d.land.getNr()));

        attributes.put(Datenberechtigung.BEHOERDEN, List.copyOf(d.behoerden));
        attributes.put(Datenberechtigung.AKZ, List.copyOf(d.akz));
        attributes.put(Datenberechtigung.VERWALTUNGSGEBIETE, List.copyOf(d.verwaltungsgebiete));
        attributes.put(Datenberechtigung.BETRIEBSSTAETTEN, List.copyOf(d.betriebsstaetten));

        final List<String> fachmoduleStrings = new ArrayList<>();
        d.fachmodule.forEach(fachmodul -> fachmoduleStrings.add(fachmodul.getBezeichner()));
        attributes.put(Datenberechtigung.FACHMODULE, fachmoduleStrings);

        final List<String> themenStrings = new ArrayList<>();
        d.themen.forEach(thema -> themenStrings.add(thema.getBezeichner()));
        attributes.put(Datenberechtigung.THEMEN, themenStrings);

        return attributes;
    }

}
