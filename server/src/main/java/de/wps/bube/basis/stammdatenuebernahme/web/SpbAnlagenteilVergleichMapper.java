package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.SpbAnlagenteilVergleichDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlagenteilVergleich;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbAnlagenteilVergleichMapper {

    @Mapping(target = "allgemein", source = ".")
    public abstract SpbAnlagenteilVergleichDto toDto(SpbAnlagenteilVergleich spbAnlagenteilVergleich);
}
