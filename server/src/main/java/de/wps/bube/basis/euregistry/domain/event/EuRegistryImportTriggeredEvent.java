package de.wps.bube.basis.euregistry.domain.event;

public record EuRegistryImportTriggeredEvent(String berichtsjahr) {

}
