package de.wps.bube.basis.stammdaten.domain;

import de.wps.bube.basis.base.persistence.DuplicateKeyException;

public class BetriebsstaetteDuplicateKeyException extends DuplicateKeyException {
    public BetriebsstaetteDuplicateKeyException(final String berichtsjahr, final String betriebsstaettenNummer) {
        super(String.format("Eine Betriebsstaette mit der Nummer '%s' existiert bereits im Berichtsjahr %s!",
                betriebsstaettenNummer, berichtsjahr));
    }
}
