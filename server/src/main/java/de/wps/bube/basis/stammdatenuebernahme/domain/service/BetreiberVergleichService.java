package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetreiberVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

@Service
public class BetreiberVergleichService {
    private final CommonVergleichService vergleichService;

    public BetreiberVergleichService(
            CommonVergleichService vergleichService) {
        this.vergleichService = vergleichService;
    }

    public SpbBetreiberVergleich vergleiche(Betreiber spl, SpbBetreiber spb) {
        SpbBetreiberVergleich vergleich = new SpbBetreiberVergleich();

        vergleich.setId(spl == null ? null : spl.getId());
        vergleich.setSpbId(spb == null ? null : spb.getId());
        vergleich.setName(
                new StringVector(spl == null ? null : spl.getName(), spb == null ? null : spb.getName()));

        if (spl == null || !spl.isBetreiberdatenUebernommen()) {
            vergleich.setAdresse(vergleichService.adressVergleich(spl == null ? null : spl.getAdresse(),
                    spb == null ? null : spb.getAdresse()));
        }

        if (spb == null) {
            vergleich.setHasChanges(false);
        } else {
            vergleich.setHasChanges(spl == null || (!spl.isBetreiberdatenUebernommen() &&
                    (!vergleich.getName().isEqual() || !vergleichService.adresseEqual(vergleich.getAdresse()))));
        }
        return vergleich;
    }
}
