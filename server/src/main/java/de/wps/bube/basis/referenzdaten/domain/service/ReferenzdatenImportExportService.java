package de.wps.bube.basis.referenzdaten.domain.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.xml.ReferenzdatenImportException;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.xml.StammdatenImportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.ImportException;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.event.ReferenzdatenImportTriggeredEvent;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenRead;
import de.wps.bube.basis.referenzdaten.security.SecuredReferenzlistenWrite;
import de.wps.bube.basis.referenzdaten.xml.ReferenzXMLMapper;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ObjectFactory;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;

import static java.lang.String.format;

@Service
@Transactional
public class ReferenzdatenImportExportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReferenzdatenImportExportService.class);

    private final ReferenzenService referenzenService;
    private final XMLService xmlService;
    private final ReferenzXMLMapper referenzXMLMapper;
    private final ApplicationEventPublisher eventPublisher;
    private final DateiService dateiService;

    public ReferenzdatenImportExportService(ReferenzenService referenzenService, XMLService xmlService,
            ReferenzXMLMapper referenzXMLMapper, ApplicationEventPublisher eventPublisher, DateiService dateiService) {
        this.referenzenService = referenzenService;
        this.xmlService = xmlService;
        this.referenzXMLMapper = referenzXMLMapper;
        this.eventPublisher = eventPublisher;
        this.dateiService = dateiService;
    }

    @SecuredReferenzlistenRead
    @Transactional(readOnly = true)
    public void exportReferenzdaten(ByteArrayOutputStream outputStream, List<String> referenzList) {

        // Die Daten werden so exportiert wie sie auch im Editor zu sehen sind.
        Stream<Referenz> stream = referenzList.stream()
                                              .map(Referenzliste::valueOf)
                                              .flatMap(referenzenService::streamReferenzlisteForEdit);

        var objectFactory = new ObjectFactory();

        var writer = xmlService.createWriter(ReferenzType.class, ReferenzlisteXDto.QUALIFIED_NAME,
                referenzXMLMapper::toXDto, objectFactory::createReferenz);



        writer.write(stream, outputStream);
    }

    @SecuredReferenzlistenWrite
    @Transactional(readOnly = true)
    public String existierendeDatei(String username) {
        return dateiService.existingDatei(username, DateiScope.REFERENZDATEN);
    }

    @SecuredReferenzlistenWrite
    public void dateiSpeichern(MultipartFile file, String username) {
        dateiService.dateiSpeichern(file, username, DateiScope.REFERENZDATEN);
    }

    @SecuredReferenzlistenWrite
    @Transactional(readOnly = true)
    public List<Validierungshinweis> validiere(String username) {
        List<Validierungshinweis> hinweise = new ArrayList<>();

        try {
            // Das XML-Schema Validieren
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.REFERENZDATEN);
            xmlService.validate(inputStream, ReferenzlisteXDto.XSD_SCHEMA_URL, hinweise);

        } catch (XMLImportException e) {
            hinweise.add(Validierungshinweis.of(e.getLocalizedMessage(), Validierungshinweis.Severity.FEHLER));
        } catch (Exception e) {
            throw new ImportException("Es ist ein Fehler bei der Validierung aufgetreten", e);
        }

        return hinweise;
    }

    /**
     * Wird vom Referenzen-Importieren-Job Aufgerufen
     */
    @SecuredReferenzlistenWrite
    public void readXml(String username, Consumer<EntityWithError<Referenz>> consumer) {
        InputStream inputStream = dateiService.getDateiStream(username, DateiScope.REFERENZDATEN);
        xmlService.read(inputStream, ReferenzType.class, referenzXMLMapper, ReferenzlisteXDto.XML_ROOT_ELEMENT_NAME,
                null, consumer);
    }

    /**
     * Wird vom Referenzen-Importieren-Job Aufgerufen
     */
    @SecuredReferenzlistenWrite
    public void importiereReferenz(Referenz referenz, JobProtokoll jobProtokoll) {
        // Referenz anlegen oder updaten
        if (!validiereImport(referenz, jobProtokoll)) {
            throw new ReferenzdatenImportException(referenz.getSchluessel(), referenz.getReferenzliste().name());
        }

        if (referenz.getReferenznummer() == null) {
            referenzenService.createReferenz(referenz);
            jobProtokoll.addEintrag(referenz + ". Fehlerfrei");
        } else {
            Optional<Referenz> existingReferenz = referenzenService.findByReferenznummer(referenz.getReferenznummer());

            if (existingReferenz.isPresent()) {
                referenz.setId(existingReferenz.get().getId());
                referenzenService.updateReferenz(referenz);
                jobProtokoll.addEintrag(referenz + ". Aktualisiert");
            } else {
                referenzenService.createReferenz(referenz);
                jobProtokoll.addEintrag(referenz + ". Fehlerfrei");
            }

        }
    }

    private boolean validiereImport(Referenz referenz, JobProtokoll jobProtokoll) {
        var erfolg = true;

        if (referenz.getReferenzliste().toString().equals("RJAHR") && !Pattern.matches( "[0-9]*", referenz.getSchluessel())) {
            jobProtokoll.addEintrag(
                    format("Jahresreferenzen sollten nur numerische Zeichen enthalten. %s kann daher nicht importiert werden", referenz.getSchluessel()));

            erfolg = false;
        }

        return erfolg;
    }


    @SecuredReferenzlistenWrite
    public void triggerImport(String username) {
        eventPublisher.publishEvent(new ReferenzdatenImportTriggeredEvent(username));
    }

    @SecuredReferenzlistenWrite
    public void dateiLoeschen(String username) {
        dateiService.loescheDatei(username, DateiScope.REFERENZDATEN);
    }
}
