package de.wps.bube.basis.stammdatenuebernahme.domain.vo;

public class AnlagenteilUebernahme {
    private Long id;
    private Long spbId;
    private Boolean leistungen;
    private Boolean name;
    private Boolean anlagenteilNr;
    private Boolean geoPunkt;
    private Boolean betriebsstatus;
    private Boolean betriebsstatusSeit;
    private Boolean inbetriebnahme;

    public AnlagenteilUebernahme() {
    }

    public AnlagenteilUebernahme(Long id, Long spbId, Boolean leistungen, Boolean name, Boolean anlagenteilNr,
            Boolean geoPunkt, Boolean betriebsstatus, Boolean betriebsstatusSeit, Boolean inbetriebnahme) {
        this.id = id;
        this.spbId = spbId;
        this.leistungen = leistungen;
        this.name = name;
        this.anlagenteilNr = anlagenteilNr;
        this.geoPunkt = geoPunkt;
        this.betriebsstatus = betriebsstatus;
        this.betriebsstatusSeit = betriebsstatusSeit;
        this.inbetriebnahme = inbetriebnahme;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public Boolean getLeistungen() {
        return leistungen;
    }

    public void setLeistungen(Boolean leistungen) {
        this.leistungen = leistungen;
    }

    public Boolean getName() {
        return name;
    }

    public void setName(Boolean name) {
        this.name = name;
    }

    public Boolean getAnlagenteilNr() {
        return anlagenteilNr;
    }

    public void setAnlagenteilNr(Boolean anlagenteilNr) {
        this.anlagenteilNr = anlagenteilNr;
    }

    public Boolean getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(Boolean geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public Boolean getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Boolean betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public Boolean getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(Boolean betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public Boolean getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(Boolean inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }
}
