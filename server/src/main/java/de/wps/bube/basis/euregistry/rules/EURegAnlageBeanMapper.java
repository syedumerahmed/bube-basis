package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.rules.bean.EURegAnlageBean;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.rules.AnlageBeanMapper;
import de.wps.bube.basis.stammdaten.rules.AnlagenteilBeanMapper;
import de.wps.bube.basis.stammdaten.rules.GeoPunktBeanMapper;
import de.wps.bube.basis.stammdaten.rules.QuelleBeanMapper;
import de.wps.bube.basis.stammdaten.rules.ZustaendigeBehoerdeBeanMapper;

@Mapper(uses = { AnlageBeanMapper.class, AnlagenteilBeanMapper.class, BerichtsdatenBeanMapper.class,
        ReferenzBeanMapper.class, ZustaendigeBehoerdeBeanMapper.class, QuelleBeanMapper.class,
        GeoPunktBeanMapper.class })
public abstract class EURegAnlageBeanMapper extends AbstractEURegAnlageBeanMapper<EURegAnlageBean> {

    @Mapping(target = "anlage", ignore = true)
    @Mapping(target = "anlagenteil", ignore = true)
    @Mapping(target = "euRegBetriebsstaette", ignore = true)
    public abstract EURegAnlageBean toBean(EURegAnlage euRegAnlage, BeanMappingContext beanMappingContext,
            EURegBeanMappers euRegBeanMappers);

    @Override
    @AfterMapping
    protected void afterMapping(EURegAnlage euRegAnlage, @MappingTarget EURegAnlageBean euRegAnlageBean,
            BeanMappingContext context, EURegBeanMappers mappers) {
        super.afterMapping(euRegAnlage, euRegAnlageBean, context, mappers);
    }

}
