package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteDto;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteListItemDto;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;

@Mapper(uses = { ReferenzMapper.class, BehoerdenMapper.class })
public abstract class BetriebsstaetteMapper {

    @Autowired
    private StammdatenService stammdatenService;

    @Mapping(target = "betreiberId", source = "betreiber.id")
    @Mapping(target = "referenzAnlagenkataster", source = "refAnlagenkataster")
    public abstract BetriebsstaetteDto toDto(Betriebsstaette betriebsstaette);

    @Mapping(target = "betreiber", source = "betreiberId")
    @Mapping(target = "anlagen", ignore = true)
    @Mapping(target = "quellen", ignore = true)
    @Mapping(target = "refAnlagenkataster", source = "referenzAnlagenkataster")
    @Mapping(target = "komplexpruefung", ignore = true)
    @Mapping(target = "berichtsobjekte", ignore = true)
    @Mapping(target = "prtrVorschriften", ignore = true)
    @Mapping(target = "prtrTaetigkeitenOhneHaupttaetigkeit", ignore = true)
    public abstract Betriebsstaette fromDto(BetriebsstaetteDto betriebsstaetteDto);

    @Mapping(target = "adresseStrasse", source = "strasse")
    @Mapping(target = "adressePlz", source = "plz")
    @Mapping(target = "adresseOrt", source = "ort")
    @Mapping(target = "adresseHausNr", source = "hausNr")
    @Mapping(target = "zustaendigeBehoerde", source = "zustaendigeBehoerde.bezeichnung")
    @Mapping(target = "nummer", source = "betriebsstaetteNr")
    @Mapping(target = "betriebsstatus", source = "betriebsstatus.ktext")
    @Mapping(target = "canDelete", source = ".", qualifiedByName = "canDelete")
    abstract BetriebsstaetteListItemDto toListDto(BetriebsstaetteListItemView betriebsstaetteListItemView);

    @Named("canDelete")
    boolean canDelete(BetriebsstaetteListItemView source) {
        return stammdatenService.canDeleteBetriebsstaette(source);
    }

    Betreiber betreiberFromDto(Long betreiberId) {
        return betreiberId != null ? stammdatenService.loadBetreiber(betreiberId) : null;
    }

    static String einleiterNrToDto(EinleiterNr einleiterNr) {
        return einleiterNr.getEinleiterNr();
    }

    static EinleiterNr einleiterNrFromDto(String einleiterNr) {
        return new EinleiterNr(einleiterNr);
    }

    static String abfallerzeugerNummerToDto(AbfallerzeugerNr abfallerzeugerNummer) {
        return abfallerzeugerNummer.getAbfallerzeugerNr();
    }

    static AbfallerzeugerNr abfallerzeugerNummerFromDto(String abfallerzeugerNummer) {
        return new AbfallerzeugerNr(abfallerzeugerNummer);
    }
}
