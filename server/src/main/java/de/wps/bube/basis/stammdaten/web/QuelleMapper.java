package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.QuelleDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;

@Mapper(uses = ReferenzMapper.class)
public interface QuelleMapper {

    QuelleDto toDto(Quelle quelle);

    Quelle fromDto(QuelleDto quelleDto);

}
