package de.wps.bube.basis.stammdatenbetreiber.web;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.web.openapi.api.SpbBetreiberApi;
import de.wps.bube.basis.base.web.openapi.model.SpbBetreiberDto;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;

@RestController
public class SpbBetreiberRestController implements SpbBetreiberApi {

    private final StammdatenBetreiberService service;
    private final SpbBetreiberMapper mapper;

    public SpbBetreiberRestController(StammdatenBetreiberService service,
            SpbBetreiberMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<SpbBetreiberDto> createSpbBetreiber(@Valid SpbBetreiberDto spbBetreiberDto) {
        return ResponseEntity.ok(mapper.toDto(
                service.createSpbBetreiber(spbBetreiberDto.getBetriebsstaetteId(), mapper.fromDto(spbBetreiberDto))));
    }

    @Override
    public ResponseEntity<SpbBetreiberDto> readSpbBetreiberByJahrLandNummer(String jahr, String land, String nummer) {
        return ResponseEntity.ok(mapper.toDto(
                service.loadSpbBetreiberByJahrLandBetriebsstaetteNummer(jahr, Land.of(land), nummer)));
    }

    @Override
    public ResponseEntity<SpbBetreiberDto> updateSpbBetreiber(@Valid SpbBetreiberDto spbBetreiberDto) {
        return ResponseEntity.ok(mapper.toDto(service.updateSpbBetreiber(mapper.fromDto(spbBetreiberDto), spbBetreiberDto.getBetriebsstaetteId())));
    }

    @Override
    public ResponseEntity<SpbBetreiberDto> deleteSpbBetreiber(Long id) {
        return ResponseEntity.ok(mapper.toDto(service.deleteSpbBetreiber(id)));
    }

}
