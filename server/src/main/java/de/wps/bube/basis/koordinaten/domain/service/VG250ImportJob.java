package de.wps.bube.basis.koordinaten.domain.service;

import static java.lang.String.format;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ContentFeatureCollection;
import org.locationtech.jts.geom.MultiPolygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.koordinaten.domain.entity.Gemeindekoordinaten;
import de.wps.bube.basis.koordinaten.persistence.GemeindekoordinatenRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import net.lingala.zip4j.ZipFile;

@Service
public class VG250ImportJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(VG250ImportJob.class);
    public static final JobStatusEnum JOB_NAME = JobStatusEnum.GEMEINDEKOORDINATEN_IMPORTIEREN;

    private static final int COUNT_SIZE = 50;

    private final GemeindekoordinatenRepository gemeindekoordinatenRepository;
    private final BenutzerJobRegistry jobRegistry;
    private final AktiverBenutzer aktiverBenutzer;

    private static final String TEMPDIR = System.getProperty("java.io.tmpdir");

    @Value("${bube.koordinaten.import.zipname}")
    String zipname;
    @Value("${bube.koordinaten.import.shapefilePath}")
    String shapefilePath;
    @Value("${bube.koordinaten.import.shapefileArchivePath}")
    String shapefileArchivePath;
    @Value("${bube.koordinaten.import.directory}")
    String directory;

    private String vg250GemPath;
    private String vg250GemDir;
    private String vg250GemArchDir;
    private String vg250Utm32Zip;

    public VG250ImportJob(
            GemeindekoordinatenRepository gemeindekoordinatenRepository,
            BenutzerJobRegistry jobRegistry, AktiverBenutzer aktiverBenutzer) {
        this.gemeindekoordinatenRepository = gemeindekoordinatenRepository;
        this.jobRegistry = jobRegistry;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @PostConstruct
    void initPaths() {
        vg250GemPath = TEMPDIR + shapefilePath;
        vg250GemArchDir = TEMPDIR + shapefileArchivePath;
        vg250GemDir = TEMPDIR + directory;
        vg250Utm32Zip = TEMPDIR + zipname;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(String downloadLink, Referenz berichtsjahr) {
        JobProtokoll jobProtokoll = new JobProtokoll();
        jobProtokoll.addEintrag(
                format("%s Start: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), aktiverBenutzer.getUsername()));

        try {
            downloadZipFile(downloadLink, jobProtokoll);
            var aktuellerPfad = vg250GemArchDir.replace("****", berichtsjahr.getSchluessel());

            URL shapeFileUrl = getShapeFilePath(aktuellerPfad);
            importShapeFile(shapeFileUrl, jobProtokoll, berichtsjahr);
            deleteTempFiles(jobProtokoll);

            jobRegistry.jobCompleted(false);
            JobStatus jobStatus = jobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Gemeinekoordinaten: %d Laufzeit Sekunden: %d", JOB_NAME,
                            DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));

        } catch (Exception e) {
            LOGGER.error("Fehler beim " + JOB_NAME + "\n" + e.getMessage());
            jobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            //Protokoll Speichern
            jobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }

    public URL getShapeFilePath(String aktuellerPfad) throws MalformedURLException {
        File shapeFile = new File(vg250GemPath);
        if(!shapeFile.exists()) {
            shapeFile = new File(aktuellerPfad);
        }
        return shapeFile.toURI().toURL();
    }

    public void downloadZipFile(String downloadLink, JobProtokoll jobProtokoll) throws IOException {

        jobProtokoll.addEintrag("Starte Download von " + downloadLink);
        File file = new File(vg250Utm32Zip);

        InputStream inputStream = new URL(downloadLink).openConnection().getInputStream();
        FileUtils.copyInputStreamToFile(inputStream, file);

        jobProtokoll.addEintrag("Entpacke ZIP-File");
        ZipFile zipFile = new ZipFile(file);
        zipFile.extractAll(TEMPDIR);
    }

    public void deleteTempFiles(JobProtokoll jobProtokoll) throws IOException {
        jobProtokoll.addEintrag("Lösche temporäre Zip-Datei: " + vg250Utm32Zip);

        File zip = new File(vg250Utm32Zip);
        zip.delete();

        jobProtokoll.addEintrag("Lösche temporäre entpacke Dateien: " + vg250GemDir);
        File dir = new File(vg250GemDir);
        FileUtils.deleteDirectory(dir);
    }

    @Transactional
    public long importShapeFile(URL shapeFileUrl, JobProtokoll jobProtokoll, Referenz berichtsjahr) throws KoordinatenException, IOException {

        AtomicInteger count = new AtomicInteger();

        // Gemeindekoordinaten alle Löschen
        jobProtokoll.addEintrag("Lösche alte Koordinaten aus Datenbank");
        gemeindekoordinatenRepository.deleteAllByBerichtsjahr(berichtsjahr);

        final ShapefileDataStore shapefileDataStore = new ShapefileDataStore(shapeFileUrl);
        shapefileDataStore.setCharset(StandardCharsets.UTF_8);

        ContentFeatureCollection featureCollection = shapefileDataStore.getFeatureSource().getFeatures();
        String agsAttr = findAttributeName(featureCollection.getSchema(), "AGS").orElseThrow(
                () -> new KoordinatenException("Attribut AGS fehlt"));
        String genAttr = findAttributeName(featureCollection.getSchema(), "GEN").orElseThrow(
                () -> new KoordinatenException("Attribut GEN fehlt"));
        String bezAttr = findAttributeName(featureCollection.getSchema(), "BEZ").orElseThrow(
                () -> new KoordinatenException("Attribut BEZ fehlt"));

        jobProtokoll.addEintrag("Lege Koordinaten in Datenbank an");

        try (SimpleFeatureIterator features = featureCollection.features()) {
            while (features.hasNext()) {
                final SimpleFeature feature = features.next();

                final String ags = (String) feature.getAttribute(agsAttr);
                final String gen = (String) feature.getAttribute(genAttr);
                final String bez = (String) feature.getAttribute(bezAttr);

                final MultiPolygon geom = (MultiPolygon) feature.getDefaultGeometry();

                if (StringUtils.isBlank(ags)) {
                    throw new KoordinatenException("AGS Attribut muss vorhanden sein");
                }
                if (geom == null) {
                    throw new KoordinatenException("Es muss eine Geometrie vorhanden sein");
                }

                geom.setSRID(Gemeindekoordinaten.SRID_UTM32);

                // Gemeindekoordinaten anlegen
                Gemeindekoordinaten s = new Gemeindekoordinaten(ags, gen, bez, geom, berichtsjahr);
                gemeindekoordinatenRepository.save(s);

                if (count.incrementAndGet() % COUNT_SIZE == 0) {
                    jobRegistry.jobCountChanged(COUNT_SIZE);
                }
            }
        }
        jobRegistry.jobCountChanged(count.get() % COUNT_SIZE);

        return count.get();
    }

    public static Optional<String> findAttributeName(SimpleFeatureType featureType, String field) {
        List<AttributeDescriptor> attributeDescriptors = featureType.getAttributeDescriptors();
        for (AttributeDescriptor attributeDescriptor : attributeDescriptors) {
            String name = attributeDescriptor.getLocalName();
            if (name.equalsIgnoreCase(field)) {
                return Optional.of(name);
            }
        }
        return Optional.empty();
    }
}
