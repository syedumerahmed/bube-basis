package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;

public class AnlageAngelegtEvent {
    public static final String EVENT_NAME = "Anlage angelegt";

    public final Anlage anlage;

    public AnlageAngelegtEvent(Anlage a) {
        this.anlage = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AnlageAngelegtEvent that = (AnlageAngelegtEvent) o;
        return Objects.equals(anlage, that.anlage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anlage);
    }
}
