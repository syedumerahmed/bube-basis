package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.SpbQuelleVergleichDto;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbQuelleVergleich;

@Mapper(uses = { ReferenzMapper.class })
public abstract class SpbQuelleVergleichMapper {
    public abstract SpbQuelleVergleichDto toDto(SpbQuelleVergleich spbQuelleVergleich);
}
