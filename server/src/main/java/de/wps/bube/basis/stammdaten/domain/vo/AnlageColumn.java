package de.wps.bube.basis.stammdaten.domain.vo;

import static de.wps.bube.basis.stammdaten.domain.entity.QAnlage.anlage;

import com.querydsl.core.types.dsl.StringExpression;

import de.wps.bube.basis.base.persistence.Column;

public enum AnlageColumn implements Column {

    ANLAGEN_NUMMER(emptyIfNull(anlage.anlageNr)),
    NAME(emptyIfNull(anlage.name)),
    BETRIEBSSTATUS(emptyIfNull(anlage.betriebsstatus.ktext)),
    AENDERUNG_BETREIBER(emptyIfNull(anlage.letzteAenderungBetreiber.stringValue()));

    private static StringExpression emptyIfNull(StringExpression expression) {
        return expression.coalesce("").asString();
    }

    private final StringExpression expression;

    AnlageColumn(StringExpression expression) {
        this.expression = expression;
    }

    @Override
    public StringExpression getExpression() {
        return expression;
    }
}
