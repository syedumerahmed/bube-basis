package de.wps.bube.basis.referenzdaten.domain.service;

public interface ReferenzdatenJob {

    String PROTOKOLL_OPENING = "Bitte Protokoll bis zum Ende durchlesen, um das Ergebnis der Übertragung bzw. Nicht-Übertragung von bestehenden Referenzdaten zu erfahren.";

}
