package de.wps.bube.basis.stammdatenbetreiber.domain.service;

import java.time.Instant;

import org.springframework.stereotype.Service;

import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;

@Service
public class UpdateStammdatenDurchBetreiberAenderungService {

    private final StammdatenService stammdatenService;

    public UpdateStammdatenDurchBetreiberAenderungService(
            StammdatenService stammdatenService) {
        this.stammdatenService = stammdatenService;
    }

    void updateAenderungDurchBetreiber(SpbBetriebsstaette spbBetriebsstaette) {
        stammdatenService.aenderungBetriebsstaetteDurchBetreiber(spbBetriebsstaette.getBetriebsstaette().getId());
    }

    void updateAenderungDurchBetreiber(SpbAnlage spbAnlage) {
        if (spbAnlage.getAnlageId() != null) {
            stammdatenService.aenderungAnlageDurchBetreiber(spbAnlage.getAnlageId());
        }
        stammdatenService.aenderungBetriebsstaetteDurchBetreiber(spbAnlage.getParentBetriebsstaetteId());
    }

    void updateAenderungDurchBetreiber(SpbAnlagenteil spbAnlagenteil, Long parentBetriebsstaetteId) {

        if (spbAnlagenteil.getAnlagenteilId() != null) {
            stammdatenService.aenderungAnlagenteilDurchBetreiber(spbAnlagenteil.getAnlagenteilId());
        }
        if (spbAnlagenteil.getParentAnlageId() != null) {
            stammdatenService.aenderungAnlageDurchBetreiber(spbAnlagenteil.getParentAnlageId());
        }

        stammdatenService.aenderungBetriebsstaetteDurchBetreiber(parentBetriebsstaetteId);
    }

    void updateAenderungDurchBetreiber(SpbQuelle spbQuelle) {

        if (spbQuelle.getQuelleId() != null) {
            stammdatenService.aenderungQuelleDurchBetreiber(spbQuelle.getQuelleId());
        }
        if (spbQuelle.getParentAnlageId() != null) {
            stammdatenService.aenderungAnlageDurchBetreiber(spbQuelle.getParentAnlageId());
        }
        if (spbQuelle.getParentBetriebsstaetteId() != null) {
            stammdatenService.aenderungBetriebsstaetteDurchBetreiber(spbQuelle.getParentBetriebsstaetteId()
            );
        }
    }

    void updateAenderungDurchBetreiber(SpbBetreiber spbBetreiber, Long parentBetriebsstaetteId) {
        stammdatenService.aenderungBetreiberDurchBetreiber(spbBetreiber.getBetreiberId());
        stammdatenService.aenderungBetriebsstaetteDurchBetreiber(parentBetriebsstaetteId);
    }
}
