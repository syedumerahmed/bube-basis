package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RQUEA;

import java.time.Instant;
import java.util.Objects;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class QuelleBean implements PruefObjekt {

    private final Long id;
    private final String referenzAnlagenkataster;
    private final String quelleNr;
    private final Long parentBetriebsstaetteId;
    private final Long parentAnlageId;
    private final GeoPunktBean geoPunkt;
    private final String name;
    private final ReferenzBean quellenArt;
    private final Double flaeche;
    private final Double laenge;
    private final Double breite;
    private final Double durchmesser;
    private final Double bauhoehe;
    private final String bemerkung;
    private final boolean betreiberdatenUebernommen;
    private final Instant ersteErfassung;
    private final Instant letzteAenderung;
    private final Supplier<BetriebsstaetteBean> betriebsstaette;
    private final java.util.function.Supplier<AnlageBean> anlage;

    public QuelleBean(Long id, String referenzAnlagenkataster, String quelleNr, Long parentBetriebsstaetteId,
            Long parentAnlageId, GeoPunktBean geoPunkt, String name, ReferenzBean quellenArt, Double flaeche,
            Double laenge, Double breite, Double durchmesser, Double bauhoehe, String bemerkung,
            boolean betreiberdatenUebernommen, Instant ersteErfassung, Instant letzteAenderung,
            Supplier<BetriebsstaetteBean> betriebsstaette, java.util.function.Supplier<AnlageBean> anlage) {
        this.id = id;
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        this.quelleNr = quelleNr;
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        this.parentAnlageId = parentAnlageId;
        this.geoPunkt = geoPunkt;
        this.name = name;
        this.quellenArt = quellenArt;
        this.flaeche = flaeche;
        this.laenge = laenge;
        this.breite = breite;
        this.durchmesser = durchmesser;
        this.bauhoehe = bauhoehe;
        this.bemerkung = bemerkung;
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        this.ersteErfassung = ersteErfassung;
        this.letzteAenderung = letzteAenderung;
        this.betriebsstaette = Suppliers.memoize(betriebsstaette::get);
        this.anlage = Suppliers.memoize(anlage::get);
    }

    @RegelApi.Member(exclude = true)
    public Long getId() {
        return id;
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.QUELLE;
    }

    @RegelApi.Member(description = "Anlagenkataster")
    public String getReferenzAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    @RegelApi.Member(description = "Nummer")
    public String getQuelleNr() {
        return quelleNr;
    }

    @RegelApi.Member(description = "Geo-Koordinaten")
    public GeoPunktBean getGeoPunkt() {
        return geoPunkt;
    }

    @RegelApi.Member(description = "Name")
    public String getName() {
        return name;
    }

    @RegelApi.Member(referenzliste = RQUEA)
    public ReferenzBean getQuellenArt() {
        return quellenArt;
    }

    @RegelApi.Member(description = "Fläche [m²]")
    public Double getFlaeche() {
        return flaeche;
    }

    @RegelApi.Member(description = "Länge [m]")
    public Double getLaenge() {
        return laenge;
    }

    @RegelApi.Member(description = "Breite [m]")
    public Double getBreite() {
        return breite;
    }

    @RegelApi.Member(description = "Durchmesser [m]")
    public Double getDurchmesser() {
        return durchmesser;
    }

    @RegelApi.Member(description = "Bauhöhe [m]")
    public Double getBauhoehe() {
        return bauhoehe;
    }

    @RegelApi.Member(description = "Bemerkung")
    public String getBemerkung() {
        return bemerkung;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    @RegelApi.Member(exclude = true)
    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    @RegelApi.Member(description = "Die Betriebsstätte der (Anlage der) Quelle")
    public BetriebsstaetteBean getBetriebsstaette() {
        return parentBetriebsstaetteId != null ? betriebsstaette.get() : getAnlage().getBetriebsstaette();
    }

    @RegelApi.Member(exclude = true)
    public Long getParentAnlageId() {
        return parentAnlageId;
    }

    @RegelApi.Member(description = "Die Anlage der Quelle (sofern diese nicht direkt einer BST zugeordnet ist)")
    public AnlageBean getAnlage() {
        return parentAnlageId != null ? anlage.get() : null;
    }

    @RegelApi.Member(description = "Betreiberdaten übernommen")
    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        QuelleBean that = (QuelleBean) o;
        return betreiberdatenUebernommen == that.betreiberdatenUebernommen && Objects.equals(id,
                that.id) && Objects.equals(referenzAnlagenkataster,
                that.referenzAnlagenkataster) && Objects.equals(quelleNr,
                that.quelleNr) && Objects.equals(parentBetriebsstaetteId,
                that.parentBetriebsstaetteId) && Objects.equals(parentAnlageId,
                that.parentAnlageId) && Objects.equals(geoPunkt, that.geoPunkt) && Objects.equals(name,
                that.name) && Objects.equals(quellenArt, that.quellenArt) && Objects.equals(flaeche,
                that.flaeche) && Objects.equals(laenge, that.laenge) && Objects.equals(breite,
                that.breite) && Objects.equals(durchmesser, that.durchmesser) && Objects.equals(
                bauhoehe, that.bauhoehe) && Objects.equals(bemerkung, that.bemerkung) && Objects.equals(
                ersteErfassung, that.ersteErfassung) && Objects.equals(letzteAenderung, that.letzteAenderung);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, referenzAnlagenkataster, quelleNr, parentBetriebsstaetteId, parentAnlageId, geoPunkt,
                name, quellenArt, flaeche, laenge, breite, durchmesser, bauhoehe, bemerkung, betreiberdatenUebernommen,
                ersteErfassung, letzteAenderung);
    }

    @Override
    public String toString() {
        return "QuelleBean{" +
                "id=" + id +
                ", referenzAnlagenkataster='" + referenzAnlagenkataster + '\'' +
                ", quelleNr='" + quelleNr + '\'' +
                ", parentBetriebsstaetteId=" + parentBetriebsstaetteId +
                ", parentAnlageId=" + parentAnlageId +
                ", geoPunkt=" + geoPunkt +
                ", name='" + name + '\'' +
                ", quellenArt=" + quellenArt +
                ", flaeche=" + flaeche +
                ", laenge=" + laenge +
                ", breite=" + breite +
                ", durchmesser=" + durchmesser +
                ", bauhoehe=" + bauhoehe +
                ", bemerkung='" + bemerkung + '\'' +
                ", betreiberdatenUebernommen=" + betreiberdatenUebernommen +
                ", ersteErfassung=" + ersteErfassung +
                ", letzteAenderung=" + letzteAenderung +
                '}';
    }

}
