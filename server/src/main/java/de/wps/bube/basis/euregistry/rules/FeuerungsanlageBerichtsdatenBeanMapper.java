package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.rules.bean.FeuerungsanlageBerichtsdatenBean;

@Mapper(uses = SpezielleBedingungIE51BeanMapper.class)
public abstract class FeuerungsanlageBerichtsdatenBeanMapper {

    public abstract FeuerungsanlageBerichtsdatenBean toBean(FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten);

}
