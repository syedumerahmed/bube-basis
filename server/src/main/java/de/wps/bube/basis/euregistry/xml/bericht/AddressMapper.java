package de.wps.bube.basis.euregistry.xml.bericht;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.Address;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;

@Mapper(uses = CommonEURegMapper.class)
public abstract class AddressMapper {

    @Mapping(target = "streetName", source = "strasse")
    @Mapping(target = "buildingNumber", source = "hausNr")
    @Mapping(target = "city", source = "ort")
    @Mapping(target = "postalCode", source = "plz")
    @Mapping(target = "confidentialityReason", source = "vertraulichkeitsgrund", qualifiedByName = "reasonValue")
    public abstract Address mapAddress(Adresse adresse);
}
