package de.wps.bube.basis.euregistry.domain.service;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.jobs.domain.JobsException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenJob;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Service
public class EuRegDatenUebernehmenJob {

    public static final JobStatusEnum JOB_NAME = JobStatusEnum.BERICHTSDATEN_UEBERNEHMEN;

    private static final Logger LOGGER = LoggerFactory.getLogger(EuRegDatenUebernehmenJob.class);

    private final EURegistryService euRegistryService;
    private final StammdatenService stammdatenService;
    private final AktiverBenutzer aktiverBenutzer;
    private final BenutzerJobRegistry benutzerJobRegistry;

    public EuRegDatenUebernehmenJob(BenutzerJobRegistry benutzerJobRegistry, EURegistryService euRegistryService,
            StammdatenService stammdatenService, AktiverBenutzer aktiverBenutzer) {
        this.benutzerJobRegistry = benutzerJobRegistry;
        this.euRegistryService = euRegistryService;
        this.stammdatenService = stammdatenService;
        this.aktiverBenutzer = aktiverBenutzer;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void runAsync(List<Long> betriebsstaettenIds, Referenz zieljahr) {
        JobProtokoll jobProtokoll = new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(
                format("%s Start: %s Zieljahr: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), zieljahr.getSchluessel(),
                        aktiverBenutzer.getUsername()));

        try {

            List<Betriebsstaette> nichtUebertragen = new ArrayList<>();

            stammdatenService.streamAllById(betriebsstaettenIds)
                             .forEach(b -> {
                                 if (euRegistryService.copyDaten(b, zieljahr, jobProtokoll)) {
                                     benutzerJobRegistry.jobCountChanged(1);
                                 } else {
                                     nichtUebertragen.add(b);
                                 }
                             });
            if (nichtUebertragen.isEmpty()) {
                benutzerJobRegistry.jobCompleted(jobProtokoll.isWarnungVorhanden());
            } else {
                String msg = format("%d Betriebsstättenbericht(e) wurde(n) nicht übernommen.", nichtUebertragen.size());
                jobProtokoll.addEintrag(msg);
                benutzerJobRegistry.jobError(new JobsException(msg));
            }

            JobStatus jobStatus = benutzerJobRegistry.getJobStatus().get();
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Betriebsstätten: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            benutzerJobRegistry.jobError(e);
            jobProtokoll.addFehler(
                    format("%s Fehler sämtliche Änderungen werden nicht gespeichert: %s", JOB_NAME, DateTime.nowCET()),
                    e.getMessage());
        } finally {
            //Protokoll Speichern
            benutzerJobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }

}

