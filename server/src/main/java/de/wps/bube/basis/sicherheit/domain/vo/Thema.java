package de.wps.bube.basis.sicherheit.domain.vo;

import java.util.stream.Stream;

public enum Thema {

    WASSER("Wasser"),
    ABFALL("Abfall"),
    LUFT("Luft"),
    BODEN("Boden"),
    LCP("LCP");

    private final String bezeichner;

    Thema(final String bezeichner) {
        this.bezeichner = bezeichner;
    }

    public String getBezeichner() {
        return bezeichner;
    }

    public static Thema of(final String bezeichner) {
        return Stream.of(values())
                     .filter(p -> p.getBezeichner().equals(bezeichner))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("Ungültiges Thema: " + bezeichner));
    }
}
