package de.wps.bube.basis.stammdaten.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RFLEZ;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RGMD;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RJAHR;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RNACE;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RVORS;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.AdresseBean;
import de.wps.bube.basis.referenzdaten.rules.bean.BehoerdeBean;
import de.wps.bube.basis.referenzdaten.rules.bean.KommunikationsverbindungBean;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

@RegelApi.Type(name = "Betriebsstätte")
public class BetriebsstaetteBean extends StammdatenObjektBean<BetriebsstaetteBean> {

    private final ReferenzBean berichtsjahr;
    private final Land land;
    private final BehoerdeBean zustaendigeBehoerde;
    private final String akz;
    private final String betriebsstaetteNr;
    private final BetreiberBean betreiber;
    private final AdresseBean adresse;
    private final List<AnsprechpartnerBean> ansprechpartner;
    private final List<KommunikationsverbindungBean> kommunikationsverbindungen;
    private final ReferenzBean gemeindekennziffer;
    private final List<String> einleiterNummern;
    private final List<ReferenzBean> vorschriften;
    private final List<String> abfallerzeugerNummern;
    private final ReferenzBean flusseinzugsgebiet;
    private final ReferenzBean naceCode;
    private final boolean direkteinleiter;
    private final boolean indirekteinleiter;
    private final List<QuelleBean> quellen;
    private final List<AnlageBean> anlagen;

    public BetriebsstaetteBean(Long id, String localId, String refAnlagenkataster, ReferenzBean berichtsjahr,
            Land land, BehoerdeBean zustaendigeBehoerde, String akz, String betriebsstaetteNr, BetreiberBean betreiber,
            AdresseBean adresse, List<AnsprechpartnerBean> ansprechpartner,
            List<KommunikationsverbindungBean> kommunikationsverbindungen, GeoPunktBean geoPunkt,
            ReferenzBean gemeindekennziffer, List<String> einleiterNummern, List<ReferenzBean> vorschriften,
            List<String> abfallerzeugerNummern, List<ZustaendigeBehoerdeBean> zustaendigeBehoerden, String name,
            ReferenzBean vertraulichkeitsgrund, ReferenzBean flusseinzugsgebiet, ReferenzBean naceCode,
            boolean direkteinleiter, boolean indirekteinleiter, ReferenzBean betriebsstatus,
            LocalDate betriebsstatusSeit, LocalDate inbetriebnahme, String bemerkung, boolean betreiberdatenUebernommen,
            List<QuelleBean> quellen, List<AnlageBean> anlagen, Instant ersteErfassung, Instant letzteAenderung,
            Supplier<BetriebsstaetteBean> vorgaenger, Supplier<BetriebsstaetteBean> nachfolger) {
        super(id, localId, refAnlagenkataster, geoPunkt, zustaendigeBehoerden, name, vertraulichkeitsgrund,
                betriebsstatus, betriebsstatusSeit, inbetriebnahme, bemerkung, betreiberdatenUebernommen,
                ersteErfassung, letzteAenderung, vorgaenger, nachfolger);
        this.berichtsjahr = berichtsjahr;
        this.land = land;
        this.zustaendigeBehoerde = zustaendigeBehoerde;
        this.akz = akz;
        this.betriebsstaetteNr = betriebsstaetteNr;
        this.betreiber = betreiber;
        this.adresse = adresse;
        this.ansprechpartner = ansprechpartner;
        this.kommunikationsverbindungen = kommunikationsverbindungen;
        this.gemeindekennziffer = gemeindekennziffer;
        this.einleiterNummern = einleiterNummern;
        this.vorschriften = vorschriften;
        this.abfallerzeugerNummern = abfallerzeugerNummern;
        this.flusseinzugsgebiet = flusseinzugsgebiet;
        this.naceCode = naceCode;
        this.direkteinleiter = direkteinleiter;
        this.indirekteinleiter = indirekteinleiter;
        this.quellen = quellen;
        this.anlagen = anlagen;
    }

    @RegelApi.Member(referenzliste = RJAHR)
    public ReferenzBean getBerichtsjahr() {
        return berichtsjahr;
    }

    public Land getLand() {
        return land;
    }

    @RegelApi.Member(description = "Zuständige Behörde")
    public BehoerdeBean getZustaendigeBehoerde() {
        return zustaendigeBehoerde;
    }

    @RegelApi.Member(description = "AKZ")
    public String getAkz() {
        return akz;
    }

    @RegelApi.Member(description = "Betriebsstättennummer")
    public String getBetriebsstaetteNr() {
        return betriebsstaetteNr;
    }

    @RegelApi.Member(description = "Betreiber")
    public BetreiberBean getBetreiber() {
        return betreiber;
    }

    @RegelApi.Member(description = "Adresse")
    public AdresseBean getAdresse() {
        return adresse;
    }

    @RegelApi.Member(description = "Ansprechpersonen")
    public List<AnsprechpartnerBean> getAnsprechpartner() {
        return ansprechpartner;
    }

    @RegelApi.Member(description = "Kommunikationsverbindungen")
    public List<KommunikationsverbindungBean> getKommunikationsverbindungen() {
        return kommunikationsverbindungen;
    }

    @RegelApi.Member(description = "Amtlicher Gemeindeschlüssel", referenzliste = RGMD)
    public ReferenzBean getGemeindekennziffer() {
        return gemeindekennziffer;
    }

    @RegelApi.Member(description = "Einleiter-Nr.")
    public List<String> getEinleiterNummern() {
        return einleiterNummern;
    }

    @RegelApi.Member(description = "Vorschriften", referenzliste = RVORS)
    public List<ReferenzBean> getVorschriften() {
        return vorschriften;
    }

    @RegelApi.Member(description = "Abfallerzeuger-Nr.")
    public List<String> getAbfallerzeugerNummern() {
        return abfallerzeugerNummern;
    }

    @RegelApi.Member(description = "Flusseinzugsgebiet", referenzliste = RFLEZ)
    public ReferenzBean getFlusseinzugsgebiet() {
        return flusseinzugsgebiet;
    }

    @RegelApi.Member(description = "NACE-Code", referenzliste = RNACE)
    public ReferenzBean getNaceCode() {
        return naceCode;
    }

    public boolean isDirekteinleiter() {
        return direkteinleiter;
    }

    public boolean isIndirekteinleiter() {
        return indirekteinleiter;
    }

    @RegelApi.Member(description = "Quellen")
    public List<QuelleBean> getQuellen() {
        return quellen;
    }

    @RegelApi.Member(description = "Anlagen")
    public List<AnlageBean> getAnlagen() {
        return anlagen;
    }

    @Override
    @RegelApi.Member(description = "Die Betriebsstätte selbst")
    public BetriebsstaetteBean getBetriebsstaette() {
        return this;
    }

    // Wird für Annotation überschrieben
    @Override
    @RegelApi.Member(description = "Betriebsstätte des jeweiligen Vorjahres; muss entweder in Land und LocalId oder in Land und BST-Nr. übereinstimmen.")
    public BetriebsstaetteBean getVorgaenger() {
        return super.getVorgaenger();
    }

    // Wird für Annotation überschrieben
    @Override
    @RegelApi.Member(description = "Betriebsstätte des jeweiligen Folgejahres; muss entweder in Land und LocalId oder in Land und BST-Nr. übereinstimmen.")
    public BetriebsstaetteBean getNachfolger() {
        return super.getNachfolger();
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.BST;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BetriebsstaetteBean that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        return direkteinleiter == that.direkteinleiter && indirekteinleiter == that.indirekteinleiter && Objects.equals(
                berichtsjahr, that.berichtsjahr) && land == that.land && Objects.equals(zustaendigeBehoerde,
                that.zustaendigeBehoerde) && Objects.equals(akz, that.akz) && Objects.equals(betriebsstaetteNr,
                that.betriebsstaetteNr) && Objects.equals(betreiber, that.betreiber) && Objects.equals(adresse,
                that.adresse) && Objects.equals(ansprechpartner, that.ansprechpartner) && Objects.equals(
                kommunikationsverbindungen, that.kommunikationsverbindungen) && Objects.equals(gemeindekennziffer,
                that.gemeindekennziffer) && Objects.equals(einleiterNummern, that.einleiterNummern) && Objects.equals(
                vorschriften, that.vorschriften) && Objects.equals(abfallerzeugerNummern,
                that.abfallerzeugerNummern) && Objects.equals(flusseinzugsgebiet,
                that.flusseinzugsgebiet) && Objects.equals(naceCode, that.naceCode) && Objects.equals(quellen,
                that.quellen) && Objects.equals(anlagen, that.anlagen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), berichtsjahr, land, zustaendigeBehoerde, akz, betriebsstaetteNr,
                betreiber, adresse, ansprechpartner, kommunikationsverbindungen, gemeindekennziffer, einleiterNummern,
                vorschriften, abfallerzeugerNummern, flusseinzugsgebiet, naceCode, direkteinleiter, indirekteinleiter,
                quellen, anlagen);
    }

    @Override
    public String toString() {
        return "BetriebsstaetteBean{" +
                "berichtsjahr=" + berichtsjahr +
                ", land=" + land +
                ", zustaendigeBehoerde=" + zustaendigeBehoerde +
                ", akz='" + akz + '\'' +
                ", betriebsstaetteNr='" + betriebsstaetteNr + '\'' +
                ", betreiber=" + betreiber +
                ", adresse=" + adresse +
                ", ansprechpartner=" + ansprechpartner +
                ", kommunikationsverbindungen=" + kommunikationsverbindungen +
                ", gemeindekennziffer=" + gemeindekennziffer +
                ", einleiterNummern=" + einleiterNummern +
                ", vorschriften=" + vorschriften +
                ", abfallerzeugerNummern=" + abfallerzeugerNummern +
                ", flusseinzugsgebiet=" + flusseinzugsgebiet +
                ", naceCode=" + naceCode +
                ", direkteinleiter=" + direkteinleiter +
                ", indirekteinleiter=" + indirekteinleiter +
                ", quellen=" + quellen +
                ", anlagen=" + anlagen +
                " // " + super.toString() +
                '}';
    }

}
