package de.wps.bube.basis.stammdaten.domain.event;

import java.util.Objects;

public class AnlageGeloeschtEvent {
    public static final String EVENT_NAME = "Anlage gelöscht";

    public final Long anlageId;
    public final Long bstId;

    public AnlageGeloeschtEvent(Long anlageId, Long bstId) {
        this.anlageId = anlageId;
        this.bstId = bstId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnlageGeloeschtEvent that = (AnlageGeloeschtEvent) o;
        return Objects.equals(anlageId, that.anlageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anlageId);
    }
}
