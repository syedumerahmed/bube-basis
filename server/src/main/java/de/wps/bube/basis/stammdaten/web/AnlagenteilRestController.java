package de.wps.bube.basis.stammdaten.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.AnlagenteilApi;
import de.wps.bube.basis.base.web.openapi.model.AnlagenteilDto;
import de.wps.bube.basis.base.web.openapi.model.AnlagenteilListItemDto;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsdatenPruefungService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class AnlagenteilRestController implements AnlagenteilApi {

    private final StammdatenService stammdatenService;
    private final AnlagenteilMapper mapper;
    private final BerichtsdatenPruefungService berichtsdatenPruefungService;

    public AnlagenteilRestController(StammdatenService stammdatenService, AnlagenteilMapper mapper,
            BerichtsdatenPruefungService berichtsdatenPruefungService) {
        this.stammdatenService = stammdatenService;
        this.mapper = mapper;
        this.berichtsdatenPruefungService = berichtsdatenPruefungService;
    }

    @Override
    public ResponseEntity<List<AnlagenteilListItemDto>> readAllAnlagenteile(Long anlageId, Long betriebsstaetteId) {

        List<AnlagenteilListItemDto> list = stammdatenService.loadAllAnlagenteileByAnlageId(anlageId, betriebsstaetteId)
                                                             .stream()
                                                             .map(anlagenteil -> {
                                                                 AnlagenteilListItemDto dto = new AnlagenteilListItemDto();
                                                                 dto.setBetriebsstatus(
                                                                         anlagenteil.getBetriebsstatus() == null ?
                                                                                 null :
                                                                                 anlagenteil.getBetriebsstatus()
                                                                                            .getSchluessel());
                                                                 dto.setId(anlagenteil.getId());
                                                                 dto.setName(anlagenteil.getName());
                                                                 dto.setNummer(anlagenteil.getAnlagenteilNr());
                                                                 dto.setBetreiberdatenUebernommen(anlagenteil.isBetreiberdatenUebernommen());
                                                                 dto.setLetzteAenderungBetreiber(
                                                                         anlagenteil.getLetzteAenderungBetreiber() == null ? null : anlagenteil.getLetzteAenderungBetreiber()
                                                                                                                                               .toString());
                                                                 return dto;
                                                             })
                                                             .collect(Collectors.toList());
        return ResponseEntity.ok(list);
    }

    @Override
    public ResponseEntity<AnlagenteilDto> createAnlagenteil(AnlagenteilDto anlagenteilDto) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.createAnlagenteil(anlagenteilDto.getParentBetriebsstaetteId(),
                        mapper.fromDto(anlagenteilDto))));
    }

    @Override
    public ResponseEntity<AnlagenteilDto> readAnlagenteil(Long betriebsstaetteId, Long parentAnlageId,
            String anlagenteilNummer) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.loadAnlagenteilByAnlagenteilNummer(betriebsstaetteId, parentAnlageId, anlagenteilNummer)));
    }

    @Override
    public ResponseEntity<AnlagenteilDto> updateAnlagenteil(AnlagenteilDto anlagenteilDto) {
        return ResponseEntity.ok(
                mapper.toDto(stammdatenService.updateAnlagenteil(anlagenteilDto.getParentBetriebsstaetteId(),
                        mapper.fromDto(anlagenteilDto))));
    }

    @Override
    public ResponseEntity<Void> deleteAnlagenteil(Long betriebsstaetteId, Long id) {
        stammdatenService.deleteAnlagenteil(betriebsstaetteId, id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleAnlagenteile(Long betriebsstaetteId, List<Long> ids) {
        stammdatenService.deleteMultipleAnlagenteile(betriebsstaetteId, ids);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Boolean> anlagenteilBrauchtWarnungVorSpeicherung(AnlagenteilDto anlagenteilDto) {
        return ResponseEntity.ok(
                berichtsdatenPruefungService.brauchtAnlagenteilWarnungVorSpeicherung(mapper.fromDto(anlagenteilDto)));
    }
}
