package de.wps.bube.basis.komplexpruefung.web;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.RegelApi;
import de.wps.bube.basis.base.web.openapi.model.RegelDto;
import de.wps.bube.basis.base.web.openapi.model.RegelListItemDto;
import de.wps.bube.basis.base.web.openapi.model.TypAendernCommand;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelService;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;

@Controller
class RegelRestController implements RegelApi {

    private final RegelService regelService;
    private final RegelListItemMapper listItemMapper;
    private final RegelMapper mapper;

    RegelRestController(RegelService regelService, RegelListItemMapper listItemMapper, RegelMapper mapper) {
        this.regelService = regelService;
        this.listItemMapper = listItemMapper;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<RegelListItemDto>> readRegelliste() {
        return ResponseEntity.ok(regelService.readAllRegeln().stream().map(listItemMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<RegelDto> loadRegel(Long nummer) {
        return ResponseEntity.ok(mapper.toDto(regelService.loadRegelByNummer(nummer)));
    }

    @Override
    public ResponseEntity<RegelDto> createRegel(RegelDto regelDto) {
        return ResponseEntity.ok(mapper.toDto(regelService.createRegel(mapper.fromDto(regelDto))));
    }

    @Override
    public ResponseEntity<RegelDto> updateRegel(RegelDto regelDto) {
        return ResponseEntity.ok(mapper.toDto(regelService.updateRegel(mapper.fromDto(regelDto))));
    }

    @Override
    public ResponseEntity<Void> typFuerMeinLandAendern(TypAendernCommand command) {
        regelService.typAendern(command.getRegeln(), RegelTyp.valueOf(command.getTyp().getValue()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteRegel(Long nummer) {
        regelService.deleteRegel(nummer);
        return ResponseEntity.ok().build();
    }

}
