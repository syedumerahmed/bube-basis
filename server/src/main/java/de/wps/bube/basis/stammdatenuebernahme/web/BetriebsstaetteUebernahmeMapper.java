package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteUebernahmeDto;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetriebsstaetteUebernahme;

@Mapper
public abstract class BetriebsstaetteUebernahmeMapper {
    public abstract BetriebsstaetteUebernahme fromDto(BetriebsstaetteUebernahmeDto betriebsstaetteUebernahmeDto);
}
