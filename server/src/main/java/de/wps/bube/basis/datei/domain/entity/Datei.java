package de.wps.bube.basis.datei.domain.entity;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.Instant;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import de.wps.bube.basis.datei.domain.DateiException;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Datei {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "datei_seq")
    @SequenceGenerator(name = "datei_seq", allocationSize = 50)
    private Long id;

    private String username;
    @Enumerated(EnumType.STRING)
    private DateiScope dateiScope;
    @CreatedDate
    private Instant createdDate;
    private boolean temp;
    private String dateiName;

    @Lob
    private Blob data;

    protected Datei() {
    }

    public Datei(String dateiName, String username, Blob data, DateiScope scope) {
        this.dateiName = dateiName;
        this.username = username;
        this.data = data;
        this.dateiScope = scope;
        this.temp = scope.isTemporary();
    }

    public InputStream getStream() {
        try {
            if (data == null) {
                return InputStream.nullInputStream();
            }
            return data.getBinaryStream();
        } catch (SQLException e) {
            throw new DateiException(e);
        }
    }

    public String getDateiName() {
        return dateiName;
    }
}
