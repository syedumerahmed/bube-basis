package de.wps.bube.basis.euregistry.xml;

import de.wps.bube.basis.base.xml.ImportException;

public class EuRegistryImportException extends ImportException {
    public EuRegistryImportException(String message) {
        super(message);
    }

    public EuRegistryImportException(String message, Throwable e) {
        super(message, e);
    }
}
