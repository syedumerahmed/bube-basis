package de.wps.bube.basis.stammdaten.security;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetreiber.betreiber;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsAccessor;

@Component
public class BetreiberBerechtigungsAccessor implements BerechtigungsAccessor {

    @Override
    public Predicate land(Land land) {
        return land.equals(Land.DEUTSCHLAND) ? null : betreiber.land.eq(land);
    }

    @Override
    public Predicate behoerdenIn(Set<String> behoerden) {
        return null;
    }

    @Override
    public Predicate akzIn(Set<String> akz) {
        return null;
    }

    @Override
    public Predicate verwaltungsgebieteIn(Set<String> verwaltungsgebiete) {
        return null;
    }

    @Override
    public Predicate betriebsstaettenIn(Set<String> betriebsstaetten) {
        return null;
    }
}
