package de.wps.bube.basis.stammdaten.domain.event;

public class BetreiberGeloeschtEvent {
    public static final String EVENT_NAME = "Betreiber gelöscht";

    public final Long betreiberId;

    public BetreiberGeloeschtEvent(Long betreiberId) {
        this.betreiberId = betreiberId;
    }
}
