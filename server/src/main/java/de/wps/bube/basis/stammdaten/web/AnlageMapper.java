package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.AnlageDto;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;

@Mapper(uses = { ReferenzMapper.class, BehoerdenMapper.class, VorschriftMapper.class })
public abstract class AnlageMapper {

    public abstract AnlageDto toDto(Anlage anlage);

    @Mapping(target = "anlagenteile", ignore = true)
    @Mapping(target = "quellen", ignore = true)
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    public abstract Anlage fromDto(AnlageDto anlageDto);

}
