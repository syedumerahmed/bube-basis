package de.wps.bube.basis.euregistry.web;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.TEILBERICHT;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.PageMapper;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.api.EuRegistryApi;
import de.wps.bube.basis.base.web.openapi.model.DownloadKomplexpruefungCommand;
import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageListItemDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaettenListStateDto;
import de.wps.bube.basis.base.web.openapi.model.ExportiereBerichtsdatenCommand;
import de.wps.bube.basis.base.web.openapi.model.KomplexpruefungDurchfuehrenCommand;
import de.wps.bube.basis.base.web.openapi.model.PageDtoEuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.base.web.openapi.model.RegelTestDto;
import de.wps.bube.basis.base.web.openapi.model.Suchattribute;
import de.wps.bube.basis.base.web.openapi.model.TesteRegelCommand;
import de.wps.bube.basis.base.web.openapi.model.UebernimmEuRegDatenInJahrCommandDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.event.BerichtPruefungEvent;
import de.wps.bube.basis.euregistry.domain.service.BerichtPruefungProtokollService;
import de.wps.bube.basis.euregistry.domain.service.BerichtPruefungService;
import de.wps.bube.basis.euregistry.domain.service.EURegistryImportExportService;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.domain.service.EURegistrySucheService;
import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteColumn;
import de.wps.bube.basis.euregistry.web.mapper.EuRegAnlagenMapper;
import de.wps.bube.basis.euregistry.web.mapper.EuRegAnlagenTreeToListMapper;
import de.wps.bube.basis.euregistry.web.mapper.EuRegBetriebsstaetteListItemMapper;
import de.wps.bube.basis.euregistry.web.mapper.EuRegBetriebsstaetteMapper;
import de.wps.bube.basis.euregistry.web.mapper.EuRegCommandMapper;
import de.wps.bube.basis.euregistry.web.mapper.EuRegListStateMapper;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.web.SuchattributeMapper;

@Controller
class EURegistryRestController implements EuRegistryApi {

    private final EURegistryService euRegistryService;
    private final EURegistryImportExportService importExportService;
    private final EURegistrySucheService sucheService;

    private final EuRegBetriebsstaetteListItemMapper listItemMapper;
    private final EuRegAnlagenTreeToListMapper anlagenTreeToListMapper;
    private final EuRegCommandMapper euRegCommandMapper;
    private final EuRegBetriebsstaetteMapper mapper;
    private final EuRegAnlagenMapper anlagenMapper;
    private final EuRegListStateMapper listStateMapper;
    private final SuchattributeMapper suchattributeMapper;
    private final PageMapper pageMapper;
    private final ReferenzMapper referenzMapper;

    private final StammdatenService stammdatenService;
    private final BerichtPruefungProtokollService berichtPruefungProtokollService;
    private final BerichtPruefungService berichtPruefungService;
    private final AktiverBenutzer aktiverBenutzer;
    private final ApplicationEventPublisher eventPublisher;

    EURegistryRestController(EURegistryService euRegistryService, EURegistryImportExportService importExportService,
            EURegistrySucheService sucheService, EuRegBetriebsstaetteListItemMapper listItemMapper,
            EuRegAnlagenTreeToListMapper anlagenTreeToListMapper,
            EuRegCommandMapper euRegCommandMapper, EuRegBetriebsstaetteMapper mapper, EuRegAnlagenMapper anlagenMapper,
            EuRegListStateMapper listStateMapper, SuchattributeMapper suchattributeMapper, PageMapper pageMapper,
            ReferenzMapper referenzMapper, StammdatenService stammdatenService,
            BerichtPruefungProtokollService berichtPruefungProtokollService,
            BerichtPruefungService berichtPruefungService,
            AktiverBenutzer aktiverBenutzer, ApplicationEventPublisher eventPublisher) {
        this.euRegistryService = euRegistryService;
        this.importExportService = importExportService;
        this.sucheService = sucheService;
        this.listItemMapper = listItemMapper;
        this.anlagenTreeToListMapper = anlagenTreeToListMapper;
        this.euRegCommandMapper = euRegCommandMapper;
        this.mapper = mapper;
        this.anlagenMapper = anlagenMapper;
        this.listStateMapper = listStateMapper;
        this.suchattributeMapper = suchattributeMapper;
        this.pageMapper = pageMapper;
        this.referenzMapper = referenzMapper;
        this.stammdatenService = stammdatenService;
        this.berichtPruefungProtokollService = berichtPruefungProtokollService;
        this.berichtPruefungService = berichtPruefungService;
        this.aktiverBenutzer = aktiverBenutzer;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public ResponseEntity<PageDtoEuRegBetriebsstaetteListItemDto> readAllBetriebsstaettenBerichte(
            Long berichtsjahrId, EuRegBetriebsstaettenListStateDto state) {

        ListStateDto<EuRegBetriebsstaetteColumn> listState = listStateMapper
                .fromEuRegBetriebsstaettenListStateDto(state);

        Page<EuRegBetriebsstaetteListItemDto> page = sucheService
                .readAllEuRegBetriebsstaette(berichtsjahrId, suchattributeMapper.fromDto(state.getSuche()),
                        listState.toPageable())
                .map(listItemMapper::toDto);
        return ResponseEntity.ok(pageMapper.mapEuRegBetriebsstaetteListItemToDto(page));
    }

    @Override
    public ResponseEntity<List<Long>> readAllBetriebsstaettenBerichteIds(Long berichtsjahrId,
            Suchattribute suchattribute) {
        return ResponseEntity.ok(sucheService.readAllEuRegBetriebsstaetteIds(berichtsjahrId,
                suchattributeMapper.fromDto(suchattribute)));
    }

    @Override
    public ResponseEntity<EuRegBetriebsstaetteDto> readBetriebsstaettenBerichtById(Long berichtId) {

        EURegBetriebsstaette bericht = euRegistryService.loadBetriebsstaettenBerichtById(berichtId);
        return ResponseEntity.ok(mapper.toDto(bericht));
    }

    @Override
    public ResponseEntity<EuRegBetriebsstaetteDto> readBetriebsstaettenBerichtByJahrLandNummer(String jahrSchluessel,
            String landSchluessel, String nummer) {

        EURegBetriebsstaette bericht = euRegistryService.loadBetriebsstaettenBerichtByJahrLandNummer(jahrSchluessel,
                Land.of(landSchluessel), nummer);
        return ResponseEntity.ok(mapper.toDto(bericht));
    }

    @Override
    public ResponseEntity<EuRegBetriebsstaetteDto> readBetriebsstaettenBerichtByJahrAndLocalId(String jahrSchluessel,
            String localId) {

        EURegBetriebsstaette bericht = euRegistryService
                .loadBetriebsstaettenBerichtByJahrAndLocalId(jahrSchluessel, localId);
        return ResponseEntity.ok(mapper.toDto(bericht));
    }

    @Override
    public ResponseEntity<EuRegBetriebsstaetteDto> updateBetriebsstaettenBericht(
            EuRegBetriebsstaetteDto euRegBetriebsstaetteDto) {
        return ResponseEntity.ok(
                mapper.toDto(euRegistryService.updateBetriebsstaettenBericht(mapper.fromDto(euRegBetriebsstaetteDto))));
    }

    @Override
    public ResponseEntity<List<EuRegAnlageListItemDto>> readAllAnlagenBerichte(Long bstId) {
        var euRegBst = euRegistryService.loadBetriebsstaettenBerichtByBstId(bstId);
        Betriebsstaette b = euRegBst.getBetriebsstaette();
        return ResponseEntity.ok(anlagenTreeToListMapper.mapAnlagen(b.getAnlagen()));
    }

    @Override
    public ResponseEntity<EuRegAnlageDto> readAnlagenBericht(Long betriebsstaetteId, String anlagenNummer) {
        return ResponseEntity.ok(
                anlagenMapper.toDto(euRegistryService.loadAnlagenBericht(betriebsstaetteId, anlagenNummer)));
    }

    @Override
    public ResponseEntity<Void> euregDatenUebernehmen(UebernimmEuRegDatenInJahrCommandDto uebernimmCommand) {
        euRegistryService.uebernimmEuRegDaten(euRegCommandMapper.fromDto(uebernimmCommand));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> exportBerichtsdaten(ExportiereBerichtsdatenCommand exportiereBerichtsdatenCommand) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        importExportService.export(new BufferedOutputStream(out),
                exportiereBerichtsdatenCommand.getBetriebsstaettenBerichtIds());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType("application/xml"))
                             .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"euregistry.xml\"")
                             .body(new ByteArrayResource(out.toByteArray()));
    }

    @Override
    public ResponseEntity<List<ReferenzDto>> getAllYearsForBericht(Long id) {
        return ResponseEntity.ok(euRegistryService.getAllYearsForBetriebsstaetteId(id).map(referenzMapper::toDto).toList());
    }

    @Override
    public ResponseEntity<EuRegAnlageDto> updateAnlagenBericht(EuRegAnlageDto euRegAnlageDto) {
        return ResponseEntity.ok(
                anlagenMapper.toDto(euRegistryService.updateAnlagenBericht(anlagenMapper.fromDto(euRegAnlageDto))));
    }

    @Override
    public ResponseEntity<EuRegAnlageDto> readAnlagenteilBericht(Long betriebsstaetteId,
            String anlageNummer, String anlagenteilNummer) {
        return ResponseEntity.ok(anlagenMapper.toDto(
                euRegistryService.loadAnlagenteilBericht(betriebsstaetteId, anlageNummer, anlagenteilNummer)));
    }

    @Override
    public ResponseEntity<Void> runKomplexpruefung(KomplexpruefungDurchfuehrenCommand command) {
        List<Long> betriebsstaettenIds = euRegistryService.getBetriebsstaetteIdsForBerichtIds(command.getIds());
        eventPublisher.publishEvent(new BerichtPruefungEvent(betriebsstaettenIds, aktiverBenutzer.getUsername()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> komplexpruefungDownload(DownloadKomplexpruefungCommand command) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if(!stammdatenService.canReadBetriebsstaette(command.getBstId())) {
            outputStream.writeBytes("Betriebsstätte nicht gefunden oder keine Berechtigung zum Öffnen".getBytes());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(byteArrayResource);
        }
        try {
            berichtPruefungProtokollService.protokolliere(outputStream, command.getBstId());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());

            return ResponseEntity.ok()
                                 .contentType(MediaType.TEXT_PLAIN)
                                 .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"komplexpruefung.txt\"")
                                 .body(byteArrayResource);
        } catch (IOException e) {
            outputStream.writeBytes("Betriebsstätte nicht gefunden oder keine Berechtigung zum Öffnen".getBytes());

            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(byteArrayResource);
        }
    }

    @Override
    public ResponseEntity<RegelTestDto> testeRegel(TesteRegelCommand command) {
        BerichtPruefungService.TestErgebnis testErgebnis;
        testErgebnis = berichtPruefungService.regelTesten(command.getCode(),
                command.getRegelObjekte()
                       .stream()
                       .map(r -> RegelObjekt.valueOf(r.toString()))
                       .filter(objekt -> !TEILBERICHT.equals(objekt))
                       .toList(),
                command.getBerichtsjahrId(),
                Land.of(command.getLand()), command.getBetriebsstaetteNr());

        RegelTestDto regelTestDto = new RegelTestDto();
        regelTestDto.setStatus(RegelTestDto.StatusEnum.fromValue(testErgebnis.status.toString()));
        regelTestDto.setMessage(testErgebnis.message);

        return ResponseEntity.ok(regelTestDto);
    }


}
