package de.wps.bube.basis.koordinaten.domain.service;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

@Service
public class KoordinatenImportService {

    private final ApplicationEventPublisher eventPublisher;

    public KoordinatenImportService(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Secured(Rollen.GESAMTADMIN)
    public void triggerImport(String downloadLink, Referenz berichtsjahr) {
        eventPublisher.publishEvent(new VG250ImportTriggeredEvent(downloadLink, berichtsjahr));
    }

}
