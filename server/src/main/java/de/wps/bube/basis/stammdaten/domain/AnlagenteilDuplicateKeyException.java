package de.wps.bube.basis.stammdaten.domain;

import static java.lang.String.format;

import de.wps.bube.basis.base.persistence.DuplicateKeyException;

public class AnlagenteilDuplicateKeyException extends DuplicateKeyException {
    public AnlagenteilDuplicateKeyException(String message) {
        super(message);
    }

    public static AnlagenteilDuplicateKeyException forAnlagenteilNr(String nummer) {
        return new AnlagenteilDuplicateKeyException(
                format("Ein Anlagenteil mit der Nummer '%s' existiert bereits.", nummer));
    }
}
