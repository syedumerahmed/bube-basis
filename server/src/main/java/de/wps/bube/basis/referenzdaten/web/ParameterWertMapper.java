package de.wps.bube.basis.referenzdaten.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.model.ParameterWertDto;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Mapper(uses = { BehoerdenMapper.class })
public abstract class ParameterWertMapper {

    public abstract ParameterWertDto toDto(ParameterWert parameterWert);

    public abstract ParameterWert fromDto(ParameterWertDto parameterWertDto);

    static Gueltigkeitsjahr gueltigkeitsjahrFromDto(final int jahr) {
        return Gueltigkeitsjahr.of(jahr);
    }

    static int gueltigkeitsjahrToDto(final Gueltigkeitsjahr jahr) {
        return jahr.getJahr();
    }

    static Land landFromDto(final String landNr) {
        return Land.of(landNr);
    }

    static String landToDto(final Land land) {
        return land.getNr();
    }

}
