package de.wps.bube.basis.referenzdaten.domain.event;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;

public record BehoerdeGeloeschtEvent(Behoerde behoerde) {
}
