package de.wps.bube.basis.sicherheit.domain.security;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_BUND_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_BUND_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_LAND_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_LAND_WRITE;

import org.springframework.util.Assert;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

public class ReferenzenBerechtigungsContext {

    private final AktiverBenutzer aktiverBenutzer;

    public ReferenzenBerechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        Assert.notNull(aktiverBenutzer.getBerechtigung(), "berechtigung == null");
        this.aktiverBenutzer = aktiverBenutzer;
    }

    public boolean checkBerechtigungWrite(ReferenzBerechtigungsInMemoryAccess referenz) {
        return hatDatenberechtigung(referenz, aktiverBenutzer.isGesamtadmin(), aktiverBenutzer.getLand(),
                aktiverBenutzer.hatRolle(REFERENZLISTEN_BUND_WRITE),
                aktiverBenutzer.hatRolle(REFERENZLISTEN_LAND_WRITE));
    }

    public boolean checkBerechtigungDelete(ReferenzBerechtigungsInMemoryAccess referenz) {
        return hatDatenberechtigung(referenz, aktiverBenutzer.isGesamtadmin(), aktiverBenutzer.getLand(),
                aktiverBenutzer.hatRolle(REFERENZLISTEN_BUND_DELETE),
                aktiverBenutzer.hatRolle(REFERENZLISTEN_LAND_DELETE));
    }

    // Das Land darf nur für den Gesamtadmin vom Land des Bearbeiters abweichen
    public boolean checkLandZugriff(Land land) {
        return aktiverBenutzer.isGesamtadmin() || aktiverBenutzer.getLand().equals(land);
    }

    public boolean hatDatenberechtigung(ReferenzBerechtigungsInMemoryAccess referenz, boolean gesamtadmin, Land land,
            boolean bundRolle, boolean landRolle) {
        return gesamtadmin || referenz.isBundesreferenz() && bundRolle || referenz.getLand().equals(land) && landRolle;
    }
}
