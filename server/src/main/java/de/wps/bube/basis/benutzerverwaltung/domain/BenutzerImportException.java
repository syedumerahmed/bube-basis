package de.wps.bube.basis.benutzerverwaltung.domain;

/**
 * Exception für den Benutzer-Importieren-Job
 */
public class BenutzerImportException extends BenutzerException {
    public BenutzerImportException(String message) {
        super(message);
    }
}
