package de.wps.bube.basis.sicherheit.domain.security;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_DELETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_WRITE;

import java.util.Set;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.Assert;

import com.querydsl.core.BooleanBuilder;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

public class StammdatenBerechtigungsContext {

    private final AktiverBenutzer aktiverBenutzer;

    public StammdatenBerechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        Assert.notNull(aktiverBenutzer.getBerechtigung(), "berechtigung == null");
        this.aktiverBenutzer = aktiverBenutzer;
    }

    private BooleanBuilder createPredicate(BerechtigungsAccessor accessor, boolean checkDatenberechtigungen) {
        var datenberechtigung = aktiverBenutzer.getBerechtigung();
        BooleanBuilder builder = new BooleanBuilder(accessor.land(datenberechtigung.land));
        if (checkDatenberechtigungen) {
            builder.and(accessor.behoerdenIn(datenberechtigung.behoerden));
            builder.and(accessor.akzIn(datenberechtigung.akz));
            builder.and(accessor.verwaltungsgebieteIn(datenberechtigung.verwaltungsgebiete));
            builder.and(accessor.betriebsstaettenIn(datenberechtigung.betriebsstaetten));
        }
        return builder;
    }

    public BooleanBuilder createPredicateRead(BerechtigungsAccessor accessor) {
        return createPredicate(accessor, !aktiverBenutzer.hatRolle(LAND_READ));
    }

    public BooleanBuilder createPredicateWrite(BerechtigungsAccessor accessor) {
        return createPredicate(accessor, !aktiverBenutzer.hatRolle(LAND_WRITE));
    }

    public BooleanBuilder createPredicateDelete(BerechtigungsAccessor accessor) {
        return createPredicate(accessor, !aktiverBenutzer.hatRolle(LAND_DELETE));
    }

    private void checkAccessTo(BerechtigungsInMemoryAccessor accessor, boolean checkDatenberechtigungen) {
        String message = violatesBerechtigung(accessor, checkDatenberechtigungen);
        if (message != null) {
            throw new AccessDeniedException(message);
        }
    }

    public void checkAccessToRead(BerechtigungsInMemoryAccessor accessor) {
        checkAccessTo(accessor, !aktiverBenutzer.hatRolle(LAND_READ));
    }

    public void checkAccessToWrite(BerechtigungsInMemoryAccessor accessor) {
        checkAccessTo(accessor, !aktiverBenutzer.hatRolle(LAND_WRITE));
    }

    public boolean hasAccessToRead(BerechtigungsInMemoryAccessor accessor) {
        return violatesBerechtigung(accessor, !aktiverBenutzer.hatRolle(LAND_READ)) == null;
    }

    public boolean hasAccessToWrite(BerechtigungsInMemoryAccessor accessor) {
        return violatesBerechtigung(accessor, !aktiverBenutzer.hatRolle(LAND_WRITE)) == null;
    }

    public boolean hasAccessToDelete(BerechtigungsInMemoryAccessor accessor) {
        return violatesBerechtigung(accessor, !aktiverBenutzer.hatRolle(LAND_DELETE)) == null;
    }

    private String violatesBerechtigung(BerechtigungsInMemoryAccessor accessor, boolean checkDatenberechtigungen) {
        String message = "Keine Datenberechtigung aufgrund fehlender Zuweisung %s %s in der Benutzerverwaltung";
        var datenberechtigung = aktiverBenutzer.getBerechtigung();
        if (!(datenberechtigung.land.equals(Land.DEUTSCHLAND) || datenberechtigung.land.equals(accessor.getLand()))) {
            return String.format(message, "des Landes", accessor.getLand().getNr());
        }
        if (checkDatenberechtigungen) {
            if (!isSuperSet(datenberechtigung.behoerden, accessor.getBehoerden())) {
                return String.format(message, "der Behörde", String.join(",", accessor.getBehoerden()));
            }
            if (!isSuperSet(datenberechtigung.akz, accessor.getAkzSet())) {
                return String.format(message, "der Akz", String.join(",", accessor.getAkzSet()));
            }
            if (!isWithinVerwaltungsgebiet(accessor.getVerwaltungsgebiete())) {
                return String.format(message, "des Verwaltungsgebiets",
                        String.join(",", accessor.getVerwaltungsgebiete()));
            }
            if (!isSuperSet(datenberechtigung.betriebsstaetten, accessor.getBetriebsstaetten())) {
                return String.format(message, "der Betriebsstätte", String.join(",", accessor.getBetriebsstaetten()));
            }
        }
        return null;
    }

    /**
     * Prüft, ob mit gegebenen Bearbeiterberechtigungen auf die Entität zugegriffen werden kann
     *
     * @param bearbeiterBerechtigung Datenberechtigung des Bearbeiters; ein leeres Set steht für "unbeschränkt"
     * @param entityBerechtigung     Berechtigung der Entity muss eine Teilmenge sein und darf nur leer sein, wenn die
     *                               Bearbeiterberechtigung ebenfalls unbeschränkt ist.
     *
     * @return true, wenn gegebene Bearbeiterberechtigungen eine Obermenge der entityBerechtigung ist
     */
    private boolean isSuperSet(Set<String> bearbeiterBerechtigung, Set<String> entityBerechtigung) {
        return entityBerechtigung == null || bearbeiterBerechtigung.isEmpty() || (!entityBerechtigung.isEmpty() && bearbeiterBerechtigung
                .containsAll(entityBerechtigung));
    }

    private boolean isWithinVerwaltungsgebiet(Set<String> verwaltungsgebiete) {
        var berechtigteVerwaltungsgebiete = aktiverBenutzer.getBerechtigung().verwaltungsgebiete;
        return verwaltungsgebiete == null || berechtigteVerwaltungsgebiete.isEmpty() || !verwaltungsgebiete.isEmpty() && verwaltungsgebiete
                .stream()
                .allMatch(v -> berechtigteVerwaltungsgebiete.stream().anyMatch(v::startsWith));
    }
}
