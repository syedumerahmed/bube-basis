package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static org.apache.commons.lang3.ArrayUtils.shuffle;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

import java.security.SecureRandom;

import org.springframework.stereotype.Component;

/**
 * Generiert ein zufälliges Passwort, welches unseren derzeitigen Richtlinien entspricht:
 * <br/><br/>
 * 1 Großbuchstabe<br/>
 * 1 Kleinbuchstabe<br/>
 * 1 Zahl<br/>
 * 2 Sonderzeichen<br/>
 * 7 Zufällige alphanumerische Zeichen<br/>
 * = 12 Zeichen
 */
@Component
public class BubeKeycloakPasswordGenerator implements PasswordGenerator {
    @Override
    public String generatePassword() {
        var random = new SecureRandom();
        String upperCaseLetters = random(1, 'A', 'Z', true, true, null, random);
        String lowerCaseLetters = random(1, 'a', 'z', true, true, null, random);
        String numbers = randomNumeric(1);
        String specialChar = random(2, '!', '/', false, false, null, random);
        String totalChars = randomAlphanumeric(7);
        String combinedChars = upperCaseLetters + lowerCaseLetters + numbers + specialChar + totalChars;
        var chars = combinedChars.toCharArray();
        shuffle(chars, random);
        return new String(chars);
    }
}
