package de.wps.bube.basis.base.xml;

public class XMLExportException extends RuntimeException {
    public XMLExportException(String message, Exception e) {
        super(message, e);
    }

    public XMLExportException(String message) {
        super(message);
    }

    public XMLExportException(Exception e) {
        this("Es ist ein Fehler beim Erzeugen der XML-Datei aufgetreten", e);
    }
}
