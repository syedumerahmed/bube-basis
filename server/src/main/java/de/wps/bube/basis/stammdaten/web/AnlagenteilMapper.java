package de.wps.bube.basis.stammdaten.web;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.web.openapi.model.AnlagenteilDto;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapper;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

@Mapper(uses = { ReferenzMapper.class, BehoerdenMapper.class, VorschriftMapper.class })
public abstract class AnlagenteilMapper {

    @Mapping(target = "parentBetriebsstaetteId", ignore = true)
    public abstract AnlagenteilDto toDto(Anlagenteil anlagenteil);

    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    public abstract Anlagenteil fromDto(AnlagenteilDto dto);

}
