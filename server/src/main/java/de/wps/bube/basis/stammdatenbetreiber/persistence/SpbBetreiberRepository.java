package de.wps.bube.basis.stammdatenbetreiber.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;

public interface SpbBetreiberRepository extends JpaRepository<SpbBetreiber, Long>,
        QuerydslPredicateProjectionRepository<SpbBetreiber> {

    @Query("select b from SpbBetreiber b where b.betreiber.id = :betreiberId")
    Optional<SpbBetreiber> findByBetreiberId(@Param("betreiberId") Long betreiberId);
}
