package de.wps.bube.basis.benutzerverwaltung.xml;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.DatenberechtigungXDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;

@Mapper()
public interface DatenberechtigungXMLMapper extends XDtoMapper<DatenberechtigungXDto, Datenberechtigung> {

    @Override
    @Mapping(target = "betriebsstaetten.nummer", source = "betriebsstaetten")
    @Mapping(target = "fachmodule.fachmodul", source = "fachmodule")
    DatenberechtigungXDto toXDto(Datenberechtigung datenberechtigung);

    @Override
    default EntityWithError<Datenberechtigung> fromXDto(DatenberechtigungXDto datenberechtigungXDto) {
        return new EntityWithError<>(map(datenberechtigungXDto));
    }

    @Mapping(target = "behoerden", ignore = true)
    @Mapping(target = "akz", ignore = true)
    @Mapping(target = "verwaltungsgebiete", ignore = true)
    @Mapping(target = "themen", ignore = true)
    @Mapping(target = "betriebsstaetten", source = "betriebsstaetten.nummer")
    @Mapping(target = "fachmodule", source = "fachmodule.fachmodul")
    Datenberechtigung map(DatenberechtigungXDto datenberechtigungXDto);

    static Land landFromDto(String landNr) {
        return Land.of(landNr);
    }

    static String landToDto(Land land) {
        return land.getNr();
    }

}
