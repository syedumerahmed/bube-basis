package de.wps.bube.basis.referenzdaten.domain.entity;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

/**
 * Daten zur Referenzliste
 */
@Entity
@Table(name = "referenzliste")
public class ReferenzlisteMetadata {

    @Id
    @Enumerated(EnumType.STRING)
    private Referenzliste name;

    @NotNull
    private String ktext;

    @NotNull
    private String ltext;

    private boolean bundeseinheitlich;

    @Deprecated
    protected ReferenzlisteMetadata() {
    }

    public ReferenzlisteMetadata(Referenzliste name, String ktext, String ltext) {
        Assert.notNull(name, "name darf nicht null sein");
        Assert.notNull(ktext, "ktext darf nicht null sein");
        Assert.notNull(ltext, "ltext darf nicht null sein");
        this.name = name;
        this.ktext = ktext;
        this.ltext = ltext;
    }

    public Referenzliste getName() {
        return name;
    }

    public void setName(Referenzliste name) {
        this.name = name;
    }

    public String getKtext() {
        return ktext;
    }

    public void setKtext(String ktext) {
        this.ktext = ktext;
    }

    public String getLtext() {
        return ltext;
    }

    public void setLtext(String ltext) {
        this.ltext = ltext;
    }

    public boolean isBundeseinheitlich() {
        return bundeseinheitlich;
    }

    public void setBundeseinheitlich(boolean bundeseinheitlich) {
        this.bundeseinheitlich = bundeseinheitlich;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReferenzlisteMetadata that = (ReferenzlisteMetadata) o;
        return name == that.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
