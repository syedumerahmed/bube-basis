package de.wps.bube.basis.referenzdaten.domain.entity;

import de.wps.bube.basis.base.vo.Land;

public interface IReferenz {

    default boolean isBundesreferenz() {
        return isOfLand(Land.DEUTSCHLAND);
    }

    boolean isOfLand(Land land);

    void pruefeGueltigkeit();

}
