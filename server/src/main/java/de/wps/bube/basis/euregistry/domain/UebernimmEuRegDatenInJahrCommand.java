package de.wps.bube.basis.euregistry.domain;

import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public final class UebernimmEuRegDatenInJahrCommand {
    private final List<Long> berichtsIds;
    private final Referenz zieljahr;

    public UebernimmEuRegDatenInJahrCommand(List<Long> berichtsIds,
            Referenz zieljahr) {
        this.berichtsIds = berichtsIds;
        this.zieljahr = zieljahr;
    }

    public List<Long> getBerichtsIds() {
        return berichtsIds;
    }

    public Referenz getZieljahr() {
        return zieljahr;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        var that = (UebernimmEuRegDatenInJahrCommand) obj;
        return Objects.equals(this.berichtsIds, that.berichtsIds) &&
                Objects.equals(this.zieljahr, that.zieljahr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(berichtsIds, zieljahr);
    }

    @Override
    public String toString() {
        return "UebernimmEuRegDatenInJahrCommand[" +
                "berichtsIds=" + berichtsIds + ", " +
                "zieljahr=" + zieljahr + ']';
    }

}
