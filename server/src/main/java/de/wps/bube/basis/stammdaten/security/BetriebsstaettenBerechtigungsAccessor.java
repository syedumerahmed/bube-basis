package de.wps.bube.basis.stammdaten.security;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsAccessor;

@Component
public class BetriebsstaettenBerechtigungsAccessor implements BerechtigungsAccessor {

    @Override
    public Predicate land(Land land) {
        return land.equals(Land.DEUTSCHLAND) ? null : betriebsstaette.land.eq(land);
    }

    @Override
    public Predicate behoerdenIn(Set<String> behoerden) {
        return behoerden.isEmpty() ? null : betriebsstaette.zustaendigeBehoerde.schluessel.in(behoerden);
    }

    @Override
    public Predicate akzIn(Set<String> akz) {
        return akz.isEmpty() ? null : betriebsstaette.akz.in(akz);
    }

    @Override
    public Predicate verwaltungsgebieteIn(Set<String> verwaltungsgebiete) {
        return verwaltungsgebiete.isEmpty() ?
                null :
                verwaltungsgebiete.stream()
                                  .map(betriebsstaette.gemeindekennziffer.schluessel::startsWith)
                                  .collect(BooleanBuilder::new, BooleanBuilder::or, BooleanBuilder::or);
    }

    @Override
    public Predicate betriebsstaettenIn(Set<String> betriebsstaetten) {
        if (betriebsstaetten.isEmpty()) {
            return null;
        }
        List<String> upper = betriebsstaetten.stream().map(String::toUpperCase).toList();
        return betriebsstaette.betriebsstaetteNr.upper().in(upper);
    }
}
