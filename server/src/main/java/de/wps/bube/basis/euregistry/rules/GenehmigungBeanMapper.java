package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.euregistry.domain.entity.Genehmigung;
import de.wps.bube.basis.euregistry.rules.bean.GenehmigungBean;

@Mapper
public abstract class GenehmigungBeanMapper {

    public abstract GenehmigungBean toBean(Genehmigung genehmigung);

}
