package de.wps.bube.basis.euregistry.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.rules.bean.BerichtsdatenBean;
import de.wps.bube.basis.referenzdaten.rules.BehoerdeBeanMapper;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;

@Mapper(uses = { BehoerdeBeanMapper.class, ReferenzBeanMapper.class })
public abstract class BerichtsdatenBeanMapper {

    public abstract BerichtsdatenBean toBean(Berichtsdaten berichtsdaten);

}
