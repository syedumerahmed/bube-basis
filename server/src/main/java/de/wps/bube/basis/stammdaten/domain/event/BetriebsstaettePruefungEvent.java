package de.wps.bube.basis.stammdaten.domain.event;

import java.util.List;

public record BetriebsstaettePruefungEvent(List<Long> ids, String username) {

    public static final String EVENT_NAME = "Komplexprüfung";

}
