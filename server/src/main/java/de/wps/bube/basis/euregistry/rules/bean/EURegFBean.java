package de.wps.bube.basis.euregistry.rules.bean;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;

public class EURegFBean extends EURegAnlageBean {

    private final FeuerungsanlageBerichtsdatenBean feuerungsanlage;

    public EURegFBean(Long id, BerichtsdatenBean berichtsdaten,
            Instant ersteErfassung, Instant letzteAenderung, String localId2, Ausgangszustandsbericht report,
            int visits, String visitUrl, String eSpirsNr, String tehgNr, String monitor, String monitorUrl,
            List<ReferenzBean> kapitelIeRl, List<ReferenzBean> anwendbareBvt, List<BVTAusnahmeBean> bvtAusnahmen,
            List<AuflageBean> auflagen, GenehmigungBean genehmigung, FeuerungsanlageBerichtsdatenBean feuerungsanlage) {
        super(id, berichtsdaten, ersteErfassung, letzteAenderung, localId2, report, visits,
                visitUrl, eSpirsNr, tehgNr, monitor, monitorUrl, kapitelIeRl, anwendbareBvt, bvtAusnahmen, auflagen,
                genehmigung);
        this.feuerungsanlage = feuerungsanlage;
    }

    @RegelApi.Member(description = "Angaben 13./17. BImSchV")
    public FeuerungsanlageBerichtsdatenBean getFeuerungsanlage() {
        return feuerungsanlage;
    }

    @Override
    public RegelObjekt getRegelObjekt() {
        return RegelObjekt.EUREG_F;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EURegFBean that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        return Objects.equals(feuerungsanlage, that.feuerungsanlage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), feuerungsanlage);
    }

    @Override
    public String toString() {
        return "EURegFBean{" +
                "feuerungsanlage=" + feuerungsanlage +
                "} " + super.toString();
    }
}
