package de.wps.bube.basis.benutzerverwaltung.xml.dto;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

@XmlRootElement(name = BenutzerListeXDto.XML_ROOT_ELEMENT_NAME, namespace = BenutzerListeXDto.XSD_NAMESPACE)
public class BenutzerListeXDto {

    public static final String XML_ROOT_ELEMENT_NAME = "benutzerliste";
    public static final String XSD_SCHEMA_URL = "classpath:xsd/Benutzer_1.30.xsd";
    public static final String XSD_NAMESPACE = "http://basis.bube.wps.de/benutzer";
    public static final QName QUALIFIED_NAME = new QName(XSD_NAMESPACE, XML_ROOT_ELEMENT_NAME);

    @XmlElement
    public List<BenutzerType> benutzer;

}
