package de.wps.bube.basis.stammdaten.domain.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.security.SecuredStammdatenRead;

@Service
@Transactional(readOnly = true)
@SecuredStammdatenRead
public class BerichtsdatenPruefungService {

    private final StammdatenService stammdatenService;

    public BerichtsdatenPruefungService(StammdatenService stammdatenService) {
        this.stammdatenService = stammdatenService;
    }

    public boolean brauchtAnlageWarnungVorSpeicherung(Anlage anlageToBeSaved) {
        if (anlageToBeSaved.isNew()) {
            return false;
        }

        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(
                anlageToBeSaved.getParentBetriebsstaetteId());

        var bstBrauchteBerichtVorher = betriebsstaette.hatEURegBst();
        var anlage = betriebsstaette.sucheAnlage(anlageToBeSaved::equals)
                                    .orElseThrow(() -> new BubeEntityNotFoundException(Anlage.class, "ID " + anlageToBeSaved.getId()));
        var anlageBrauchteBerichtVorher = anlage.hatEURegAnl();

        if (anlage.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)) {
            return true;
        }

        // Update die Vorschriften als einziges EUReg-relevantes Feld
        anlage.setVorschriften(anlageToBeSaved.getVorschriften());

        return (bstBrauchteBerichtVorher && !betriebsstaette.hatEURegBst()) || (anlageBrauchteBerichtVorher && !anlage.hatEURegAnl());
    }

    public boolean brauchtAnlagenteilWarnungVorSpeicherung(Anlagenteil anlagenteilToBeSaved) {
        if (anlagenteilToBeSaved.isNew()) {
            return false;
        }

        Anlage parentAnage = stammdatenService.loadAnlage(anlagenteilToBeSaved.getParentAnlageId());
        Betriebsstaette betriebsstaette = stammdatenService.loadBetriebsstaetteForRead(
                parentAnage.getParentBetriebsstaetteId());

        var bstBrauchteBerichtVorher = betriebsstaette.hatEURegBst();
        var anlagenteil = betriebsstaette.sucheAnlagenteil(anlagenteilToBeSaved::equals)
                                         .orElseThrow(() -> new BubeEntityNotFoundException(Anlagenteil.class, "ID " + anlagenteilToBeSaved.getId()));
        var anlagenteilBrauchteBerichtVorher = anlagenteil.hatEURegAnl();

        if (anlagenteil.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlagenteilToBeSaved)) {
            return true;
        }

        // Update die Vorschriften als einziges EUReg-relevantes Feld
        anlagenteil.setVorschriften(anlagenteilToBeSaved.getVorschriften());

        return (bstBrauchteBerichtVorher && !betriebsstaette.hatEURegBst()) || (anlagenteilBrauchteBerichtVorher && !anlagenteil
                .hatEURegAnl());
    }

}
