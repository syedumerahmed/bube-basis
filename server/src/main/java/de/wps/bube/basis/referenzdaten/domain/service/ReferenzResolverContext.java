package de.wps.bube.basis.referenzdaten.domain.service;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public interface ReferenzResolverContext {
    Land getLand();

    Gueltigkeitsjahr getJahr();

    default void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
        throw new ReferenzdatenNotFoundException(referenzliste, schluessel, getLand(), getJahr());
    }

    default void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
        throw new ReferenzdatenNotFoundException(referenzliste, schluessel, getLand(), getJahr());
    }

    default void onBehoerdeNotFound(String schluessel) {
        throw new BubeEntityNotFoundException(Behoerde.class, schluessel);
    }

    default void onBehoerdeUngueltig(String schluessel) {
        throw new BubeEntityNotFoundException(Behoerde.class, schluessel);
    }
}
