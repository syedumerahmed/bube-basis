package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import com.google.common.base.Objects;

import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

public class GeoPunktVector {
    private GeoPunkt spl;
    private GeoPunkt spb;
    private boolean equal;

    public GeoPunktVector(GeoPunkt spl, GeoPunkt spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = Objects.equal(spl, spb);
    }

    public GeoPunkt getSpl() {
        return spl;
    }

    public void setSpl(GeoPunkt spl) {
        this.spl = spl;
    }

    public GeoPunkt getSpb() {
        return spb;
    }

    public void setSpb(GeoPunkt spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setIsEqual(boolean equal) {
        this.equal = equal;
    }
}
