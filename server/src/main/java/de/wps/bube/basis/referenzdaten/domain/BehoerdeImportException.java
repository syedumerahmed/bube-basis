package de.wps.bube.basis.referenzdaten.domain;

public class BehoerdeImportException extends RuntimeException {
    public BehoerdeImportException() {
        super("Abbruch des Imports aufgrund von Fehlern. Siehe Protokoll");
    }
}
