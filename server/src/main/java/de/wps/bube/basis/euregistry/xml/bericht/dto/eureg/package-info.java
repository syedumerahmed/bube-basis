@javax.xml.bind.annotation.XmlSchema(
        namespace = SchemaSet.SCHEMA_EUREG,
        elementFormDefault = XmlNsForm.QUALIFIED)
package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlNsForm;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
