package de.wps.bube.basis.euregistry.rules.bean;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATE;

import java.time.LocalDate;
import java.util.Objects;

import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.referenzdaten.rules.bean.ReferenzBean;

public class BVTAusnahmeBean {

    private final ReferenzBean emissionswertAusnahme;
    private final LocalDate beginntAm;
    private final LocalDate endetAm;
    private final String oeffentlicheBegruendungUrl;

    public BVTAusnahmeBean(ReferenzBean emissionswertAusnahme, LocalDate beginntAm, LocalDate endetAm,
            String oeffentlicheBegruendungUrl) {
        this.emissionswertAusnahme = emissionswertAusnahme;
        this.beginntAm = beginntAm;
        this.endetAm = endetAm;
        this.oeffentlicheBegruendungUrl = oeffentlicheBegruendungUrl;
    }

    @RegelApi.Member(description = "Ausnahme nach Art. 15(4) 2010-75-EU (IE-RL) §7(1b), §12(1b) BImSchG: BVT-assoziierter Emissionswert", referenzliste = RBATE)
    public ReferenzBean getEmissionswertAusnahme() {
        return emissionswertAusnahme;
    }

    @RegelApi.Member(description = "Datum Beginn der Regelung")
    public LocalDate getBeginntAm() {
        return beginntAm;
    }

    @RegelApi.Member(description = "Datum Ende der Regelung")
    public LocalDate getEndetAm() {
        return endetAm;
    }

    @RegelApi.Member(description = "URL öffentliche Begründung, nach Art. 24(2)(f) IE-RL")
    public String getOeffentlicheBegruendungUrl() {
        return oeffentlicheBegruendungUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BVTAusnahmeBean that = (BVTAusnahmeBean) o;
        return Objects.equals(emissionswertAusnahme, that.emissionswertAusnahme) && Objects.equals(
                beginntAm, that.beginntAm) && Objects.equals(endetAm, that.endetAm) && Objects.equals(
                oeffentlicheBegruendungUrl, that.oeffentlicheBegruendungUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emissionswertAusnahme, beginntAm, endetAm, oeffentlicheBegruendungUrl);
    }

    @Override
    public String toString() {
        return "BVTAusnahmeBean{" +
                "emissionswertAusnahme=" + emissionswertAusnahme +
                ", beginntAm=" + beginntAm +
                ", endetAm=" + endetAm +
                ", oeffentlicheBegruendungUrl='" + oeffentlicheBegruendungUrl + '\'' +
                '}';
    }

}