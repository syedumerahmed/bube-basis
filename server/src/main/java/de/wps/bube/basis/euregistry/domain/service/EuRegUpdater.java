package de.wps.bube.basis.euregistry.domain.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;

@Mapper
public interface EuRegUpdater {

    @Mapping(target = "betriebsstaette", ignore = true)
    @Mapping(target = "komplexpruefung", ignore = true)
    void update(@MappingTarget EURegBetriebsstaette target, EURegBetriebsstaette source);

    @Mapping(target = "berichtsart", ignore = true)
    @Mapping(target = "anlage", ignore = true)
    @Mapping(target = "anlagenteil", ignore = true)
    @Mapping(target = "genehmigungsbehoerden", ignore = true)
    @Mapping(target = "ueberwachungsbehoerden", ignore = true)
    void update(@MappingTarget EURegAnlage target, EURegAnlage source);
}
