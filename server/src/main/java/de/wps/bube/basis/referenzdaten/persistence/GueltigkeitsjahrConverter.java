package de.wps.bube.basis.referenzdaten.persistence;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

@Converter(autoApply = true)
public class GueltigkeitsjahrConverter implements AttributeConverter<Gueltigkeitsjahr, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final Gueltigkeitsjahr gueltigkeitsjahr) {
        if (gueltigkeitsjahr == null) {
            return null;
        }
        return gueltigkeitsjahr.getJahr();
    }

    @Override
    public Gueltigkeitsjahr convertToEntityAttribute(final Integer berichtsjahr) {
        if (berichtsjahr == null) {
            return null;
        }

        return Gueltigkeitsjahr.of(berichtsjahr);
    }
}