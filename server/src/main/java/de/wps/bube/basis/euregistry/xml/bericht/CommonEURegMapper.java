package de.wps.bube.basis.euregistry.xml.bericht;

import static org.apache.commons.lang3.StringUtils.isBlank;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import de.wps.bube.basis.euregistry.domain.XeurTeilberichtException;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base.Identifier;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

@Mapper
public abstract class CommonEURegMapper {

    @Autowired
    KoordinatenTransformationService koordinatenTransformationService;

    @Mapping(target = "code", source = ".")
    @Mapping(target = "schema", constant = SchemaSet.REASON_VALUE_CODES)
    @Named("reasonValue")
    public abstract ReferenceType mapVertraulichkeitsgrund(Referenz referenz);

    public String mapToEu(Referenz referenz) {
        if (referenz == null) {
            return null;
        }
        if (isBlank(referenz.getEu())) {
            throw new XeurTeilberichtException("Fehlender EU-Wert für " + referenz);
        }
        return referenz.getEu();
    }

    @Mapping(target = "srsDimension", constant = "2")
    @Mapping(target = "pos", source = ".")
    @Mapping(target = "gmlId", ignore = true) // TODO
    public abstract Point mapGeoPunkt(GeoPunkt geoPunkt);

    String transformPos(GeoPunkt geoPunkt) {
        double[] coordinates = koordinatenTransformationService.transformToEPSG4258(geoPunkt);
        return coordinates[0] + " " + coordinates[1];
    }

    public InspireID mapInspireID(EURegAnlage euRegAnlage,
            @Context ProductionInstallationMapperContext context) {
        var inspireID = new InspireID();
        var identifier = new Identifier();
        var inspireParam = context.getInspireParam();
        identifier.setNamespace(inspireParam.getNamespace());
        String localId;

        if (inspireParam.getExtension() != null) {
            localId = euRegAnlage.getLocalId() + inspireParam.getExtension();
        } else {
            Assert.notNull(euRegAnlage.getLocalId2(),
                    "LocalId2 muss angegeben sein, wenn Parameter 'insp_extension' leer ist");
            localId = euRegAnlage.getLocalId2();
        }
        identifier.setLocalId(localId);
        inspireID.setIdentifier(identifier);
        return inspireID;
    }
}
