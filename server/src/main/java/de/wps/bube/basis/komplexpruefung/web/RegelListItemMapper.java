package de.wps.bube.basis.komplexpruefung.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.RegelListItemDto;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

@Mapper
public interface RegelListItemMapper {

    RegelListItemDto toDto(Regel regel);

    static int gueltigkeitsjahrToDto(Gueltigkeitsjahr jahr) {
        return jahr.getJahr();
    }

    static String landToDto(Land land) {
        return land.getNr();
    }

    static String gruppeToDto(RegelGruppe gruppe) {
        return gruppe.getBezeichner();
    }

    static String objektToDto(RegelObjekt objekt) {
        return objekt.getBezeichner();
    }

    static String typToDto(RegelTyp typ) {
        return typ.getBezeichner();
    }
}
