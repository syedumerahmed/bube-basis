package de.wps.bube.basis.euregistry.xml.bericht.dto.eureg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;

@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureName {

    @XmlElement(required = true, name = "nameOfFeature")
    private String nameOfFeature;

    @XmlElement(required = false, name = "confidentialityReason")
    private ReferenceType confidentialityReason;

    public String getNameOfFeature() {
        return nameOfFeature;
    }

    public void setNameOfFeature(String nameOfFeature) {
        this.nameOfFeature = nameOfFeature;
    }

    public ReferenceType getConfidentialityReason() {
        return confidentialityReason;
    }

    public void setConfidentialityReason(ReferenceType confidentialityReason) {
        this.confidentialityReason = confidentialityReason;
    }
}
