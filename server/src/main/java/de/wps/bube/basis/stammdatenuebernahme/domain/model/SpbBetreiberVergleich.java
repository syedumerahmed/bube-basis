package de.wps.bube.basis.stammdatenuebernahme.domain.model;

public class SpbBetreiberVergleich {
    private Long id;
    private Long spbId;
    private boolean hasChanges;
    private AdresseVergleich adresse;
    private StringVector name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public AdresseVergleich getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseVergleich adresse) {
        this.adresse = adresse;
    }

    public StringVector getName() {
        return name;
    }

    public void setName(StringVector name) {
        this.name = name;
    }

    public boolean isHasChanges() {
        return hasChanges;
    }

    public void setHasChanges(boolean hasChanges) {
        this.hasChanges = hasChanges;
    }
}
