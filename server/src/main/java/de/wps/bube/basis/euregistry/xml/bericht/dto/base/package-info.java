@javax.xml.bind.annotation.XmlSchema(
        namespace = SchemaSet.SCHEMA_BASE,
        elementFormDefault = XmlNsForm.QUALIFIED)
package de.wps.bube.basis.euregistry.xml.bericht.dto.base;

import javax.xml.bind.annotation.XmlNsForm;

import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
