package de.wps.bube.basis.sicherheit.domain.vo;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

import org.springframework.util.Assert;

import de.wps.bube.basis.base.vo.Land;

public final class Datenberechtigung {

    public final static String LAND = "Land";
    public final static String BEHOERDEN = "Behoerden";
    public final static String AKZ = "AKZ";
    public final static String VERWALTUNGSGEBIETE = "Verwaltungsgebiete";
    public final static String BETRIEBSSTAETTEN = "Betriebsstaetten";
    public final static String FACHMODULE = "Fachmodule";
    public final static String THEMEN = "Themen";

    public final Land land;
    public final Set<String> behoerden;
    public final Set<String> akz;
    public final Set<String> verwaltungsgebiete;
    public final Set<String> betriebsstaetten;
    public final Set<Fachmodul> fachmodule;
    public final Set<Thema> themen;

    public Datenberechtigung(Land land, Set<String> behoerden, Set<String> akz, Set<String> verwaltungsgebiete,
            Set<String> betriebsstaetten, Set<Fachmodul> fachmodule, Set<Thema> themen) {
        Assert.notNull(land, "Land muss als Benutzerattribut angegeben werden");
        this.land = land;
        this.behoerden = behoerden != null ? Set.copyOf(behoerden) : Collections.emptySet();
        this.akz = akz != null ? Set.copyOf(akz) : Collections.emptySet();
        this.verwaltungsgebiete = verwaltungsgebiete != null ? Set.copyOf(verwaltungsgebiete) : Collections.emptySet();
        this.betriebsstaetten = betriebsstaetten != null ? Set.copyOf(betriebsstaetten) : Collections.emptySet();
        this.fachmodule = fachmodule != null ? Set.copyOf(fachmodule) : Collections.emptySet();
        this.themen = themen != null ? Set.copyOf(themen) : Collections.emptySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Datenberechtigung that)) {
            return false;
        }
        return Objects.equals(land, that.land)
                && Objects.equals(behoerden, that.behoerden)
                && Objects.equals(akz, that.akz)
                && Objects.equals(verwaltungsgebiete, that.verwaltungsgebiete)
                && Objects.equals(betriebsstaetten, that.betriebsstaetten)
                && Objects.equals(fachmodule, that.fachmodule)
                && Objects.equals(themen, that.themen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(land, behoerden, akz, verwaltungsgebiete, betriebsstaetten, fachmodule, themen);
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", Datenberechtigung.class.getSimpleName() + "[", "]").add(
                "land='" + land + "'");
        if (!behoerden.isEmpty()) {
            joiner.add("behoerden=" + behoerden);
        }
        if (!akz.isEmpty()) {
            joiner.add("akz=" + akz);
        }
        if (!verwaltungsgebiete.isEmpty()) {
            joiner.add("verwaltungsgebiete=" + verwaltungsgebiete);
        }
        if (!betriebsstaetten.isEmpty()) {
            joiner.add("betriebsstaetten=" + betriebsstaetten);
        }
        if (!fachmodule.isEmpty()) {
            joiner.add("fachmodule=" + fachmodule);
        }
        if (!themen.isEmpty()) {
            joiner.add("themen=" + themen);
        }
        return joiner.toString();

    }

    public static class DatenberechtigungBuilder {
        private final Land land;
        private Set<String> behoerden;
        private Set<String> akz;
        private Set<String> verwaltungsgebiete;
        private Set<String> betriebsstaetten;
        private Set<Fachmodul> fachmodule;
        private Set<Thema> themen;

        private DatenberechtigungBuilder(Land land) {
            this.land = land;
        }

        public static DatenberechtigungBuilder land(Land land) {
            return new DatenberechtigungBuilder(land);
        }

        public DatenberechtigungBuilder behoerden(Set<String> behoerden) {
            this.behoerden = behoerden;
            return this;
        }

        public DatenberechtigungBuilder behoerden(String... behoerden) {
            return behoerden(Set.of(behoerden));
        }

        public DatenberechtigungBuilder akz(Set<String> akz) {
            this.akz = akz;
            return this;
        }

        public DatenberechtigungBuilder akz(String... akz) {
            return akz(Set.of(akz));
        }

        public DatenberechtigungBuilder verwaltungsgebiete(Set<String> verwaltungsgebiete) {
            this.verwaltungsgebiete = verwaltungsgebiete;
            return this;
        }

        public DatenberechtigungBuilder verwaltungsgebiete(String... verwaltungsgebiete) {
            return verwaltungsgebiete(Set.of(verwaltungsgebiete));
        }

        public DatenberechtigungBuilder betriebsstaetten(Set<String> betriebsstaetten) {
            this.betriebsstaetten = betriebsstaetten;
            return this;
        }

        public DatenberechtigungBuilder betriebsstaetten(String... betriebsstaetten) {
            return betriebsstaetten(Set.of(betriebsstaetten));
        }

        public DatenberechtigungBuilder fachmodule(Set<Fachmodul> fachmodule) {
            this.fachmodule = fachmodule;
            return this;
        }

        public DatenberechtigungBuilder fachmodule(Fachmodul... fachmodule) {
            return fachmodule(Set.of(fachmodule));
        }

        public DatenberechtigungBuilder themen(Set<Thema> themen) {
            this.themen = themen;
            return this;
        }

        public DatenberechtigungBuilder themen(Thema... themen) {
            return themen(Set.of(themen));
        }

        public Datenberechtigung build() {
            return new Datenberechtigung(land, behoerden, akz, verwaltungsgebiete, betriebsstaetten, fachmodule,
                    themen);
        }
    }
}
