package de.wps.bube.basis.stammdatenbetreiber.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.wps.bube.basis.base.web.openapi.api.SpbAnlagenApi;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlageDto;
import de.wps.bube.basis.base.web.openapi.model.SpbAnlageListItemDto;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;

@RestController
class SpbAnlagenRestController implements SpbAnlagenApi {

    private final StammdatenBetreiberService service;
    private final SpbAnlageMapper mapper;

    public SpbAnlagenRestController(StammdatenBetreiberService service, SpbAnlageMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<SpbAnlageListItemDto>> listAllSpbAnlagen(Long betriebsstaetteId) {
        return ResponseEntity.ok(service.listAllSpbAnlagenAsListItemByBetriebsstaettenId(betriebsstaetteId)
                                        .stream()
                                        .map(mapper::toListItemDto)
                                        .collect(
                                                Collectors.toList()));
    }

    @Override
    public ResponseEntity<List<SpbAnlageListItemDto>> listAllNeueSpbAnlagen(Long betriebsstaetteId) {
        return ResponseEntity.ok(service.listAllNeueSpbAnlagenAsListItemByBetriebsstaettenId(betriebsstaetteId)
                                        .stream()
                                        .map(mapper::toListItemDto)
                                        .collect(
                                                Collectors.toList()));
    }

    @Override
    public ResponseEntity<SpbAnlageDto> createSpbAnlage(@Valid SpbAnlageDto spbAnlageDto) {
        return ResponseEntity.ok(mapper.toDto(service.createSpbAnlage(mapper.fromDto(spbAnlageDto))));
    }

    @Override
    public ResponseEntity<SpbAnlageDto> readSpbAnlage(Long betriebsstaetteId, String anlagenNummer) {
        return ResponseEntity.ok(mapper.toDto(service.loadSpbAnlage(betriebsstaetteId, anlagenNummer)));
    }

    @Override
    public ResponseEntity<SpbAnlageDto> updateSpbAnlage(@Valid SpbAnlageDto spbAnlageDto) {
        return ResponseEntity.ok(mapper.toDto(service.updateSpbAnlage(mapper.fromDto(spbAnlageDto))));
    }

    @Override
    public ResponseEntity<Void> deleteSpbAnlage(Long id) {
        service.deleteSpbAnlageByID(id);
        return ResponseEntity.ok().build();
    }

}
