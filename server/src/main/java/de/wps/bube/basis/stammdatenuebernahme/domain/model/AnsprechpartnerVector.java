package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;

public class AnsprechpartnerVector {
    private Ansprechpartner spl;
    private Ansprechpartner spb;
    private boolean equal;

    public AnsprechpartnerVector(Ansprechpartner spl, Ansprechpartner spb) {
        this.spl = spl;
        this.spb = spb;
        this.equal = eq(spl, spb);
    }

    public Ansprechpartner getSpl() {
        return spl;
    }

    public void setSpl(Ansprechpartner spl) {
        this.spl = spl;
    }

    public Ansprechpartner getSpb() {
        return spb;
    }

    public void setSpb(Ansprechpartner spb) {
        this.spb = spb;
    }

    public boolean isEqual() {
        return equal;
    }

    public void setEqual(boolean equal) {
        this.equal = equal;
    }

    private boolean eq(Ansprechpartner o, Ansprechpartner that) {
        return (o == null && that == null) || (o != null && that != null && o.equalValues(that));
    }
}
