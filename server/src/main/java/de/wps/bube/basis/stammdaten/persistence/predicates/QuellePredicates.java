package de.wps.bube.basis.stammdaten.persistence.predicates;

import static de.wps.bube.basis.stammdaten.domain.entity.QQuelle.quelle;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.stammdaten.domain.vo.QuelleColumn;

public class QuellePredicates {

    public static Predicate parentBetriebsstaetteIdEquals(final long betriebsstaetteId) {
        return quelle.parentBetriebsstaetteId.eq(betriebsstaetteId);
    }

    public static Predicate parentAnlageIdEquals(final long anlageId) {
        return quelle.parentAnlageId.eq(anlageId);
    }

    public static Predicate mapQuelleFiltersToPredicate(
            final List<ListStateDto.FilterStateDto<QuelleColumn>> filters) {
        if (filters == null) {
            return null;
        }
        return createPredicateByQuelleColumns(
                filters.stream().collect(Collectors.toMap(f -> f.property, f -> f.value)));
    }

    public static Predicate createPredicateByQuelleColumns(final Map<QuelleColumn, String> columns) {
        if (columns == null || columns.isEmpty()) {
            return null;
        }
        final BooleanBuilder booleanBuilder = new BooleanBuilder();
        columns.forEach((property, value) -> booleanBuilder.and(property.getExpression().containsIgnoreCase(value)));
        return booleanBuilder;
    }
}
