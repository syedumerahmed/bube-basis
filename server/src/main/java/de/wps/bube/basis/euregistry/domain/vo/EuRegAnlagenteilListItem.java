package de.wps.bube.basis.euregistry.domain.vo;

public class EuRegAnlagenteilListItem {

    public Long id;
    public String name;
    public String anlagenteilNr;
    public String betriebsstatus;
    public String berichtsart;

}
