package de.wps.bube.basis.komplexpruefung.domain.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.expression.EvaluationException;

import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;

public class RegelPruefungErgebnis {

    private final List<PruefObjekt> gueltigeObjekte = new ArrayList<>();
    private final List<PruefObjekt> ungueltigeObjekte = new ArrayList<>();
    private final Map<PruefObjekt, EvaluationException> pruefungsFehler = new HashMap<>();

    public List<PruefObjekt> getGueltigeObjekte() {
        return gueltigeObjekte;
    }

    public List<PruefObjekt> getUngueltigeObjekte() {
        return ungueltigeObjekte;
    }

    public Map<PruefObjekt, EvaluationException> getPruefungsFehler() {
        return pruefungsFehler;
    }

    public void add(PruefObjekt objekt, boolean ergebnis) {
        (ergebnis ? gueltigeObjekte : ungueltigeObjekte).add(objekt);
    }

    public void addFehler(PruefObjekt objekt, EvaluationException exception) {
        pruefungsFehler.put(objekt, exception);
    }

}
