package de.wps.bube.basis.desktop.domain.entity;

import static javax.persistence.EnumType.STRING;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;

import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;

@Embeddable
public class DesktopItemId implements Serializable {

    private String username;

    private Long itemId;

    @Enumerated(STRING)
    private DesktopTyp desktopTyp;

    public DesktopItemId(String username, Long itemId, DesktopTyp desktopTyp) {
        this.username = username;
        this.itemId = itemId;
        this.desktopTyp = desktopTyp;
    }

    @Deprecated
    protected DesktopItemId() {
    }

    public String getUsername() {
        return username;
    }

    public Long getItemId() {
        return itemId;
    }

    public DesktopTyp getDesktopTyp() {
        return desktopTyp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DesktopItemId that = (DesktopItemId) o;
        return username.equals(that.username) && itemId.equals(
                that.itemId) && desktopTyp == that.desktopTyp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, itemId, desktopTyp);
    }
}
