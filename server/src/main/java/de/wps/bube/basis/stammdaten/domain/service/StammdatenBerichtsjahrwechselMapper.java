package de.wps.bube.basis.stammdaten.domain.service;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.control.DeepClone;
import org.mapstruct.control.NoComplexMapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverContext;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;

@Mapper(mappingControl = DeepClone.class)
public abstract class StammdatenBerichtsjahrwechselMapper {

    @Autowired
    private ReferenzResolverService referenzResolverService;

    public EntityWithError<Betriebsstaette> copyBetriebsstaetteToBerichtsjahr(Betriebsstaette origin, Referenz jahr,
            Betreiber neuerBetreiber) {
        var errors = new ArrayList<MappingError>();

        var betriebsstaette = copyBetriebsstaetteToBerichtsjahr(origin,
                new StammdatenBerichtsjahrwechselMapper.MappingContext(origin.getLand(),
                        Gueltigkeitsjahr.of(jahr.getSchluessel()), origin.getName(), origin.getBetriebsstaetteNr(),
                        errors::add));
        betriebsstaette.setBetreiber(neuerBetreiber);
        betriebsstaette.setBerichtsjahr(jahr);
        return new EntityWithError<>(betriebsstaette, errors);
    }

    @Mapping(target = "letzteAenderung", expression = "java(null)")
    @Mapping(target = "letzteAenderungBetreiber", expression = "java(null)")
    @Mapping(target = "schreibsperreBetreiber", constant = "false")
    @Mapping(target = "betreiberdatenUebernommen", constant = "false")
    @Mapping(target = "id", expression = "java(null)")
    @Mapping(target = "betreiber", ignore = true)
    @Mapping(target = "berichtsjahr", ignore = true)
    @Mapping(target = "einleiterNummern", mappingControl = NoComplexMapping.class)
    @Mapping(target = "abfallerzeugerNummern", mappingControl = NoComplexMapping.class)
    @Mapping(target = "pruefungsfehler", constant = "false")
    @Mapping(target = "pruefungVorhanden", constant = "false")
    @Mapping(target = "komplexpruefung", ignore = true)
    @Mapping(target = "berichtsobjekte", ignore = true)
    @Mapping(target = "prtrVorschriften", ignore = true)
    @Mapping(target = "prtrTaetigkeitenOhneHaupttaetigkeit", ignore = true)
    abstract Betriebsstaette copyBetriebsstaetteToBerichtsjahr(Betriebsstaette betriebsstaette,
            @Context MappingContext context);

    @Mapping(target = "letzteAenderung", expression = "java(null)")
    @Mapping(target = "letzteAenderungBetreiber", expression = "java(null)")
    @Mapping(target = "betreiberdatenUebernommen", constant = "false")
    @Mapping(target = "id", expression = "java(null)")
    @Mapping(target = "parentBetriebsstaetteId", expression = "java(null)")
    abstract Anlage copyToBerichtsjahr(Anlage anlage, @Context MappingContext context);

    @Mapping(target = "letzteAenderung", expression = "java(null)")
    @Mapping(target = "letzteAenderungBetreiber", expression = "java(null)")
    @Mapping(target = "betreiberdatenUebernommen", constant = "false")
    @Mapping(target = "id", expression = "java(null)")
    @Mapping(target = "parentAnlageId", expression = "java(null)")
    abstract Anlagenteil copyToBerichtsjahr(Anlagenteil anlagenteil, @Context MappingContext context);

    @Mapping(target = "id", expression = "java(null)")
    abstract Ansprechpartner copyToBerichtsjahr(Ansprechpartner anlagenteil, @Context MappingContext context);

    @Mapping(target = "letzteAenderung", expression = "java(null)")
    @Mapping(target = "letzteAenderungBetreiber", expression = "java(null)")
    @Mapping(target = "betreiberdatenUebernommen", constant = "false")
    @Mapping(target = "id", expression = "java(null)")
    @Mapping(target = "parentAnlageId", expression = "java(null)")
    @Mapping(target = "parentBetriebsstaetteId", expression = "java(null)")
    abstract Quelle copyToBerichtsjahr(Quelle quelle, @Context MappingContext context);

    Referenz copyToBerichtsjahr(Referenz referenz, @Context MappingContext context) {
        if (referenz == null) {
            return null;
        }
        if (referenz.gueltigFuer(context.jahr)) {
            return referenz;
        }
        return referenzResolverService.loadReferenz(referenz.getReferenzliste(), referenz.getSchluessel(), context);
    }

    Behoerde copyToBerichtsjahr(Behoerde behoerde, @Context MappingContext context) {
        if (behoerde == null) {
            return null;
        }
        if (behoerde.gueltigFuer(context.jahr)) {
            return behoerde;
        }
        return referenzResolverService.loadBehoerde(behoerde.getSchluessel(), context);
    }

    static class MappingContext implements ReferenzResolverContext {
        private final Land land;
        private final Gueltigkeitsjahr jahr;
        private final String bstName, bstNummer;
        private final Consumer<MappingError> errorConsumer;

        MappingContext(Land land, Gueltigkeitsjahr jahr,
                String bstName, String bstNummer, Consumer<MappingError> errorConsumer) {
            this.land = land;
            this.jahr = jahr;
            this.bstName = bstName;
            this.bstNummer = bstNummer;
            this.errorConsumer = errorConsumer;
        }

        @Override
        public Land getLand() {
            return land;
        }

        @Override
        public Gueltigkeitsjahr getJahr() {
            return jahr;
        }

        @Override
        public void onReferenzNotFound(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s wurde nicht gefunden", referenzliste.name(), schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onReferenzUngueltig(Referenzliste referenzliste, String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Referenz %s mit Schlüssel %s ist ungültig", referenzliste.name(), schluessel), true);
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeNotFound(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s wurde nicht gefunden", schluessel));
            errorConsumer.accept(error);
        }

        @Override
        public void onBehoerdeUngueltig(String schluessel) {
            var error = new MappingError(format("Betriebsstätte %s Nr. %s", bstName, bstNummer),
                    format("Behörde mit Schlüssel %s ist ungültig", schluessel), true);
            errorConsumer.accept(error);
        }
    }
}
