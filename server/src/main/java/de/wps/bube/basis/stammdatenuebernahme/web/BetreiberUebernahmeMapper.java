package de.wps.bube.basis.stammdatenuebernahme.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.BetreiberUebernahmeDto;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetreiberUebernahme;

@Mapper
public abstract class BetreiberUebernahmeMapper {

    public abstract BetreiberUebernahme fromDto(BetreiberUebernahmeDto betreiberUebernahmeDto);
}
