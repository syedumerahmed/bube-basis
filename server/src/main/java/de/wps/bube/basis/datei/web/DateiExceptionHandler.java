package de.wps.bube.basis.datei.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.datei.domain.DateiException;
import de.wps.bube.basis.datei.domain.UploadException;

@ControllerAdvice
public class DateiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ UploadException.class })
    protected ResponseEntity<Object> handleUploadException(UploadException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ DateiException.class })
    protected ResponseEntity<Object> handleDateiException(DateiException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

}
