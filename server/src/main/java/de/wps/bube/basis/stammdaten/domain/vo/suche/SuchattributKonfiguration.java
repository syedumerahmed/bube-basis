package de.wps.bube.basis.stammdaten.domain.vo.suche;

import java.util.Objects;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

public enum SuchattributKonfiguration {
    BETRIEBSSTAETTEN_LAND(new StringFilter(b -> b.land.stringValue(), a -> a.betriebsstaette.land)),
    BETRIEBSSTAETTEN_NUMMER(new StringFilter(b -> b.betriebsstaetteNr, a -> a.betriebsstaette.nummer)),
    BETRIEBSSTAETTEN_LOCAL_ID(new StringFilter(b -> b.localId, a -> a.betriebsstaette.localId)),
    NAME(new StringFilter(b -> b.name, a -> a.betriebsstaette.name)),
    PLZ(new StringFilter(b -> b.adresse.plz, a -> a.betriebsstaette.plz)),
    ORT(new StringFilter(b -> b.adresse.ort, a -> a.betriebsstaette.ort)),
    ZUSTAENDIGE_BEHOERDE(new BehoerdeFilter(b -> b.zustaendigeBehoerde, a -> a.betriebsstaette.behoerdeIn)),
    AKZ(new StringFilter(b -> b.akz, a -> a.betriebsstaette.akz)),
    BETRIEBSSTATUS(new ReferenzFilter(b -> b.betriebsstatus, a -> a.betriebsstaette.statusIn)),
    NACE(new ReferenzFilter(b -> b.naceCode, a -> a.betriebsstaette.naceIn)),
    GEMEINDE(new ReferenzFilter(b -> b.gemeindekennziffer, a -> a.betriebsstaette.gemeindeIn)),
    UEBERNOMMEN(new BooleanFilter(b -> b.betreiberdatenUebernommen, a -> a.betriebsstaette.uebernommen)),

    BETREIBER_NAME(new StringFilter(b -> b.betreiber.name, a -> a.betreiber.name)),
    BETREIBER_PLZ(new StringFilter(b -> b.betreiber.adresse.plz, a -> a.betreiber.plz)),
    BETREIBER_ORT(new StringFilter(b -> b.betreiber.adresse.ort, a -> a.betreiber.ort)),

    ANLAGE_NUMMER(AnlageOrAnlagenteilFilter.forStringExpression(
            b -> b.anlagen.any().anlageNr,
            b -> b.anlagen.any().anlagenteile.any().anlagenteilNr,
            a -> a.anlage.nummer)),
    ANLAGE_LOCAL_ID(AnlageOrAnlagenteilFilter.forStringExpression(
            b -> b.anlagen.any().localId,
            b -> b.anlagen.any().anlagenteile.any().localId,
            a -> a.anlage.localId)),
    ANLAGE_NAME(AnlageOrAnlagenteilFilter.forStringExpression(
            b -> b.anlagen.any().name,
            b -> b.anlagen.any().anlagenteile.any().name,
            a -> a.anlage.name)),
    ANLAGE_BETRIEBSSTATUS(AnlageOrAnlagenteilFilter.forReferenz(
            b -> b.anlagen.any().betriebsstatus,
            b -> b.anlagen.any().anlagenteile.any().betriebsstatus,
            a -> a.anlage.statusIn)),
    ANLAGE_UEBERNOMMEN(AnlageOrAnlagenteilFilter.forBooleanExpression(
            b -> b.anlagen.any().betreiberdatenUebernommen,
            b -> b.anlagen.any().anlagenteile.any().betreiberdatenUebernommen,
            a -> a.anlage.uebernommen)),
    ANLAGE_ZUSTAENDIGKEITEN(AnlageOrAnlagenteilFilter.forReferenz(
            b -> b.anlagen.any().zustaendigeBehoerden.any().zustaendigkeit,
            b -> b.anlagen.any().anlagenteile.any().zustaendigeBehoerden.any().zustaendigkeit,
            a -> a.anlage.zustaendigkeitIn)),
    ANLAGE_BEHOERDEN(AnlageOrAnlagenteilFilter.forBehoerde(
            b -> b.anlagen.any().zustaendigeBehoerden.any().behoerde,
            b -> b.anlagen.any().anlagenteile.any().zustaendigeBehoerden.any().behoerde,
            a -> a.anlage.behoerdeIn)),
    ANLAGE_VORSCHRIFTEN(new AnlagenMitAllenVorschriftenFilter(
            b -> b.anlagen.any().vorschriften,
            b -> b.anlagen.any().anlagenteile.any().vorschriften,
            a -> a.anlage.vorschriftenIn)),
    ANLAGE_4BIMSCHV_TAETIGKEITEN(AnlageOrAnlagenteilFilter.forReferenz(
            b -> b.anlagen.any().vorschriften.any().taetigkeiten.any(),
            b -> b.anlagen.any().anlagenteile.any().vorschriften.any().taetigkeiten.any(),
            a -> a.anlage.taetigkeiten.nr4BImSchV)),
    ANLAGE_PRTR_TAETIGKEITEN(AnlageOrAnlagenteilFilter.forReferenz(
            b -> b.anlagen.any().vorschriften.any().taetigkeiten.any(),
            b -> b.anlagen.any().anlagenteile.any().vorschriften.any().taetigkeiten.any(),
            a -> a.anlage.taetigkeiten.prtr)),
    ANLAGE_IERL_TAETIGKEITEN(AnlageOrAnlagenteilFilter.forReferenz(
            b -> b.anlagen.any().vorschriften.any().taetigkeiten.any(),
            b -> b.anlagen.any().anlagenteile.any().vorschriften.any().taetigkeiten.any(),
            a -> a.anlage.taetigkeiten.ierl));

    private final Filter filter;

    SuchattributKonfiguration(Filter filter) {
        this.filter = Objects.requireNonNull(filter);
    }

    public Predicate createPredicate(Suchattribute suchattribute, QBetriebsstaette bstRoot) {
        return filter.createPredicate(suchattribute, bstRoot);
    }

}
