package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.Assert;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Anlage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "anlage_seq")
    @SequenceGenerator(name = "anlage_seq", allocationSize = 50)
    private Long id;

    private String localId;
    private String referenzAnlagenkataster;

    @NotNull
    private String anlageNr;

    // Eine Anlage hat eine Eltern Betriebsstätte
    private Long parentBetriebsstaetteId;

    // hat GeoPunkt 0..1
    @Embedded
    private GeoPunkt geoPunkt;

    // hat Vorschriften 0..n
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Vorschrift> vorschriften;

    // hat Leistungen 0..n
    @ElementCollection
    private List<Leistung> leistungen;

    // hat ZBehoerde // zuständige Bhd. 0..n
    @ElementCollection
    private List<ZustaendigeBehoerde> zustaendigeBehoerden;

    @NotNull
    private String name;

    @ManyToOne
    private Referenz vertraulichkeitsgrund;

    @ManyToOne
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;
    private String bemerkung;

    private boolean betreiberdatenUebernommen;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentAnlageId", referencedColumnName = "id")
    @OrderBy("quelleNr ASC")
    private List<Quelle> quellen;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentAnlageId", referencedColumnName = "id")
    @OrderBy("anlagenteilNr ASC")
    private List<Anlagenteil> anlagenteile;

    @CreatedDate
    private Instant ersteErfassung;

    @LastModifiedDate
    private Instant letzteAenderung;

    private Instant letzteAenderungBetreiber;

    @Deprecated
    protected Anlage() {
    }

    public Anlage(String anlageNr, Long parentBetriebsstaetteId, String name) {
        Assert.notNull(anlageNr, "anlageNr darf nicht null sein");
        Assert.notNull(name, "name darf nicht null sein");
        Assert.isTrue(!name.isEmpty(), "name darf nicht leer sein");
        this.anlageNr = anlageNr;
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenzAnlagenkataster() {
        return referenzAnlagenkataster;
    }

    public void setReferenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getAnlageNr() {
        return anlageNr;
    }

    public void setAnlageNr(String anlageNr) {
        this.anlageNr = anlageNr;
    }

    public GeoPunkt getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public List<Vorschrift> getVorschriften() {
        if (vorschriften == null) {
            return Collections.emptyList();
        }
        return vorschriften;
    }

    public void setVorschriften(List<Vorschrift> vorschriften) {
        this.vorschriften = vorschriften;
    }

    public List<Leistung> getLeistungen() {
        return leistungen;
    }

    public void setLeistungen(List<Leistung> leistungen) {
        this.leistungen = leistungen;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Referenz getVertraulichkeitsgrund() {
        return vertraulichkeitsgrund;
    }

    public void setVertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
    }

    public Referenz getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDate getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDate getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public Instant getErsteErfassung() {
        return ersteErfassung;
    }

    public void setErsteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
    }

    public Instant getLetzteAenderung() {
        return letzteAenderung;
    }

    public void setLetzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
    }

    public Long getParentBetriebsstaetteId() {
        return parentBetriebsstaetteId;
    }

    public void setParentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
    }

    public List<Quelle> getQuellen() {
        if (quellen == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(quellen);
    }

    public void setQuellen(List<Quelle> quellen) {
        this.quellen = quellen;
    }

    public List<Anlagenteil> getAnlagenteile() {
        if (anlagenteile == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(anlagenteile);
    }

    public void setAnlagenteile(List<Anlagenteil> anlagenteile) {
        this.anlagenteile = anlagenteile;
    }

    public List<ZustaendigeBehoerde> getZustaendigeBehoerden() {
        return zustaendigeBehoerden;
    }

    public void setZustaendigeBehoerden(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        this.zustaendigeBehoerden = zustaendigeBehoerden;
    }

    public boolean isBetreiberdatenUebernommen() {
        return betreiberdatenUebernommen;
    }

    public void setBetreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
    }

    public Instant getLetzteAenderungBetreiber() {
        return letzteAenderungBetreiber;
    }

    public void setLetzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
    }

    /**
     * Prüft erst die id und wenn die null ist den fachlichen Key
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Anlage anlage = (Anlage) o;

        return id != null ? id.equals(anlage.id) : this.hasSameKey(anlage);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isNew() {
        return id == null;
    }

    public boolean hasSameKey(Anlage other) {
        return Objects.equals(parentBetriebsstaetteId, other.parentBetriebsstaetteId) && anlageNr.equals(
                other.anlageNr);
    }

    public boolean hasSameLocalIdOrNull(Anlage other) {
        return other.localId == null || Objects.equals(localId, other.localId);
    }

    public Stream<Vorschrift> alleVorschriften() {
        return Stream.concat(getVorschriften().stream(),
                getAnlagenteile().stream().flatMap(Anlagenteil::alleVorschriften));
    }

    public boolean hatEURegAnl() {
        return getVorschriften().stream()
                                .map(Vorschrift::getArt)
                                .map(Referenz::getSchluessel)
                                .filter(VorschriftSchluessel::schluesselExist)
                                .map(VorschriftSchluessel::ofSchluessel)
                                .anyMatch(VorschriftSchluessel::brauchtAnlageBericht);
    }

    public boolean isFeuerungsanlage() {
        return getVorschriften().stream()
                                .map(Vorschrift::getArt)
                                .map(Referenz::getSchluessel)
                                .filter(VorschriftSchluessel::schluesselExist)
                                .map(VorschriftSchluessel::ofSchluessel)
                                .anyMatch(VorschriftSchluessel::brauchtFeuerungsanlageBericht);
    }

    public boolean isFeuerungsanlageMit17BV() {
        return getVorschriften().stream()
                                .map(Vorschrift::getArt)
                                .map(Referenz::getSchluessel)
                                .filter(VorschriftSchluessel::schluesselExist)
                                .map(VorschriftSchluessel::ofSchluessel)
                                .anyMatch(VorschriftSchluessel::is17BV);
    }

    public boolean isFeuerungsanlageMit13BV() {
        return getVorschriften().stream()
                                .map(Vorschrift::getArt)
                                .map(Referenz::getSchluessel)
                                .filter(VorschriftSchluessel::schluesselExist)
                                .map(VorschriftSchluessel::ofSchluessel)
                                .anyMatch(VorschriftSchluessel::is13BV);
    }

    public boolean pruefeLoeschungFeuerungsanlageBerichtsdaten(Anlage anlageToBeSaved) {
        if (!this.isFeuerungsanlage()) {
            return false;
        }

        // Berichtsdaten für Feuerungsanlage
        if (this.isFeuerungsanlage() && !anlageToBeSaved.isFeuerungsanlage()) {
            return true;
        }

        // Berichtsdaten für Speziellen Bedingungen
        if (this.isFeuerungsanlageMit17BV() && !anlageToBeSaved.isFeuerungsanlageMit17BV()) {
            return true;
        }

        // Wird nur die 13.BV gelöscht und die 17.BV ist noch vorhanden,
        // so wird die Liste der IE-Ausnahmen (nach Art 31-35 IE-RL) leer gemacht.
        return (this.isFeuerungsanlageMit13BV() && this.isFeuerungsanlageMit17BV()) &&
               (!anlageToBeSaved.isFeuerungsanlageMit13BV() && anlageToBeSaved.isFeuerungsanlageMit17BV());
    }

    public Double getGesamtwaermeleistung() {
        for (Leistung leistung : leistungen) {
            if (leistung.istGenehmigt() && leistung.isMegaWatt() && leistung.isFWL()) {
                return leistung.getLeistung();
            }
        }

        for (Leistung leistung : leistungen) {
            if (leistung.istInstalliert() && leistung.isMegaWatt() && leistung.isFWL()) {
                return leistung.getLeistung();
            }
        }

        for (Leistung leistung : leistungen) {
            if (leistung.wirdBetrieben() && leistung.isMegaWatt() && leistung.isFWL()) {
                return leistung.getLeistung();
            }
        }

        return 0.0;
    }

    public Vorschrift getIERLVorschrift() {
        return vorschriften.stream().filter(Vorschrift::isIERL).findFirst().orElse(null);
    }

    public List<Behoerde> getUeberwachungsbehoerden() {
        return zustaendigeBehoerden.stream()
                                   .filter(ZustaendigeBehoerde::isUeberwachungsbehoerde)
                                   .map(ZustaendigeBehoerde::getBehoerde).collect(Collectors.toList());
    }

    public List<Behoerde> getGenehmigungsbehoerden() {
        return zustaendigeBehoerden.stream()
                                   .filter(ZustaendigeBehoerde::isGenehmigungsbehoerde)
                                   .map(ZustaendigeBehoerde::getBehoerde).collect(Collectors.toList());
    }


    public boolean hasDoppelteVorschriften() {
        if(getVorschriften().stream().distinct().count() != getVorschriften().size()){
            return true;
        }
        for ( Anlagenteil anlagenteil : getAnlagenteile() ) {
            if (anlagenteil.hasDoppelteVorschriften()) {
                return true;
            }
        }
        return false;
    }

    public void markiereAlsUebernommen() {
        this.betreiberdatenUebernommen = true;
        this.letzteAenderungBetreiber = null;
    }
}
