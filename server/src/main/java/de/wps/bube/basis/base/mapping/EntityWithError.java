package de.wps.bube.basis.base.mapping;

import static java.util.Collections.emptyList;

import java.util.List;

public class EntityWithError<T> {

    public final T entity;
    private final List<MappingError> errors;

    public EntityWithError(T entity) {
        this.entity = entity;
        this.errors = null;
    }

    public EntityWithError(T entity, List<MappingError> errors) {
        this.entity = entity;
        this.errors = errors;
    }

    public List<MappingError> errorsOrEmpty() {
        if (errors == null) {
            return emptyList();
        }
        return errors;
    }
}
