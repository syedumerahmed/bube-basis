package de.wps.bube.basis.stammdaten.web;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import de.wps.bube.basis.base.web.openapi.api.AnlagenApi;
import de.wps.bube.basis.base.web.openapi.model.AnlageDto;
import de.wps.bube.basis.base.web.openapi.model.AnlageListItemDto;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsdatenPruefungService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Controller
class AnlagenRestController implements AnlagenApi {

    private final StammdatenService stammdatenService;
    private final AnlageMapper mapper;
    private final BerichtsdatenPruefungService berichtsdatenPruefungService;

    public AnlagenRestController(StammdatenService stammdatenService, AnlageMapper mapper,
            BerichtsdatenPruefungService berichtsdatenPruefungService) {
        this.stammdatenService = stammdatenService;
        this.mapper = mapper;
        this.berichtsdatenPruefungService = berichtsdatenPruefungService;
    }

    @Override
    public ResponseEntity<List<AnlageListItemDto>> readAllAnlage(Long betriebsstaetteId) {

        List<AnlageListItemDto> list = stammdatenService.loadAllAnlagenByBetriebsstaettenId(betriebsstaetteId).stream()
                                                        .map(anlage -> {
                                                            AnlageListItemDto dto = new AnlageListItemDto();
                                                            dto.setBetriebsstatus(anlage.getBetriebsstatus() == null ?
                                                                    null :
                                                                    anlage.getBetriebsstatus().getSchluessel());
                                                            dto.setId(anlage.getId());
                                                            dto.setName(anlage.getName());
                                                            dto.setNummer(anlage.getAnlageNr());
                                                            dto.setBetreiberdatenUebernommen(anlage.isBetreiberdatenUebernommen());
                                                            dto.setLetzteAenderungBetreiber(
                                                                    anlage.getLetzteAenderungBetreiber() == null ?
                                                                            null :
                                                                            anlage.getLetzteAenderungBetreiber().toString());
                                                            return dto;
                                                        }).collect(Collectors.toList());
        return ResponseEntity.ok(list);
    }

    @Override
    public ResponseEntity<AnlageDto> createAnlage(AnlageDto anlageDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.createAnlage(mapper.fromDto(anlageDto))));
    }

    @Override
    public ResponseEntity<AnlageDto> readAnlage(Long betriebsstaetteId, String anlagenNummer) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.loadAnlageByAnlageNummer(betriebsstaetteId, anlagenNummer)));
    }

    @Override
    public ResponseEntity<AnlageDto> updateAnlage(AnlageDto anlageDto) {
        return ResponseEntity.ok(mapper.toDto(stammdatenService.updateAnlage(mapper.fromDto(anlageDto))));
    }

    @Override
    public ResponseEntity<Void> deleteAnlage(Long id) {
        stammdatenService.deleteAnlage(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultipleAnlagen(List<Long> ids) {
        stammdatenService.deleteMultipleAnlagen(ids);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Boolean> anlageBrauchtWarnungVorSpeicherung(AnlageDto anlageDto) {
        return ResponseEntity.ok(
                berichtsdatenPruefungService.brauchtAnlageWarnungVorSpeicherung(mapper.fromDto(anlageDto)));
    }
}
