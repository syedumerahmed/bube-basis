package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import java.util.List;

public class SpbAnlagenteilVergleich {
    private Long id;
    private Long spbId;
    private boolean hasChanges;
    private StringVector name;
    private StringVector anlagenteilNr;
    private GeoPunktVector geoPunkt;
    private List<LeistungVector> leistungen;
    private ReferenzVector betriebsstatus;
    private LocalDateVector betriebsstatusSeit;
    private LocalDateVector inbetriebnahme;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpbId() {
        return spbId;
    }

    public void setSpbId(Long spbId) {
        this.spbId = spbId;
    }

    public StringVector getName() {
        return name;
    }

    public void setName(StringVector name) {
        this.name = name;
    }

    public StringVector getAnlagenteilNr() {
        return anlagenteilNr;
    }

    public void setAnlagenteilNr(StringVector anlagenteilNr) {
        this.anlagenteilNr = anlagenteilNr;
    }

    public GeoPunktVector getGeoPunkt() {
        return geoPunkt;
    }

    public void setGeoPunkt(GeoPunktVector geoPunkt) {
        this.geoPunkt = geoPunkt;
    }

    public List<LeistungVector> getLeistungen() {
        return leistungen;
    }

    public void setLeistungen(List<LeistungVector> leistungen) {
        this.leistungen = leistungen;
    }

    public ReferenzVector getBetriebsstatus() {
        return betriebsstatus;
    }

    public void setBetriebsstatus(ReferenzVector betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
    }

    public LocalDateVector getBetriebsstatusSeit() {
        return betriebsstatusSeit;
    }

    public void setBetriebsstatusSeit(LocalDateVector betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
    }

    public LocalDateVector getInbetriebnahme() {
        return inbetriebnahme;
    }

    public void setInbetriebnahme(LocalDateVector inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
    }

    public boolean isHasChanges() {
        return hasChanges;
    }

    public void setHasChanges(boolean hasChanges) {
        this.hasChanges = hasChanges;
    }
}
