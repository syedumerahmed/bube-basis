package de.wps.bube.basis.desktop.domain.vo;

public enum DesktopItemTyp {
    BETRIEBSSTAETTE, ANLAGE, QUELLE, EUREG_BST, EUREG_ANL, EUREG_F
}
