package de.wps.bube.basis.stammdaten.rules;

import org.mapstruct.Mapper;

import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.rules.bean.VorschriftBean;

@Mapper(uses = ReferenzBeanMapper.class)
public abstract class VorschriftBeanMapper {

    public abstract VorschriftBean toBean(Vorschrift vorschrift);

}
