package de.wps.bube.basis.stammdaten.rules;

import java.util.function.Supplier;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapper;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.AnlagenteilBean;

@Mapper(uses = { GeoPunktBeanMapper.class, VorschriftBeanMapper.class, LeistungBeanMapper.class,
        ZustaendigeBehoerdeBeanMapper.class,
        ReferenzBeanMapper.class })
public abstract class AnlagenteilBeanMapper {

    @Autowired
    protected StammdatenService stammdatenService;

    @Mapping(target = "anlage", expression = "java(anlage(anlagenteil, context, mappers))")
    @Mapping(target = "vorgaenger", expression = "java(vorgaenger(anlagenteil, context, mappers))")
    @Mapping(target = "nachfolger", expression = "java(nachfolger(anlagenteil, context, mappers))")
    public abstract AnlagenteilBean toBean(Anlagenteil anlagenteil, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers);

    protected Supplier<AnlageBean> anlage(Anlagenteil anlagenteil, @Context BeanMappingContext context,
            @Context StammdatenBeanMappers mappers) {
        return () -> {
            Anlage anlage = stammdatenService.loadAnlage(anlagenteil.getParentAnlageId());
            return mappers.getAnlageBeanMapper().toBean(anlage, context, mappers);
        };
    }

    protected Supplier<AnlagenteilBean> vorgaenger(Anlagenteil anlagenteil, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            AnlageBean anlage = anlage(anlagenteil, context, mappers).get();
            AnlageBean vorgaenger = anlage.getVorgaenger();
            return vorgaenger == null || anlagenteil.getLocalId() == null ?
                    null :
                    stammdatenService.findAnlagenteilByLocalId(vorgaenger.getId(),
                                             vorgaenger.getParentBetriebsstaetteId(), anlagenteil.getLocalId())
                                     .map(a -> mappers.getAnlagenteilBeanMapper()
                                                      .toBean(a, context.vorgaenger(), mappers))
                                     .orElse(null);
        };
    }

    protected Supplier<AnlagenteilBean> nachfolger(Anlagenteil anlagenteil, BeanMappingContext context,
            StammdatenBeanMappers mappers) {
        return () -> {
            AnlageBean anlage = anlage(anlagenteil, context, mappers).get();
            AnlageBean nachfolger = anlage.getNachfolger();
            return nachfolger == null || anlagenteil.getLocalId() == null ?
                    null :
                    stammdatenService.findAnlagenteilByLocalId(nachfolger.getId(),
                                             nachfolger.getParentBetriebsstaetteId(), anlagenteil.getLocalId())
                                     .map(a -> mappers.getAnlagenteilBeanMapper()
                                                      .toBean(a, context.nachfolger(), mappers))
                                     .orElse(null);
        };
    }

}
