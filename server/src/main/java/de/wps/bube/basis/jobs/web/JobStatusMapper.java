package de.wps.bube.basis.jobs.web;

import org.mapstruct.Mapper;

import de.wps.bube.basis.base.web.openapi.model.JobStatusDto;
import de.wps.bube.basis.jobs.domain.JobStatus;

@Mapper(uses = JobActionMapper.class)
public abstract class JobStatusMapper {

    abstract JobStatusDto toDto(JobStatus jobStatus);
}
