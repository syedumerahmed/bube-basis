package de.wps.bube.basis.komplexpruefung.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.wps.bube.basis.komplexpruefung.domain.RegelException;
import de.wps.bube.basis.koordinaten.domain.KoordinatenException;

@ControllerAdvice
public class KomplexpruefungExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RegelException.class)
    protected ResponseEntity<Object> handleRegelException(RegelException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(KoordinatenException.class)
    protected ResponseEntity<Object> handleKoordinatenException(KoordinatenException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
    }

}
