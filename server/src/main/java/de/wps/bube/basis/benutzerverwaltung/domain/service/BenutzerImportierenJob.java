package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerImportJobStatus;
import de.wps.bube.basis.benutzerverwaltung.domain.persistence.BenutzerImportJobStatusRepository;
import de.wps.bube.basis.benutzerverwaltung.xml.BenutzerXMLMapper;
import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import de.wps.bube.basis.benutzerverwaltung.xml.dto.BenutzerListeXDto;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.DateTime;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@Service
@Transactional
public class BenutzerImportierenJob {

    public static final String JOB_NAME = "Benutzer importieren";

    private static final Logger LOGGER = LoggerFactory.getLogger(BenutzerImportierenJob.class);

    private static final String PROTOKOLL_OPENING = "Bitte Protokoll bis zum Ende durchlesen, um das Ergebnis des Benutzer-Imports zu erfahren.";

    private static final int COUNT_SIZE = 10;

    private final BenutzerverwaltungImportExportService benutzerImportExportService;
    private final BenutzerverwaltungService benutzerService;
    private final StammdatenService stammdatenService;
    private final BenutzerJobRegistry jobRegistry;
    private final XMLService xmlService;
    private final DateiService dateiService;
    private final BenutzerXMLMapper benutzerXMLMapper;
    private final BenutzerImportJobStatusRepository benutzerImportJobStatusRepository;

    public BenutzerImportierenJob(BenutzerverwaltungImportExportService benutzerImportExportService,
            BenutzerverwaltungService benutzerService,
            StammdatenService stammdatenService,
            BenutzerJobRegistry jobRegistry, XMLService xmlService,
            DateiService dateiService, BenutzerXMLMapper benutzerXMLMapper,
            BenutzerImportJobStatusRepository benutzerImportJobStatusRepository) {
        this.benutzerImportExportService = benutzerImportExportService;
        this.benutzerService = benutzerService;
        this.stammdatenService = stammdatenService;
        this.jobRegistry = jobRegistry;
        this.xmlService = xmlService;
        this.dateiService = dateiService;
        this.benutzerXMLMapper = benutzerXMLMapper;
        this.benutzerImportJobStatusRepository = benutzerImportJobStatusRepository;
    }

    @Async
    public void runAsync(String username) {

        JobProtokoll jobProtokoll = new JobProtokoll(PROTOKOLL_OPENING);
        jobProtokoll.addEintrag(format("%s Start: %s Benutzer: %s", JOB_NAME, DateTime.nowCET(), username));

        try {
            Set<String> betriebsstaettenNummern = Set.copyOf(stammdatenService.allBetriebsstaettenNrForCurrentUser());
            Set<String> allBetreiberBenutzer = benutzerService.listAllBetreiberBenutzer()
                                                              .map(Benutzer::getBenutzername)
                                                              .collect(toSet());
            AtomicInteger count = new AtomicInteger();
            InputStream inputStream = dateiService.getDateiStream(username, DateiScope.BENUTZER);
            List<String> benutzerNamen = new ArrayList<>();
            xmlService.read(inputStream, BenutzerType.class, benutzerXMLMapper, BenutzerListeXDto.XML_ROOT_ELEMENT_NAME,
                    null, benutzer -> {
                        var imported = benutzerImportExportService.importiereBenutzer(benutzer.entity,
                                betriebsstaettenNummern, allBetreiberBenutzer, jobProtokoll);
                        if (imported.getEmail() == null) {
                            benutzerNamen.add(imported.getBenutzername());
                        }
                        if (count.incrementAndGet() % COUNT_SIZE == 0) {
                            jobRegistry.jobCountChanged(COUNT_SIZE);
                        }
                    });

            jobRegistry.jobCountChanged(count.get() % COUNT_SIZE);
            if (benutzerNamen.isEmpty()) {
                jobRegistry.jobCompleted(false);
            } else {
                jobRegistry.jobCompleted(true);
                jobProtokoll.addWarnung(format("Es wurden Benutzer ohne E-Mail-Adresse angelegt: %s",
                        String.join(", ", benutzerNamen)));
                jobProtokoll.addWarnung("Bitte generieren Sie die Einmalpasswörter!");
            }
            BenutzerImportJobStatus jobStatus
                    = benutzerImportJobStatusRepository.findById(username).get();
            jobStatus.setBenutzerNamen(benutzerNamen);
            benutzerImportJobStatusRepository.save(jobStatus);
            jobProtokoll.addEintrag(
                    format("%s Ende: %s Anzahl Benutzer: %d Laufzeit Sekunden: %d", JOB_NAME, DateTime.nowCET(),
                            jobStatus.getCount(), jobStatus.getRuntimeSec()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            jobRegistry.jobError(e);
            jobProtokoll.addFehler(format("%s Fehler: %s", JOB_NAME, DateTime.nowCET()), e.getMessage());
        } finally {
            // Datei wird gelöscht, egal ob der Job durchlief oder nicht
            benutzerImportExportService.dateiLoeschen(username);
            //Protokoll Speichern
            jobRegistry.jobProtokoll(jobProtokoll.getProtokoll());
        }
    }
}
