package de.wps.bube.basis.referenzdaten.domain.entity;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;

public class BehoerdeBuilder {

    private static final Instant NOW = Instant.now();
    private Long id;
    private Land land;
    private String schluessel;
    private String kreis;
    private String bezeichnung;
    private String bezeichnung2;
    private Adresse adresse;
    private List<Kommunikationsverbindung> kommunikationsverbindungen;
    private Set<Zustaendigkeit> zustaendigkeiten;
    private Gueltigkeitsjahr gueltigVon;
    private Gueltigkeitsjahr gueltigBis;
    private Instant letzteAenderung;

    private BehoerdeBuilder() {
    }

    public static BehoerdeBuilder aBehoerde(String schluessel) {
        return new BehoerdeBuilder().land(Land.NORDRHEIN_WESTFALEN)
                                    .schluessel(schluessel)
                                    .kreis("123")
                                    .bezeichnung(schluessel)
                                    .bezeichnung2(schluessel)
                                    .gueltigVon(Gueltigkeitsjahr.of(2007))
                                    .gueltigBis(Gueltigkeitsjahr.of(2099))
                                    .letzteAenderung(NOW)
                                    .adresse(AdresseBuilder.anAdresse().build());
    }

    public static BehoerdeBuilder aFullBehoerde(String schluessel) {
        return new BehoerdeBuilder().land(Land.NORDRHEIN_WESTFALEN)
                                    .schluessel(schluessel)
                                    .kreis("123")
                                    .bezeichnung(schluessel)
                                    .bezeichnung2(schluessel + "-2")
                                    .gueltigVon(Gueltigkeitsjahr.of(2007))
                                    .gueltigBis(Gueltigkeitsjahr.of(2099))
                                    .letzteAenderung(NOW)
                                    .adresse(AdresseBuilder.aFullAdresse().build())
                                    .kommunikationsverbindungen(
                                            new Kommunikationsverbindung(
                                                    ReferenzBuilder
                                                            .aKommunikationsTypReferenz(
                                                                    Kommunikationsverbindung.EMAIL_SCHLUESSEL)
                                                            .build(),
                                                    schluessel + "@gmail.de"),
                                            new Kommunikationsverbindung(
                                                    ReferenzBuilder
                                                            .aKommunikationsTypReferenz(
                                                                    Kommunikationsverbindung.TELEFON_SCHLUESSEL)
                                                            .build(),
                                                    "040 76234151")
                                    );
    }

    public BehoerdeBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public BehoerdeBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public BehoerdeBuilder schluessel(String schluessel) {
        this.schluessel = schluessel;
        return this;
    }

    public BehoerdeBuilder kreis(String kreis) {
        this.kreis = kreis;
        return this;
    }

    public BehoerdeBuilder bezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
        return this;
    }

    public BehoerdeBuilder bezeichnung2(String bezeichnung2) {
        this.bezeichnung2 = bezeichnung2;
        return this;
    }

    public BehoerdeBuilder adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public BehoerdeBuilder kommunikationsverbindungen(List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
        return this;
    }

    public BehoerdeBuilder kommunikationsverbindungen(Kommunikationsverbindung... kommunikationsverbindungen) {
        return kommunikationsverbindungen(List.of(kommunikationsverbindungen));
    }

    public BehoerdeBuilder zustaendigkeiten(Zustaendigkeit... zustaendigkeiten) {
        return zustaendigkeiten(Set.of(zustaendigkeiten));
    }

    public BehoerdeBuilder zustaendigkeiten(Set<Zustaendigkeit> zustaendigkeiten) {
        this.zustaendigkeiten = zustaendigkeiten;
        return this;
    }

    public BehoerdeBuilder gueltigVon(int gueltigVon) {
        return gueltigVon(Gueltigkeitsjahr.of(gueltigVon));
    }

    public BehoerdeBuilder gueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
        return this;
    }

    public BehoerdeBuilder gueltigBis(int gueltigBis) {
        return gueltigBis(Gueltigkeitsjahr.of(gueltigBis));
    }

    public BehoerdeBuilder gueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
        return this;
    }

    public BehoerdeBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public Behoerde build() {
        var b = new Behoerde();
        b.setId(id);
        b.setLand(land);
        b.setSchluessel(schluessel);
        b.setKreis(kreis);
        b.setBezeichnung(bezeichnung);
        b.setBezeichnung2(bezeichnung2);
        b.setAdresse(adresse);
        b.setKommunikationsverbindungen(kommunikationsverbindungen);
        b.setZustaendigkeiten(zustaendigkeiten);
        b.setGueltigVon(gueltigVon);
        b.setGueltigBis(gueltigBis);
        b.setLetzteAenderung(letzteAenderung);
        return b;
    }
}
