package de.wps.bube.basis.komplexpruefung.domain.entity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.komplexpruefung.domain.RegelException;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.base.vo.Land;

class RegelTest {

    @Test
    void pruefeObjekteInGruppe_A_OK() {
        var regel = RegelBuilder.aRegel()
                                .objekte(List.of(RegelObjekt.BST, RegelObjekt.ANLAGE, RegelObjekt.BETREIBER,
                                        RegelObjekt.ANLAGENTEIL, RegelObjekt.QUELLE))
                                .gruppe(RegelGruppe.GRUPPE_A)
                                .build();
        regel.pruefeObjekteInGruppe();
    }

    @Test
    void pruefeObjekteInGruppe_A_NotOK() {
        var regel = RegelBuilder.aRegel()
                                .objekte(List.of(RegelObjekt.BST, RegelObjekt.ANLAGE, RegelObjekt.BETREIBER,
                                        RegelObjekt.ANLAGENTEIL, RegelObjekt.QUELLE, RegelObjekt.EUREG_ANLAGE))
                                .gruppe(RegelGruppe.GRUPPE_A)
                                .build();
        assertThrows(RegelException.class, regel::pruefeObjekteInGruppe);
    }

    @Test
    void pruefeObjekteInGruppe_B_OK() {
        var regel = RegelBuilder.aRegel()
                                .objekte(List.of(RegelObjekt.BST, RegelObjekt.ANLAGE, RegelObjekt.BETREIBER,
                                        RegelObjekt.ANLAGENTEIL, RegelObjekt.QUELLE, RegelObjekt.EUREG_ANLAGE))
                                .gruppe(RegelGruppe.GRUPPE_B)
                                .build();
        regel.pruefeObjekteInGruppe();
    }

    @Test
    void aendereTyp() {
        var regel = RegelBuilder.aRegel()
                                .typ(RegelTyp.IGNORIERT)
                                .land(Land.DEUTSCHLAND)
                                .typBundeseinheitlich(false)
                                .build();

        assertEquals(RegelTyp.IGNORIERT, regel.getTyp());
        assertEquals(RegelTyp.IGNORIERT, regel.getTyp(Land.BERLIN));

        regel.aendereTyp(Land.BERLIN, RegelTyp.FEHLER);

        assertEquals(RegelTyp.IGNORIERT, regel.getTyp());
        assertEquals(RegelTyp.IGNORIERT, regel.getTyp(Land.BAYERN));
        assertEquals(RegelTyp.FEHLER, regel.getTyp(Land.BERLIN));
    }

    @Test
    void aendereTyp_BerlinerRegel_shouldThrowException() {
        var regel = RegelBuilder.aRegel().land(Land.BERLIN).build();
        assertThrows(RegelException.class, () -> regel.aendereTyp(Land.BERLIN, RegelTyp.FEHLER));
    }

    @Test
    void aendereTyp_bundeseinheitlich_shouldThrowException() {
        var regel = RegelBuilder.aRegel().land(Land.DEUTSCHLAND).typBundeseinheitlich(true).build();
        assertThrows(RegelException.class, () -> regel.aendereTyp(Land.BERLIN, RegelTyp.FEHLER));
    }

    @Test
    void hatDatenberechtigung_WrongRolle_shouldFail() {
        var regel = RegelBuilder.aRegel().land(Land.NORDRHEIN_WESTFALEN).build();
        var result = regel.hatDatenberechtigung(false, Land.NORDRHEIN_WESTFALEN, false);
        assertThat(result).isFalse();
    }

    @Test
    void hatDatenberechtigung_Land_shouldFail() {
        var regel = RegelBuilder.aRegel().land(Land.NORDRHEIN_WESTFALEN).build();
        var result = regel.hatDatenberechtigung(false, Land.HAMBURG, true);
        assertThat(result).isFalse();
    }

    @Test
    void hatDatenberechtigung_Land_Ok() {
        var regel = RegelBuilder.aRegel().land(Land.HAMBURG).build();
        var result = regel.hatDatenberechtigung(false, Land.HAMBURG, true);
        assertThat(result).isTrue();
    }

    @Test
    void hatDatenberechtigung_Land_Gesamtadmin_Ok() {
        var regel = RegelBuilder.aRegel().land(Land.SACHSEN).build();
        var result = regel.hatDatenberechtigung(true, Land.DEUTSCHLAND, false);
        assertThat(result).isTrue();
    }

}
