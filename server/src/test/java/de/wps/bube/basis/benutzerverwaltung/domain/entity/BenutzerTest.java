package de.wps.bube.basis.benutzerverwaltung.domain.entity;

import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerDoppeltFehler;
import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerExistiertFehler;
import static de.wps.bube.basis.base.vo.Validierungshinweis.benutzerExistiertHinweis;
import static de.wps.bube.basis.base.vo.Validierungshinweis.betriebsstaetteNichtVorhandenFehler;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;

class BenutzerTest {

    private static final String USER = "benutzername";
    private static final String BST_NR = "nummer-1";

    private Benutzer benutzer;

    @BeforeEach
    void setUp() {
        benutzer = new Benutzer(USER);
        benutzer.setDatenberechtigung(
                Datenberechtigung.DatenberechtigungBuilder.land(Land.HAMBURG).betriebsstaetten(BST_NR).build());
    }

    @Test
    void validiereFuerImport_happyPath() {
        assertThat(benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(BST_NR), Set.of(),
                benutzer.getLand())).isEmpty();
    }

    @Test
    void validiereFuerImport_ignoreCase() {
        assertThat(benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(BST_NR.toUpperCase()), Set.of(),
                benutzer.getLand())).isEmpty();
    }

    @Test
    void validiereFuerImport_Land() {
        assertThat(
                benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(BST_NR), Set.of(), Land.MECKLENBURG_VORPOMMERN))
                .isNotEmpty();
    }

    @Test
    void validiereFuerImport_Land_Gesamtadmin() {
        assertThat(
                benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(BST_NR), Set.of(), Land.DEUTSCHLAND)).isEmpty();
    }

    @Test
    void validiereFuerImport_usernameDoppelt() {
        var hinweise = benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(BST_NR), Set.of(USER),
                benutzer.getLand());

        assertThat(hinweise).containsExactly(benutzerDoppeltFehler(USER));
    }

    @Test
    void validiereFuerImport_bstNichtVorhanden() {
        var hinweise = benutzer.validiereFuerImport(Set.of(), Set.of(), Set.of(), Set.of(), benutzer.getLand());

        assertThat(hinweise).containsExactly(betriebsstaetteNichtVorhandenFehler(USER, BST_NR));
    }

    @Test
    void validiereFuerImport_existiertBereits_alsBetreiber() {
        var hinweise = benutzer.validiereFuerImport(Set.of(USER), Set.of(USER), Set.of(BST_NR), Set.of(),
                benutzer.getLand());

        assertThat(hinweise).containsExactly(benutzerExistiertHinweis(USER));
    }

    @Test
    void validiereFuerImport_existiertBereits_nichtBetreiber() {
        var hinweise = benutzer.validiereFuerImport(Set.of(), Set.of(USER), Set.of(BST_NR), Set.of(),
                benutzer.getLand());

        assertThat(hinweise).containsExactly(benutzerExistiertFehler(USER));
    }

    @Test
    void validiereFuerImport_doppelt_bstNichtVorhanden_existiertBereitsAlsBetreiber() {
        var hinweise = benutzer.validiereFuerImport(Set.of(USER), Set.of(USER), Set.of(), Set.of(USER),
                benutzer.getLand());

        assertThat(hinweise).containsExactlyInAnyOrder(benutzerDoppeltFehler(USER),
                betriebsstaetteNichtVorhandenFehler(USER, BST_NR), benutzerExistiertHinweis(USER));
    }

    @Test
    void validiereFuerImport_doppelt_bstNichtVorhanden_existiertBereitsAlsNichtBetreiber() {
        var hinweise = benutzer.validiereFuerImport(Set.of(), Set.of(USER), Set.of(), Set.of(USER), benutzer.getLand());

        assertThat(hinweise).containsExactlyInAnyOrder(benutzerDoppeltFehler(USER),
                betriebsstaetteNichtVorhandenFehler(USER, BST_NR), benutzerExistiertFehler(USER));
    }

    @Test
    void containsAllIgnoreCase() {

        Datenberechtigung.DatenberechtigungBuilder builder = Datenberechtigung.DatenberechtigungBuilder.land(Land.HAMBURG);

        benutzer.setDatenberechtigung(builder.betriebsstaetten().build());
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of())).isTrue();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc"))).isTrue();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc", "xyz"))).isTrue();

        benutzer.setDatenberechtigung(builder.betriebsstaetten("abc").build());
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc"))).isTrue();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("ABC"))).isTrue();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc", "xyz"))).isTrue();

        benutzer.setDatenberechtigung(builder.betriebsstaetten("abc", "xyz").build());
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc"))).isFalse();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("ABC"))).isFalse();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("abc", "xyz"))).isTrue();
        assertThat(benutzer.allBstBerechtigungenContainedInIgnoreCase(List.of("aBc", "XyZ"))).isTrue();

    }

}
