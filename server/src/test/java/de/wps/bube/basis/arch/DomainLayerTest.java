package de.wps.bube.basis.arch;

import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAPackage;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;

@AnalyzeClasses(packages = DomainLayerTest.PACKAGE_BASE, importOptions = ImportOption.DoNotIncludeTests.class)
class DomainLayerTest {

    static final String PACKAGE_BASE = "de.wps.bube.basis";

    @ArchTest
    public static final ArchRule desktopLayer = domainLayers()
            .whereLayer("Desktop").mayNotBeAccessedByAnyLayer();

    @ArchTest
    public static final ArchRule euRegistryLayer = domainLayers()
            .whereLayer("EURegistry").mayOnlyBeAccessedByLayers("Desktop");

    @ArchTest
    public static final ArchRule benutzerverwaltungLayer = domainLayers()
            .whereLayer("Benutzerverwaltung").mayOnlyBeAccessedByLayers("Desktop", "EURegistry");

    @ArchTest
    public static final ArchRule stammdatenUebernahmeLayer = domainLayers()
            .whereLayer("StammdatenUebernahme").mayNotBeAccessedByAnyLayer();

    @ArchTest
    public static final ArchRule spbStammdaten = domainLayers()
            .whereLayer("SPB-Stammdaten").mayOnlyBeAccessedByLayers("StammdatenUebernahme", "Desktop");

    @ArchTest
    public static final ArchRule stammdatenLayer = domainLayers()
            .whereLayer("Stammdaten")
            .mayOnlyBeAccessedByLayers("Benutzerverwaltung", "Desktop", "EURegistry",
                    "SPB-Stammdaten", "StammdatenUebernahme");

    @ArchTest
    public static final ArchRule komplexpruefungLayer = domainLayers()
            .whereLayer("Komplexprüfung").mayOnlyBeAccessedByLayers("Stammdaten", "EURegistry");

    @ArchTest
    public static final ArchRule koordinatenLayer = domainLayers()
            .whereLayer("GemeindeKoordinaten").mayOnlyBeAccessedByLayers("Komplexprüfung", "Stammdaten", "EURegistry");

    @ArchTest
    public static final ArchRule referenzdatenLayer = domainLayers()
            .whereLayer("Referenzdaten")
            .mayOnlyBeAccessedByLayers("Benutzerverwaltung", "Stammdaten", "EURegistry", "Komplexprüfung",
                    "SPB-Stammdaten", "GemeindeKoordinaten", "StammdatenUebernahme");

    @ArchTest
    public static final ArchRule dateiLayer = domainLayers()
            .whereLayer("Datei")
            .mayOnlyBeAccessedByLayers("Referenzdaten", "Stammdaten", "Benutzerverwaltung", "EURegistry");

    @ArchTest
    public static final ArchRule jobsLayer = domainLayers()
            .whereLayer("Jobs")
            .mayOnlyBeAccessedByLayers("Datei", "Referenzdaten", "Stammdaten", "Benutzerverwaltung", "EURegistry",
                    "Komplexprüfung", "GemeindeKoordinaten");

    @ArchTest
    public static final ArchRule sicherheitLayer = domainLayers()
            .whereLayer("Sicherheit")
            .mayOnlyBeAccessedByLayers("Jobs", "Datei", "Referenzdaten", "Stammdaten", "SPB-Stammdaten",
                    "Benutzerverwaltung", "EURegistry", "Komplexprüfung", "Desktop", "GemeindeKoordinaten");

    private static Architectures.LayeredArchitecture domainLayers() {
        return layeredArchitecture()
                .layer("Desktop").definedBy(resideInAPackage(PACKAGE_BASE + ".desktop.."))
                .layer("EURegistry").definedBy(resideInAPackage(PACKAGE_BASE + ".euregistry.."))
                .layer("Benutzerverwaltung").definedBy(resideInAPackage(PACKAGE_BASE + ".benutzerverwaltung.."))
                .layer("StammdatenUebernahme").definedBy(resideInAPackage(PACKAGE_BASE + ".stammdatenuebernahme.."))
                .layer("SPB-Stammdaten").definedBy(resideInAPackage(PACKAGE_BASE + ".stammdatenbetreiber.."))
                .layer("Stammdaten").definedBy(resideInAPackage(PACKAGE_BASE + ".stammdaten.."))
                .layer("Komplexprüfung").definedBy(resideInAPackage(PACKAGE_BASE + ".komplexpruefung.."))
                .layer("GemeindeKoordinaten").definedBy(resideInAPackage(PACKAGE_BASE + ".koordinaten.."))
                .layer("Referenzdaten").definedBy(resideInAPackage(PACKAGE_BASE + ".referenzdaten.."))
                .layer("Datei").definedBy(resideInAPackage(PACKAGE_BASE + ".datei.."))
                .layer("Jobs").definedBy(resideInAPackage(PACKAGE_BASE + ".jobs.."))
                .layer("Sicherheit").definedBy(resideInAPackage(PACKAGE_BASE + ".sicherheit.."))
                .layer("Base").definedBy(resideInAPackage(PACKAGE_BASE + ".base.."));
    }

}
