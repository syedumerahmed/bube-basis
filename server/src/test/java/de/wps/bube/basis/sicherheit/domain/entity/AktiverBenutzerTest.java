package de.wps.bube.basis.sicherheit.domain.entity;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

class AktiverBenutzerTest {

    @Test
    void requireUsername() {
        assertThatThrownBy(() -> new AktiverBenutzer(null, Set.of(BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build()))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void requireRollen() {
        assertThatThrownBy(() -> new AktiverBenutzer("mjoergens", null,
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build()))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void requireParameter() {
        assertThatThrownBy(() -> new AktiverBenutzer("mjoergens", Set.of(BEHOERDENBENUTZER), null))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getUsername() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens", Set.of(BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.getUsername()).isEqualTo("mjoergens");
    }

    @Test
    void getLand() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens", Set.of(BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.getLand()).isEqualTo(HAMBURG);
    }

    @ParameterizedTest
    @EnumSource(value = Rolle.class, names = { "BENUTZERVERWALTUNG_DELETE", "BEHOERDE_READ" })
    void hatRolle(Rolle rolle) {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.hatRolle(rolle)).isTrue();
    }

    @ParameterizedTest
    @EnumSource(value = Rolle.class, names = { "BENUTZERVERWALTUNG_DELETE", "BEHOERDE_READ", "BEHOERDENBENUTZER" },
            mode = EnumSource.Mode.EXCLUDE)
    void hatRolleNicht(Rolle rolle) {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.hatRolle(rolle)).isFalse();
    }

    @Test
    void hatAlleRollen() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, GESAMTADMIN, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(DEUTSCHLAND).build());
        assertThat(aktiverBenutzer.hatAlleRollen(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ)).isTrue();
        assertThat(aktiverBenutzer.hatAlleRollen(List.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ))).isTrue();
    }

    @Test
    void hatNichtAlleRollen() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.hatAlleRollen(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ,
                GESAMTADMIN, BEHOERDENBENUTZER)).isFalse();
        assertThat(aktiverBenutzer.hatAlleRollen(
                List.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, GESAMTADMIN))).isFalse();
    }

    @Test
    void hatEineDerRollen() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.hatEineDerRollen(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ,
                GESAMTADMIN)).isTrue();
        assertThat(aktiverBenutzer.hatEineDerRollen(
                List.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, GESAMTADMIN))).isTrue();
    }

    @Test
    void hatKeineDerRollen() {
        AktiverBenutzer aktiverBenutzer = new AktiverBenutzer("mjoergens",
                Set.of(BENUTZERVERWALTUNG_DELETE, BEHOERDE_READ, BEHOERDENBENUTZER),
                Datenberechtigung.DatenberechtigungBuilder.land(HAMBURG).build());
        assertThat(aktiverBenutzer.hatEineDerRollen(GESAMTADMIN, BENUTZERVERWALTUNG_READ)).isFalse();
        assertThat(aktiverBenutzer.hatEineDerRollen(List.of(GESAMTADMIN, BENUTZERVERWALTUNG_READ))).isFalse();
    }
}
