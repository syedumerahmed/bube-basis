package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.PROTOTYP;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.RECHERCHE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_BUND_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_LAND_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.BODEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.WASSER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder;
import de.wps.bube.basis.sicherheit.config.SecurityConfig;

@SpringBootTest(classes = { KeycloakService.class, KeycloakServiceMapper.class })
@ContextConfiguration(classes = SecurityConfig.class)
@Tag("INTEGRATION")
class KeycloakServiceIntegrationTest {

    private static final String BENUTZERNAME = "unittest";

    @Autowired
    KeycloakService keycloakService;

    @BeforeEach
    void setUp() {
        keycloakService.findUserByUsername(BENUTZERNAME).map(Benutzer::getId).ifPresent(keycloakService::deleteUser);
    }

    @Test
    void listAllUsers() {
        var stream = keycloakService.listAllUsers();
        assertThat(stream).isNotEmpty();
    }

    @Test
    void createUser() {
        Benutzer b = getBenutzer().id(null).build();

        Benutzer bRead = keycloakService.createUser(b);

        assertNotNull(bRead.getId());
        assertEquals(b.getBenutzername(), bRead.getBenutzername());
        assertEquals(b.getDatenberechtigung(), bRead.getDatenberechtigung());
        assertThat(bRead.getZugewieseneRollen()).containsExactlyInAnyOrderElementsOf(b.getZugewieseneRollen());

        keycloakService.deleteUser(bRead.getId());
    }

    @Test
    void getUserByUsername() {
        assertThat(keycloakService.findUserByUsername("diesenbenutzergibtesnicht")).isEmpty();
    }

    @Test
    void updateUser() {

        // Setup
        Benutzer b = keycloakService.createUser(getBenutzer().nachname("Nachname").build());

        // Act
        b.setNachname("Nachname-verändert");
        b.setZugewieseneRollen(List.of(BEHOERDENBENUTZER, LAND_WRITE, REFERENZLISTEN_BUND_WRITE));
        Benutzer bRead = keycloakService.updateUser(b);

        // Test
        assertEquals(b.getId(), bRead.getId());
        assertEquals("Nachname-verändert", bRead.getNachname());
        assertThat(bRead.getZugewieseneRollen()).containsExactlyInAnyOrderElementsOf(b.getZugewieseneRollen());
        keycloakService.deleteUser(b.getId());
    }

    @Test
    void deleteUser() {
        Benutzer bRead = keycloakService.createUser(getBenutzer().build());

        assertNotNull(keycloakService.getUserById(bRead.getId()));
        keycloakService.deleteUser(bRead.getId());
        assertThrows(EntityNotFoundException.class, () -> keycloakService.getUserById(bRead.getId()));
    }

    @Test
    void getUser() {
        Benutzer b = keycloakService.createUser(getBenutzer().build());

        Benutzer bRead = keycloakService.getUserById(b.getId());
        assertEquals(b.getBenutzername(), bRead.getBenutzername());
        assertEquals(b.getId(), bRead.getId());
        assertEquals(b.getNachname(), bRead.getNachname());
        assertEquals(b.getVorname(), bRead.getVorname());
        assertEquals(b.isAktiv(), bRead.isAktiv());
        assertEquals(b.getTelefonnummer(), bRead.getTelefonnummer());
        assertEquals(b.getEmail(), bRead.getEmail());
        assertEquals(b.getDatenberechtigung(), bRead.getDatenberechtigung());
        assertThat(bRead.getZugewieseneRollen()).containsAll(b.getZugewieseneRollen());
        assertThat(bRead.getZugewieseneRollen()).contains(BEHOERDENBENUTZER);

        keycloakService.deleteUser(bRead.getId());
    }

    private static BenutzerBuilder getBenutzer() {
        return aBehoerdenbenutzer(BENUTZERNAME, HAMBURG)
                .id(null)
                .nachname("Nachname")
                .vorname("Vorname")
                .email("email")
                .aktiv(true)
                .telefonnummer("1234567890")

                .akz("akz")
                .behoerden("8765643")
                .betriebsstaetten("00001")
                .verwaltungsgebiete("HHKK")
                .themen(WASSER, BODEN)
                .fachmodule(PROTOTYP, RECHERCHE)
                .addRollen(LAND_WRITE, REFERENZLISTEN_LAND_WRITE);
    }
}
