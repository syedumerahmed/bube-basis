package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.BETRIEBSSTATUS;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.NAME;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.querydsl.core.BooleanBuilder;

import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;

class FilterMapperTest {

    private SuchattributePredicateFactory mapper;

    @BeforeEach
    void setUp() {
        mapper = new SuchattributePredicateFactory();
    }

    @Test
    void shouldMapNull() {
        assertThat(mapper.createPredicate(null, betriebsstaette)).isNull();
    }

    @Test
    void shouldMapEmpty() {
        assertThat(mapper.createPredicate(new Suchattribute(), betriebsstaette)).isEqualTo(new BooleanBuilder());
    }

    @Test
    void shouldMapSingleParameter() {
        var suchparameter = new Suchattribute() {{
            betriebsstaette.name = "name";
        }};
        var expected = NAME.createPredicate(suchparameter, betriebsstaette);

        var predicate = mapper.createPredicate(suchparameter, betriebsstaette);

        assertThat(predicate).isEqualTo(new BooleanBuilder(expected));
    }

    @Test
    void shouldAndMultipleParameters() {
        var suchparameter = new Suchattribute() {{
            betriebsstaette.name = "name";
            betriebsstaette.statusIn = List.of(15L, 20L);
        }};
        var expected = new BooleanBuilder(NAME.createPredicate(suchparameter, betriebsstaette))
                .and(BETRIEBSSTATUS.createPredicate(suchparameter, betriebsstaette));

        var predicate = mapper.createPredicate(suchparameter, betriebsstaette);

        assertThat(predicate).isEqualTo(expected);
    }
}
