package de.wps.bube.basis.desktop.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTagsEnum;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlageListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlagenteilListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbQuelleListItem;

@ExtendWith(MockitoExtension.class)
class SpbBetriebsstaetteTreeNodeMapperTest {

    @Mock
    private StammdatenBetreiberService stammdatenBetreiberService;

    @Mock
    private StammdatenService stammdatenService;

    private SpbBestriebsstaetteTreeNodeMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new SpbBestriebsstaetteTreeNodeMapper(stammdatenBetreiberService, stammdatenService);
    }

    @Test
    void mapBetriebsstaette_Quelle_an_BST() {

        // Setup
        SpbBetriebsstaette b = new SpbBetriebsstaette(BetriebsstaetteBuilder.aBetriebsstaette().id(1L)
                                                                            .name("name")
                                                                            .betriebsstaetteNr("100")
                                                                            .land(Land.HAMBURG)
                                                                            .build());
        SpbAnlageListItem anlageListItem = new SpbAnlageListItem(321L, "200", "anlage",
                ReferenzBuilder.aBetriebsstatusReferenz("01").build(),
                true, 1L);
        SpbQuelleListItem quelleListItem = new SpbQuelleListItem(123L, "300", "quelle",
                false, "999");
        SpbAnlagenteilListItem anlagenteilListItem = new SpbAnlagenteilListItem(322L, "400", "anlagenteil",
                ReferenzBuilder.aBetriebsstatusReferenz("01").build(),
                true);

        when(stammdatenBetreiberService.listAllSpbAnlagenAsListItemByBetriebsstaettenId(
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(anlageListItem));
        when(stammdatenBetreiberService.listAllSpbQuellenAsListItemByBetriebsstaettenId(
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(quelleListItem));
        when(stammdatenBetreiberService.listAllSpbAnlagenteileAsListItemByAnlageId(anlageListItem.getAnlageId(),
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(anlagenteilListItem));

        // Act
        TreeNodeDto betriebsstaetteDto = mapper.mapBetriebsstaette(b);

        // Assert
        assertEquals("name", betriebsstaetteDto.getName());
        assertEquals(TreeNodeTypEnum.BETRIEBSSTAETTE, betriebsstaetteDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100", betriebsstaetteDto.getLink());
        assertEquals("100", betriebsstaetteDto.getNummer());
        assertTrue(betriebsstaetteDto.getTags().isEmpty());
        assertEquals(2, betriebsstaetteDto.getChildren().size());

        // Act
        TreeNodeDto anlageDto = betriebsstaetteDto.getChildren().get(0);

        // Assert
        assertEquals("anlage", anlageDto.getName());
        assertEquals(TreeNodeTypEnum.ANLAGE, anlageDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200",
                anlageDto.getLink());
        assertEquals("200", anlageDto.getNummer());
        assertTrue(anlageDto.getTags().contains(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER));
        assertEquals(1, anlageDto.getChildren().size());

        // Act
        TreeNodeDto anlagenteilDto = anlageDto.getChildren().get(0);

        // Assert
        assertEquals("anlagenteil", anlagenteilDto.getName());
        assertEquals(TreeNodeTypEnum.ANLAGE, anlagenteilDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200/anlagenteil/nummer/400",
                anlagenteilDto.getLink());
        assertEquals("400", anlagenteilDto.getNummer());
        assertTrue(anlagenteilDto.getTags().contains(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER));
        assertNull(anlagenteilDto.getChildren());

        // Act
        TreeNodeDto quelleDto = betriebsstaetteDto.getChildren().get(1);

        // Assert
        assertEquals("quelle", quelleDto.getName());
        assertEquals(TreeNodeTypEnum.QUELLE, quelleDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/quelle/nummer/300",
                quelleDto.getLink());
        assertEquals("300", quelleDto.getNummer());
        assertTrue(quelleDto.getTags().contains(TreeNodeTagsEnum.NEU_DURCH_BETREIBER));
        assertNull(quelleDto.getChildren());

    }

    @Test
    void mapBetriebsstaette_Quelle_an_Anlage() {

        // Setup
        SpbBetriebsstaette b = new SpbBetriebsstaette(BetriebsstaetteBuilder.aBetriebsstaette().id(1L)
                                                                            .name("name")
                                                                            .betriebsstaetteNr("100")
                                                                            .land(Land.HAMBURG)
                                                                            .build());
        SpbAnlageListItem anlageListItem = new SpbAnlageListItem(321L, "200", "anlage",
                ReferenzBuilder.aBetriebsstatusReferenz("01").build(),
                true, 1L);
        SpbQuelleListItem quelleListItem = new SpbQuelleListItem(123L, "300", "quelle",
                false, "999");
        SpbAnlagenteilListItem anlagenteilListItem = new SpbAnlagenteilListItem(322L, "400", "anlagenteil",
                ReferenzBuilder.aBetriebsstatusReferenz("01").build(),
                true);

        when(stammdatenBetreiberService.listAllSpbAnlagenAsListItemByBetriebsstaettenId(
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(anlageListItem));
        when(stammdatenBetreiberService.listAllSpbQuellenAsListItemByAnlageId(anlageListItem.getAnlageId(),
                anlageListItem.getSpbAnlageId(),
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(quelleListItem));
        when(stammdatenBetreiberService.listAllSpbAnlagenteileAsListItemByAnlageId(anlageListItem.getAnlageId(),
                b.getBetriebsstaette().getId())).thenReturn(
                List.of(anlagenteilListItem));

        // Act
        TreeNodeDto betriebsstaetteDto = mapper.mapBetriebsstaette(b);

        // Assert
        assertEquals("name", betriebsstaetteDto.getName());
        assertEquals(TreeNodeTypEnum.BETRIEBSSTAETTE, betriebsstaetteDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100", betriebsstaetteDto.getLink());
        assertEquals("100", betriebsstaetteDto.getNummer());
        assertTrue(betriebsstaetteDto.getTags().isEmpty());
        assertEquals(1, betriebsstaetteDto.getChildren().size());

        // Act
        TreeNodeDto anlageDto = betriebsstaetteDto.getChildren().get(0);

        // Assert
        assertEquals("anlage", anlageDto.getName());
        assertEquals(TreeNodeTypEnum.ANLAGE, anlageDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200",
                anlageDto.getLink());
        assertEquals("200", anlageDto.getNummer());
        assertTrue(anlageDto.getTags().contains(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER));
        assertEquals(2, anlageDto.getChildren().size());

        // Act
        TreeNodeDto anlagenteilDto = anlageDto.getChildren().get(0);

        // Assert
        assertEquals("anlagenteil", anlagenteilDto.getName());
        assertEquals(TreeNodeTypEnum.ANLAGE, anlagenteilDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200/anlagenteil/nummer/400",
                anlagenteilDto.getLink());
        assertEquals("400", anlagenteilDto.getNummer());
        assertTrue(anlagenteilDto.getTags().contains(TreeNodeTagsEnum.AENDERUNG_DURCH_BETREIBER));
        assertNull(anlagenteilDto.getChildren());

        // Act
        TreeNodeDto quelleDto = anlageDto.getChildren().get(1);

        // Assert
        assertEquals("quelle", quelleDto.getName());
        assertEquals(TreeNodeTypEnum.QUELLE, quelleDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200/quelle/nummer/300",
                quelleDto.getLink());
        assertEquals("300", quelleDto.getNummer());
        assertTrue(quelleDto.getTags().contains(TreeNodeTagsEnum.NEU_DURCH_BETREIBER));
        assertNull(quelleDto.getChildren());

    }

}
