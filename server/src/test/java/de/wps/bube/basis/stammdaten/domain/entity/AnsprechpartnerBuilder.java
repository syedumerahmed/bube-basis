package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aFunktionsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;

import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;

public final class AnsprechpartnerBuilder {

    private Long id;
    private String vorname;
    private String nachname;
    private Referenz funktion;
    private String bemerkung;
    private List<Kommunikationsverbindung> kommunikationsverbindungen = new ArrayList<>();

    private AnsprechpartnerBuilder() {
    }

    public static AnsprechpartnerBuilder anAnsprechpartner() {
        return new AnsprechpartnerBuilder();
    }

    public static AnsprechpartnerBuilder anAnsprechpartner(Long id) {
        return anAnsprechpartner().id(id);
    }

    public static AnsprechpartnerBuilder aFullAnsprechpartner() {
        return anAnsprechpartner().id(123L)
                                  .vorname("vorname")
                                  .nachname("nachname")
                                  .funktion(aFunktionsReferenz("funktion").build())
                                  .bemerkung("bemerkung")
                                  .kommunikationsverbindungen(
                                          new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                  "123"),
                                          new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                  "321"));
    }

    public AnsprechpartnerBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public AnsprechpartnerBuilder vorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public AnsprechpartnerBuilder nachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public AnsprechpartnerBuilder funktion(Referenz funktion) {
        this.funktion = funktion;
        return this;
    }

    public AnsprechpartnerBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public AnsprechpartnerBuilder kommunikationsverbindungen(Kommunikationsverbindung... kommunikationsverbindungen) {
        return kommunikationsverbindungen(List.of(kommunikationsverbindungen));
    }

    public AnsprechpartnerBuilder kommunikationsverbindungen(
            List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
        return this;
    }

    public Ansprechpartner build() {
        Ansprechpartner ansprechpartner = new Ansprechpartner();
        ansprechpartner.setId(id);
        ansprechpartner.setBemerkung(bemerkung);
        ansprechpartner.setFunktion(funktion);
        ansprechpartner.setKommunikationsverbindungen(kommunikationsverbindungen);
        ansprechpartner.setNachname(nachname);
        ansprechpartner.setVorname(vorname);
        return ansprechpartner;
    }

}
