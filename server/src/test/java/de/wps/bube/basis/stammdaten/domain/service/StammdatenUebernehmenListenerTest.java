package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.stammdaten.domain.event.StammdatenUebernehmenTriggeredEvent;

@ExtendWith(MockitoExtension.class)
class StammdatenUebernehmenListenerTest {

    @Mock
    BenutzerJobRegistry jobRegistry;
    @Mock
    private StammdatenUebernehmenJob job;

    private StammdatenUebernehmenListener listener;

    @BeforeEach
    void setUp() {
        listener = new StammdatenUebernehmenListener(jobRegistry, job);
    }

    @Test
    void testHandle() {
        var ids = List.of(1L, 2L, 3L);
        var zieljahr = aJahrReferenz(2018).build();
        var username = "user";
        var event = new StammdatenUebernehmenTriggeredEvent(ids, zieljahr, username);

        listener.handleStammdatenUebernehmen(event);

        verify(job).runAsync(ids, zieljahr, username);
    }

}
