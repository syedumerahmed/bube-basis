package de.wps.bube.basis.benutzerverwaltung.xml;

import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.PRTR_LCP;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.BODEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.WASSER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.ObjectFactory;
import de.wps.bube.basis.benutzerverwaltung.xml.dto.BenutzerListeXDto;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.xmlunit.builder.Input;

import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;

@SpringBootTest(classes = { BenutzerXMLMapperImpl.class, DatenberechtigungXMLMapperImpl.class })
class BenutzerXMLServiceWriteTest {

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BenutzerXMLMapper benutzerXMLMapper;

    @Test
    void writeBenutzerEmpty() {
        var objectFactory = new ObjectFactory();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Stream<Benutzer> benutzerStream = Stream.empty();

        var writer = xmlService.createWriter(BenutzerType.class, BenutzerListeXDto.QUALIFIED_NAME,
                benutzerXMLMapper::toXDto, objectFactory::createBenutzer);

        writer.write(benutzerStream, outputStream);

        MatcherAssert.assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(Input.fromString(
                        "<?xml version='1.0' encoding='UTF-8'?><benutzerliste xmlns=\"http://basis.bube.wps.de/benutzer\" />")).ignoreElementContentWhitespace());
    }

    @Test
    void writeBenutzer() {
        var objectFactory = new ObjectFactory();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        List<Benutzer> list = new ArrayList<>();
        list.add(getBenutzer());
        Stream<Benutzer> benutzerStream = list.stream();

        var writer = xmlService.createWriter(BenutzerType.class, BenutzerListeXDto.QUALIFIED_NAME,
                benutzerXMLMapper::toXDto, objectFactory::createBenutzer);

        writer.write(benutzerStream, outputStream);

        var expectedXML = Input.fromString("""
                <?xml version='1.0' encoding='UTF-8'?>
                <benutzerliste xmlns="http://basis.bube.wps.de/benutzer">
                    <benutzer benutzername="unittest" vorname="Vorname" nachname="Nachname" email="email@somewhere.com"
                              telefonnummer="1234567890" aktiv="true">
                        <datenberechtigung>
                            <land>00</land>
                            <betriebsstaetten><nummer>bst1</nummer></betriebsstaetten>
                            <fachmodule><fachmodul>PRTR+LCP</fachmodul></fachmodule>
                        </datenberechtigung>
                    </benutzer>
                </benutzerliste>
                """);
        assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
    }

    private Benutzer getBenutzer() {
        Benutzer b = new Benutzer("unittest");
        b.setNachname("Nachname");
        b.setVorname("Vorname");
        b.setEmail("email@somewhere.com");
        b.setAktiv(true);
        b.setTelefonnummer("1234567890");
        b.setDatenberechtigung(Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND)
                                                                         .betriebsstaetten("bst1")
                                                                         .themen(WASSER, BODEN)
                                                                         .fachmodule(PRTR_LCP)
                                                                         .build());
        return b;
    }

}
