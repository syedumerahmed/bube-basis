package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.datei.domain.entity.DateiScope.EUREG;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.ADMIN;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.euregistry.xml.EURegBstXmlMapperImpl;
import de.wps.bube.basis.euregistry.xml.EuRegBetriebsstaetteImportDtoMapperImpl;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.AnlagenUpdaterImpl;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsobjektService;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettenUpdaterImpl;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenBerichtsjahrwechselMapperImpl;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenJob;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = {
        EURegistryImportExportService.class,
        EURegistryService.class,
        EuRegistryImportierenJob.class,
        EURegBstXmlMapperImpl.class,
        EURegBetriebsstaetteRepository.class,
        EuRegBstBerechtigungsAccessor.class,
        EuRegistryBerichtsjahrwechselMapperImpl.class,
        EuRegBetriebsstaetteImportDtoMapperImpl.class,
        EuRegUpdaterImpl.class,
        EuRegistryImportierenListener.class,

        StammdatenService.class,
        BerichtsobjektService.class,
        BetriebsstaettenBerechtigungsAccessor.class,
        BetreiberBerechtigungsAccessor.class,
        StammdatenBerichtsjahrwechselMapperImpl.class,
        AnlagenUpdaterImpl.class,
        BetriebsstaettenUpdaterImpl.class,
        KomplexpruefungService.class,
        RegelPruefungService.class,
        BehoerdenService.class,
        BehoerdeBerechtigungsAccessor.class,
        EigeneBehoerdenBerechtigungsAcessor.class,
        ReferenzenService.class,
        ReferenzResolverService.class,

        XMLService.class,
})
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.euregistry",
        "de.wps.bube.basis.stammdaten",
        "de.wps.bube.basis.komplexpruefung",
        "de.wps.bube.basis.referenzdaten" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.euregistry",
        "de.wps.bube.basis.stammdaten",
        "de.wps.bube.basis.komplexpruefung",
        "de.wps.bube.basis.referenzdaten" })
class EuRegistryImportierenJobIntegrationTest extends BubeIntegrationTest {

    @Autowired
    TestEntityManager testEntityManager;

    @MockBean
    ParameterService parameterService;
    @MockBean
    DateiService dateiService;
    @MockBean
    BenutzerJobRegistry benutzerJobRegistry;
    @MockBean
    ReferenzenBerechtigungsContext referenzenBerechtigungsContext;

    @Autowired
    ReferenzResolverService referenzResolverService;
    @Autowired
    ReferenzenService referenzenService;
    @Autowired
    StammdatenService stammdatenService;
    @Autowired
    BerichtsobjektService berichtsobjektService;
    @Autowired
    EURegistryImportExportService importService;
    @Autowired
    EURegistryService euRegistryService;

    @Captor
    ArgumentCaptor<String> protokollCaptor;
    @Captor
    ArgumentCaptor<JobStatus> jobStatusCaptor;

    Betriebsstaette betriebsstaette;
    Anlage anlage;

    @BeforeEach
    void setUp() {
        // Berichtspflichtige Stammdaten anlegen
        var berichtsjahr = testEntityManager.persist(aJahrReferenz(2019).build());
        anlage = anAnlage().localId("localId2").add17BimschvVorschrift().build();
        anlage.getVorschriften().forEach(vorschrift -> testEntityManager.persist(vorschrift.getArt()));
        betriebsstaette = aBetriebsstaette().berichtsjahr(berichtsjahr)
                                            .land(HAMBURG)
                                            .betriebsstaetteNr("stammdatenNr")
                                            .localId("localId")
                                            .zustaendigeBehoerde(
                                                    BehoerdeBuilder.aBehoerde("1234").land(HAMBURG).build())
                                            .anlagen(anlage)
                                            .build();
        testEntityManager.persist(betriebsstaette.getZustaendigeBehoerde());
        testEntityManager.persist(betriebsstaette.getGemeindekennziffer());
        testEntityManager.persist(betriebsstaette.getBetriebsstatus());
    }

    @Test
    @WithBubeMockUser(name = "admin_hh", land = HAMBURG, roles = ADMIN)
    void triggerImport() throws FileNotFoundException {
        betriebsstaette = stammdatenService.createBetriebsstaette(betriebsstaette);
        testEntityManager.getEntityManager().getTransaction().commit();
        when(dateiService.getDateiStream("admin_hh", EUREG))
                .thenReturn(getResource("xml/euregistry/import_simple.xml"));

        var oldBericht = euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId());
        oldBericht.ifPresent(bericht -> {
            bericht.getFeuerungsanlage().setOeffentlicheBekanntmachungUrl("http://old.public.disclosure");
            euRegistryService.updateAnlagenBericht(bericht);
            testEntityManager.detach(bericht);
        });


        JobStatus status = new JobStatus(JobStatusEnum.EUREGISRTY_IMPORTIEREN);
        when(benutzerJobRegistry.getJobStatus()).thenReturn(Optional.of(status));

        importService.triggerImport(betriebsstaette.getBerichtsjahr().getSchluessel());

        verify(benutzerJobRegistry).jobCompleted(false);
        verify(benutzerJobRegistry, never()).jobError(any());
        verify(benutzerJobRegistry).jobCountChanged(1L);

        verify(benutzerJobRegistry).jobProtokoll(protokollCaptor.capture());
        var protokoll = protokollCaptor.getValue();
        assertThat(protokoll).startsWith(StammdatenJob.PROTOKOLL_OPENING);
        assertThat(protokoll).doesNotContain("Technische Ursache");
        assertThat(protokoll).doesNotContain("nicht gefunden");

        verify(benutzerJobRegistry).jobStarted(jobStatusCaptor.capture());
        var jobStatus = jobStatusCaptor.getValue();
        assertThat(jobStatus.getJobName()).isEqualTo(EuRegistryImportierenJob.JOB_NAME.name());

        var updatedBericht = euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId());
        assertThat(updatedBericht).map(EURegAnlage::getFeuerungsanlage)
                                  .map(FeuerungsanlageBerichtsdaten::getOeffentlicheBekanntmachungUrl)
                                  .hasValue("https://new.public.disclosure");


    }
}
