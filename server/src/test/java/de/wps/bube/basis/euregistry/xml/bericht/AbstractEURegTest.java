package de.wps.bube.basis.euregistry.xml.bericht;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;

@ExtendWith(MockitoExtension.class)
class AbstractEURegTest {

    private final InspireParam inspireParam = new InspireParam("test-ns", ".uba.de", "-ext");
    private final XeurProperties.UbaProperties ubaProperties = new XeurProperties.UbaProperties(
            "UBA", "Umweltbundesamt", "EU-Registry-DE@uba.de", "+49 (0)340 2103-2954", "+49 (0)340 2104-2954",
            new XeurProperties.UbaProperties.AddressProperties("Wörlitzer Platz", "1", "Dessau-Rosslau", "D-06844")
    );
    private ReportData reportData;

    private ProductionSiteMapper productionSiteMapper;
    private ProductionFacilityMapper productionFacilityMapper;
    private ProductionInstallationMapper productionInstallationMapper;
    private ProductionInstallationPartMapper productionInstallationPartMapper;

    @BeforeEach
    void setUp() {

        reportData = new ReportDataMapperImpl().mapReportData("2020");

        CommonEURegMapper commonEURegMapper = new CommonEURegMapperImpl();
        commonEURegMapper.koordinatenTransformationService = mock(KoordinatenTransformationService.class);
        when(commonEURegMapper.koordinatenTransformationService.transformToEPSG4258(any())).thenReturn(new double[] {1, 2});
        AddressMapper addressMapper = new AddressMapperImpl(commonEURegMapper);

        productionSiteMapper = new ProductionSiteMapperImpl(commonEURegMapper);
        productionFacilityMapper = new ProductionFacilityMapperImpl(addressMapper, commonEURegMapper);
        productionInstallationPartMapper = new ProductionInstallationPartMapperImpl(commonEURegMapper);
        productionInstallationMapper = new ProductionInstallationMapperImpl(commonEURegMapper,
                productionInstallationPartMapper);

    }

    protected InspireParam getInspireParam() {
        return inspireParam;
    }

    protected XeurProperties.UbaProperties getUbaProperties() {
        return ubaProperties;
    }

    protected ReportData getReportData() {
        return reportData;
    }

    protected ProductionSiteMapper getProductionSiteMapper() {
        return productionSiteMapper;
    }

    protected ProductionFacilityMapper getProductionFacilityMapper() {
        return productionFacilityMapper;
    }

    protected ProductionInstallationMapper getProductionInstallationMapper() {
        return productionInstallationMapper;
    }

    protected ProductionInstallationPartMapper getProductionInstallationPartMapper() {
        return productionInstallationPartMapper;
    }
}