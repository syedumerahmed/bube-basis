package de.wps.bube.basis.sicherheit.domain.entity;

import java.util.EnumSet;
import java.util.Set;

import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

public final class BenutzerBuilder {
    private String username;
    private Datenberechtigung berechtigung;
    private Set<Rolle> rollen = EnumSet.noneOf(Rolle.class);

    private BenutzerBuilder() {
    }

    public static BenutzerBuilder aBenutzer() {
        return new BenutzerBuilder();
    }

    public static BenutzerBuilder aBenutzer(String username) {
        return aBenutzer().username(username);
    }

    public static BenutzerBuilder aBehoerdenbenutzer() {
        return aBenutzer("behoerdenbenutzer").rollen(Rolle.BEHOERDENBENUTZER);
    }

    public BenutzerBuilder username(String username) {
        this.username = username;
        return this;
    }

    public BenutzerBuilder berechtigung(Datenberechtigung berechtigung) {
        this.berechtigung = berechtigung;
        return this;
    }

    public BenutzerBuilder rollen(Rolle... rollen) {
        return rollen(Set.of(rollen));
    }

    public BenutzerBuilder rollen(Set<Rolle> rollen) {
        this.rollen = EnumSet.copyOf(rollen);
        return this;
    }

    public BenutzerBuilder addRollen(Rolle... rollen) {
        this.rollen.addAll(Set.of(rollen));
        return this;
    }

    public AktiverBenutzer build() {
        return new AktiverBenutzer(username, rollen, berechtigung);
    }
}
