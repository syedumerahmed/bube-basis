package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBetreiberbenutzer;
import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NIEDERSACHSEN;
import static de.wps.bube.basis.base.vo.Land.SCHLESWIG_HOLSTEIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.E_ERKLAERUNG;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.PRTR_LCP;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.RECHERCHE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.ABFALL;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.BODEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.LUFT;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerException;
import de.wps.bube.basis.benutzerverwaltung.domain.persistence.BenutzerImportJobStatusRepository;
import de.wps.bube.basis.benutzerverwaltung.xml.BenutzerXMLMapper;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = BenutzerverwaltungService.class)
class BenutzerverwaltungServiceSecurityTest extends SecurityTest {

    @MockBean
    private KeycloakService keycloakService;
    @MockBean
    private DateiService dateiService;
    @MockBean
    private XMLService XMLService;
    @MockBean
    private BenutzerXMLMapper xmlMapper;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private PasswordGenerator passwordGenerator;

    @Autowired
    private BenutzerverwaltungService service;

    @Nested
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    class AsGesamtadmin {
        @Test
        void createBehoerdenbenutzer() {
            var toCreate = aBehoerdenbenutzer("newUser", SCHLESWIG_HOLSTEIN)
                    .addRollen(BEHOERDENBENUTZER, BEHOERDE_READ).build();

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void updateBehoerdenbenutzer() {
            var toUpdate = aBehoerdenbenutzer("newUser", SCHLESWIG_HOLSTEIN).id(UUID.randomUUID().toString())
                                                                            .addRollen(BEHOERDE_READ)
                                                                            .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void deleteBehoerdenbenutzer() {
            var toDelete = aBehoerdenbenutzer("newUser", SCHLESWIG_HOLSTEIN).id(UUID.randomUUID().toString())
                                                                            .addRollen(BEHOERDE_READ)
                                                                            .build();
            when(keycloakService.findUserByUsername(toDelete.getBenutzername())).thenReturn(Optional.of(toDelete));

            service.deleteBenutzer(toDelete.getBenutzername());

            verify(keycloakService).deleteUser(toDelete.getId());
        }

        @Test
        void createBetreiberbenutzer() {
            var toCreate = aBetreiberbenutzer("newUser", SCHLESWIG_HOLSTEIN).betriebsstaetten("BST").build();
            when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("BST"));

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void updateBetreiberbenutzer() {
            var toUpdate = aBetreiberbenutzer("newUser", SCHLESWIG_HOLSTEIN).betriebsstaetten("BST")
                                                                            .id(UUID.randomUUID().toString())
                                                                            .build();
            when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("BST"));
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void deleteBetreiberbenutzer() {
            var toDelete = aBetreiberbenutzer("newUser", SCHLESWIG_HOLSTEIN).id(UUID.randomUUID().toString()).build();
            when(keycloakService.findUserByUsername(toDelete.getBenutzername())).thenReturn(Optional.of(toDelete));

            service.deleteBenutzer(toDelete.getBenutzername());

            verify(keycloakService).deleteUser(toDelete.getId());
        }

        @Test
        void createGesamtadmin() {
            var toCreate = aBehoerdenbenutzer("newUser", DEUTSCHLAND).addRollen(GESAMTADMIN).build();

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void updateGesamtadmin() {
            var toUpdate = aBehoerdenbenutzer("newUser", DEUTSCHLAND).id(UUID.randomUUID().toString())
                                                                     .addRollen(GESAMTADMIN)
                                                                     .build();
            when(keycloakService.findUserByUsername(toUpdate.getBenutzername())).thenReturn(Optional.of(toUpdate));

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void deleteGesamtadmin() {
            var toDelete = aBehoerdenbenutzer("newUser", DEUTSCHLAND).id(UUID.randomUUID().toString())
                                                                     .addRollen(GESAMTADMIN)
                                                                     .build();
            when(keycloakService.findUserByUsername(toDelete.getBenutzername())).thenReturn(Optional.of(toDelete));

            service.deleteBenutzer(toDelete.getBenutzername());

            verify(keycloakService).deleteUser(toDelete.getId());
        }
    }

    @Nested
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BENUTZERVERWALTUNG_WRITE }, land = HAMBURG)
    class AsBenutzerverwaltungWriteUser {
        @Test
        void createUser_withSameRollenAndDatenberechtigungen() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE).build();

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void createUser_withRestrictedRolleAndBerechtigung() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_READ)
                                                                 .themen(BODEN)
                                                                 .fachmodule(E_ERKLAERUNG)
                                                                 .build();

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void createUser_denyHigherRole() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_DELETE).build();

            assertThrows(BenutzerException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void createUser_denyWrongDatenberechtigung() {
            var toCreate = aBehoerdenbenutzer("newUser", NIEDERSACHSEN).build();

            assertThrows(AccessDeniedException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void createBetreiberbenutzer() {
            var toCreate = aBetreiberbenutzer("newUser", HAMBURG).betriebsstaetten("BST").build();
            when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("BST"));

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void updateBetreiberbenutzer() {
            var toUpdate = aBetreiberbenutzer("newUser", HAMBURG).betriebsstaetten("BST")
                                                                 .id(UUID.randomUUID().toString())
                                                                 .build();
            when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("BST"));
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void updateUser_withSameRollenAndDatenberechtigungen() {
            var toUpdate = aBehoerdenbenutzer("newUser", HAMBURG).id(UUID.randomUUID().toString())
                                                                 .addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void updateUser_withRestrictedRolleAndBerechtigung() {
            var toUpdate = aBehoerdenbenutzer("newUser", HAMBURG).id(UUID.randomUUID().toString())
                                                                 .addRollen(BENUTZERVERWALTUNG_READ)
                                                                 .themen(BODEN)
                                                                 .fachmodule(E_ERKLAERUNG)
                                                                 .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void updateUser_allowHigherRoleIfUnchanged() {
            var toUpdate = aBehoerdenbenutzer("newUser", HAMBURG).id(UUID.randomUUID().toString())
                                                                 .addRollen(BENUTZERVERWALTUNG_DELETE)
                                                                 .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void updateUser_denyNewHigherRole() {
            var updatedUserBuilder = aBehoerdenbenutzer("newUser", HAMBURG).id(UUID.randomUUID().toString());
            var beforeUpdate = updatedUserBuilder.addRollen(BENUTZERVERWALTUNG_READ).build();
            var afterUpdate = updatedUserBuilder.addRollen(BENUTZERVERWALTUNG_DELETE).build();
            when(keycloakService.getUserById(beforeUpdate.getId())).thenReturn(beforeUpdate);

            assertThrows(BenutzerException.class, () -> service.updateBenutzer(afterUpdate));

            verify(keycloakService).getUserById(beforeUpdate.getId());
            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void updateUser_denyWrongDatenberechtigungForExistingUser() {
            var userId = UUID.randomUUID().toString();
            var beforeUpdate = aBehoerdenbenutzer("user", NIEDERSACHSEN).id(userId).build();
            var afterUpdate = aBehoerdenbenutzer("user", HAMBURG).id(userId).build();
            when(keycloakService.getUserById(beforeUpdate.getId())).thenReturn(beforeUpdate);

            assertThrows(AccessDeniedException.class, () -> service.updateBenutzer(afterUpdate));

            verify(keycloakService).getUserById(beforeUpdate.getId());
            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void updateUser_denyWrongDatenberechtigungForUpdatedUser() {
            var afterUpdate = aBehoerdenbenutzer("user", NIEDERSACHSEN).id(UUID.randomUUID().toString()).build();

            assertThrows(AccessDeniedException.class, () -> service.updateBenutzer(afterUpdate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void updateUser_denyDemotionOfPrivilegedUser() {
            var existingUser = aBehoerdenbenutzer("theAdmin", DEUTSCHLAND).id(UUID.randomUUID().toString())
                                                                          .addRollen(GESAMTADMIN)
                                                                          .build();
            var toUpdate = aBehoerdenbenutzer("theAdmin", HAMBURG).id(existingUser.getId())
                                                                  .addRollen(BENUTZERVERWALTUNG_READ)
                                                                  .themen(BODEN)
                                                                  .fachmodule(E_ERKLAERUNG)
                                                                  .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(existingUser);

            assertThrows(AccessDeniedException.class, () -> service.updateBenutzer(toUpdate));

            verify(keycloakService).getUserById(toUpdate.getId());
            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void deleteUser_accessDenied() {
            assertThrows(AccessDeniedException.class, () -> service.deleteBenutzer("anyone"));

            verifyNoInteractions(keycloakService);
        }
    }

    @Nested
    @WithBubeMockUser(roles = { BENUTZERVERWALTUNG_READ, BEHOERDENBENUTZER }, land = HAMBURG)
    class AsBenutzerverwaltungReadUser {
        @Test
        void createUser_accessDenied() {
            var toCreate = aBetreiberbenutzer("newUser", HAMBURG).build();

            assertThrows(AccessDeniedException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void updateUser_accessDenied() {
            var toUpdate = aBetreiberbenutzer("newUser", HAMBURG).build();
            assertThrows(AccessDeniedException.class, () -> service.updateBenutzer(toUpdate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void deleteUser_accessDenied() {
            assertThrows(AccessDeniedException.class, () -> service.deleteBenutzer("anyone"));

            verifyNoInteractions(keycloakService);
        }
    }

    @Nested
    @WithBubeMockUser(name = "myself", roles = BEHOERDE_READ, land = HAMBURG)
    class WithoutBenutzerverwaltungsrecht {

        @Test
        void updateUser_accessDenied() {
            var toUpdate = aBehoerdenbenutzer("newUser", HAMBURG).build();
            assertThrows(AccessDeniedException.class, () -> service.updateBenutzer(toUpdate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void updateSelf() {
            var toUpdate = aBehoerdenbenutzer("myself", HAMBURG)
                    .id(UUID.randomUUID().toString())
                    .addRollen(BEHOERDENBENUTZER, BEHOERDE_READ)
                    .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            service.updateCurrentBenutzer(toUpdate);

            verify(keycloakService).updateUser(toUpdate);
        }

        @Test
        void updateSelf_denyRoleEscalation() {
            var updatedUserBuilder = aBehoerdenbenutzer("myself", HAMBURG).id(UUID.randomUUID().toString())
                                                                          .addRollen(BENUTZERVERWALTUNG_READ);
            var beforeUpdate = updatedUserBuilder.build();
            var afterUpdate = updatedUserBuilder.addRollen(BENUTZERVERWALTUNG_DELETE).build();
            when(keycloakService.getUserById(afterUpdate.getId())).thenReturn(beforeUpdate);

            assertThrows(BenutzerException.class, () -> service.updateCurrentBenutzer(afterUpdate));

            verify(keycloakService).getUserById(afterUpdate.getId());
            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void updateSelf_denyDatenberechtigungEscalation() {
            var toUpdate = aBehoerdenbenutzer("myself", DEUTSCHLAND).id(UUID.randomUUID().toString())
                                                                    .addRollen(BEHOERDE_READ)
                                                                    .build();
            when(keycloakService.getUserById(toUpdate.getId())).thenReturn(toUpdate);

            assertThrows(AccessDeniedException.class, () -> service.updateCurrentBenutzer(toUpdate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void updateOther() {
            var toUpdate = aBehoerdenbenutzer("newUser", HAMBURG).build();

            assertThrows(IllegalArgumentException.class, () -> service.updateCurrentBenutzer(toUpdate));

            verifyNoInteractions(keycloakService);
        }
    }

    @Nested
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BENUTZERVERWALTUNG_WRITE }, land = HAMBURG,
            fachmodule = { RECHERCHE, PRTR_LCP }, themen = { ABFALL, BODEN })
    class WithFachmodulAndThemenRestriction {
        @Test
        void createUser_withSameRestrictions() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .fachmodule(RECHERCHE, PRTR_LCP)
                                                                 .themen(ABFALL, BODEN)
                                                                 .build();

            service.createBenutzer(toCreate);

            verify(keycloakService).createUser(toCreate);
        }

        @Test
        void createUser_denyOtherThema() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .fachmodule(RECHERCHE, PRTR_LCP)
                                                                 .themen(ABFALL, BODEN, LUFT)
                                                                 .build();

            assertThrows(BenutzerException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void createUser_denyOtherFachmodul() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .fachmodule(RECHERCHE, PRTR_LCP, E_ERKLAERUNG)
                                                                 .themen(ABFALL, BODEN)
                                                                 .build();

            assertThrows(BenutzerException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void createUser_denyNoThema() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .fachmodule(RECHERCHE, PRTR_LCP)
                                                                 .build();

            assertThrows(BenutzerException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }

        @Test
        void createUser_denyNoFachmodul() {
            var toCreate = aBehoerdenbenutzer("newUser", HAMBURG).addRollen(BENUTZERVERWALTUNG_WRITE)
                                                                 .themen(ABFALL, BODEN)
                                                                 .build();

            assertThrows(BenutzerException.class, () -> service.createBenutzer(toCreate));

            verifyNoInteractions(keycloakService);
        }
    }
}
