package de.wps.bube.basis.stammdatenbetreiber.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetreiberGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.BetriebsstaetteGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.QuelleGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.service.SucheService;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAdresse;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlageListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbAnlagenteilListItem;
import de.wps.bube.basis.stammdatenbetreiber.domain.vo.SpbQuelleListItem;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbAnlageRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbAnlagenteilRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbBetreiberRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbBetriebsstaetteRepository;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbQuelleRepository;

@ExtendWith(MockitoExtension.class)
class StammdatenBetreiberServiceTest {

    public final Betreiber BETREIBER = BetreiberBuilder.aFullBetreiber().build();
    public final Betriebsstaette BETRIEBSSTAETTE = BetriebsstaetteBuilder.aFullBetriebsstaette().build();
    public final Anlage ANLAGE = AnlageBuilder.aFullAnlage().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();
    public final Anlagenteil ANLAGENTEIL = AnlagenteilBuilder.aFullAnlagenteil().parentAnlageId(ANLAGE.getId()).build();
    public final Quelle QUELLE = QuelleBuilder.aFullQuelle().parentAnlageId(ANLAGE.getId()).build();

    private StammdatenBetreiberService service;

    @Mock
    private SpbBetreiberRepository spbBetreiberRepository;
    @Mock
    private SpbBetriebsstaetteRepository spbBetriebsstaetteRepository;
    @Mock
    private SpbAnlagenteilRepository spbAnlagenteilRepository;
    @Mock
    private SpbAnlageRepository spbAnlageRepository;
    @Mock
    private SpbQuelleRepository spbQuelleRepository;
    @Mock
    private SucheService sucheService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private SpbBetriebsstaetteUpdater spbBetriebsstaetteUpdater;
    @Mock
    private UpdateStammdatenDurchBetreiberAenderungService aenderungService;

    @BeforeEach
    void setUp() {

        service = new StammdatenBetreiberService(
                spbBetriebsstaetteRepository,
                spbAnlageRepository,
                spbAnlagenteilRepository,
                spbBetreiberRepository,
                spbQuelleRepository,
                sucheService,
                stammdatenService,
                aenderungService,
                spbBetriebsstaetteUpdater);

        lenient().when(stammdatenService.checkBetriebsstaetteDatenberechtigungWrite(BETRIEBSSTAETTE.getId()))
                 .thenReturn(BETRIEBSSTAETTE);
        lenient().when(stammdatenService.loadBetriebsstaetteForRead(BETRIEBSSTAETTE.getId()))
                 .thenReturn(BETRIEBSSTAETTE);
        lenient().when(stammdatenService.checkAnlageDatenberechtigungWrite(ANLAGE.getId())).thenReturn(ANLAGE);
    }

    @Test
    void shouldDelegateSucheToService() {
        service.sucheIds(2020L);

        verify(sucheService).sucheIds(eq(2020L), any());
    }

    @Test
    void loadSpbBetriebssstaetteById() {
        when(spbBetriebsstaetteRepository.getById(15L)).thenReturn(new SpbBetriebsstaette(BETRIEBSSTAETTE));

        service.loadSpbBetriebsstaette(15L);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
    }

    @Test
    void deleteSpbBetriebsstaetteForUebernahme() {
        var spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);

        service.deleteSpbBetriebsstaetteUebernahme(spbBetriebsstaette);

        verify(spbBetriebsstaetteRepository).delete(spbBetriebsstaette);
    }

    @Test
    void findSpbBetriebsstaetteByBetriebsstaetteId() {
        SpbBetriebsstaette spbMock = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(any())).thenReturn(Optional.of(spbMock));

        service.findSpbBetriebsstaetteByBetriebsstaetteId(15L);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(15L);
        verify(spbBetriebsstaetteRepository).findOneByBetriebsstaetteId(15L);
    }

    @Test
    void loadSpbBetriebsstaetteByJahrLandNummer_empty() {
        when(stammdatenService.loadBetriebsstaetteByJahrLandNummer(any(), any(), any())).thenReturn(BETRIEBSSTAETTE);
        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(any())).thenReturn(Optional.empty());

        SpbBetriebsstaette spbBetriebsstaette = service.loadSpbBetriebsstaetteByJahrLandNummer("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getBetriebsstaetteNr());

        assertThat(spbBetriebsstaette.getName()).isEqualTo(BETRIEBSSTAETTE.getName());
    }

    @Test
    void loadSpbBetriebsstaetteByJahrLandNummer_WithChangedValue() {
        when(stammdatenService.loadBetriebsstaetteByJahrLandNummer(any(), any(), any())).thenReturn(BETRIEBSSTAETTE);

        SpbBetriebsstaette spbMock = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbMock.setName("changedName");

        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(any())).thenReturn(Optional.of(spbMock));

        SpbBetriebsstaette spbBetriebsstaette = service.loadSpbBetriebsstaetteByJahrLandNummer("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getBetriebsstaetteNr());

        assertThat(spbBetriebsstaette.getName()).isEqualTo(spbMock.getName());
    }

    @Test
    void updateSpbBetriebsstaette() {
        SpbBetriebsstaette spbMock = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbMock.setId(15L);

        when(spbBetriebsstaetteRepository.findById(15L)).thenReturn(Optional.of(spbMock));

        service.updateSpbBetriebsstaette(spbMock);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(BETRIEBSSTAETTE.getId());
        verify(spbBetriebsstaetteUpdater).update(any(), any());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbBetriebsstaette.class));
        verify(spbBetriebsstaetteRepository).save(spbMock);
    }

    @Test
    void createSpbBetriebsstaette() {
        SpbBetriebsstaette spbMock = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbMock.setErsteErfassung(null);

        service.createSpbBetriebsstaette(spbMock);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(BETRIEBSSTAETTE.getId());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbBetriebsstaette.class));
        verify(spbBetriebsstaetteRepository).save(spbMock);
        assertThat(spbMock.getErsteErfassung()).isEqualTo(BETRIEBSSTAETTE.getErsteErfassung());
    }

    @Test
    void handleBetriebsstaetteGeloeschtEvent() {
        BetriebsstaetteGeloeschtEvent event = new BetriebsstaetteGeloeschtEvent(BETRIEBSSTAETTE.getId());

        SpbBetriebsstaette spbMock = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbMock.setId(15L);

        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(
                Optional.of(spbMock));
        when(spbAnlageRepository.findByParentBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(
                Collections.emptyList());
        service.handleBetriebsstaetteGeloeschtEvent(event);

        verify(spbBetriebsstaetteRepository).delete(spbMock);
    }

    @Test
    void handleBetriebsstaetteGeloeschtEventohneSpbBST() {
        BetriebsstaetteGeloeschtEvent event = new BetriebsstaetteGeloeschtEvent(1L);

        SpbAnlage spbAnlageMock = new SpbAnlage();

        spbAnlageMock.setId(2L);
        spbAnlageMock.setParentBetriebsstaetteId(1L);

        when(spbAnlageRepository.findByParentBetriebsstaetteId(1L)).thenReturn(
                List.of(spbAnlageMock));

        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(1L)).thenReturn(
                Optional.empty());

        when(stammdatenService.checkBetriebsstaetteDatenberechtigungDelete(
                spbAnlageMock.getParentBetriebsstaetteId())).thenReturn(BETRIEBSSTAETTE);

        service.handleBetriebsstaetteGeloeschtEvent(event);

        verify(spbAnlagenteilRepository).deleteAllByParentSpbAnlageId(2L);
        verify(spbAnlageRepository).deleteById(spbAnlageMock.getId());

    }

    @Test
    void listAllNeueSpbAnlagenAsListItemByBetriebsstaettenId() {

        when(spbAnlageRepository.findByParentBetriebsstaetteIdAndAnlageIdIsNull(15L)).thenReturn(
                List.of(new SpbAnlage(ANLAGE)));

        List<SpbAnlageListItem> spbAnlageListItems = service.listAllNeueSpbAnlagenAsListItemByBetriebsstaettenId(15L);

        assertThat(spbAnlageListItems.size()).isEqualTo(1);
        assertThat(spbAnlageListItems.get(0).getAnlageNr()).isEqualTo(ANLAGE.getAnlageNr());
        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(15L);
    }

    @Test
    void listAllSpbAnlagenAsListItemByBetriebsstaettenId() {

        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setAnlageId(163L);
        spbAnlageMock.setId(99L);

        when(spbAnlageRepository.findByParentBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(
                List.of(spbAnlageMock));
        when(stammdatenService.loadAllAnlagenByBetriebsstaettenId(BETRIEBSSTAETTE.getId())).thenReturn(List.of(ANLAGE));

        List<SpbAnlageListItem> listItems = service.listAllSpbAnlagenAsListItemByBetriebsstaettenId(
                BETRIEBSSTAETTE.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(listItems.stream().filter(l -> l.getSpbAnlageId() == null).count()).isEqualTo(1);
        assertThat(listItems.stream().filter(l -> spbAnlageMock.getId().equals(l.getSpbAnlageId())).count()).isEqualTo(
                1);
    }

    @Test
    void loadSpbAnlage_bstId_AnlNr_Vorhanden() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(99L);

        when(spbAnlageRepository.findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(BETRIEBSSTAETTE.getId(),
                ANLAGE.getAnlageNr())).thenReturn(Optional.of(spbAnlageMock));

        SpbAnlage spbAnlage = service.loadSpbAnlage(BETRIEBSSTAETTE.getId(), ANLAGE.getAnlageNr());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbAnlage).isEqualTo(spbAnlageMock);
    }

    @Test
    void loadSpbAnlage_bstId_AnlNr_Empty() {
        when(spbAnlageRepository.findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(BETRIEBSSTAETTE.getId(),
                ANLAGE.getAnlageNr())).thenReturn(Optional.empty());
        when(stammdatenService.loadAnlageByAnlageNummer(BETRIEBSSTAETTE.getId(), ANLAGE.getAnlageNr())).thenReturn(
                ANLAGE);

        SpbAnlage spbAnlage = service.loadSpbAnlage(BETRIEBSSTAETTE.getId(), ANLAGE.getAnlageNr());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbAnlage.getAnlageId()).isEqualTo(ANLAGE.getId());
    }

    @Test
    void loadSpbAnlage_NoSpbAnlage() {
        when(spbAnlageRepository.findById(any())).thenReturn(Optional.empty());
        try {
            service.loadSpbAnlage(15L);
        } catch (BubeEntityNotFoundException e) {
            assertThat(e.getMessage()).isEqualTo(
                    "SpbAnlage mit ID 15 wurde nicht gefunden oder keine Berechtigung zum Öffnen des Objektes");
        }
    }

    @Test
    void findSpbAnlageByAnlageId() {
        service.findSpbAnlageByAnlageId(15L);
        verify(spbAnlageRepository).findOneByAnlageId(15L);
    }

    @Test
    void createSpbAnlage() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setErsteErfassung(null);

        when(spbAnlageRepository
                .existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(BETRIEBSSTAETTE.getId(), ANLAGE.getAnlageNr()))
                .thenReturn(false);
        when(spbAnlageRepository.save(spbAnlageMock)).thenReturn(spbAnlageMock);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);

        SpbAnlage spbAnlage = service.createSpbAnlage(spbAnlageMock);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbAnlage.class));
        verify(spbAnlageRepository).save(spbAnlageMock);
        assertThat(spbAnlage.getErsteErfassung()).isEqualTo(ANLAGE.getErsteErfassung());
    }

    @Test
    void aktualisiereSpbNachAnlageUebernahme() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(564L);

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setId(456L);

        SpbQuelle spbQuelle = new SpbQuelle();
        spbQuelle.setId(451L);

        when(spbAnlageRepository.findById(spbAnlageMock.getId())).thenReturn(Optional.of(spbAnlageMock));
        when(stammdatenService.canReadBetriebsstaette(spbAnlageMock.getParentBetriebsstaetteId())).thenReturn(true);

        when(spbAnlagenteilRepository.findByParentSpbAnlageId(spbAnlageMock.getId())).thenReturn(
                List.of(spbAnlagenteil));
        when(spbQuelleRepository.findByParentSpbAnlageId(spbAnlageMock.getId())).thenReturn(List.of(spbQuelle));

        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);

        service.aktualisiereSpbNachAnlageUebernahme(ANLAGE.getId(), spbAnlageMock.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(BETRIEBSSTAETTE.getId());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbAnlagenteil.class), any());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbQuelle.class));
    }

    @Test
    void updateSpbAnlage() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(99L);

        service.updateSpbAnlage(spbAnlageMock);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbAnlage.class));
        verify(spbAnlageRepository).save(spbAnlageMock);
    }

    @Test
    void deleteSpbAnlage() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(99L);

        when(stammdatenService.checkBetriebsstaetteDatenberechtigungDelete(BETRIEBSSTAETTE.getId()))
                .thenReturn(BETRIEBSSTAETTE);

        service.deleteSpbAnlage(spbAnlageMock);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungDelete(ANLAGE.getParentBetriebsstaetteId());
        verify(spbAnlagenteilRepository).deleteAllByParentSpbAnlageId(any());
        verify(spbAnlagenteilRepository).flush();
        verify(spbAnlageRepository).deleteById(spbAnlageMock.getId());
    }

    @Test
    void deleteUebernommeneSpbAnlage() {
        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        service.deleteUebernommeneSpbAnlage(spbAnlage);

        verify(stammdatenService, never()).aenderungAnlageDurchBetreiber(any());
        verify(spbAnlageRepository).delete(spbAnlage);
        verify(spbAnlageRepository).flush();
    }

    @Test
    void handleAnlageAngelegtEvent() {
        SpbAnlage spbAnlage = new SpbAnlage();

        AnlageAngelegtEvent event = new AnlageAngelegtEvent(ANLAGE);
        when(spbAnlageRepository.findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(
                ANLAGE.getParentBetriebsstaetteId(),
                ANLAGE.getAnlageNr()))
                .thenReturn(Optional.of(spbAnlage));

        service.handleAnlageAngelegtEvent(event);

        verify(stammdatenService).checkAnlageDatenberechtigungWrite(ANLAGE.getId());
        verify(aenderungService).updateAenderungDurchBetreiber(spbAnlage);
        assertThat(spbAnlage.getAnlageId()).isEqualTo(ANLAGE.getId());
    }

    @Test
    void handleAnlageGeloeschtEvent() {
        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        AnlageGeloeschtEvent event = new AnlageGeloeschtEvent(ANLAGE.getId(), ANLAGE.getParentBetriebsstaetteId());
        when(spbAnlageRepository.findOneByAnlageId(event.anlageId)).thenReturn(Optional.of(spbAnlage));

        service.handleAnlageGeloeschtEvent(event);

        verify(spbAnlageRepository).deleteById(spbAnlage.getId());
        verify(spbAnlagenteilRepository).deleteAllByParentSpbAnlageId(any());
    }

    @Test
    void listAllSpbAnlagenteileAsListItemByAnlageId() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(66945L);

        SpbAnlagenteil spbAnlagenteilMock = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteilMock.setId(3245623L);
        spbAnlagenteilMock.setAnlagenteilId(156132L);

        when(spbAnlagenteilRepository.findByParentAnlageId(ANLAGE.getId())).thenReturn(List.of(spbAnlagenteilMock));
        when(stammdatenService.loadAllAnlagenteileByAnlageId(ANLAGE.getId(), BETRIEBSSTAETTE.getId())).thenReturn(
                List.of(ANLAGENTEIL));

        List<SpbAnlagenteilListItem> spbAnlagenteilListItems = service.listAllSpbAnlagenteileAsListItemByAnlageId(
                ANLAGE.getId(), spbAnlageMock.getParentBetriebsstaetteId());

        assertThat(spbAnlagenteilListItems.stream().map(SpbAnlagenteilListItem::getAnlagenteilNr).toList()).contains(
                spbAnlagenteilMock.getAnlagenteilNr());
        verify(stammdatenService).loadAnlage(spbAnlageMock.getAnlageId());

        assertThat(spbAnlagenteilListItems.stream()
                                          .filter(listItem -> listItem.getSpbAnlagenteilId() != null && listItem.getSpbAnlagenteilId()
                                                                                                                .equals(spbAnlagenteilMock.getId()))
                                          .count()).isEqualTo(1);
        assertThat(spbAnlagenteilListItems.stream()
                                          .filter(listItem -> listItem.getAnlagenteilNr()
                                                                      .equals(ANLAGENTEIL.getAnlagenteilNr()))
                                          .count()).isEqualTo(2);
    }

    @Test
    void listAllSpbAnlagenteileAsListItemBySpbAnlageId() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(66945L);

        SpbAnlagenteil spbAnlagenteilMock = new SpbAnlagenteil(ANLAGENTEIL);

        when(spbAnlagenteilRepository.findByParentSpbAnlageId(spbAnlageMock.getId())).thenReturn(
                List.of(spbAnlagenteilMock));

        List<SpbAnlagenteilListItem> spbAnlagenteilListItems = service.listAllSpbAnlagenteileAsListItemBySpbAnlageId(
                spbAnlageMock.getId(), spbAnlageMock.getParentBetriebsstaetteId());

        assertThat(spbAnlagenteilListItems.stream().map(SpbAnlagenteilListItem::getAnlagenteilNr)).contains(
                spbAnlagenteilMock.getAnlagenteilNr());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
    }

    @Test
    void listAllNeueSpbAnlagenteileAsListItemByAnlageId() {

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setAnlagenteilId(null);

        when(spbAnlagenteilRepository.findByParentAnlageIdAndAnlagenteilIdIsNull(ANLAGE.getId())).thenReturn(
                List.of(spbAnlagenteil));

        List<SpbAnlagenteilListItem> spbAnlagenteilListItems = service.listAllNeueSpbAnlagenteileAsListItemByAnlageId(
                ANLAGE.getId());

        verify(stammdatenService).loadAnlage(ANLAGE.getId());
        assertThat(spbAnlagenteilListItems.size()).isEqualTo(1);
        assertThat(spbAnlagenteilListItems.get(0).getAnlagenteilNr()).isEqualTo(ANLAGENTEIL.getAnlagenteilNr());
    }

    @Test
    void loadSpbAnlagenteil_HasSpbAnlage() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(66945L);

        SpbAnlagenteil spbAnlagenteilMock = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteilMock.setParentSpbAnlageId(spbAnlageMock.getId());

        when(spbAnlagenteilRepository.findOneByParentSpbAnlageIdAndAnlagenteilNrIgnoreCase(ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr())).thenReturn(Optional.of(spbAnlagenteilMock));

        SpbAnlagenteil spbAnlagenteil = service.loadSpbAnlagenteilByAnlageteilNummer(BETRIEBSSTAETTE.getId(),
                ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr(), true);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbAnlagenteil.getParentSpbAnlageId()).isEqualTo(spbAnlageMock.getId());
    }

    @Test
    void loadSpbAnlagenteil_HasNoSpbAnlage() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(66945L);

        SpbAnlagenteil spbAnlagenteilMock = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteilMock.setParentSpbAnlageId(spbAnlageMock.getId());

        when(spbAnlagenteilRepository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr())).thenReturn(Optional.of(spbAnlagenteilMock));

        SpbAnlagenteil spbAnlagenteil = service.loadSpbAnlagenteilByAnlageteilNummer(BETRIEBSSTAETTE.getId(),
                ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr(), false);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbAnlagenteil.getParentAnlageId()).isEqualTo(ANLAGE.getId());
    }

    @Test
    void loadSpbAnlagenteil_HasNoSpbAnlage_NoSpbAnlagenteil() {
        SpbAnlage spbAnlageMock = new SpbAnlage(ANLAGE);
        spbAnlageMock.setId(66945L);

        SpbAnlagenteil spbAnlagenteilMock = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteilMock.setParentSpbAnlageId(spbAnlageMock.getId());

        when(spbAnlagenteilRepository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr())).thenReturn(Optional.empty());
        when(stammdatenService.loadAnlagenteilByAnlagenteilNummer(BETRIEBSSTAETTE.getId(), ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr()))
                .thenReturn(ANLAGENTEIL);

        SpbAnlagenteil spbAnlagenteil = service.loadSpbAnlagenteilByAnlageteilNummer(BETRIEBSSTAETTE.getId(),
                ANLAGE.getId(),
                ANLAGENTEIL.getAnlagenteilNr(), false);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(
                ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbAnlagenteil.getParentAnlageId()).isEqualTo(ANLAGE.getId());
    }

    @Test
    void loadSpbAnlagenteil_ParentAnlage() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setParentAnlageId(ANLAGE.getId());
        spbAnlagenteil.setId(9L);

        when(spbAnlagenteilRepository.findById(spbAnlagenteil.getId())).thenReturn(Optional.of(spbAnlagenteil));

        SpbAnlagenteil spbAnlagenteil1 = service.loadSpbAnlagenteilById(spbAnlagenteil.getId());
        verify(stammdatenService).loadAnlage(spbAnlagenteil.getParentAnlageId());
        assertThat(spbAnlagenteil1.getAnlagenteilId()).isEqualTo(spbAnlagenteil.getAnlagenteilId());
    }

    @Test
    void loadSpbAnlagenteil_ParentSpbAnlage() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setParentAnlageId(null);
        spbAnlagenteil.setParentSpbAnlageId(100L);
        spbAnlagenteil.setId(9L);

        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(100L);

        when(spbAnlagenteilRepository.findById(spbAnlagenteil.getId())).thenReturn(Optional.of(spbAnlagenteil));
        when(stammdatenService.canReadBetriebsstaette(ANLAGE.getParentBetriebsstaetteId())).thenReturn(true);
        when(spbAnlageRepository.findById(spbAnlagenteil.getParentSpbAnlageId())).thenReturn(Optional.of(spbAnlage));

        SpbAnlagenteil spbAnlagenteil1 = service.loadSpbAnlagenteilById(spbAnlagenteil.getId());

        assertThat(spbAnlagenteil1.getAnlagenteilId()).isEqualTo(spbAnlagenteil.getAnlagenteilId());
    }

    @Test
    void createSpbAnlagenteil_parentAnlage() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);

        when(stammdatenService.loadAnlage(spbAnlagenteil.getParentAnlageId())).thenReturn(ANLAGE);
        when(stammdatenService.loadAnlagenteil(spbAnlagenteil.getAnlagenteilId())).thenReturn(ANLAGENTEIL);
        when(spbAnlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(
                spbAnlagenteil.getParentAnlageId(),
                spbAnlagenteil.getAnlagenteilNr())).thenReturn(false);

        service.createSpbAnlagenteil(spbAnlagenteil);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbAnlagenteil.class), any());
        verify(spbAnlagenteilRepository).save(spbAnlagenteil);
        assertThat(spbAnlagenteil.getErsteErfassung()).isEqualTo(ANLAGENTEIL.getErsteErfassung());
    }

    @Test
    void createSpbAnlagenteil_parentSpbAnlage() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setParentAnlageId(null);
        spbAnlagenteil.setAnlagenteilId(null);
        spbAnlagenteil.setParentSpbAnlageId(156L);

        when(spbAnlageRepository.findById(any())).thenReturn(Optional.of(new SpbAnlage(ANLAGE)));
        when(stammdatenService.canReadBetriebsstaette(ANLAGE.getParentBetriebsstaetteId())).thenReturn(true);
        when(spbAnlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(
                spbAnlagenteil.getParentAnlageId(), spbAnlagenteil.getAnlagenteilNr()))
                .thenReturn(false);

        service.createSpbAnlagenteil(spbAnlagenteil);

        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbAnlagenteil.class), any());
        verify(spbAnlagenteilRepository).save(spbAnlagenteil);
    }

    @Test
    void updateSpbAnlagenteil() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setId(546L);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);

        service.updateSpbAnlagenteil(spbAnlagenteil);

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());
        verify(spbAnlagenteilRepository).save(spbAnlagenteil);
    }

    @Test
    void deleteSpbAnlagenteil() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);

        when(spbAnlagenteilRepository.findById(spbAnlagenteil.getId())).thenReturn(Optional.of(spbAnlagenteil));

        service.deleteSpbAnlagenteil(BETRIEBSSTAETTE.getId(), spbAnlagenteil.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());

        verify(spbAnlagenteilRepository).delete(spbAnlagenteil);
    }

    @Test
    void handleAnlagenteilGeloeschtEvent() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        AnlagenteilGeloeschtEvent event = new AnlagenteilGeloeschtEvent(ANLAGENTEIL.getId(), BETRIEBSSTAETTE.getId());

        when(spbAnlagenteilRepository.findOneByAnlagenteilId(ANLAGENTEIL.getId())).thenReturn(
                Optional.of(spbAnlagenteil));

        service.handleAnlagenteilGeloeschtEvent(event);

        verify(spbAnlagenteilRepository).findOneByAnlagenteilId(event.anlagenteilId);
        verify(spbAnlagenteilRepository).delete(spbAnlagenteil);
    }

    @Test
    void handleAnlagenteilAngelegtEvent() {
        AnlagenteilAngelegtEvent event = new AnlagenteilAngelegtEvent(ANLAGENTEIL);
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil();

        when(spbAnlagenteilRepository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(ANLAGENTEIL.getParentAnlageId(),
                ANLAGENTEIL.getAnlagenteilNr())).thenReturn(Optional.of(
                spbAnlagenteil));

        service.handleAnlagenteilAngelegtEvent(event);

        assertThat(spbAnlagenteil.getAnlagenteilId()).isEqualTo(ANLAGENTEIL.getId());
    }

    @Test
    void handleBetreiberGeloeschtEvent() {
        BetreiberGeloeschtEvent event = new BetreiberGeloeschtEvent(BETREIBER.getId());

        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbBetriebsstaette.setSpbBetreiber(spbBetreiber);

        when(spbBetreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.canReadBetreiber(BETREIBER.getId())).thenReturn(true);
        when(spbBetreiberRepository.findById(spbBetreiber.getId())).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.loadBetreiber(BETREIBER.getId())).thenReturn(BETREIBER);
        when(spbBetriebsstaetteRepository.findOneBySpbBetreiberId(spbBetreiber.getId())).thenReturn(
                Optional.of(spbBetriebsstaette));

        service.handleBetreiberGeloeschtEvent(event);

        verify(spbBetreiberRepository).deleteById(spbBetreiber.getId());
        assertThat(spbBetriebsstaette.getSpbBetreiber()).isNull();
    }

    @Test
    void loadSpbBetreiberByJahrLandBetriebsstaetteNummer_hasBetreiberAndSPBBetreiber() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        Betriebsstaette bst = BetriebsstaetteBuilder.aFullBetriebsstaette().betreiber(BETREIBER).build();

        when(stammdatenService.loadBetriebsstaetteByJahrLandNummer("2020", Land.HAMBURG,
                bst.getBetriebsstaetteNr())).thenReturn(bst);
        when(spbBetreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.canReadBetreiber(any())).thenReturn(true);

        SpbBetreiber spbBetreiber1 = service.loadSpbBetreiberByJahrLandBetriebsstaetteNummer("2020", Land.HAMBURG,
                bst.getBetriebsstaetteNr());

        assertThat(spbBetreiber1).isEqualTo(spbBetreiber);
    }

    @Test
    void loadSpbBetreiberByJahrLandBetriebsstaetteNummer_hasBetreiberNoSPBBetreiber() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        when(stammdatenService.loadBetriebsstaetteByJahrLandNummer("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getBetriebsstaetteNr())).thenReturn(BETRIEBSSTAETTE);
        when(spbBetreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        SpbBetreiber spbBetreiber1 = service.loadSpbBetreiberByJahrLandBetriebsstaetteNummer("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getBetriebsstaetteNr());

        assertThat(spbBetreiber1.getBetreiberId()).isEqualTo(BETREIBER.getId());
    }

    @Test
    void loadSpbBetreiberByJahrLandBetriebsstaetteLocalId() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        when(stammdatenService.loadBetriebsstaetteByJahrLandLocalId("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getLocalId())).thenReturn(BETRIEBSSTAETTE);
        when(spbBetreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.canReadBetreiber(BETREIBER.getId())).thenReturn(true);

        SpbBetreiber spbBetreiber1 = service.loadSpbBetreiberByJahrLandBetriebsstaetteLocalId("2020", Land.HAMBURG,
                BETRIEBSSTAETTE.getLocalId());

        assertThat(spbBetreiber1).isEqualTo(spbBetreiber);
    }

    @Test
    void findSpbBetreiberByBetreiberId() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        when(spbBetreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.canReadBetreiber(any())).thenReturn(true);

        Optional<SpbBetreiber> spbBetreiberByBetreiberId = service.findSpbBetreiberByBetreiberId(spbBetreiber.getId());

        assertThat(spbBetreiberByBetreiberId).contains(spbBetreiber);
    }

    @Test
    void createSpbBetreiber_WithBetreiber() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);

        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(BETRIEBSSTAETTE.getId()))
                .thenReturn(Optional.of(spbBetriebsstaette));

        service.createSpbBetreiber(BETRIEBSSTAETTE.getId(), spbBetreiber);

        verify(spbBetreiberRepository).save(spbBetreiber);
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbBetriebsstaette.class));
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbBetreiber.class), any());
        assertThat(spbBetreiber.getErsteErfassung()).isEqualTo(BETREIBER.getErsteErfassung());
    }

    @Test
    void updateSpbBetreiber() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        BETREIBER.setBetreiberdatenUebernommen(true);

        service.updateSpbBetreiber(spbBetreiber, BETRIEBSSTAETTE.getId());

        verify(spbBetreiberRepository).save(spbBetreiber);
        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbBetreiber.class), any());
    }

    @Test
    void deleteSpbBetreiber() {
        SpbBetreiber spbBetreiber = new SpbBetreiber(BETREIBER);
        spbBetreiber.setId(15L);

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbBetriebsstaette.setSpbBetreiber(spbBetreiber);

        when(spbBetreiberRepository.findById(spbBetreiber.getId())).thenReturn(Optional.of(spbBetreiber));
        when(stammdatenService.loadBetreiber(BETREIBER.getId())).thenReturn(BETREIBER);
        when(spbBetriebsstaetteRepository.findOneBySpbBetreiberId(spbBetreiber.getId())).thenReturn(
                Optional.of(spbBetriebsstaette));

        SpbBetreiber spbBetreiber1 = service.deleteSpbBetreiber(15L);

        verify(spbBetreiberRepository).deleteById(spbBetreiber.getId());
        assertThat(spbBetreiber1).isNotNull();
    }

    @Test
    void findSpbQuelleByQuelleId() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);

        when(spbQuelleRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbQuelle));
        when(stammdatenService.canReadQuelle(any())).thenReturn(true);

        Optional<SpbQuelle> quelle = service.findSpbQuelleByQuelleId(spbQuelle.getId());

        assertThat(quelle).contains(spbQuelle);
    }

    @Test
    void handleQuelleAngelegtEvent() {
        SpbQuelle spbQuelle = new SpbQuelle();

        QuelleAngelegtEvent event = new QuelleAngelegtEvent(QUELLE);
        when(spbQuelleRepository.findOneByParentAnlageIdAndQuelleNrIgnoreCase(
                QUELLE.getParentAnlageId(),
                QUELLE.getQuelleNr()))
                .thenReturn(Optional.of(spbQuelle));

        service.handleQuelleAngelegtEvent(event);

        verify(aenderungService).updateAenderungDurchBetreiber(spbQuelle);
        assertThat(spbQuelle.getQuelleId()).isEqualTo(QUELLE.getId());
    }

    @Test
    void handleQuelleGeloeschtEvent() {
        QuelleGeloeschtEvent event = new QuelleGeloeschtEvent(QUELLE.getId());

        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);

        when(spbQuelleRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(spbQuelle));
        when(stammdatenService.canReadQuelle(any())).thenReturn(true);

        service.handleQuelleGeloeschtEvent(event);

        verify(spbQuelleRepository).deleteById(spbQuelle.getId());
    }

    @Test
    void createSpbQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setQuelleId(null);
        spbQuelle.setId(null);
        spbQuelle.setParentAnlageId(ANLAGE.getId());
        spbQuelle.setParentSpbAnlageId(ANLAGE.getId());
        spbQuelle.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        when(stammdatenService.findQuelle(BETRIEBSSTAETTE.getId(), QUELLE.getQuelleNr())).thenReturn(
                Optional.of(QUELLE));
        when(stammdatenService.loadQuelle(QUELLE.getId())).thenReturn(QUELLE);
        when(stammdatenService.checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId()))
                .thenReturn(BETRIEBSSTAETTE);
        when(spbQuelleRepository.save(spbQuelle)).thenReturn(spbQuelle);

        service.createSpbQuelle(spbQuelle);

        verify(stammdatenService, times(2)).checkBetriebsstaetteDatenberechtigungRead(
                ANLAGE.getParentBetriebsstaetteId());

        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbQuelle.class));
        verify(spbQuelleRepository).save(spbQuelle);
        assertThat(spbQuelle.getErsteErfassung()).isEqualTo(QUELLE.getErsteErfassung());
    }

    @Test
    void updateSpbQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());
        spbQuelle.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());
        when(stammdatenService.checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId()))
                .thenReturn(BETRIEBSSTAETTE);

        service.updateSpbQuelle(spbQuelle);

        verify(aenderungService).updateAenderungDurchBetreiber(any(SpbQuelle.class));
        verify(spbQuelleRepository).save(spbQuelle);
    }

    @Test
    void deleteSpbQuelle() {

        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);

        service.deleteSpbQuelleFromRest(BETRIEBSSTAETTE.getId(), spbQuelle.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungWrite(ANLAGE.getParentBetriebsstaetteId());
        verify(spbQuelleRepository).deleteById(spbQuelle.getId());
    }

    @Test
    void listAllSpbQuellenAsListItemByBetriebsstaettenId() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setQuelleNr("SPB");
        spbQuelle.setQuelleId(null);
        Quelle splQuelle = QuelleBuilder.aFullQuelle().id(334L).parentAnlageId(ANLAGE.getId()).quelleNr("SPL").build();

        when(spbQuelleRepository.findByParentBetriebsstaetteId(BETRIEBSSTAETTE.getId()))
                .thenReturn(List.of(spbQuelle));
        when(stammdatenService.loadAllQuellenByBetriebsstaettenId(BETRIEBSSTAETTE.getId()))
                .thenReturn(List.of(splQuelle));

        Collection<SpbQuelleListItem> spbQuelleListItems = service
                .listAllSpbQuellenAsListItemByBetriebsstaettenId(BETRIEBSSTAETTE.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        assertThat(spbQuelleListItems).map(SpbQuelleListItem::getQuelleNr)
                                      .containsExactlyInAnyOrder(spbQuelle.getQuelleNr(), splQuelle.getQuelleNr());
    }

    @Test
    void listAllNeueSpbQuellenAsListItemByBetriebsstaettenId() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);

        when(spbQuelleRepository.findByParentBetriebsstaetteIdAndQuelleIdIsNull(BETRIEBSSTAETTE.getId())).thenReturn(
                List.of(spbQuelle));

        Collection<SpbQuelleListItem> spbQuelleListItems = service.listAllNeueSpbQuellenAsListItemByBetriebsstaettenId(
                BETRIEBSSTAETTE.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        assertThat(spbQuelleListItems.stream()
                                     .filter(l -> l.getQuelleNr().equals(QUELLE.getQuelleNr()))
                                     .count()).isEqualTo(1);
    }

    @Test
    void listAllSpbQuellenAsListItemByAnlagenId_ANLAGE() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setQuelleId(123456L);
        spbQuelle.setId(16654L);

        when(spbQuelleRepository.findByParentAnlageId(ANLAGE.getId())).thenReturn(List.of(spbQuelle));
        when(stammdatenService.loadAllQuellenByAnlagenId(ANLAGE.getId(),
                ANLAGE.getParentBetriebsstaetteId())).thenReturn(List.of(QUELLE));

        Collection<SpbQuelleListItem> spbQuelleListItems = service.listAllSpbQuellenAsListItemByAnlageId(
                ANLAGE.getId(), null, BETRIEBSSTAETTE.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        assertThat(spbQuelleListItems.stream()
                                     .filter(l -> l.getSpbQuelleId() != null && l.getSpbQuelleId()
                                                                                 .equals(spbQuelle.getId()))
                                     .count()).isEqualTo(1);
        assertThat(spbQuelleListItems.stream()
                                     .filter(l -> l.getQuelleNr().equals(QUELLE.getQuelleNr()))
                                     .count()).isEqualTo(2);
    }

    @Test
    void listAllSpbQuellenAsListItemByAnlagenId_SPB_ANLAGE() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(13L);

        when(spbQuelleRepository.findByParentSpbAnlageId(spbAnlage.getId())).thenReturn(List.of(spbQuelle));

        List<SpbQuelleListItem> spbQuelleListItems = service.listAllSpbQuellenAsListItemByAnlageId(
                null, spbAnlage.getId(), BETRIEBSSTAETTE.getId());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        assertThat(spbQuelleListItems.stream()
                                     .filter(l -> l.getQuelleNr().equals(QUELLE.getQuelleNr()))
                                     .count()).isEqualTo(1);
    }

    @Test
    void listAllNeueSpbQuellenAsListItemByAnlagenId() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setQuelleId(null);

        when(spbQuelleRepository.findByParentAnlageIdAndQuelleIdIsNull(15L)).thenReturn(List.of(spbQuelle));

        List<SpbQuelleListItem> spbQuelleListItems = service.listAllNeueSpbQuellenAsListItemByAnlagenId(15L);

        verify(stammdatenService).loadAnlage(15L);

        assertThat(spbQuelleListItems.size()).isEqualTo(1);
        assertThat(spbQuelleListItems.get(0).getQuelleNr()).isEqualTo(QUELLE.getQuelleNr());
    }

    @Test
    void loadSpbQuelleByBetriebsstaette_WithQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());
        spbQuelle.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        when(stammdatenService.findQuelleOfBst(BETRIEBSSTAETTE.getId(), QUELLE.getQuelleNr())).thenReturn(
                Optional.of(QUELLE));
        when(spbQuelleRepository.findOneByParentBetriebsstaetteIdAndQuelleId(BETRIEBSSTAETTE.getId(),
                QUELLE.getId())).thenReturn(Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleByBetriebsstaette(BETRIEBSSTAETTE.getId(), QUELLE.getQuelleNr());

        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void loadSpbQuelleByBetriebsstaette_noQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle();
        spbQuelle.setId(16L);

        when(stammdatenService.findQuelleOfBst(BETRIEBSSTAETTE.getId(), QUELLE.getQuelleNr())).thenReturn(
                Optional.of(QUELLE));
        when(spbQuelleRepository.findOneByParentBetriebsstaetteIdAndQuelleId(BETRIEBSSTAETTE.getId(),
                QUELLE.getId())).thenReturn(Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleByBetriebsstaette(BETRIEBSSTAETTE.getId(), QUELLE.getQuelleNr());

        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void loadSpbQuelleByAnlage_WithQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());
        spbQuelle.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        when(stammdatenService.findQuelleOfAnlage(ANLAGE.getId(),
                QUELLE.getQuelleNr())).thenReturn(Optional.of(QUELLE));
        when(spbQuelleRepository.findOneByParentAnlageIdAndQuelleId(ANLAGE.getId(), QUELLE.getId())).thenReturn(
                Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleByAnlage(ANLAGE.getId(), QUELLE.getQuelleNr());

        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void loadSpbQuelleByAnlage_NoQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());
        spbQuelle.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(15L);

        when(stammdatenService.findQuelleOfAnlage(ANLAGE.getId(),
                QUELLE.getQuelleNr())).thenReturn(Optional.empty());
        when(spbQuelleRepository.findOneByParentAnlageIdAndQuelleNrIgnoreCase(ANLAGE.getId(),
                QUELLE.getQuelleNr())).thenReturn(
                Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleByAnlage(ANLAGE.getId(), QUELLE.getQuelleNr());

        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void loadSpbQuelleBySpbAnlage_WithQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());

        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(15L);

        when(spbAnlageRepository.findById(spbAnlage.getId())).thenReturn(Optional.of(spbAnlage));
        when(stammdatenService.canReadBetriebsstaette(ANLAGE.getParentBetriebsstaetteId())).thenReturn(true);
        when(stammdatenService.findQuelleOfAnlage(ANLAGE.getId(),
                QUELLE.getQuelleNr())).thenReturn(Optional.of(QUELLE));
        when(spbQuelleRepository.findOneByParentAnlageIdAndQuelleId(spbAnlage.getId(), QUELLE.getId())).thenReturn(
                Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleBySpbAnlage(spbAnlage.getId(), BETRIEBSSTAETTE.getId(),
                QUELLE.getQuelleNr());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(ANLAGE.getParentBetriebsstaetteId());
        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void loadSpbQuelleBySpbAnlage_NoQuelle() {
        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(16L);
        spbQuelle.setParentAnlageId(ANLAGE.getId());

        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setId(15L);
        spbAnlage.setParentBetriebsstaetteId(BETRIEBSSTAETTE.getId());

        when(spbAnlageRepository.findById(spbAnlage.getId())).thenReturn(Optional.of(spbAnlage));
        when(stammdatenService.canReadBetriebsstaette(BETRIEBSSTAETTE.getId())).thenReturn(true);

        when(spbQuelleRepository.findOneByParentSpbAnlageIdAndQuelleNrIgnoreCase(spbAnlage.getId(),
                spbQuelle.getQuelleNr())).thenReturn(Optional.of(spbQuelle));

        SpbQuelle spbQuelle1 = service.loadSpbQuelleBySpbAnlage(spbAnlage.getId(), BETRIEBSSTAETTE.getId(),
                QUELLE.getQuelleNr());

        verify(stammdatenService).checkBetriebsstaetteDatenberechtigungRead(BETRIEBSSTAETTE.getId());
        assertThat(spbQuelle1).isEqualTo(spbQuelle);
    }

    @Test
    void schliesseBetriebsstaetteUebernahmeAb() {
        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        spbBetriebsstaette.setId(879L);

        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(15L);
        SpbAnlage spbAnlage1 = new SpbAnlage();
        spbAnlage1.setId(987L);

        List<SpbAnlage> spbAnlagen = new ArrayList<>();
        spbAnlagen.add(spbAnlage);
        spbAnlagen.add(spbAnlage1);

        List<Quelle> quellen = new ArrayList<>();
        quellen.add(QUELLE);

        SpbQuelle spbQuelle = new SpbQuelle(QUELLE);
        spbQuelle.setId(879132L);

        SpbQuelle spbQuelleNeu = new SpbQuelle();
        spbQuelle.setId(546L);

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setId(879L);

        when(stammdatenService.loadBetriebsstaetteForWrite(BETRIEBSSTAETTE.getId())).thenReturn(BETRIEBSSTAETTE);
        when(stammdatenService.loadAllAnlagenByBetriebsstaettenId(BETRIEBSSTAETTE.getId())).thenReturn(List.of(ANLAGE));
        when(stammdatenService.loadAllQuellenByBetriebsstaettenId(BETRIEBSSTAETTE.getId())).thenReturn(
                new ArrayList<>());
        when(stammdatenService.loadAllQuellenByAnlagenId(ANLAGE.getId(), BETRIEBSSTAETTE.getId())).thenReturn(quellen);
        when(stammdatenService.loadAllAnlagenteileByAnlageId(ANLAGE.getId(), BETRIEBSSTAETTE.getId())).thenReturn(
                List.of(ANLAGENTEIL));

        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(
                Optional.of(spbBetriebsstaette));
        when(spbAnlageRepository.findByParentBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(spbAnlagen);
        when(spbAnlagenteilRepository.findByParentSpbAnlageId(any())).thenReturn(Collections.emptyList());
        when(spbAnlagenteilRepository.findByParentAnlageId(ANLAGE.getId())).thenReturn(List.of(spbAnlagenteil));
        when(spbQuelleRepository.findByParentBetriebsstaetteId(BETRIEBSSTAETTE.getId())).thenReturn(
                Collections.emptyList());
        when(spbQuelleRepository.findByParentSpbAnlageId(any())).thenReturn(Collections.emptyList());
        when(spbQuelleRepository.findByParentAnlageId(ANLAGE.getId())).thenReturn(
                Collections.singletonList(spbQuelleNeu));
        when(spbQuelleRepository.findByQuelleId(QUELLE.getId())).thenReturn(Optional.of(spbQuelle));

        service.schliesseBetriebsstaetteUebernahmeAb(BETRIEBSSTAETTE.getId());

        verify(spbQuelleRepository).deleteAll(any());
        verify(spbBetriebsstaetteRepository).delete(spbBetriebsstaette);
        verify(spbAnlagenteilRepository).deleteAll(any());
        verify(spbAnlageRepository).deleteAll(any());

        verify(stammdatenService).updateSchreibsperreBetreiber(BETRIEBSSTAETTE.getId(), true);
        verify(stammdatenService).updateStammdatenObjekteForAbschlussUebernahme(any(), anyList(), anyList(), anyList(),
                any());
    }

    @Test
    void sucheShouldUseBetreiberData() {
        long berichtsjahrId = 2020;
        when(sucheService.suche(berichtsjahrId, null, Pageable.unpaged()))
                .thenReturn(new PageImpl<>(List.of(
                        new BetriebsstaetteListItemView() {{
                            id = 10L;
                            name = "Behörde BST 10";
                        }},
                        new BetriebsstaetteListItemView() {{
                            id = 20L;
                            name = "Behörde BST 20";
                        }})));
        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(10L))
                .thenReturn(Optional.empty());
        var spbBst = new SpbBetriebsstaette(aBetriebsstaette().build());
        spbBst.setName("Betreiber BST 20");
        spbBst.setAdresse(new SpbAdresse("Straße", "Nr", "Ort", "PLZ"));
        when(spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(20L)).thenReturn(Optional.of(spbBst));

        var ergebnisse = service.suche(berichtsjahrId, Pageable.unpaged());

        assertThat(ergebnisse.getContent()).usingRecursiveComparison().isEqualTo(List.of(
                new BetriebsstaetteListItemView() {{
                    id = 10L;
                    name = "Behörde BST 10";
                }},
                new BetriebsstaetteListItemView() {{
                    id = 20L;
                    name = "Betreiber BST 20";
                    betriebsstatus = spbBst.getBetriebsstatus();
                    strasse = spbBst.getAdresse().getStrasse();
                    hausNr = spbBst.getAdresse().getHausNr();
                    ort = spbBst.getAdresse().getOrt();
                    plz = spbBst.getAdresse().getPlz();
                }}));
    }
}
