package de.wps.bube.basis.jobs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.JobStatus;

class JobStatusTest {

    @Test()
    void isNotOld() {

        final JobStatus status = getStatus();
        assertFalse(status.isOld(System.currentTimeMillis() + JobStatus.REMOVE_MILLIES + 100));

    }

    @Test()
    void isOldJob_Completed() {
        final JobStatus status = getStatus();
        assertTrue(status.isRunning());
        status.setCompleted();
        assertTrue(status.isOld(System.currentTimeMillis() + JobStatus.REMOVE_MILLIES + 100));
        assertFalse(status.isRunning());
    }

    @Test()
    void isOldJob_Error() {
        final JobStatus status = getStatus();
        assertTrue(status.isRunning());
        status.setError("Error Message");
        assertTrue(status.isOld(System.currentTimeMillis() + JobStatus.REMOVE_MILLIES + 100));
        assertFalse(status.isRunning());
    }

    @Test
    void addToCount() {

        final JobStatus status = getStatus();
        assertEquals(0, status.getCount());
        status.addToCount(50);
        assertEquals(50, status.getCount());
        status.addToCount(50);
        assertEquals(100, status.getCount());
        status.addToCount(-1);
        assertEquals(99, status.getCount());
    }

    private static JobStatus getStatus() {
        return new JobStatus(JobStatusEnum.BENUTZER_IMPORTIEREN);
    }

}
