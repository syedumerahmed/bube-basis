package de.wps.bube.basis.euregistry.xml.bericht;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.FeatureMember;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;

class EURegMapperImplTest extends AbstractEURegTest {

    private static InspireID getInspireId(FeatureMember featureMember) {
        if (featureMember.getProductionSite() != null) {
            return featureMember.getProductionSite().getInspireID();
        } else if (featureMember.getProductionFacility() != null) {
            return featureMember.getProductionFacility().getInspireID();
        } else if (featureMember.getProductionInstallation() != null) {
            return featureMember.getProductionInstallation().getInspireID();
        } else if (featureMember.getProductionInstallationPart() != null) {
            return featureMember.getProductionInstallationPart().getInspireID();
        }
        return null;
    }

    private EURegMapperImpl euRegMapper;

    @BeforeEach
    void setUp() {
        super.setUp();
        euRegMapper = new EURegMapperImpl(getProductionSiteMapper(), getProductionFacilityMapper(),
                getProductionInstallationMapper());
    }

    @Test
    void toXDto() {

        List<String> log = new ArrayList<>();

        EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        euRegBetriebsstaette.getBetriebsstaette().setLocalId("BST1");
        Referenz inBetrieb = ReferenzBuilder.aBetriebsstatusReferenz("in Betrieb").eu("01").build();
        List<EURegAnlage> euRegAnlageList = List.of(
                EURegAnlageBuilder.anEURegAnlage(2L).kapitelIeRl().berichtsdaten(BerichtsdatenBuilder.anEuRegAnlBerichtsdaten().build()).build(),
                EURegAnlageBuilder.anEURegAnlage(3L).kapitelIeRl().berichtsdaten(BerichtsdatenBuilder.anEuRegFBerichtsdaten().build()).build()
        );
        List<String> anlageLocalIds = new ArrayList<>();
        euRegAnlageList.forEach(euRegAnlage -> {
            euRegAnlage.getAnlage().setBetriebsstatus(inBetrieb);
            String localId = "A" + euRegAnlage.getId();
            euRegAnlage.getAnlage().setLocalId(localId);
            anlageLocalIds.add(localId);
        });

        Stream<EURegAnlage> euRegAnlagen = euRegAnlageList.stream().peek(euRegAnlage -> {
            log.add(euRegAnlage.getAnlage().getLocalId());
        });

        Stream<FeatureMember> featureMemberStream = euRegMapper.toXDto(euRegBetriebsstaette, euRegAnlagen,
                new AtomicLong(1000)::incrementAndGet, getReportData(), getInspireParam(), getUbaProperties(),
                anlageLocalIds);
        Iterator<FeatureMember> featureMembers = featureMemberStream
                .peek(featureMember -> log.add(getInspireId(featureMember).getIdentifier().getLocalId()))
                .iterator();

        FeatureMember featureMember;

        assertThat(featureMembers).hasNext();
        featureMember = featureMembers.next();
        assertThat(featureMember.getProductionSite()).isNotNull();
        assertThat(featureMember.getProductionSite().getInspireID().getIdentifier().getLocalId()).isEqualTo("BST1.uba.de");
        assertThat(featureMember.getProductionSite().getInspireID().getIdentifier().getNamespace()).isEqualTo("test-ns");

        assertThat(featureMembers).hasNext();
        featureMember = featureMembers.next();
        assertThat(featureMember.getProductionFacility()).isNotNull();
        assertThat(featureMember.getProductionFacility().getInspireID().getIdentifier().getLocalId()).isEqualTo("BST1");
        assertThat(featureMember.getProductionFacility().getInspireID().getIdentifier().getNamespace()).isEqualTo("test-ns");

        assertThat(featureMembers).hasNext();
        featureMember = featureMembers.next();
        assertThat(featureMember.getProductionInstallation()).isNotNull();
        assertThat(featureMember.getProductionInstallation().getInspireID().getIdentifier().getLocalId()).isEqualTo("A2-ext");
        assertThat(featureMember.getProductionInstallation().getInspireID().getIdentifier().getNamespace()).isEqualTo("test-ns");

        assertThat(featureMembers).hasNext();
        featureMember = featureMembers.next();
        assertThat(featureMember.getProductionInstallation()).isNotNull();
        assertThat(featureMember.getProductionInstallation().getInspireID().getIdentifier().getLocalId()).isEqualTo("A3-ext");
        assertThat(featureMember.getProductionInstallation().getInspireID().getIdentifier().getNamespace()).isEqualTo("test-ns");

        assertThat(featureMembers).hasNext();
        featureMember = featureMembers.next();
        assertThat(featureMember.getProductionInstallationPart()).isNotNull();
        assertThat(featureMember.getProductionInstallationPart().getInspireID().getIdentifier().getLocalId()).isEqualTo("A3-ext");
        assertThat(featureMember.getProductionInstallationPart().getInspireID().getIdentifier().getNamespace()).isEqualTo("test-ns");

        assertThat(featureMembers).isExhausted();
        assertThat(log).containsExactly("BST1.uba.de", "BST1", "A2", "A2-ext", "A3", "A3-ext", "A3-ext");

    }

}