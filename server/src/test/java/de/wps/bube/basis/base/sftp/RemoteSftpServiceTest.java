package de.wps.bube.basis.base.sftp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.OpenSSHKnownHosts;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import net.schmizz.sshj.userauth.password.PasswordFinder;

@SpringBootTest(classes = { SftpProperties.class }, properties = {
        "bube.sftp.host-key=xyz",
        "bube.sftp.host=test.bube.de",
        "bube.sftp.port=123",
        "bube.sftp.username=bube",
        "bube.sftp.passphrase=pass",
        "bube.sftp.key-dir=keys",
        "bube.sftp.key-name=key",
        "bube.sftp.upload-dir=/upload/",
})
@EnableConfigurationProperties(SftpProperties.class)
class RemoteSftpServiceTest {

    @Autowired
    private SftpProperties sftpProperties;

    private SftpService remoteSftpService;
    private SSHClient sshClient;
    private SFTPClient sftpClient;

    @BeforeEach
    void setUp() throws IOException {
        SSHClientProvider sshClientProvider = mock(SSHClientProvider.class);
        sshClient = mock(SSHClient.class);
        when(sshClientProvider.newSSHClient()).thenReturn(sshClient);
        sftpClient = mock(SFTPClient.class);
        when(sshClient.newSFTPClient()).thenReturn(sftpClient);
        remoteSftpService = new SftpService.Remote(sftpProperties, sshClientProvider);
    }

    @Test
    void testUpload() throws IOException {

        File keyFile = new File("keys", "key");
        KeyProvider keyProvider = mock(KeyProvider.class);
        when(sshClient.loadKeys(eq(keyFile.toString()), any(PasswordFinder.class))).thenReturn(keyProvider);

        File file = new File("test.xml");
        remoteSftpService.uploadFile(file);

        verify(sshClient).addHostKeyVerifier(argThat((ArgumentMatcher<OpenSSHKnownHosts>) argument -> {
            assertThat(argument.entries()).singleElement()
                                          .extracting(OpenSSHKnownHosts.KnownHostEntry::getLine)
                                          .isEqualTo("xyz");
            return true;
        }));
        verify(sshClient).connect("test.bube.de", 123);
        verify(sshClient).loadKeys(eq(keyFile.toString()), any(PasswordFinder.class));
        verify(sshClient).authPublickey("bube", keyProvider);
        verify(sshClient).newSFTPClient();
        verify(sshClient).close();

        verify(sftpClient).mkdirs("/upload/");
        verify(sftpClient).put(file.getAbsolutePath(), "/upload/test.xml");
        verify(sftpClient).close();

        verifyNoMoreInteractions(sshClient, sftpClient);

    }

}