package de.wps.bube.basis.sicherheit.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HESSEN;
import static de.wps.bube.basis.base.vo.Land.SAARLAND;
import static de.wps.bube.basis.base.vo.Land.SACHSEN_ANHALT;
import static de.wps.bube.basis.sicherheit.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.security.BenutzerBerechtigungsAdapter;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung.DatenberechtigungBuilder;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

@ExtendWith(MockitoExtension.class)
class BenutzerStammdatenBerechtigungsContextTest {

    private StammdatenBerechtigungsContext context;

    @Test
    void gesamtadmin_shouldHaveAccessToBenutzerOfLand() {
        context = new StammdatenBerechtigungsContext(aBehoerdenbenutzer().berechtigung(DatenberechtigungBuilder
                .land(DEUTSCHLAND)
                .behoerden("BEHD1", "BEHD2")
                .akz("AKZ1", "AKZ2")
                .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                .betriebsstaetten("BETRIEB1", "BETRIEB2")
                .build()).addRollen(GESAMTADMIN, LAND_READ).build());
        Benutzer benutzer = getBenutzer(DatenberechtigungBuilder.land(HESSEN).build());
        assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
    }

    @Nested
    class AsLandUserBundesland {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(DatenberechtigungBuilder
                    .land(SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(LAND_READ).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBenutzerOfOwnLand() {
            Benutzer benutzer = getBenutzer(DatenberechtigungBuilder.land(SAARLAND).build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherLand() {
            Benutzer benutzer = getBenutzer(DatenberechtigungBuilder.land(HESSEN).build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));

        }

        @Test
        void shouldNotHaveAccessToBenutzerOfDeutschland() {
            Benutzer benutzer = getBenutzer(DatenberechtigungBuilder.land(DEUTSCHLAND).build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }
    }

    @Nested
    class AsBehoerdeUserBundesland {
        DatenberechtigungBuilder matchingDatenberechtigungBuilder;

        @BeforeEach
        void setUp() {
            matchingDatenberechtigungBuilder = DatenberechtigungBuilder
                    .land(SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHN")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2");
            var benutzer = aBehoerdenbenutzer().berechtigung(matchingDatenberechtigungBuilder.build())
                                               .addRollen(BEHOERDE_READ).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBenutzerMatchingAllPredicates() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveAccessToBenutzerWithSubsetBehoerden() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.behoerden("BEHD1").build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveNoAccessToBenutzerWithNoBehoerden() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.behoerden().build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveAccessToBenutzerWithSubsetAKZ() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.akz("AKZ1").build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveNoAccessToBenutzerWithNoAKZ() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.akz().build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveAccessToBenutzerWithSubsetVerwaltungsgebiete() {
            Benutzer benutzer = getBenutzer(
                    matchingDatenberechtigungBuilder.verwaltungsgebiete("HHN", "HHN003").build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveNoAccessToBenutzerWithNoVerwaltungsgebiete() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.verwaltungsgebiete().build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveAccessToBenutzerWithSubsetBetriebsstaetten() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.betriebsstaetten("BETRIEB2").build());
            assertTrue(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldHaveNoAccessToBenutzerWithNoBetriebsstaetten() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.betriebsstaetten().build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherLand() {
            var benutzer = getBenutzer(DatenberechtigungBuilder.land(SACHSEN_ANHALT).build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherBehoerde() {
            Benutzer benutzer = getBenutzer(
                    matchingDatenberechtigungBuilder.behoerden("BEHD1", "BEHD2", "BEHD3").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherBehoerde2() {

            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.behoerden("NOT MINE").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherAkz() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.akz("AKZ3").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherGemeinde() {
            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.verwaltungsgebiete("LLBKKGEM").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherBetriebsstaette() {

            Benutzer benutzer = getBenutzer(
                    matchingDatenberechtigungBuilder.betriebsstaetten("BETRIEB1", "BETRIEB2", "BETRIEB5").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

        @Test
        void shouldNotHaveAccessToBenutzerOfOtherBetriebsstaette2() {

            Benutzer benutzer = getBenutzer(matchingDatenberechtigungBuilder.betriebsstaetten("BETRIEB5").build());
            assertFalse(context.hasAccessToRead(new BenutzerBerechtigungsAdapter(benutzer)));
        }

    }

    private static Benutzer getBenutzer(Datenberechtigung datenberechtigung) {
        Benutzer benutzer = new Benutzer("benutzername");
        benutzer.setDatenberechtigung(datenberechtigung);
        benutzer.setZugewieseneRollen(List.of(Rolle.BEHOERDENBENUTZER));
        return benutzer;
    }
}
