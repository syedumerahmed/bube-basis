package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlagenteilVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;
import de.wps.bube.basis.test.BubeIntegrationTest;

class AnlagenteilVergleichServiceTest extends BubeIntegrationTest {

    public static final Anlagenteil ANLAGENTEIL = AnlagenteilBuilder.aFullAnlagenteil()
                                                                    .inbetriebnahme(LocalDate.now())
                                                                    .id(26L)
                                                                    .build();
    private AnlagenteilVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        vergleichService = new AnlagenteilVergleichService(new CommonVergleichService());
    }

    @Test
    void vergleicheGleich() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setId(15L);

        SpbAnlagenteilVergleich vergleiche = vergleichService.vergleiche(ANLAGENTEIL, spbAnlagenteil);

        assertThat(vergleiche.getId()).isEqualTo(ANLAGENTEIL.getId());
        assertThat(vergleiche.getSpbId()).isEqualTo(spbAnlagenteil.getId());

        StringVector anlagenteilNr = vergleiche.getAnlagenteilNr();
        assertThat(anlagenteilNr.getSpl()).isEqualTo(anlagenteilNr.getSpb());

        ReferenzVector betriebsstatus = vergleiche.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleiche.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleiche.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isEqualTo(inbetriebnahme.getSpb());

        GeoPunktVector geoPunkt = vergleiche.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isEqualTo(geoPunkt.getSpb());

        List<LeistungVector> leistungen = vergleiche.getLeistungen();
        for (LeistungVector leistungVector : leistungen) {
            assertThat(leistungVector.getSpl()).isEqualTo(leistungVector.getSpb());
            assertThat(leistungVector.isEqual()).isTrue();
        }

        StringVector name = vergleiche.getName();
        assertThat(name.getSpl()).isEqualTo(name.getSpb());

        assertThat(anlagenteilNr.isEqual()).isTrue();
        assertThat(name.isEqual()).isTrue();
        assertThat(betriebsstatus.isEqual()).isTrue();
        assertThat(betriebsstatusSeit.isEqual()).isTrue();
        assertThat(inbetriebnahme.isEqual()).isTrue();
        assertThat(geoPunkt.isEqual()).isTrue();

        assertThat(vergleiche.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheUnterschiedlich() {
        Referenz taetigkeit = ReferenzBuilder.aTaetigkeitsReferenz("test").id(546L).build();
        Anlagenteil andererAnlagenteil = AnlagenteilBuilder.anAnlagenteil()
                                                           .id(654L)
                                                           .anlagenteilNr("test")
                                                           .name("test")
                                                           .betriebsstatus(
                                                                   ReferenzBuilder.aBetriebsstatusReferenz("test")
                                                                                  .id(564L)
                                                                                  .build())
                                                           .betriebsstatusSeit(LocalDate.MIN)
                                                           .inbetriebnahme(LocalDate.MIN)
                                                           .geoPunkt(new GeoPunkt(0, 1,
                                                                   ReferenzBuilder.anEpsgReferenz("test")
                                                                                  .id(15654L)
                                                                                  .build()))
                                                           .leistungen(new Leistung(10D,
                                                                   ReferenzBuilder.aLeistungseinheitReferenz("test")
                                                                                  .id(4561L)
                                                                                  .build(), "test",
                                                                   Leistungsklasse.BETRIEBEN))
                                                           .build();

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(andererAnlagenteil);
        spbAnlagenteil.setId(15L);

        SpbAnlagenteilVergleich vergleiche = vergleichService.vergleiche(ANLAGENTEIL, spbAnlagenteil);

        assertThat(vergleiche.getId()).isEqualTo(ANLAGENTEIL.getId());
        assertThat(vergleiche.getSpbId()).isEqualTo(spbAnlagenteil.getId());

        StringVector name = vergleiche.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        StringVector anlageNr = vergleiche.getAnlagenteilNr();
        assertThat(anlageNr.getSpl()).isNotEqualTo(anlageNr.getSpb());

        ReferenzVector betriebsstatus = vergleiche.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isNotEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleiche.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isNotEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleiche.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isNotEqualTo(inbetriebnahme.getSpb());

        GeoPunktVector geoPunkt = vergleiche.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isNotEqualTo(geoPunkt.getSpb());

        List<LeistungVector> leistungen = vergleiche.getLeistungen();
        for (LeistungVector leistungVector : leistungen) {
            assertThat(leistungVector.getSpl()).isNotEqualTo(leistungVector.getSpb());
        }

        assertThat(anlageNr.isEqual()).isFalse();
        assertThat(name.isEqual()).isFalse();
        assertThat(betriebsstatus.isEqual()).isFalse();
        assertThat(betriebsstatusSeit.isEqual()).isFalse();
        assertThat(inbetriebnahme.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleiche.isHasChanges()).isTrue();
    }

    @Test
    void vergleicheNurSPL() {
        SpbAnlagenteilVergleich vergleich = vergleichService.vergleiche(ANLAGENTEIL, null);

        assertThat(vergleich.getId()).isEqualTo(ANLAGENTEIL.getId());
        assertThat(vergleich.getSpbId()).isNull();

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isEqualTo(ANLAGENTEIL.getName());

        StringVector anlageNr = vergleich.getAnlagenteilNr();
        assertThat(anlageNr.getSpl()).isEqualTo(ANLAGENTEIL.getAnlagenteilNr());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl().getSchluessel()).isEqualTo(ANLAGENTEIL.getBetriebsstatus().getSchluessel());

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isEqualTo(ANLAGENTEIL.getBetriebsstatusSeit().toString());

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isEqualTo(ANLAGENTEIL.getInbetriebnahme().toString());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl().getEpsgCode().getSchluessel()).isEqualTo(
                ANLAGENTEIL.getGeoPunkt().getEpsgCode().getSchluessel());

        List<LeistungVector> leistungen = vergleich.getLeistungen();
        assertThat(
                leistungen.stream().map(LeistungVector::getSpl).map(Leistung::getLeistungsklasse).toList()).isEqualTo(
                ANLAGENTEIL.getLeistungen().stream().map(
                        Leistung::getLeistungsklasse).toList());

        assertThat(anlageNr.isEqual()).isFalse();
        assertThat(name.isEqual()).isFalse();
        assertThat(betriebsstatus.isEqual()).isFalse();
        assertThat(betriebsstatusSeit.isEqual()).isFalse();
        assertThat(inbetriebnahme.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheNurSPB() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(ANLAGENTEIL);
        spbAnlagenteil.setId(15L);

        SpbAnlagenteilVergleich vergleiche = vergleichService.vergleiche(null, spbAnlagenteil);

        assertThat(vergleiche.getSpbId()).isEqualTo(spbAnlagenteil.getId());
        assertThat(vergleiche.getId()).isNull();

        StringVector name = vergleiche.getName();
        assertThat(name.getSpb()).isEqualTo(spbAnlagenteil.getName());

        StringVector anlageNr = vergleiche.getAnlagenteilNr();
        assertThat(anlageNr.getSpb()).isEqualTo(spbAnlagenteil.getAnlagenteilNr());

        ReferenzVector betriebsstatus = vergleiche.getBetriebsstatus();
        assertThat(betriebsstatus.getSpb().getSchluessel()).isEqualTo(
                spbAnlagenteil.getBetriebsstatus().getSchluessel());

        LocalDateVector betriebsstatusSeit = vergleiche.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpb()).isEqualTo(spbAnlagenteil.getBetriebsstatusSeit().toString());

        LocalDateVector inbetriebnahme = vergleiche.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpb()).isEqualTo(spbAnlagenteil.getInbetriebnahme().toString());

        GeoPunktVector geoPunkt = vergleiche.getGeoPunkt();
        assertThat(geoPunkt.getSpb().getEpsgCode().getSchluessel()).isEqualTo(
                spbAnlagenteil.getGeoPunkt().getEpsgCode().getSchluessel());

        List<LeistungVector> leistungen = vergleiche.getLeistungen();
        assertThat(leistungen.stream().map(LeistungVector::getSpb).map(Leistung::getLeistungsklasse).map(
                Enum::name).toList()).isEqualTo(spbAnlagenteil.getLeistungen().stream().map(
                Leistung::getLeistungsklasse).map(Leistungsklasse::name).toList());

        assertThat(anlageNr.isEqual()).isFalse();
        assertThat(name.isEqual()).isFalse();
        assertThat(betriebsstatus.isEqual()).isFalse();
        assertThat(betriebsstatusSeit.isEqual()).isFalse();
        assertThat(inbetriebnahme.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleiche.isHasChanges()).isTrue();
    }

}
