package de.wps.bube.basis.arch;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = PersistenceLayersTest.PACKAGE_BASE, importOptions = ImportOption.DoNotIncludeTests.class)
class PersistenceLayersTest {

    static final String PACKAGE_BASE = "de.wps.bube.basis";

    @ArchTest
    public static final ArchRule stammdatenPersistenceRule = persistenceRule("stammdaten");

    @ArchTest
    public static final ArchRule referenzdatenPersistenceRule = persistenceRule("referenzdaten");

    @ArchTest
    public static final ArchRule dateiPersistenceRule = persistenceRule("datei");

    @ArchTest
    public static final ArchRule desktopPersistenceRule = persistenceRule("desktop");

    @ArchTest
    public static final ArchRule euregistryPersistenceRule = persistenceRule("euregistry");

    @ArchTest
    public static final ArchRule jobsPersistenceRule = persistenceRule("jobs");

    @ArchTest
    public static final ArchRule komplexpruefungPersistenceRule = persistenceRule("komplexpruefung");

    @ArchTest
    public static final ArchRule spbStammdatenPersistenceRule = persistenceRule("stammdatenbetreiber");

    private static ArchRule persistenceRule(String domain) {
        return classes().that().resideInAPackage("%s.%s.persistence".formatted(PACKAGE_BASE, domain))
                        .should().onlyBeAccessed().byAnyPackage("%s.%s..".formatted(PACKAGE_BASE, domain));
    }

}
