package de.wps.bube.basis.euregistry.security;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = EuRegBstBerechtigungsAccessor.class)
class EuRegBstBerechtigungsAccessorTest {

    @Autowired
    private EuRegBstBerechtigungsAccessor accessor;

    @Test
    void betriebsstaettenIn() {
        assertThat(accessor.betriebsstaettenIn(Set.of())).isNull();
        assertThat(accessor.betriebsstaettenIn(Set.of("abc"))).hasToString(
                "upper(eURegBetriebsstaette.betriebsstaette.betriebsstaetteNr) = ABC");
    }

}