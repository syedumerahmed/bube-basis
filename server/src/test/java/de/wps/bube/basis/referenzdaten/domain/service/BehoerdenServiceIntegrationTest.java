package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.base.vo.Land.*;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_LAND_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.REFERENZLISTEN_USE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = {
        BehoerdenService.class,
        BehoerdeBerechtigungsAccessor.class,
        EigeneBehoerdenBerechtigungsAcessor.class
})
@EnableJpaRepositories(basePackages = "de.wps.bube.basis.referenzdaten.persistence", repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan("de.wps.bube.basis.referenzdaten")
public class BehoerdenServiceIntegrationTest extends BubeIntegrationTest {

    @MockBean
    private BenutzerverwaltungService benutzerService;
    @Autowired
    private BehoerdenRepository repository;
    @Autowired
    private BehoerdenService service;

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_READ, land = BERLIN)
    void readBehoerde_anderesLand() {
        var b = repository.save(aBehoerde("01").land(HAMBURG).build());
        assertThrows(BubeEntityNotFoundException.class, () -> service.readBehoerde(b.getId()));
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_READ, land = HAMBURG)
    void readBehoerde_andereId() {
        var b = repository.save(aBehoerde("01").land(HAMBURG).build());
        assertThrows(BubeEntityNotFoundException.class, () -> service.readBehoerde(b.getId() + 1));
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_READ, land = HAMBURG, behoerden = { "02", "03" })
    void readBehoerde_ignore_datenberechtigung() {
        var b = repository.save(aBehoerde("01").land(HAMBURG).build());
        assertThat(service.readBehoerde(b.getId())).isEqualTo(b);
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_READ, land = HAMBURG)
    void readBehoerde_ok() {
        var b = repository.save(aBehoerde("01").land(HAMBURG).build());
        assertThat(service.readBehoerde(b.getId())).isEqualTo(b);
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void readBehoerde_gesamtadmin() {
        var b = repository.save(aBehoerde("01").land(HAMBURG).build());
        assertThat(service.readBehoerde(b.getId())).isEqualTo(b);
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void loadBehoerde_byKey_ungueltigVon() {
        var schluessel = "01";
        var land = HAMBURG;
        repository.save(aBehoerde(schluessel).land(land).gueltigVon(2010).gueltigBis(2020).build());
        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde(schluessel, Gueltigkeitsjahr.of(2009), land));
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void loadBehoerde_byKey_ungueltigBis() {
        var schluessel = "01";
        var land = HAMBURG;
        repository.save(aBehoerde(schluessel).land(land).gueltigVon(2010).gueltigBis(2020).build());
        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde(schluessel, Gueltigkeitsjahr.of(2021), land));
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void loadBehoerde_byKey_falschesLand() {
        var schluessel = "01";
        repository.save(aBehoerde(schluessel).land(BERLIN).gueltigVon(2010).gueltigBis(2020).build());
        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde(schluessel, Gueltigkeitsjahr.of(2010), HAMBURG));
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void loadBehoerde_byKey_falscherSchluessel() {
        var schluessel = "05";
        var land = HAMBURG;
        repository.save(aBehoerde(schluessel).land(land).gueltigVon(2010).gueltigBis(2020).build());
        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde("01", Gueltigkeitsjahr.of(2010), land));
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_USE, land = SCHLESWIG_HOLSTEIN)
    void loadBehoerde_byKey_keineBerechtigung_land() {
        var schluessel = "01";
        var land = HAMBURG;
        repository.save(aBehoerde(schluessel).land(land).gueltigVon(2010).gueltigBis(2020).build());
        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde(schluessel, Gueltigkeitsjahr.of(2010), land));
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void readBehoerden_by_jahr_noLand() {
        var b1_2010_2020 = repository.save(
                aBehoerde("01").land(HAMBURG).gueltigVon(2010).gueltigBis(2020).build());
        var b2_2010_2030 = repository.save(aBehoerde("02").land(BERLIN).gueltigVon(2010).gueltigBis(2030).build());
        var b3_2010_2099 = repository.save(
                aBehoerde("03").land(BRANDENBURG).gueltigVon(2010).gueltigBis(2099).build());
        var b4_2015_2015 = repository.save(
                aBehoerde("04").land(BADEN_WUERTTEMBERG).gueltigVon(2015).gueltigBis(2015).build());
        repository.save(aBehoerde("05").land(MECKLENBURG_VORPOMMERN).gueltigVon(2000).gueltigBis(2005).build());
        repository.save(aBehoerde("06").land(MECKLENBURG_VORPOMMERN).gueltigVon(2016).gueltigBis(2018).build());

        assertThat(service.readBehoerden(Gueltigkeitsjahr.of(2015), null))
                .extracting("id")
                .containsExactlyInAnyOrder(
                        b1_2010_2020.getId(),
                        b2_2010_2030.getId(),
                        b3_2010_2099.getId(),
                        b4_2015_2015.getId());
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void readBehoerden_by_jahr_land_gesamtadmin() {
        var b1 = repository.save(aBehoerde("01").land(HAMBURG).gueltigVon(2010).gueltigBis(2020).build());
        var b2 = repository.save(aBehoerde("02").land(HAMBURG).gueltigVon(2020).gueltigBis(2020).build());
        repository.save(aBehoerde("01").land(BERLIN).gueltigVon(2010).gueltigBis(2020).build());
        repository.save(aBehoerde("02").land(BAYERN).gueltigVon(2020).gueltigBis(2020).build());

        assertThat(service.readBehoerden(Gueltigkeitsjahr.of(2020), HAMBURG))
                .extracting("id")
                .containsExactlyInAnyOrder(b1.getId(), b2.getId());
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_USE, land = HAMBURG, behoerden = { "01", "02" })
    void readBehoerden_by_jahr_land_berechtigungen() {
        var b1 = repository.save(aBehoerde("01").land(HAMBURG).gueltigVon(2010).gueltigBis(2020).build());
        var b2 = repository.save(aBehoerde("02").land(HAMBURG).gueltigVon(2010).gueltigBis(2020).build());
        var b3 = repository.save(aBehoerde("03").land(HAMBURG).gueltigVon(2010).gueltigBis(2020).build());
        repository.save(aBehoerde("01").land(HAMBURG).gueltigVon(2010).gueltigBis(2010).build());
        repository.save(aBehoerde("01").land(BERLIN).gueltigVon(2010).gueltigBis(2020).build());
        repository.save(aBehoerde("02").land(BAYERN).gueltigVon(2010).gueltigBis(2020).build());

        assertThat(service.readBehoerden(Gueltigkeitsjahr.of(2020), HAMBURG))
                .extracting("id")
                .containsExactlyInAnyOrder(b1.getId(), b2.getId())
                .doesNotContain(b3.getId());
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_WRITE, land = HAMBURG)
    void updateBehoerde_keineBerechtigung_existing() {
        var existing = repository.save(aBehoerde("schl").land(BRANDENBURG).build());
        var b = aBehoerde("schl").id(existing.getId()).land(HAMBURG).build();
        assertThrows(BubeEntityNotFoundException.class, () -> service.updateBehoerde(b));
    }

    @Test
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_WRITE, land = HAMBURG)
    void updateBehoerde_keineBerechtigung_updated() {
        var existing = repository.save(aBehoerde("schl").land(HAMBURG).build());
        var b = aBehoerde("schl").id(existing.getId()).land(BRANDENBURG).build();
        assertThrows(AccessDeniedException.class, () -> service.updateBehoerde(b));
    }

}
