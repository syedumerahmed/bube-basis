package de.wps.bube.basis.desktop.web;

import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten.EUREG_ANL_SCHLUESSEL;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.aFullAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.aFullAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;

@ExtendWith(MockitoExtension.class)
class EuBerichtTreeNodeMapperTest {

    @Mock
    private EURegistryService eURegistryService;

    private EuBerichtTreeNodeMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new EuBerichtTreeNodeMapper(eURegistryService);
    }

    @Test
    void mapEuBericht() {

        // Setup
        EURegBetriebsstaette b = aEURegBetriebsstaette().betriebsstaette(
                aFullBetriebsstaette().name("Halsbrecher Betrieb")
                                      .land(NORDRHEIN_WESTFALEN)
                                      .betriebsstaetteNr("nummer")
                                      .build()).build();

        // Act
        TreeNodeDto berichtDto = mapper.mapEURegBetriebsstaette(b);

        // Assert
        assertEquals("Halsbrecher Betrieb", berichtDto.getName());
        assertEquals(TreeNodeTypEnum.EUREG_BST, berichtDto.getTyp());
        assertEquals("betriebsstaette/land/05/nummer/nummer", berichtDto.getLink());
        assertEquals("nummer", berichtDto.getNummer());
        assertThat(berichtDto.getChildren()).isEmpty();

    }

    @Test
    void mapEuBerichtWithAnlagen() {

        // Setup
        EURegBetriebsstaette b = aEURegBetriebsstaette().betriebsstaette(aFullBetriebsstaette().anlagen(
                aFullAnlage().id(1L).anlageNr("1")
                             .anlagenteile(aFullAnlagenteil().id(11L).anlagenteilNr("11").build()).build(),
                aFullAnlage().id(2L).anlageNr("2").anlagenteile().build(),
                aFullAnlage().id(3L).anlageNr("3").anlagenteile().build(),
                aFullAnlage().id(4L).anlageNr("4")
                             .anlagenteile(
                                     aFullAnlagenteil().id(41L).anlagenteilNr("41").build(),
                                     aFullAnlagenteil().id(42L).anlagenteilNr("42").build(),
                                     aFullAnlagenteil().id(43L).anlagenteilNr("43").build())
                             .build()).build()).build();

        when(eURegistryService.findAnlagenBerichtByAnlageId(1L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().berichtsdaten(
                aFullBerichtsdaten().berichtsart(aReferenz(RRTYP, EUREG_ANL_SCHLUESSEL).build()).build()).build()));
        when(eURegistryService.findAnlagenBerichtByAnlageId(2L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(11L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(41L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(42L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(43L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));

        // Act
        TreeNodeDto berichtDto = mapper.mapEURegBetriebsstaette(b);

        // Assert
        assertThat(berichtDto.getChildren()).extracting("nummer").containsExactly("1", "11", "2", "41", "42", "43");
        assertThat(berichtDto.getChildren().size()).isEqualTo(6);

        assertThat(berichtDto.getChildren().get(0).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/1");
        assertThat(berichtDto.getChildren().get(1).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/1/anlagenteil/nummer/11");
        assertThat(berichtDto.getChildren().get(2).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/2");
        assertThat(berichtDto.getChildren().get(3).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/4/anlagenteil/nummer/41");
        assertThat(berichtDto.getChildren().get(4).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/4/anlagenteil/nummer/42");
        assertThat(berichtDto.getChildren().get(5).getLink()).isEqualTo(
                "betriebsstaette/land/05/nummer/nummer/anlage/nummer/4/anlagenteil/nummer/43");
    }

}
