package de.wps.bube.basis.referenzdaten.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ObjectFactory;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.xmlunit.builder.Input;

import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;

@SpringBootTest(classes = { ReferenzXMLMapperImpl.class, CommonXMLMapperImpl.class })
class ReferenzXMLServiceWriteTest {

    private static final String VALID_FULL_XML = "xml/referenzdaten_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private ReferenzXMLMapper mapper;

    @Test
    void writeReferenz() throws IOException {
        var objectFactory = new ObjectFactory();
        var expectedXML = Input.fromStream(getResource(VALID_FULL_XML));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        var writer = xmlService.createWriter(ReferenzType.class, ReferenzlisteXDto.QUALIFIED_NAME,
                        mapper::toXDto, objectFactory::createReferenz);

        writer.write(Stream.of(getReferenz()), outputStream);

        MatcherAssert.assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
    }

    private Referenz getReferenz() {
        return ReferenzBuilder.aReferenz(Referenzliste.REPSG, "0001")
                              .referenznummer("1234567890")
                              .ltext("ltext")
                              .eu("eu")
                              .strg("strg")
                              .letzteAenderung(LocalDateTime.of(2020, 6, 30, 14, 0).toInstant(ZoneOffset.of("Z")))
                              .build();
    }

}
