package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten.EUREG_ANL_SCHLUESSEL;
import static de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten.EUREG_BST_SCHLUESSEL;
import static de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten.EUREG_F_SCHLUESSEL;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegAnl;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegF;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.anEURegAnlage;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.entity.QEURegAnlage.eURegAnlage;
import static de.wps.bube.basis.euregistry.domain.service.EURegistryService.BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.URL_MONI_KEY;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.URL_PUBLIC_DISCLOSURE_KEY;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.URL_VISIT_KEY;
import static de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder.aParameterWert;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RSPECC;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.persistence.EuRegAnlageRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageBearbeitetEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsobjektService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class EURegistryServiceTest {

    @Mock
    private EURegBetriebsstaetteRepository euRegBetriebsstaetteRepository;
    @Mock
    private ReferenzenService referenzenService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private BerichtsobjektService berichtsobjektService;
    @Mock
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    private EuRegBstBerechtigungsAccessor accessor;
    @Mock
    private EuRegAnlageRepository euRegAnlageRepository;
    @Mock
    private KomplexpruefungService komplexpruefungService;
    @Mock
    private ParameterService parameterService;
    @Mock
    private AktiverBenutzer aktiverBenutzer;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private EuRegistryBerichtsjahrwechselMapper euRegistryBerichtsjahrwechselMapper;
    @Mock
    private EuRegUpdater euRegUpdater;

    private EURegistryService service;

    @BeforeEach
    void setUp() {
        service = new EURegistryService(
                euRegBetriebsstaetteRepository,
                referenzenService,
                stammdatenService,
                berichtsobjektService,
                stammdatenBerechtigungsContext,
                accessor,
                euRegAnlageRepository,
                komplexpruefungService,
                parameterService,
                eventPublisher,
                euRegistryBerichtsjahrwechselMapper,
                aktiverBenutzer,
                euRegUpdater);
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldCallDelete() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);
        EURegBetriebsstaette euRegBetriebsstaette = aEURegBetriebsstaette().id(3L).build();

        when(bst.hatEURegBst()).thenReturn(false);
        when(euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())).thenReturn(true);
        when(euRegBetriebsstaetteRepository.findAllIds(any(Predicate.class))).thenReturn(
                List.of(euRegBetriebsstaette.getId()));
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        verify(euRegBetriebsstaetteRepository).deleteByBetriebsstaetteId(bst.getId());
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldNotCallDelete() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(bst.hatEURegBst()).thenReturn(false);
        when(euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())).thenReturn(false);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        verify(euRegBetriebsstaetteRepository, never()).deleteByBetriebsstaetteId(bst.getId());
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldCallSave() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(bst.hatEURegBst()).thenReturn(true);
        when(bst.getZustaendigeBehoerde()).thenReturn(aBehoerde("BEHOERDE").build());
        when(bst.getAkz()).thenReturn("akz");
        when(bst.getLand()).thenReturn(Land.BREMEN);
        when(euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())).thenReturn(false);
        when(euRegBetriebsstaetteRepository.save(any(EURegBetriebsstaette.class))).thenAnswer(
                call -> call.getArgument(0));
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        when(referenzenService.readReferenz(RRTYP, EUREG_BST_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand())).thenReturn(
                aReferenz(RRTYP, EUREG_BST_SCHLUESSEL).build());

        when(referenzenService.readReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand())).thenReturn(aReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL).build());

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        ArgumentCaptor<EURegBetriebsstaette> captor = ArgumentCaptor.forClass(EURegBetriebsstaette.class);
        verify(euRegBetriebsstaetteRepository).save(captor.capture());
        assertThat(captor.getValue().getBetriebsstaette()).isSameAs(bst);
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldNotCallSave() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(bst.hatEURegBst()).thenReturn(true);
        when(euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())).thenReturn(true);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        verify(euRegBetriebsstaetteRepository, never()).save(any());
    }

    @Test
    void handleAnlageLoeschenEvent_ShoudCallDelete() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anlage = anAnlage().id(1L).build();
        when(euRegAnlageRepository.existsByAnlageId(anlage.getId())).thenReturn(true);

        service.handleAnlageGeloeschtEvent(new AnlageGeloeschtEvent(anlage.getId(), bst.getId()));

        verify(euRegAnlageRepository).deleteByAnlageId(anlage.getId());
    }

    @Test
    void handleAnlagenteilLoeschenEvent_ShoudCallDelete() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlagenteil anlagenteil = anAnlagenteil().id(1L).build();
        when(euRegAnlageRepository.existsByAnlagenteilId(anlagenteil.getId())).thenReturn(true);

        service.handleAnlagenteilGeloeschtEvent(new AnlagenteilGeloeschtEvent(anlagenteil.getId(), bst.getId()));

        verify(euRegAnlageRepository).deleteByAnlagenteilId(anlagenteil.getId());
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldCallSaveAnlagenbericht() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(anl.hatEURegAnl()).thenReturn(true);
        when(anl.isFeuerungsanlage()).thenReturn(false);
        when(anl.getLocalId()).thenReturn("Local-ID");
        when(bst.hatEURegBst()).thenReturn(false);
        when(bst.getZustaendigeBehoerde()).thenReturn(aBehoerde("BEHOERDE").build());
        when(bst.getAkz()).thenReturn("akz");
        when(euRegAnlageRepository.existsByAnlageId(anl.getId())).thenReturn(false);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        when(referenzenService.readReferenz(RRTYP, EUREG_ANL_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand())).thenReturn(
                aReferenz(RRTYP, EUREG_ANL_SCHLUESSEL).build());

        when(referenzenService.readReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand())).thenReturn(aReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL).build());

        when(parameterService.findParameterWert(URL_VISIT_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("visiturl").build()));

        when(parameterService.findParameterWert(URL_MONI_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("moniurl").build()));

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegAnlageRepository).save(captor.capture());
        assertThat(captor.getValue().getLocalId2()).isEqualTo(anl.getLocalId());
    }

    @Test
    void handleAnlagenteilAngelegtEvent_ShouldCallSaveAnlagenbericht() {
        var an = anAnlagenteil().euRegPflichtigBst(true).localId("Local-ID").build();
        var anlage = anAnlage().id(16L).anlagenteile(an).build();
        Betriebsstaette bst = aBetriebsstaette()
                .id(15L)
                .anlagen(anlage)
                .build();

        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);
        when(stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);

        when(referenzenService.readReferenz(eq(RRTYP), anyString(),
                eq(bst.getGueltigkeitsjahrFromBerichtsjahr()), eq(bst.getLand())))
                .thenAnswer(i -> aReferenz(RRTYP, i.getArgument(1)).build());

        when(referenzenService.readReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand())).thenReturn(aReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL).build());

        when(parameterService.findParameterWert(URL_VISIT_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("visiturl").build()));

        when(parameterService.findParameterWert(URL_MONI_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("moniurl").build()));
        when(euRegBetriebsstaetteRepository.save(any(EURegBetriebsstaette.class))).thenAnswer(
                call -> call.getArgument(0));

        service.handleAnlagenteilAngelegtEvent(new AnlagenteilAngelegtEvent(an));

        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegAnlageRepository).save(captor.capture());
        assertThat(captor.getValue().getLocalId2()).isEqualTo(an.getLocalId());
    }

    @Test
    void handleAnlageAngelegtEvent_ShouldCallSaveAnlagenbericht_Feuerungsanlage() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(anl.hatEURegAnl()).thenReturn(true);
        when(anl.isFeuerungsanlage()).thenReturn(true);
        when(anl.isFeuerungsanlageMit17BV()).thenReturn(true);
        when(bst.hatEURegBst()).thenReturn(false);
        when(bst.getZustaendigeBehoerde()).thenReturn(aBehoerde("BEHOERDE").build());
        when(bst.getAkz()).thenReturn("akz");
        when(euRegAnlageRepository.existsByAnlageId(anl.getId())).thenReturn(false);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        when(referenzenService.readReferenz(RRTYP, EUREG_F_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand())).thenReturn(
                aReferenz(RRTYP, EUREG_F_SCHLUESSEL).build());

        when(referenzenService.readReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand())).thenReturn(aReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL).build());

        when(parameterService.findParameterWert(URL_VISIT_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("visiturl").build()));

        when(parameterService.findParameterWert(URL_MONI_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("moniurl").build()));

        when(parameterService.findParameterWert(URL_PUBLIC_DISCLOSURE_KEY,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand(),
                bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("pdisclurl").build()));

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegAnlageRepository).save(captor.capture());
        assertThat(captor.getValue().getFeuerungsanlage().getOeffentlicheBekanntmachungUrl()).isEqualTo("pdisclurl");
    }

    @Test
    void handleAnlageAngelegtEvent_Feuerungsanlage13BImSchV() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);

        when(anl.hatEURegAnl()).thenReturn(true);
        when(anl.isFeuerungsanlage()).thenReturn(true);
        when(anl.isFeuerungsanlageMit17BV()).thenReturn(false);
        when(bst.hatEURegBst()).thenReturn(false);
        when(bst.getZustaendigeBehoerde()).thenReturn(aBehoerde("BEHOERDE").build());
        when(bst.getAkz()).thenReturn("akz");
        when(euRegAnlageRepository.existsByAnlageId(anl.getId())).thenReturn(false);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        when(referenzenService.readReferenz(RRTYP, EUREG_F_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand())).thenReturn(
                aReferenz(RRTYP, EUREG_F_SCHLUESSEL).build());

        when(referenzenService.readReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand())).thenReturn(aReferenz(RBEA,
                BEARBEITUNGSSTATUS_UNBEARBEITET_SCHLUESSEL).build());

        when(parameterService.findParameterWert(URL_VISIT_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("visiturl").build()));

        when(parameterService.findParameterWert(URL_MONI_KEY, bst.getGueltigkeitsjahrFromBerichtsjahr(),
                bst.getLand(), bst.getZustaendigeBehoerde().getId())).thenReturn(
                Optional.of(aParameterWert().wert("moniurl").build()));

        service.handleAnlageAngelegtEvent(new AnlageAngelegtEvent(anl));

        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegAnlageRepository).save(captor.capture());
        assertThat(captor.getValue().getFeuerungsanlage().getOeffentlicheBekanntmachungUrl()).isNull();
    }

    @Test
    void updateBerichtsartAnlage_from_EURegAnl_to_EURegF() {
        Betriebsstaette bst = mock(Betriebsstaette.class);
        Anlage anl = mock(Anlage.class);
        EURegAnlage bericht = mock(EURegAnlage.class);

        when(stammdatenService.canReadBetriebsstaette(anyLong())).thenReturn(true);
        when(stammdatenService.canWriteBetriebsstaette(anyLong())).thenReturn(true);
        when(anl.hatEURegAnl()).thenReturn(true);
        when(euRegAnlageRepository.existsByAnlageId(anl.getId())).thenReturn(true);
        when(anl.isFeuerungsanlage()).thenReturn(true);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);

        when(euRegAnlageRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(bericht));
        when(bericht.isEURegAnl()).thenReturn(true);
        when(bericht.getAnlage()).thenReturn(anl);

        when(referenzenService.readReferenz(RRTYP, EUREG_F_SCHLUESSEL,
                bst.getGueltigkeitsjahrFromBerichtsjahr(), bst.getLand())).thenReturn(
                aReferenz(RRTYP, EUREG_F_SCHLUESSEL).build());

        // Act
        service.handleAnlageBearbeitetEvent(new AnlageBearbeitetEvent(anl));

        // Assert
        verify(euRegAnlageRepository).save(any());
    }

    @Test
    void updateBerichtsartAnlage_removeVorschrift17BV_shouldResetAll17BVProperties() {
        Anlage anl = anAnlage().add13BimschvVorschrift().id(13L).build();
        Betriebsstaette bst = aBetriebsstaette().id(15L).anlagen(anl).build();
        EURegAnlage bericht = aFullEURegF().anlage(anl)
                                           .feuerungsanlage(
                                                   aFeuerungsanlageBerichtsdaten()
                                                           // Eigenschaften der 17.BImSchV
                                                           .spezielleBedingungen(new SpezielleBedingungIE51(
                                                                   aReferenz(RSPECC, "01").build(), "info", "url"))
                                                           .kapazitaetGefAbfall(100D)
                                                           .kapazitaetNichtGefAbfall(200D)
                                                           .gesamtKapazitaetAbfall(300D)
                                                           .mehrWaermeVonGefAbfall(true)
                                                           .mehrWaermeVonGefAbfall(true)
                                                           .oeffentlicheBekanntmachung("Public Disclosure")
                                                           .oeffentlicheBekanntmachungUrl(
                                                                   "https://public.disclosure.url")
                                                           .build())
                                           .build();

        when(stammdatenService.canReadBetriebsstaette(bst.getId())).thenReturn(true);
        when(stammdatenService.canWriteBetriebsstaette(bst.getId())).thenReturn(true);
        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);
        when(euRegAnlageRepository.existsByAnlageId(anl.getId())).thenReturn(true);
        when(euRegBetriebsstaetteRepository.existsByBetriebsstaetteId(bst.getId())).thenReturn(true);

        when(euRegAnlageRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(bericht));

        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);

        // Act
        service.handleAnlageBearbeitetEvent(new AnlageBearbeitetEvent(anl));

        // Assert
        verify(euRegAnlageRepository).save(captor.capture());

        var euRegAnlage = captor.getValue();
        assertThat(euRegAnlage.isEURegF()).isTrue();

        var feuerungsanlage = euRegAnlage.getFeuerungsanlage();
        assertThat(feuerungsanlage.getSpezielleBedingungen()).isEmpty();
        assertThat(feuerungsanlage.getGesamtKapazitaetAbfall()).isZero();
        assertThat(feuerungsanlage.getKapazitaetGefAbfall()).isZero();
        assertThat(feuerungsanlage.getKapazitaetNichtGefAbfall()).isZero();
        assertThat(feuerungsanlage.isMehrWaermeVonGefAbfall()).isFalse();
        assertThat(feuerungsanlage.isMitverbrennungVonUnbehandeltemAbfall()).isFalse();
        assertThat(feuerungsanlage.getOeffentlicheBekanntmachung()).isNull();
        assertThat(feuerungsanlage.getOeffentlicheBekanntmachungUrl()).isNull();
    }

    @Test
    void streamAllByBetriebsstaetteIdIn() {
        List<Long> ids = List.of(1L, 2L, 3L);
        Stream<EURegBetriebsstaette> result = Stream.empty();
        when(stammdatenService.canReadAllBetriebsstaette(ids)).thenReturn(true);
        when(euRegBetriebsstaetteRepository.streamAllByBetriebsstaetteIdIn(ids)).thenReturn(result);

        var stream = service.streamAllByBetriebsstaetteIdIn(ids);

        assertThat(stream).isSameAs(result);
    }

    @Test
    void streamAllByBetriebsstaetteIdIn_Denied() {
        List<Long> ids = List.of(1L, 2L, 3L);
        when(stammdatenService.canReadAllBetriebsstaette(ids)).thenReturn(false);

        assertThrows(AccessDeniedException.class, () -> service.streamAllByBetriebsstaetteIdIn(ids));
    }

    @Test
    void findAllAnlagenBerichteForBstBericht() {
        // Arrange
        var anlagenteilOhneBericht = anAnlagenteil(2L).build();
        var anlagenteilMitBericht = anAnlagenteil(3L).build();
        var anlageOhneBericht = anAnlage(1L)
                .anlagenteile(anlagenteilOhneBericht, anlagenteilMitBericht).build();
        var anlagenteil2MitBericht = anAnlagenteil(5L).build();
        var anlageMitBericht = anAnlage(4L)
                .anlagenteile(anlagenteil2MitBericht).build();
        var anlageOhneAnlagenteile = anAnlage(6L)
                .anlagenteile().build();
        var euRegBst = aEURegBetriebsstaette()
                .betriebsstaette(aBetriebsstaette()
                        .anlagen(anlageOhneBericht, anlageMitBericht, anlageOhneAnlagenteile).build())
                .build();

        when(euRegAnlageRepository.findOne(eURegAnlage.anlagenteil.id.eq(anlagenteilMitBericht.getId())))
                .thenReturn(Optional.of(anEURegAnlage(anlagenteilMitBericht.getId())
                        .anlagenteil(anlagenteilMitBericht).build()));
        when(euRegAnlageRepository.findOne(eURegAnlage.anlagenteil.id.eq(anlagenteil2MitBericht.getId())))
                .thenReturn(Optional.of(anEURegAnlage(anlagenteil2MitBericht.getId())
                        .anlagenteil(anlagenteil2MitBericht).build()));
        when(euRegAnlageRepository.findOne(eURegAnlage.anlage.id.eq(anlageMitBericht.getId())))
                .thenReturn(Optional.of(anEURegAnlage(anlageMitBericht.getId()).build()));

        when(euRegAnlageRepository.findOne(eURegAnlage.anlage.id.eq(anlageOhneBericht.getId())))
                .thenReturn(Optional.empty());
        when(euRegAnlageRepository.findOne(eURegAnlage.anlage.id.eq(anlageOhneAnlagenteile.getId())))
                .thenReturn(Optional.empty());
        when(euRegAnlageRepository.findOne(eURegAnlage.anlagenteil.id.eq(anlagenteilOhneBericht.getId())))
                .thenReturn(Optional.empty());

        when(stammdatenService.canReadBetriebsstaette(any())).thenReturn(true);

        // Act
        var anlageBerichte = service.findAllAnlagenberichteForBstBericht(euRegBst);

        // Assert
        assertThat(anlageBerichte).extracting(EURegAnlage::getId).containsExactly(
                anlagenteilMitBericht.getId(), anlageMitBericht.getId(), anlagenteil2MitBericht.getId()
        );
        verifyNoMoreInteractions(euRegAnlageRepository);
    }

    @Test
    void updateBetriebsstaettenBericht_shouldUpdateBearbeitungsstatusSeit() {

        // Arrange
        var bstBericht = aEURegBetriebsstaette().id(1L).build();
        bstBericht.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS02").id(2L).build());
        var bstBerichtOld = aEURegBetriebsstaette().id(2L).build();
        bstBerichtOld.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());

        when(euRegBetriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(bstBerichtOld));
        when(stammdatenBerechtigungsContext.createPredicateRead(any())).thenReturn(new BooleanBuilder());

        // Act
        service.updateBetriebsstaettenBericht(bstBericht);

        // Assert
        ArgumentCaptor<EURegBetriebsstaette> captor = ArgumentCaptor.forClass(EURegBetriebsstaette.class);
        verify(euRegUpdater).update(any(), captor.capture());
        assertThat(captor.getValue().getBerichtsdaten().getBearbeitungsstatusSeit()).isEqualTo(LocalDate.now());
    }

    @Test
    void updateBetriebsstaettenBericht_shouldNotUpdateBearbeitungsstatusSeit() {

        // Arrange
        var bstBericht = aEURegBetriebsstaette().id(1L).build();
        bstBericht.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());
        var bstBerichtOld = aEURegBetriebsstaette().id(2L).build();
        bstBerichtOld.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());

        when(euRegBetriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(bstBerichtOld));
        when(stammdatenBerechtigungsContext.createPredicateRead(any())).thenReturn(new BooleanBuilder());

        // Act
        service.updateBetriebsstaettenBericht(bstBericht);

        // Assert
        ArgumentCaptor<EURegBetriebsstaette> captor = ArgumentCaptor.forClass(EURegBetriebsstaette.class);
        verify(euRegUpdater).update(any(), captor.capture());
        assertThat(captor.getValue().getBerichtsdaten().getBearbeitungsstatusSeit()).isNotEqualTo(LocalDate.now());
    }

    @Test
    void updateAnlagenBericht_shouldUpdateBearbeitungsstatusSeit() {

        // Arrange
        var anlageBericht = aFullEURegAnl().id(1L).build();
        anlageBericht.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS02").id(2L).build());
        var anlageBerichtOld = aFullEURegAnl().id(2L).build();
        anlageBerichtOld.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());

        when(euRegAnlageRepository.findById(anyLong())).thenReturn(Optional.of(anlageBerichtOld));
        when(stammdatenService.canWriteBetriebsstaette(any())).thenReturn(true);

        // Act
        service.updateAnlagenBericht(anlageBericht);

        // Assert
        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegUpdater).update(any(), captor.capture());
        assertThat(captor.getValue().getBerichtsdaten().getBearbeitungsstatusSeit()).isEqualTo(LocalDate.now());
    }

    @Test
    void updateAnlagenBericht_shouldNotUpdateBearbeitungsstatusSeit() {

        // Arrange
        var anlageBericht = aFullEURegAnl().id(1L).build();
        anlageBericht.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());
        var anlageBerichtOld = aFullEURegAnl().id(2L).build();
        anlageBerichtOld.getBerichtsdaten().setBearbeitungsstatus(aReferenz(RBEA, "BS01").id(1L).build());

        when(euRegAnlageRepository.findById(anyLong())).thenReturn(Optional.of(anlageBerichtOld));
        when(stammdatenService.canWriteBetriebsstaette(any())).thenReturn(true);

        // Act
        service.updateAnlagenBericht(anlageBericht);

        // Assert
        ArgumentCaptor<EURegAnlage> captor = ArgumentCaptor.forClass(EURegAnlage.class);
        verify(euRegUpdater).update(any(), captor.capture());
        assertThat(captor.getValue().getBerichtsdaten().getBearbeitungsstatusSeit()).isNotEqualTo(LocalDate.now());
    }

}
