package de.wps.bube.basis.stammdaten.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.base.web.PageMapperImpl;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.model.BetriebsstaettenListStateDto;
import de.wps.bube.basis.base.web.openapi.model.FilterStateDtoBetriebsstaetteColumn;
import de.wps.bube.basis.base.web.openapi.model.FilterStateDtoBetriebsstaetteColumnPropertyEnum;
import de.wps.bube.basis.base.web.openapi.model.PageStateDto;
import de.wps.bube.basis.base.web.openapi.model.SortStateDtoBetriebsstaetteColumn;
import de.wps.bube.basis.base.web.openapi.model.SortStateDtoBetriebsstaetteColumnByEnum;
import de.wps.bube.basis.stammdaten.domain.vo.BetriebsstaetteColumn;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { StammdatenListStateMapperImpl.class, PageMapperImpl.class })
class StammdatenListStateDtoMapperTest {

    @Autowired
    private StammdatenListStateMapperImpl mapper;

    @Test
    void fromBetriebsstaetteListItemDto() {
        BetriebsstaettenListStateDto state = new BetriebsstaettenListStateDto();

        FilterStateDtoBetriebsstaetteColumn column = new FilterStateDtoBetriebsstaetteColumn();
        column.setValue("test");
        column.setProperty(FilterStateDtoBetriebsstaetteColumnPropertyEnum.NAME);
        state.setFilters(Collections.singletonList(column));

        PageStateDto page = new PageStateDto();
        page.setCurrent(5);
        page.setSize(10);
        state.setPage(page);

        SortStateDtoBetriebsstaetteColumn sort = new SortStateDtoBetriebsstaetteColumn();
        sort.setBy(SortStateDtoBetriebsstaetteColumnByEnum.NAME);
        sort.setReverse(false);
        state.setSort(sort);

        ListStateDto<BetriebsstaetteColumn> listState = mapper.fromBetriebsstaetteListItemDto(state);

        assertThat(listState.page.current).isEqualTo(page.getCurrent());
        assertThat(listState.page.size).isEqualTo(page.getSize());

        assertThat(listState.filters.get(0).value).isEqualTo(column.getValue());
        assertThat(listState.filters.get(0).property.toString()).isEqualTo(column.getProperty().getValue());
    }
}
