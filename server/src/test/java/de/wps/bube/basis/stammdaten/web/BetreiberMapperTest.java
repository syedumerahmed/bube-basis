package de.wps.bube.basis.stammdaten.web;

import static de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder.aFullAdresse;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aFullBetreiber;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;

class BetreiberMapperTest {

    final BetreiberMapper mapper = new BetreiberMapperImpl(new ReferenzMapperImpl());

    @Test
    void toListDto() {
        var betreiber = aFullBetreiber().name("betreiber")
                                        .adresse(aFullAdresse().ort("ort").plz("plz").build())
                                        .build();

        var dto = mapper.toListDto(betreiber);

        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(betreiber.getId());
        assertThat(dto.getBetreiberNummer()).isEqualTo(betreiber.getBetreiberNummer());
        assertThat(dto.getBetreiberdatenUebernommen()).isEqualTo(betreiber.isBetreiberdatenUebernommen());
        assertThat(dto.getName()).isEqualTo(betreiber.getName());
        assertThat(dto.getLetzteAenderungBetreiber()).isNull();
        assertThat(dto.getDisplay()).isEqualTo("betreiber - plz - ort");
    }
}
