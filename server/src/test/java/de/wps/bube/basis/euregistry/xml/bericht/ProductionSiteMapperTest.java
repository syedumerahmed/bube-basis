package de.wps.bube.basis.euregistry.xml.bericht;

import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.from;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base.Identifier;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ReportData;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;

class ProductionSiteMapperTest {

    private ProductionSiteMapper mapper;

    @BeforeEach
    void setUp() {
        CommonEURegMapperImpl commonEURegMapper = new CommonEURegMapperImpl();
        KoordinatenTransformationService koordinatenTransformationService = mock(KoordinatenTransformationService.class);
        when(koordinatenTransformationService.transformToEPSG4258(any())).thenReturn(new double[] {0.0, 0.0});
        commonEURegMapper.koordinatenTransformationService = koordinatenTransformationService;
        mapper = new ProductionSiteMapperImpl(commonEURegMapper);
    }

    @Test
    void toXDto() {
        var euRegBst = aEURegBetriebsstaette().build();
        var reportData = new ReportData();
        reportData.setReportingYear("2020");
        reportData.setCountryId(new ReferenceType("schema", "DE"));
        var inspireParam = new InspireParam("insp_ns", "insp_site", "insp_extension");
        var context = mock(ProductionSiteMapper.ProductionSiteMapperContext.class);
        when(context.getReportData()).thenReturn(reportData);
        when(context.getInspireParam()).thenReturn(inspireParam);

        var xdto = mapper.toXDto(euRegBst, context);

        assertThat(xdto).isNotNull();
        assertThat(xdto).extracting(ProductionSite::getInspireID)
                        .isNotNull()
                        .extracting(InspireID::getIdentifier)
                        .isNotNull()
                        .returns(euRegBst.getLocalId() + inspireParam.getSite(), from(Identifier::getLocalId))
                        .returns(inspireParam.getNamespace(), from(Identifier::getNamespace));
        assertThat(xdto.getSiteName().getNameOfFeature()).isEqualTo(euRegBst.getBetriebsstaette().getName());
        assertThat(xdto.getReportData().getHref()).isEqualTo("#" + reportData.getGmlId());
        verify(context).afterProductionSite(same(xdto));

    }
}
