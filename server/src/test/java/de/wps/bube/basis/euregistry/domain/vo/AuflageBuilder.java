package de.wps.bube.basis.euregistry.domain.vo;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATE;

import de.wps.bube.basis.euregistry.domain.entity.Auflage;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public final class AuflageBuilder {
    private Referenz emissionswert;
    private boolean nachArt18;
    private boolean nachArt14;

    private AuflageBuilder() {
    }

    public static AuflageBuilder anAuflage() {
        return new AuflageBuilder();
    }

    public static AuflageBuilder aFullAuflage() {
        return anAuflage()
                .emissionswert(aReferenz(RBATE, "RBATE").build())
                .nachArt14(true);
    }

    public AuflageBuilder emissionswert(Referenz emissionswert) {
        this.emissionswert = emissionswert;
        return this;
    }

    public AuflageBuilder nachArt18(boolean nachArt18) {
        this.nachArt18 = nachArt18;
        return this;
    }

    public AuflageBuilder nachArt14(boolean nachArt14) {
        this.nachArt14 = nachArt14;
        return this;
    }

    public Auflage build() {
        return new Auflage(emissionswert, nachArt18, nachArt14);
    }
}
