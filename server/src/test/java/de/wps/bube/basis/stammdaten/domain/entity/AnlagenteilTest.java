package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

class AnlagenteilTest {

    @Test
    void hasSameKey() {
        var a1 = anAnlagenteil().parentAnlageId(2L).anlagenteilNr("nummer").id(1L).build();
        var a2 = anAnlagenteil().parentAnlageId(2L).anlagenteilNr("nummer").id(2L).build();

        assertThat(a1.hasSameKey(a2)).isTrue();
    }

    @Test
    void hasSameKey_otherParent() {
        var a1 = anAnlagenteil().parentAnlageId(1L).anlagenteilNr("nummer").build();
        var a2 = anAnlagenteil().parentAnlageId(2L).anlagenteilNr("nummer").build();

        assertThat(a1.hasSameKey(a2)).isFalse();
    }

    @Test
    void hasSameKey_otherNummer() {
        var a1 = anAnlagenteil().parentAnlageId(1L).anlagenteilNr("nummer1").build();
        var a2 = anAnlagenteil().parentAnlageId(1L).anlagenteilNr("nummer2").build();

        assertThat(a1.hasSameKey(a2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull() {
        var a1 = anAnlagenteil().localId("local").build();
        var a2 = anAnlagenteil().localId("local").build();

        assertThat(a1.hasSameLocalIdOrNull(a2)).isTrue();
    }

    @Test
    void hasSameLocalIdOrNull_otherId() {
        var a1 = anAnlagenteil().localId("local").build();
        var a2 = anAnlagenteil().localId("local-id").build();

        assertThat(a1.hasSameLocalIdOrNull(a2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull_FirstNull() {
        var a1 = anAnlagenteil().localId(null).build();
        var a2 = anAnlagenteil().localId("local-id").build();

        assertThat(a1.hasSameLocalIdOrNull(a2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull_SecondNull() {
        var a1 = anAnlagenteil().localId("local-id").build();
        var a2 = anAnlagenteil().localId(null).build();

        assertThat(a1.hasSameLocalIdOrNull(a2)).isTrue();
    }

    @Test
    void hatEuRegAnl_keineRelevanteVorschrift_PRTR() {
        var prtr = aVorschriftReferenz(VorschriftSchluessel.V_RPRTR.getSchluessel()).build();
        var a = anAnlagenteil().vorschriften(aVorschrift().art(prtr).build()).build();
        assertThat(a.hatEURegAnl()).isFalse();
    }

    @Test
    void hatEuRegAnl_relevanteVorschrift_IERL() {
        var ierl = aVorschriftReferenz(VorschriftSchluessel.V_IE_RL.getSchluessel()).build();
        var a = anAnlagenteil().vorschriften(aVorschrift().art(ierl).build()).build();
        assertThat(a.hatEURegAnl()).isTrue();
    }

    @Test
    void hatEuRegAnl_isFeuerungsanlage_IERL() {
        var ierl = aVorschriftReferenz(VorschriftSchluessel.V_IE_RL.getSchluessel()).build();
        var a = anAnlagenteil().vorschriften(aVorschrift().art(ierl).build()).build();
        assertThat(a.isFeuerungsanlage()).isFalse();
    }

    @Test
    void hatEuRegAnl_isFeuerungsanlage_13BV() {
        var v13bv = aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build();
        var a = anAnlagenteil().vorschriften(aVorschrift().art(v13bv).build()).build();
        assertThat(a.isFeuerungsanlage()).isTrue();
        assertThat(a.isFeuerungsanlageMit17BV()).isFalse();
        assertThat(a.isFeuerungsanlageMit13BV()).isTrue();
    }

    @Test
    void hatEuRegAnl_isFeuerungsanlage_17BV() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();
        var a = anAnlagenteil().vorschriften(aVorschrift().art(v17bv).build()).build();
        assertThat(a.isFeuerungsanlage()).isTrue();
        assertThat(a.isFeuerungsanlageMit17BV()).isTrue();
        assertThat(a.isFeuerungsanlageMit13BV()).isFalse();
    }

    @Test
    void pruefeLoeschungFeuerungsanlageBerichtsdaten_17und13BV_leer() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();
        var v13bv = aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build();

        var a = anAnlagenteil().vorschriften(List.of(
                aVorschrift().art(v17bv).build(),
                aVorschrift().art(v13bv).build()))
                               .build();
        var anlageToBeSaved = anAnlagenteil().vorschriften(Collections.emptyList()).build();

        assertThat(a.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)).isTrue();
    }

    @Test
    void pruefeLoeschungFeuerungsanlageBerichtsdaten_17und13BV_13BV() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();
        var v13bv = aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build();

        var a = anAnlagenteil().vorschriften(List.of(
                aVorschrift().art(v17bv).build(),
                aVorschrift().art(v13bv).build()))
                               .build();
        var anlageToBeSaved = anAnlagenteil().vorschriften(aVorschrift().art(v13bv).build()).build();

        assertThat(a.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)).isTrue();
    }

    @Test
    void pruefeLoeschungFeuerungsanlageBerichtsdaten_17und13BV_17BV() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();
        var v13bv = aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build();

        var a = anAnlagenteil().vorschriften(List.of(
                aVorschrift().art(v17bv).build(),
                aVorschrift().art(v13bv).build()))
                               .build();
        var anlageToBeSaved = anAnlagenteil().vorschriften(aVorschrift().art(v17bv).build()).build();

        assertThat(a.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)).isTrue();
    }

    @Test
    void pruefeLoeschungFeuerungsanlageBerichtsdaten_17BV_17BV() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();

        var a = anAnlagenteil().vorschriften(aVorschrift().art(v17bv).build()).build();
        var anlageToBeSaved = anAnlagenteil().vorschriften(aVorschrift().art(v17bv).build()).build();

        assertThat(a.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)).isFalse();
    }

    @Test
    void pruefeLoeschungFeuerungsanlageBerichtsdaten_leer_17BV() {
        var v17bv = aVorschriftReferenz(VorschriftSchluessel.V_17BV.getSchluessel()).build();

        var a = anAnlagenteil().vorschriften(Collections.emptyList()).build();
        var anlageToBeSaved = anAnlagenteil().vorschriften(aVorschrift().art(v17bv).build()).build();

        assertThat(a.pruefeLoeschungFeuerungsanlageBerichtsdaten(anlageToBeSaved)).isFalse();
    }

    @Test
    void hasDoppelteVorschriften() {
        var v1 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();
        var v2 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();

        var a = anAnlagenteil().vorschriften(List.of(
                        aVorschrift().art(v1).build(),
                        aVorschrift().art(v2).build()))
                .build();

        assertThat(a.hasDoppelteVorschriften()).isTrue();
    }

    @Test
    void hasDoppelteVorschriften_shouldReturnFalse() {
        var v1 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();
        var v2 = aVorschriftReferenz("TestSchlüssel2").id(2L).build();

        var a = anAnlagenteil().vorschriften(List.of(
                        aVorschrift().art(v1).build(),
                        aVorschrift().art(v2).build()))
                .build();

        assertThat(a.hasDoppelteVorschriften()).isFalse();
    }

    @Test
    void hasDoppelteVorschriften_empty() {
        var a = anAnlagenteil().build();

        assertThat(a.hasDoppelteVorschriften()).isFalse();
    }
}
