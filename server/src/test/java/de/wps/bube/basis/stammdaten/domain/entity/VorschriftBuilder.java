package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aTaetigkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RIET;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RPRTR;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_13BV;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_17BV;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_IE_RL;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_RPRTR;
import static org.springframework.util.CollectionUtils.isEmpty;

import java.util.List;

import com.google.common.collect.Lists;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

public final class VorschriftBuilder {
    private Referenz art;
    private List<Referenz> taetigkeiten;
    private Referenz haupttaetigkeit;

    private VorschriftBuilder() {
    }

    public static VorschriftBuilder aVorschrift() {
        return new VorschriftBuilder().art(aVorschriftReferenz(VorschriftSchluessel.V_IE_RL.getSchluessel()).build());
    }

    public static VorschriftBuilder aVorschriftMitTaetigkeiten() {
        var haupttaetigkeit = aTaetigkeitsReferenz("haupt").build();
        return aVorschrift().haupttaetigkeit(haupttaetigkeit)
                            .taetigkeiten(haupttaetigkeit, aTaetigkeitsReferenz("neben").build());
    }

    public static VorschriftBuilder a13BImSchVVorschrift() {
        return aVorschrift().art(aVorschriftReferenz(V_13BV.getSchluessel()).build());
    }

    public static VorschriftBuilder a17BImSchVVorschrift() {
        return aVorschrift().art(aVorschriftReferenz(V_17BV.getSchluessel()).build());
    }

    public static VorschriftBuilder anIerlVorschrift() {
        var haupttaetigkeit = aTaetigkeitsReferenz(RIET, "haupt").build();
        return aVorschrift()
                .art(aVorschriftReferenz(V_IE_RL.getSchluessel()).build())
                .taetigkeiten(haupttaetigkeit, aTaetigkeitsReferenz(RIET, "neben").build())
                .haupttaetigkeit(haupttaetigkeit);
    }

    public static VorschriftBuilder aPrtrVorschrift() {
        var haupttaetigkeit = aTaetigkeitsReferenz(RPRTR, "haupt").build();
        return aVorschrift()
                .art(aVorschriftReferenz(V_RPRTR.getSchluessel()).build())
                .taetigkeiten(haupttaetigkeit, aTaetigkeitsReferenz(RPRTR, "neben").build())
                .haupttaetigkeit(haupttaetigkeit);
    }

    public VorschriftBuilder art(Referenz art) {
        this.art = art;
        return this;
    }

    public VorschriftBuilder taetigkeiten(List<Referenz> taetigkeiten) {
        this.taetigkeiten = taetigkeiten;
        if (!isEmpty(taetigkeiten)) {
            this.haupttaetigkeit = taetigkeiten.get(0);
        }
        return this;
    }

    public VorschriftBuilder taetigkeiten(Referenz... taetigkeiten) {
        this.taetigkeiten = Lists.newArrayList(taetigkeiten);
        return this;
    }

    public VorschriftBuilder haupttaetigkeit(Referenz haupttaetigkeit) {
        this.haupttaetigkeit = haupttaetigkeit;
        return this;
    }

    public Vorschrift build() {
        return new Vorschrift(art, taetigkeiten, haupttaetigkeit);
    }
}
