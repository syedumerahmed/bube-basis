package de.wps.bube.basis.stammdaten.domain.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;

@ExtendWith(MockitoExtension.class)
class AnsprechpartnerLoeschenJobTest {

    @Mock
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Mock
    private BenutzerJobRegistry benutzerJobRegistry;
    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private AnsprechpartnerLoeschenJob ansprechpartnerLoeschenJob;

    @Captor
    ArgumentCaptor<Betriebsstaette> captor;

    public static final String USER = "testUser";
    public static final Referenz REFERENZJAHR = ReferenzBuilder.aJahrReferenz(2021).build();
    public static final Referenz REFERENZJAHR_OLD = ReferenzBuilder.aJahrReferenz(2008).build();

    public static final Betriebsstaette BETRIEBSSTAETTE_HH = BetriebsstaetteBuilder.aFullBetriebsstaette()
                                                                                   .land(Land.HAMBURG)
                                                                                   .berichtsjahr(REFERENZJAHR)
                                                                                   .ansprechpartner(
                                                                                           Collections.emptyList())
                                                                                   .build();
    public static final Betriebsstaette BETRIEBSSTAETTE_NRW = BetriebsstaetteBuilder.aFullBetriebsstaette()
                                                                                    .ansprechpartner(
                                                                                            Collections.emptyList())
                                                                                    .berichtsjahr(REFERENZJAHR).build();

    @BeforeEach
    void setUp() {

        Stream<Betriebsstaette> betriebsstaetteStream = Stream.of(BETRIEBSSTAETTE_HH, BETRIEBSSTAETTE_NRW);

        when(betriebsstaetteRepository.findAllByBerichtsjahr(any())).thenReturn(betriebsstaetteStream);
    }

    @Test
    void runAsync_GesamtAdmin() {
        // ACT
        ansprechpartnerLoeschenJob.runAsync(REFERENZJAHR, USER, Land.DEUTSCHLAND);


        // ASSERT
        verify(betriebsstaetteRepository, times(2)).saveAndFlush(captor.capture());
        List<Betriebsstaette> captorBetriebsstaetteList = captor.getAllValues();

        List<Land> landList = captorBetriebsstaetteList.stream()
                                                      .map(Betriebsstaette::getLand)
                                                      .collect(Collectors.toList());
        assert(landList.contains(BETRIEBSSTAETTE_HH.getLand()));
        assert(landList.contains(BETRIEBSSTAETTE_NRW.getLand()));
    }

    @Test
    void runAsync_LandesAdmin_HH() {
        // ACT
        ansprechpartnerLoeschenJob.runAsync(REFERENZJAHR, USER, Land.HAMBURG);

        // ASSERT
        verify(betriebsstaetteRepository, times(1)).saveAndFlush(captor.capture());
        List<Betriebsstaette> captorBetriebsstaetteList = captor.getAllValues();

        List<Land> landList = captorBetriebsstaetteList.stream()
                                                      .map(Betriebsstaette::getLand)
                                                      .collect(Collectors.toList());
        assert(landList.contains(BETRIEBSSTAETTE_HH.getLand()));
        assert(!landList.contains(BETRIEBSSTAETTE_NRW.getLand()));
    }
}
