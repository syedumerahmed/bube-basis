package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVertraulichkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder.aFullAdresse;
import static de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder.anAdresse;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.base.vo.Land;

public final class BetreiberBuilder {
    private Long id;
    private String referenzAnlagenkataster;
    private Land land;
    private String betreiberNummer;
    private Referenz berichtsjahr;
    private Adresse adresse;
    private String name;
    private Referenz vertraulichkeitsgrund;
    private String url;
    private String bemerkung;
    private boolean betreiberdatenUebernommen;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private Instant letzteAenderungBetreiber;

    private BetreiberBuilder() {
    }

    public static BetreiberBuilder aBetreiber() {
        return new BetreiberBuilder().name("betreiber").land(Land.HAMBURG).adresse(anAdresse().build());
    }

    public static BetreiberBuilder aBetreiber(Long id) {
        return aBetreiber().id(id);
    }

    public static BetreiberBuilder aFullBetreiber() {
        return aBetreiber(1L).referenzAnlagenkataster("refAnlagen")
                             .adresse(aFullAdresse().build())
                             .land(Land.HAMBURG)
                             .betreiberNummer("1234")
                             .vertraulichkeitsgrund(aVertraulichkeitsReferenz("BetreiberVertraulich").build())
                             .letzteAenderung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                             .ersteErfassung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                             .jahr(aJahrReferenz(2020).build());
    }

    public BetreiberBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public BetreiberBuilder referenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        return this;
    }

    public BetreiberBuilder jahr(Referenz berichtsjahr) {
        this.berichtsjahr = berichtsjahr;
        return this;
    }

    public BetreiberBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public BetreiberBuilder betreiberNummer(String nummer) {
        this.betreiberNummer = nummer;
        return this;
    }

    public BetreiberBuilder adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public BetreiberBuilder name(String name) {
        this.name = name;
        return this;
    }

    public BetreiberBuilder vertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        return this;
    }

    public BetreiberBuilder url(String url) {
        this.url = url;
        return this;
    }

    public BetreiberBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public BetreiberBuilder betreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        return this;
    }

    public BetreiberBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public BetreiberBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public BetreiberBuilder letzteAenderungBetreiber(Instant letzteAenderung) {
        this.letzteAenderungBetreiber = letzteAenderung;
        return this;
    }

    public Betreiber build() {
        Betreiber betreiber = new Betreiber(id, name);
        betreiber.setReferenzAnlagenkataster(referenzAnlagenkataster);
        betreiber.setLand(land);
        betreiber.setBerichtsjahr(berichtsjahr);
        betreiber.setBetreiberNummer(betreiberNummer);
        betreiber.setAdresse(adresse);
        betreiber.setVertraulichkeitsgrund(vertraulichkeitsgrund);
        betreiber.setUrl(url);
        betreiber.setBemerkung(bemerkung);
        betreiber.setBetreiberdatenUebernommen(betreiberdatenUebernommen);
        betreiber.setErsteErfassung(ersteErfassung);
        betreiber.setLetzteAenderung(letzteAenderung);
        betreiber.setLetzteAenderungBetreiber(letzteAenderungBetreiber);
        return betreiber;
    }

}
