package de.wps.bube.basis.referenzdaten.web;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.R4BV;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.base.vo.Land;

@SpringBootTest(classes = ReferenzMapperImpl.class)
class ReferenzMapperTest {

    @Autowired
    private ReferenzMapper mapper;

    @Test
    void identity() {
        Referenz r = ReferenzBuilder.aReferenz(R4BV, "S")
                                    .id(100L)
                                    .letzteAenderung(Instant.now())
                                    .ltext("ltext")
                                    .land(Land.DEUTSCHLAND)
                                    .sortier("01")
                                    .ktext("ktext")
                                    .gueltigVon(2007)
                                    .gueltigBis(2099)
                                    .eu("eu")
                                    .strg("strg")
                                    .build();

        var dto = mapper.toDto(r);
        var referenz = mapper.fromDto(dto);

        assertThat(referenz).usingRecursiveComparison().isEqualTo(r);
    }

}
