package de.wps.bube.basis.test;

import org.mapstruct.Mapper;

/**
 * Empty Mapper, damit beim mvn test-compile keine Annotation Fehler kommen
 * https://github.com/mapstruct/mapstruct/issues/1638
 */
@Mapper
public interface EmptyMapper {
}
