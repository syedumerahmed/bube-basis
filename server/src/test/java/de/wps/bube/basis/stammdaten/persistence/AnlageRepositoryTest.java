package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class AnlageRepositoryTest {

    @Autowired
    private AnlageRepository repository;

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @Test
    public void testFindById() {

        var anlage = AnlageRepositoryTest.getAnlage();
        var anlageRead = repository.save(anlage);

        assertThat(repository.findById(anlageRead.getId())).isPresent();

    }

    @Test
    public void testNotNull() {

        var anlage = new Anlage("0100", 200L, "neue Anlage");

        assertThrows(RuntimeException.class, () -> repository.saveAndFlush(anlage));
    }

    @Test
    public void testCreate() {

        var anlage = AnlageRepositoryTest.getAnlage();

        assertNull(anlage.getId());
        var anlageRead = repository.save(anlage);
        assertThat(anlageRead.getId()).isNotNull();
        assertThat(anlage.getAnlageNr()).isEqualTo(anlageRead.getAnlageNr());
        assertThat(anlageRead.getErsteErfassung()).isEqualTo(anlageRead.getErsteErfassung());
        assertThat(anlage.getLetzteAenderung()).isEqualTo(anlageRead.getLetzteAenderung());
    }

    @Test
    public void testDelete() {

        var anlage = AnlageRepositoryTest.getAnlage();
        var anlageRead = repository.save(anlage);

        assertThat(repository.findById(anlageRead.getId())).isPresent();
        repository.delete(anlageRead);
        assertThat(repository.findById(anlageRead.getId())).isEmpty();

    }

    @Test
    void testVorschriften() {

        var b = anAnlage();
        var vorschrift1 = referenzRepository.save(aVorschriftReferenz("11. BImSchV").build());
        var vorschrift2 = referenzRepository.save(aVorschriftReferenz("4. BImSchV").build());
        var taetigkeit1 = referenzRepository.save(aTaetigkeitsReferenz("Tät").build());
        var taetigkeit2 = referenzRepository.save(aTaetigkeitsReferenz("igkeit").build());
        var taetigkeiten = List.of(taetigkeit1, taetigkeit2);
        b.vorschriften(List.of(new Vorschrift(vorschrift1, taetigkeiten, taetigkeit1),
                new Vorschrift(vorschrift2, taetigkeiten, taetigkeit2)));

        var bRead = repository.saveAndFlush(b.build());
        assertThat(bRead.getVorschriften()).hasSize(2);
        assertThat(bRead.getVorschriften().get(0).getTaetigkeiten()).hasSize(2);

        assertDoesNotThrow(() -> repository.saveAndFlush(
                anAnlage(2L).vorschriften(new Vorschrift(vorschrift1, taetigkeiten, taetigkeit1),
                        new Vorschrift(vorschrift2, taetigkeiten, taetigkeit2)).build()));

    }

    @Test
    void testLeistungen() {

        var b = getAnlage();
        var leistungen = new ArrayList<Leistung>();
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());
        leistungen.add(AnlageRepositoryTest.getLeistung());

        b.setLeistungen(leistungen);

        var bRead = repository.save(b);
        assertThat(bRead.getLeistungen()).hasSize(7);

        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead = repository.save(b);
        assertThat(bRead.getLeistungen()).isEmpty();

    }

    @Test
    void testFindByParentBetriebsstaetteIdAndAnlageNr() {
        var behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        var jahr = referenzRepository.save(aJahrReferenz(2020).build());
        var gemeinde = referenzRepository.save(aGemeindeReferenz("G1").build());
        var betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        var betriebsstaette = betriebsstaetteRepository.save(BetriebsstaetteBuilder.aBetriebsstaette(30L)
                                                                                   .zustaendigeBehoerde(behoerde)
                                                                                   .betriebsstatus(betriebsstatus)
                                                                                   .berichtsjahr(jahr)
                                                                                   .gemeindekennziffer(gemeinde)
                                                                                   .build());
        var anlage1 = repository.save(AnlageBuilder.anAnlage(1L)
                                                   .parentBetriebsstaetteId(betriebsstaette.getId())
                                                   .anlageNr("nummer1")
                                                   .build());
        repository.save(AnlageBuilder.anAnlage(2L)
                                     .parentBetriebsstaetteId(betriebsstaette.getId())
                                     .anlageNr("nummer2")
                                     .build());

        var anlage = repository.findByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(betriebsstaette.getId(), "nummer1");

        assertThat(anlage).hasValue(anlage1);
    }

    @Test
    void existsByParentBetriebsstaetteIdAndAnlageNr() {
        BetriebsstaetteBuilder bstBuilder
                = BetriebsstaetteBuilder.aBetriebsstaette()
                                        .zustaendigeBehoerde(behoerdenRepository.save(aBehoerde("BEHD1").build()))
                                        .betriebsstatus(referenzRepository.save(aBetriebsstatusReferenz("Neu").build()))
                                        .berichtsjahr(referenzRepository.save(aJahrReferenz(2020).build()))
                                        .gemeindekennziffer(referenzRepository.save(aGemeindeReferenz("G1").build()));

        Betriebsstaette bst1 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST1").build());
        Betriebsstaette bst2 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST2").build());
        repository.save(anAnlage().parentBetriebsstaetteId(bst1.getId()).anlageNr("abc").build());

        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "ABC")).isTrue();
    }

    private static Anlage getAnlage() {
        return new Anlage("0100", 1L, "Name");
    }

    private static Leistung getLeistung() {
        return new Leistung(100.0121D, aLeistungseinheitReferenz("m³").build(), "Bezug",
                Leistungsklasse.of("installiert"));
    }
}
