package de.wps.bube.basis.stammdaten.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.web.openapi.model.DownloadKomplexpruefungCommand;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefungBuilder;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungenMap;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;

@ExtendWith(MockitoExtension.class)
class BetriebsstaettePruefungProtokollServiceTest {

    private static final ZoneOffset ZONE_OFFSET;

    static {
        LocalDateTime summertime = LocalDateTime.of(2021, 7, 2, 8, 24);
        ZoneId zone = ZoneId.of("Europe/Berlin");
        ZONE_OFFSET = zone.getRules().getOffset(summertime);
    }

    @Mock
    private KomplexpruefungService komplexpruefungService;
    @Mock
    private StammdatenService stammdatenService;

    private BetriebsstaettePruefungProtokollService service;

    @BeforeEach
    void setUp() {
        service = new BetriebsstaettePruefungProtokollService(komplexpruefungService, stammdatenService);
    }

    @Test
    void protokolliereBetriebsstaette_ohneFehler() throws IOException {

        var bst = BetriebsstaetteBuilder.aFullBetriebsstaette().build();
        DownloadKomplexpruefungCommand command = new DownloadKomplexpruefungCommand();
        command.setBstId(bst.getId());
        Komplexpruefung komplexpruefung = new Komplexpruefung();
        bst.setKomplexpruefung(komplexpruefung);
        komplexpruefung.setId(1L);
        komplexpruefung.setAusfuehrung(LocalDateTime.of(2021, 7, 2, 8, 24, 55).toInstant(ZONE_OFFSET));

        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);
        when(komplexpruefungService.readRegelPruefungen(komplexpruefung)).thenReturn(new RegelPruefungenMap());


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        service.protokolliere(outputStream, command.getBstId());

        String protokoll = """
                Komplexprüfung Start: 2021-07-02T08:24:55 Jahr: 2018
                Das Protokoll enthält 0 Fehler und 0 Warnungen.

                Betriebsstätte: nummer | Halsbrecher Betrieb
                  Betreiber: 1234 | betreiber
                  Anlage: 1 | Name
                    Anlagenteil: 1 | Name
                    Quelle: 3 | quelle
                  Anlage: 2 | Name
                  Quelle: 1 | quelle
                  Quelle: 2 | quelle
                """;
        assertThat(outputStream.toString()).isEqualToNormalizingNewlines(protokoll);
    }

    @Test
    void protokolliereBetriebsstaette_mitFehler() throws IOException {

        var bst = BetriebsstaetteBuilder.aFullBetriebsstaette().build();
        DownloadKomplexpruefungCommand command = new DownloadKomplexpruefungCommand();
        command.setBstId(bst.getId());
        Komplexpruefung komplexpruefung = new Komplexpruefung();
        bst.setKomplexpruefung(komplexpruefung);
        komplexpruefung.addAnzahlFehler(2L);
        komplexpruefung.addAnzahlWarnungen(2L);
        komplexpruefung.setId(1L);
        komplexpruefung.setAusfuehrung(LocalDateTime.of(2021, 7, 2, 8, 24, 55).toInstant(ZONE_OFFSET));

        Stream<RegelPruefung> stream = Stream.of(
                RegelPruefungBuilder.aRegelPruefungMitFehler()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.BST)
                                    .pruefObjektId(bst.getId())
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitWarnung()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.BST)
                                    .pruefObjektId(bst.getId())
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitWarnung()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.ANLAGE)
                                    .pruefObjektId(bst.getAnlagen().get(0).getId())
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitFehler()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.ANLAGE)
                                    .pruefObjektId(bst.getAnlagen().get(1).getId())
                                    .build()
        );
        RegelPruefungenMap regelPruefungenMap = new RegelPruefungenMap();
        stream.forEach(regelPruefungenMap::put);

        when(stammdatenService.loadBetriebsstaetteForRead(bst.getId())).thenReturn(bst);
        when(komplexpruefungService.readRegelPruefungen(komplexpruefung)).thenReturn(regelPruefungenMap);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        service.protokolliere(outputStream, command.getBstId());

        String protokoll = """
                Komplexprüfung Start: 2021-07-02T08:24:55 Jahr: 2018
                Das Protokoll enthält 2 Fehler und 2 Warnungen.

                Betriebsstätte: nummer | Halsbrecher Betrieb
                FEHLER 42: Dies ist eine Fehlernachricht
                WARNUNG 21: Dies ist eine Warnung
                  Betreiber: 1234 | betreiber
                  Anlage: 1 | Name
                  WARNUNG 21: Dies ist eine Warnung
                    Anlagenteil: 1 | Name
                    Quelle: 3 | quelle
                  Anlage: 2 | Name
                  FEHLER 42: Dies ist eine Fehlernachricht
                  Quelle: 1 | quelle
                  Quelle: 2 | quelle
                """;
        assertThat(outputStream.toString()).isEqualToNormalizingNewlines(protokoll);
    }
}