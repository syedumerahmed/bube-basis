package de.wps.bube.basis.jobs.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class JobStatusRepositoryTest {

    @Autowired
    private JobStatusRepository repository;

    @Test
    public void testFindById() {
        final var jobStatus = getJobStatus();

        assertFalse(repository.findById(jobStatus.getUsername()).isPresent());
        repository.saveAndFlush(jobStatus);
        assertTrue(repository.findById(jobStatus.getUsername()).isPresent());
    }

    @Test
    public void testCreate() {
        final var jobStatus = getJobStatus();
        final var jobStatusRead = repository.save(jobStatus);

        assertEquals(jobStatus.getUsername(), jobStatusRead.getUsername());
        assertEquals(jobStatus.getJobName(), jobStatusRead.getJobName());
        assertFalse(jobStatus.isWarnungVorhanden());
    }

    @Test
    public void testUpdate() {

        final var jobStatus = repository.save(getJobStatus());
        jobStatus.addToCount(100);
        jobStatus.setCompleted();
        jobStatus.setWarnungVorhanden(true);

        final var jobStatusRead = repository.save(jobStatus);

        assertTrue(jobStatusRead.isCompleted());
        assertEquals(100, jobStatus.getCount());
        assertTrue(jobStatusRead.isWarnungVorhanden());
    }

    @Test
    public void testDelete() {

        final var jobStatus = repository.save(getJobStatus());

        assertFalse(repository.findById(jobStatus.getUsername()).isEmpty());
        repository.delete(jobStatus);
        assertTrue(repository.findById(jobStatus.getUsername()).isEmpty());
    }

    private static JobStatus getJobStatus() {
        JobStatus s = new JobStatus(JobStatusEnum.BENUTZER_IMPORTIEREN);
        s.setUsername("username");
        s.setProtokoll("Dies ist das Protokoll");
        return s;
    }
}
