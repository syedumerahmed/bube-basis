package de.wps.bube.basis.referenzdaten.domain.vo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class GueltigkeitsjahrTest {

    @Test
    void of() {
        assertNotNull(Gueltigkeitsjahr.of(2099));
    }

    @ParameterizedTest
    @ValueSource(ints = { 2019, 2007 })
    void isValid(int jahr) {
        assertThat(Gueltigkeitsjahr.isInvalid(jahr)).isFalse();
    }

    @ParameterizedTest
    @ValueSource(ints = { 1899, 1, 99991 })
    void isInvalid(int jahr) {
        assertThat(Gueltigkeitsjahr.isInvalid(jahr)).isTrue();
    }

    @Test
    void getJahr() {
        assertEquals(Gueltigkeitsjahr.of(2099).getJahr(), 2099);
    }

}
