package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.SACHSEN;
import static de.wps.bube.basis.base.vo.Validierungshinweis.Severity.FEHLER;
import static de.wps.bube.basis.base.vo.Validierungshinweis.Severity.WARNUNG;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.multipart.MultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.StreamingXMLEntityWriter;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.event.EuRegistryImportTriggeredEvent;
import de.wps.bube.basis.euregistry.xml.EURegBstXmlMapper;
import de.wps.bube.basis.euregistry.xml.EuRegBetriebsstaetteImportDtoMapper;
import de.wps.bube.basis.euregistry.xml.EuRegistryImportException;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBetriebsstaetteImportDto;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBst;
import de.wps.bube.basis.euregistry.xml.dto.EuRegBstListXDto;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobsException;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

@ExtendWith(MockitoExtension.class)
class EURegistryImportExportServiceTest {
    @Mock
    private EURegistryService euRegistryService;
    @Mock
    private XMLService xmlService;
    @Mock
    private EURegBstXmlMapper euRegBstXmlMapper;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private DateiService dateiService;
    @Mock
    private EuRegBetriebsstaetteImportDtoMapper euRegBetriebsstaetteImportDtoMapper;

    private EURegistryImportExportService service;
    private AktiverBenutzer aktiverBenutzer;

    @BeforeEach
    void setUp() {
        aktiverBenutzer = new AktiverBenutzer("username", EnumSet.allOf(Rolle.class),
                Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND).build());
        service = new EURegistryImportExportService(euRegistryService, xmlService, euRegBstXmlMapper, eventPublisher,
                aktiverBenutzer, dateiService);
    }

    @Nested
    class Export {
        @Test
        void shouldFailWhenGesamtadminAndDifferentLaender(@Mock OutputStream outputStream) {
            var ids = List.of(1L, 2L, 3L);
            when(euRegistryService.haveSameLand(ids)).thenReturn(false);

            assertThrows(JobsException.class, () -> service.export(outputStream, ids));
            verifyNoInteractions(xmlService, euRegBstXmlMapper);
            verifyNoMoreInteractions(euRegistryService);
        }

        @Test
        void shouldFailWhenDifferentJahre(@Mock OutputStream outputStream) {
            var ids = List.of(1L, 2L, 3L);
            when(euRegistryService.haveSameLand(ids)).thenReturn(true);
            when(euRegistryService.haveSameJahr(ids)).thenReturn(false);

            assertThrows(JobsException.class, () -> service.export(outputStream, ids));
            verifyNoInteractions(xmlService, euRegBstXmlMapper);
            verifyNoMoreInteractions(euRegistryService);
        }

        @Test
        void shouldWriteXml(@Mock OutputStream outputStream, @Mock Stream<EURegBetriebsstaette> entities, @Mock
                StreamingXMLEntityWriter<EURegBetriebsstaette, EuRegBst> writer) {
            var ids = List.of(5L);
            when(euRegistryService.haveSameLand(ids)).thenReturn(true);
            when(euRegistryService.haveSameJahr(ids)).thenReturn(true);
            when(euRegistryService.streamAllByBerichtIdIn(ids)).thenReturn(entities);
            when(xmlService.createWriter(same(EuRegBst.class),
                    same(EuRegBstListXDto.QUALIFIED_NAME),
                    ArgumentMatchers.<Function<EURegBetriebsstaette, EuRegBst>>any(),
                    any())).thenReturn(writer);
            when(writer.withErrorHandler(any())).thenReturn(writer);

            service.export(outputStream, ids);

            verify(writer).write(entities, outputStream);

            verifyNoMoreInteractions(xmlService, euRegBstXmlMapper, euRegistryService);
        }
    }

    @Nested
    class Import {

        @Test
        void shouldPublishEvent() {
            var eventCaptor = ArgumentCaptor.forClass(EuRegistryImportTriggeredEvent.class);

            service.triggerImport("2020");

            verify(eventPublisher).publishEvent(eventCaptor.capture());
            assertThat(eventCaptor.getValue()).isInstanceOf(EuRegistryImportTriggeredEvent.class);
            assertThat(eventCaptor.getValue().berichtsjahr()).isEqualTo("2020");
        }

        @Test
        void shouldCheckForExisting() {
            service.existierendeDatei();

            verify(dateiService).existingDatei(aktiverBenutzer.getUsername(), DateiScope.EUREG);
        }

        @Test
        void shouldSaveFile(@Mock MultipartFile file) {
            service.dateiSpeichern(file);

            verify(dateiService).dateiSpeichern(file, aktiverBenutzer.getUsername(), DateiScope.EUREG);
        }

        @Test
        void shouldDeleteFile() {
            service.dateiLoeschen();

            verify(dateiService).loescheDatei(aktiverBenutzer.getUsername(), DateiScope.EUREG);
        }

        @Test
        void shouldValidateFile(@Mock InputStream inputStream) {
            when(dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG)).thenReturn(inputStream);
            var validierungshinweis = Validierungshinweis.of("Ein Hinweis!", WARNUNG);
            doAnswer(invocation -> invocation.<List<Validierungshinweis>>getArgument(2).add(validierungshinweis))
                    .when(xmlService).validate(same(inputStream), eq(EuRegBstListXDto.XSD_SCHEMA_URL), anyList());

            var hinweise = service.validiere("2019");

            assertThat(hinweise).contains(validierungshinweis);
        }

        @Test
        void shouldValidateFile_WrongBerichtsjahr(@Mock InputStream inputStream) {
            when(dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG)).thenReturn(inputStream);
            var importDto = new EuRegBst();
            importDto.setLand(HAMBURG.getNr());
            importDto.setJahr("2017");
            doAnswer(invocation -> {
                invocation.<Consumer<EntityWithError<EuRegBst>>>getArgument(5)
                          .accept(new EntityWithError<>(importDto));
                return null;
            }).when(xmlService)
              .read(same(inputStream), eq(EuRegBst.class),
                      ArgumentMatchers.<Function<EuRegBst, EntityWithError<EuRegBst>>>any(),
                      eq(EuRegBstListXDto.XML_ROOT_ELEMENT_NAME), isNull(), any());

            var hinweise = service.validiere("2019");

            assertThat(hinweise).singleElement().satisfies(hinweis -> {
                assertThat(hinweis.severity).isEqualTo(FEHLER);
                assertThat(hinweis.hinweis).contains("Ungültiges Berichtsjahr '2017'");
            });
        }

        @Test
        void shouldValidateFile_MultipleLaender(@Mock InputStream inputStream) {
            when(dateiService.getDateiStream(aktiverBenutzer.getUsername(), DateiScope.EUREG)).thenReturn(inputStream);
            doAnswer(invocation -> {
                var entityConsumer = invocation.<Consumer<EntityWithError<EuRegBst>>>getArgument(5);
                Stream.of(HAMBURG.getNr(), SACHSEN.getNr())
                      .map(landNr -> {
                          var euRegBst = new EuRegBst();
                          euRegBst.setLand(landNr);
                          euRegBst.setJahr("2019");
                          return euRegBst;
                      })
                      .map(EntityWithError::new)
                      .forEach(entityConsumer);
                return null;
            }).when(xmlService)
              .read(same(inputStream), eq(EuRegBst.class),
                      ArgumentMatchers.<Function<EuRegBst, EntityWithError<EuRegBst>>>any(),
                      eq(EuRegBstListXDto.XML_ROOT_ELEMENT_NAME), isNull(), any());

            var hinweise = service.validiere("2019");

            assertThat(hinweise).singleElement().satisfies(hinweis -> {
                assertThat(hinweis.severity).isEqualTo(FEHLER);
                assertThat(hinweis.hinweis).contains("Es können nur die Daten eines Landes importiert werden");
            });
        }

        @Test
        void shouldNotImportWithError() {
            var protokoll = new JobProtokoll("Präambel");
            var importedEntity = new EntityWithError<>(
                    new EuRegBetriebsstaetteImportDto(aEURegBetriebsstaette().build(), emptyList()),
                    List.of(new MappingError("ID", "Fehler")));

            assertThrows(EuRegistryImportException.class, () -> service.importiereEuRegBetriebsstaette(importedEntity,
                    importedEntity.entity.getEuRegBetriebsstaette()
                                         .getBetriebsstaette()
                                         .getBerichtsjahr()
                                         .getSchluessel(),
                    protokoll));

            assertThat(protokoll.getProtokoll()).startsWith("Präambel");
            assertThat(protokoll.getProtokoll()).contains("ID: Fehler");
            verify(euRegistryService, never()).updateBetriebsstaettenBericht(any());
            verify(euRegistryService, never()).updateAnlagenBericht(any());
        }

        @Test
        void shouldNotImportWhenNotFound() {
            var protokoll = new JobProtokoll();
            var importedEntity = new EntityWithError<>(
                    new EuRegBetriebsstaetteImportDto(aEURegBetriebsstaette().build(), emptyList()), emptyList());
            var bst = importedEntity.entity.getEuRegBetriebsstaette().getBetriebsstaette();
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(
                    bst.getBerichtsjahr().getSchluessel(), bst.getLocalId()))
                    .thenReturn(Optional.empty());

            service.importiereEuRegBetriebsstaette(importedEntity,
                    importedEntity.entity.getEuRegBetriebsstaette()
                                         .getBetriebsstaette()
                                         .getBerichtsjahr()
                                         .getSchluessel(), protokoll);

            assertThat(protokoll.getProtokoll())
                    .containsSubsequence("EuRegBetriebsstaette für Betriebsstätte", bst.getName(),
                            bst.getBetriebsstaetteNr(), bst.getLocalId(), "wurde nicht gefunden");

            verify(euRegistryService, never()).updateBetriebsstaettenBericht(any());
            verify(euRegistryService, never()).updateAnlagenBericht(any());
        }

        @Test
        void shouldNotImportWhenWrongBerichtsjahr() {
            var protokoll = new JobProtokoll();
            var importedEntity = new EntityWithError<>(
                    new EuRegBetriebsstaetteImportDto(aEURegBetriebsstaette()
                            .betriebsstaette(aFullBetriebsstaette().berichtsjahr(aJahrReferenz(2018).build()).build())
                            .build(),
                            emptyList()), emptyList());
            var bst = importedEntity.entity.getEuRegBetriebsstaette().getBetriebsstaette();

            assertThrows(EuRegistryImportException.class,
                    () -> service.importiereEuRegBetriebsstaette(importedEntity, "2019", protokoll));

            assertThat(protokoll.getProtokoll())
                    .containsSubsequence("EuRegBetriebsstaette für Betriebsstätte", bst.getName(), bst.getLocalId(),
                            "falsches Jahr", "2018");

            verify(euRegistryService, never()).updateBetriebsstaettenBericht(any());
            verify(euRegistryService, never()).updateAnlagenBericht(any());
        }

        @Test
        void shouldUpdateWhenFound() {
            var protokoll = new JobProtokoll();
            var importedEntity = new EntityWithError<>(
                    new EuRegBetriebsstaetteImportDto(aEURegBetriebsstaette().build(), emptyList()), emptyList());
            var bst = importedEntity.entity.getEuRegBetriebsstaette().getBetriebsstaette();
            EURegBetriebsstaette existingEuRegBst = aEURegBetriebsstaette().id(20L).betriebsstaette(bst).build();
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(
                    bst.getBerichtsjahr().getSchluessel(), bst.getLocalId()))
                    .thenReturn(Optional.of(existingEuRegBst));

            service.importiereEuRegBetriebsstaette(importedEntity,
                    importedEntity.entity.getEuRegBetriebsstaette()
                                         .getBetriebsstaette()
                                         .getBerichtsjahr()
                                         .getSchluessel(), protokoll);

            assertThat(protokoll.getProtokoll())
                    .containsSubsequence("EuRegBetriebsstaette für Betriebsstätte", bst.getName(),
                            bst.getBetriebsstaetteNr(), bst.getLocalId(), "wurde aktualisiert");

            verify(euRegistryService).updateBetriebsstaettenBericht(
                    same(importedEntity.entity.getEuRegBetriebsstaette()));
            verify(euRegistryService, never()).updateAnlagenBericht(any());
        }
    }
}
