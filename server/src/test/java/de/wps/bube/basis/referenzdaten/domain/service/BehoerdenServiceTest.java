package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;

@ExtendWith(MockitoExtension.class)
class BehoerdenServiceTest {

    @Mock
    private BehoerdenRepository repository;
    @Mock
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    private BehoerdeBerechtigungsAccessor behoerdenBerechtigungsAccessor;
    @Mock
    private EigeneBehoerdenBerechtigungsAcessor eigeneBehoerdenBerechtigungsAcessor;

    private BehoerdenService service;

    @BeforeEach
    void setUp() {
        lenient().when(stammdatenBerechtigungsContext.createPredicateRead(any())).thenReturn(new BooleanBuilder());
        lenient().when(stammdatenBerechtigungsContext.createPredicateWrite(any())).thenReturn(new BooleanBuilder());
        service = new BehoerdenService(repository, stammdatenBerechtigungsContext, behoerdenBerechtigungsAccessor,
                eigeneBehoerdenBerechtigungsAcessor);
    }

    @Test
    void readBehoerde_notFound() {
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> service.readBehoerde(1L));
    }

    @Test
    void readBehoerde_ok() {
        var b = aBehoerde("moin").build();
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));

        assertThat(service.readBehoerde(2L)).isEqualTo(b);
    }

    @Test
    void loadBehoerde_notFound() {
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class,
                () -> service.loadBehoerde("schluessel", Gueltigkeitsjahr.of(2020), Land.HESSEN));
    }

    @Test
    void loadBehoerde_ok() {
        var b = aBehoerde("moin").land(Land.HESSEN).build();
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));

        assertThat(service.loadBehoerde("schluessel", Gueltigkeitsjahr.of(2020), Land.HESSEN)).isEqualTo(b);
    }

    @Test
    void createBehoerde_duplicateKey() {
        var land = Land.SCHLESWIG_HOLSTEIN;
        var schluessel = "moin";
        var b = aBehoerde(schluessel).land(land).gueltigVon(2010).gueltigBis(2030).build();
        when(repository.functionalKeyDuplicate(null, land.getNr(), schluessel, 2010, 2030)).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.createBehoerde(b));
    }

    @Test
    void createBehoerde_ok() {
        var land = Land.SCHLESWIG_HOLSTEIN;
        var schluessel = "moin";
        var b = mock(Behoerde.class);
        when(b.getId()).thenReturn(null);
        when(b.getLand()).thenReturn(land);
        when(b.getSchluessel()).thenReturn(schluessel);
        when(b.getGueltigVon()).thenReturn(Gueltigkeitsjahr.of(2010));
        when(b.getGueltigBis()).thenReturn(Gueltigkeitsjahr.of(2010));

        when(repository.functionalKeyDuplicate(null, land.getNr(), schluessel, 2010, 2010)).thenReturn(false);

        service.createBehoerde(b);

        verify(b).pruefeGueltigkeit();
        verify(repository).save(b);
    }

    @Test
    void updateBehoerde_keineBerechtigung_existing() {
        var b = aBehoerde("moin").id(22L).build();
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> service.updateBehoerde(b));
    }

    @Test
    void updateBehoerde_duplicateKey() {
        var land = Land.SCHLESWIG_HOLSTEIN;
        var schluessel = "moin";
        var id = 23L;
        var b = aBehoerde(schluessel).id(id).land(land).gueltigVon(2010).gueltigBis(2030).build();
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(repository.functionalKeyDuplicate(id, land.getNr(), schluessel, 2010, 2030)).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.updateBehoerde(b));
    }

    @Test
    void updateBehoerde_ok() {
        var land = Land.SCHLESWIG_HOLSTEIN;
        var schluessel = "moin";
        var b = mock(Behoerde.class);
        var id = 33L;
        when(b.getId()).thenReturn(id);
        when(b.getLand()).thenReturn(land);
        when(b.getSchluessel()).thenReturn(schluessel);
        when(b.getGueltigVon()).thenReturn(Gueltigkeitsjahr.of(2010));
        when(b.getGueltigBis()).thenReturn(Gueltigkeitsjahr.of(2010));

        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(repository.functionalKeyDuplicate(id, land.getNr(), schluessel, 2010, 2010)).thenReturn(false);

        service.updateBehoerde(b);

        verify(b).pruefeGueltigkeit();
        verify(repository).save(b);
    }
}
