package de.wps.bube.basis.referenzdaten.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class ParameterWertRepositoryTest {

    @Autowired
    private ParameterWertRepository repository;

    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @Test
    void save() {

        var parameterWert = getWert();
        assertNull(parameterWert.getId());

        var parameterWertRead = repository.save(parameterWert);

        assertNotNull(parameterWertRead.getId());
        assertEquals(parameterWert.getPschluessel(), parameterWertRead.getPschluessel());
        assertEquals(parameterWert.getLand(), parameterWertRead.getLand());
        assertEquals(parameterWert.getWert(), parameterWertRead.getWert());
        assertEquals(parameterWert.getGueltigVon(), parameterWertRead.getGueltigVon());
        assertEquals(parameterWert.getGueltigBis(), parameterWertRead.getGueltigBis());
        assertEquals(parameterWert.getLetzteAenderung(), parameterWertRead.getLetzteAenderung());

    }

    @Test
    void findByPSchluessel() {

        var hh = getWert();
        hh.setLand(Land.HAMBURG);
        repository.save(hh);

        var h = getWert();
        h.setLand(Land.HESSEN);
        repository.save(h);

        var nw = getWert();
        nw.setLand(Land.NORDRHEIN_WESTFALEN);
        repository.save(nw);

        assertFalse(repository.findByPschluessel(hh.getPschluessel()).isEmpty());
        assertTrue(repository.findByPschluessel("KEIN_SCHLUESSEL").isEmpty());
    }

    @Test
    void findByKeyWithoutBehoerde() {

        var pw = getWert();
        pw.setLand(Land.HAMBURG);
        pw.setGueltigVon(Gueltigkeitsjahr.of(2000));
        pw.setGueltigBis(Gueltigkeitsjahr.of(2005));
        repository.save(pw);

        assertThat(repository.findByKeyWithoutBehoerde(pw.getLand(), pw.getPschluessel(),
                Gueltigkeitsjahr.of(1999))).isEmpty();
        assertThat(repository.findByKeyWithoutBehoerde(pw.getLand(), pw.getPschluessel(),
                Gueltigkeitsjahr.of(2000))).isPresent();
        assertThat(repository.findByKeyWithoutBehoerde(pw.getLand(), pw.getPschluessel(),
                Gueltigkeitsjahr.of(2006))).isEmpty();
    }

    @Test
    void findByKeyWithBehoerde() {
        
        var behoerde = behoerdenRepository.saveAndFlush(BehoerdeBuilder.aBehoerde("15").build());

        var pw = getWert();
        pw.setLand(Land.HAMBURG);
        pw.setGueltigVon(Gueltigkeitsjahr.of(2000));
        pw.setGueltigBis(Gueltigkeitsjahr.of(2005));
        pw.setBehoerde(behoerde);
        repository.save(pw);

        assertThat(repository.findByKeyWithBehoerde(pw.getLand(), pw.getPschluessel(), Gueltigkeitsjahr.of(1999),
                behoerde.getId())).isEmpty();
        assertThat(repository.findByKeyWithBehoerde(pw.getLand(), pw.getPschluessel(), Gueltigkeitsjahr.of(2000),
                behoerde.getId())).isPresent();
        assertThat(repository.findByKeyWithBehoerde(pw.getLand(), pw.getPschluessel(), Gueltigkeitsjahr.of(2006),
                behoerde.getId())).isEmpty();
    }

    @Test
    void findByPSchluesselAndLandIn() {

        var hh = getWert();
        hh.setLand(Land.HAMBURG);
        repository.save(hh);

        var h = getWert();
        h.setLand(Land.HESSEN);
        repository.save(h);

        var nw = getWert();
        nw.setLand(Land.NORDRHEIN_WESTFALEN);
        repository.save(nw);

        assertEquals(0,
                repository.findByPschluesselAndLandIn(hh.getPschluessel(), List.of(Land.SCHLESWIG_HOLSTEIN)).size());
        assertEquals(2,
                repository.findByPschluesselAndLandIn(hh.getPschluessel(), List.of(Land.HAMBURG, Land.HESSEN)).size());

    }

    @Test
    void fk_parameter_schluessel() {
        var wert = getWert();
        wert.setPschluessel("KEIN_SCHLUESSEL");

        assertThrows(DataIntegrityViolationException.class, () -> repository.saveAndFlush(wert));
    }

    @Test
    void delete() {
        var parameterWertRead = repository.save(getWert());
        assertTrue(repository.existsById(parameterWertRead.getId()));
        repository.deleteById(parameterWertRead.getId());
        assertFalse(repository.existsById(parameterWertRead.getId()));
    }

    private ParameterWert getWert() {
        return ParameterWertBuilder.aParameterWert().build();
    }

}
