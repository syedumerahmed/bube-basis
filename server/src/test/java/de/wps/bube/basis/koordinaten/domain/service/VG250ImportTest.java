package de.wps.bube.basis.koordinaten.domain.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.koordinaten.persistence.GemeindekoordinatenRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;

@SpringBootTest(classes = { VG250ImportJob.class, BenutzerJobRegistry.class })
@EnableJpaRepositories(value = { "de.wps.bube.basis.koordinaten.persistence", "de.wps.bube.basis.referenzdaten.persistence" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({ "de.wps.bube.basis.koordinaten.domain", "de.wps.bube.basis.referenzdaten.domain" })
class VG250ImportJobIntegrationTest extends BubeIntegrationTest {
    public static final String DOWNLOAD_LINK = "https://daten.gdz.bkg.bund.de/produkte/vg/vg250_ebenen_0101/aktuell/vg250_01-01.utm32s.shape.ebenen.zip";
    public Referenz R_2020 = ReferenzBuilder.aJahrReferenz(2020).build();

    @MockBean
    private BenutzerJobRegistry jobRegistry;

    @Autowired
    private VG250ImportJob vG250ImportJob;

    @Autowired
    ReferenzRepository referenzRepository;

    @Autowired
    private GemeindekoordinatenRepository gemeindekoordinatenRepository;

    @BeforeEach
    void setUp() {
        R_2020 = referenzRepository.save(R_2020);
    }

    /* Das ShapeFile test.shp wurde für den Test extra klein gemacht */
    @Test
    void importShapeFile() throws IOException {
        File file = new File("src/test/resources/koordinaten/test.shp");
        assertThat(vG250ImportJob.importShapeFile(file.toURI().toURL(), new JobProtokoll(), R_2020)).isEqualTo(5L);

        gemeindekoordinatenRepository.flush();
    }

    /* Disabled damit nicht jedesmal der Download gemacht wird */
    @Test
    @Disabled
    void downloadShapeFile() throws IOException {
        vG250ImportJob.downloadZipFile(DOWNLOAD_LINK, new JobProtokoll());
        URL url =  vG250ImportJob.getShapeFilePath("");

        File shapeFile = new File(url.getFile());
        assertTrue(shapeFile.isFile());

        vG250ImportJob.deleteTempFiles(new JobProtokoll());
        shapeFile = new File(url.getFile());
        assertFalse(shapeFile.exists());

        gemeindekoordinatenRepository.flush();
    }
}
