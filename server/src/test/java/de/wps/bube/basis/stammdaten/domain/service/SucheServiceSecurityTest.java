package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_READ;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = SucheService.class)
class SucheServiceSecurityTest extends SecurityTest {

    @Autowired
    private SucheService service;

    @MockBean
    private BetriebsstaetteRepository betriebsstaetteRepository;

    @MockBean
    private SuchattributePredicateFactory suchattributePredicatefactory;

    @MockBean
    private BetriebsstaettenBerechtigungsAccessor berechtigungsAccessor;

    @Test
    @WithBubeMockUser
    void shouldDenyWithoutRoles() {
        assertThrows(AccessDeniedException.class, () -> service.suche(2020L, null, Pageable.unpaged()));
        verifyNoInteractions(betriebsstaetteRepository);
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_READ })
    void shouldAllowForBehoerde() {
        assertDoesNotThrow(() -> service.suche(2020L, null, Pageable.unpaged()));

        verify(betriebsstaetteRepository).findAllAsListItems(any(Predicate.class), eq(Pageable.unpaged()));
    }
}
