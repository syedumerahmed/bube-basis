package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtException;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@SpringBootTest(classes = XeurTeilberichtService.class)
class XeurTeilberichtServiceTest {

    @MockBean
    private XeurTeilberichtRepository xeurTeilberichtRepository;
    @MockBean
    private EURegistryService euRegistryService;
    @MockBean
    private EURegGMLService gmlService;
    @MockBean
    private BerichtPruefungService berichtPruefungService;
    @MockBean
    private RegelService regelService;
    @MockBean
    private KomplexpruefungService komplexpruefungService;
    @MockBean
    private ParameterService parameterService;
    @MockBean
    private EmailService emailService;
    @MockBean
    private BenutzerverwaltungService benutzerverwaltungService;
    @MockBean
    private AktiverBenutzer aktiverBenutzer;

    @Autowired
    XeurTeilberichtService xeurTeilberichtService;

    @Test
    void teilberichtFreigeben_keineEMail() {
        Benutzer benutzer = BenutzerBuilder.aBehoerdenbenutzer("benutzername", NORDRHEIN_WESTFALEN).build();
        when(benutzerverwaltungService.loadCurrentBenutzer()).thenReturn(benutzer);

        assertThrows(XeurTeilberichtException.class, () -> xeurTeilberichtService.teilberichtFreigeben(1L));
    }

    @Test
    void teilberichtFreigeben_nichtAbgabefaehig() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(NORDRHEIN_WESTFALEN)
                                                                .abgabefaehig(false)
                                                                .build();

        when(aktiverBenutzer.isGesamtadmin()).thenReturn(true);
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);

        Benutzer benutzer = BenutzerBuilder.aBehoerdenbenutzer("benutzername", NORDRHEIN_WESTFALEN)
                                           .email("test@bube.de")
                                           .build();
        when(benutzerverwaltungService.loadCurrentBenutzer()).thenReturn(benutzer);

        assertThrows(XeurTeilberichtException.class, ()
                -> xeurTeilberichtService.teilberichtFreigeben(xeurTeilbericht.getId()));
    }

    @Test
    void teilberichtFreigeben_bereitsFreigegeben() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(NORDRHEIN_WESTFALEN)
                                                                .abgabefaehig(true)
                                                                .freigegebenAt(Instant.now())
                                                                .build();

        when(aktiverBenutzer.isGesamtadmin()).thenReturn(true);
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);

        Benutzer benutzer = BenutzerBuilder.aBehoerdenbenutzer("benutzername", NORDRHEIN_WESTFALEN)
                                           .email("test@bube.de")
                                           .build();
        when(benutzerverwaltungService.loadCurrentBenutzer()).thenReturn(benutzer);

        assertThrows(XeurTeilberichtException.class, ()
                -> xeurTeilberichtService.teilberichtFreigeben(xeurTeilbericht.getId()));
    }

    @Test
    void teilberichtFreigeben_shouldSendEmail() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(NORDRHEIN_WESTFALEN)
                                                                .abgabefaehig(true)
                                                                .build();

        when(aktiverBenutzer.isGesamtadmin()).thenReturn(true);
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);

        Benutzer benutzer = BenutzerBuilder.aBehoerdenbenutzer("benutzername", NORDRHEIN_WESTFALEN)
                                           .email("test@bube.de")
                                           .build();
        when(benutzerverwaltungService.loadCurrentBenutzer()).thenReturn(benutzer);

        xeurTeilberichtService.teilberichtFreigeben(xeurTeilbericht.getId());

        String to = "test@bube.de";
        String ubaVerteiler = "bube-dev@wps.de";
        String from = "bube@umweltschutz-niedersachsen.de";
        String subject = "Freigabe der EU-Registry-Daten für das Berichtsjahr 2020 aus NORDRHEIN_WESTFALEN";
        String text = "Die EU-Registry-Daten des Landes NORDRHEIN_WESTFALEN für das Berichtsjahr 2020 wurden freigegeben. Freigabezeitpunkt ist";

        verify(emailService).sendSimpleMessage(eq(from), eq(subject), startsWith(text), eq(to), eq(ubaVerteiler));
    }

}