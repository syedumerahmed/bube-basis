package de.wps.bube.basis.sicherheit.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.sicherheit.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_WRITE;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.security.BetriebsstaetteBerechtigungsAdapter;

@ExtendWith(MockitoExtension.class)
class BetriebsstaetteStammdatenBerechtigungsContextTest {

    private StammdatenBerechtigungsContext context;

    @Nested
    class AsGesamtAdmin {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.DEUTSCHLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(GESAMTADMIN, LAND_WRITE, LAND_READ).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBetriebsstaetteOfLand() {
            Betriebsstaette betriebsstaette = aBetriebsstaette().land(Land.HESSEN).build();

            assertThatCode(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette))).doesNotThrowAnyException();
        }

    }

    @Nested
    class AsLandUserBundesland {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(LAND_WRITE, LAND_READ).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBetriebsstaetteOfOwnLand() {
            Betriebsstaette betriebsstaette = aBetriebsstaette().land(Land.SAARLAND).akz("AKZ3").build();

            assertThatCode(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            Betriebsstaette betriebsstaette = aBetriebsstaette().land(Land.HESSEN).build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette))).isInstanceOf(
                    AccessDeniedException.class);
        }
    }

    @Nested
    class AsBehoerdeUserBundesland {
        BetriebsstaetteBuilder matchingBetriebsstaetteBuilder;

        @BeforeEach
        void setUp() {
            var datenberechtigung = Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHN")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build();
            matchingBetriebsstaetteBuilder = aBetriebsstaette()
                    .land(datenberechtigung.land)
                    .zustaendigeBehoerde(aBehoerde("BEHD2").build())
                    .akz("AKZ2")
                    .gemeindekennziffer(ReferenzBuilder.aGemeindeReferenz("HHN0UHL").build())
                    .betriebsstaetteNr("BETRIEB2");
            var benutzer = aBehoerdenbenutzer().berechtigung(datenberechtigung).addRollen(BEHOERDE_WRITE).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBetriebsstaetteMatchingAllPredicates() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.build();

            assertThatCode(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.land(Land.SACHSEN_ANHALT).build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)))
                    .isInstanceOf(AccessDeniedException.class)
                    .hasMessageContaining("Landes 15");
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherBehoerde() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.zustaendigeBehoerde(
                    aBehoerde("NOT MINE").build()).build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)))
                    .isInstanceOf(AccessDeniedException.class)
                    .hasMessageContaining("Behörde NOT MINE");
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherAkz() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.akz("AKZ3").build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)))
                    .isInstanceOf(AccessDeniedException.class)
                    .hasMessageContaining("Akz AKZ3");
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherGemeinde() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.gemeindekennziffer(
                    ReferenzBuilder.aGemeindeReferenz("LLBKKGEM").build()).build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)))
                    .isInstanceOf(AccessDeniedException.class)
                    .hasMessageContaining("Verwaltungsgebiets LLBKKGEM");

        }

        @Test
        void shouldNotHaveAccessToOtherBetriebsstaette() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5")
                                                                            .build();

            assertThatThrownBy(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)))
                    .isInstanceOf(AccessDeniedException.class)
                    .hasMessageContaining("Betriebsstätte BETRIEB5");

        }
    }

    @Nested
    class AsBehoerdeWriteLandReadUser {
        BetriebsstaetteBuilder matchingBetriebsstaetteBuilder;

        @BeforeEach
        void setUp() {
            var datenberechtigung = Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHN")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build();
            matchingBetriebsstaetteBuilder = aBetriebsstaette()
                    .land(datenberechtigung.land)
                    .zustaendigeBehoerde(aBehoerde("BEHD2").build())
                    .akz("AKZ2")
                    .gemeindekennziffer(ReferenzBuilder.aGemeindeReferenz("HHN0UHL").build())
                    .betriebsstaetteNr("BETRIEB2");
            var benutzer = aBehoerdenbenutzer().berechtigung(datenberechtigung)
                                               .addRollen(LAND_READ, BEHOERDE_WRITE)
                                               .build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveWriteAccessToBetriebsstaetteMatchingAllPredicates() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.build();

            assertThatCode(() -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveWriteAccessToBetriebsstaetteNotMatching() {
            Betriebsstaette betriebsstaette = matchingBetriebsstaetteBuilder.zustaendigeBehoerde(
                    aBehoerde("BEHD5").build()).build();

            assertThrows(AccessDeniedException.class, () -> context.checkAccessToWrite(new BetriebsstaetteBerechtigungsAdapter(betriebsstaette)));
        }
    }
}
