package de.wps.bube.basis.euregistry.web.mapper;

import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten.EUREG_ANL_SCHLUESSEL;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.aFullAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.aFullAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageListItemDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;

@ExtendWith(MockitoExtension.class)
class EuRegAnlagenTreeToListMapperTest {

    @Mock
    private EURegistryService eURegistryService;

    private EuRegAnlagenTreeToListMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new EuRegAnlagenTreeToListMapper(eURegistryService);
    }

    @Test
    void mapAnlagen() {

        // Setup
        EURegBetriebsstaette b = aEURegBetriebsstaette().betriebsstaette(aFullBetriebsstaette().anlagen(
                aFullAnlage().id(1L).anlageNr("1")
                             .anlagenteile(aFullAnlagenteil().id(11L).anlagenteilNr("11").build()).build(),
                aFullAnlage().id(2L).anlageNr("2").anlagenteile().build(),
                aFullAnlage().id(3L).anlageNr("3").anlagenteile().build(),
                aFullAnlage().id(4L).anlageNr("4")
                             .anlagenteile(
                                     aFullAnlagenteil().id(41L).anlagenteilNr("41").build(),
                                     aFullAnlagenteil().id(42L).anlagenteilNr("42").build(),
                                     aFullAnlagenteil().id(43L).anlagenteilNr("43").build())
                             .build()).build()).build();

        when(eURegistryService.findAnlagenBerichtByAnlageId(1L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().berichtsdaten(
                aFullBerichtsdaten().berichtsart(aReferenz(RRTYP, EUREG_ANL_SCHLUESSEL).build()).build()).build()));
        when(eURegistryService.findAnlagenBerichtByAnlageId(2L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(11L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(41L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(42L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(43L)).thenReturn(Optional.of(
                EURegAnlageBuilder.aFullEURegAnl().build()));

        // Act
        List<EuRegAnlageListItemDto> dtoList = mapper.mapAnlagen(b.getBetriebsstaette().getAnlagen());

        // Assert
        assertThat(dtoList.size()).isEqualTo(6);

        assertThat(dtoList.get(0).getId()).isEqualTo(1L);
        assertThat(dtoList.get(1).getId()).isEqualTo(11L);
        assertThat(dtoList.get(2).getId()).isEqualTo(2L);
        assertThat(dtoList.get(3).getId()).isEqualTo(41L);
        assertThat(dtoList.get(4).getId()).isEqualTo(42L);
        assertThat(dtoList.get(5).getId()).isEqualTo(43L);

        assertThat(dtoList.get(0).getBerichtsart()).isEqualTo("EURegAnl");

        assertThat(dtoList.get(0).getIsAnlage()).isTrue();
        assertThat(dtoList.get(1).getIsAnlage()).isFalse();

        assertThat(dtoList.get(0).getParentAnlageNr()).isNull();
        assertThat(dtoList.get(1).getParentAnlageNr()).isEqualTo("1");

    }

}
