package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BETREIBER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Set;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockMultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerImportException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.event.BenutzerImportTriggeredEvent;
import de.wps.bube.basis.benutzerverwaltung.xml.BenutzerXMLMapper;
import de.wps.bube.basis.benutzerverwaltung.xml.dto.BenutzerListeXDto;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BenutzerverwaltungImportExportServiceTest {

    private static final String USER = "username";

    @Mock
    private KeycloakService keycloakService;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private DateiService dateiService;
    @Mock
    private XMLService xmlService;
    @Mock
    private AktiverBenutzer aktiverBenutzer;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private BenutzerXMLMapper benutzerXMLMapper;
    @Mock
    private BenutzerverwaltungService benutzerverwaltungService;
    @Mock
    private JobProtokoll jobProtokoll;

    private BenutzerverwaltungImportExportService service;

    @BeforeEach
    void setUp() {
        service = new BenutzerverwaltungImportExportService(benutzerverwaltungService, stammdatenService,
                aktiverBenutzer, keycloakService, eventPublisher, dateiService, xmlService, benutzerXMLMapper);
    }

    @Test
    void triggerImport() {
        service.triggerImport(USER);
        verify(eventPublisher).publishEvent(any(BenutzerImportTriggeredEvent.class));
    }

    @Test
    void dateiSpeichern() {
        byte[] bytes = {};
        var file = new MockMultipartFile("dateiName", bytes);

        service.dateiSpeichern(file, USER);

        verify(dateiService).dateiSpeichern(file, USER, DateiScope.BENUTZER);
    }

    @Test
    void existierendeDatei() {
        service.existierendeDatei(USER);
        verify(dateiService).existingDatei(USER, DateiScope.BENUTZER);
    }

    @Test
    void dateiLoeschen() {
        service.dateiLoeschen(USER);
        verify(dateiService).loescheDatei(USER, DateiScope.BENUTZER);
    }

    @Test
    void importiereBenutzer() {
        var benutzer = aBehoerdenbenutzer("user1", Land.BERLIN).betriebsstaetten("nummer-2").build();
        var betriebsstaetten = Set.of("nummer-1", "nummer-2");
        when(benutzerverwaltungService.createBenutzer(benutzer)).thenReturn(benutzer);

        service.importiereBenutzer(benutzer, betriebsstaetten, Set.of(), jobProtokoll);

        assertThat(benutzer.getZugewieseneRollen()).containsExactly(BETREIBER);
        verify(benutzerverwaltungService).createBenutzer(benutzer);
    }

    @Test
    void importiereBenutzer_ignoreCase() {
        var benutzer = aBehoerdenbenutzer("user1", Land.BERLIN).betriebsstaetten("NUMMER-3").build();
        var betriebsstaetten = Set.of("nummer-2", "nummer-3");
        when(benutzerverwaltungService.createBenutzer(benutzer)).thenReturn(benutzer);

        service.importiereBenutzer(benutzer, betriebsstaetten, Set.of(), jobProtokoll);

        assertThat(benutzer.getZugewieseneRollen()).containsExactly(BETREIBER);
        verify(benutzerverwaltungService).createBenutzer(benutzer);
    }

    @Test
    void importiereBenutzer_update() {
        var benutzer = aBehoerdenbenutzer("user1", Land.BERLIN).id("22").betriebsstaetten("nummer-2").build();
        var betriebsstaetten = Set.of("nummer-1", "nummer-2");
        when(benutzerverwaltungService.loadBenutzerByName("user1")).thenReturn(benutzer);
        when(benutzerverwaltungService.updateBenutzer(benutzer)).thenReturn(benutzer);

        service.importiereBenutzer(benutzer, betriebsstaetten, Set.of("user1", "user2"), jobProtokoll);

        assertThat(benutzer.getZugewieseneRollen()).containsExactly(BETREIBER);
        assertThat(benutzer.getId()).isEqualTo("22");
        verify(benutzerverwaltungService).updateBenutzer(benutzer);
    }

    @Test
    void importiereBenutzer_bst_nichtVorhanden() {
        var benutzer = aBehoerdenbenutzer("user1", Land.BERLIN).betriebsstaetten("nummer-2").build();
        var betriebsstaetten = Set.of("nummer-1");

        assertThrows(BenutzerImportException.class,
                () -> service.importiereBenutzer(benutzer, betriebsstaetten, Set.of(), jobProtokoll));
        verifyNoMoreInteractions(keycloakService);
    }

    @Test
    void validiere_shouldCall_validiereFuerImport() {
        var b1 = mock(Benutzer.class);
        var b2 = mock(Benutzer.class);
        var b3 = mock(Benutzer.class);
        var benutzerToBeValidated = List.of(new EntityWithError<>(b1), new EntityWithError<>(b2),
                new EntityWithError<>(b3));

        when(b1.getBenutzername()).thenReturn("user1");
        when(b2.getBenutzername()).thenReturn("user2");
        when(b3.getBenutzername()).thenReturn("user3");

        doAnswer(invocation -> {
            benutzerToBeValidated.forEach(invocation.getArgument(5));
            return null;
        }).when(xmlService).read(any(), eq(BenutzerType.class), eq(benutzerXMLMapper),
                eq(BenutzerListeXDto.XML_ROOT_ELEMENT_NAME), any(), notNull());

        service.validiere(USER);

        benutzerToBeValidated.forEach(
                b -> verify(b.entity).validiereFuerImport(anySet(), anySet(), anySet(), anySet(), any()));
        assertThat(service.validiere(USER)).isEmpty();
    }
}
