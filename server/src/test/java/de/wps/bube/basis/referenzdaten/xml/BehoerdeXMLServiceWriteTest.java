package de.wps.bube.basis.referenzdaten.xml;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.stream.Stream;

import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;
import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.ObjectFactory;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.xmlunit.builder.Input;

import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;
import de.wps.bube.basis.referenzdaten.xml.dto.BehoerdenlisteXDto;

@SpringBootTest(classes = { BehoerdeXMLMapperImpl.class, CommonXMLMapperImpl.class, ReferenzResolverService.class })
class BehoerdeXMLServiceWriteTest {

    private static final String VALID_FULL_XML = "xml/behoerde_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BehoerdeXMLMapper mapper;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private BehoerdenService behoerdenService;

    @Test
    void writeBehoerde() throws IOException {
        var objectFactory = new ObjectFactory();
        var expectedXML = Input.fromStream(getResource(VALID_FULL_XML));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        var writer = xmlService.createWriter(BehoerdeType.class, BehoerdenlisteXDto.QUALIFIED_NAME,
                mapper::toXDto, objectFactory::createBehoerde);

        writer.write(Stream.of(getBehoerde()) , outputStream);

        MatcherAssert.assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
    }

    private Behoerde getBehoerde() {
        return BehoerdeBuilder.aBehoerde("100")
                              .id(11L)
                              .bezeichnung("bezeichnung")
                              .bezeichnung2("bezeichnung2")
                              .kommunikationsverbindungen(
                                      new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                              "040-67 676 576"),
                                      new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                              "040-67 676 576-0"))
                              .zustaendigkeiten(Zustaendigkeit.BIMSCHV11)
                              .letzteAenderung(Instant.parse("2020-06-30T14:00:00Z"))
                              .build();
    }

}
