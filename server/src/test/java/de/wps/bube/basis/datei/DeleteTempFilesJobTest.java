package de.wps.bube.basis.datei;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.datei.domain.service.DeleteTempFilesJob;

@ExtendWith(MockitoExtension.class)
class DeleteTempFilesJobTest {

    @Mock
    private DateiService dateiService;

    private DeleteTempFilesJob job;

    @BeforeEach
    void setUp() {
        this.job = new DeleteTempFilesJob(dateiService);
    }

    @Test
    void shouldDeleteTempFiles() {
        job.run();
        verify(dateiService).deleteTemporaryFiles();
    }
}
