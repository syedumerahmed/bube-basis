package de.wps.bube.basis.euregistry.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.base.sftp.SftpService;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder;
import de.wps.bube.basis.euregistry.domain.event.XeurTeilberichtFreigegebenEvent;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;

@SpringBootTest(classes = XeurGesamtberichtService.class)
@ActiveProfiles("noop-crypto")
class XeurGesamtberichtServiceTest {

    @MockBean
    private XeurTeilberichtRepository xeurTeilberichtRepository;
    @MockBean
    private EURegGMLService euRegGMLService;
    @MockBean
    private EmailService emailService;
    @MockBean
    private SftpService sftpService;
    @MockBean
    private BenutzerverwaltungService benutzerverwaltungService;

    @Autowired
    private XeurGesamtberichtService xeurGesamtberichtService;

    @BeforeEach
    void setUp() {
        Schema schema = mock(Schema.class);
        when(schema.newValidator()).thenReturn(mock(Validator.class));
        when(euRegGMLService.getSchema()).thenReturn(schema);
    }

    @Test
    void writeGesamtbericht() throws IOException {

        Referenz berichtsjahr = ReferenzBuilder.aJahrReferenz(2020).build();

        Function<String, String> fileContentReader = (suffix) -> {
            String prefix = this.getClass().getSimpleName() + "_writeGesamtbericht_";
            try {
                URL resource = this.getClass().getResource(prefix + suffix);
                return resource != null ? Files.readString(Paths.get(resource.toURI())) : null;
            } catch (IOException | URISyntaxException e) {
                throw new IllegalStateException(e);
            }
        };

        XeurTeilbericht teilbericht1 = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                             .xeurFile(fileContentReader.apply("teilbericht1.xml"))
                                                             .land(Land.HAMBURG)
                                                             .build();
        XeurTeilbericht teilbericht2 = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                             .xeurFile(fileContentReader.apply("teilbericht2.xml"))
                                                             .land(Land.NIEDERSACHSEN)
                                                             .build();

        when(xeurTeilberichtRepository.findAllLatestFreigegeben(berichtsjahr.getId())).thenReturn(
                Stream.of(teilbericht1, teilbericht2));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        xeurGesamtberichtService.writeGesamtberichtAndReturnLaenderListe(berichtsjahr, out);

        assertThat(out.toString()).isEqualToIgnoringNewLines(fileContentReader.apply("gesamtbericht.xml"));

    }

    @Test
    void handleTeilberichtFreigegeben_fehlerBeiSetup() {

        XeurTeilberichtFreigegebenEvent event = new XeurTeilberichtFreigegebenEvent(null);
        xeurGesamtberichtService.handleTeilberichtFreigegeben(event);

        verify(emailService).sendSimpleMessage(
                eq("bube@umweltschutz-niedersachsen.de"),
                eq("Fehler bei BUBE-Gesamtberichterstellung"),
                argThat(arg -> {
                    assertThat(arg).startsWith("Bei der Erstellung des Gesamtberichts ist ein Fehler aufgetreten:\n\n" +
                                               "java.lang.NullPointerException");
                    return true;
                }),
                eq(null),
                eq("bube-dev@wps.de"));

    }

    @Test
    void handleTeilberichtFreigegeben_fehlerBeiGesamtberichtErstellung() {

        Referenz berichtsjahr = ReferenzBuilder.aJahrReferenz(2020).build();
        XeurTeilberichtFreigegebenEvent event = new XeurTeilberichtFreigegebenEvent(berichtsjahr);
        when(xeurTeilberichtRepository.findAllLatestFreigegeben(berichtsjahr.getId()))
                .thenThrow(new EmptyResultDataAccessException(1));
        xeurGesamtberichtService.handleTeilberichtFreigegeben(event);

        verify(emailService).sendSimpleMessage(
                eq("bube@umweltschutz-niedersachsen.de"),
                eq("Fehler bei BUBE-Gesamtberichterstellung"),
                argThat(arg -> {
                    Pattern pattern = Pattern.compile(
                            "Beim Erzeugen der Gesamtberichtsdatei '.*2020-UBA_eureg_XEUR_.*\\.zip'" +
                            " ist ein Fehler aufgetreten:.*EmptyResultDataAccessException:.*",
                            Pattern.DOTALL);
                    assertThat(arg).matches(pattern);
                    return true;
                }),
                eq(null),
                eq("bube-dev@wps.de"));

    }

    @Test
    void handleTeilberichtFreigegeben_ohneFehler() throws IOException {

        Referenz berichtsjahr = ReferenzBuilder.aJahrReferenz(2020).build();
        XeurTeilberichtFreigegebenEvent event = new XeurTeilberichtFreigegebenEvent(berichtsjahr);
        xeurGesamtberichtService.handleTeilberichtFreigegeben(event);

        verifyNoInteractions(emailService);
        verify(sftpService).uploadFile(any(File.class));

    }

}