package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerImportJobStatus;
import de.wps.bube.basis.benutzerverwaltung.domain.persistence.BenutzerImportJobStatusRepository;
import de.wps.bube.basis.benutzerverwaltung.xml.BenutzerXMLMapper;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BenutzerImportierenJobTest {

    private static final String USER = "current_user";

    @Mock
    private BenutzerverwaltungImportExportService benutzerImportExportService;
    @Mock
    private BenutzerverwaltungService benutzerService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private BenutzerJobRegistry benutzerJobRegistry;
    @Mock
    private XMLService xmlService;
    @Mock
    private BenutzerXMLMapper benutzerXMLMapper;
    @Mock
    private DateiService dateiService;
    @Mock
    private BenutzerImportJobStatusRepository benutzerImportJobStatusRepository;

    private BenutzerImportierenJob job;

    @BeforeEach
    void setUp() {
        job = new BenutzerImportierenJob(benutzerImportExportService, benutzerService, stammdatenService,
                benutzerJobRegistry, xmlService, dateiService, benutzerXMLMapper, benutzerImportJobStatusRepository);
    }

    @Test
    void importUsers() {
        var toBeImported = Set.of(
                new EntityWithError<>(aBehoerdenbenutzer("user1", Land.BERLIN).email("user1@bube.de").build()),
                new EntityWithError<>(aBehoerdenbenutzer("user2", Land.SAARLAND).build()));
        Stream<Benutzer> allBetreiber = Stream.empty();
        when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("nummer-1"));
        when(benutzerService.listAllBetreiberBenutzer()).thenReturn(allBetreiber);
        when(benutzerImportJobStatusRepository.findById(USER)).thenReturn(Optional.of(new BenutzerImportJobStatus()));
        when(benutzerImportExportService.importiereBenutzer(any(Benutzer.class), any(), any(), any(JobProtokoll.class)))
                .thenAnswer(i -> i.getArgument(0));

        doAnswer(invocation -> {
            toBeImported.forEach(invocation.getArgument(5));
            return null;
        }).when(xmlService).read(any(), any(), ArgumentMatchers.<XDtoMapper<?, ?>>any(), any(), any(), any());

        job.runAsync(USER);

        toBeImported.forEach(
                b -> verify(benutzerImportExportService).importiereBenutzer(eq(b.entity), eq(Set.of("nummer-1")),
                        eq(Set.of()), any(JobProtokoll.class)));
        verify(benutzerImportExportService).dateiLoeschen(USER);
        verify(benutzerJobRegistry).jobCompleted(true);
        verify(benutzerImportJobStatusRepository).save(argThat(status -> {
            assertThat(status.getBenutzerNamen()).containsExactly("user2");
            return true;
        }));
    }

    @Test
    void exceptionInJob() {
        var e = new RuntimeException();
        when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenThrow(e);

        job.runAsync(USER);

        verify(benutzerJobRegistry).jobError(e);
        verify(benutzerImportExportService).dateiLoeschen(USER);
    }
}
