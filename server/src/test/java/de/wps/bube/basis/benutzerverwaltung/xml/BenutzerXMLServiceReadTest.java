package de.wps.bube.basis.benutzerverwaltung.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

import de.wps.bube.basis.benutzerverwaltung.xml.benutzer.dto.BenutzerType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.xml.dto.BenutzerListeXDto;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;

@SpringBootTest(classes = {BenutzerXMLMapperImpl.class, DatenberechtigungXMLMapperImpl.class})
class BenutzerXMLServiceReadTest {

    private static final String VALID_XML = "xml/benutzer_import_test.xml";
    private static final String INVALID_KENNUNG_XML = "xml/invalid_kennung_benutzer_import_test.xml";
    private static final String INVALID_EMAIL_XML = "xml/invalid_email_benutzer_import_test.xml";
    private static final String VALID_FACHMODULE_XML = "xml/valid_fachmodule_benutzer_import_test.xml";
    private static final String INVALID_NACHNAME_XML = "xml/invalid_nachname_benutzer_import_test.xml";
    private static final String INVALID_BSTLISTE_XML = "xml/invalid_bstliste_benutzer_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BenutzerXMLMapper benutzerXMLMapper;

    @Test
    void test_validXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(VALID_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(0);
    }

    @Test
    void read_invalidXml_kennung() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_KENNUNG_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(2);
    }

    @Test
    void read_invalidXml_keineBST() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_BSTLISTE_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise.get(0).hinweis)
                .startsWith("XML-Import Fehler Zeile 11: cvc-complex-type.2.4.b: Content des Elements 'betriebsstaetten' ist nicht vollständig.");

        assertEquals(Validierungshinweis.Severity.FEHLER, hinweise.get(0).severity);

    }

    @Test
    void read_invalidXml_email() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_EMAIL_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(2);
    }

    @Test
    void read_validXml_ohne_fachmodule() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(VALID_FACHMODULE_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(0);
    }

    @Test
    void read_invalidXml_nachname() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_NACHNAME_XML), BenutzerListeXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(2);
    }

    @Test
    void unmarshal() throws IOException, JAXBException, XMLStreamException {

        XMLInputFactory staxFactory = XMLInputFactory.newInstance();
        XMLEventReader staxReader = staxFactory.createXMLEventReader(getResource("xml/benutzer_test.xml"));

        JAXBContext jaxbContext = JAXBContext.newInstance(BenutzerType.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<BenutzerType> jaxbElement = unmarshaller.unmarshal(staxReader, BenutzerType.class);
        BenutzerType benutzer = jaxbElement.getValue();

        assertNotNull(benutzer.getDatenberechtigung());
        assertEquals("emustermann", benutzer.getBenutzername());
        assertEquals("Erika", benutzer.getVorname());

    }

    @Test
    void read_validXml() throws IOException {
        xmlService.read(getResource(VALID_XML), BenutzerType.class, benutzerXMLMapper,
                BenutzerListeXDto.XML_ROOT_ELEMENT_NAME, null, bWithErrors -> {
                    var benutzer = bWithErrors.entity;
                    assertThat(benutzer).isNotNull();
                    assertEquals("emustermann", benutzer.getBenutzername());
                    assertEquals("Erika", benutzer.getVorname());
                    assertEquals("Mustermann", benutzer.getNachname());
                    assertEquals("erika.muster@mail.de", benutzer.getEmail());
                    assertEquals("99999", benutzer.getTelefonnummer());

                    assertNotNull(benutzer.getDatenberechtigung());
                    assertEquals(Land.HAMBURG, benutzer.getDatenberechtigung().land);
                    assertThat(benutzer.getDatenberechtigung().betriebsstaetten).hasSize(2);
                    assertTrue(benutzer.getDatenberechtigung().fachmodule.containsAll(
                            Set.of(Fachmodul.PRTR_LCP, Fachmodul.E_ERKLAERUNG)));
                    assertTrue(benutzer.getDatenberechtigung().themen.isEmpty());

                });
    }

}
