package de.wps.bube.basis.komplexpruefung.domain.entity;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTestStatus;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;

public final class RegelPruefungBuilder {

    private Komplexpruefung komplexpruefung;
    private Long regelNummer;
    private RegelTestStatus status;
    private RegelObjekt regelObjekt;
    private Long pruefObjektId;
    private String message;
    private RegelTyp typ;

    private RegelPruefungBuilder() {
    }

    public static RegelPruefungBuilder aRegelPruefungMitFehler() {
        return new RegelPruefungBuilder().regelNummer(42L)
                                         .status(RegelTestStatus.BST_GUELTIG)
                                         .regelObjekt(RegelObjekt.BST)
                                         .message("Dies ist eine Fehlernachricht")
                                         .typ(RegelTyp.FEHLER);
    }

    public static RegelPruefungBuilder aRegelPruefungMitWarnung() {
        return new RegelPruefungBuilder().regelNummer(21L)
                                         .status(RegelTestStatus.BST_GUELTIG)
                                         .regelObjekt(RegelObjekt.BST)
                                         .message("Dies ist eine Warnung")
                                         .typ(RegelTyp.WARNUNG);
    }

    public RegelPruefungBuilder komplexpruefung(Komplexpruefung komplexpruefung) {
        this.komplexpruefung = komplexpruefung;
        return this;
    }

    public RegelPruefungBuilder regelNummer(Long regelNummer) {
        this.regelNummer = regelNummer;
        return this;
    }

    public RegelPruefungBuilder regelObjekt(RegelObjekt regelObjekt) {
        this.regelObjekt = regelObjekt;
        return this;
    }

    public RegelPruefungBuilder pruefObjektId(Long pruefObjektId) {
        this.pruefObjektId = pruefObjektId;
        return this;
    }

    public RegelPruefungBuilder status(RegelTestStatus status) {
        this.status = status;
        return this;
    }

    public RegelPruefungBuilder message(String message) {
        this.message = message;
        return this;
    }

    public RegelPruefungBuilder typ(RegelTyp typ) {
        this.typ = typ;
        return this;
    }

    public RegelPruefung build() {
        RegelPruefung regelPruefung = new RegelPruefung(komplexpruefung, regelNummer, status);
        regelPruefung.setRegelObjekt(regelObjekt);
        regelPruefung.setPruefObjektId(pruefObjektId);
        regelPruefung.setMessage(message);
        regelPruefung.setTyp(typ);
        return regelPruefung;
    }
}
