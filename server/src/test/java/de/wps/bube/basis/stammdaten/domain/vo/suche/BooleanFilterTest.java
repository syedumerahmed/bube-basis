package de.wps.bube.basis.stammdaten.domain.vo.suche;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.UEBERNOMMEN;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;

class BooleanFilterTest {

    @Test
    void shouldMapNull() {
        var booleanFilter = UEBERNOMMEN.createPredicate(new Suchattribute(), betriebsstaette);

        assertThat(booleanFilter).isNull();
    }

    @Test
    void shouldMapValue() {
        var booleanFilter = UEBERNOMMEN.createPredicate(new Suchattribute() {{
            betriebsstaette.uebernommen = true;
        }}, betriebsstaette);

        assertThat(booleanFilter).isEqualTo(QBetriebsstaette.betriebsstaette.betreiberdatenUebernommen.eq(true));
    }
}
