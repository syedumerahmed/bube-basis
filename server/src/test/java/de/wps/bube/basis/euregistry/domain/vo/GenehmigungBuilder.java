package de.wps.bube.basis.euregistry.domain.vo;

import java.time.LocalDate;

import de.wps.bube.basis.euregistry.domain.entity.Genehmigung;

public class GenehmigungBuilder {

    private Boolean erteilt;
    private Boolean neuBetrachtet;
    private Boolean geaendert;

    private LocalDate erteilungsdatum;
    private LocalDate neuBetrachtetDatum;
    private LocalDate aenderungsdatum;

    private String url;
    private String durchsetzungsmassnahmen;

    public static GenehmigungBuilder aFullGenehmigung() {
        return new GenehmigungBuilder().erteilt(true)
                                        .neuBetrachtet(false)
                                        .geaendert(false)
                                        .erteilungsdatum(LocalDate.parse("2019-08-10"))
                                        .url("https://genehmigung.url")
                                        .durchsetzungsmassnahmen("durchsetzungsmassnahmen");

    }

    public GenehmigungBuilder erteilt(Boolean erteilt) {
        this.erteilt = erteilt;
        return this;
    }

    public GenehmigungBuilder neuBetrachtet(Boolean neuBetrachtet) {
        this.neuBetrachtet = neuBetrachtet;
        return this;
    }

    public GenehmigungBuilder geaendert(Boolean geaendert) {
        this.geaendert = geaendert;
        return this;
    }

    public GenehmigungBuilder erteilungsdatum(LocalDate datum) {
        this.erteilungsdatum = datum;
        return this;
    }

    public GenehmigungBuilder neuBetrachtetDatum(LocalDate datum) {
        this.neuBetrachtetDatum = datum;
        return this;
    }

    public GenehmigungBuilder aenderungsdatum(LocalDate datum) {
        this.aenderungsdatum = datum;
        return this;
    }

    public GenehmigungBuilder url(String url) {
        this.url = url;
        return this;
    }

    public GenehmigungBuilder durchsetzungsmassnahmen(String durchsetzungsmassnahmen) {
        this.durchsetzungsmassnahmen = durchsetzungsmassnahmen;
        return this;
    }

    public Genehmigung build() {
        return new Genehmigung(
                erteilt,
                neuBetrachtet,
                geaendert,
                erteilungsdatum,
                neuBetrachtetDatum,
                aenderungsdatum,
                url,
                durchsetzungsmassnahmen);
    }
}
