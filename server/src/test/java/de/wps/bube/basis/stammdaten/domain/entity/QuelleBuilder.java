package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.anEpsgReferenz;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public final class QuelleBuilder {
    private Long id;
    private String referenzAnlagenkataster;
    private String quelleNr;
    // Eine Quelle kann eine Eltern Betriebsstätte haben
    private Long parentBetriebsstaetteId;
    // Eine Quelle kann eine Eltern Anlage habnen
    private Long parentAnlageId;
    // hat GeoPunkt 0..1
    private GeoPunkt geoPunkt;
    private String name;
    private Referenz quellenArt;
    private Double flaeche;
    private Double laenge;
    private Double breite;
    private Double durchmesser;
    private Double bauhoehe;
    private String bemerkung;
    private boolean betreiberdatenUebernommen;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private Instant letzteAenderungBetreiber;

    private QuelleBuilder() {
    }

    public QuelleBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public static QuelleBuilder aQuelle() {
        return new QuelleBuilder().name("quelle").quelleNr("quelle");
    }

    public static QuelleBuilder aQuelle(Long id) {
        return new QuelleBuilder().name("quelle")
                                  .id(id)
                                  .quelleNr(String.valueOf(id))
                                  .letzteAenderung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                                  .ersteErfassung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")));
    }

    public static QuelleBuilder aFullQuelle() {
        return aQuelle(333L).parentAnlageId(222L)
                            .parentBetriebsstaetteId(111L)
                            .bemerkung("bemerkung")
                            .quelleNr("nummer")
                            .betreiberdatenUebernommen(false)
                            .breite(1.1)
                            .bauhoehe(2.2)
                            .durchmesser(3.3)
                            .flaeche(4.4)
                            .laenge(5.5)
                            .geoPunkt(new GeoPunkt(1, 2, anEpsgReferenz("epsg").build()))
                            .referenzAnlagenkataster("kataster")
                            .name("quelle")
                            .quellenArt(ReferenzBuilder.aReferenz(Referenzliste.RQUEA, "quelle").build());
    }

    public QuelleBuilder referenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        return this;
    }

    public QuelleBuilder quelleNr(String quelleNr) {
        this.quelleNr = quelleNr;
        return this;
    }

    public QuelleBuilder parentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        return this;
    }

    public QuelleBuilder parentAnlageId(Long parentAnlageId) {
        this.parentAnlageId = parentAnlageId;
        return this;
    }

    public QuelleBuilder geoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
        return this;
    }

    public QuelleBuilder name(String name) {
        this.name = name;
        return this;
    }

    public QuelleBuilder quellenArt(Referenz quellenArt) {
        this.quellenArt = quellenArt;
        return this;
    }

    public QuelleBuilder flaeche(Double flaeche) {
        this.flaeche = flaeche;
        return this;
    }

    public QuelleBuilder laenge(Double laenge) {
        this.laenge = laenge;
        return this;
    }

    public QuelleBuilder breite(Double breite) {
        this.breite = breite;
        return this;
    }

    public QuelleBuilder durchmesser(Double durchmesser) {
        this.durchmesser = durchmesser;
        return this;
    }

    public QuelleBuilder bauhoehe(Double bauhoehe) {
        this.bauhoehe = bauhoehe;
        return this;
    }

    public QuelleBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public QuelleBuilder betreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        return this;
    }

    public QuelleBuilder letzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
        return this;
    }

    public QuelleBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public QuelleBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public Quelle build() {
        Quelle quelle = new Quelle(name, quelleNr);
        quelle.setId(id);
        quelle.setReferenzAnlagenkataster(referenzAnlagenkataster);
        quelle.setParentBetriebsstaetteId(parentBetriebsstaetteId);
        quelle.setParentAnlageId(parentAnlageId);
        quelle.setGeoPunkt(geoPunkt);
        quelle.setQuellenArt(quellenArt);
        quelle.setFlaeche(flaeche);
        quelle.setLaenge(laenge);
        quelle.setBreite(breite);
        quelle.setDurchmesser(durchmesser);
        quelle.setBauhoehe(bauhoehe);
        quelle.setBemerkung(bemerkung);
        quelle.setBetreiberdatenUebernommen(betreiberdatenUebernommen);
        quelle.setErsteErfassung(ersteErfassung);
        quelle.setLetzteAenderung(letzteAenderung);
        quelle.setLetzteAenderungBetreiber(letzteAenderungBetreiber);
        return quelle;
    }
}
