package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.euregistry.domain.UebernimmEuRegDatenInJahrCommand;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.persistence.EuRegAnlageRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsobjektService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = { EURegistryService.class, EuRegBstBerechtigungsAccessor.class })
class EURegistryServiceSecurityTest extends SecurityTest {

    @MockBean
    private EURegBetriebsstaetteRepository repository;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BerichtsobjektService berichtsobjektService;
    @MockBean
    private AnlageRepository anlageRepository;
    @MockBean
    private KomplexpruefungService komplexpruefungService;
    @MockBean
    private EuRegAnlageRepository euRegAnlageRepository;
    @MockBean
    private ParameterService parameterService;
    @MockBean
    private EuRegistryBerichtsjahrwechselMapper euRegistryBerichtsjahrwechselMapper;
    @MockBean
    private EuRegUpdater euRegUpdater;

    @Autowired
    private EURegistryService service;
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Test
    @WithBubeMockUser()
        // Testet, dass @Secured-Annotationen bei @EventHandlern beachtet werden
    void eventHandlerSecured() {
        assertThrows(AccessDeniedException.class,
                () -> eventPublisher.publishEvent(new AnlageAngelegtEvent(AnlageBuilder.anAnlage().build())));
    }

    @Test
    @WithBubeMockUser()
    void update_accessDenied_MissingRole() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        assertThrows(AccessDeniedException.class, () -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = HAMBURG)
    void update_accessDenied_wrongLand() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertThrows(AccessDeniedException.class, () -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN)
    void update_shouldBeOK_Land() {

        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertDoesNotThrow(() -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, akz = "TESTAKZ")
    void update_shouldBeOK_nullAKZ() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        eureg.getBerichtsdaten().setAkz(null);
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertDoesNotThrow(() -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, akz = "TESTAKZ")
    void update_shouldBeOK_Berichtsdaten_AKZ() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        eureg.getBerichtsdaten().setAkz("TESTAKZ");
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertDoesNotThrow(() -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, akz = "TESTAKZ")
    void update_accessDenied_wrongAKZ() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBetriebsstaette().setLand(NORDRHEIN_WESTFALEN);
        eureg.getBerichtsdaten().setAkz("TOTAL_WAS_ANDERES");
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertThrows(AccessDeniedException.class, () -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void update_accessDenied_wrongBehoerde() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBerichtsdaten().setZustaendigeBehoerde(BehoerdeBuilder.aBehoerde("TOTAL_WAS_ANDERES").build());
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertThrows(AccessDeniedException.class, () -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void update_shouldBeOK_Behoerde() {
        var eureg = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(1L).build();
        eureg.getBerichtsdaten().setZustaendigeBehoerde(BehoerdeBuilder.aBehoerde("BEHOERDE").build());
        when(repository.findOne(any(Predicate.class))).thenReturn(Optional.of(eureg));
        assertDoesNotThrow(() -> service.updateBetriebsstaettenBericht(eureg));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ, BEHOERDE_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void uebernimmEuRegDaten_accessDenied_wrongRechte() {
        List<Long> ids = Collections.singletonList(1L);
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        var command = new UebernimmEuRegDatenInJahrCommand(ids, jahr);

        assertThrows(AccessDeniedException.class, () -> service.uebernimmEuRegDaten(command));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDENBENUTZER, BEHOERDE_WRITE }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void uebernimmEuRegDaten_shouldBeOk_Behoerde() {
        List<Long> berichtIds = List.of(1L);
        List<Long> bstIds = List.of(2L);
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        when(this.repository.findAllBstIds(any())).thenReturn(bstIds);
        var command = new UebernimmEuRegDatenInJahrCommand(berichtIds, jahr);

        assertDoesNotThrow(() -> service.uebernimmEuRegDaten(command));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_WRITE }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void uebernimmEuRegDaten_shouldBeOk_Land() {
        List<Long> berichtIds = List.of(1L);
        List<Long> bstIds = List.of(2L);
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        when(this.repository.findAllBstIds(any())).thenReturn(bstIds);
        var command = new UebernimmEuRegDatenInJahrCommand(berichtIds, jahr);

        assertDoesNotThrow(() -> service.uebernimmEuRegDaten(command));
    }

}
