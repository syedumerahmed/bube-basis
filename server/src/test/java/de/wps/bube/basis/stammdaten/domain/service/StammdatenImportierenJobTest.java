package de.wps.bube.basis.stammdaten.domain.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.xml.XDtoMapper;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapper;
import de.wps.bube.basis.stammdaten.xml.StammdatenImportException;

@ExtendWith({ MockitoExtension.class })
public class StammdatenImportierenJobTest {

    private StammdatenImportierenJob job;
    @Mock
    private DateiService dateiService;
    @Mock
    private StammdatenImportExportService stammdatenImportExportService;
    @Mock
    private BenutzerJobRegistry benutzerJobRegistry;
    @Mock
    private XMLService xmlService;
    @Mock
    private BetriebsstaetteXMLMapper betriebsstaetteXMLMapper;
    @Mock
    private AktiverBenutzer aktiverBenutzer;

    @BeforeEach
    void setUp() {
        this.job = new StammdatenImportierenJob(stammdatenImportExportService, benutzerJobRegistry, aktiverBenutzer,
                xmlService, dateiService, betriebsstaetteXMLMapper);
        when(aktiverBenutzer.getUsername()).thenReturn("name");
    }

    @Test
    void importiereStammdatenServiceErrorImport() {
        JobStatus jobStatus = new JobStatus(JobStatusEnum.STAMMDATEN_IMPORTIEREN);
        jobStatus.setCompleted();

        when(benutzerJobRegistry.getJobStatus()).thenReturn(Optional.of(jobStatus));
        doThrow(new StammdatenImportException("Fehler")).when(stammdatenImportExportService)
                                                        .importiereBetriebsstaette(any(), any(), any());
        doAnswer(invocation -> {
            invocation.<Consumer<EntityWithError<Betriebsstaette>>>getArgument(5).accept(new EntityWithError<>(
                    BetriebsstaetteBuilder.aBetriebsstaette().build()));
            return null;
        }).when(xmlService).read(any(), any(), ArgumentMatchers.<XDtoMapper<?, ?>>any(), any(), any(), any());

        this.job.runAsync("2020");

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(benutzerJobRegistry).jobProtokoll(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).contains("Fehler");
    }

    @Test
    void importiereStammdatenServiceErrorXmlRead() {
        doThrow(new RuntimeException("Fehler"))
                .when(xmlService).read(any(), any(), ArgumentMatchers.<XDtoMapper<?, ?>>any(), any(), any(), any());

        this.job.runAsync("2020");

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(benutzerJobRegistry).jobProtokoll(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).contains("Fehler");
    }
}
