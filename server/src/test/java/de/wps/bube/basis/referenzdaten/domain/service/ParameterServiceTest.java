package de.wps.bube.basis.referenzdaten.domain.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.ParameterRepository;
import de.wps.bube.basis.referenzdaten.persistence.ParameterWertRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;

@ExtendWith(MockitoExtension.class)
class ParameterServiceTest {

    @Mock
    private ParameterRepository parameterRepository;

    @Mock
    private ParameterWertRepository parameterWertRepository;

    @Mock
    private AktiverBenutzer bearbeiter;

    private ParameterService parameterService;

    @BeforeEach
    void setUp() {
        parameterService = new ParameterService(parameterRepository, parameterWertRepository, bearbeiter);
    }

    @Test
    void createParameterWert_ShouldThrowException() {
        when(bearbeiter.isGesamtadmin()).thenReturn(false);
        assertThrows(ReferenzdatenException.class, () -> parameterService.createParameterWert(getParameterWert()));
    }

    @Test
    void createParameterWert_Gesamtadmin() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        parameterService.createParameterWert(getParameterWert());
    }

    @Test
    void createParameterWert_Ref_Bund_Write() {
        when(bearbeiter.isGesamtadmin()).thenReturn(false);
        lenient().when(bearbeiter.hatRolle(Rolle.REFERENZLISTEN_BUND_WRITE)).thenReturn(true);
        lenient().when(bearbeiter.hatRolle(Rolle.REFERENZLISTEN_LAND_WRITE)).thenReturn(false);
        parameterService.createParameterWert(getParameterWert());
    }

    private ParameterWert getParameterWert() {
        return ParameterWertBuilder.aParameterWert().land(Land.DEUTSCHLAND).build();
    }
}
