package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.QUELLE_ZUORDNUNG;
import static de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert.QUELLE_ZUORDNUNG_ANLAGE;
import static de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder.aParameterWert;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.persistence.QuelleRepository;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapperImpl;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = { StammdatenImportExportService.class, StammdatenImportierenJob.class, XMLService.class,
        StammdatenService.class, BetriebsstaetteXMLMapperImpl.class, BetriebsstaetteRepository.class,
        AnlagenUpdaterImpl.class, AnlageRepository.class, QuelleRepository.class,
        StammdatenBerichtsjahrwechselMapperImpl.class, ReferenzenService.class, ReferenzRepository.class,
        BetriebsstaettenBerechtigungsAccessor.class, BetreiberBerechtigungsAccessor.class,
        BetriebsstaettenUpdaterImpl.class, ReferenzResolverService.class, BehoerdenService.class,
        BehoerdeBerechtigungsAccessor.class, EigeneBehoerdenBerechtigungsAcessor.class,
        StammdatenImportierenListener.class })
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.stammdaten.persistence",
        "de.wps.bube.basis.komplexpruefung.persistence",
        "de.wps.bube.basis.referenzdaten.persistence" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.stammdaten.domain",
        "de.wps.bube.basis.komplexpruefung.domain",
        "de.wps.bube.basis.referenzdaten" })
@AutoConfigureTestEntityManager
class StammdatenImportExportServiceIntegrationTest extends BubeIntegrationTest {

    private static final String VALID_XML = "xml/stammdaten_import_test_valid.xml";

    @Autowired
    StammdatenImportExportService stammdatenImportExportService;
    @Autowired
    ReferenzenService referenzenService;
    @Autowired
    StammdatenService stammdatenService;
    @Autowired
    BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    ApplicationEventPublisher eventPublisher;
    @Autowired
    BehoerdenRepository behoerdenRepository;
    @Autowired
    TestEntityManager testEntityManager;

    @MockBean
    ParameterService parameterService;
    @MockBean
    BenutzerJobRegistry benutzerJobRegistry;
    @MockBean
    DateiService dateiService;

    final String localId = "localId";
    Referenz berichtsjahr;

    @BeforeEach
    void setUp() {
        Referenz gemeinde = new Referenz(Referenzliste.RGMD, NORDRHEIN_WESTFALEN, "050001", "", "",
                Gueltigkeitsjahr.of(2007), Gueltigkeitsjahr.of(9999));
        Behoerde behoerde = BehoerdeBuilder.aBehoerde("behoerde").land(NORDRHEIN_WESTFALEN).build();
        Behoerde behoerde2 = BehoerdeBuilder.aBehoerde("behoerde2").land(NORDRHEIN_WESTFALEN).build();
        Behoerde behoerde3 = BehoerdeBuilder.aBehoerde("behoerde3").land(NORDRHEIN_WESTFALEN).build();

        behoerde = behoerdenRepository.saveAndFlush(behoerde);
        behoerdenRepository.saveAndFlush(behoerde2);
        behoerdenRepository.saveAndFlush(behoerde3);

        berichtsjahr = referenzenService.readReferenz(Referenzliste.RJAHR, "2018", Gueltigkeitsjahr.of(2018),
                Land.DEUTSCHLAND);
        referenzenService.createReferenz(gemeinde);

        Betriebsstaette original = BetriebsstaetteBuilder.aBetriebsstaette()
                                                         .localId(localId)
                                                         .betriebsstaetteNr("nummer")
                                                         .land(NORDRHEIN_WESTFALEN)
                                                         .gemeindekennziffer(gemeinde)
                                                         .zustaendigeBehoerde(behoerde)
                                                         .betriebsstatus(berichtsjahr)
                                                         .name("name")
                                                         .berichtsjahr(berichtsjahr)
                                                         .build();

        betriebsstaetteRepository.saveAndFlush(original);

        // Commit, da der Test und die gestarteten Async-Jobs in einer anderen Transaktion laufen
        testEntityManager.getEntityManager().getTransaction().commit();
    }

    @Test
    @WithBubeMockUser(land = NORDRHEIN_WESTFALEN, roles = Rolle.ADMIN)
    void importValidXmlMitUeberschreiben() throws FileNotFoundException {
        when(dateiService.getDateiStream(any(), eq(DateiScope.STAMMDATEN))).thenReturn(getResource(VALID_XML));
        when(parameterService.readParameterWert(QUELLE_ZUORDNUNG, Gueltigkeitsjahr.of(2018),
                NORDRHEIN_WESTFALEN)).thenReturn(aParameterWert().wert(QUELLE_ZUORDNUNG_ANLAGE).build());

        JobStatus jobStatus = new JobStatus(JobStatusEnum.STAMMDATEN_IMPORTIEREN);
        jobStatus.setCompleted();
        when(benutzerJobRegistry.getJobStatus()).thenReturn(Optional.of(jobStatus));

        // ACT
        stammdatenImportExportService.triggerImport(berichtsjahr.getSchluessel());

        // ASSERT
        Betriebsstaette betriebsstaette = stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                berichtsjahr.getSchluessel(), NORDRHEIN_WESTFALEN, localId).orElse(null);

        assertThat(betriebsstaette.getBetriebsstaetteNr()).isEqualTo("nummer");
        assertThat(betriebsstaette.getAnlagen()).hasSize(2);
        assertThat(betriebsstaette.getAnlagen().stream().map(Anlage::getQuellen).flatMap(Collection::stream))
                .hasSize(1);
    }
}
