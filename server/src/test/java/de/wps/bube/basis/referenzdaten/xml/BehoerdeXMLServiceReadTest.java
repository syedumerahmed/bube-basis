package de.wps.bube.basis.referenzdaten.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import de.wps.bube.basis.referenzdaten.xml.behoerde.dto.BehoerdeType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;
import de.wps.bube.basis.referenzdaten.xml.dto.BehoerdenlisteXDto;

@SpringBootTest(classes = { BehoerdeXMLMapperImpl.class, CommonXMLMapperImpl.class, ReferenzResolverService.class })
class BehoerdeXMLServiceReadTest {

    private static final String VALID_XML = "xml/behoerde_import_test.xml";
    private static final String INVALID_XML = "xml/invalid_behoerde_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BehoerdeXMLMapper mapper;

    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private BehoerdenService behoerdenService;

    @Test
    void test_validXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(VALID_XML), BehoerdenlisteXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(0);
    }

    @Test
    void read_invalidXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_XML), BehoerdenlisteXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(2);
    }

    @Test
    void read_validXml() throws IOException {
        when(referenzenService.findReferenz(any(Referenzliste.class), anyString(), any(Gueltigkeitsjahr.class),
                any(Land.class))).thenAnswer(i -> Optional.of(
                ReferenzBuilder.aReferenz(i.getArgument(0), i.getArgument(1)).land(i.getArgument(3)).build()));

        when(referenzenService.getAktuellesBerichtsjahr()).thenReturn(ReferenzBuilder.aJahrReferenz(2021).build());

        xmlService.read(getResource(VALID_XML), BehoerdeType.class, mapper, BehoerdenlisteXDto.XML_ROOT_ELEMENT_NAME,
                null, rWithError -> {
                    var behoerde = rWithError.entity;
                    assertThat(behoerde).isNotNull();

                    assertEquals(11L, behoerde.getId());
                    assertEquals(Land.NORDRHEIN_WESTFALEN, behoerde.getLand());
                    assertEquals("123", behoerde.getKreis());
                    assertEquals("100", behoerde.getSchluessel());
                    assertEquals("bezeichnung", behoerde.getBezeichnung());
                    assertEquals("bezeichnung2", behoerde.getBezeichnung2());

                    assertEquals("ort", behoerde.getAdresse().getOrt());
                    assertEquals("plz", behoerde.getAdresse().getPlz());

                    assertEquals(2, behoerde.getKommunikationsverbindungen().size());
                    assertEquals("tel", behoerde.getKommunikationsverbindungen().get(0).getTyp().getSchluessel());
                    assertEquals("040-67 676 576", behoerde.getKommunikationsverbindungen().get(0).getVerbindung());

                    assertEquals("fax", behoerde.getKommunikationsverbindungen().get(1).getTyp().getSchluessel());
                    assertEquals("040-67 676 576-0", behoerde.getKommunikationsverbindungen().get(1).getVerbindung());

                    assertEquals(1, behoerde.getZustaendigkeiten().size());
                    assertEquals(Set.of(Zustaendigkeit.BIMSCHV11), behoerde.getZustaendigkeiten());

                    assertEquals(2007, behoerde.getGueltigVon().getJahr());
                    assertEquals(2099, behoerde.getGueltigBis().getJahr());
                    assertEquals("2020-06-30T14:00:00Z", behoerde.getLetzteAenderung().toString());

                });
    }

}
