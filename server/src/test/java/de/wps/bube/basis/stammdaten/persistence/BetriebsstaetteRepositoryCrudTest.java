package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class BetriebsstaetteRepositoryCrudTest {

    @Autowired
    private BetriebsstaetteRepository repository;
    @Autowired
    private BetreiberRepository betreiberRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Behoerde behoerde;
    private Referenz epsgCode, vorschrift, jahr, gemeindeKennziffer, betriebsstatus;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        epsgCode = referenzRepository.save(anEpsgReferenz("EPSG").build());
        vorschrift = referenzRepository.save(aVorschriftReferenz("RVORS").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
    }

    @Test
    void findById() {

        var b = getDefaultBetriebsstaetteBuilder().build();
        var bRead = repository.save(b);

        assertTrue(repository.findById(bRead.getId()).isPresent());

    }

    @Test
    void notNull() {

        var b = new Betriebsstaette();

        assertThrows(RuntimeException.class, () -> repository.saveAndFlush(b));
    }

    @Test
    void create() {

        var b = getDefaultBetriebsstaetteBuilder().build();

        assertNull(b.getId());
        var bRead = repository.save(b);
        assertNotNull(bRead.getId());
        assertEquals(b.getBerichtsjahr(), bRead.getBerichtsjahr());
        assertEquals(b.getLand(), bRead.getLand());
        assertEquals(b.getZustaendigeBehoerde(), bRead.getZustaendigeBehoerde());
        assertEquals(b.getBetriebsstaetteNr(), bRead.getBetriebsstaetteNr());
        assertEquals(b.getBetreiber(), bRead.getBetreiber());
        assertEquals(b.getErsteErfassung(), bRead.getErsteErfassung());
        assertEquals(b.getLetzteAenderung(), bRead.getLetzteAenderung());

    }

    @Test
    void delete() {

        var b = getDefaultBetriebsstaetteBuilder().build();
        var bRead = repository.save(b);

        assertTrue(repository.findById(bRead.getId()).isPresent());
        repository.delete(bRead);
        assertFalse(repository.findById(bRead.getId()).isPresent());

    }

    @Test
    void geoPoint_create() {
        var b = getDefaultBetriebsstaetteBuilder().build();
        b.setGeoPunkt(new GeoPunkt(12, 21, this.epsgCode));

        var bRead = repository.save(b);
        var saved = repository.findById(bRead.getId());

        assertThat(saved).map(Betriebsstaette::getGeoPunkt).hasValue(b.getGeoPunkt());
    }

    @Test
    void geoPoint_delete() {
        var b = getDefaultBetriebsstaetteBuilder().build();
        b.setGeoPunkt(new GeoPunkt(12, 21, this.epsgCode));

        var bRead = repository.save(b);
        assertNotNull(bRead.getGeoPunkt());

        bRead.setGeoPunkt(null);
        repository.save(bRead);

        var saved = repository.findById(bRead.getId());
        assertThat(saved).map(Betriebsstaette::getGeoPunkt).isEmpty();
    }

    @Test
    void vorschrift() {

        var b = getDefaultBetriebsstaetteBuilder().build();
        var vorschriften = new ArrayList<Referenz>();
        vorschriften.add(this.vorschrift);
        b.setVorschriften(vorschriften);

        var bRead = repository.save(b);
        assertThat(bRead.getVorschriften()).hasSize(1);
    }

    @Test
    void testAnsprechpartner() {

        var b = getDefaultBetriebsstaetteBuilder().build();
        var ansprechpartner = new ArrayList<Ansprechpartner>();
        ansprechpartner.add(new Ansprechpartner());
        ansprechpartner.add(new Ansprechpartner());
        b.setAnsprechpartner(ansprechpartner);

        var bRead = repository.save(b);
        assertThat(bRead.getAnsprechpartner()).hasSize(2);

    }

    @Test
    void testCountAllBetriebsstaetteByBetreiberIdShouldReturnTwo() {
        var betreiber = aBetreiber().jahr(jahr).build();
        betreiberRepository.save(betreiber);
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber).build());
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber).build());

        var count = repository.countAllByBetreiberId(betreiber.getId());

        assertThat(count).isEqualTo(2);
    }

    @Test
    void testCountAllBetriebsstaetteByBetreiberIdShouldReturnZero() {
        var betreiber = aBetreiber().jahr(jahr).build();
        betreiberRepository.save(betreiber);
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber).build());
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber).build());

        var count = repository.countAllByBetreiberId(2654L);

        assertThat(count).isEqualTo(0);
    }

    @Test
    void testFindBetriebsstaettenByBetreiberId() {
        var betreiber = aBetreiber().jahr(jahr).build();
        betreiberRepository.save(betreiber);
        List<Betriebsstaette> betriebsstaetten = repository.saveAll(List.of(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber).build(),
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber).build(),
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("3").build()));

        Stream<Betriebsstaette> actual = repository.findByBetreiberId(betreiber.getId());

        assertThat(actual).containsExactlyInAnyOrder(betriebsstaetten.get(0), betriebsstaetten.get(1));
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsstatus)
                                 .akz("AKZ")
                                 .zustaendigeBehoerde(this.behoerde)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(this.gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }

}
