package de.wps.bube.basis.test;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.annotation.RequestScope;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;

@TestConfiguration
public class BenutzerTestConfig {
    @Bean
    @RequestScope
    public AktiverBenutzer benutzer() {
        return (AktiverBenutzer) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Bean
    @RequestScope
    public StammdatenBerechtigungsContext berechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        return new StammdatenBerechtigungsContext(aktiverBenutzer);
    }

    @Bean
    @RequestScope
    public ReferenzenBerechtigungsContext referenzenBerechtigungsContext(AktiverBenutzer aktiverBenutzer) {
        return new ReferenzenBerechtigungsContext(aktiverBenutzer);
    }
}
