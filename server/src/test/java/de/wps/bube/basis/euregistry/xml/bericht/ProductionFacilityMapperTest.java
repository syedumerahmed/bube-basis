package de.wps.bube.basis.euregistry.xml.bericht;

import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aTaetigkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RIET;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RPRTR;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschriftMitTaetigkeiten;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_RPRTR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.from;
import static org.assertj.core.api.InstanceOfAssertFactories.STRING;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.Function;
import de.wps.bube.basis.euregistry.xml.bericht.dto.actcore.ThematicId;
import de.wps.bube.basis.euregistry.xml.bericht.dto.base.Identifier;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.Address;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.CompetentAuthorityEPRTR;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.EPRTRAnnexIActivity;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.FeatureNameContainer;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ParentCompany;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.Point;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.ReferenceType;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.InspireID;
import de.wps.bube.basis.euregistry.xml.bericht.dto.pf.Status;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

@SpringBootTest(classes = {
        ProductionFacilityMapperImpl.class,
        AddressMapperImpl.class,
        CommonEURegMapperImpl.class,
        CommonXMLMapperImpl.class })
class ProductionFacilityMapperTest {

    @MockBean
    private KoordinatenTransformationService koordinatenTransformationService;

    @Autowired
    private ProductionFacilityMapper mapper;

    @BeforeEach
    void setUp() {
        when(koordinatenTransformationService.transformToEPSG4258(any())).thenAnswer(new Answer<double[]>() {
            @Override
            public double[] answer(InvocationOnMock invocation) throws Throwable {
                GeoPunkt geoPunkt = invocation.getArgument(0);
                return new double[] {geoPunkt.getNord(), geoPunkt.getOst()};
            }
        });
    }

    @Test
    void toXDto() {
        var euRegBst = aEURegBetriebsstaette().build();
        var productionSite = new ProductionSite();
        productionSite.setGmlId("siteGmlId");

        var prtrHaupttaetigkeit = aTaetigkeitsReferenz(RPRTR, "haupttaetigkeit").build();
        var prtrNebentaetigkeit = aTaetigkeitsReferenz(RPRTR, "aNebentaetigkeit").build();
        var prtrVorschrift = aVorschriftMitTaetigkeiten()
                .art(aVorschriftReferenz(V_RPRTR.getSchluessel()).build())
                .haupttaetigkeit(prtrHaupttaetigkeit)
                .taetigkeiten(prtrHaupttaetigkeit, prtrNebentaetigkeit)
                .build();
        var otherVorschrift = aVorschriftMitTaetigkeiten()
                .taetigkeiten(aTaetigkeitsReferenz(RIET, "anotherNebentaetigkeit").build())
                .build();
        var betriebsstaette = euRegBst.getBetriebsstaette();
        betriebsstaette.getAnlagen().get(0).setVorschriften(List.of(prtrVorschrift, otherVorschrift));

        var ubaProperties = new XeurProperties.UbaProperties(
                "+49 123456",
                "+49 987654",
                "uba@wps.de",
                "UBA",
                "Umweltbundesamt",
                new XeurProperties.UbaProperties.AddressProperties(
                        "Wörlitzer Platz",
                        "1",
                        "06844",
                        "Dessau"));
        var inspireParam = new InspireParam("insp_ns", "insp_site", "insp_extension");

        var context = mock(ProductionFacilityMapper.ProductionFacilityMapperContext.class);
        when(context.getProductionSite()).thenReturn(productionSite);
        when(context.getUbaProperties()).thenReturn(ubaProperties);
        when(context.getInspireParam()).thenReturn(inspireParam);

        var productionFacility = mapper.toXDto(euRegBst, context);

        assertThat(productionFacility).isNotNull();
        assertThat(productionFacility.getFacilityName())
                .isNotNull()
                .returns(betriebsstaette.getName(),
                        from(FeatureNameContainer::getNameOfFeature))
                .extracting(FeatureNameContainer::getConfidentialityReason)
                .isNotNull()
                .returns(SchemaSet.REASON_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(betriebsstaette.getVertraulichkeitsgrund().getEu(), from(ReferenceType::getCode));

        assertThat(productionFacility.getFacilityType())
                .isNotNull()
                .returns(SchemaSet.FACILITY_TYPE_CODES, from(ReferenceType::getSchema))
                .returns(euRegBst.getBerichtsdaten().getBerichtsart().getEu(), from(ReferenceType::getCode));
        assertThat(productionFacility.getFunction())
                .isNotNull()
                .extracting(Function::getActivity)
                .returns(SchemaSet.NACE_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(betriebsstaette.getNaceCode().getEu(), from(ReferenceType::getCode));
        // TODO Test AdressMapper
        assertThat(productionFacility.getAddress())
                .isNotNull()
                .returns(betriebsstaette.getAdresse().getOrt(), from(Address::getCity));
        assertThat(productionFacility.getRemarks()).isEqualTo(betriebsstaette.getBemerkung());
        assertThat(productionFacility.getBeginLifespanVersion()).isNull();
        assertThat(productionFacility.getCompetentAuthorityEPRTR())
                .isNotNull()
                .returns(ubaProperties.getOrganisationName(), from(CompetentAuthorityEPRTR::getOrganisationName))
                .returns(ubaProperties.getIndividualName(), from(CompetentAuthorityEPRTR::getIndividualName))
                .returns(ubaProperties.getElectronicMailAddress(),
                        from(CompetentAuthorityEPRTR::getElectronicMailAddress))
                .returns(ubaProperties.getTelephoneNo(), from(CompetentAuthorityEPRTR::getTelephoneNo))
                .returns(ubaProperties.getFaxNo(), from(CompetentAuthorityEPRTR::getFaxNo))
                .extracting(CompetentAuthorityEPRTR::getAddress)
                .isNotNull()
                .returns(ubaProperties.getAddress().getStreetName(), from(Address::getStreetName))
                .returns(ubaProperties.getAddress().getBuildingNumber(), from(Address::getBuildingNumber))
                .returns(ubaProperties.getAddress().getPostalCode(), from(Address::getPostalCode))
                .returns(ubaProperties.getAddress().getCity(), from(Address::getCity));
        assertThat(productionFacility.getRiverBasinDistrict()).isEqualTo(
                betriebsstaette.getFlusseinzugsgebiet().getEu());
        assertThat(productionFacility.getDateOfStartOfOperation())
                .asString().isEqualTo(betriebsstaette.getInbetriebnahme().toString());
        assertThat(productionFacility.getGeometry())
                .isNotNull()
                .extracting(Point::getPos, STRING)
                .containsSubsequence(
                        Double.toString(betriebsstaette.getGeoPunkt().getNord()),
                        Double.toString(betriebsstaette.getGeoPunkt().getOst()));
        assertThat(productionFacility.getGmlId()).isEqualTo(productionSite.getGmlId() + betriebsstaette.getLocalId());
        assertThat(productionFacility.getHostingSite())
                .isNotNull()
                .returns("#" + productionSite.getGmlId(), from(ReferenceType::getHref));
        assertThat(productionFacility.getInspireID())
                .isNotNull()
                .extracting(InspireID::getIdentifier)
                .isNotNull()
                .returns(betriebsstaette.getLocalId(), from(Identifier::getLocalId))
                .returns(inspireParam.getNamespace(), from(Identifier::getNamespace));
        var betreiber = betriebsstaette.getBetreiber();
        assertThat(productionFacility.getParentCompany())
                .isNotNull()
                .returns(betreiber.getName(), from(ParentCompany::getParentCompanyName))
                .extracting(ParentCompany::getConfidentialityReason)
                .isNotNull()
                .returns(SchemaSet.REASON_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(betreiber.getVertraulichkeitsgrund().getEu(), from(ReferenceType::getCode));
        assertThat(productionFacility.getThematicId())
                .isNotNull()
                .returns(euRegBst.getBerichtsdaten().getThematicId(), from(ThematicId::getIdentifier))
                .returns(euRegBst.getBerichtsdaten().getSchema(), from(ThematicId::getIdentifierScheme));
        assertThat(productionFacility.getStatus())
                .isNotNull()
                .returns(betriebsstaette.getBetriebsstatusSeit().toString(), from(s -> s.getValidFrom().toString()))
                .extracting(Status::getStatusType)
                .isNotNull()
                .returns(SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(betriebsstaette.getBetriebsstatus().getEu(), from(ReferenceType::getCode));
        assertThat(productionFacility.getEprtrAnnexIActivity())
                .isNotNull()
                .extracting(EPRTRAnnexIActivity::getMainActivity)
                .isNotNull()
                .returns(SchemaSet.EPRTR_ACTIVITY_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(prtrHaupttaetigkeit.getEu(), from(ReferenceType::getCode));
        assertThat(productionFacility.getEprtrAnnexIActivity())
                .extracting(EPRTRAnnexIActivity::getOtherActivity, InstanceOfAssertFactories.list(ReferenceType.class))
                .singleElement()
                .returns(SchemaSet.EPRTR_ACTIVITY_VALUE_CODES, from(ReferenceType::getSchema))
                .returns(prtrNebentaetigkeit.getEu(), from(ReferenceType::getCode));
    }
}
