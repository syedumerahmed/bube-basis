package de.wps.bube.basis.sicherheit.domain.service;

import static de.wps.bube.basis.sicherheit.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAdapter;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;

@ExtendWith(MockitoExtension.class)
class BetreiberStammdatenBerechtigungsContextTest {

    private StammdatenBerechtigungsContext context;

    @Test
    void shouldHaveAccessToBetreiberOfLand() {
        var benutzer = aBehoerdenbenutzer().berechtigung(
                Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND)
                                                          .behoerden("BEHD1", "BEHD2")
                                                          .akz("AKZ1", "AKZ2")
                                                          .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                                                          .betriebsstaetten("BETRIEB1", "BETRIEB2")
                                                          .build()).addRollen(GESAMTADMIN, LAND_READ).build();
        context = new StammdatenBerechtigungsContext(benutzer);
        Betreiber betreiber = BetreiberBuilder.aBetreiber().land(Land.HESSEN).build();
        assertThatCode(() -> context.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber))).doesNotThrowAnyException();
    }

    @Nested
    class AsLandUserBundesland {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(
                    Datenberechtigung.DatenberechtigungBuilder.land(Land.SAARLAND)
                                                              .behoerden("BEHD1", "BEHD2")
                                                              .akz("AKZ1", "AKZ2")
                                                              .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                                                              .betriebsstaetten("BETRIEB1", "BETRIEB2")
                                                              .build())
                                               .addRollen(LAND_READ).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBetreiberOfOwnLand() {
            Betreiber betreiber = BetreiberBuilder.aBetreiber().land(Land.SAARLAND).build();
            assertThatCode(() -> context.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveAccessToBetreiberOfOtherLand() {
            Betreiber betreiber = BetreiberBuilder.aBetreiber().land(Land.HESSEN).build();
            assertThatThrownBy(() -> context.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber))).isInstanceOf(AccessDeniedException.class);
        }
    }

    @Nested
    class AsBehoerdeUserBundesland {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHN")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(BEHOERDE_WRITE).build();
            context = new StammdatenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveAccessToBetreiberOfOwnLand() {
            Betreiber betreiber = BetreiberBuilder.aBetreiber().land(Land.SAARLAND).build();
            assertThatCode(() -> context.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveAccessToBetreiberOfOtherLand() {
            Betreiber betreiber = BetreiberBuilder.aBetreiber().land(Land.HESSEN).build();
            assertThatThrownBy(() -> context.checkAccessToWrite(new BetreiberBerechtigungsAdapter(betreiber))).isInstanceOf(AccessDeniedException.class);
        }
    }

}
