package de.wps.bube.basis.euregistry.domain.entity;

import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.euregistry.domain.vo.FeuerungsanlageTyp;

public class FeuerungsanlageBerichtsdatenBuilder {

    private FeuerungsanlageTyp feuerungsanlageTyp;

    private Double gesamtKapazitaetAbfall;
    private Double kapazitaetGefAbfall;
    private Double kapazitaetNichtGefAbfall;

    private boolean mehrWaermeVonGefAbfall;
    private boolean mitverbrennungVonUnbehandeltemAbfall;

    private String oeffentlicheBekanntmachung;
    private String oeffentlicheBekanntmachungUrl;
    private final List<SpezielleBedingungIE51> spezielleBedingungen = new ArrayList<>();

    public static FeuerungsanlageBerichtsdatenBuilder aFeuerungsanlageBerichtsdaten() {
        return new FeuerungsanlageBerichtsdatenBuilder()
                .gesamtKapazitaetAbfall(6.0)
                .kapazitaetGefAbfall(2.0)
                .kapazitaetNichtGefAbfall(4.0)
                .oeffentlicheBekanntmachung("oeffentlicheBekanntmachung")
                .oeffentlicheBekanntmachungUrl("oeffentlicheBekanntmachungUrl")
                .feuerungsanlageTyp(FeuerungsanlageTyp.LCP);
    }

    public FeuerungsanlageBerichtsdatenBuilder oeffentlicheBekanntmachungUrl(String oeffentlicheBekanntmachungUrl) {
        this.oeffentlicheBekanntmachungUrl = oeffentlicheBekanntmachungUrl;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder oeffentlicheBekanntmachung(String oeffentlicheBekanntmachung) {
        this.oeffentlicheBekanntmachung = oeffentlicheBekanntmachung;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder mehrWaermeVonGefAbfall(boolean mehrWaermeVonGefAbfall) {
        this.mehrWaermeVonGefAbfall = mehrWaermeVonGefAbfall;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder mitverbrennungVonUnbehandeltemAbfall(
            boolean mitverbrennungVonUnbehandeltemAbfall) {
        this.mitverbrennungVonUnbehandeltemAbfall = mitverbrennungVonUnbehandeltemAbfall;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder gesamtKapazitaetAbfall(Double gesamtKapazitaetAbfall) {
        this.gesamtKapazitaetAbfall = gesamtKapazitaetAbfall;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder kapazitaetGefAbfall(Double kapazitaetGefAbfall) {
        this.kapazitaetGefAbfall = kapazitaetGefAbfall;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder kapazitaetNichtGefAbfall(Double kapazitaetNichtGefAbfall) {
        this.kapazitaetNichtGefAbfall = kapazitaetNichtGefAbfall;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder feuerungsanlageTyp(FeuerungsanlageTyp feuerungsanlageTyp) {
        this.feuerungsanlageTyp = feuerungsanlageTyp;
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder spezielleBedingungen(List<SpezielleBedingungIE51> bedingungen) {
        this.spezielleBedingungen.clear();
        this.spezielleBedingungen.addAll(bedingungen);
        return this;
    }

    public FeuerungsanlageBerichtsdatenBuilder spezielleBedingungen(SpezielleBedingungIE51... bedingungen) {
        return spezielleBedingungen(List.of(bedingungen));
    }

    public FeuerungsanlageBerichtsdaten build() {
        final FeuerungsanlageBerichtsdaten b = new FeuerungsanlageBerichtsdaten(gesamtKapazitaetAbfall,
                kapazitaetGefAbfall, kapazitaetNichtGefAbfall);
        b.setFeuerungsanlageTyp(feuerungsanlageTyp);
        b.setMehrWaermeVonGefAbfall(mehrWaermeVonGefAbfall);
        b.setMitverbrennungVonUnbehandeltemAbfall(mitverbrennungVonUnbehandeltemAbfall);
        b.setOeffentlicheBekanntmachung(oeffentlicheBekanntmachung);
        b.setOeffentlicheBekanntmachungUrl(oeffentlicheBekanntmachungUrl);
        b.setSpezielleBedingungen(List.copyOf(spezielleBedingungen));
        return b;
    }
}
