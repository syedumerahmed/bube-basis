package de.wps.bube.basis.referenzdaten.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.vo.ParameterTyp;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class ParameterRepositoryTest {

    @Autowired
    private ParameterRepository repository;

    @Test
    void loadListe() {
        assertTrue(repository.findAll().size() > 15);
    }

    @Test
    void loadDMY_REFKAT() {
        var parameter = repository.findById("dmy_refkat").get();

        assertEquals("dmy_refkat", parameter.getSchluessel());
        assertEquals("Dummy-Wert für die Attribute REFKAT, siehe oben, Pflichtangabe", parameter.getText());
        assertEquals(ParameterTyp.STRING, parameter.getTyp());

    }

}
