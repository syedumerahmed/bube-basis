package de.wps.bube.basis.desktop.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.desktop.domain.entity.DesktopItem;
import de.wps.bube.basis.desktop.domain.entity.DesktopItemId;
import de.wps.bube.basis.desktop.domain.vo.DesktopTyp;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class DesktopRepositoryTest {

    @Autowired
    private DesktopRepository repository;

    @Test
    public void testFindByDesktopItemIdUsernameAndBerichtsjahrId() {

        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(2L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(3L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(4L));

        assertEquals(4,
                repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "2020",
                        DesktopTyp.STAMMDATEN).size());
        assertEquals(0,
                repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("xxxxxxxx", "2020",
                        DesktopTyp.STAMMDATEN).size());
        assertEquals(0,
                repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "1970",
                        DesktopTyp.STAMMDATEN).size());

    }

    @Test
    public void testFindByDesktopTyp() {

        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1L));
        repository.save(DesktopRepositoryTest.getDesktopItemEUReg(2L));

        assertEquals(1,
                repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "2020",
                        DesktopTyp.STAMMDATEN).size());

        assertEquals(1,
                repository.findByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "2020",
                        DesktopTyp.EUREG).size());
    }

    @Test
    public void testSave() {
        final var desktopItem = DesktopRepositoryTest.getDesktopItemStammdaten(1234L);

        final var desktopItemRead = repository.save(desktopItem);

        assertNotNull(desktopItemRead.getDesktopItemId());
        assertEquals(desktopItem.getDesktopItemId(), desktopItemRead.getDesktopItemId());
        assertEquals(desktopItem.getJahrSchluessel(), desktopItemRead.getJahrSchluessel());
    }

    @Test
    public void testUpdate() {

        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1234L));
        assertEquals(1, repository.findAll().size());

        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1234L));
        assertEquals(1, repository.findAll().size());
    }

    @Test
    public void testDelete() {

        final var desktopItem = repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1234L));

        assertTrue(repository.findById(desktopItem.getDesktopItemId()).isPresent());
        repository.deleteById(desktopItem.getDesktopItemId());
        assertFalse(repository.findById(desktopItem.getDesktopItemId()).isPresent());

    }

    @Test
    public void testDeleteByStammdaten() {

        // Setup
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(2L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(3L));
        repository.save(DesktopRepositoryTest.getDesktopItemEUReg(4L));

        assertEquals(4, repository.findAll().size());

        // Act
        repository.deleteAllByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "2020",
                DesktopTyp.STAMMDATEN);

        // Assert
        assertEquals(1, repository.findAll().size());
    }

    @Test
    public void testDeleteByEUReg() {

        // Setup
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(1L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(2L));
        repository.save(DesktopRepositoryTest.getDesktopItemStammdaten(3L));
        repository.save(DesktopRepositoryTest.getDesktopItemEUReg(4L));

        assertEquals(4, repository.findAll().size());

        // Act
        repository.deleteAllByDesktopItemIdUsernameAndJahrSchluesselAndDesktopItemIdDesktopTyp("username", "2020",
                DesktopTyp.EUREG);

        // Assert
        assertEquals(3, repository.findAll().size());
    }

    private static DesktopItem getDesktopItemStammdaten(Long betriebsstaetteId) {
        return new DesktopItem(new DesktopItemId("username", betriebsstaetteId, DesktopTyp.STAMMDATEN), "2020");
    }

    private static DesktopItem getDesktopItemEUReg(Long betriebsstaetteId) {
        return new DesktopItem(new DesktopItemId("username", betriebsstaetteId, DesktopTyp.EUREG), "2020");
    }

}
