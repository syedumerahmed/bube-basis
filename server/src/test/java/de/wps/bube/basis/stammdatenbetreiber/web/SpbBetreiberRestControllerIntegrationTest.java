package de.wps.bube.basis.stammdatenbetreiber.web;

import static de.wps.bube.basis.stammdatenbetreiber.web.SpbBetreiberRestControllerIntegrationTest.BASIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verifyNoInteractions;

import java.time.Instant;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.model.SpbAdresseDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetreiberDto;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.stammdaten.domain.service.AnlagenUpdater;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettenUpdater;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenBerichtsjahrwechselMapper;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.service.SuchattributePredicateFactory;
import de.wps.bube.basis.stammdaten.domain.service.SucheService;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.SpbBetriebsstaetteUpdater;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.UpdateStammdatenDurchBetreiberAenderungService;
import de.wps.bube.basis.stammdatenbetreiber.persistence.SpbBetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = {
        BetriebsstaettenBerechtigungsAccessor.class,
        BetreiberBerechtigungsAccessor.class,
        SuchattributePredicateFactory.class,
        SucheService.class,
        StammdatenService.class,
        StammdatenBetreiberService.class,
        ReferenzMapperImpl.class,
        SpbBetreiberMapperImpl.class,
        SpbBetreiberRestController.class,
        UpdateStammdatenDurchBetreiberAenderungService.class
})
@EnableJpaRepositories(basePackages = {
        BASIS + ".referenzdaten",
        BASIS + ".komplexpruefung",
        BASIS + ".stammdaten",
        BASIS + ".stammdatenbetreiber"
}, repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        BASIS + ".referenzdaten",
        BASIS + ".komplexpruefung",
        BASIS + ".stammdaten",
        BASIS + ".stammdatenbetreiber"
})
@Sql("/sql/SpbBetreiberRestControllerIntegrationTest.sql")
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
class SpbBetreiberRestControllerIntegrationTest extends BubeIntegrationTest {

    public static final String BASIS = "de.wps.bube.basis";

    @MockBean
    private StammdatenBerichtsjahrwechselMapper stammdatenBerichtsjahrwechselMapper;
    @MockBean
    private AnlagenUpdater anlagenUpdater;
    @MockBean
    private BetriebsstaettenUpdater betriebsstaettenUpdater;
    @MockBean
    private SpbBetriebsstaetteUpdater spbBetriebsstaetteUpdater;
    @MockBean
    private KomplexpruefungService komplexpruefungService;

    @Autowired
    private SpbBetriebsstaetteRepository spbBetriebsstaetteRepository;
    @Autowired
    private SpbBetreiberRestController spbBetreiberRestController;

    @Test
    @WithBubeMockUser(roles = Rolle.BETREIBER, land = Land.HAMBURG,
            betriebsstaetten = { "101", "102", "103", "104", "105", "106", "107" })
    void readSpbBetreiberByJahrLandNummer() {

        SpbBetreiberDto dto;

        // BST ohne SpbBetriebsstaette und ohne Betreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "101").getBody();
        assertThat(dto).isNull();

        // BST mit SpbBetriebsstaette, aber ohne Betreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "102").getBody();
        assertThat(dto).isNull();

        // BST ohne SpbBetriebsstaette, mit Betreiber, aber ohne SpbBetreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "103").getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNull();
        assertThat(dto.getBetreiberId()).isEqualTo(13);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 13");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20013");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();

        // BST mit SpbBetriebsstaette und mit Betreiber, aber ohne SpbBetreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "104").getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNull();
        assertThat(dto.getBetreiberId()).isEqualTo(14);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 14");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20014");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();

        // BST ohne SpbBetriebsstaette, aber mit Betreiber und SpbBetreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "105").getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(25);
        assertThat(dto.getBetreiberId()).isEqualTo(15);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 25");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20025");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();

        // BST mit SpbBetriebsstaette, mit Betreiber und mit SpbBetreiber
        dto = spbBetreiberRestController.readSpbBetreiberByJahrLandNummer("2020", "02", "106").getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(26);
        assertThat(dto.getBetreiberId()).isEqualTo(16);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 26");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20026");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
    }

    @Test
    @WithBubeMockUser(roles = Rolle.BETREIBER, land = Land.HAMBURG,
            betriebsstaetten = { "101", "102", "103", "104", "105", "106", "107" })
    void createSpbBetreiber() {

        SpbBetreiberDto dto;
        Optional<SpbBetriebsstaette> betriebsstaette;
        IllegalArgumentException exception;

        // BST ohne SpbBetriebsstaette, mit Betreiber, aber ohne SpbBetreiber
        dto = newDto(null, 13L, 103L, "B 23", "20023");
        dto = spbBetreiberRestController.createSpbBetreiber(dto).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNotNull();
        assertThat(dto.getBetreiberId()).isEqualTo(13);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 23");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20023");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
        //    Es wurde keine SpbBetriebsstaette angelegt:
        betriebsstaette = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(103L);
        assertThat(betriebsstaette).isEmpty();

        // BST mit SpbBetriebsstaette und mit Betreiber, aber ohne SpbBetreiber
        dto = newDto(null, 14L, 104L, "B 24", "20024");
        dto = spbBetreiberRestController.createSpbBetreiber(dto).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNotNull();
        assertThat(dto.getBetreiberId()).isEqualTo(14);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 24");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20024");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
        //    Die existierende SpbBetriebsstaette wurde mit dem SpbBetreiber verknüpft:
        betriebsstaette = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(104L);
        assertThat(betriebsstaette).isPresent();
        assertThat(betriebsstaette.get().getSpbBetreiber()).isNotNull();
        assertThat(betriebsstaette.get().getSpbBetreiber().getId()).isEqualTo(dto.getId());

        // BST ohne SpbBetriebsstaette, aber mit Betreiber und SpbBetreiber
        dto = newDto(25L, 15L, 105L, "B 35", "20035");
        SpbBetreiberDto dto5 = dto;
        exception = assertThrows(IllegalArgumentException.class,
                () -> spbBetreiberRestController.createSpbBetreiber(dto5));
        assertThat(exception.getMessage()).isEqualTo("SPB Betreiber bereits vorhanden");

        // BST mit SpbBetriebsstaette, mit Betreiber und mit SpbBetreiber
        dto = newDto(26L, 16L, 106L, "B 36", "20036");
        SpbBetreiberDto dto6 = dto;
        exception = assertThrows(IllegalArgumentException.class,
                () -> spbBetreiberRestController.createSpbBetreiber(dto6));
        assertThat(exception.getMessage()).isEqualTo("SPB Betreiber bereits vorhanden");

    }

    @Test
    @WithBubeMockUser(roles = Rolle.BETREIBER, land = Land.HAMBURG,
            betriebsstaetten = { "101", "102", "103", "104", "105", "106", "107" })
    void updateSpbBetreiber() {

        SpbBetreiberDto dto;
        Optional<SpbBetriebsstaette> spbBetriebsstaette;
        IllegalArgumentException exception;

        // BST ohne SpbBetriebsstaette, mit Betreiber, aber ohne SpbBetreiber
        dto = newDto(null, 13L, 103L, "B 23", "20023");
        SpbBetreiberDto dto3 = dto;
        exception = assertThrows(IllegalArgumentException.class,
                () -> spbBetreiberRestController.updateSpbBetreiber(dto3));
        assertThat(exception.getMessage()).isEqualTo("SPB Betreiber nicht vorhanden");

        // BST mit SpbBetriebsstaette und mit Betreiber, aber ohne SpbBetreiber
        dto = newDto(null, 14L, 104L, "B 24", "20024");
        SpbBetreiberDto dto4 = dto;
        exception = assertThrows(IllegalArgumentException.class,
                () -> spbBetreiberRestController.updateSpbBetreiber(dto4));
        assertThat(exception.getMessage()).isEqualTo("SPB Betreiber nicht vorhanden");

        // BST ohne SpbBetriebsstaette, aber mit Betreiber und SpbBetreiber
        dto = newDto(25L, 15L, 105L, "B 35", "20035");
        dto = spbBetreiberRestController.updateSpbBetreiber(dto).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(25);
        assertThat(dto.getBetreiberId()).isEqualTo(15);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 35");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20035");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
        //    Es wurde keine SpbBetriebsstaette angelegt:
        spbBetriebsstaette = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(105L);
        assertThat(spbBetriebsstaette).isEmpty();

        // BST mit SpbBetriebsstaette, mit Betreiber und mit SpbBetreiber
        dto = newDto(26L, 16L, 106L, "B 36", "20036");
        dto = spbBetreiberRestController.updateSpbBetreiber(dto).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(26);
        assertThat(dto.getBetreiberId()).isEqualTo(16);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 36");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20036");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
        //    Die existierende SpbBetriebsstaette wurde mit dem SpbBetreiber verknüpft:
        spbBetriebsstaette = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(106L);
        assertThat(spbBetriebsstaette).isPresent();
        assertThat(spbBetriebsstaette.get().getSpbBetreiber()).isNotNull();
        assertThat(spbBetriebsstaette.get().getSpbBetreiber().getId()).isEqualTo(dto.getId());
    }

    @Test
    @WithBubeMockUser(roles = Rolle.BETREIBER, land = Land.HAMBURG,
            betriebsstaetten = { "101", "102", "103", "104", "105", "106", "107" })
    void deleteSpbBetreiber() {

        SpbBetreiberDto dto;
        Optional<SpbBetriebsstaette> spbBetriebsstaette;

        // BST ohne SpbBetriebsstaette, aber mit Betreiber und SpbBetreiber
        dto = spbBetreiberRestController.deleteSpbBetreiber(25L).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNull();
        assertThat(dto.getBetreiberId()).isEqualTo(15);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 15");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20015");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();

        // BST mit SpbBetriebsstaette, mit Betreiber und mit SpbBetreiber
        dto = spbBetreiberRestController.deleteSpbBetreiber(26L).getBody();
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNull();
        assertThat(dto.getBetreiberId()).isEqualTo(16);
        assertThat(dto.getBetriebsstaetteId()).isNull();
        assertThat(dto.getName()).isEqualTo("B 16");
        assertThat(dto.getAdresse()).isNotNull();
        assertThat(dto.getAdresse().getOrt()).isEqualTo("Hamburg");
        assertThat(dto.getAdresse().getPlz()).isEqualTo("20016");
        assertThat(dto.getErsteErfassung()).isNotNull();
        assertThat(dto.getLetzteAenderung()).isNotNull();
        //    Die existierende SpbBetriebsstaette wurde aktualisiert:
        spbBetriebsstaette = spbBetriebsstaetteRepository.findOneByBetriebsstaetteId(106L);
        assertThat(spbBetriebsstaette).isPresent();
        assertThat(spbBetriebsstaette.get().getSpbBetreiber()).isNull();
    }

    @AfterEach
    void tearDown() {
        verifyNoInteractions(stammdatenBerichtsjahrwechselMapper);
        verifyNoInteractions(anlagenUpdater);
        verifyNoInteractions(betriebsstaettenUpdater);
        verifyNoInteractions(spbBetriebsstaetteUpdater);
    }

    private SpbBetreiberDto newDto(Long id, Long betreiberId, Long betriebsstaetteId, String name, String plz) {
        SpbBetreiberDto dto = new SpbBetreiberDto();
        dto.setAdresse(new SpbAdresseDto());
        dto.setId(id);
        dto.setBetreiberId(betreiberId);
        dto.setBetriebsstaetteId(betriebsstaetteId);
        dto.setName(name);
        dto.getAdresse().setOrt("Hamburg");
        dto.getAdresse().setPlz(plz);
        dto.setErsteErfassung(Instant.now().toString());
        dto.setLetzteAenderung(Instant.now().toString());
        return dto;
    }

}
