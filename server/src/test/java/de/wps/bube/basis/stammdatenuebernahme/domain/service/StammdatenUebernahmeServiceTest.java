package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.anEpsgReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.AnlageDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.AnlagenteilDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.QuelleDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.AnsprechpartnerBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenbetreiber.domain.service.StammdatenBetreiberService;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AdresseUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlageUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.AnlagenteilUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetreiberUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.BetriebsstaetteUebernahme;
import de.wps.bube.basis.stammdatenuebernahme.domain.vo.QuelleUebernahme;

@ExtendWith(MockitoExtension.class)
class StammdatenUebernahmeServiceTest {

    public static final Adresse ADRESSE = AdresseBuilder.aFullAdresse().build();
    public static final Betriebsstaette BETRIEBSSTAETTE = BetriebsstaetteBuilder.aFullBetriebsstaette()
                                                                                .adresse(ADRESSE)
                                                                                .build();
    public static final Anlage ANLAGE = AnlageBuilder.aFullAnlage()
                                                     .parentBetriebsstaetteId(BETRIEBSSTAETTE.getId())
                                                     .build();
    public static final Anlagenteil ANLAGENTEIL = AnlagenteilBuilder.aFullAnlagenteil()
                                                                    .parentAnlageId(ANLAGE.getId())
                                                                    .build();
    public static final Quelle QUELLE = QuelleBuilder.aFullQuelle().build();
    public static final Betreiber BETREIBER = BetreiberBuilder.aFullBetreiber().adresse(ADRESSE).build();

    private StammdatenUebernahmeService service;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private StammdatenBetreiberService stammdatenBetreiberService;

    @BeforeEach
    void setUp() {
        service = new StammdatenUebernahmeService(stammdatenService, stammdatenBetreiberService);
    }

    @Test
    void uebernehmeNeueAnlage() {
        var neueAnlage = anAnlage().anlageNr("0100").uebernommen(false).letzteAenderungBetreiber(null).build();
        var spbAnlage = new SpbAnlage();
        spbAnlage.setId(4711L);

        when(stammdatenService.createAnlage(any(Anlage.class))).thenAnswer(i -> i.getArgument(0));
        when(stammdatenBetreiberService.findSpbAnlage(neueAnlage.getParentBetriebsstaetteId(),
                neueAnlage.getAnlageNr())).thenReturn(Optional.of(spbAnlage));

        var uebernommeneAnlage = service.uebernehmeNeueAnlage(spbAnlage.getId(), neueAnlage);

        verify(stammdatenService).createAnlage(neueAnlage);
        verify(stammdatenBetreiberService).aktualisiereSpbNachAnlageUebernahme(null, spbAnlage.getId());

        assertThat(uebernommeneAnlage.isBetreiberdatenUebernommen()).isTrue();
        assertThat(uebernommeneAnlage.getLetzteAenderungBetreiber()).isNull();
    }

    @Test
    void uebernehmeNeueAnlage_modifiedAnlageNr() {
        var neueAnlage = anAnlage().anlageNr("0100").uebernommen(false).letzteAenderungBetreiber(null).build();
        var spbAnlage = new SpbAnlage();
        spbAnlage.setId(4711L);
        spbAnlage.setAnlageNr("0200");

        when(stammdatenService.createAnlage(any(Anlage.class))).thenAnswer(i -> i.getArgument(0));
        when(stammdatenBetreiberService.findSpbAnlage(neueAnlage.getParentBetriebsstaetteId(),
                neueAnlage.getAnlageNr())).thenReturn(Optional.empty());

        service.uebernehmeNeueAnlage(spbAnlage.getId(), neueAnlage);

        verify(stammdatenService).createAnlage(neueAnlage);
        verify(stammdatenBetreiberService).aktualisiereSpbNachAnlageUebernahme(null, spbAnlage.getId());
    }

    @Test
    void uebernehmeNeueAnlage_modifiedAnlageNr_duplicate() {
        var neueAnlage = anAnlage().anlageNr("0100").uebernommen(false).letzteAenderungBetreiber(null).build();
        var spbAnlage = new SpbAnlage();
        spbAnlage.setId(4711L);
        spbAnlage.setAnlageNr("0200");
        var spbAnlageWithSameNummer = new SpbAnlage();
        spbAnlageWithSameNummer.setId(4712L);
        spbAnlageWithSameNummer.setAnlageNr("0100");

        when(stammdatenBetreiberService.findSpbAnlage(neueAnlage.getParentBetriebsstaetteId(),
                neueAnlage.getAnlageNr())).thenReturn(Optional.of(spbAnlageWithSameNummer));

        assertThrows(AnlageDuplicateKeyException.class,
                () -> service.uebernehmeNeueAnlage(spbAnlage.getId(), neueAnlage));

        verifyNoMoreInteractions(stammdatenService);
        verifyNoMoreInteractions(stammdatenBetreiberService);
    }

    @Test
    void uebernehmeAnlage() {
        Anlage andereAnlage = AnlageBuilder.anAnlage(12L)
                                           .betriebsstatusSeit(LocalDate.now())
                                           .inbetriebnahme(LocalDate.now())
                                           .anlageNr("test")
                                           .name("test")
                                           .betriebsstatus(
                                                   ReferenzBuilder.aBetriebsstatusReferenz("test").id(546L).build())
                                           .geoPunkt(new GeoPunkt(15, 15,
                                                   ReferenzBuilder.anEpsgReferenz("test").id(456L).build()))
                                           .leistungen(Collections.emptyList())
                                           .build();

        SpbAnlage spbAnlage = new SpbAnlage(andereAnlage);
        spbAnlage.setId(564L);

        AnlageUebernahme uebernahme = new AnlageUebernahme(ANLAGE.getId(), spbAnlage.getId(), true, true, true, true,
                true, true, true);

        when(stammdatenBetreiberService.loadSpbAnlage(spbAnlage.getId())).thenReturn(spbAnlage);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);

        when(stammdatenService.updateAnlageForUebernahme(any())).thenAnswer(i -> i.getArgument(0));

        Anlage anlage = service.uebernehmeAnlage(uebernahme);

        assertThat(anlage.isBetreiberdatenUebernommen()).isTrue();
        assertThat(anlage.getName()).isEqualTo(spbAnlage.getName());
        assertThat(anlage.getAnlageNr()).isEqualTo(spbAnlage.getAnlageNr());
        assertThat(anlage.getGeoPunkt()).isEqualTo(spbAnlage.getGeoPunkt());
        assertThat(anlage.getBetriebsstatus()).isEqualTo(spbAnlage.getBetriebsstatus());
        assertThat(anlage.getBetriebsstatusSeit()).isEqualTo(spbAnlage.getBetriebsstatusSeit());
        assertThat(anlage.getInbetriebnahme()).isEqualTo(spbAnlage.getInbetriebnahme());
        assertThat(anlage.getLeistungen()).containsExactlyInAnyOrderElementsOf(spbAnlage.getLeistungen());

        verify(stammdatenBetreiberService).deleteUebernommeneSpbAnlage(any());
    }

    @Test
    void uebernehmeNeuesAnlagenteil() {
        var bstId = 555L;
        var parentAnlage = anAnlage(222L).parentBetriebsstaetteId(bstId).build();
        var neuesAnlagenteil = anAnlagenteil().parentAnlageId(parentAnlage.getId())
                                              .anlagenteilNr("0100")
                                              .uebernommen(false)
                                              .letzteAenderungBetreiber(null)
                                              .build();
        var spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setId(4711L);

        when(stammdatenBetreiberService.findSpbAnlagenteil(parentAnlage.getId(), neuesAnlagenteil.getAnlagenteilNr()))
                .thenReturn(Optional.of(spbAnlagenteil));
        when(stammdatenService.loadAnlage(parentAnlage.getId())).thenReturn(parentAnlage);
        when(stammdatenService.createAnlagenteil(eq(bstId), any(Anlagenteil.class))).thenAnswer(i -> i.getArgument(1));

        var uebernommenesAnlagenteil = service.uebernehmeNeuesAnlagenteil(spbAnlagenteil.getId(), neuesAnlagenteil);

        verify(stammdatenBetreiberService).aktualisiereSpbNachAnlagenteilUebernahme(spbAnlagenteil.getId());

        assertThat(uebernommenesAnlagenteil.isBetreiberdatenUebernommen()).isTrue();
        assertThat(uebernommenesAnlagenteil.getLetzteAenderungBetreiber()).isNull();
    }

    @Test
    void uebernehmeNeuesAnlagenteil_modifiedAnlagenteilNr() {
        var bstId = 555L;
        var parentAnlage = anAnlage(222L).parentBetriebsstaetteId(bstId).build();
        var neuesAnlagenteil = anAnlagenteil().parentAnlageId(parentAnlage.getId())
                                              .anlagenteilNr("0100")
                                              .uebernommen(false)
                                              .letzteAenderungBetreiber(null)
                                              .build();
        var spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setId(4711L);
        spbAnlagenteil.setAnlagenteilNr("0200");

        when(stammdatenBetreiberService.findSpbAnlagenteil(parentAnlage.getId(), neuesAnlagenteil.getAnlagenteilNr()))
                .thenReturn(Optional.empty());
        when(stammdatenService.loadAnlage(parentAnlage.getId())).thenReturn(parentAnlage);
        when(stammdatenService.createAnlagenteil(eq(bstId), any(Anlagenteil.class))).thenAnswer(i -> i.getArgument(1));

        service.uebernehmeNeuesAnlagenteil(spbAnlagenteil.getId(), neuesAnlagenteil);

        verify(stammdatenBetreiberService).aktualisiereSpbNachAnlagenteilUebernahme(spbAnlagenteil.getId());
    }

    @Test
    void uebernehmeNeuesAnlagenteil_modifiedAnlagenteilNr_duplicate() {
        var bstId = 555L;
        var parentAnlage = anAnlage(222L).parentBetriebsstaetteId(bstId).build();
        var neuesAnlagenteil = anAnlagenteil().parentAnlageId(parentAnlage.getId())
                                              .anlagenteilNr("0100")
                                              .uebernommen(false)
                                              .letzteAenderungBetreiber(null)
                                              .build();
        var spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setId(4711L);
        spbAnlagenteil.setAnlagenteilNr("0200");
        var spbAnlagenteilWithSameNummer = new SpbAnlagenteil();
        spbAnlagenteilWithSameNummer.setId(4712L);
        spbAnlagenteilWithSameNummer.setAnlagenteilNr("0100");

        when(stammdatenBetreiberService.findSpbAnlagenteil(parentAnlage.getId(), neuesAnlagenteil.getAnlagenteilNr()))
                .thenReturn(Optional.of(spbAnlagenteilWithSameNummer));

        assertThrows(AnlagenteilDuplicateKeyException.class,
                () -> service.uebernehmeNeuesAnlagenteil(spbAnlagenteil.getId(), neuesAnlagenteil));

        verifyNoMoreInteractions(stammdatenService);
        verifyNoMoreInteractions(stammdatenBetreiberService);
    }

    @Test
    void uebernehmeAnlagenteil() {
        Anlagenteil andererAnlagenteil = anAnlagenteil()
                .id(654L)
                .anlagenteilNr("test")
                .name("test")
                .betriebsstatus(
                        ReferenzBuilder.aBetriebsstatusReferenz("test")
                                       .id(564L)
                                       .build())
                .betriebsstatusSeit(LocalDate.MIN)
                .inbetriebnahme(LocalDate.MIN)
                .geoPunkt(new GeoPunkt(0, 1,
                        ReferenzBuilder.anEpsgReferenz("test")
                                       .id(15654L)
                                       .build()))
                .leistungen(new Leistung(10D,
                        ReferenzBuilder.aLeistungseinheitReferenz("test")
                                       .id(4561L)
                                       .build(), "test",
                        Leistungsklasse.BETRIEBEN))
                .build();

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(andererAnlagenteil);
        spbAnlagenteil.setId(15L);

        AnlagenteilUebernahme uebernahme = new AnlagenteilUebernahme(ANLAGENTEIL.getId(), spbAnlagenteil.getId(), true,
                true, true, true, true, true, true);

        when(stammdatenBetreiberService.loadSpbAnlagenteilById(spbAnlagenteil.getId())).thenReturn(
                spbAnlagenteil);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);
        when(stammdatenService.loadAnlagenteil(ANLAGENTEIL.getId())).thenReturn(ANLAGENTEIL);

        when(stammdatenService.updateAnlagenteil(any(), any())).thenAnswer(i -> i.getArgument(1));

        Anlagenteil anlagenteil = service.uebernehmeAnlagenteil(uebernahme);

        assertThat(anlagenteil.isBetreiberdatenUebernommen()).isTrue();
        assertThat(anlagenteil.getName()).isEqualTo(spbAnlagenteil.getName());
        assertThat(anlagenteil.getAnlagenteilNr()).isEqualTo(spbAnlagenteil.getAnlagenteilNr());
        assertThat(anlagenteil.getGeoPunkt()).isEqualTo(spbAnlagenteil.getGeoPunkt());
        assertThat(anlagenteil.getBetriebsstatus()).isEqualTo(spbAnlagenteil.getBetriebsstatus());
        assertThat(anlagenteil.getBetriebsstatusSeit()).isEqualTo(spbAnlagenteil.getBetriebsstatusSeit());
        assertThat(anlagenteil.getInbetriebnahme()).isEqualTo(spbAnlagenteil.getInbetriebnahme());
        assertThat(anlagenteil.getLeistungen()).containsExactlyInAnyOrderElementsOf(spbAnlagenteil.getLeistungen());

        verify(stammdatenBetreiberService).deleteSpbAnlagenteil(any(), any());
    }

    @Test
    void uebernehmeBetreiber() {
        Betreiber betreiberAnders = BetreiberBuilder.aBetreiber()
                                                    .adresse(AdresseBuilder.anAdresse()
                                                                           .strasse("andereStrasse")
                                                                           .plz("15")
                                                                           .hausNr("15")
                                                                           .ort("15")
                                                                           .ortsteil("15")
                                                                           .postfach("15")
                                                                           .postfachPlz("15")
                                                                           .landIsoCode(ReferenzBuilder.aReferenz(
                                                                                                               Referenzliste.RJAHR, "a")
                                                                                                       .id(6541L)
                                                                                                       .build())
                                                                           .build()
                                                    )
                                                    .bemerkung("test")
                                                    .betreiberNummer("798")
                                                    .ersteErfassung(Instant.now())
                                                    .jahr(ReferenzBuilder.aJahrReferenz(16).build())
                                                    .land(Land.NORDRHEIN_WESTFALEN)
                                                    .name("test").build();

        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiberAnders);
        spbBetreiber.setId(16L);

        AdresseUebernahme adresse = new AdresseUebernahme(true,
                true, true, true, true, true, true, true);

        BetreiberUebernahme uebernahme = new BetreiberUebernahme(BETREIBER.getId(), spbBetreiber.getId(), adresse,
                true);

        when(stammdatenService.loadBetreiber(BETREIBER.getId())).thenReturn(BETREIBER);
        when(stammdatenBetreiberService.loadSpbBetreiber(spbBetreiber.getId())).thenReturn(spbBetreiber);

        when(stammdatenService.updateBetreiber(any())).thenAnswer(i -> i.getArgument(0));

        Betreiber betreiber = service.uebernehmeBetreiber(uebernahme);

        assertThat(betreiber.isBetreiberdatenUebernommen()).isTrue();
        assertThat(betreiber.getName()).isEqualTo(spbBetreiber.getName());
        assertThat(betreiber.getAdresse().getStrasse()).isEqualTo(spbBetreiber.getAdresse().getStrasse());
        assertThat(betreiber.getAdresse().getHausNr()).isEqualTo(spbBetreiber.getAdresse().getHausNr());
        assertThat(betreiber.getAdresse().getOrt()).isEqualTo(spbBetreiber.getAdresse().getOrt());
        assertThat(betreiber.getAdresse().getOrtsteil()).isEqualTo(spbBetreiber.getAdresse().getOrtsteil());
        assertThat(betreiber.getAdresse().getPlz()).isEqualTo(spbBetreiber.getAdresse().getPlz());
        assertThat(betreiber.getAdresse().getPostfach()).isEqualTo(spbBetreiber.getAdresse().getPostfach());
        assertThat(betreiber.getAdresse().getPostfachPlz()).isEqualTo(spbBetreiber.getAdresse().getPostfachPlz());
        assertThat(betreiber.getAdresse().getLandIsoCode()).isEqualTo(spbBetreiber.getAdresse().getLandIsoCode());

        verify(stammdatenBetreiberService).deleteSpbBetreiber(any());
    }

    @Test
    void uebernehmeBetriebsstaette() {
        Adresse adresse = AdresseBuilder.anAdresse()
                                        .strasse("test")
                                        .ort("test")
                                        .landIsoCode(ReferenzBuilder.aBetriebsstatusReferenz("test").id(564L).build())
                                        .ortsteil("test")
                                        .hausNr("test").postfach("test").postfachPlz("test").plz("test")
                                        .build();
        Betreiber betreiber = BetreiberBuilder.aBetreiber().adresse(adresse).name("test").build();
        Kommunikationsverbindung andereKommunikationsverbindung = new Kommunikationsverbindung(
                ReferenzBuilder.aKommunikationsTypReferenz("tel").id(159L).build(), "123456");
        Ansprechpartner ansprechpartner = AnsprechpartnerBuilder.anAnsprechpartner()
                                                                .nachname("test")
                                                                .vorname("test")
                                                                .funktion(
                                                                        ReferenzBuilder.aBetriebsstatusReferenz("test")
                                                                                       .id(546L)
                                                                                       .build())
                                                                .kommunikationsverbindungen(
                                                                        new Kommunikationsverbindung(
                                                                                ReferenzBuilder.aKommunikationsTypReferenz(
                                                                                        "test").id(21453L).build(),
                                                                                "test"))
                                                                .build();

        Betriebsstaette andereBetriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette()
                                                                      .betriebsstaetteNr("test")
                                                                      .name("test")
                                                                      .betriebsstatus(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "test").id(415L).build())
                                                                      .abfallerzeugerNummern(
                                                                              List.of(new AbfallerzeugerNr("15"),
                                                                                      new AbfallerzeugerNr("654")))
                                                                      .adresse(adresse)
                                                                      .betreiber(betreiber)
                                                                      .ansprechpartner(ansprechpartner)
                                                                      .betriebsstatusSeit(LocalDate.now())
                                                                      .inbetriebnahme(LocalDate.now())
                                                                      .istIndirekteinleiter(true)
                                                                      .istDirekteinleiter(false)
                                                                      .kommunikationsverbindungen(
                                                                              andereKommunikationsverbindung)
                                                                      .flusseinzugsgebiet(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "2020").id(15L).build())
                                                                      .build();

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(
                andereBetriebsstaette);
        spbBetriebsstaette.setId(879312879L);

        when(stammdatenService.loadBetriebsstaetteForWrite(BETRIEBSSTAETTE.getId())).thenReturn(BETRIEBSSTAETTE);
        when(stammdatenBetreiberService.loadSpbBetriebsstaette(spbBetriebsstaette.getId())).thenReturn(
                spbBetriebsstaette);

        when(stammdatenService.updateBetriebsstaetteForUebernahme(any())).thenAnswer(i -> i.getArgument(0));

        AdresseUebernahme adresseUebernahme = new AdresseUebernahme(true,
                true, true, true, true, true, true, true);

        BetriebsstaetteUebernahme uebernahme = new BetriebsstaetteUebernahme(BETRIEBSSTAETTE.getId(),
                spbBetriebsstaette.getId(), adresseUebernahme,
                true, true, true, true, true, true, true, true, true, true,
                true, true, true);

        Betriebsstaette betriebsstaette = service.uebernehmeBetriebsstaette(uebernahme);

        assertThat(betriebsstaette.isBetreiberdatenUebernommen()).isTrue();
        assertThat(betriebsstaette.getName()).isEqualTo(spbBetriebsstaette.getName());
        assertThat(betriebsstaette.getAdresse().getStrasse()).isEqualTo(spbBetriebsstaette.getAdresse().getStrasse());
        assertThat(betriebsstaette.getAdresse().getHausNr()).isEqualTo(spbBetriebsstaette.getAdresse().getHausNr());
        assertThat(betriebsstaette.getAdresse().getOrt()).isEqualTo(spbBetriebsstaette.getAdresse().getOrt());
        assertThat(betriebsstaette.getAdresse().getOrtsteil()).isEqualTo(spbBetriebsstaette.getAdresse().getOrtsteil());
        assertThat(betriebsstaette.getAdresse().getPlz()).isEqualTo(spbBetriebsstaette.getAdresse().getPlz());
        assertThat(betriebsstaette.getAdresse().getPostfach()).isEqualTo(spbBetriebsstaette.getAdresse().getPostfach());
        assertThat(betriebsstaette.getAdresse().getPostfachPlz()).isEqualTo(
                spbBetriebsstaette.getAdresse().getPostfachPlz());
        assertThat(betriebsstaette.getAdresse().getLandIsoCode()).isEqualTo(
                spbBetriebsstaette.getAdresse().getLandIsoCode());

        assertThat(betriebsstaette.getEinleiterNummern()).containsExactlyInAnyOrderElementsOf(
                spbBetriebsstaette.getEinleiterNummern());
        assertThat(betriebsstaette.getAbfallerzeugerNummern()).containsExactlyInAnyOrderElementsOf(
                spbBetriebsstaette.getAbfallerzeugerNummern());
        assertThat(betriebsstaette.getAnsprechpartner()).containsExactlyInAnyOrderElementsOf(
                spbBetriebsstaette.getAnsprechpartner());
        assertThat(betriebsstaette.getKommunikationsverbindungen()).containsExactlyInAnyOrderElementsOf(
                spbBetriebsstaette.getKommunikationsverbindungen());

        assertThat(betriebsstaette.getFlusseinzugsgebiet()).isEqualTo(spbBetriebsstaette.getFlusseinzugsgebiet());
        assertThat(betriebsstaette.getNaceCode()).isEqualTo(spbBetriebsstaette.getNaceCode());
        assertThat(betriebsstaette.isDirekteinleiter()).isEqualTo(spbBetriebsstaette.isDirekteinleiter());
        assertThat(betriebsstaette.isIndirekteinleiter()).isEqualTo(spbBetriebsstaette.isIndirekteinleiter());
        assertThat(betriebsstaette.getGeoPunkt()).isEqualTo(spbBetriebsstaette.getGeoPunkt());
        assertThat(betriebsstaette.getBetriebsstatus()).isEqualTo(spbBetriebsstaette.getBetriebsstatus());
        assertThat(betriebsstaette.getBetriebsstatusSeit()).isEqualTo(spbBetriebsstaette.getBetriebsstatusSeit());
        assertThat(betriebsstaette.getInbetriebnahme()).isEqualTo(spbBetriebsstaette.getInbetriebnahme());

        verify(stammdatenBetreiberService).deleteSpbBetriebsstaetteUebernahme(any());
    }

    @Nested
    class UebernehmeNeueQuelleAnBst {
        Quelle neueQuelle;
        SpbQuelle spbQuelle;
        final long bstId = 15L;

        @BeforeEach
        void setUp() {
            neueQuelle = aQuelle().quelleNr("0100")
                                  .parentBetriebsstaetteId(bstId)
                                  .betreiberdatenUebernommen(false)
                                  .letzteAenderungBetreiber(null)
                                  .build();
            spbQuelle = new SpbQuelle();
            spbQuelle.setId(4711L);
        }

        @Test
        void uebernehmeNeueQuelle() {
            when(stammdatenBetreiberService.findSpbQuelleOfBetriebsstaette(bstId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.of(spbQuelle));
            when(stammdatenService.createQuelle(any(Quelle.class))).thenAnswer(i -> i.getArgument(0));

            var uebernommeneQuelle = service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle);

            verify(stammdatenService).createQuelle(neueQuelle);
            verify(stammdatenBetreiberService).aktualisiereSpbNachQuelleUebernahme(spbQuelle.getId());

            assertThat(uebernommeneQuelle.isBetreiberdatenUebernommen()).isTrue();
            assertThat(uebernommeneQuelle.getLetzteAenderungBetreiber()).isNull();
        }

        @Test
        void uebernehmeNeueQuelle_modifiedQuelleNr() {
            spbQuelle.setQuelleNr("0200");

            when(stammdatenBetreiberService.findSpbQuelleOfBetriebsstaette(bstId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.empty());
            when(stammdatenService.createQuelle(any(Quelle.class))).thenAnswer(i -> i.getArgument(0));

            service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle);

            verify(stammdatenService).createQuelle(neueQuelle);
            verify(stammdatenBetreiberService).aktualisiereSpbNachQuelleUebernahme(spbQuelle.getId());
        }

        @Test
        void uebernehmeNeueQuelle_modifiedQuelleNr_duplicate() {
            spbQuelle.setQuelleNr("0200");
            var spbQuelleWithSameNummer = new SpbQuelle();
            spbQuelleWithSameNummer.setId(4712L);
            spbQuelleWithSameNummer.setQuelleNr("0100");

            when(stammdatenBetreiberService.findSpbQuelleOfBetriebsstaette(bstId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.of(spbQuelleWithSameNummer));

            assertThrows(QuelleDuplicateKeyException.class,
                    () -> service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle));

            verifyNoMoreInteractions(stammdatenService);
            verifyNoMoreInteractions(stammdatenBetreiberService);
        }
    }

    @Nested
    class UebernehmeNeueQuelleAnAnlage {
        Quelle neueQuelle;
        SpbQuelle spbQuelle;
        final long anlageId = 15L;

        @BeforeEach
        void setUp() {
            neueQuelle = aQuelle().quelleNr("0100")
                                  .parentAnlageId(anlageId)
                                  .betreiberdatenUebernommen(false)
                                  .letzteAenderungBetreiber(null)
                                  .build();
            spbQuelle = new SpbQuelle();
            spbQuelle.setId(4711L);
        }

        @Test
        void uebernehmeNeueQuelle() {
            when(stammdatenBetreiberService.findSpbQuelleOfAnlage(anlageId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.of(spbQuelle));
            when(stammdatenService.createQuelle(any(Quelle.class))).thenAnswer(i -> i.getArgument(0));

            var uebernommeneQuelle = service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle);

            verify(stammdatenService).createQuelle(neueQuelle);
            verify(stammdatenBetreiberService).aktualisiereSpbNachQuelleUebernahme(spbQuelle.getId());

            assertThat(uebernommeneQuelle.isBetreiberdatenUebernommen()).isTrue();
            assertThat(uebernommeneQuelle.getLetzteAenderungBetreiber()).isNull();
        }

        @Test
        void uebernehmeNeueQuelle_modifiedQuelleNr() {
            spbQuelle.setQuelleNr("0200");

            when(stammdatenBetreiberService.findSpbQuelleOfAnlage(anlageId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.empty());
            when(stammdatenService.createQuelle(any(Quelle.class))).thenAnswer(i -> i.getArgument(0));

            service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle);

            verify(stammdatenService).createQuelle(neueQuelle);
            verify(stammdatenBetreiberService).aktualisiereSpbNachQuelleUebernahme(spbQuelle.getId());
        }

        @Test
        void uebernehmeNeueQuelle_modifiedQuelleNr_duplicate() {
            spbQuelle.setQuelleNr("0200");
            var spbQuelleWithSameNummer = new SpbQuelle();
            spbQuelleWithSameNummer.setId(4712L);
            spbQuelleWithSameNummer.setQuelleNr("0100");

            when(stammdatenBetreiberService.findSpbQuelleOfAnlage(anlageId, neueQuelle.getQuelleNr()))
                    .thenReturn(Optional.of(spbQuelleWithSameNummer));

            assertThrows(QuelleDuplicateKeyException.class,
                    () -> service.uebernehmeNeueQuelle(spbQuelle.getId(), neueQuelle));

            verifyNoMoreInteractions(stammdatenService);
            verifyNoMoreInteractions(stammdatenBetreiberService);
        }
    }

    @Test
    void uebernehmeQuelle() {
        Quelle quelleAnders = aQuelle().id(33L)
                                       .parentAnlageId(222L)
                                       .parentBetriebsstaetteId(111L)
                                       .bemerkung("bemerkung2")
                                       .quelleNr("nummer2")
                                       .betreiberdatenUebernommen(false)
                                       .breite(12.12)
                                       .bauhoehe(22.22)
                                       .durchmesser(23.32)
                                       .flaeche(42.42)
                                       .laenge(52.52)
                                       .geoPunkt(new GeoPunkt(21, 22, anEpsgReferenz("2epsg").build()))
                                       .referenzAnlagenkataster("kataster2")
                                       .name("quelle2")
                                       .quellenArt(ReferenzBuilder.aReferenz(Referenzliste.RQUEA, "quelle2")
                                                                  .id(15987L)
                                                                  .build())
                                       .build();

        SpbQuelle spbQuelle = new SpbQuelle(quelleAnders);
        spbQuelle.setId(10L);

        QuelleUebernahme uebernahme = new QuelleUebernahme(QUELLE.getId(), spbQuelle.getId(), true, true, true, true,
                true, true, true, true);

        when(stammdatenService.loadQuelle(QUELLE.getId())).thenReturn(QUELLE);
        when(stammdatenBetreiberService.loadSpbQuelle(spbQuelle.getId())).thenReturn(spbQuelle);

        when(stammdatenService.updateQuelle(any())).thenAnswer(i -> i.getArgument(0));

        Quelle quelle = service.uebernehmeQuelle(uebernahme);

        assertThat(quelle.isBetreiberdatenUebernommen()).isTrue();
        assertThat(quelle.getName()).isEqualTo(spbQuelle.getName());
        assertThat(quelle.getQuelleNr()).isEqualTo(spbQuelle.getQuelleNr());
        assertThat(quelle.getGeoPunkt()).isEqualTo(spbQuelle.getGeoPunkt());
        assertThat(quelle.getFlaeche()).isEqualTo(spbQuelle.getFlaeche());
        assertThat(quelle.getLaenge()).isEqualTo(spbQuelle.getLaenge());
        assertThat(quelle.getBreite()).isEqualTo(spbQuelle.getBreite());
        assertThat(quelle.getDurchmesser()).isEqualTo(spbQuelle.getDurchmesser());
        assertThat(quelle.getBauhoehe()).isEqualTo(spbQuelle.getBauhoehe());

        verify(stammdatenBetreiberService).deleteSpbQuelle(any());
    }

    @Test
    void uebernehmeAnlageNichtsUebernommen() {
        Anlage andereAnlage = AnlageBuilder.anAnlage(12L)
                                           .betriebsstatusSeit(LocalDate.now())
                                           .inbetriebnahme(LocalDate.now())
                                           .anlageNr("test")
                                           .name("test")
                                           .betriebsstatus(
                                                   ReferenzBuilder.aBetriebsstatusReferenz("test").id(546L).build())
                                           .geoPunkt(new GeoPunkt(15, 15,
                                                   ReferenzBuilder.anEpsgReferenz("test").id(456L).build()))
                                           .leistungen(Collections.emptyList())
                                           .build();

        SpbAnlage spbAnlage = new SpbAnlage(andereAnlage);
        spbAnlage.setId(564L);

        AnlageUebernahme uebernahme = new AnlageUebernahme(ANLAGE.getId(), spbAnlage.getId(), false, false, false,
                false, false, false, false);

        when(stammdatenBetreiberService.loadSpbAnlage(spbAnlage.getId())).thenReturn(spbAnlage);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);

        when(stammdatenService.updateAnlageForUebernahme(any())).thenAnswer(i -> i.getArgument(0));

        Anlage anlage = service.uebernehmeAnlage(uebernahme);

        assertThat(anlage.isBetreiberdatenUebernommen()).isTrue();
        assertThat(anlage.getName()).isEqualTo(ANLAGE.getName());
        assertThat(anlage.getAnlageNr()).isEqualTo(ANLAGE.getAnlageNr());
        assertThat(anlage.getGeoPunkt()).isEqualTo(ANLAGE.getGeoPunkt());
        assertThat(anlage.getBetriebsstatus()).isEqualTo(ANLAGE.getBetriebsstatus());
        assertThat(anlage.getBetriebsstatusSeit()).isEqualTo(ANLAGE.getBetriebsstatusSeit());
        assertThat(anlage.getInbetriebnahme()).isEqualTo(ANLAGE.getInbetriebnahme());
        assertThat(anlage.getLeistungen()).containsExactlyInAnyOrderElementsOf(ANLAGE.getLeistungen());

        verify(stammdatenBetreiberService).deleteUebernommeneSpbAnlage(any());
    }

    @Test
    void uebernehmeAnlagenteilNichtsUebernommen() {
        Anlagenteil andererAnlagenteil = anAnlagenteil()
                .id(654L)
                .anlagenteilNr("test")
                .name("test")
                .betriebsstatus(
                        ReferenzBuilder.aBetriebsstatusReferenz("test")
                                       .id(564L)
                                       .build())
                .betriebsstatusSeit(LocalDate.MIN)
                .inbetriebnahme(LocalDate.MIN)
                .geoPunkt(new GeoPunkt(0, 1,
                        ReferenzBuilder.anEpsgReferenz("test")
                                       .id(15654L)
                                       .build()))
                .leistungen(new Leistung(10D,
                        ReferenzBuilder.aLeistungseinheitReferenz("test")
                                       .id(4561L)
                                       .build(), "test",
                        Leistungsklasse.BETRIEBEN))
                .build();

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(andererAnlagenteil);
        spbAnlagenteil.setId(15L);

        AnlagenteilUebernahme uebernahme = new AnlagenteilUebernahme(ANLAGENTEIL.getId(), spbAnlagenteil.getId(), false,
                false, false, false, false, false, false);

        when(stammdatenBetreiberService.loadSpbAnlagenteilById(spbAnlagenteil.getId())).thenReturn(
                spbAnlagenteil);
        when(stammdatenService.loadAnlage(ANLAGE.getId())).thenReturn(ANLAGE);
        when(stammdatenService.loadAnlagenteil(ANLAGENTEIL.getId())).thenReturn(ANLAGENTEIL);

        when(stammdatenService.updateAnlagenteil(any(), any())).thenAnswer(i -> i.getArgument(1));

        Anlagenteil anlagenteil = service.uebernehmeAnlagenteil(uebernahme);

        assertThat(anlagenteil.isBetreiberdatenUebernommen()).isTrue();
        assertThat(anlagenteil.getName()).isEqualTo(ANLAGENTEIL.getName());
        assertThat(anlagenteil.getAnlagenteilNr()).isEqualTo(ANLAGENTEIL.getAnlagenteilNr());
        assertThat(anlagenteil.getGeoPunkt()).isEqualTo(ANLAGENTEIL.getGeoPunkt());
        assertThat(anlagenteil.getBetriebsstatus()).isEqualTo(ANLAGENTEIL.getBetriebsstatus());
        assertThat(anlagenteil.getBetriebsstatusSeit()).isEqualTo(ANLAGENTEIL.getBetriebsstatusSeit());
        assertThat(anlagenteil.getInbetriebnahme()).isEqualTo(ANLAGENTEIL.getInbetriebnahme());
        assertThat(anlagenteil.getLeistungen()).containsExactlyInAnyOrderElementsOf(ANLAGENTEIL.getLeistungen());

        verify(stammdatenBetreiberService).deleteSpbAnlagenteil(any(), any());
    }

    @Test
    void uebernehmeBetreiberNichtsUebernommen() {
        Betreiber betreiberAnders = BetreiberBuilder.aBetreiber()
                                                    .adresse(AdresseBuilder.anAdresse()
                                                                           .strasse("andereStrasse")
                                                                           .plz("15")
                                                                           .hausNr("15")
                                                                           .ort("15")
                                                                           .ortsteil("15")
                                                                           .postfach("15")
                                                                           .postfachPlz("15")
                                                                           .landIsoCode(ReferenzBuilder.aReferenz(
                                                                                                               Referenzliste.RJAHR, "a")
                                                                                                       .id(6541L)
                                                                                                       .build())
                                                                           .build()
                                                    )
                                                    .bemerkung("test")
                                                    .betreiberNummer("798")
                                                    .ersteErfassung(Instant.now())
                                                    .jahr(ReferenzBuilder.aJahrReferenz(16).build())
                                                    .land(Land.NORDRHEIN_WESTFALEN)
                                                    .name("test").build();

        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiberAnders);
        spbBetreiber.setId(16L);

        AdresseUebernahme adresse = new AdresseUebernahme(false,
                false, false, false, false, false, false, false);

        BetreiberUebernahme uebernahme = new BetreiberUebernahme(BETREIBER.getId(), spbBetreiber.getId(), adresse,
                false);

        when(stammdatenService.loadBetreiber(BETREIBER.getId())).thenReturn(BETREIBER);
        when(stammdatenBetreiberService.loadSpbBetreiber(spbBetreiber.getId())).thenReturn(spbBetreiber);

        when(stammdatenService.updateBetreiber(any())).thenAnswer(i -> i.getArgument(0));

        Betreiber betreiber = service.uebernehmeBetreiber(uebernahme);

        assertThat(betreiber.isBetreiberdatenUebernommen()).isTrue();
        assertThat(betreiber.getName()).isEqualTo(BETREIBER.getName());
        assertThat(betreiber.getAdresse().getStrasse()).isEqualTo(BETREIBER.getAdresse().getStrasse());
        assertThat(betreiber.getAdresse().getHausNr()).isEqualTo(BETREIBER.getAdresse().getHausNr());
        assertThat(betreiber.getAdresse().getOrt()).isEqualTo(BETREIBER.getAdresse().getOrt());
        assertThat(betreiber.getAdresse().getOrtsteil()).isEqualTo(BETREIBER.getAdresse().getOrtsteil());
        assertThat(betreiber.getAdresse().getPlz()).isEqualTo(BETREIBER.getAdresse().getPlz());
        assertThat(betreiber.getAdresse().getPostfach()).isEqualTo(BETREIBER.getAdresse().getPostfach());
        assertThat(betreiber.getAdresse().getPostfachPlz()).isEqualTo(BETREIBER.getAdresse().getPostfachPlz());
        assertThat(betreiber.getAdresse().getLandIsoCode()).isEqualTo(BETREIBER.getAdresse().getLandIsoCode());

        verify(stammdatenBetreiberService).deleteSpbBetreiber(any());
    }

    @Test
    void uebernehmeBetriebsstaetteNichtsUebernommen() {
        Adresse adresse = AdresseBuilder.anAdresse()
                                        .strasse("test")
                                        .ort("test")
                                        .landIsoCode(ReferenzBuilder.aBetriebsstatusReferenz("test").id(564L).build())
                                        .ortsteil("test")
                                        .hausNr("test").postfach("test").postfachPlz("test").plz("test")
                                        .build();
        Betreiber betreiber = BetreiberBuilder.aBetreiber().adresse(adresse).name("test").build();
        Kommunikationsverbindung andereKommunikationsverbindung = new Kommunikationsverbindung(
                ReferenzBuilder.aKommunikationsTypReferenz("tel").id(159L).build(), "123456");
        Ansprechpartner ansprechpartner = AnsprechpartnerBuilder.anAnsprechpartner()
                                                                .nachname("test")
                                                                .vorname("test")
                                                                .funktion(
                                                                        ReferenzBuilder.aBetriebsstatusReferenz("test")
                                                                                       .id(546L)
                                                                                       .build())
                                                                .kommunikationsverbindungen(
                                                                        new Kommunikationsverbindung(
                                                                                ReferenzBuilder.aKommunikationsTypReferenz(
                                                                                        "test").id(21453L).build(),
                                                                                "test"))
                                                                .build();

        Betriebsstaette andereBetriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette()
                                                                      .betriebsstaetteNr("test")
                                                                      .name("test")
                                                                      .betriebsstatus(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "test").id(415L).build())
                                                                      .abfallerzeugerNummern(
                                                                              List.of(new AbfallerzeugerNr("15"),
                                                                                      new AbfallerzeugerNr("654")))
                                                                      .adresse(adresse)
                                                                      .betreiber(betreiber)
                                                                      .ansprechpartner(ansprechpartner)
                                                                      .betriebsstatusSeit(LocalDate.now())
                                                                      .inbetriebnahme(LocalDate.now())
                                                                      .istIndirekteinleiter(true)
                                                                      .istDirekteinleiter(false)
                                                                      .kommunikationsverbindungen(
                                                                              andereKommunikationsverbindung)
                                                                      .flusseinzugsgebiet(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "2020").id(15L).build())
                                                                      .build();

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(
                andereBetriebsstaette);
        spbBetriebsstaette.setId(879312879L);

        when(stammdatenService.loadBetriebsstaetteForWrite(BETRIEBSSTAETTE.getId())).thenReturn(BETRIEBSSTAETTE);
        when(stammdatenBetreiberService.loadSpbBetriebsstaette(spbBetriebsstaette.getId())).thenReturn(
                spbBetriebsstaette);

        when(stammdatenService.updateBetriebsstaetteForUebernahme(any())).thenAnswer(i -> i.getArgument(0));

        AdresseUebernahme adresseUebernahme = new AdresseUebernahme(false,
                false, false, false, false, false, false, false);

        BetriebsstaetteUebernahme uebernahme = new BetriebsstaetteUebernahme(BETRIEBSSTAETTE.getId(),
                spbBetriebsstaette.getId(), adresseUebernahme,
                false, false, false, false, false, false, false, false, false, false,
                false, false, false);

        Betriebsstaette betriebsstaette = service.uebernehmeBetriebsstaette(uebernahme);

        assertThat(betriebsstaette.isBetreiberdatenUebernommen()).isTrue();
        assertThat(betriebsstaette.getName()).isEqualTo(BETRIEBSSTAETTE.getName());
        assertThat(betriebsstaette.getAdresse().getStrasse()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getStrasse());
        assertThat(betriebsstaette.getAdresse().getHausNr()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getHausNr());
        assertThat(betriebsstaette.getAdresse().getOrt()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getOrt());
        assertThat(betriebsstaette.getAdresse().getOrtsteil()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getOrtsteil());
        assertThat(betriebsstaette.getAdresse().getPlz()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getPlz());
        assertThat(betriebsstaette.getAdresse().getPostfach()).isEqualTo(BETRIEBSSTAETTE.getAdresse().getPostfach());
        assertThat(betriebsstaette.getAdresse().getPostfachPlz()).isEqualTo(
                BETRIEBSSTAETTE.getAdresse().getPostfachPlz());
        assertThat(betriebsstaette.getAdresse().getLandIsoCode()).isEqualTo(
                BETRIEBSSTAETTE.getAdresse().getLandIsoCode());

        assertThat(betriebsstaette.getEinleiterNummern()).containsExactlyInAnyOrderElementsOf(
                BETRIEBSSTAETTE.getEinleiterNummern());
        assertThat(betriebsstaette.getAbfallerzeugerNummern()).containsExactlyInAnyOrderElementsOf(
                BETRIEBSSTAETTE.getAbfallerzeugerNummern());
        assertThat(betriebsstaette.getAnsprechpartner()).containsExactlyInAnyOrderElementsOf(
                BETRIEBSSTAETTE.getAnsprechpartner());
        assertThat(betriebsstaette.getKommunikationsverbindungen()).containsExactlyInAnyOrderElementsOf(
                BETRIEBSSTAETTE.getKommunikationsverbindungen());

        assertThat(betriebsstaette.getFlusseinzugsgebiet()).isEqualTo(BETRIEBSSTAETTE.getFlusseinzugsgebiet());
        assertThat(betriebsstaette.getNaceCode()).isEqualTo(BETRIEBSSTAETTE.getNaceCode());
        assertThat(betriebsstaette.isDirekteinleiter()).isEqualTo(BETRIEBSSTAETTE.isDirekteinleiter());
        assertThat(betriebsstaette.isIndirekteinleiter()).isEqualTo(BETRIEBSSTAETTE.isIndirekteinleiter());
        assertThat(betriebsstaette.getGeoPunkt()).isEqualTo(BETRIEBSSTAETTE.getGeoPunkt());
        assertThat(betriebsstaette.getBetriebsstatus()).isEqualTo(BETRIEBSSTAETTE.getBetriebsstatus());
        assertThat(betriebsstaette.getBetriebsstatusSeit()).isEqualTo(BETRIEBSSTAETTE.getBetriebsstatusSeit());
        assertThat(betriebsstaette.getInbetriebnahme()).isEqualTo(BETRIEBSSTAETTE.getInbetriebnahme());

        verify(stammdatenBetreiberService).deleteSpbBetriebsstaetteUebernahme(any());
    }

    @Test
    void uebernehmeQuelleNichtsUebernommen() {
        Quelle quelleAnders = aQuelle().id(33L)
                                       .parentAnlageId(222L)
                                       .parentBetriebsstaetteId(111L)
                                       .bemerkung("bemerkung2")
                                       .quelleNr("nummer2")
                                       .betreiberdatenUebernommen(false)
                                       .breite(12.12)
                                       .bauhoehe(22.22)
                                       .durchmesser(23.32)
                                       .flaeche(42.42)
                                       .laenge(52.52)
                                       .geoPunkt(new GeoPunkt(21, 22, anEpsgReferenz("2epsg").build()))
                                       .referenzAnlagenkataster("kataster2")
                                       .name("quelle2")
                                       .quellenArt(ReferenzBuilder.aReferenz(Referenzliste.RQUEA, "quelle2")
                                                                  .id(15987L)
                                                                  .build())
                                       .build();

        SpbQuelle spbQuelle = new SpbQuelle(quelleAnders);
        spbQuelle.setId(10L);

        QuelleUebernahme uebernahme = new QuelleUebernahme(QUELLE.getId(), spbQuelle.getId(), false, false, false,
                false, false, false, false, false);

        when(stammdatenService.loadQuelle(QUELLE.getId())).thenReturn(QUELLE);
        when(stammdatenBetreiberService.loadSpbQuelle(spbQuelle.getId())).thenReturn(spbQuelle);

        when(stammdatenService.updateQuelle(any())).thenAnswer(i -> i.getArgument(0));

        Quelle quelle = service.uebernehmeQuelle(uebernahme);

        assertThat(quelle.isBetreiberdatenUebernommen()).isTrue();
        assertThat(quelle.getName()).isEqualTo(QUELLE.getName());
        assertThat(quelle.getQuelleNr()).isEqualTo(QUELLE.getQuelleNr());
        assertThat(quelle.getGeoPunkt()).isEqualTo(QUELLE.getGeoPunkt());
        assertThat(quelle.getFlaeche()).isEqualTo(QUELLE.getFlaeche());
        assertThat(quelle.getLaenge()).isEqualTo(QUELLE.getLaenge());
        assertThat(quelle.getBreite()).isEqualTo(QUELLE.getBreite());
        assertThat(quelle.getDurchmesser()).isEqualTo(QUELLE.getDurchmesser());
        assertThat(quelle.getBauhoehe()).isEqualTo(QUELLE.getBauhoehe());

        verify(stammdatenBetreiberService).deleteSpbQuelle(any());
    }
}
