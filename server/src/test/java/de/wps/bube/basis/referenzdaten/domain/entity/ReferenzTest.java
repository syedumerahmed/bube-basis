package de.wps.bube.basis.referenzdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public class ReferenzTest {

    @Test
    public void testEquals() {

        Referenz v1 = getVorschrift();
        Referenz v2 = getVorschrift();

        assertEquals(v1, v2);
    }

    private static Referenz getVorschrift() {
        return aReferenz(Referenzliste.RVORS, "1")
                .land(Land.HESSEN)
                .ktext("Vorschrift 1")
                .gueltigVon(2017)
                .gueltigBis(2099)
                .build();
    }

    @Test
    void pruefeDaten_BundeseinheitlicheListe_Deutschland() {
        var ref = aReferenz(Referenzliste.RIET, "1").land(Land.DEUTSCHLAND).build();
        assertFalse(ref.isLaenderspezifisch());
        assertDoesNotThrow(ref::pruefeLand);
    }

    @Test
    void pruefeDaten_BundeseinheitlicheListe_Hessen() {
        var ref = aReferenz(Referenzliste.RIET, "1").land(Land.HESSEN).build();
        assertFalse(ref.isLaenderspezifisch());
        assertThrows(ReferenzdatenException.class, ref::pruefeLand);
    }

    @Test
    void pruefeDaten_LaenderspezifischeListe_Deutschland() {
        var ref = aReferenz(Referenzliste.RGMD, "1").land(Land.DEUTSCHLAND).build();
        assertTrue(ref.isLaenderspezifisch());
        assertDoesNotThrow(ref::pruefeLand);
    }

    @Test
    void pruefeDaten_LaenderspezifischeListe_Hessen() {
        var ref = aReferenz(Referenzliste.RGMD, "1").land(Land.HESSEN).build();
        assertTrue(ref.isLaenderspezifisch());
        assertDoesNotThrow(ref::pruefeLand);
    }

    @Test
    void testGueltigkeitszeitraumBis_OK() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(2007));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2015));
        assertDoesNotThrow(ref::pruefeGueltigkeit);
    }

    @Test
    void testGueltigkeitszeitraumBis_OK2() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(2007));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2007));
        assertDoesNotThrow(ref::pruefeGueltigkeit);
    }

    @Test
    void testGueltigkeitszeitraumBis_NOTOK() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(2007));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2006));
        assertThrows(ReferenzdatenException.class, ref::pruefeGueltigkeit);
    }

    @Test
    void testGueltigkeitszeitraumVon_OK() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(2007));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2015));
        assertDoesNotThrow(ref::pruefeGueltigkeit);
    }

    @Test
    void testGueltigkeitszeitraumVon_NOTOK() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(9999));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2015));
        assertThrows(ReferenzdatenException.class, ref::pruefeGueltigkeit);
    }

    @Test
    void gueltigFuer() {
        var ref = aReferenz(Referenzliste.RVORS, "1").build();
        ref.setGueltigVon(Gueltigkeitsjahr.of(2000));
        ref.setGueltigBis(Gueltigkeitsjahr.of(2015));

        assertFalse(ref.gueltigFuer(Gueltigkeitsjahr.of(1999)));
        assertTrue(ref.gueltigFuer(Gueltigkeitsjahr.of(2000)));
        assertTrue(ref.gueltigFuer(Gueltigkeitsjahr.of(2013)));
        assertTrue(ref.gueltigFuer(Gueltigkeitsjahr.of(2015)));
        assertFalse(ref.gueltigFuer(Gueltigkeitsjahr.of(2016)));
    }
}
