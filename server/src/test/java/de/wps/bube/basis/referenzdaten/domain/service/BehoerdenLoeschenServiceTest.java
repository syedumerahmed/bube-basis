package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.context.ApplicationEventPublisher;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdeGeloeschtEvent;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BehoerdenLoeschenServiceTest {

    @Mock
    private BehoerdenRepository repository;
    @Mock
    private BenutzerverwaltungService benutzerService;
    @Mock
    private BehoerdenService behoerdenService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private ParameterService parameterService;
    @Mock
    private ApplicationEventPublisher eventPublisher;

    private BehoerdenLoeschenService service;

    @BeforeEach
    void setUp() {
        service = new BehoerdenLoeschenService(behoerdenService, repository,
                parameterService, eventPublisher);
        lenient().doCallRealMethod().when(stammdatenService).handleBehoerdeGeloescht(any(BehoerdeGeloeschtEvent.class));
        lenient().doCallRealMethod().when(benutzerService).handleBehoerdeGeloescht(any(BehoerdeGeloeschtEvent.class));
        lenient().doAnswer(invocation -> {
            BehoerdeGeloeschtEvent event = invocation.getArgument(0);
            stammdatenService.handleBehoerdeGeloescht(event);
            benutzerService.handleBehoerdeGeloescht(event);
            return null;
        }).when(eventPublisher).publishEvent(any(BehoerdeGeloeschtEvent.class));
    }

    @Test
    void deleteBehoerde_behoerdeUsed_db() {
        var schluessel = "moin";
        var id = 123L;
        var land = Land.HESSEN;
        var b = aBehoerde(schluessel).id(id).land(land).build();
        when(behoerdenService.readBehoerdeForDelete(id)).thenReturn(b);
        when(stammdatenService.behoerdeUsed(id)).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.deleteBehoerde(id));
    }

    @Test
    void deleteBehoerde_behoerdeUsed_keycloak() {
        var land = Land.SAARLAND;
        var schluessel = "moin";
        var id = 123L;
        var b = aBehoerde(schluessel).id(id).land(land).build();
        when(behoerdenService.readBehoerdeForDelete(id)).thenReturn(b);
        when(stammdatenService.behoerdeUsed(id)).thenReturn(false);
        when(benutzerService.behoerdeStillUsed(land, schluessel)).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.deleteBehoerde(id));
    }

    @Test
    void deleteBehoerde_behoerdeUsed_parameter() {
        var land = Land.SAARLAND;
        var schluessel = "moin";
        var id = 123L;
        var b = aBehoerde(schluessel).id(id).land(land).build();
        when(behoerdenService.readBehoerdeForDelete(id)).thenReturn(b);
        when(stammdatenService.behoerdeUsed(id)).thenReturn(false);
        when(benutzerService.behoerdeStillUsed(land, schluessel)).thenReturn(false);
        when(parameterService.behoerdeUsedForBehoerdeLoeschen(b)).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.deleteBehoerde(id));
    }

    @Test
    void deleteBehoerde_ok() {
        var land = Land.SAARLAND;
        var schluessel = "moin";
        var id = 123L;
        var b = aBehoerde(schluessel).id(id).land(land).build();
        when(behoerdenService.readBehoerdeForDelete(id)).thenReturn(b);
        when(stammdatenService.behoerdeUsed(id)).thenReturn(false);
        when(benutzerService.behoerdeStillUsed(land, schluessel)).thenReturn(false);

        service.deleteBehoerde(id);

        verify(repository).deleteById(id);
    }
}
