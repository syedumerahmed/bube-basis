package de.wps.bube.basis.komplexpruefung;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.AnlagenteilBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetreiberBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;
import de.wps.bube.basis.euregistry.rules.bean.EURegAnlageBean;
import de.wps.bube.basis.euregistry.rules.bean.EURegBetriebsstaetteBean;
import de.wps.bube.basis.euregistry.rules.bean.EURegFBean;
import de.wps.bube.basis.stammdaten.rules.bean.QuelleBean;
import de.wps.bube.basis.referenzdaten.rules.bean.RegelApi;
import de.wps.bube.basis.stammdaten.rules.bean.StammdatenObjektBean;
import de.wps.bube.basis.euregistry.rules.bean.TeilberichtBean;
import de.wps.bube.basis.referenzdaten.domain.vo.Zustaendigkeit;

public class RegelApiDocumentationGenerator {

    private static final Class<?>[] OBJECT_CLASSES = {
            BetriebsstaetteBean.class, AnlageBean.class, AnlagenteilBean.class, QuelleBean.class, BetreiberBean.class,
            EURegBetriebsstaetteBean.class, EURegAnlageBean.class, EURegFBean.class, StammdatenObjektBean.class,
            TeilberichtBean.class
    };

    private static final String XML_PREFIX = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

    private static final DocumentBuilderFactory DOCUMENT_BUILDER = DocumentBuilderFactory.newDefaultInstance();
    private static final TransformerFactory TRANSFORMER = TransformerFactory.newInstance();

    private static final Map<Class<?>, Class<?>> BASE_TYPES = Map.of(
            Long.class, long.class,
            Double.class, double.class,
            String.class, String.class,
            LocalDate.class, Date.class,
            Instant.class, Date.class
    );
    private static URL xslResource;

    public static void main(String[] args)
            throws IOException, ClassNotFoundException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, ParserConfigurationException, SAXException,
            TransformerException, URISyntaxException {

        xslResource = RegelApiDocumentationGenerator.class.getClassLoader().getResource("regel-api.xsl");

        File xmlDir = new File(xslResource.toURI().resolve("../regel-api").toURL().getFile());
        File htmlDir = new File(
                xslResource.toURI().resolve("../../../webapp/src/assets/regel-api/generated").toURL().getFile());

        xmlDir.mkdirs();
        htmlDir.mkdirs();

        RegelApiDocumentationGenerator generator = new RegelApiDocumentationGenerator(xmlDir, htmlDir);
        generator.generateDocumentation();

    }

    private final File xmlDir;
    private final File htmlDir;
    private final LinkedHashMap<Class<?>, String> classes;
    private final LinkedHashMap<Class<? extends Enum<?>>, String> enums;

    public RegelApiDocumentationGenerator(File xmlDir, File htmlDir) {
        this.xmlDir = xmlDir;
        this.htmlDir = htmlDir;
        this.classes = new LinkedHashMap<>();
        this.enums = new LinkedHashMap<>();
    }

    public void generateDocumentation()
            throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            IOException, ParserConfigurationException, SAXException, TransformerException {

        for (Class<?> objectClass : OBJECT_CLASSES) {
            classes.put(objectClass, getTypeName(objectClass));
        }

        for (int i = 0; i < classes.size(); i++) {
            Class<?> clazz = new ArrayList<>(classes.keySet()).get(i);
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                Class<?> type = field.getType();
                RegelApi.Member member = field.getAnnotation(RegelApi.Member.class);
                if (member != null) {
                    if (member.exclude()) {
                        continue;
                    }
                    if (!member.type().equals(RegelApi.Null.class)) {
                        type = member.type();
                    }
                }
                if (Collection.class.isAssignableFrom(type)) {
                    type = getGenericType(clazz, field, type);
                }
                if (Enum.class.isAssignableFrom(type)) {
                    if (!enums.containsKey(type)) {
                        @SuppressWarnings("unchecked") Class<? extends Enum<?>> enumType = (Class<? extends Enum<?>>) type;
                        String name = enumType.equals(Zustaendigkeit.class) ? "Zuständigkeit" : enumType.getSimpleName();
                        enums.put(enumType, name);
                    }
                }
                String simpleName = type.getSimpleName();
                if (simpleName.endsWith("Bean") && !classes.containsKey(type)) {
                    classes.put(type, getTypeName(type));
                }
            }
        }

        File file = new File(xmlDir, "links.xml");
        try (FileWriter out = new FileWriter(file, StandardCharsets.UTF_8)) {
            out.append(XML_PREFIX).append("<types name=\"Links\">\n");
            BASE_TYPES.values()
                      .stream()
                      .map(Class::getSimpleName)
                      .map(StringUtils::capitalize)
                      .sorted()
                      .distinct()
                      .forEach(name -> {
                                  try {
                                      out.append("  <base-type name=\"").append(name).append("\"/>\n");
                                  } catch (IOException e) {
                                      throw new IllegalStateException(e);
                                  }
                              }
                      );

            for (Map.Entry<Class<? extends Enum<?>>, String> entry : enums.entrySet()) {
                documentEnum(entry.getKey());
                xml2Html(entry.getValue());
                out.append("  <enum-type name=\"").append(entry.getValue()).append("\"/>\n");
            }
            for (Map.Entry<Class<?>, String> entry : classes.entrySet()) {
                documentClass(entry.getKey());
                xml2Html(entry.getValue());
                out.append("  <bean-type name=\"").append(entry.getValue()).append("\"/>\n");
            }

            out.append("</types>").flush();
        }
        xml2Html("links");

    }

    private Class<?> getGenericType(Class<?> clazz, Member member, Class<?> type) throws ClassNotFoundException {
        Type genericType;
        if (member instanceof Field field) {
            genericType = field.getGenericType();
        } else if (member instanceof Method method) {
            genericType = method.getGenericReturnType();
        } else {
            throw new IllegalStateException("Invalid member type " + member.getClass());
        }
        if (genericType instanceof ParameterizedType parameterizedType) {
            type = Class.forName(parameterizedType.getActualTypeArguments()[0].getTypeName());
        } else {
            System.out.println("WARNING: target type of member '" + clazz.getName() + "." + member.getName()
                    + " cannot be determined due to type erasure");
        }
        return type;
    }

    private String getTypeName(Class<?> clazz) {
        RegelApi.Type type = clazz.getAnnotation(RegelApi.Type.class);
        return type != null && !type.name().isEmpty() ? type.name() : clazz.getSimpleName().replaceAll("Bean$", "");
    }

    private void documentClass(Class<?> clazz) throws IOException, ClassNotFoundException {
        String className = classes.get(clazz);
        File file = new File(xmlDir, className + ".xml");
        try (FileWriter out = new FileWriter(file, StandardCharsets.UTF_8)) {
            out.append(XML_PREFIX).append("<bean-type name=\"").append(className).append("\">\n");
            Field[] fields = clazz.getFields();
            for (Field field : fields) {
                writeField(out, clazz, field, field.getType(), field.getName(),
                        field.getAnnotation(RegelApi.Member.class));
            }
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                String name = method.getName();
                RegelApi.Member annotation = method.getAnnotation(RegelApi.Member.class);
                if ("getRegelObjekt".equals(name) || "getClass".equals(name) || (method.getParameterTypes().length > 0 && annotation == null)) {
                    continue;
                }
                Class<?> returnType = method.getReturnType();
                if (("getVorgaenger".equals(name) || "getNachfolger".equals(name)) && !clazz.equals(method.getReturnType())) {
                    continue;
                } else if (name.matches("^get[A-Z].*") && method.getParameterTypes().length == 0) {
                    name = StringUtils.uncapitalize(name.substring(3));
                } else if (name.matches("^is[A-Z].*$") && method.getParameterTypes().length == 0
                        && (returnType.equals(boolean.class) || returnType.equals(Boolean.class))) {
                    name = StringUtils.uncapitalize(name.substring(2));
                } else if (annotation == null) {
                    continue;
                }
                writeField(out, clazz, method, returnType, name, annotation);
            }
            out.append("</bean-type>").flush();
        }
    }

    private void writeField(FileWriter out, Class<?> clazz, Member member, Class<?> fieldType, String fieldName,
            RegelApi.Member memberAnnotation) throws ClassNotFoundException, IOException {
        if (memberAnnotation != null) {
            if (memberAnnotation.exclude()) {
                return;
            }
            if (!memberAnnotation.type().equals(RegelApi.Null.class)) {
                fieldType = memberAnnotation.type();
            }
        }
        boolean collection = Collection.class.isAssignableFrom(fieldType);
        if (Collection.class.isAssignableFrom(fieldType)) {
            fieldType = getGenericType(clazz, member, fieldType);
        }
        out.append("  <bean-field name=\"").append(fieldName).append("\" type=\"");
        if (fieldType.isPrimitive()) {
            out.append(fieldType.getName());
        } else if (BASE_TYPES.containsKey(fieldType)) {
            out.append(BASE_TYPES.get(fieldType).getSimpleName());
        } else if (classes.containsKey(fieldType)) {
            out.append(classes.get(fieldType)).append("\" bean=\"true");
        } else if (enums.containsKey(fieldType)) {
            out.append(enums.get(fieldType)).append("\" enum=\"true");
        } else {
            throw new IllegalStateException(fieldType.getName());
        }
        if (collection) {
            out.append("\" list=\"true");
        }
        if (memberAnnotation != null && !memberAnnotation.description().isBlank()) {
            out.append("\" description=\"").append(memberAnnotation.description());
        }
        if (memberAnnotation != null && memberAnnotation.referenzliste().length > 0) {
            String reflist = Arrays.stream(memberAnnotation.referenzliste())
                                   .map(Enum::name)
                                   .collect(Collectors.joining("|"));
            out.append("\" ref-list=\"").append(reflist);
        }
        if (member instanceof Method method && method.getParameterTypes().length > 0) {
            String params = Arrays.stream(method.getParameterTypes())
                                   .map(Class::getSimpleName)
                                   .collect(Collectors.joining(", "));
            out.append("\" params=\"").append(params);
        }
        out.append("\"/>\n");
    }

    private void documentEnum(Class<? extends Enum<?>> enumType)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IOException {

        String className = enums.get(enumType);
        File file = new File(xmlDir, className + ".xml");
        try (FileWriter out = new FileWriter(file, StandardCharsets.UTF_8)) {
            out.append(XML_PREFIX).append("<enum-type name=\"").append(className).append("\">\n");

            Map<String, Method> properties = new LinkedHashMap<>();
            Field[] declaredFields = enumType.getDeclaredFields();
            for (Field field : declaredFields) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    String getterName = "get" + field.getName().toUpperCase().charAt(0) + field.getName().substring(1);
                    Method method = enumType.getDeclaredMethod(getterName);
                    if (method != null) {
                        properties.put(field.getName(), method);
                    }
                }
            }
            for (Field constant : declaredFields) {
                if (constant.isEnumConstant()) {
                    out.append("  <enum-constant name=\"").append(constant.getName());
                    for (Map.Entry<String, Method> entry : properties.entrySet()) {
                        Object value = entry.getValue().invoke(constant.get(null));
                        out.append("\" ").append(entry.getKey()).append("=\"").append(value.toString());
                    }
                    out.append("\"/>\n");
                }
            }
            out.append("</enum-type>").flush();
        }
    }

    private void xml2Html(String className)
            throws ParserConfigurationException, IOException, SAXException, TransformerException {

        Document xml = DOCUMENT_BUILDER.newDocumentBuilder().parse(new File(xmlDir, className + ".xml"));
        try (InputStream xsl = xslResource.openStream()) {
            File html = new File(htmlDir, className + ".html");
            TRANSFORMER.newTransformer(new StreamSource(xsl)).transform(new DOMSource(xml), new StreamResult(html));
        }

    }

}
