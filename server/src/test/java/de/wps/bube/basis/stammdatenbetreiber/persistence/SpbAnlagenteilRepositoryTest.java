package de.wps.bube.basis.stammdatenbetreiber.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.AnlagenteilRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class SpbAnlagenteilRepositoryTest {

    @Autowired
    private SpbAnlagenteilRepository repository;

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    private AnlageRepository anlageRepository;
    @Autowired
    private AnlagenteilRepository anlagenteilRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Referenz vorschrift, leistungseinheit, jahr, gemeindeKennziffer, betriebsstatus;
    private Behoerde behoerde;

    private Anlage anlage;
    private Anlagenteil anlagenteil;
    private Betriebsstaette bst;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        vorschrift = referenzRepository.save(aVorschriftReferenz("RVORS").build());
        leistungseinheit = referenzRepository.save(aVorschriftReferenz("REINH").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());

        bst = getDefaultBetriebsstaetteBuilder().build();
        betriebsstaetteRepository.save(bst);

        anlage = AnlageBuilder.anAnlage().name("Name").anlageNr("NUMMER").parentBetriebsstaetteId(bst.getId()).build();
        anlageRepository.save(anlage);

        anlagenteil = AnlagenteilBuilder.anAnlagenteil()
                                        .name("Name")
                                        .anlagenteilNr("NUMMER")
                                        .parentAnlageId(anlage.getId())
                                        .build();
        anlagenteilRepository.save(anlagenteil);
    }

    @Test
    void createSpbAnlage_FromAnlage_with_Leitung_and_Vorschrift() {

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(anlagenteil);
        spbAnlagenteil.setName("Neue Anlage");
        spbAnlagenteil.setAnlagenteilNr("876598-NEU");

        //Vorschriften
        spbAnlagenteil.setVorschriften(List.of(VorschriftBuilder.aVorschrift().art(vorschrift).build()));

        //Leistungen
        var leistung = new Leistung(100.0, leistungseinheit, "Bezug",
                Leistungsklasse.GENEHMIGT);
        spbAnlagenteil.setLeistungen(List.of(leistung));

        // Act
        var spbAnlagenteilRead = repository.saveAndFlush(spbAnlagenteil);

        // Assert
        assertNotNull(spbAnlagenteilRead.getId());
        assertThat(spbAnlagenteilRead.getAnlagenteilId()).isEqualTo(anlagenteil.getId());
        assertThat(spbAnlagenteilRead.getName()).isEqualTo(spbAnlagenteilRead.getName());
        assertThat(spbAnlagenteilRead.getAnlagenteilNr()).isEqualTo(spbAnlagenteilRead.getAnlagenteilNr());

        assertThat(spbAnlagenteilRead.getVorschriften().size()).isEqualTo(1);
        assertThat(spbAnlagenteilRead.getVorschriften().get(0).getArt().getSchluessel()).isEqualTo(
                vorschrift.getSchluessel());

        assertThat(spbAnlagenteilRead.getLeistungen().size()).isEqualTo(1);
        assertThat(spbAnlagenteilRead.getLeistungen().get(0)).isEqualTo(
                leistung);

    }

    @Test
    void createSpbAnlage_New_AnlageChild() {

        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setParentAnlageId(anlage.getId());
        spbAnlagenteil.setName("Neue Anlage");
        spbAnlagenteil.setAnlagenteilNr("876598-NEU");

        // Act
        var spbAnlagenteilRead = repository.saveAndFlush(spbAnlagenteil);

        // Assert
        assertNotNull(spbAnlagenteilRead.getId());
    }

    @Test
    void findOnes() {
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setParentAnlageId(anlage.getId());
        spbAnlagenteil.setName("Neue Anlage");
        spbAnlagenteil.setAnlagenteilNr("876598-NEU");

        var spbAnlagenteilRead = repository.saveAndFlush(spbAnlagenteil);

        // Assert
        assertThat(repository.findOneByParentAnlageIdAndAnlagenteilNrIgnoreCase(spbAnlagenteilRead.getParentAnlageId(),
                spbAnlagenteilRead.getAnlagenteilNr())).isPresent();

        assertThat(repository.findOneByParentSpbAnlageIdAndAnlagenteilNrIgnoreCase(spbAnlagenteilRead.getParentAnlageId(),
                spbAnlagenteilRead.getAnlagenteilNr())).isEmpty();
    }

    @Test
    void existsByParentAnlageIdAndAnlagenteilNr() {
        var anlage1 = anlageRepository.save(anAnlage().build());
        var anlage2 = anlageRepository.save(anAnlage().build());
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil();
        spbAnlagenteil.setParentAnlageId(anlage1.getId());
        spbAnlagenteil.setAnlagenteilNr("abc");
        spbAnlagenteil.setName("SPB-Anlage");
        repository.save(spbAnlagenteil);

        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "ABC")).isTrue();
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsstatus)
                                 .akz("AKZ")
                                 .zustaendigeBehoerde(this.behoerde)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(this.gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }
}
