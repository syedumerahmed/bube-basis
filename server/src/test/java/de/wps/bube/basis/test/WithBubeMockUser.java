package de.wps.bube.basis.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.test.context.support.WithSecurityContext;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Thema;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@WithSecurityContext(factory = WithBubeMockUserFactory.class)
public @interface WithBubeMockUser {
    String name() default "bubeMockUser";

    Rolle[] roles() default { Rolle.BEHOERDENBENUTZER };

    Land land() default Land.HAMBURG;

    String[] behoerden() default {};

    String[] akz() default {};

    String[] verwaltungsgebiete() default {};

    String[] betriebsstaetten() default {};

    Fachmodul[] fachmodule() default {};

    Thema[] themen() default {};
}
