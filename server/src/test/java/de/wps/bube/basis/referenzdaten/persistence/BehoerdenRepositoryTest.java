package de.wps.bube.basis.referenzdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class BehoerdenRepositoryTest {

    @Autowired
    private BehoerdenRepository repository;

    @Test
    void testZeitraum_gueltig_vor_other() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, Land.HESSEN.getNr(), "01",
                2010, 2011);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_gueltig_nach_other() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2010)
                                         .gueltigBis(2012)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, Land.HESSEN.getNr(), "01",
                2013, 2020);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_ungueltig_gueltigBis_betweenOther() {
        repository.saveAndFlush(aBehoerde("moin").land(Land.HESSEN)
                                                 .schluessel("01")
                                                 .gueltigVon(2012)
                                                 .gueltigBis(2020)
                                                 .build());

        var duplicate = repository.functionalKeyDuplicate(987654L, Land.HESSEN.getNr(), "01",
                2010, 2013);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_beides_betweenOther() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, Land.HESSEN.getNr(), "01",
                2013, 2015);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_gueltigVon_betweenOther() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, Land.HESSEN.getNr(), "01",
                2015, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_umschliessend() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, Land.HESSEN.getNr(), "01",
                2010, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_gueltig_selbeID() {
        var id = repository.save(aBehoerde("moin").land(Land.HESSEN)
                                                  .schluessel("01")
                                                  .gueltigVon(2012)
                                                  .gueltigBis(2020)
                                                  .build()).getId();

        var duplicate = repository.functionalKeyDuplicate(id, Land.HESSEN.getNr(), "01",
                2010, 2015);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_ungueltig_ohneID() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(null, Land.HESSEN.getNr(), "01",
                2010, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_gueltig_ohneID() {
        repository.save(aBehoerde("moin").land(Land.HESSEN)
                                         .schluessel("01")
                                         .gueltigVon(2012)
                                         .gueltigBis(2020)
                                         .build());

        var duplicate = repository.functionalKeyDuplicate(null, Land.HESSEN.getNr(), "01",
                2010, 2011);

        assertThat(duplicate).isFalse();
    }

    @Test
    void findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc() {

        var behoerdeBuilder = aBehoerde("SCHLUESSEL").land(Land.HAMBURG);
        var gesamtliste = repository.saveAll(
                List.of(behoerdeBuilder.bezeichnung2("04").gueltigBis(2020).build(),
                        behoerdeBuilder.bezeichnung2("03").gueltigBis(2019).build(),
                        behoerdeBuilder.bezeichnung2("02").gueltigBis(2018).build(),
                        behoerdeBuilder.bezeichnung2("01").gueltigBis(2017).build()));

        var optionalBehoerde = repository.findFirstByLandAndSchluesselOrderByGueltigBisDesc(Land.HAMBURG,
                "SCHLUESSEL");

        assertTrue(optionalBehoerde.isPresent());
        assertThat(optionalBehoerde.get().getBezeichnung2()).isEqualTo("04");
    }
}
