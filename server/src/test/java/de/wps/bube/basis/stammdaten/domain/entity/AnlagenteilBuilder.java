package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.a13BImSchVVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.a17BImSchVVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschriftMitTaetigkeiten;
import static java.util.Collections.emptyList;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

public final class AnlagenteilBuilder {
    private Long id;
    private String referenzAnlagenkataster;
    private String anlagenteilNr;
    // Eine Teilanlage hat eine Eltern Anlage
    private Long parentAnlageId;

    // hat GeoPunkt 0..1
    private GeoPunkt geoPunkt;
    // hat Vorschriften 0..n
    private List<Vorschrift> vorschriften = new ArrayList<>();
    // hat Leistungen 0..n
    private List<Leistung> leistungen = new ArrayList<>();
    private List<ZustaendigeBehoerde> zustaendigeBehoerden = new ArrayList<>();
    private String name;
    private Referenz vertraulichkeitsgrund;
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;
    private String bemerkung;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private String localId;
    private boolean uebernommen;
    private Instant letzteAenderungBetreiber;

    private AnlagenteilBuilder() {
    }

    public static AnlagenteilBuilder anAnlagenteil() {
        return anAnlagenteil(null);
    }

    public static AnlagenteilBuilder anAnlagenteil(Long id) {
        return new AnlagenteilBuilder().id(id)
                                       .anlagenteilNr(String.valueOf(id))
                                       .name("Name")
                                       .letzteAenderung(
                                               LocalDateTime.of(2021, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                                       .ersteErfassung(
                                               LocalDateTime.of(2021, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")));
    }

    public static AnlagenteilBuilder aFullAnlagenteil() {
        return anAnlagenteil().id(2L)
                              .referenzAnlagenkataster("kataster")
                              .anlagenteilNr("nummer")
                              .parentAnlageId(23L)
                              .geoPunkt(new GeoPunkt(1, 2, anEpsgReferenz("epsg").build()))
                              .vorschriften(
                                      aVorschriftMitTaetigkeiten().art(
                                              aVorschriftReferenz("vorschrift1").id(1L).build()).build(),
                                      aVorschriftMitTaetigkeiten().art(
                                              aVorschriftReferenz("vorschrift2").id(2L).build()).build())
                              .leistungen(
                                      List.of(new Leistung(2.5, aLeistungseinheitReferenz("kg/schuss").build(), "bezug",
                                                      Leistungsklasse.GENEHMIGT),
                                              new Leistung(2.8, aLeistungseinheitReferenz("m/s").build(), "bezug2",
                                                      Leistungsklasse.BETRIEBEN)))
                              .name("Anlagenteil")
                              .vertraulichkeitsgrund(aVertraulichkeitsReferenz("vertraulich").build())
                              .betriebsstatus(aBetriebsstatusReferenz("betrieb").build())
                              .betriebsstatusSeit(LocalDate.of(2019, 10, 12))
                              .inbetriebnahme(LocalDate.of(2017, 1, 1))
                              .bemerkung("bemerkung")
                              .localId("localId")
                              .zustaendigeBehoerden(new ZustaendigeBehoerde(aBehoerde("behoerde2").build(),
                                              aZustaendigkeitsReferenz("zustaendig").build()),
                                      new ZustaendigeBehoerde(aBehoerde("behoerde3").build(),
                                              aZustaendigkeitsReferenz("zustaendig2").build()));
    }

    public AnlagenteilBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public AnlagenteilBuilder referenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        return this;
    }

    public AnlagenteilBuilder anlagenteilNr(String anlagenteilNr) {
        this.anlagenteilNr = anlagenteilNr;
        return this;
    }

    public AnlagenteilBuilder localId(String localId) {
        this.localId = localId;
        return this;
    }

    public AnlagenteilBuilder parentAnlageId(Long parentAnlageId) {
        this.parentAnlageId = parentAnlageId;
        return this;
    }

    public AnlagenteilBuilder geoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
        return this;
    }

    public AnlagenteilBuilder vorschriften(Vorschrift... vorschriften) {
        return vorschriften(List.of(vorschriften));
    }

    public AnlagenteilBuilder vorschriften(List<Vorschrift> vorschriften) {
        this.vorschriften = new ArrayList<>(vorschriften);
        return this;
    }

    public AnlagenteilBuilder addVorschrift(Vorschrift vorschrift) {
        this.vorschriften.add(vorschrift);
        return this;
    }

    public AnlagenteilBuilder leistungen(List<Leistung> leistungen) {
        this.leistungen = leistungen;
        return this;
    }

    public AnlagenteilBuilder leistungen(Leistung... leistungen) {
        return this.leistungen(List.of(leistungen));
    }

    public AnlagenteilBuilder name(String name) {
        this.name = name;
        return this;
    }

    public AnlagenteilBuilder vertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        return this;
    }

    public AnlagenteilBuilder betriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
        return this;
    }

    public AnlagenteilBuilder betriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
        return this;
    }

    public AnlagenteilBuilder inbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
        return this;
    }

    public AnlagenteilBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public AnlagenteilBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public AnlagenteilBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public AnlagenteilBuilder zustaendigeBehoerden(ZustaendigeBehoerde... zustaendigeBehoerden) {
        return zustaendigeBehoerden(new ArrayList<>(Arrays.asList(zustaendigeBehoerden)));
    }

    public AnlagenteilBuilder zustaendigeBehoerden(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        this.zustaendigeBehoerden = zustaendigeBehoerden;
        return this;
    }

    public AnlagenteilBuilder uebernommen(boolean uebernommen) {
        this.uebernommen = uebernommen;
        return this;
    }

    public AnlagenteilBuilder letzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
        return this;
    }

    public AnlagenteilBuilder addZustaendigeBehoerde(ZustaendigeBehoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerden.add(zustaendigeBehoerde);
        return this;
    }

    /**
     * Macht ein Anlagenteil für EuReg Betriebsstätte berichtspflichtig oder nicht
     *
     * @param pflichtig Wenn {@code true} werden die Vorschriften mit einer berichtspflichten Vorschrift überschrieben
     *                  Wenn {@code false} werden alle Vorschriften gelöscht
     *
     * @return
     */
    public AnlagenteilBuilder euRegPflichtigBst(boolean pflichtig) {
        if (pflichtig) {
            this.vorschriften(
                    aVorschriftMitTaetigkeiten().art(
                                                        aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build())
                                                .build());
        } else {
            this.vorschriften(emptyList());
        }
        return this;
    }

    public AnlagenteilBuilder add13BimschvVorschrift() {
        return addVorschrift(a13BImSchVVorschrift().build());
    }

    public AnlagenteilBuilder add17BimschvVorschrift() {
        return addVorschrift(a17BImSchVVorschrift().build());
    }

    public Anlagenteil build() {
        Anlagenteil anlagenteil = new Anlagenteil(parentAnlageId, anlagenteilNr, name);
        anlagenteil.setId(id);
        anlagenteil.setReferenzAnlagenkataster(referenzAnlagenkataster);
        anlagenteil.setGeoPunkt(geoPunkt);
        anlagenteil.setVorschriften(vorschriften);
        anlagenteil.setLeistungen(leistungen);
        anlagenteil.setLocalId(localId);
        anlagenteil.setVertraulichkeitsgrund(vertraulichkeitsgrund);
        anlagenteil.setBetriebsstatus(betriebsstatus);
        anlagenteil.setBetriebsstatusSeit(betriebsstatusSeit);
        anlagenteil.setInbetriebnahme(inbetriebnahme);
        anlagenteil.setBemerkung(bemerkung);
        anlagenteil.setErsteErfassung(ersteErfassung);
        anlagenteil.setLetzteAenderung(letzteAenderung);
        anlagenteil.setZustaendigeBehoerden(zustaendigeBehoerden);
        anlagenteil.setBetreiberdatenUebernommen(uebernommen);
        anlagenteil.setLetzteAenderungBetreiber(letzteAenderungBetreiber);
        return anlagenteil;
    }
}
