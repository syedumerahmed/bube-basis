package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class AnlagenteilRepositoryTest {

    @Autowired
    private AnlagenteilRepository repository;

    @Autowired
    private AnlageRepository anlageRepository;
    @Autowired
    private ReferenzRepository referenzRepository;

    @Test
    public void testFindById() {

        var anlagenteil = AnlagenteilRepositoryTest.getAnlagenteil();
        var anlagenteilRead = repository.save(anlagenteil);

        assertThat(repository.findById(anlagenteilRead.getId())).isPresent();

    }

    @Test
    public void testCreate() {

        var anlagenteil = AnlagenteilRepositoryTest.getAnlagenteil();

        assertThat(anlagenteil.getId()).isNull();
        var anlagenteilRead = repository.save(anlagenteil);
        assertThat(anlagenteilRead.getId()).isNotNull();
        assertThat(anlagenteil.getAnlagenteilNr()).isEqualTo(anlagenteilRead.getAnlagenteilNr());
        assertThat(anlagenteilRead.getErsteErfassung()).isEqualTo(anlagenteilRead.getErsteErfassung());
        assertThat(anlagenteil.getLetzteAenderung()).isEqualTo(anlagenteilRead.getLetzteAenderung());
    }

    @Test
    public void testDelete() {

        var anlagenteil = AnlagenteilRepositoryTest.getAnlagenteil();
        var anlagenteilRead = repository.save(anlagenteil);

        assertThat(repository.findById(anlagenteilRead.getId())).isPresent();
        repository.delete(anlagenteilRead);
        assertThat(repository.findById(anlagenteilRead.getId())).isEmpty();

    }

    @Test
    void testLeistungen() {

        var b = getAnlagenteil();
        var leistungen = new ArrayList<Leistung>();
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());
        leistungen.add(AnlagenteilRepositoryTest.getLeistung());

        b.setLeistungen(leistungen);

        var bRead = repository.save(b);
        assertThat(bRead.getLeistungen()).hasSize(7);

        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead.getLeistungen().remove(0);
        bRead = repository.save(b);
        assertThat(bRead.getLeistungen()).isEmpty();
    }

    @Test
    void testFindByParentAnlageIdAndAnlagenteilNr() {
        var anlage = anlageRepository.save(anAnlage(1L).build());
        repository.save(anAnlagenteil(1L).parentAnlageId(anlage.getId()).anlagenteilNr("nummer1").build());
        var anlagenteil2 = repository.save(
                anAnlagenteil(2L).parentAnlageId(anlage.getId()).anlagenteilNr("nummer2").build());

        var anlagenteile = repository.findByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage.getId(), "nummer2");

        assertThat(anlagenteile).hasValue(anlagenteil2);
    }

    @Test
    void testVorschriften_OneToMany() {
        var vorschrift1 = aVorschrift().art(referenzRepository.save(aVorschriftReferenz("11. BImSchV").build()))
                                       .build();
        var vorschrift2 = aVorschrift().art(referenzRepository.save(aVorschriftReferenz("4. BImSchV").build())).build();

        repository.saveAndFlush(anAnlagenteil(1L).vorschriften(vorschrift1, vorschrift2).build());
        assertDoesNotThrow(
                () -> repository.saveAndFlush(anAnlagenteil(2L).vorschriften(vorschrift1, vorschrift2).build()));
    }

    @Test
    void existsByParentAnlageIdAndAnlagenteilNr() {
        var anlage1 = anlageRepository.save(anAnlage().build());
        var anlage2 = anlageRepository.save(anAnlage().build());
        repository.save(anAnlagenteil().parentAnlageId(anlage1.getId()).anlagenteilNr("abc").build());

        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(anlage1.getId(), "ABC")).isTrue();
    }

    private static Anlagenteil getAnlagenteil() {
        return new Anlagenteil(15L, "0100", "Name");
    }

    private static Leistung getLeistung() {
        return new Leistung(100.0121D, aLeistungseinheitReferenz("m³").build(), "Bezug", Leistungsklasse.of("installiert"));
    }
}
