package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { StammdatenBerichtsjahrwechselMapperImpl.class, ReferenzResolverService.class })
class StammdatenBerichtsjahrwechselMapperTest {

    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private BehoerdenService behoerdenService;

    @Autowired
    private StammdatenBerichtsjahrwechselMapper mapper;

    @Test
    void referenzenNichtGefunden() {
        var betriebsstatus = aBetriebsstatusReferenz("01").gueltigBis(2020).build();
        var b = aBetriebsstaette(1L)
                .betriebsstatus(betriebsstatus)
                .build();
        var zieljahr = aJahrReferenz(2021).build();
        var neuerBetreiber = aBetreiber().build();
        when(referenzenService.findReferenz(any(), any(), any(), any())).thenReturn(empty());
        when(referenzenService.sucheReferenzUeberAlleJahre(any(), any(), any())).thenReturn(empty());

        var copyWithError = mapper.copyBetriebsstaetteToBerichtsjahr(b, zieljahr, neuerBetreiber);

        assertThat(copyWithError.errorsOrEmpty()).hasSize(1);
        assertThat(copyWithError.errorsOrEmpty().get(0).istWarnung()).isFalse();
    }

    @Test
    void referenzenGefunden() {
        var betriebsstatus = aBetriebsstatusReferenz("01").gueltigBis(2020).build();
        var betriebsstatusGueltig = aBetriebsstatusReferenz("01").gueltigBis(2021).build();

        var b = aBetriebsstaette(1L)
                .betriebsstatus(betriebsstatus)
                .build();
        var zieljahr = aJahrReferenz(2021).build();
        var neuerBetreiber = aBetreiber().build();
        when(referenzenService.findReferenz(any(), any(), any(), any())).thenReturn(Optional.of(betriebsstatusGueltig));

        var copyWithError = mapper.copyBetriebsstaetteToBerichtsjahr(b, zieljahr, neuerBetreiber);

        assertThat(copyWithError.errorsOrEmpty()).hasSize(0);
        assertThat(copyWithError.entity.getBetriebsstatus()).isEqualTo(betriebsstatusGueltig);
    }

    @Test
    void referenzenUberSucheGefunden() {
        var betriebsstatus = aBetriebsstatusReferenz("01").gueltigBis(2020).build();
        var betriebsstatusAlt = aBetriebsstatusReferenz("01").gueltigBis(2001).build();

        var b = aBetriebsstaette(1L)
                .betriebsstatus(betriebsstatus)
                .build();
        var zieljahr = aJahrReferenz(2021).build();
        var neuerBetreiber = aBetreiber().build();
        when(referenzenService.findReferenz(any(), any(), any(), any())).thenReturn(empty());
        when(referenzenService.sucheReferenzUeberAlleJahre(any(), any(), any())).thenReturn(
                Optional.of(betriebsstatusAlt));

        var copyWithError = mapper.copyBetriebsstaetteToBerichtsjahr(b, zieljahr, neuerBetreiber);

        assertThat(copyWithError.errorsOrEmpty()).hasSize(1);
        assertThat(copyWithError.errorsOrEmpty().get(0).istWarnung()).isTrue();
        assertThat(copyWithError.entity.getBetriebsstatus()).isEqualTo(betriebsstatusAlt);
    }

    @Test
    void copy() {
        var b = aFullBetriebsstaette().berichtsjahr(aJahrReferenz(2018).build()).build();
        var neuerBetreiber = aBetreiber(5L).build();
        var zieljahr = aJahrReferenz(2021).build();

        var copy = mapper.copyBetriebsstaetteToBerichtsjahr(b, zieljahr, neuerBetreiber).entity;

        assertThat(copy).isNotSameAs(b);
        assertThat(copy).usingRecursiveComparison()
                        .ignoringFieldsMatchingRegexes(".*id$", ".*Id$", ".*letzteAenderung.*", ".*betreiberdatenUebernommen")
                        .ignoringFields("betreiber", "berichtsjahr", "schreibsperreBetreiber")
                        .isEqualTo(b);
        assertThat(copy.getId()).isNull();
        assertThat(copy.getBerichtsjahr()).isSameAs(zieljahr);
        assertThat(copy.getBetreiber()).isSameAs(neuerBetreiber);
        assertThat(copy.isBetreiberdatenUebernommen()).isFalse();
        assertThat(copy.getAnlagen().get(0).getLetzteAenderungBetreiber()).isNull();
        assertThat(copy.getAnlagen().get(0).isBetreiberdatenUebernommen()).isFalse();
        assertThat(copy.getAnlagen().get(0).getAnlagenteile().get(0).getLetzteAenderungBetreiber()).isNull();
        assertThat(copy.getAnlagen().get(0).getAnlagenteile().get(0).isBetreiberdatenUebernommen()).isFalse();
        assertThat(copy.getAnlagen().get(0).getQuellen().get(0).getLetzteAenderungBetreiber()).isNull();
        assertThat(copy.getAnlagen().get(0).getQuellen().get(0).isBetreiberdatenUebernommen()).isFalse();
        assertThat(copy.getQuellen().get(0).getLetzteAenderungBetreiber()).isNull();
        assertThat(copy.getQuellen().get(0).isBetreiberdatenUebernommen()).isFalse();
    }
}
