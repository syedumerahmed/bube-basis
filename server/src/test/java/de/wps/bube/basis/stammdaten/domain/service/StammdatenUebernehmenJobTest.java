package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.stream.Stream;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;

@ExtendWith(MockitoExtension.class)
class StammdatenUebernehmenJobTest {

    private static final Referenz ZIELJAHR = aJahrReferenz(2019).build();
    private static final Referenz JAHR = aJahrReferenz(2018).build();
    private static final Land LAND = Land.MECKLENBURG_VORPOMMERN;
    private static final String USER = "username";

    @Mock
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Mock
    private BenutzerJobRegistry jobController;
    @Mock
    private StammdatenService stammdatenService;

    private StammdatenUebernehmenJob stammdatenUebernehmenJob;

    @BeforeEach
    void setUp() {
        stammdatenUebernehmenJob = new StammdatenUebernehmenJob(jobController, betriebsstaetteRepository,
                stammdatenService);
    }

    @Test
    void emptyBetriebsstaettenIds() {
        when(stammdatenService.streamAllById(Lists.emptyList())).thenReturn(Stream.empty());

        stammdatenUebernehmenJob.runAsync(Lists.emptyList(), ZIELJAHR, USER);

        verifyNoInteractions(betriebsstaetteRepository);
        verifyNoMoreInteractions(stammdatenService);
        verify(jobController, never()).jobCountChanged(1);
        verify(jobController).jobCompleted(false);
    }

    @Test
    void alreadyExisting_betriebsstaette_notCopied() {
        var b1 = aBetriebsstaette(1L).berichtsjahr(JAHR).land(LAND).betriebsstaetteNr("nummer1").build();
        var ids = List.of(1L);
        var jobStatus = new JobStatus(JobStatusEnum.STAMMDATEN_UEBERNEHEMEN);

        when(stammdatenService.streamAllById(ids)).thenReturn(Stream.of(b1));
        when(betriebsstaetteRepository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(ZIELJAHR.getId(), LAND,
                "nummer1")).thenReturn(true);
        when(jobController.getJobStatus()).thenReturn(java.util.Optional.of(jobStatus));

        stammdatenUebernehmenJob.runAsync(ids, ZIELJAHR, USER);

        verify(stammdatenService, never()).copyBetriebsstaette(eq(b1), eq(ZIELJAHR), any(JobProtokoll.class));
        verify(jobController, times(0)).jobCountChanged(1);
        verify(jobController).jobCompleted(true);
    }

    @Test
    void jobFailed() {
        var e = new RuntimeException();
        when(stammdatenService.streamAllById(anyList())).thenThrow(e);

        stammdatenUebernehmenJob.runAsync(List.of(1L), ZIELJAHR, USER);

        verifyNoMoreInteractions(stammdatenService);
        verifyNoInteractions(betriebsstaetteRepository);
        verify(jobController, never()).jobCountChanged(1);
        verify(jobController, never()).jobCompleted(false);
        verify(jobController).jobError(e);
    }
}
