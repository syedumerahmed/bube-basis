package de.wps.bube.basis.koordinaten.domain.service;

import static de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService.EPSG_25832;
import static de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService.EPSG_25833;
import static de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService.EPSG_4647;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;

class KoordinatenTransformationServiceTest {

    @Test
    void transform25832_Alsterfontaene() {
        KoordinatenTransformationService service = new KoordinatenTransformationService();
        var geoPunkt = new GeoPunkt(5934478L, 565964L, ReferenzBuilder.anEpsgReferenz(EPSG_25832).build());
        var koordinaten = service.transformToEPSG4258(geoPunkt);
        assertThat(koordinaten[0]).isEqualTo(53.55504);
        assertThat(koordinaten[1]).isEqualTo(9.99575);
    }

    @Test
    void transform25833_Alsterfontaene() {
        KoordinatenTransformationService service = new KoordinatenTransformationService();
        var geoPunkt = new GeoPunkt(5945671L, 168613L, ReferenzBuilder.anEpsgReferenz(EPSG_25833).build());
        var koordinaten = service.transformToEPSG4258(geoPunkt);
        assertThat(koordinaten[0]).isEqualTo(53.55504);
        assertThat(koordinaten[1]).isEqualTo(9.9958);
    }

    @Test
    void transform4647_Alsterfontaene() {
        KoordinatenTransformationService service = new KoordinatenTransformationService();
        var geoPunkt = new GeoPunkt(5934481L, 32565965L, ReferenzBuilder.anEpsgReferenz(EPSG_4647).build());
        var koordinaten = service.transformToEPSG4258(geoPunkt);
        assertThat(koordinaten[0]).isEqualTo(53.55507);
        assertThat(koordinaten[1]).isEqualTo(9.99576);
    }

    @Test
    void transformWrongEPSG() {
        KoordinatenTransformationService service = new KoordinatenTransformationService();
        var geoPunkt = new GeoPunkt(5934478L, 565964L, ReferenzBuilder.anEpsgReferenz("101").build());
        assertThrows(KoordinatenException.class, () -> service.transformToEPSG4258(geoPunkt));
    }
}