package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class BetreiberRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BetreiberRepository repository;
    @Autowired
    private ReferenzRepository referenzRepository;
    private Referenz jahr;

    @BeforeEach
    void setUp() {
        jahr = entityManager.persistFlushFind(aJahrReferenz(2012).build());
    }

    @Test
    public void testFindById() {

        var b = getBetreiberBuilder().build();
        var bRead = repository.save(b);

        assertTrue(repository.findById(bRead.getId()).isPresent());

    }

    @Test
    public void testCreate() {

        var b = getBetreiberBuilder().build();

        assertNull(b.getId());
        var bRead = repository.save(b);
        assertNotNull(bRead.getId());

        assertEquals(b.getAdresse(), bRead.getAdresse());
        assertEquals(b.getErsteErfassung(), bRead.getErsteErfassung());
        assertEquals(b.getLetzteAenderung(), bRead.getLetzteAenderung());

    }

    @Test
    public void testDelete() {

        var b = getBetreiberBuilder().build();
        var bRead = repository.save(b);

        assertTrue(repository.findById(bRead.getId()).isPresent());
        repository.delete(bRead);
        assertFalse(repository.findById(bRead.getId()).isPresent());

    }

    @Test
    void testFindByJahrLandNummer_OhneNummer() {
        var jahr = referenzRepository.save(aJahrReferenz(2020).build());
        var b = BetreiberBuilder.aBetreiber().jahr(jahr).land(Land.BAYERN).betreiberNummer("nummer").build();
        repository.save(b);

        var result = repository.findByBerichtsjahrAndLandAndBetreiberNummer(jahr, Land.BAYERN, null);

        assertThat(result).isEmpty();
    }

    @Test
    void aenderungDurchBetreiber() {
        var betreiber = entityManager.persistFlushFind(
                getBetreiberBuilder()
                        .betreiberdatenUebernommen(true)
                        .letzteAenderungBetreiber(null)
                        .build());
        var modifiedDateSpl = betreiber.getLetzteAenderung();

        repository.aenderungDurchBetreiber(betreiber.getId());
        entityManager.clear();
        var updatedBst = repository.getById(betreiber.getId());

        assertThat(updatedBst.getLetzteAenderung()).isCloseTo(modifiedDateSpl, within(1, MILLIS));
        assertThat(updatedBst.isBetreiberdatenUebernommen()).isFalse();
        assertThat(updatedBst.getLetzteAenderungBetreiber()).isAfter(betreiber.getErsteErfassung());
    }

    private BetreiberBuilder getBetreiberBuilder() {
        return BetreiberBuilder.aBetreiber().jahr(jahr).name("name");
    }
}
