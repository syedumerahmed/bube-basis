package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder.aFullAdresse;
import static de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder.anAdresse;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aFullBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static java.util.Collections.emptyList;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

public final class BetriebsstaetteBuilder {
    private Long id;
    private Referenz berichtsjahr;
    private String localId;
    private Land land;
    private Behoerde zustaendigeBehoerde;
    // Aufgabenbereichskennzahl
    private String akz;
    private String betriebsstaetteNr;
    private Betreiber betreiber;
    // hat immer genau eine Adresse
    private Adresse adresse;
    // hat Ansprechpartner 0..n
    private List<Ansprechpartner> ansprechpartner = new ArrayList<>();
    // hat GeoPunkt 0..1
    private GeoPunkt geoPunkt;
    // hat Gemeinden
    private Referenz gemeindekennziffer;
    // hat EinleiterNr-n 0..n
    private List<EinleiterNr> einleiterNummern = new ArrayList<>();
    // hat Vorschriften 0..n
    private List<Referenz> vorschriften = new ArrayList<>();
    // hat AbfallENr-n // Abfallerzeugernr. 0..n
    private List<AbfallerzeugerNr> abfallerzeugerNummern = new ArrayList<>();
    // hat ZBehoerde // zuständige Bhd. 0..n
    private List<ZustaendigeBehoerde> zustaendigeBehoerden = new ArrayList<>();
    private List<Kommunikationsverbindung> kommunikationsverbindungen = new ArrayList<>();
    private String name;
    private Referenz vertraulichkeitsgrund;
    private Referenz flusseinzugsgebiet;
    private Referenz naceCode;
    private boolean istDirekteinleiter;
    private boolean istIndirekteinleiter;
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;
    private String bemerkung;
    private boolean betreiberdatenUebernommen;
    private Instant letzteAenderungBetreiber;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private List<Anlage> anlagen = new ArrayList<>();
    private List<Quelle> quellen = new ArrayList<>();

    private BetriebsstaetteBuilder() {
    }

    public static BetriebsstaetteBuilder aBetriebsstaette() {
        return aBetriebsstaette(null);
    }

    public static BetriebsstaetteBuilder aBetriebsstaette(Long id) {
        Referenz jahr = aJahrReferenz(2020).build();
        return new BetriebsstaetteBuilder().id(id)
                                           .berichtsjahr(jahr)
                                           .land(Land.HAMBURG)
                                           .zustaendigeBehoerde(aBehoerde("BEHOERDE").land(Land.HAMBURG).build())
                                           .betriebsstaetteNr("BS000000")
                                           .betriebsstatus(aBetriebsstatusReferenz("In Betrieb").build())
                                           .gemeindekennziffer(aGemeindeReferenz("GEMEINDE").build())
                                           .name("A Betriebsstätte")
                                           .adresse(anAdresse().build());
    }

    public static BetriebsstaetteBuilder aFullBetriebsstaette() {
        return new BetriebsstaetteBuilder().id(1L)
                                           .berichtsjahr(aJahrReferenz(2018).build())
                                           .betreiber(aFullBetreiber().build())
                                           .localId("localId")
                                           .betriebsstaetteNr("nummer")
                                           .land(Land.NORDRHEIN_WESTFALEN)
                                           .letzteAenderung(
                                                   LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                                           .ersteErfassung(
                                                   LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                                           .anlagen(anAnlage(1L).vorschriften(
                                                                        aVorschrift().art(aVorschriftReferenz("R4BV").build())
                                                                                     .taetigkeiten(aTaetigkeitsReferenz("16").build())
                                                                                     .haupttaetigkeit(aTaetigkeitsReferenz("16").build())
                                                                                     .build(),
                                                                        aVorschrift().art(aVorschriftReferenz("R4BV").build()).build())
                                                                .anlagenteile(anAnlagenteil(1L).vorschriften(
                                                                        aVorschrift().art(
                                                                                             aVorschriftReferenz("R4BV").build())
                                                                                     .taetigkeiten(aTaetigkeitsReferenz(
                                                                                             "16").build())
                                                                                     .haupttaetigkeit(
                                                                                             aTaetigkeitsReferenz(
                                                                                                     "16").build())
                                                                                     .build()).build())
                                                                .quellen(aQuelle(3L).betreiberdatenUebernommen(true)
                                                                                    .letzteAenderungBetreiber(
                                                                                            Instant.ofEpochSecond(
                                                                                                    1637327737))
                                                                                    .build())
                                                                .uebernommen(true)
                                                                .letzteAenderungBetreiber(
                                                                        Instant.ofEpochSecond(1637327737))
                                                                .build(), anAnlage(2L).build())
                                           .quellen(aQuelle(1L)
                                                   .letzteAenderungBetreiber(Instant.ofEpochSecond(1637327737))
                                                   .build(), aQuelle(2L).build())
                                           .gemeindekennziffer(ReferenzBuilder.aGemeindeReferenz("gemeinde").build())
                                           .zustaendigeBehoerde(aBehoerde("behoerde").build())
                                           .abfallerzeugerNummern(
                                                   List.of(new AbfallerzeugerNr("nr1"), new AbfallerzeugerNr("nr2")))
                                           .akz("AKZ")
                                           .adresse(aFullAdresse().build())
                                           .ansprechpartner(AnsprechpartnerBuilder.aFullAnsprechpartner().build(),
                                                   AnsprechpartnerBuilder.aFullAnsprechpartner().build())
                                           .bemerkung("bemerkung")
                                           .betreiberdatenUebernommen(false)
                                           .betriebsstatus(ReferenzBuilder.aBetriebsstatusReferenz("betrieb").build())
                                           .betriebsstatusSeit(LocalDate.of(2018, 10, 10))
                                           .einleiterNummern(List.of(new EinleiterNr("nr1"), new EinleiterNr("nr2")))
                                           .flusseinzugsgebiet(aFlusseinzugsgebiet("FLEZ").build())
                                           .geoPunkt(new GeoPunkt(1, 2, anEpsgReferenz("epsg").build()))
                                           .inbetriebnahme(LocalDate.of(2018, 12, 15))
                                           .istDirekteinleiter(true)
                                           .istIndirekteinleiter(false)
                                           .kommunikationsverbindungen(new Kommunikationsverbindung(
                                                           aKommunikationsTypReferenz("tel").build(), "123"),
                                                   new Kommunikationsverbindung(
                                                           aKommunikationsTypReferenz("fax").build(), "321"))
                                           .naceCode(aNaceCodeReferenz("nace").build())
                                           .name("Halsbrecher Betrieb")
                                           .vertraulichkeitsgrund(aVertraulichkeitsReferenz("vertraulich").build())
                                           .vorschriften(aVorschriftReferenz("R4BV").build(),
                                                   aVorschriftReferenz("11BV").build())
                                           .zustaendigeBehoerden(new ZustaendigeBehoerde(aBehoerde("behoerde2").build(),
                                                           aZustaendigkeitsReferenz("zustaendig").build()),
                                                   new ZustaendigeBehoerde(aBehoerde("behoerde3").build(),
                                                           aZustaendigkeitsReferenz("zustaendig2").build()));
    }

    public BetriebsstaetteBuilder id(Long id) {
        this.id = id;
        this.anlagen.forEach(a -> a.setParentBetriebsstaetteId(id));
        this.quellen.forEach(a -> a.setParentBetriebsstaetteId(id));
        return this;
    }

    public BetriebsstaetteBuilder berichtsjahr(Referenz berichtsjahr) {
        this.berichtsjahr = berichtsjahr;
        return this;
    }

    public BetriebsstaetteBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public BetriebsstaetteBuilder zustaendigeBehoerde(Behoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerde = zustaendigeBehoerde;
        return this;
    }

    public BetriebsstaetteBuilder akz(String akz) {
        this.akz = akz;
        return this;
    }

    public BetriebsstaetteBuilder betriebsstaetteNr(String betriebsstaetteNr) {
        this.betriebsstaetteNr = betriebsstaetteNr;
        return this;
    }

    public BetriebsstaetteBuilder betreiber(Betreiber betreiber) {
        this.betreiber = betreiber;
        return this;
    }

    public BetriebsstaetteBuilder adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public BetriebsstaetteBuilder ansprechpartner(Ansprechpartner... ansprechpartner) {
        return ansprechpartner(List.of(ansprechpartner));
    }

    public BetriebsstaetteBuilder ansprechpartner(List<Ansprechpartner> ansprechpartner) {
        this.ansprechpartner = new ArrayList<>(ansprechpartner);
        return this;
    }

    public BetriebsstaetteBuilder geoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
        return this;
    }

    public BetriebsstaetteBuilder gemeindekennziffer(Referenz gemeindekennziffer) {
        this.gemeindekennziffer = gemeindekennziffer;
        return this;
    }

    public BetriebsstaetteBuilder einleiterNummern(List<EinleiterNr> einleiterNummern) {
        this.einleiterNummern = einleiterNummern;
        return this;
    }

    public BetriebsstaetteBuilder vorschriften(Referenz... vorschriften) {
        return vorschriften(List.of(vorschriften));
    }

    public BetriebsstaetteBuilder vorschriften(List<Referenz> vorschriften) {
        this.vorschriften = vorschriften;
        return this;
    }

    public BetriebsstaetteBuilder abfallerzeugerNummern(List<AbfallerzeugerNr> abfallerzeugerNummern) {
        this.abfallerzeugerNummern = abfallerzeugerNummern;
        return this;
    }

    public BetriebsstaetteBuilder zustaendigeBehoerden(ZustaendigeBehoerde... zustaendigeBehoerden) {
        return zustaendigeBehoerden(List.of(zustaendigeBehoerden));
    }

    public BetriebsstaetteBuilder zustaendigeBehoerden(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        this.zustaendigeBehoerden = zustaendigeBehoerden;
        return this;
    }

    public BetriebsstaetteBuilder kommunikationsverbindungen(Kommunikationsverbindung... kommunikationsverbindungen) {
        return kommunikationsverbindungen(List.of(kommunikationsverbindungen));
    }

    public BetriebsstaetteBuilder kommunikationsverbindungen(
            List<Kommunikationsverbindung> kommunikationsverbindungen) {
        this.kommunikationsverbindungen = kommunikationsverbindungen;
        return this;
    }

    public BetriebsstaetteBuilder name(String name) {
        this.name = name;
        return this;
    }

    public BetriebsstaetteBuilder vertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        return this;
    }

    public BetriebsstaetteBuilder flusseinzugsgebiet(Referenz flusseinzugsgebiet) {
        this.flusseinzugsgebiet = flusseinzugsgebiet;
        return this;
    }

    public BetriebsstaetteBuilder naceCode(Referenz naceCode) {
        this.naceCode = naceCode;
        return this;
    }

    public BetriebsstaetteBuilder istDirekteinleiter(boolean istDirekteinleiter) {
        this.istDirekteinleiter = istDirekteinleiter;
        return this;
    }

    public BetriebsstaetteBuilder istIndirekteinleiter(boolean istIndirekteinleiter) {
        this.istIndirekteinleiter = istIndirekteinleiter;
        return this;
    }

    public BetriebsstaetteBuilder betriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
        return this;
    }

    public BetriebsstaetteBuilder betriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
        return this;
    }

    public BetriebsstaetteBuilder inbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
        return this;
    }

    public BetriebsstaetteBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public BetriebsstaetteBuilder betreiberdatenUebernommen(boolean betreiberdatenUebernommen) {
        this.betreiberdatenUebernommen = betreiberdatenUebernommen;
        return this;
    }

    public BetriebsstaetteBuilder letzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
        return this;
    }

    public BetriebsstaetteBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public BetriebsstaetteBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public BetriebsstaetteBuilder localId(String localId) {
        this.localId = localId;
        return this;
    }

    public BetriebsstaetteBuilder anlagen(List<Anlage> anlagen) {
        this.anlagen = new ArrayList<>(anlagen);
        this.anlagen.forEach(a -> a.setParentBetriebsstaetteId(this.id));
        return this;
    }

    public BetriebsstaetteBuilder anlagen(Anlage... anlagen) {
        return this.anlagen(List.of(anlagen));
    }

    public BetriebsstaetteBuilder addAnlage(Anlage anlage) {
        this.anlagen.add(anlage);
        return this;
    }

    public BetriebsstaetteBuilder addAnlage(AnlageBuilder anlageBuilder) {
        return addAnlage(anlageBuilder.parentBetriebsstaetteId(id).build());
    }

    public BetriebsstaetteBuilder quellen(List<Quelle> quellen) {
        this.quellen = quellen;
        return this;
    }

    public BetriebsstaetteBuilder quellen(Quelle... quellen) {
        return this.quellen(List.of(quellen));
    }

    /**
     * Fügt einer Betriebsstätte die 13.BImSchV Vorschrift hinzu oder löscht alle Vorschriften
     *
     * @param hatVorschrift Wenn {@code true} werden die Vorschriften mit der 13.BImSchV Vorschrift überschrieben
     *                      Wenn {@code false} werden alle Vorschriften und alle Anlagen gelöscht
     *
     * @return
     */
    public BetriebsstaetteBuilder hat13BImSchV(boolean hatVorschrift) {
        if (hatVorschrift) {
            this.vorschriften(aVorschriftReferenz(VorschriftSchluessel.V_13BV.getSchluessel()).build());
        } else {
            this.vorschriften(emptyList());
            this.anlagen(emptyList());
        }
        return this;
    }

    public Betriebsstaette build() {
        Betriebsstaette betriebsstaette = new Betriebsstaette();
        betriebsstaette.setId(id);
        betriebsstaette.setBerichtsjahr(berichtsjahr);
        betriebsstaette.setLand(land);
        betriebsstaette.setZustaendigeBehoerde(zustaendigeBehoerde);
        betriebsstaette.setBetriebsstaetteNr(betriebsstaetteNr);
        betriebsstaette.setBetreiber(betreiber);
        betriebsstaette.setAkz(akz);
        betriebsstaette.setAdresse(adresse);
        betriebsstaette.setAnsprechpartner(ansprechpartner);
        betriebsstaette.setGeoPunkt(geoPunkt);
        betriebsstaette.setGemeindekennziffer(gemeindekennziffer);
        betriebsstaette.setEinleiterNummern(einleiterNummern);
        betriebsstaette.setVorschriften(vorschriften);
        betriebsstaette.setAbfallerzeugerNummern(abfallerzeugerNummern);
        betriebsstaette.setZustaendigeBehoerden(zustaendigeBehoerden);
        betriebsstaette.setName(name);
        betriebsstaette.setVertraulichkeitsgrund(vertraulichkeitsgrund);
        betriebsstaette.setFlusseinzugsgebiet(flusseinzugsgebiet);
        betriebsstaette.setNaceCode(naceCode);
        betriebsstaette.setDirekteinleiter(istDirekteinleiter);
        betriebsstaette.setIndirekteinleiter(istIndirekteinleiter);
        betriebsstaette.setBetriebsstatus(betriebsstatus);
        betriebsstaette.setBetriebsstatusSeit(betriebsstatusSeit);
        betriebsstaette.setInbetriebnahme(inbetriebnahme);
        betriebsstaette.setBemerkung(bemerkung);
        betriebsstaette.setBetreiberdatenUebernommen(betreiberdatenUebernommen);
        betriebsstaette.setLetzteAenderungBetreiber(letzteAenderungBetreiber);
        betriebsstaette.setErsteErfassung(ersteErfassung);
        betriebsstaette.setLetzteAenderung(letzteAenderung);
        betriebsstaette.setLocalId(localId);
        betriebsstaette.setQuellen(quellen);
        anlagen.forEach(a -> a.setParentBetriebsstaetteId(id));
        betriebsstaette.setAnlagen(anlagen);
        betriebsstaette.setKommunikationsverbindungen(kommunikationsverbindungen);
        return betriebsstaette;
    }
}
