package de.wps.bube.basis.stammdaten.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.stream.Stream;

import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;
import de.wps.bube.basis.stammdaten.xml.dto.ObjectFactory;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.xmlunit.builder.Input;

import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;

@SpringBootTest(classes = { BetriebsstaetteXMLMapperImpl.class, CommonXMLMapperImpl.class,
        ReferenzResolverService.class })
class BetriebsstaetteXMLServiceWriteTest {

    private static final String VALID_FULL_XML = "xml/stammdaten_import_test_full.xml";
    private static final String WHITESPACE = "[\n\r\t ]";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BetriebsstaetteXMLMapper mapper;
    @MockBean
    private BehoerdenService behoerdenService;
    @MockBean
    private ReferenzenService referenzenService;

    @Test
    void writeBetriebsstaette() throws IOException {
        var expectedXML = Input.fromStream(getResource(VALID_FULL_XML));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        var objectFactory = new ObjectFactory();

        var writer = xmlService.createWriter(BetriebsstaetteType.class, BetriebsstaettenXDto.QUALIFIED_NAME,
                (Betriebsstaette betriebsstaette) -> mapper.toXDto(betriebsstaette), objectFactory::createBetriebsstaette);

        writer.write(Stream.of(getBetriebsstaette()), outputStream);

        MatcherAssert.assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
    }

    private Betriebsstaette getBetriebsstaette() {
        return BetriebsstaetteBuilder.aFullBetriebsstaette().betreiberdatenUebernommen(true).build();
    }

}
