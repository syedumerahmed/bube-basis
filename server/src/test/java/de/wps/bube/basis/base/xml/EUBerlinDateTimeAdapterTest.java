package de.wps.bube.basis.base.xml;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.jupiter.api.Test;

class EUBerlinDateTimeAdapterTest {

    @Test
    void unmarshal_ohne_zeitzone() {
       // Assert
        assertThrows(IllegalArgumentException.class, () -> EUBerlinDateTimeAdapter.unmarshal("2021-03-22T19:55:02"));
    }

    @Test
    void unmarshal_winterzeit_mit_zeitzone_Z() {
        // Act
        XMLGregorianCalendar xmlGregorianCalendar = EUBerlinDateTimeAdapter.unmarshal("2021-03-22T19:55:02Z");

        // Assert
        assertThat(xmlGregorianCalendar.getYear()).isEqualTo(2021);
        assertThat(xmlGregorianCalendar.getMonth()).isEqualTo(3);
        assertThat(xmlGregorianCalendar.getDay()).isEqualTo(22);

        assertThat(xmlGregorianCalendar.getHour()).isEqualTo(19);
        assertThat(xmlGregorianCalendar.getMinute()).isEqualTo(55);
        assertThat(xmlGregorianCalendar.getSecond()).isEqualTo(2);
    }

    @Test
    void unmarshal_nur_minuten_exception() {
       // Assert
        assertThrows(IllegalArgumentException.class, () -> EUBerlinDateTimeAdapter.unmarshal("2021-03-22T19:55"));
    }

    @Test
    void unmarshal_winterzeit_mit_zeitzone() {
        // Act
        XMLGregorianCalendar xmlGregorianCalendar = EUBerlinDateTimeAdapter.unmarshal("2021-03-22T19:55:02+01:00");

        // Assert
        assertThat(xmlGregorianCalendar.getYear()).isEqualTo(2021);
        assertThat(xmlGregorianCalendar.getMonth()).isEqualTo(3);
        assertThat(xmlGregorianCalendar.getDay()).isEqualTo(22);

        assertThat(xmlGregorianCalendar.getHour()).isEqualTo(18); // UTC Winterzeit -1h
        assertThat(xmlGregorianCalendar.getMinute()).isEqualTo(55);
        assertThat(xmlGregorianCalendar.getSecond()).isEqualTo(2);
    }

    @Test
    void unmarshal_sommerzeit_mit_zeitzone() {
        // Act
        XMLGregorianCalendar xmlGregorianCalendar = EUBerlinDateTimeAdapter.unmarshal("2021-07-01T01:00:59+02:00");

        // Assert
        assertThat(xmlGregorianCalendar.getYear()).isEqualTo(2021);
        assertThat(xmlGregorianCalendar.getMonth()).isEqualTo(6);
        assertThat(xmlGregorianCalendar.getDay()).isEqualTo(30);

        assertThat(xmlGregorianCalendar.getHour()).isEqualTo(23); // UTC Winterzeit -2h
        assertThat(xmlGregorianCalendar.getMinute()).isEqualTo(0);
        assertThat(xmlGregorianCalendar.getSecond()).isEqualTo(59);
    }

    @Test
    void marshal_winterzeit() throws Exception {
        // Setup Data
        var utcDateTime = ZonedDateTime.of(LocalDate.of(2021, 3, 22), LocalTime.of(18, 55, 2), ZoneId.of("UTC"));
        GregorianCalendar gregorianCalendar = GregorianCalendar.from(utcDateTime);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance()
                                                                   .newXMLGregorianCalendar(gregorianCalendar);
        // Assert
        assertThat(EUBerlinDateTimeAdapter.marshal(xmlGregorianCalendar)).isEqualTo(
                "2021-03-22T19:55:02+01:00"); // CET Winterzeit +1h

    }

    @Test
    void marshal_sommerzeit() throws Exception {
        // Setup Data
        var utcDateTime = ZonedDateTime.of(LocalDate.of(2021, 6, 30), LocalTime.of(23, 0, 59), ZoneId.of("UTC"));
        GregorianCalendar gregorianCalendar = GregorianCalendar.from(utcDateTime);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance()
                                                                   .newXMLGregorianCalendar(gregorianCalendar);
        // Assert
        assertThat(EUBerlinDateTimeAdapter.marshal(xmlGregorianCalendar)).isEqualTo(
                "2021-07-01T01:00:59+02:00"); // CET Sommerzeit +2h

    }

    @Test
    void marshal_jahreswechsel() throws Exception {
        // Setup Data
        var utcDateTime = ZonedDateTime.of(LocalDate.of(2020, 12, 31), LocalTime.of(23, 30, 0), ZoneId.of("UTC"));
        GregorianCalendar gregorianCalendar = GregorianCalendar.from(utcDateTime);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance()
                                                                   .newXMLGregorianCalendar(gregorianCalendar);
        // Assert
        assertThat(EUBerlinDateTimeAdapter.marshal(xmlGregorianCalendar)).isEqualTo(
                "2021-01-01T00:30:00+01:00"); // CET Winterzeit +1h

    }

}