package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.event.AnsprechpartnerLoeschenTriggerdEvent;

@ExtendWith(MockitoExtension.class)
class AnsprechpartnerLoeschenListenerTest {

    @Mock
    BenutzerJobRegistry jobRegistry;
    @Mock
    private AnsprechpartnerLoeschenJob job;

    private AnsprechpartnerLoeschenListener listener;

    @BeforeEach
    void setUp() {
        listener = new AnsprechpartnerLoeschenListener(jobRegistry, job);
    }

    @Test
    void testHandle() {
        var jahr = aJahrReferenz(2010).build();
        var username = "user";

        var event = new AnsprechpartnerLoeschenTriggerdEvent(jahr, username, Land.DEUTSCHLAND);

        listener.handleAnsprechpartnerLoeschen(event);

        verify(job).runAsync(jahr, username, Land.DEUTSCHLAND);
    }

}
