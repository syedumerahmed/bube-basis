package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aTaetigkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.AnlageDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.AnlagenteilDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.BetreiberInUseException;
import de.wps.bube.basis.stammdaten.domain.BetriebsstaetteDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.InvalidTaetigkeitException;
import de.wps.bube.basis.stammdaten.domain.LocalIdBereitsVerwendetException;
import de.wps.bube.basis.stammdaten.domain.QuelleDuplicateKeyException;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.domain.event.AnlageAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlageGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilAngelegtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnlagenteilGeloeschtEvent;
import de.wps.bube.basis.stammdaten.domain.event.AnsprechpartnerLoeschenTriggerdEvent;
import de.wps.bube.basis.stammdaten.domain.event.StammdatenUebernehmenTriggeredEvent;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.AnlagenteilRepository;
import de.wps.bube.basis.stammdaten.persistence.BetreiberRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.persistence.QuelleRepository;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetriebsstaetteBerechtigungsAdapter;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;

@ExtendWith(MockitoExtension.class)
class StammdatenServiceTest {

    @Mock
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Mock
    private BetreiberRepository betreiberRepository;
    @Mock
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    private BetreiberBerechtigungsAccessor betreiberBerechtigungsAccessor;
    @Mock
    private BetriebsstaettenBerechtigungsAccessor betriebsstaetteBerechtigungsAccessor;
    @Mock
    private AnlageRepository anlageRepository;
    @Mock
    private AnlagenteilRepository anlagenteilRepository;
    @Mock
    private QuelleRepository quelleRepository;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private StammdatenBerichtsjahrwechselMapper referenzenResolverService;
    @Mock
    private AktiverBenutzer bearbeiter;

    private StammdatenService stammdatenService;

    @BeforeEach
    void setUp() {
        lenient().when(stammdatenBerechtigungsContext.createPredicateRead(any())).thenReturn(new BooleanBuilder());
        lenient().when(stammdatenBerechtigungsContext.createPredicateWrite(any())).thenReturn(new BooleanBuilder());
        lenient().when(stammdatenBerechtigungsContext.createPredicateDelete(any())).thenReturn(new BooleanBuilder());
        stammdatenService = new StammdatenService(stammdatenBerechtigungsContext, betriebsstaetteBerechtigungsAccessor,
                betreiberBerechtigungsAccessor, betriebsstaetteRepository, betreiberRepository, anlageRepository,
                anlagenteilRepository, quelleRepository, referenzenResolverService, new AnlagenUpdaterImpl(),
                new BetriebsstaettenUpdaterImpl(), eventPublisher, bearbeiter);
    }

    @Test
    void loadBetriebsstaette_success() {
        // Arrange
        final var betriebstaetteId = 2L;
        Betriebsstaette expectedBetriebsstaette = aBetriebsstaette(10L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(expectedBetriebsstaette));

        // Act
        Betriebsstaette actualBetriebsstaette = stammdatenService.loadBetriebsstaetteForRead(betriebstaetteId);

        // Assert
        assertThat(actualBetriebsstaette).isEqualTo(expectedBetriebsstaette);
    }

    @Test
    void loadBetriebsstaette_entityNotFound() {
        // Arrange
        final var id = 3L;
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadBetriebsstaetteForRead(id));
    }

    @Test
    void findBetriebsstaetteByNummerNameOrt() {
        // Arrange
        var betriebsstaette = aBetriebsstaette().build();
        when(betriebsstaetteRepository.findAll(any(Predicate.class), eq(PageRequest.of(0, 10)))).thenReturn(
                new PageImpl<>(List.of(betriebsstaette)));

        // Act
        List<Betriebsstaette> actual = stammdatenService.findBetriebsstaetteByNummerNameOrtLimitBy10("").getContent();

        // Assert
        assertThat(actual).containsExactly(betriebsstaette);
    }

    @Test
    void createBetriebsstaette_neue_betriebsstaette() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette().build();
        when(betriebsstaetteRepository.save(betriebsstaette)).thenReturn(betriebsstaette);

        // Act
        stammdatenService.createBetriebsstaette(betriebsstaette);

        // Assert
        verify(betriebsstaetteRepository).save(betriebsstaette);
    }

    @Test
    void createBetriebsstaette_neue_betriebsstaette_withLandDeutschland_shouldThrowException() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette().land(Land.DEUTSCHLAND).build();

        // Act + Assert
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.createBetriebsstaette(betriebsstaette));
    }

    @Test
    void createBetriebsstaette_bestehende_betriebsstaette() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette(10L).build();

        // Act + Assert
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.createBetriebsstaette(betriebsstaette));
        verify(betriebsstaetteRepository, never()).save(betriebsstaette);
    }

    @Test
    void createBetriebsstaetteShouldPublishAnlageAngelegtEvent() {
        var betriebsstaette = aBetriebsstaette().anlagen(anAnlage(2L).build(), anAnlage(3L).build()).build();
        when(betriebsstaetteRepository.save(betriebsstaette)).thenReturn(betriebsstaette);

        stammdatenService.createBetriebsstaette(betriebsstaette);

        verify(eventPublisher).publishEvent(new AnlageAngelegtEvent(anAnlage(2L).build()));
        verify(eventPublisher).publishEvent(new AnlageAngelegtEvent(anAnlage(3L).build()));
    }

    @Test
    void createBetriebsstaetteShouldPublishAnlagenteilAngelegtEvent() {
        var betriebsstaette = aBetriebsstaette().anlagen(
                anAnlage(1L).anlagenteile(anAnlagenteil(2L).build(), anAnlagenteil(3L).build()).build()).build();
        when(betriebsstaetteRepository.save(betriebsstaette)).thenReturn(betriebsstaette);

        stammdatenService.createBetriebsstaette(betriebsstaette);

        verify(eventPublisher).publishEvent(new AnlagenteilAngelegtEvent(anAnlagenteil(2L).build()));
        verify(eventPublisher).publishEvent(new AnlagenteilAngelegtEvent(anAnlagenteil(3L).build()));
    }

    @Test
    void updateBetriebsstaette_bestehende_betriebsstaette() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette(10L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        // Act
        stammdatenService.updateBetriebsstaette(betriebsstaette);

        // Assert
        verify(betriebsstaetteRepository).save(betriebsstaette);
    }

    @Test
    void updateBetriebsstaette_bestehende_betriebsstaette_ohne_berechtigung_fuer_neue() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette(10L).build();
        doThrow(AccessDeniedException.class).when(stammdatenBerechtigungsContext)
                                            .checkAccessToWrite(any(BetriebsstaetteBerechtigungsAdapter.class));

        // Act + Assert
        assertThatThrownBy(() -> stammdatenService.updateBetriebsstaette(betriebsstaette)).isInstanceOf(
                AccessDeniedException.class);

        // Assert
        verifyNoInteractions(betriebsstaetteRepository);
    }

    @Test
    void updateBetriebsstaette_bestehende_betriebsstaette_ohne_berechtigung_fuer_bestehende() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette(10L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        // Act + Assert
        assertThatThrownBy(() -> stammdatenService.updateBetriebsstaette(betriebsstaette)).isInstanceOf(
                EntityNotFoundException.class);

        // Assert
        verifyNoMoreInteractions(betriebsstaetteRepository);
    }

    @Test
    void updateBetriebsstaette_neue_betriebsstaette() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette().build();

        // Act + Assert
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.updateBetriebsstaette(betriebsstaette));
        verify(betriebsstaetteRepository, never()).save(betriebsstaette);
    }

    @Test
    void updateBetriebsstaette_entityNotFound() {
        // Arrange
        final var id = 3L;
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadBetriebsstaetteForWrite(id));
    }

    @Test
    void updateBetriebsstaetteForUebernahme() {
        // Arrange
        Betriebsstaette betriebsstaette = aBetriebsstaette(10L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        // Act
        stammdatenService.updateBetriebsstaetteForUebernahme(betriebsstaette);

        // Assert
        verify(betriebsstaetteRepository).save(betriebsstaette);
    }

    @Test
    void deleteBetriebsstaette() {
        // Arrange
        final var id = 20L;
        Betriebsstaette toDelete = aBetriebsstaette(id).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(toDelete));

        // Act
        stammdatenService.deleteBetriebsstaette(id);

        // Assert
        verify(betriebsstaetteRepository).delete(toDelete);
    }

    @Test
    void loadBetreiberByJahrLandNummer_success() {
        // Arrange
        var jahr = aJahrReferenz(2010).build();
        final var nummer = "nummer";
        Betreiber expectedBetreiber = aBetreiber(2L).jahr(jahr).land(Land.BERLIN).betreiberNummer(nummer).build();
        when(betreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(expectedBetreiber));

        // Act
        Betreiber actualBetreiber = stammdatenService.loadBetreiberByJahrLandNummer(jahr.getSchluessel(), Land.BERLIN,
                nummer);

        // Assert
        assertThat(actualBetreiber).isEqualTo(expectedBetreiber);
    }

    @Test
    void loadBetreiberByJahrLandNummer_empty() {
        // Arrange
        when(betreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(BubeEntityNotFoundException.class,
                () -> stammdatenService.loadBetreiberByJahrLandNummer("02", Land.BAYERN, "nummer"));
    }

    @Test
    void createBetreiber_neuer_betreiber() {
        // Arrange
        var betriebsstaette = aBetriebsstaette().id(1L).betriebsstaetteNr("11-23").build();
        var betreiber = aBetreiber().build();
        var savedBetreiber = aBetreiber(2L).jahr(aJahrReferenz(2020).build()).build();

        when(betreiberRepository.save(betreiber)).thenReturn(savedBetreiber);
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        // Act
        stammdatenService.createBetreiberZuBetriebsstaette(betreiber, "11-23");

        // Assert
        verify(betreiberRepository).save(savedBetreiber);
    }

    @Test
    void createBetreiber_isCorrectlyAssociatedToBetriebsstaette() {
        var betriebsstaette = aBetriebsstaette().id(1L).betriebsstaetteNr("11-23").betreiber(null).build();
        var betreiber = aBetreiber().build();
        var savedBetreiber = aBetreiber(2L).jahr(aJahrReferenz(2019).build()).build();

        when(betreiberRepository.save(betreiber)).thenReturn(savedBetreiber);
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        stammdatenService.createBetreiberZuBetriebsstaette(betreiber, "11-23");

        assertThat(betriebsstaette.getBetreiber()).isEqualTo(savedBetreiber);
    }

    @Test
    void createBetreiber_bestehender_betreiber() {
        // Arrange
        Betreiber betreiber = aBetreiber().id(5L).build();

        // Act + Assert
        assertThrows(IllegalArgumentException.class,
                () -> stammdatenService.createBetreiberZuBetriebsstaette(betreiber, ""));
        verify(betreiberRepository, never()).save(betreiber);
    }

    @Test
    void updateBetreiber_bestehender_betreiber() {
        // Arrange
        Betreiber betreiber = aBetreiber().id(5L).build();
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(true);

        // Act
        stammdatenService.updateBetreiber(betreiber);

        // Assert
        verify(betreiberRepository).save(betreiber);
    }

    @Test
    void updateBetreiber_neuer_betreiber() {
        // Arrange
        Betreiber betreiber = aBetreiber().build();

        // Act + Assert
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.updateBetreiber(betreiber));
        verify(betreiberRepository, never()).save(betreiber);
    }

    @Test
    void updateBetreiber_keineBerechtigung() {
        var b = aBetreiber(2L).build();
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(false);

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.updateBetreiber(b));
    }

    @Test
    void deleteBetreiber() {
        // Arrange
        var betreiber = aBetreiber(20L).build();
        var betriebsstaette = aBetriebsstaette().id(10L).betriebsstaetteNr("BST10").betreiber(betreiber).build();
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(true);
        when(betriebsstaetteRepository.countAllByBetreiberId(any())).thenReturn(1L);
        when(betriebsstaetteRepository.findByBetreiberId(20L)).thenReturn(Stream.of(betriebsstaette));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenAnswer(
                invocation -> "betriebsstaette.id = 10".equals(invocation.getArgument(0).toString())
                        ? optionalBst() : Optional.empty()
        );

        // Act
        stammdatenService.deleteBetreiber(20L);

        // Assert
        verify(betreiberRepository).deleteById(20L);
        verifyNoMoreInteractions(betreiberRepository);
        verify(betriebsstaetteRepository).save(betriebsstaette);
        verifyNoMoreInteractions(betriebsstaetteRepository);
    }

    @Test
    void deleteBetreiberUsedByMultipleBetriebsstaettenShouldThrowException() {
        // Arrange
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(true);
        when(betriebsstaetteRepository.countAllByBetreiberId(20L)).thenReturn(2L);

        // Act + Assert
        assertThrows(BetreiberInUseException.class, () -> stammdatenService.deleteBetreiber(20L));
        verify(betreiberRepository, never()).deleteById(20L);
    }

    @Test
    void deleteBetreiberShouldRemoveBetreiberFromBetriebsstaetteBeforeDeletingBetreiber() {
        Betriebsstaette betriebsstaette = mock(Betriebsstaette.class);
        // Arrange
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(true);
        when(betriebsstaetteRepository.countAllByBetreiberId(any())).thenReturn(1L);
        when(betriebsstaetteRepository.findByBetreiberId(any())).thenReturn(Stream.of(betriebsstaette));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());

        // Act
        stammdatenService.deleteBetreiber(20L);

        // Assert
        InOrder inOrder = inOrder(betriebsstaette, betriebsstaetteRepository, betreiberRepository);
        inOrder.verify(betriebsstaette).setBetreiber(null);
        inOrder.verify(betriebsstaetteRepository).save(betriebsstaette);
        inOrder.verify(betreiberRepository).deleteById(20L);
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteBetreiberIfBetreiberNoLongerInUse() {
        // Arrange
        var betreiberId = 2L;
        var betriebsstaetteId = 1L;
        var betreiber = aBetreiber(betreiberId).build();
        var betriebsstaette = aBetriebsstaette(betriebsstaetteId).betreiber(betreiber).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(betriebsstaetteRepository.countAllByBetreiberId(betreiberId)).thenReturn(0L);

        // Act
        stammdatenService.deleteBetriebsstaette(betriebsstaetteId);

        // Assert
        var inOrder = inOrder(betriebsstaetteRepository, betreiberRepository);
        inOrder.verify(betriebsstaetteRepository).delete(betriebsstaette);
        inOrder.verify(betreiberRepository).delete(betreiber);
    }

    @Test
    void deleteBetriebsstaetteShouldNotDeleteBetreiberIfBetreiberStillInUse() {
        // Arrange
        var betreiberId = 2L;
        var betriebsstaetteId = 1L;
        var betreiber = aBetreiber(betreiberId).build();
        var betriebsstaette = aBetriebsstaette(betriebsstaetteId).betreiber(betreiber).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(betriebsstaetteRepository.countAllByBetreiberId(betreiberId)).thenReturn(2L);

        // Act
        stammdatenService.deleteBetriebsstaette(betriebsstaetteId);

        // Assert
        verify(betreiberRepository, never()).delete(betreiber);
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteAllAnlagen() {
        Anlage anlage2 = anAnlage(2L).build();
        Anlage anlage3 = anAnlage(3L).build();
        var betriebsstaette = aBetriebsstaette(1L).anlagen(anlage2, anlage3).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(anlageRepository.findById(anlage2.getId())).thenReturn(Optional.of(anlage2));
        when(anlageRepository.findById(anlage3.getId())).thenReturn(Optional.of(anlage3));

        stammdatenService.deleteBetriebsstaette(1L);

        assertThat(anlageRepository.findAllById(List.of(2L, 3L))).isEmpty();
    }

    @Test
    void deleteBetriebsstaetteShouldPublishAnlageGeloeschtEvent() {
        Anlage anlage2 = anAnlage(2L).build();
        Anlage anlage3 = anAnlage(3L).build();
        var betriebsstaette = aBetriebsstaette(1L).anlagen(anlage2, anlage3).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(anlageRepository.findById(anlage2.getId())).thenReturn(Optional.of(anlage2));
        when(anlageRepository.findById(anlage3.getId())).thenReturn(Optional.of(anlage3));


        stammdatenService.deleteBetriebsstaette(1L);

        verify(eventPublisher).publishEvent(new AnlageGeloeschtEvent(2L, 1L));
        verify(eventPublisher).publishEvent(new AnlageGeloeschtEvent(3L, 1L));
    }

    @Test
    void deleteBetriebsstaetteShouldPublishAnlagenteilGeloeschtEvent() {
        Anlage anlage = anAnlage(1L).anlagenteile(anAnlagenteil(2L).build(), anAnlagenteil(3L).build()).build();
        var betriebsstaette = aBetriebsstaette(1L).anlagen(
                anlage).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(anlageRepository.findById(anlage.getId())).thenReturn(Optional.of(anlage));

        stammdatenService.deleteBetriebsstaette(1L);

        verify(eventPublisher).publishEvent(new AnlagenteilGeloeschtEvent(2L, 1L));
        verify(eventPublisher).publishEvent(new AnlagenteilGeloeschtEvent(3L, 1L));
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteAllAnlagenteile() {
        Anlage anlage = anAnlage(2L).anlagenteile(anAnlagenteil(2L).build(), anAnlagenteil(3L).build()).build();
        var betriebsstaette = aBetriebsstaette(1L).anlagen(
               anlage).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(anlageRepository.findById(anlage.getId())).thenReturn(Optional.of(anlage));

        stammdatenService.deleteBetriebsstaette(1L);

        assertThat(anlagenteilRepository.findAllById(List.of(2L, 3L))).isEmpty();
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteAllAnlageQuellen() {
        Anlage anlage = anAnlage(1L).quellen(aQuelle(2L).build(), aQuelle(3L).build()).build();
        var betriebsstaette = aBetriebsstaette(1L).anlagen(
                anlage).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(anlageRepository.findById(anlage.getId())).thenReturn(Optional.of(anlage));

        stammdatenService.deleteBetriebsstaette(1L);

        assertThat(quelleRepository.findAllById(List.of(2L, 3L))).isEmpty();
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteAllBetriebsstaetteQuellen() {
        var betriebsstaette = aBetriebsstaette(1L).quellen(aQuelle(2L).build(), aQuelle(3L).build()).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        stammdatenService.deleteBetriebsstaette(1L);

        assertThat(quelleRepository.findAllById(List.of(2L, 3L))).isEmpty();
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteBetriebsstaette() {
        var betriebsstaette = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));

        stammdatenService.deleteBetriebsstaette(1L);

        verify(betriebsstaetteRepository).delete(betriebsstaette);
    }

    @Test
    void deleteBetriebsstaetteShouldDeleteBetreiberIfBetreiberIsOrphan() {
        var betreiber = aBetreiber(1L).build();
        var betriebsstaette = aBetriebsstaette(1L).betreiber(betreiber).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(betriebsstaetteRepository.countAllByBetreiberId(1L)).thenReturn(0L);
        stammdatenService.deleteBetriebsstaette(1L);

        verify(betreiberRepository).delete(betreiber);
    }

    @Test
    void deleteBetriebsstaetteShouldNotDeleteBetreiberIfBetreiberIsNoOrphan() {
        var betriebsstaette = aBetriebsstaette(1L).betreiber(aBetreiber(1L).build()).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(betriebsstaette));
        when(betriebsstaetteRepository.countAllByBetreiberId(1L)).thenReturn(2L);

        stammdatenService.deleteBetriebsstaette(1L);

        verify(betreiberRepository, never()).deleteById(1L);
    }

    @Test
    void createBetriebsstaette_duplicate_functionalKey_shouldThrow() {
        var b = aBetriebsstaette().build();
        when(betriebsstaetteRepository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(b.getBerichtsjahr().getId(),
                b.getLand(), b.getBetriebsstaetteNr())).thenReturn(true);

        assertThrows(BetriebsstaetteDuplicateKeyException.class, () -> stammdatenService.createBetriebsstaette(b));
    }

    @Test
    void createBetriebsstaette_localId_schonVergeben() {
        final var localId = "localId";
        var b = aBetriebsstaette().localId(localId).build();
        when(betriebsstaetteRepository.localIdExists(localId, b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(true);

        assertThrows(LocalIdBereitsVerwendetException.class, () -> stammdatenService.createBetriebsstaette(b));
    }

    @Test
    void createBetriebsstaette_localId_nochNichtVergeben() {
        final var localId = "localId";
        var b = aBetriebsstaette().localId(localId).build();
        when(betriebsstaetteRepository.localIdExists(localId, b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(false);
        when(betriebsstaetteRepository.save(b)).thenReturn(b);
        stammdatenService.createBetriebsstaette(b);

        verify(betriebsstaetteRepository).save(b);
    }

    @Test
    void updateBetriebsstaette_duplicate_functionalKey_shouldThrow() {
        var existingBetriebsstaette = aBetriebsstaette(1L).build();
        var toBeUpdated = aBetriebsstaette(1L).betriebsstaetteNr("neueNummer").build();

        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(existingBetriebsstaette));
        when(betriebsstaetteRepository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(
                toBeUpdated.getBerichtsjahr().getId(), toBeUpdated.getLand(),
                toBeUpdated.getBetriebsstaetteNr())).thenReturn(true);

        assertThrows(BetriebsstaetteDuplicateKeyException.class,
                () -> stammdatenService.updateBetriebsstaette(toBeUpdated));
    }

    @Test
    void updateBetriebsstaette_withoutKeyChange_shouldBeAllowed() {
        var existingBetriebsstaette = aBetriebsstaette(1L).build();
        var toBeUpdated = aBetriebsstaette(1L).akz("anderes AKZ").build();

        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(existingBetriebsstaette));

        stammdatenService.updateBetriebsstaette(toBeUpdated);

        verify(betriebsstaetteRepository).save(toBeUpdated);
    }

    @Test
    void updateBetriebsstaette_localId_notChanged() {
        final var localId = "localId";
        var b = aBetriebsstaette(2L).localId(localId).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));

        stammdatenService.updateBetriebsstaette(b);

        verify(betriebsstaetteRepository).save(b);
    }

    @Test
    void updateBetriebsstaette_localId_schonVergeben() {
        var b = aBetriebsstaette(1L).build();
        var toBeUpdated = aBetriebsstaette(1L).localId("123").build();

        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(betriebsstaetteRepository.localIdExists("123", b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(true);

        assertThrows(LocalIdBereitsVerwendetException.class,
                () -> stammdatenService.updateBetriebsstaette(toBeUpdated));
    }

    @Test
    void updateBetriebsstaette_localId_nochNichtVergeben() {
        var b = aBetriebsstaette(1L).localId("444").build();
        var toBeUpdated = aBetriebsstaette(1L).localId("123").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(betriebsstaetteRepository.localIdExists("123", b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(false);

        stammdatenService.updateBetriebsstaette(toBeUpdated);

        verify(betriebsstaetteRepository).save(toBeUpdated);
    }

    @Test
    void updateBetriebsstaette_localId_nochNichtVergeben_fromNull() {
        var b = aBetriebsstaette(1L).localId(null).build();
        var toBeUpdated = aBetriebsstaette(1L).localId("123").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(betriebsstaetteRepository.localIdExists("123", b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(false);

        stammdatenService.updateBetriebsstaette(toBeUpdated);

        verify(betriebsstaetteRepository).save(toBeUpdated);
    }

    @Test
    void updateBetriebsstaette_localId_setToNull() {
        var b = aBetriebsstaette(1L).localId("444").build();
        var toBeUpdated = aBetriebsstaette(1L).localId(null).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));

        stammdatenService.updateBetriebsstaette(toBeUpdated);

        verify(betriebsstaetteRepository).save(toBeUpdated);
    }

    @Test
    void createAnlage_neueAnlage() {
        var a = anAnlage().parentBetriebsstaetteId(2L).anlageNr("nummer").build();
        var b = aBetriebsstaette(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(2L, "nummer")).thenReturn(false);

        stammdatenService.createAnlage(a);

        verify(anlageRepository).saveAndFlush(a);
    }

    @Test
    void createAnlage_ohneBerechtigung() {
        var a = anAnlage().parentBetriebsstaetteId(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_bestehendeAnlage() {
        var a = anAnlage(2L).build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_functionalKey_alreadyExists() {
        var a = anAnlage().parentBetriebsstaetteId(2L).anlageNr("nummer").build();
        var b = aBetriebsstaette(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(2L, "nummer")).thenReturn(true);

        assertThrows(AnlageDuplicateKeyException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_functionalKey_alreadyExistsInSpb() {
        var a = anAnlage().parentBetriebsstaetteId(2L).anlageNr("nummer").build();
        var b = aBetriebsstaette(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(2L, "nummer")).thenReturn(true);

        assertThrows(AnlageDuplicateKeyException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_localId_schonVergeben() {
        final var localId = "localId";
        var a = anAnlage().parentBetriebsstaetteId(1L).localId(localId).anlageNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(false);
        when(betriebsstaetteRepository.localIdExists(localId, b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(true);

        assertThrows(LocalIdBereitsVerwendetException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_taetigkeiten_valid() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg("R4BV").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build()))
                                                .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build())
                                                .build(),
                aVorschrift().art(aVorschriftReferenz("03").strg("RPRTR").build())
                             .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                             .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build())
                             .build(), aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                    .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RIET,
                                                            "").build()))
                                                    .haupttaetigkeit(
                                                            aTaetigkeitsReferenz(Referenzliste.RIET, "").build())
                                                    .build());
        var a = anAnlage().parentBetriebsstaetteId(1L).vorschriften(vorschriften).anlageNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(false);

        stammdatenService.createAnlage(a);

        verify(anlageRepository).saveAndFlush(a);
    }

    @Test
    void createAnlage_taetigkeiten_keineTaetigkeiten() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").strg(null).build()).build(),
                aVorschrift().art(aVorschriftReferenz("02").build()).build(),
                aVorschrift().art(aVorschriftReferenz("RIET").build()).build());
        var a = anAnlage().parentBetriebsstaetteId(1L).vorschriften(vorschriften).anlageNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(false);

        stammdatenService.createAnlage(a);

        verify(anlageRepository).saveAndFlush(a);
    }

    @Test
    void createAnlage_taetigkeiten_keineTaetigkeitenErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg(null).build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RIET, "").build()))
                                                .build());
        var a = anAnlage().parentBetriebsstaetteId(1L).vorschriften(vorschriften).anlageNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(false);

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void createAnlage_taetigkeiten_taetigkeitenNichtErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                                                .build());
        var a = anAnlage().parentBetriebsstaetteId(1L).vorschriften(vorschriften).anlageNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(false);

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.createAnlage(a));
    }

    @Test
    void updateAnlage_bestehendeAnlage() {
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L).bemerkung("hi").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlage(a);

        verify(anlageRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlage_neueAnlage() {
        var toBeUpdated = anAnlage().build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_ohneBerechtigung() {
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L).bemerkung("hi").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_duplicateKey() {
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).anlageNr("hallo").build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L).anlageNr("moin").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "moin")).thenReturn(true);

        assertThrows(AnlageDuplicateKeyException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_localId_schonVergeben() {
        var a = anAnlage(1L).parentBetriebsstaetteId(234L).anlageNr("moin").build();
        var toBeUpdated = anAnlage(1L).anlageNr("moin").parentBetriebsstaetteId(1L).localId("123").build();
        var b = aBetriebsstaette(1L).build();

        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(1L)).thenReturn(Optional.of(a));
        when(betriebsstaetteRepository.localIdExists("123", b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(true);
        when(anlageRepository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "moin")).thenReturn(false);

        assertThrows(LocalIdBereitsVerwendetException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_entityNotFound() {
        var toBeUpdated = anAnlage(5L).parentBetriebsstaetteId(1235L).build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(5L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_taetigkeiten_valid() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg("R4BV").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build()))
                                                .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build())
                                                .build(),
                aVorschrift().art(aVorschriftReferenz("03").strg("RPRTR").build())
                             .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                             .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build())
                             .build(), aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                    .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RIET,
                                                            "").build()))
                                                    .haupttaetigkeit(
                                                            aTaetigkeitsReferenz(Referenzliste.RIET, "").build())
                                                    .build());
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L)
                                      .vorschriften(vorschriften)
                                      .anlageNr("nummer")
                                      .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlage(toBeUpdated);

        verify(anlageRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlage_taetigkeiten_keineTaetigkeiten() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").build()).build(),
                aVorschrift().art(aVorschriftReferenz("02").build()).build(),
                aVorschrift().art(aVorschriftReferenz("RIET").build()).build());
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L)
                                      .vorschriften(vorschriften)
                                      .anlageNr("nummer")
                                      .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlage(toBeUpdated);

        verify(anlageRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlage_taetigkeiten_keineTaetigkeitenErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg(null).build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RIET, "").build()))
                                                .build());
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L)
                                      .vorschriften(vorschriften)
                                      .anlageNr("nummer")
                                      .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void updateAnlage_taetigkeiten_taetigkeitenNichtErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                                                .build());
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = anAnlage(2L).parentBetriebsstaetteId(1L)
                                      .vorschriften(vorschriften)
                                      .anlageNr("nummer")
                                      .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(a));

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.updateAnlage(toBeUpdated));
    }

    @Test
    void loadAnlage_existing() {
        var a = anAnlage(2L).parentBetriebsstaetteId(1L).anlageNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(anlageRepository.findByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(Optional.of(a));

        assertEquals(a, stammdatenService.loadAnlageByAnlageNummer(1L, "nummer"));
    }

    @Test
    void loadAnlage_keineBerechtigung() {
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadAnlageByAnlageNummer(2L, ""));
    }

    @Test
    void loadAnlage_notFound() {
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(anlageRepository.findByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(1L, "nummer")).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadAnlageByAnlageNummer(1L, "nummer"));
    }

    @Test
    void deleteAnlage() {
        var a = anAnlage(3L).parentBetriebsstaetteId(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(anlageRepository.findById(3L)).thenReturn(Optional.of(a));

        stammdatenService.deleteAnlage(3L);

        verify(anlageRepository).deleteById(3L);
    }

    @Test
    void deleteAnlage_keineBerechtigung() {
        var a = anAnlage(3L).parentBetriebsstaetteId(123L).build();
        when(anlageRepository.findById(3L)).thenReturn(Optional.of(a));

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.deleteAnlage(3L));
    }

    @Test
    void deleteAnlage_notFound() {
        when(anlageRepository.findById(3L)).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.deleteAnlage(3L));
    }

    @Test
    void createAnlagenteil_neuesAnlagenteil() {
        var a = anAnlagenteil().parentAnlageId(2L).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(2L, "nummer")).thenReturn(false);

        stammdatenService.createAnlagenteil(b.getId(), a);

        verify(anlagenteilRepository).saveAndFlush(a);
    }

    @Test
    void createAnlagenteil_ohneBerechtigung() {
        var a = anAnlagenteil().build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.createAnlagenteil(1L, a));
    }

    @Test
    void createAnlagenteil_bestehendesAnlagenteil() {
        var a = anAnlagenteil(2L).build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.createAnlagenteil(1L, a));
    }

    @Test
    void createAnlagenteil_functionalKey_alreadyExists() {
        var a = anAnlagenteil().parentAnlageId(2L).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(2L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(2L, "nummer")).thenReturn(true);

        assertThrows(AnlagenteilDuplicateKeyException.class, () -> stammdatenService.createAnlagenteil(b.getId(), a));
    }

    @Test
    void createAnlagenteil_localId_schonVergeben() {
        final var localId = "localId";
        var a = anAnlagenteil().parentAnlageId(1L).localId(localId).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(false);
        when(betriebsstaetteRepository.localIdExists(localId, b.getLand().getNr(),
                b.getBerichtsjahr().getId())).thenReturn(true);

        assertThrows(LocalIdBereitsVerwendetException.class, () -> stammdatenService.createAnlagenteil(b.getId(), a));
    }

    @Test
    void createAnlagenteil_taetigkeiten_valid() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg("R4BV").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build()))
                                                .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build())
                                                .build(),
                aVorschrift().art(aVorschriftReferenz("03").strg("RPRTR").build())
                             .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                             .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build())
                             .build(), aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                    .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RIET,
                                                            "").build()))
                                                    .haupttaetigkeit(
                                                            aTaetigkeitsReferenz(Referenzliste.RIET, "").build())
                                                    .build());
        var a = anAnlagenteil().parentAnlageId(1L).vorschriften(vorschriften).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(false);

        stammdatenService.createAnlagenteil(b.getId(), a);

        verify(anlagenteilRepository).saveAndFlush(a);
    }

    @Test
    void createAnlagenteil_taetigkeiten_keineTaetigkeiten() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").build()).build(),
                aVorschrift().art(aVorschriftReferenz("02").build()).build(),
                aVorschrift().art(aVorschriftReferenz("RIET").build()).build());
        var a = anAnlagenteil().parentAnlageId(1L).vorschriften(vorschriften).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(false);

        stammdatenService.createAnlagenteil(b.getId(), a);

        verify(anlagenteilRepository).saveAndFlush(a);
    }

    @Test
    void createAnlagenteil_taetigkeiten_keineTaetigkeitenErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg(null).build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RIET, "").build()))
                                                .build());
        var a = anAnlagenteil().parentAnlageId(1L).vorschriften(vorschriften).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(false);

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.createAnlagenteil(b.getId(), a));
    }

    @Test
    void createAnlagenteil_taetigkeiten_taetigkeitenNichtErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                                                .build());
        var a = anAnlagenteil().parentAnlageId(1L).vorschriften(vorschriften).anlagenteilNr("nummer").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(false);

        assertThrows(InvalidTaetigkeitException.class, () -> stammdatenService.createAnlagenteil(b.getId(), a));
    }

    @Test
    void updateAnlagenteil_bestehendesAnlagenteil() {
        var a = anAnlagenteil(2L).parentAnlageId(1L).build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L).bemerkung("hi").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlagenteil(b.getId(), a);

        verify(anlagenteilRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlagenteil_neuesAnlagenteil() {
        var toBeUpdated = anAnlagenteil().build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.updateAnlagenteil(1L, toBeUpdated));
    }

    @Test
    void updateAnlagenteil_ohneBerechtigung() {
        var toBeUpdated = anAnlagenteil(2L).bemerkung("hi").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.updateAnlagenteil(1L, toBeUpdated));
    }

    @Test
    void updateAnlagenteil_duplicateKey() {
        var a = anAnlagenteil(2L).parentAnlageId(1L).anlagenteilNr("hallo").build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L).anlagenteilNr("moin").build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));
        when(anlagenteilRepository.existsByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "moin")).thenReturn(true);

        assertThrows(AnlagenteilDuplicateKeyException.class,
                () -> stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated));
    }

    @Test
    void updateAnlagenteil_entityNotFound() {
        var toBeUpdated = anAnlagenteil(5L).build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(5L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated));
    }

    @Test
    void loadAnlagenteil_existing() {
        var a = anAnlagenteil(2L).anlagenteilNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(anlagenteilRepository.findByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(Optional.of(a));

        assertEquals(a, stammdatenService.loadAnlagenteilByAnlagenteilNummer(1L, 1L, "nummer"));
    }

    @Test
    void updateAnlagenteil_taetigkeiten_valid() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg("R4BV").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build()))
                                                .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.R4BV, "").build())
                                                .build(),
                aVorschrift().art(aVorschriftReferenz("03").strg("RPRTR").build())
                             .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                             .haupttaetigkeit(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build())
                             .build(), aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                    .taetigkeiten(List.of(aTaetigkeitsReferenz(Referenzliste.RIET,
                                                            "").build()))
                                                    .haupttaetigkeit(
                                                            aTaetigkeitsReferenz(Referenzliste.RIET, "").build())
                                                    .build());
        var a = anAnlagenteil(2L).anlagenteilNr("nummer").build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L)
                                           .vorschriften(vorschriften)
                                           .anlagenteilNr("nummer")
                                           .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated);

        verify(anlagenteilRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlagenteil_taetigkeiten_keineTaetigkeiten() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").build()).build(),
                aVorschrift().art(aVorschriftReferenz("02").build()).build(),
                aVorschrift().art(aVorschriftReferenz("RIET").build()).build());
        var a = anAnlagenteil(2L).anlagenteilNr("nummer").build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L)
                                           .vorschriften(vorschriften)
                                           .anlagenteilNr("nummer")
                                           .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));

        stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated);

        verify(anlagenteilRepository).save(toBeUpdated);
    }

    @Test
    void updateAnlagenteil_taetigkeiten_keineTaetigkeitenErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("02").strg(null).build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RIET, "").build()))
                                                .build());
        var a = anAnlagenteil(2L).anlagenteilNr("nummer").build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L)
                                           .vorschriften(vorschriften)
                                           .anlagenteilNr("nummer")
                                           .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));

        assertThrows(InvalidTaetigkeitException.class,
                () -> stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated));
    }

    @Test
    void updateAnlagenteil_taetigkeiten_taetigkeitenNichtErlaubt() {
        var vorschriften = List.of(aVorschrift().art(aVorschriftReferenz("01").strg("RIET").build())
                                                .taetigkeiten(
                                                        List.of(aTaetigkeitsReferenz(Referenzliste.RPRTR, "").build()))
                                                .build());
        var a = anAnlagenteil(2L).anlagenteilNr("nummer").build();
        var toBeUpdated = anAnlagenteil(2L).parentAnlageId(1L)
                                           .vorschriften(vorschriften)
                                           .anlagenteilNr("nummer")
                                           .build();
        var b = aBetriebsstaette(1L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(b));
        when(anlagenteilRepository.findById(2L)).thenReturn(Optional.of(a));

        assertThrows(InvalidTaetigkeitException.class,
                () -> stammdatenService.updateAnlagenteil(b.getId(), toBeUpdated));
    }

    @Test
    void loadAnlagenteil_keineBerechtigung() {
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadAnlagenteilByAnlagenteilNummer(1L, 1L, "nummer"));
    }

    @Test
    void loadAnlagenteil_notFound() {
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(anlagenteilRepository.findByParentAnlageIdAndAnlagenteilNrIgnoreCase(1L, "nummer")).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadAnlagenteilByAnlagenteilNummer(1L, 1L, "nummer"));
    }

    @Test
    void deleteAnlagenteil() {
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());

        stammdatenService.deleteAnlagenteil(1L, 3L);

        verify(anlagenteilRepository).deleteById(3L);
    }

    @Test
    void deleteAnlagenteil_keineBerechtigung() {
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.deleteAnlagenteil(1L, 3L));
    }

    @Test
    void createQuelle_neueQuelle() {
        var q = aQuelle().parentBetriebsstaetteId(2L).quelleNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(2L, "nummer")).thenReturn(false);

        stammdatenService.createQuelle(q);

        verify(quelleRepository).saveAndFlush(q);
    }

    @Test
    void createQuelle_ohneBerechtigung() {
        var q = aQuelle().parentBetriebsstaetteId(1L).build();

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.createQuelle(q));
    }

    @Test
    void createQuelle_bestehendeQuelle() {
        var q = aQuelle(2L).build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.createQuelle(q));
    }

    @Test
    void createQuelle_functionalKey_alreadyExists_forAnlage() {
        var q = aQuelle().parentAnlageId(1L).parentBetriebsstaetteId(2L).quelleNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCase(1L, "nummer")).thenReturn(true);
        when(anlageRepository.findById(1L)).thenReturn(Optional.of(anAnlage(1L).parentBetriebsstaetteId(2L).build()));

        assertThrows(QuelleDuplicateKeyException.class, () -> stammdatenService.createQuelle(q));
    }

    @Test
    void createQuelle_functionalKey_alreadyExists_forBetriebsstaette() {
        var q = aQuelle().parentAnlageId(null).parentBetriebsstaetteId(2L).quelleNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(2L, "nummer")).thenReturn(true);

        assertThrows(QuelleDuplicateKeyException.class, () -> stammdatenService.createQuelle(q));
    }

    @Test
    void updateQuelle_bestehendeQuelle() {
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = aQuelle(2L).parentBetriebsstaetteId(1L).bemerkung("hi").build();
        when(quelleRepository.findById(2L)).thenReturn(Optional.of(q));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());

        stammdatenService.updateQuelle(toBeUpdated);

        verify(quelleRepository).save(toBeUpdated);
    }

    @Test
    void updateQuelle_neueQuelle() {
        var toBeUpdated = aQuelle().build();
        assertThrows(IllegalArgumentException.class, () -> stammdatenService.updateQuelle(toBeUpdated));
    }

    @Test
    void updateQuelle_ohneBerechtigung() {
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).build();
        var toBeUpdated = aQuelle(2L).parentBetriebsstaetteId(1L).bemerkung("hi").build();

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.updateQuelle(toBeUpdated));
    }

    @Test
    void updateQuelle_duplicateKey_Betriebsstaette() {
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).quelleNr("hallo").build();
        var toBeUpdated = aQuelle(2L).parentBetriebsstaetteId(1L).quelleNr("moin").build();
        when(quelleRepository.findById(2L)).thenReturn(Optional.of(q));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(1L, "moin")).thenReturn(true);

        assertThrows(QuelleDuplicateKeyException.class, () -> stammdatenService.updateQuelle(toBeUpdated));
    }

    @Test
    void updateQuelle_duplicateKey_Anlage() {
        Anlage anlage = anAnlage(2L).parentBetriebsstaetteId(1L).build();
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).parentAnlageId(anlage.getId()).quelleNr("hallo").build();
        var toBeUpdated = aQuelle(2L).parentBetriebsstaetteId(1L).parentAnlageId(2L).quelleNr("moin").build();
        when(quelleRepository.findById(2L)).thenReturn(Optional.of(q));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.existsByParentAnlageIdAndQuelleNrIgnoreCase(2L, "moin")).thenReturn(true);
        when(anlageRepository.findById(2L)).thenReturn(Optional.of(anlage));

        assertThrows(QuelleDuplicateKeyException.class, () -> stammdatenService.updateQuelle(toBeUpdated));
    }

    @Test
    void updateQuelle_entityNotFound() {
        var toBeUpdated = aQuelle(5L).parentBetriebsstaetteId(123L).build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.findById(5L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> stammdatenService.updateQuelle(toBeUpdated));
    }

    @Test
    void loadQuelle_existing() {
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).quelleNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(1L, "nummer")).thenReturn(Optional.of(q));

        assertEquals(q, stammdatenService.loadQuelleByQuelleNummer(1L, "nummer"));
    }

    @Test
    void loadQuelle_keineBerechtigung() {
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadQuelleByQuelleNummer(2L, ""));
    }

    @Test
    void loadQuelle_notFound() {
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(1L, "nummer")).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadQuelleByQuelleNummer(1L, "nummer"));
    }

    @Test
    void loadQuelleOfAnlage_existing() {
        var q = aQuelle(2L).parentBetriebsstaetteId(1L).parentAnlageId(5L).quelleNr("nummer").build();
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());
        when(quelleRepository.findByParentAnlageIdAndQuelleNrIgnoreCase(5L, "nummer")).thenReturn(Optional.of(q));
        when(anlageRepository.findById(5L)).thenReturn(Optional.of(new Anlage("15", 15L, "name")));

        assertEquals(q, stammdatenService.loadQuelleOfAnlage(5L, "nummer"));
    }

    @Test
    void loadQuelleOfAnlage_keineBerechtigung() {
        var q = aQuelle(3L).parentBetriebsstaetteId(2L).parentAnlageId(123L).quelleNr("nummer").build();

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadQuelleOfAnlage(123L, "nummer"));
    }

    @Test
    void loadQuelleOfAnlage_notFound() {
        when(quelleRepository.findByParentAnlageIdAndQuelleNrIgnoreCase(22L, "nummer")).thenReturn(Optional.empty());
        when(anlageRepository.findById(22L)).thenReturn(Optional.of(new Anlage("15", 15L, "name")));
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());

        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.loadQuelleOfAnlage(22L, "nummer"));
    }

    @Test
    void delete_quelle() {
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(optionalBst());

        stammdatenService.deleteQuelle(2L, 3L);

        verify(quelleRepository).deleteById(3L);
    }

    @Test
    void delete_quelle_keineBerechtigung() {
        assertThrows(BubeEntityNotFoundException.class, () -> stammdatenService.deleteQuelle(2L, 3L));
    }

    @Test
    void uebernimmBetriebsstaetten_keineBerechtigung() {
        List<Long> bIds = List.of(1L, 2L, 3L, 4L, 5L, 6L);
        when(betriebsstaetteRepository.count(any(Predicate.class))).thenReturn((long) (bIds.size() - 1));

        assertThrows(BubeEntityNotFoundException.class,
                () -> stammdatenService.uebernimmBetriebsstaetten(bIds, null));
    }

    @Test
    void uebernimmBetriebsstaetten() {
        var bIds = List.of(1L, 2L, 3L, 4L, 5L, 6L);
        var zielJahr = aJahrReferenz(2019).build();
        final var username = "user";
        when(betriebsstaetteRepository.count(any(Predicate.class))).thenReturn((long) bIds.size());

        stammdatenService.uebernimmBetriebsstaetten(bIds, zielJahr);

        verify(eventPublisher).publishEvent(any(StammdatenUebernehmenTriggeredEvent.class));
    }

    @Test
    void copyBetriebsstaette_betreiberDoesNotExist_inZieljahr() {
        var jahr = aJahrReferenz(2019).build();
        var zieljahr = aJahrReferenz(2020).build();
        var betreiberBuilder = aBetreiber(44L).jahr(jahr).land(Land.BAYERN).betreiberNummer("betreiber");
        var oldBetreiber = betreiberBuilder.build();
        var newBetreiber = betreiberBuilder.jahr(zieljahr).id(null).build();
        var builder = aBetriebsstaette(1L).berichtsjahr(jahr)
                                          .land(Land.BAYERN)
                                          .betriebsstaetteNr("nummer")
                                          .betreiber(oldBetreiber);
        var originalBetriebsstaette = builder.build();

        when(betreiberRepository.findByBerichtsjahrAndLandAndBetreiberNummer(zieljahr, Land.BAYERN,
                oldBetreiber.getBetreiberNummer())).thenReturn(Optional.empty());
        when(betreiberRepository.save(newBetreiber)).thenReturn(newBetreiber);
        when(referenzenResolverService.copyBetriebsstaetteToBerichtsjahr(originalBetriebsstaette, zieljahr,
                newBetreiber)).thenReturn(
                new EntityWithError<>(builder.betreiber(newBetreiber).berichtsjahr(zieljahr).id(null).build()));
        when(betriebsstaetteRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        stammdatenService.copyBetriebsstaette(originalBetriebsstaette, zieljahr,
                new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING));

        verify(betreiberRepository).save(newBetreiber);
        var betriebsstaetteCaptor = ArgumentCaptor.forClass(Betriebsstaette.class);
        verify(betriebsstaetteRepository).save(betriebsstaetteCaptor.capture());
        assertThat(betriebsstaetteCaptor.getValue().getBetreiber()).isSameAs(newBetreiber);
    }

    @Test
    void deleteAnsprechpartner_shouldPublishEvent() {
        var jahr = aJahrReferenz(2017).build();
        var username = "user";
        stammdatenService.deleteAllAnsprechpartner(jahr, username);

        verify(eventPublisher).publishEvent(any(AnsprechpartnerLoeschenTriggerdEvent.class));
    }

    @Test
    void updateStammdatenObjekteForAbschlussUebernahme() {

        Betriebsstaette betriebsstaette;
        List<Anlage> anlagen;
        List<Quelle> quellen;
        List<Anlagenteil> anlagenteile;
        Betreiber betreiber;

        // test invocation with "minimal" parameters (null where allowed, empty lists)
        betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette().betreiberdatenUebernommen(true)
                                                .letzteAenderungBetreiber(Instant.now()).build();
        anlagen = new ArrayList<>();
        quellen = new ArrayList<>();
        anlagenteile = new ArrayList<>();
        betreiber = null;
        stammdatenService.updateStammdatenObjekteForAbschlussUebernahme(betriebsstaette, anlagen, quellen, anlagenteile,
                betreiber);

        verify(betriebsstaetteRepository).save(betriebsstaette);
        verifyNoMoreInteractions(betriebsstaetteRepository);
        verifyNoInteractions(anlageRepository);
        verifyNoInteractions(quelleRepository);
        verifyNoInteractions(anlagenteilRepository);
        verifyNoInteractions(betreiberRepository);

        assertThat(betriebsstaette.isBetreiberdatenUebernommen()).isTrue();
        assertThat(betriebsstaette.getLetzteAenderungBetreiber()).isNull();

        // test invocation with non-trivial parameters
        reset(betriebsstaetteRepository);
        reset(anlageRepository);
        reset(quelleRepository);
        reset(anlagenteilRepository);
        reset(betreiberRepository);


        anlagen.add(AnlageBuilder.anAnlage(1L).build());
        quellen.add(QuelleBuilder.aQuelle(2L).build());
        quellen.add(QuelleBuilder.aQuelle(3L).build());
        anlagenteile.add(AnlagenteilBuilder.anAnlagenteil(4L).build());
        anlagenteile.add(AnlagenteilBuilder.anAnlagenteil(5L).build());
        anlagenteile.add(AnlagenteilBuilder.anAnlagenteil(6L).build());
        betreiber = BetreiberBuilder.aFullBetreiber().build();

        stammdatenService.updateStammdatenObjekteForAbschlussUebernahme(betriebsstaette, anlagen, quellen, anlagenteile,
                betreiber);

        verify(betriebsstaetteRepository).save(betriebsstaette);
        verifyNoMoreInteractions(betriebsstaetteRepository);
        verify(anlageRepository).save(argThat(a -> a.getId() == 1));
        verifyNoMoreInteractions(anlageRepository);
        verify(quelleRepository, times(2)).save(argThat(q -> q.getId() == 2 || q.getId() == 3));
        verifyNoMoreInteractions(quelleRepository);
        verify(anlagenteilRepository, times(3)).save(argThat(a -> 4 <= a.getId() && a.getId() <= 6));
        verifyNoMoreInteractions(anlagenteilRepository);
        verify(betreiberRepository).save(betreiber);
        verifyNoMoreInteractions(betreiberRepository);

        assertThat(anlagen).hasSize(1);
        assertThat(anlagen.get(0).isBetreiberdatenUebernommen()).isFalse();
        assertThat(anlagen.get(0).getLetzteAenderungBetreiber()).isNull();
        assertThat(quellen).hasSize(2);
        for (Quelle quelle : quellen) {
            assertThat(quelle.isBetreiberdatenUebernommen()).isFalse();
            assertThat(quelle.getLetzteAenderungBetreiber()).isNull();
        }
        assertThat(anlagenteile).hasSize(3);
        for (Anlagenteil anlagenteil : anlagenteile) {
            assertThat(anlagenteil.isBetreiberdatenUebernommen()).isFalse();
            assertThat(anlagenteil.getLetzteAenderungBetreiber()).isNull();
        }
        assertThat(betreiber.isBetreiberdatenUebernommen()).isFalse();
        assertThat(betreiber.getLetzteAenderungBetreiber()).isNull();

    }

    private Optional<Betriebsstaette> optionalBst() {
        return Optional.of(BetriebsstaetteBuilder.aBetriebsstaette().build());
    }

}
