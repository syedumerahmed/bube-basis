package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository.berichtsjahrIdEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;

@ExtendWith(MockitoExtension.class)
class SucheServiceTest {

    @Mock
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Mock
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    private BetriebsstaettenBerechtigungsAccessor accessor;
    @Mock
    private AktiverBenutzer aktiverBenutzer;
    @Mock
    private SuchattributePredicateFactory suchattributePredicatefactory;

    private SucheService service;

    @BeforeEach
    void setUp() {
        when(stammdatenBerechtigungsContext.createPredicateRead(accessor)).thenReturn(new BooleanBuilder());
        service = new SucheService(stammdatenBerechtigungsContext, betriebsstaetteRepository, accessor,
                suchattributePredicatefactory, aktiverBenutzer);
    }

    @Test
    void sucheIds() {
        var selectedBerichtsjahrId = 1L;

        List<Long> list = service.sucheIds(selectedBerichtsjahrId, null);

        verify(betriebsstaetteRepository).findAllIds(any(Predicate.class));
        assertTrue(list.isEmpty());
    }

    @Test
    void shouldUsePage() {
        var selectedBerichtsjahrId = 1L;
        var pageRequest = PageRequest.of(2, 10);

        service.suche(selectedBerichtsjahrId, null, pageRequest);

        verify(betriebsstaetteRepository).findAllAsListItems(any(Predicate.class), same(pageRequest));
    }

    @Test
    void shouldUseSucheAsBehoerdenbenutzer() {
        when(aktiverBenutzer.isBehoerdennutzer()).thenReturn(true);
        var selectedBerichtsjahrId = 1L;

        var suche = new Suchattribute() {{
            betriebsstaette.ort = "*6%**";
        }};
        var suchPredicate = new BooleanBuilder(betriebsstaette.adresse.ort.likeIgnoreCase("%6\\%*%", '\\'));
        when(suchattributePredicatefactory.createPredicate(same(suche), same(betriebsstaette))).thenReturn(
                suchPredicate);

        service.suche(selectedBerichtsjahrId, suche, Pageable.unpaged());

        verify(betriebsstaetteRepository).findAllAsListItems(
                eq(new BooleanBuilder(berichtsjahrIdEquals(selectedBerichtsjahrId)).and(suchPredicate)),
                any(Pageable.class));
    }

    @Test
    void shouldIgnoreSucheAsBetreiber() {
        when(aktiverBenutzer.isBehoerdennutzer()).thenReturn(false);
        var selectedBerichtsjahrId = 1L;

        var suche = new Suchattribute() {{
            betriebsstaette.ort = "*6%**";
        }};

        service.suche(selectedBerichtsjahrId, suche, Pageable.unpaged());

        verify(betriebsstaetteRepository).findAllAsListItems(
                eq(new BooleanBuilder(berichtsjahrIdEquals(selectedBerichtsjahrId))), any(Pageable.class));
        verifyNoInteractions(suchattributePredicatefactory);
    }
}
