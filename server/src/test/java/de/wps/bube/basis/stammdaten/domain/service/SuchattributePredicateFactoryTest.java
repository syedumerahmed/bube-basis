package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.NAME;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.ORT;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.querydsl.core.BooleanBuilder;

import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;

class SuchattributePredicateFactoryTest {

    private SuchattributePredicateFactory mapper;

    @BeforeEach
    void setUp() {
        mapper = new SuchattributePredicateFactory();
    }

    @Test
    void mapSuchattribute_shouldMapNull() {
        var builder = mapper.createPredicate(null, betriebsstaette);

        assertThat(builder).isNull();
    }

    @Test
    void mapSuchattribute_shouldAndMultipleExpressions() {
        var suchattribute = new Suchattribute() {{
            betriebsstaette.name = "Name";
            betriebsstaette.ort = "Ort";
        }};

        var builder = mapper.createPredicate(suchattribute, betriebsstaette);

        assertThat(builder).isEqualTo(
                new BooleanBuilder(NAME.createPredicate(suchattribute, betriebsstaette)).and(
                        ORT.createPredicate(suchattribute, betriebsstaette)));
    }
}
