package de.wps.bube.basis.referenzdaten.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class ReferenzlisteMetadataRepositoryTest {

    @Autowired
    private ReferenzlisteMetadataRepository repository;

    @Test
    void loadListe() {
        assertTrue(repository.findAll().size() > 18);
    }

    @Test
    void loadRVORS() {
        var r = repository.findById(Referenzliste.RVORS).get();

        assertEquals(Referenzliste.RVORS, r.getName());
        assertEquals("Vorschriften", r.getKtext());
        assertEquals("Vorschriften, unter die die BSt bzw. AnlAN fällt", r.getLtext());
        assertTrue(r.isBundeseinheitlich());
    }

}
