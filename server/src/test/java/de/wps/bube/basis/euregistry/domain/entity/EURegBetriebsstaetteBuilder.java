package de.wps.bube.basis.euregistry.domain.entity;

import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;

import java.time.Instant;

import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;

public class EURegBetriebsstaetteBuilder {

    private Long id;
    private Betriebsstaette betriebsstaette;
    private Berichtsdaten berichtsdaten;
    private Instant ersteErfassung;
    private Instant letzteAenderung;

    public static EURegBetriebsstaetteBuilder aEURegBetriebsstaette() {
        return new EURegBetriebsstaetteBuilder().betriebsstaette(aFullBetriebsstaette().build())
                                                .berichtsdaten(aFullBerichtsdaten().build())
                                                .ersteErfassung(Instant.parse("2021-08-08T16:48:09Z"))
                                                .letzteAenderung(Instant.parse("2021-08-07T16:48:09Z"));
    }

    public EURegBetriebsstaetteBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public EURegBetriebsstaetteBuilder betriebsstaette(Betriebsstaette betriebsstaette) {
        this.betriebsstaette = betriebsstaette;
        return this;
    }

    public EURegBetriebsstaetteBuilder berichtsdaten(Berichtsdaten berichtsdaten) {
        this.berichtsdaten = berichtsdaten;
        return this;
    }

    public EURegBetriebsstaetteBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public EURegBetriebsstaetteBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public EURegBetriebsstaette build() {
        EURegBetriebsstaette b = new EURegBetriebsstaette(betriebsstaette, berichtsdaten);
        b.setId(id);
        b.setErsteErfassung(ersteErfassung);
        b.setLetzteAenderung(letzteAenderung);
        return b;
    }

}
