package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.base.vo.Land.SCHLESWIG_HOLSTEIN;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.anEURegAnlage;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht.LIEGT_VOR;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.euregistry.domain.UebernimmEuRegDatenInJahrCommand;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.persistence.EuRegAnlageRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.BerichtsobjektService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = { EURegistryService.class, EuRegBstBerechtigungsAccessor.class, BehoerdenService.class,
        BehoerdeBerechtigungsAccessor.class, EigeneBehoerdenBerechtigungsAcessor.class,
        EuRegistryBerichtsjahrwechselMapperImpl.class, EuRegUpdaterImpl.class, ReferenzResolverService.class,
        ReferenzenService.class })
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.euregistry.persistence",
        "de.wps.bube.basis.stammdaten.persistence",
        "de.wps.bube.basis.komplexpruefung.persistence",
        "de.wps.bube.basis.referenzdaten.persistence" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.euregistry.domain",
        "de.wps.bube.basis.stammdaten.domain",
        "de.wps.bube.basis.komplexpruefung.domain",
        "de.wps.bube.basis.referenzdaten" })
class EURegistryServiceIntegrationTest extends BubeIntegrationTest {

    private static final String ANLAGE_NR = "Anlage-123";

    @Autowired
    private EURegistryService service;
    @Autowired
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Autowired
    private EuRegBstBerechtigungsAccessor berechtigungsAccessor;
    @Autowired
    private EURegBetriebsstaetteRepository euRegBstRepository;
    @Autowired
    private EuRegAnlageRepository euRegAnlageRepository;
    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    private AnlageRepository anlageRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @MockBean
    private KomplexpruefungService komplexpruefungService;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BerichtsobjektService berichtsobjektService;
    @MockBean
    private ParameterService parameterService;

    private EURegBetriebsstaette bericht2020_02;
    private EURegAnlage anlageBericht;
    private Referenz berichtsart;
    private Referenz jahr2020;
    private Referenz betriebsStatus;
    private Referenz agsHH;
    private Behoerde behoerdeBst;
    private Referenz bearbeitungsstatus;
    private Behoerde behoerdeBericht;
    private Betriebsstaette betriebsstaette2020_02;

    @BeforeEach
    void setUp() {
        jahr2020 = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsStatus = referenzRepository.save(aBetriebsstatusReferenz("in Betrieb").build());
        agsHH = referenzRepository.save(aGemeindeReferenz("0200000").build());
        behoerdeBst = behoerdenRepository.save(aBehoerde("HHBST").land(HAMBURG).build());
        betriebsstaette2020_02 = betriebsstaetteRepository.save(
                getBetriebsstaetteBuilder().betriebsstaetteNr("2020_02").build());

        berichtsart = referenzRepository.save(aReferenz(RRTYP, "ART01").build());
        bearbeitungsstatus = referenzRepository.save(aReferenz(RBEA, "BS01").build());
        behoerdeBericht = behoerdenRepository.save(aBehoerde("HHEUREG").land(HAMBURG).build());
        var berichtsdaten = getBerichtsdatenBuilder();
        bericht2020_02 = euRegBstRepository.save(
                aEURegBetriebsstaette().betriebsstaette(betriebsstaette2020_02)
                                       .berichtsdaten(berichtsdaten.build())
                                       .build());
        var anlage = anlageRepository.save(
                anAnlage().anlageNr(ANLAGE_NR).parentBetriebsstaetteId(betriebsstaette2020_02.getId()).build());
        anlageBericht = euRegAnlageRepository.save(anEURegAnlage().anlage(anlage)
                                                                  .berichtsdaten(berichtsdaten.build())
                                                                  .build());

    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void shouldUpdateAkz() {

        assertThat(bericht2020_02.getBerichtsdaten().getAkz()).isEqualTo("BERICHT_AKZ");

        // Act
        var bericht = service.loadBetriebsstaettenBericht(bericht2020_02.getId());
        bericht.getBerichtsdaten().setAkz("BERICHT_AKZ_NEU");
        service.updateBetriebsstaettenBericht(bericht);
        var updated = service.loadBetriebsstaettenBericht(bericht2020_02.getId());

        // Assert
        assertThat(updated.getBerichtsdaten().getAkz()).isEqualTo("BERICHT_AKZ_NEU");
        assertThat(updated.getErsteErfassung().toEpochMilli()).isEqualTo(
                bericht2020_02.getErsteErfassung().toEpochMilli());
        assertThat(updated.getBerichtsdaten().getBearbeitungsstatus()).isEqualTo(
                bericht2020_02.getBerichtsdaten().getBearbeitungsstatus());
        assertThat(updated.getBerichtsdaten().getBearbeitungsstatusSeit()).isEqualTo(
                bericht2020_02.getBerichtsdaten().getBearbeitungsstatusSeit());

    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ, BEHOERDE_WRITE }, land = HAMBURG, akz = "anderesAKZ")
    void landReadBehoerdeWrite_shouldnot_haveAccessToUpdate_notMatchingDatenberechtigung() {
        assertThrows(AccessDeniedException.class,
                () -> service.updateBetriebsstaettenBericht(bericht2020_02));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ, BEHOERDE_WRITE }, land = HAMBURG, akz = { "BERICHT_AKZ", "BERICHT_AKZ_NEU" })
    void landReadBehoerdeWrite_should_haveAccessToUpdate_matchingDatenberechtigung() {
        var bericht = service.loadBetriebsstaettenBericht(bericht2020_02.getId());
        bericht.getBerichtsdaten().setAkz("BERICHT_AKZ_NEU");
        assertDoesNotThrow(() -> service.updateBetriebsstaettenBericht(bericht));
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG)
    void loadAnlagenbericht_keineBerechtigung() {
        var bstId = 2L;
        when(stammdatenService.canReadBetriebsstaette(bstId)).thenReturn(false);
        assertThrows(AccessDeniedException.class,
                () -> service.loadAnlagenBericht(bstId, "anlage"));
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG)
    void loadAnlagenbericht() {
        var bstId = bericht2020_02.getBetriebsstaette().getId();
        var anlNummer = anlageBericht.getAnlage().getAnlageNr();
        assertThat(anlNummer).isEqualTo(ANLAGE_NR);
        when(stammdatenService.canReadBetriebsstaette(bstId)).thenReturn(true);
        var result = service.loadAnlagenBericht(bstId, ANLAGE_NR);

        assertThat(result).isEqualTo(anlageBericht);
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG)
    void loadAnlagenbericht_ignoreCase() {
        var bstId = bericht2020_02.getBetriebsstaette().getId();
        var anlNummer = anlageBericht.getAnlage().getAnlageNr();
        assertThat(anlNummer).isEqualTo(ANLAGE_NR);
        when(stammdatenService.canReadBetriebsstaette(bstId)).thenReturn(true);
        var result = service.loadAnlagenBericht(bstId, ANLAGE_NR.toUpperCase());

        assertThat(result).isEqualTo(anlageBericht);
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG)
    void loadAnlagenbericht_wrongAnlageNr() {
        var bstId = bericht2020_02.getBetriebsstaette().getId();
        var anlNummer = anlageBericht.getAnlage().getAnlageNr();
        assertThat(anlNummer).isEqualTo(ANLAGE_NR);
        when(stammdatenService.canReadBetriebsstaette(bstId)).thenReturn(true);
        assertThrows(BubeEntityNotFoundException.class, () -> service.loadAnlagenBericht(bstId, ANLAGE_NR + "4"));
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_WRITE, land = HAMBURG)
    void updateEuRegAnlage_keineBerechtigung() {
        when(stammdatenService.canWriteBetriebsstaette(
                anlageBericht.getAnlage().getParentBetriebsstaetteId())).thenReturn(false);
        assertThrows(AccessDeniedException.class, () -> service.updateAnlagenBericht(anlageBericht));
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_WRITE, land = HAMBURG)
    void updateEuRegAnlage() {
        var updatedBericht = anEURegAnlage(anlageBericht.getId())
                .bvtAusnahmen()
                .berichtsdaten(aFullBerichtsdaten().akz("mein-akz").build())
                .eSpirsNr("spi")
                .monitor("monitor")
                .monitorUrl("url")
                .report(LIEGT_VOR)
                .build();

        when(stammdatenService.canWriteBetriebsstaette(
                anlageBericht.getAnlage().getParentBetriebsstaetteId())).thenReturn(true);

        var result = service.updateAnlagenBericht(updatedBericht);

        assertThat(result.getBerichtsdaten().getAkz()).isEqualTo("mein-akz");
        assertThat(result.geteSpirsNr()).isEqualTo("spi");
        assertThat(result.getMonitor()).isEqualTo("monitor");
        assertThat(result.getMonitorUrl()).isEqualTo("url");
        assertThat(result.getReport()).isEqualTo(LIEGT_VOR);
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_WRITE, land = NORDRHEIN_WESTFALEN)
    void uebernimmEuRegDaten_keineBerechtigung() {
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        var command = new UebernimmEuRegDatenInJahrCommand(List.of(bericht2020_02.getId()), jahr);

        assertThrows(AccessDeniedException.class, () -> service.uebernimmEuRegDaten(command));
    }

    @Test
    @WithBubeMockUser(roles = LAND_WRITE, land = NORDRHEIN_WESTFALEN)
    void uebernimmEuRegDaten() {
        // Arrange
        JobProtokoll jobProtokoll = new JobProtokoll("test");

        Referenz jahr2018 = ReferenzBuilder.aJahrReferenz(2018).build();
        Referenz bearbeitungsstatus = referenzRepository.getById(10201L);
        Referenz gemeindeKennziffer = aGemeindeReferenz("GEMEINDE").build();

        Behoerde zustOld = BehoerdeBuilder.aBehoerde("test").bezeichnung("old").gueltigBis(2019).build();
        Behoerde zustNew = BehoerdeBuilder.aBehoerde("test").bezeichnung("new").gueltigVon(2020).build();

        referenzRepository.saveAndFlush(gemeindeKennziffer);
        referenzRepository.saveAndFlush(jahr2020);
        referenzRepository.saveAndFlush(jahr2018);

        var originalBerichtsdaten = BerichtsdatenBuilder.aFullBerichtsdaten()
                                                        .berichtsart(berichtsart)
                                                        .bearbeitungsstatus(bearbeitungsstatus)
                                                        .zustaendigeBehoerde(zustOld).build();
        var originalBetriebsstaette = BetriebsstaetteBuilder
                .aBetriebsstaette()
                .land(NORDRHEIN_WESTFALEN)
                .gemeindekennziffer(gemeindeKennziffer)
                .betriebsstatus(bearbeitungsstatus)
                .localId("465798")
                .berichtsjahr(jahr2018)
                .zustaendigeBehoerde(zustOld)
                .build();

        behoerdenRepository.saveAndFlush(zustOld);
        behoerdenRepository.saveAndFlush(zustNew);

        originalBetriebsstaette = betriebsstaetteRepository.saveAndFlush(originalBetriebsstaette);
        var originalAnlage = AnlageBuilder
                .anAnlage()
                .localId("localId")
                .parentBetriebsstaetteId(originalBetriebsstaette.getId())
                .build();
        var originalEuRegBetriebsstaette = EURegBetriebsstaetteBuilder
                .aEURegBetriebsstaette()
                .betriebsstaette(originalBetriebsstaette)
                .berichtsdaten(originalBerichtsdaten)
                .build();
        var originalEuRegAnlage = anEURegAnlage()
                .bvtAusnahmen()
                .berichtsdaten(originalBerichtsdaten)
                .anlage(originalAnlage)
                .eSpirsNr("spi")
                .monitor("monitor")
                .monitorUrl("url")
                .localId2("localId")
                .report(LIEGT_VOR)
                .build();

        originalAnlage = anlageRepository.saveAndFlush(originalAnlage);
        originalBetriebsstaette.setAnlagen(new ArrayList<>(List.of(originalAnlage)));
        betriebsstaetteRepository.saveAndFlush(originalBetriebsstaette);
        euRegAnlageRepository.saveAndFlush(originalEuRegAnlage);
        euRegBstRepository.saveAndFlush(originalEuRegBetriebsstaette);
        var copyBetriebsstaette = BetriebsstaetteBuilder
                .aBetriebsstaette()
                .gemeindekennziffer(gemeindeKennziffer)
                .betriebsstatus(bearbeitungsstatus)
                .land(NORDRHEIN_WESTFALEN)
                .localId("465798")
                .berichtsjahr(jahr2020)
                .zustaendigeBehoerde(zustOld)
                .build();

        var copyBerichtsdaten = new Berichtsdaten(zustNew, null, berichtsart, bearbeitungsstatus, LocalDate.now());

        Betriebsstaette betriebsstaette = betriebsstaetteRepository.saveAndFlush(copyBetriebsstaette);

        var copyAnlage = AnlageBuilder
                .anAnlage()
                .localId("localId")
                .parentBetriebsstaetteId(betriebsstaette.getId())
                .build();
        var copyEuRegAnlage = anEURegAnlage()
                .bvtAusnahmen()
                .berichtsdaten(copyBerichtsdaten)
                .eSpirsNr("tt")
                .monitor("tt")
                .anlage(copyAnlage)
                .localId2("localId")
                .monitorUrl("tt")
                .report(LIEGT_VOR)
                .build();
        var copyEuRegBetriebsstaette = EURegBetriebsstaetteBuilder
                .aEURegBetriebsstaette()
                .betriebsstaette(copyBetriebsstaette)
                .berichtsdaten(copyBerichtsdaten)
                .build();
        copyAnlage = anlageRepository.saveAndFlush(copyAnlage);
        copyBetriebsstaette.setAnlagen(new ArrayList<>(List.of(copyAnlage)));
        betriebsstaetteRepository.save(copyBetriebsstaette);
        euRegAnlageRepository.saveAndFlush(copyEuRegAnlage);
        euRegBstRepository.saveAndFlush(copyEuRegBetriebsstaette);

        // ACT
        when(stammdatenService.canWriteBetriebsstaette(any())).thenReturn(true);

        service.copyDaten(
                originalBetriebsstaette,
                jahr2020,
                jobProtokoll
        );

        // ASSERT
        var resultBetriebsstaette = euRegBstRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.betriebsstaette.localId
                                            .eq(originalBetriebsstaette.getLocalId())
                                            .and(eURegBetriebsstaette.betriebsstaette.berichtsjahr.schluessel
                                                    .eq(jahr2020.getSchluessel()))))
                                                      .get();
        var resultAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlage(copyAnlage.getLocalId(),
                jahr2020);

        assertEquals(resultBetriebsstaette.getBerichtsdaten().getZustaendigeBehoerde().getSchluessel(),
                originalBerichtsdaten.getZustaendigeBehoerde().getSchluessel());
        assertEquals(resultBetriebsstaette.getBerichtsdaten().getZustaendigeBehoerde(),
                zustNew);
        assertEquals(resultAnlage.getBerichtsdaten().getAkz(), originalBerichtsdaten.getAkz());
        assertEquals(resultAnlage.getAnlage(), copyAnlage);
    }

    @Test
    @WithBubeMockUser(roles = LAND_WRITE, land = NORDRHEIN_WESTFALEN)
    void uebernimmEuRegDaten_ueberspringBereitsBearbeitet() {
        // Arrange
        JobProtokoll jobProtokoll = new JobProtokoll("test");

        Referenz jahr2018 = ReferenzBuilder.aJahrReferenz(2018).build();
        Referenz bearbeitungsstatus = referenzRepository.getById(10201L);
        Referenz bearbeitungsstatus_bearbeitet = referenzRepository.getById(10203L);
        Referenz gemeindeKennziffer = aGemeindeReferenz("GEMEINDE").build();

        Behoerde zustOld = BehoerdeBuilder.aBehoerde("test").bezeichnung("old").gueltigBis(2019).build();
        Behoerde zustNew = BehoerdeBuilder.aBehoerde("test").bezeichnung("new").gueltigVon(2020).build();

        referenzRepository.saveAndFlush(gemeindeKennziffer);
        referenzRepository.saveAndFlush(jahr2020);
        referenzRepository.saveAndFlush(jahr2018);

        var originalBerichtsdaten = BerichtsdatenBuilder.aFullBerichtsdaten()
                                                        .berichtsart(berichtsart)
                                                        .bearbeitungsstatus(bearbeitungsstatus)
                                                        .zustaendigeBehoerde(zustOld).build();
        var originalBetriebsstaette = BetriebsstaetteBuilder
                .aBetriebsstaette()
                .land(NORDRHEIN_WESTFALEN)
                .gemeindekennziffer(gemeindeKennziffer)
                .betriebsstatus(bearbeitungsstatus)
                .localId("465798")
                .berichtsjahr(jahr2018)
                .zustaendigeBehoerde(zustOld)
                .build();

        behoerdenRepository.saveAndFlush(zustOld);
        behoerdenRepository.saveAndFlush(zustNew);

        originalBetriebsstaette = betriebsstaetteRepository.saveAndFlush(originalBetriebsstaette);
        var originalAnlage = AnlageBuilder
                .anAnlage()
                .localId("localId")
                .parentBetriebsstaetteId(originalBetriebsstaette.getId())
                .build();
        var originalEuRegBetriebsstaette = EURegBetriebsstaetteBuilder
                .aEURegBetriebsstaette()
                .betriebsstaette(originalBetriebsstaette)
                .berichtsdaten(originalBerichtsdaten)
                .build();
        var originalEuRegAnlage = anEURegAnlage()
                .bvtAusnahmen()
                .berichtsdaten(originalBerichtsdaten)
                .anlage(originalAnlage)
                .eSpirsNr("spi")
                .monitor("monitor")
                .monitorUrl("url")
                .localId2("localId")
                .report(LIEGT_VOR)
                .build();
        originalAnlage = anlageRepository.saveAndFlush(originalAnlage);
        originalBetriebsstaette.setAnlagen(new ArrayList<>(List.of(originalAnlage)));
        betriebsstaetteRepository.saveAndFlush(originalBetriebsstaette);
        euRegAnlageRepository.saveAndFlush(originalEuRegAnlage);
        euRegBstRepository.saveAndFlush(originalEuRegBetriebsstaette);
        var copyBetriebsstaette = BetriebsstaetteBuilder
                .aBetriebsstaette()
                .gemeindekennziffer(gemeindeKennziffer)
                .betriebsstatus(bearbeitungsstatus)
                .land(NORDRHEIN_WESTFALEN)
                .localId("465798")
                .berichtsjahr(jahr2020)
                .zustaendigeBehoerde(zustOld)
                .build();

        var copyBerichtsdaten = new Berichtsdaten(zustNew, null, berichtsart, bearbeitungsstatus_bearbeitet,
                LocalDate.now());

        Betriebsstaette betriebsstaette = betriebsstaetteRepository.saveAndFlush(copyBetriebsstaette);

        var copyAnlage = AnlageBuilder
                .anAnlage()
                .localId("localId")
                .parentBetriebsstaetteId(betriebsstaette.getId())
                .build();
        var copyEuRegAnlage = anEURegAnlage()
                .bvtAusnahmen()
                .berichtsdaten(copyBerichtsdaten)
                .eSpirsNr("tt")
                .monitor("tt")
                .anlage(copyAnlage)
                .localId2("localId")
                .monitorUrl("tt")
                .report(LIEGT_VOR)
                .build();
        var copyEuRegBetriebsstaette = EURegBetriebsstaetteBuilder
                .aEURegBetriebsstaette()
                .betriebsstaette(copyBetriebsstaette)
                .berichtsdaten(copyBerichtsdaten)
                .build();
        copyAnlage = anlageRepository.saveAndFlush(copyAnlage);
        copyBetriebsstaette.setAnlagen(new ArrayList<>(List.of(copyAnlage)));
        betriebsstaetteRepository.save(copyBetriebsstaette);
        euRegAnlageRepository.saveAndFlush(copyEuRegAnlage);
        euRegBstRepository.saveAndFlush(copyEuRegBetriebsstaette);

        // ACT
        when(stammdatenService.canWriteBetriebsstaette(any())).thenReturn(true);

        service.copyDaten(
                originalBetriebsstaette,
                jahr2020,
                jobProtokoll
        );

        // ASSERT
        var resultBetriebsstaette = euRegBstRepository.findOne(
                stammdatenBerechtigungsContext.createPredicateRead(berechtigungsAccessor)
                                              .and(eURegBetriebsstaette.betriebsstaette.localId
                                            .eq(originalBetriebsstaette.getLocalId())
                                            .and(eURegBetriebsstaette.betriebsstaette.berichtsjahr.schluessel
                                                    .eq(jahr2020.getSchluessel()))))
                                                      .get();
        var resultAnlage = euRegAnlageRepository.findByLocalIdAndBerichtsjahrForAnlage(copyAnlage.getLocalId(),
                jahr2020);

        assertEquals(resultBetriebsstaette.getBerichtsdaten().getZustaendigeBehoerde().getSchluessel(),
                originalBerichtsdaten.getZustaendigeBehoerde().getSchluessel());
        assertEquals(resultBetriebsstaette.getBerichtsdaten().getZustaendigeBehoerde(),
                zustNew);
        assertNull(resultAnlage.getBerichtsdaten().getAkz());
        assertEquals(resultAnlage.getAnlage(), copyAnlage);
    }

    @Nested
    @WithBubeMockUser(roles = LAND_WRITE, land = HAMBURG)
    class StreamAllByIdIn {

        private List<EURegBetriebsstaette> bstBerichte;

        @BeforeEach
        void setUp() {
            var builder = getBetriebsstaetteBuilder();
            bstBerichte = betriebsstaetteRepository
                    .saveAll(List.of(
                            builder.land(HAMBURG).build(),
                            builder.land(HAMBURG).betriebsstaetteNr("2").build(),
                            builder.land(HAMBURG).betriebsstaetteNr("3").build(),
                            builder.land(SCHLESWIG_HOLSTEIN).build()))
                    .stream()
                    .map(b -> euRegBstRepository.save(
                            aEURegBetriebsstaette().berichtsdaten(getBerichtsdatenBuilder().build())
                                                   .betriebsstaette(b)
                                                   .build()))
                    .collect(toList());
        }

        @Test
        void shouldStreamByBetriebsstaettenIds() {
            var ids = List.of(
                    bstBerichte.get(0).getBetriebsstaette().getId(),
                    bstBerichte.get(1).getBetriebsstaette().getId()
            );
            when(stammdatenService.canReadAllBetriebsstaette(ids)).thenReturn(true);
            var result = service.streamAllByBetriebsstaetteIdIn(ids).toList();

            assertThat(result).containsExactlyInAnyOrder(bstBerichte.get(0), bstBerichte.get(1));
        }

        @Test
        void shouldDenyBetriebsstaettenIdsForOtherLand() {
            var ids = List.of(
                    bstBerichte.get(0).getBetriebsstaette().getId(),
                    bstBerichte.get(3).getBetriebsstaette().getId()
            );
            when(stammdatenService.canReadAllBetriebsstaette(ids)).thenReturn(false);

            assertThrows(AccessDeniedException.class, () -> service.streamAllByBetriebsstaetteIdIn(ids));
        }

        @Test
        void shouldStreamByBerichtIds() {
            var result = service.streamAllByBerichtIdIn(List.of(
                    bstBerichte.get(0).getId(),
                    bstBerichte.get(1).getId()
            )).toList();

            assertThat(result).containsExactlyInAnyOrder(bstBerichte.get(0), bstBerichte.get(1));
        }

        @Test
        void shouldDenyBerichtIdsForOtherLand() {
            var ids = List.of(
                    bstBerichte.get(0).getId(),
                    bstBerichte.get(3).getId()
            );

            assertThrows(AccessDeniedException.class, () -> service.streamAllByBerichtIdIn(ids));
        }
    }

    private BetriebsstaetteBuilder getBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr2020)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsStatus)
                                 .akz("AKZ")
                                 .gemeindekennziffer(agsHH)
                                 .zustaendigeBehoerde(behoerdeBst);
    }

    private BerichtsdatenBuilder getBerichtsdatenBuilder() {
        return aFullBerichtsdaten()
                .zustaendigeBehoerde(behoerdeBericht)
                .akz("BERICHT_AKZ")
                .berichtsart(berichtsart)
                .bearbeitungsstatus(bearbeitungsstatus);
    }
}
