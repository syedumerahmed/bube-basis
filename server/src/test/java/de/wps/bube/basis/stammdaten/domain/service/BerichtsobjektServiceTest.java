package de.wps.bube.basis.stammdaten.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Berichtsobjekt;
import de.wps.bube.basis.stammdaten.domain.entity.BerichtsobjektBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.persistence.BerichtsobjektRepository;

@ExtendWith(MockitoExtension.class)
class BerichtsobjektServiceTest {

    private static final LocalDate TODAY = LocalDate.now();

    @Mock
    private BerichtsobjektRepository berichtsobjektRepository;
    @Mock
    private ReferenzenService referenzenService;
    @Mock
    private StammdatenService stammdatenService;

    private final Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(523L).build();
    private final Referenz unbearbeitet = ReferenzBuilder.aBearbeitungsstatusReferenz("unbearbeitet").id(1L).build();
    private final Referenz inBearbeitung = ReferenzBuilder.aBearbeitungsstatusReferenz("in Bearbeitung").id(2L).build();
    private final Referenz euRegBST = ReferenzBuilder.aBerichtsartReferenz("EURegBST").id(3L).build();
    private final Berichtsobjekt berichtsobjekt = BerichtsobjektBuilder.aBerichtsobjekt()
                                                                       .id(123L)
                                                                       .betriebsstaetteId(523L)
                                                                       .bearbeitungsstatus(unbearbeitet)
                                                                       .bearbeitungsstatusSeit(TODAY)
                                                                       .berichtsart(euRegBST)
                                                                       .url("test")
                                                                       .build();

    // System under test
    private BerichtsobjektService berichtsobjektService;

    @BeforeEach
    void setUp() {
        berichtsobjektService = new BerichtsobjektService(berichtsobjektRepository, referenzenService,
                stammdatenService);
    }

    @Test
    void addBerichtsobjekt_invalidBetriebsstaetteId() {

        when(stammdatenService.loadBetriebsstaetteForRead(any())).thenThrow(
                new BubeEntityNotFoundException(Betriebsstaette.class));

        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class, () ->
                berichtsobjektService.addBerichtsobjekt(524L, "unbearbeitet", "EURegBST", LocalDate.now(), null));
        assertThat(exception.getMessage()).startsWith(Betriebsstaette.class.getSimpleName());

    }

    @Test
    void addBerichtsobjekt_invalidBerichtsart() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenAnswer(
                this::refNotFound);

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class, () ->
                berichtsobjektService.addBerichtsobjekt(523L, "invalid", "EURegBST", LocalDate.now(), null));
        assertThat(exception.referenzliste).isSameAs(Referenzliste.RRTYP);
        assertThat(exception.schluessel).isEqualTo("invalid");
        assertThat(exception.jahr).isEqualTo(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        assertThat(exception.land).isEqualTo(betriebsstaette.getLand());

    }

    @Test
    void addBerichtsobjekt_invalidBearbeitungsstatus() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenReturn(euRegBST);
        when(referenzenService.readReferenz(eq(Referenzliste.RBEA), any(), any(), any())).thenAnswer(this::refNotFound);

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class, () ->
                berichtsobjektService.addBerichtsobjekt(523L, "unbearbeitet", "invalid", LocalDate.now(), null));
        assertThat(exception.referenzliste).isSameAs(Referenzliste.RBEA);
        assertThat(exception.schluessel).isEqualTo("invalid");
        assertThat(exception.jahr).isEqualTo(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        assertThat(exception.land).isEqualTo(betriebsstaette.getLand());

    }

    @Test
    void addBerichtsobjekt_valid() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), eq(euRegBST.getSchluessel()), any(),
                any())).thenReturn(euRegBST);
        when(referenzenService.readReferenz(eq(Referenzliste.RBEA), eq(unbearbeitet.getSchluessel()), any(),
                any())).thenReturn(unbearbeitet);

        ArgumentCaptor<Berichtsobjekt> captor = ArgumentCaptor.forClass(Berichtsobjekt.class);

        LocalDate bearbeitungsstatusSeit = LocalDate.now();
        berichtsobjektService.addBerichtsobjekt(523L, "EURegBST", "unbearbeitet", bearbeitungsstatusSeit, "/test");

        verify(berichtsobjektRepository).save(captor.capture());
        Berichtsobjekt berichtsobjekt = captor.getValue();
        assertThat(berichtsobjekt).isNotNull();
        assertThat(berichtsobjekt.getBetriebsstaetteId()).isEqualTo(betriebsstaette.getId());
        assertThat(berichtsobjekt.getBerichtsart()).isSameAs(euRegBST);
        assertThat(berichtsobjekt.getBearbeitungsstatus()).isSameAs(unbearbeitet);
        assertThat(berichtsobjekt.getBearbeitungsstatusSeit()).isSameAs(bearbeitungsstatusSeit);
        assertThat(berichtsobjekt.getUrl()).isEqualTo("/test");

    }

    @Test
    void deleteBerichtsobjekt_invalidBetriebsstaetteId() {

        when(stammdatenService.loadBetriebsstaetteForRead(any())).thenThrow(
                new BubeEntityNotFoundException(Betriebsstaette.class));

        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class, () ->
                berichtsobjektService.deleteBerichtsobjekt(524L, "EURegBST"));
        assertThat(exception.getMessage()).startsWith(Betriebsstaette.class.getSimpleName());
    }

    @Test
    void deleteBerichtsobjekt_invalidBerichtsart() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenAnswer(
                this::refNotFound);

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class, () ->
                berichtsobjektService.deleteBerichtsobjekt(523L, "invalid"));
        assertThat(exception.referenzliste).isSameAs(Referenzliste.RRTYP);
        assertThat(exception.schluessel).isEqualTo("invalid");
        assertThat(exception.jahr).isEqualTo(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        assertThat(exception.land).isEqualTo(betriebsstaette.getLand());
    }

    @Test
    void deleteBerichtsobjekt_valid() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), eq(euRegBST.getSchluessel()), any(),
                any())).thenReturn(euRegBST);

        when(berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(523L, euRegBST)).thenReturn(
                Optional.of(berichtsobjekt));

        berichtsobjektService.deleteBerichtsobjekt(523L, "EURegBST");
        verify(berichtsobjektRepository).forceDelete(123L);
    }

    @Test
    void updateBerichtsobjekt_invalidBetriebsstaetteId() {

        when(stammdatenService.loadBetriebsstaetteForRead(any())).thenThrow(
                new BubeEntityNotFoundException(Betriebsstaette.class));

        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class, () ->
                berichtsobjektService.updateBerichtsobjekt(524L, "EURegBST", "unbearbeitet", LocalDate.now()));
        assertThat(exception.getMessage()).startsWith(Betriebsstaette.class.getSimpleName());
    }

    @Test
    void updateBerichtsobjekt_invalidBerichtsart() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenAnswer(
                this::refNotFound);

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class, () ->
                berichtsobjektService.updateBerichtsobjekt(523L, "invalid", "unbearbeitet", LocalDate.now()));
        assertThat(exception.referenzliste).isSameAs(Referenzliste.RRTYP);
        assertThat(exception.schluessel).isEqualTo("invalid");
        assertThat(exception.jahr).isEqualTo(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        assertThat(exception.land).isEqualTo(betriebsstaette.getLand());
    }

    @Test
    void updateBerichtsobjekt_invalidBerichtsobjekt() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenReturn(euRegBST);
        when(berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(523L, euRegBST)).thenThrow(
                new BubeEntityNotFoundException(Berichtsobjekt.class));

        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class, () ->
                berichtsobjektService.updateBerichtsobjekt(523L, "EURegBST", "invalid", LocalDate.now()));
        assertThat(exception.getMessage()).startsWith(Berichtsobjekt.class.getSimpleName());
    }

    @Test
    void updateBerichtsobjekt_invalidBearbeitungsstatus() {

        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenReturn(euRegBST);
        when(referenzenService.readReferenz(eq(Referenzliste.RBEA), any(), any(), any())).thenAnswer(this::refNotFound);
        when(berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(523L, euRegBST)).thenReturn(
                Optional.of(berichtsobjekt));

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class, () ->
                berichtsobjektService.updateBerichtsobjekt(523L, "EURegBST", "invalid", LocalDate.now()));
        assertThat(exception.referenzliste).isSameAs(Referenzliste.RBEA);
        assertThat(exception.schluessel).isEqualTo("invalid");
        assertThat(exception.jahr).isEqualTo(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        assertThat(exception.land).isEqualTo(betriebsstaette.getLand());
    }

    @Test
    void updateBerichtsobjekt_nothingChanged() {
        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenReturn(euRegBST);
        when(referenzenService.readReferenz(eq(Referenzliste.RBEA), any(), any(), any())).thenReturn(unbearbeitet);
        when(berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(523L, euRegBST)).thenReturn(
                Optional.of(berichtsobjekt));

        ArgumentCaptor<Berichtsobjekt> captor = ArgumentCaptor.forClass(Berichtsobjekt.class);

        berichtsobjektService.updateBerichtsobjekt(523L, berichtsobjekt.getBerichtsart().getSchluessel(),
                berichtsobjekt.getBearbeitungsstatus().getSchluessel(), berichtsobjekt.getBearbeitungsstatusSeit());

        verifyNoMoreInteractions(berichtsobjektRepository);

    }

    @Test
    void updateBerichtsobjekt_statusChanged() {
        when(stammdatenService.loadBetriebsstaetteForRead(523L)).thenReturn(betriebsstaette);
        when(referenzenService.readReferenz(eq(Referenzliste.RRTYP), any(), any(), any())).thenReturn(euRegBST);
        when(referenzenService.readReferenz(eq(Referenzliste.RBEA), any(), any(), any())).thenReturn(inBearbeitung);
        when(berichtsobjektRepository.findByBetriebsstaetteIdAndBerichtsart(523L, euRegBST)).thenReturn(
                Optional.of(berichtsobjekt));

        ArgumentCaptor<Berichtsobjekt> captor = ArgumentCaptor.forClass(Berichtsobjekt.class);

        berichtsobjektService.updateBerichtsobjekt(523L, berichtsobjekt.getBerichtsart().getSchluessel(),
                inBearbeitung.getSchluessel(), berichtsobjekt.getBearbeitungsstatusSeit());

        verify(berichtsobjektRepository).save(captor.capture());
        Berichtsobjekt berichtsobjekt = captor.getValue();
        assertThat(berichtsobjekt).isNotNull();
        assertThat(berichtsobjekt.getBetriebsstaetteId()).isEqualTo(betriebsstaette.getId());
        assertThat(berichtsobjekt.getBerichtsart()).isSameAs(euRegBST);
        assertThat(berichtsobjekt.getBearbeitungsstatus()).isSameAs(inBearbeitung);
        assertThat(berichtsobjekt.getBearbeitungsstatusSeit()).isSameAs(TODAY);
        assertThat(berichtsobjekt.getUrl()).isEqualTo("test");

    }

    private Referenz refNotFound(InvocationOnMock call) {
        throw new ReferenzdatenNotFoundException(call.getArgument(0), call.getArgument(1), call.getArgument(3),
                call.getArgument(2));
    }

}