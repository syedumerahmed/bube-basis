package de.wps.bube.basis.stammdaten.domain.vo;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;

class LeistungTest {

    @Test
    void istGenehmigt() {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "01").build(), "bezug",
                Leistungsklasse.GENEHMIGT);
        assertTrue(l.istGenehmigt());
        assertFalse(l.istInstalliert());
        assertFalse(l.wirdBetrieben());
    }

    @Test
    void istInstalliert() {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "01").build(), "bezug",
                Leistungsklasse.INSTALLIERT);
        assertFalse(l.istGenehmigt());
        assertTrue(l.istInstalliert());
        assertFalse(l.wirdBetrieben());
    }

    @Test
    void isMegaWatt() {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "MW").ktext("MW").build(),
                "bezug", Leistungsklasse.INSTALLIERT);
        assertTrue(l.isMegaWatt());
    }

    @Test
    void isNotMegaWatt() {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "01").ktext("W").build(), "bezug",
                Leistungsklasse.INSTALLIERT);
        assertFalse(l.isMegaWatt());
    }

    @ParameterizedTest
    @ValueSource(strings = { "f", "W", "Feuerungswaermeleistung" })
    void isNoFWL(String bezug) {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "01").build(), bezug,
                Leistungsklasse.INSTALLIERT);
        assertFalse(l.isFWL());
    }

    @ParameterizedTest
    @ValueSource(strings = { "fwl", "FWL", " FWL ", "Feuerungswärmeleistung", "FeuerungswärmeLEISTUNG" })
    void isFWL(String bezug) {
        Leistung l = new Leistung(0.0, ReferenzBuilder.aReferenz(Referenzliste.REINH, "01").build(), bezug,
                Leistungsklasse.INSTALLIERT);
        assertTrue(l.isFWL());
    }

}