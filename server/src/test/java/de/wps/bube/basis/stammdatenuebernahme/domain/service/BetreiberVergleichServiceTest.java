package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.AdresseVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetreiberVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;
import de.wps.bube.basis.test.BubeIntegrationTest;

class BetreiberVergleichServiceTest extends BubeIntegrationTest {

    public static final Adresse ADRESSE = AdresseBuilder.aFullAdresse().build();
    private BetreiberVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        vergleichService = new BetreiberVergleichService(new CommonVergleichService());
        Referenz iso = ADRESSE.getLandIsoCode();
        iso.setId(99L);
        ADRESSE.setLandIsoCode(iso);
    }

    @Test
    void vergleicheGleich() {
        Betreiber betreiber = BetreiberBuilder.aFullBetreiber().adresse(ADRESSE).id(15L).build();

        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiber);
        spbBetreiber.setId(16L);

        SpbBetreiberVergleich vergleich = vergleichService.vergleiche(betreiber, spbBetreiber);

        assertThat(vergleich.getId()).isEqualTo(betreiber.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbBetreiber.getId());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isEqualTo(name.getSpl());

        AdresseVergleich adresseVergleich = vergleich.getAdresse();

        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpl()).isEqualTo(strasse.getSpb());

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpl()).isEqualTo(hausNr.getSpb());

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpl()).isEqualTo(ort.getSpb());

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpl()).isEqualTo(ortsteil.getSpb());

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpl()).isEqualTo(plz.getSpb());

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpl()).isEqualTo(postfach.getSpb());

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpl()).isEqualTo(postfachPlz.getSpb());

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpl()).isEqualTo(landIsoCode.getSpb());

        assertThat(name.isEqual()).isTrue();
        assertThat(strasse.isEqual()).isTrue();
        assertThat(hausNr.isEqual()).isTrue();
        assertThat(ort.isEqual()).isTrue();
        assertThat(ortsteil.isEqual()).isTrue();
        assertThat(plz.isEqual()).isTrue();
        assertThat(postfach.isEqual()).isTrue();
        assertThat(postfachPlz.isEqual()).isTrue();
        assertThat(landIsoCode.isEqual()).isTrue();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheUnterschiedlich() {

        Betreiber betreiber = BetreiberBuilder.aFullBetreiber().adresse(ADRESSE).id(15L).build();

        Betreiber betreiberAnders = BetreiberBuilder.aBetreiber()
                                                    .adresse(AdresseBuilder.anAdresse()
                                                                           .strasse("andereStrasse")
                                                                           .plz("15")
                                                                           .hausNr("15")
                                                                           .ort("15")
                                                                           .ortsteil("15")
                                                                           .postfach("15")
                                                                           .postfachPlz("15")
                                                                           .landIsoCode(ReferenzBuilder.aReferenz(
                                                                                                               Referenzliste.RJAHR, "a")
                                                                                                       .id(6541L)
                                                                                                       .build())
                                                                           .build()
                                                    )
                                                    .bemerkung("test")
                                                    .betreiberNummer("798")
                                                    .ersteErfassung(Instant.now())
                                                    .jahr(ReferenzBuilder.aJahrReferenz(16).build())
                                                    .land(Land.NORDRHEIN_WESTFALEN)
                                                    .name("test").build();

        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiberAnders);
        spbBetreiber.setId(16L);

        SpbBetreiberVergleich vergleich = vergleichService.vergleiche(betreiber, spbBetreiber);

        assertThat(vergleich.getId()).isEqualTo(betreiber.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbBetreiber.getId());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        AdresseVergleich adresseVergleich = vergleich.getAdresse();

        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpl()).isNotEqualTo(strasse.getSpb());

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpl()).isNotEqualTo(hausNr.getSpb());

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpl()).isNotEqualTo(ort.getSpb());

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpl()).isNotEqualTo(ortsteil.getSpb());

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpl()).isNotEqualTo(plz.getSpb());

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpl()).isNotEqualTo(postfach.getSpb());

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpl()).isNotEqualTo(postfachPlz.getSpb());

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpl()).isNotEqualTo(landIsoCode.getSpb());

        assertThat(name.isEqual()).isFalse();
        assertThat(strasse.isEqual()).isFalse();
        assertThat(hausNr.isEqual()).isFalse();
        assertThat(ort.isEqual()).isFalse();
        assertThat(ortsteil.isEqual()).isFalse();
        assertThat(plz.isEqual()).isFalse();
        assertThat(postfach.isEqual()).isFalse();
        assertThat(postfachPlz.isEqual()).isFalse();
        assertThat(landIsoCode.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }

    @Test
    void vergleicheNurSPL() {

        Betreiber betreiber = BetreiberBuilder.aFullBetreiber().adresse(ADRESSE).id(15L).build();

        SpbBetreiberVergleich vergleich = vergleichService.vergleiche(betreiber, null);

        assertThat(vergleich.getId()).isEqualTo(betreiber.getId());
        assertThat(vergleich.getSpbId()).isNull();

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isEqualTo(betreiber.getName());
        assertThat(name.getSpb()).isNull();

        AdresseVergleich adresseVergleich = vergleich.getAdresse();

        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpl()).isEqualTo(betreiber.getAdresse().getStrasse());
        assertThat(strasse.getSpb()).isNull();

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpl()).isEqualTo(betreiber.getAdresse().getHausNr());
        assertThat(hausNr.getSpb()).isNull();

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpl()).isEqualTo(betreiber.getAdresse().getOrt());
        assertThat(ort.getSpb()).isNull();

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpl()).isEqualTo(betreiber.getAdresse().getOrtsteil());
        assertThat(ortsteil.getSpb()).isNull();

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpl()).isEqualTo(betreiber.getAdresse().getPlz());
        assertThat(plz.getSpb()).isNull();

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpl()).isEqualTo(betreiber.getAdresse().getPostfach());
        assertThat(postfach.getSpb()).isNull();

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpl()).isEqualTo(betreiber.getAdresse().getPostfachPlz());
        assertThat(postfachPlz.getSpb()).isNull();

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpl().getSchluessel()).isEqualTo(
                betreiber.getAdresse().getLandIsoCode().getSchluessel());
        assertThat(landIsoCode.getSpb()).isNull();

        assertThat(name.isEqual()).isFalse();
        assertThat(strasse.isEqual()).isFalse();
        assertThat(hausNr.isEqual()).isFalse();
        assertThat(ort.isEqual()).isFalse();
        assertThat(ortsteil.isEqual()).isFalse();
        assertThat(plz.isEqual()).isFalse();
        assertThat(postfach.isEqual()).isFalse();
        assertThat(postfachPlz.isEqual()).isFalse();
        assertThat(landIsoCode.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheNurSPB() {

        Betreiber betreiber = BetreiberBuilder.aFullBetreiber().adresse(ADRESSE).id(15L).build();

        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiber);
        spbBetreiber.setId(16L);

        SpbBetreiberVergleich vergleich = vergleichService.vergleiche(null, spbBetreiber);

        assertThat(vergleich.getId()).isNull();
        assertThat(vergleich.getSpbId()).isEqualTo(spbBetreiber.getId());

        StringVector name = vergleich.getName();
        assertThat(name.getSpb()).isEqualTo(spbBetreiber.getName());
        assertThat(name.getSpl()).isNull();

        AdresseVergleich adresseVergleich = vergleich.getAdresse();

        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpb()).isEqualTo(spbBetreiber.getAdresse().getStrasse());
        assertThat(strasse.getSpl()).isNull();

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpb()).isEqualTo(spbBetreiber.getAdresse().getHausNr());
        assertThat(hausNr.getSpl()).isNull();

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpb()).isEqualTo(spbBetreiber.getAdresse().getOrt());
        assertThat(ort.getSpl()).isNull();

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpb()).isEqualTo(spbBetreiber.getAdresse().getOrtsteil());
        assertThat(ortsteil.getSpl()).isNull();

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpb()).isEqualTo(spbBetreiber.getAdresse().getPlz());
        assertThat(plz.getSpl()).isNull();

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpb()).isEqualTo(spbBetreiber.getAdresse().getPostfach());
        assertThat(postfach.getSpl()).isNull();

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpb()).isEqualTo(spbBetreiber.getAdresse().getPostfachPlz());
        assertThat(postfachPlz.getSpl()).isNull();

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpb().getSchluessel()).isEqualTo(
                spbBetreiber.getAdresse().getLandIsoCode().getSchluessel());
        assertThat(landIsoCode.getSpl()).isNull();

        assertThat(name.isEqual()).isFalse();
        assertThat(strasse.isEqual()).isFalse();
        assertThat(hausNr.isEqual()).isFalse();
        assertThat(ort.isEqual()).isFalse();
        assertThat(ortsteil.isEqual()).isFalse();
        assertThat(plz.isEqual()).isFalse();
        assertThat(postfach.isEqual()).isFalse();
        assertThat(postfachPlz.isEqual()).isFalse();
        assertThat(landIsoCode.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }
}
