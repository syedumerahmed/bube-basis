package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import de.wps.bube.basis.base.xml.StreamingXMLEntityWriter;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.web.openapi.model.ExportiereStammdatenCommand;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.jobs.domain.JobsException;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.xml.BetriebsstaetteXMLMapper;
import de.wps.bube.basis.stammdaten.xml.StammdatenImportException;

@ExtendWith(MockitoExtension.class)
class StammdatenImportExportServiceTest {

    @Mock
    private StammdatenService stammdatenService;

    @Mock
    private XMLService xmlService;

    @Mock
    private BetriebsstaetteXMLMapper betriebsstaetteXMLMapper;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private AktiverBenutzer bearbeiter;

    @Mock
    private DateiService dateiService;

    @Mock
    private ParameterService parameterService;

    @Mock
    private ReferenzenService referenzenService;

    private StammdatenImportExportService stammdatenImportExportService;

    @BeforeEach
    void setUp() {
        this.stammdatenImportExportService = new StammdatenImportExportService(stammdatenService, xmlService,
                betriebsstaetteXMLMapper, applicationEventPublisher, dateiService, bearbeiter, parameterService, referenzenService);
    }

    @Test
    void exportStammdatenWithBetriebsstaetteInDifferentLandShouldThrowException() {
        ExportiereStammdatenCommand exportiereStammdatenCommand = new ExportiereStammdatenCommand() {{
            setBetriebsstaetten(List.of(1L, 2L));
        }};
        when(bearbeiter.getLand()).thenReturn(Land.DEUTSCHLAND);
        when(stammdatenService.haveSameLand(exportiereStammdatenCommand.getBetriebsstaetten())).thenReturn(false);
        ByteArrayOutputStream outputStream = Mockito.mock(ByteArrayOutputStream.class);
        assertThrows(JobsException.class,
                () -> stammdatenImportExportService.exportStammdaten(outputStream,
                        exportiereStammdatenCommand.getBetriebsstaetten()));

    }

    @SuppressWarnings("unchecked")
    @Test
    void exportStammdatenWithBetriebsstaetteInSameLandShouldResolveBetriebsstaettenAndWriteOnXmlService() {
        ExportiereStammdatenCommand exportiereStammdatenCommand = new ExportiereStammdatenCommand() {{
            setBetriebsstaetten(List.of(1L, 2L));
        }};
        when(bearbeiter.getLand()).thenReturn(Land.DEUTSCHLAND);
        when(stammdatenService.haveSameLand(exportiereStammdatenCommand.getBetriebsstaetten())).thenReturn(true);
        Betriebsstaette betriebsstaette1 = BetriebsstaetteBuilder.aBetriebsstaette(1L).build();
        Betriebsstaette betriebsstaette2 = BetriebsstaetteBuilder.aBetriebsstaette(2L).build();
        when(stammdatenService.loadBetriebsstaetteForRead(1L)).thenReturn(betriebsstaette1);
        when(stammdatenService.loadBetriebsstaetteForRead(2L)).thenReturn(betriebsstaette2);
        ByteArrayOutputStream outputStream = Mockito.mock(ByteArrayOutputStream.class);
        ArgumentCaptor<Stream<Betriebsstaette>> streamArgumentCaptor = ArgumentCaptor.forClass(Stream.class);

        var writerMock = mock(StreamingXMLEntityWriter.class);

        when(xmlService.createWriter(eq(BetriebsstaetteType.class), eq(BetriebsstaettenXDto.QUALIFIED_NAME),any(),any()))
                .thenReturn(writerMock);

        stammdatenImportExportService.exportStammdaten(outputStream, exportiereStammdatenCommand.getBetriebsstaetten());

        verify(writerMock).write(streamArgumentCaptor.capture(), eq(outputStream));


        assertThat(streamArgumentCaptor.getValue()).containsExactly(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void importiereBetriebsstaetteWithNonMatchingBerichtsjahrShouldNotImportAndWriteJobProtokoll() {
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                Gueltigkeitsjahr.of(2019),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());

        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        when(bearbeiter.getLand()).thenReturn(HAMBURG);
        assertThrows(StammdatenImportException.class,
                () -> stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2019", jobProtokoll));

        verifyNoInteractions(stammdatenService);
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr falsches Berichtsjahr");
        verifyNoMoreInteractions(jobProtokoll);
    }

    @Test
    void importiereBetriebsstaetteWithMatchingJahrAndLandShouldBeValid() {
        when(bearbeiter.getLand()).thenReturn(HAMBURG);
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .land(HAMBURG)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020", jobProtokoll);

        verify(stammdatenService).createBetriebsstaette(betriebsstaette.entity);
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr. Fehlerfrei und angelegt");
    }

    @Test
    void importiereBetriebsstaetteWithNonMatchingLandShouldNotImportAndWriteProtokoll() {
        when(bearbeiter.getLand()).thenReturn(HAMBURG);
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .land(Land.BERLIN)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        assertThrows(StammdatenImportException.class,
                () -> stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020", jobProtokoll));

        verifyNoInteractions(stammdatenService);
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr falsches Land");
        verifyNoMoreInteractions(jobProtokoll);
    }

    @Test
    void validiereImportShouldShowMultipleErrors() {
        var name = "Test";
        var nr = "BsNr";
        var error1 = new MappingError("name, nr", "Fehler Referenz 1");
        var error2 = new MappingError("name, nr", "Fehler Referenz 2");
        var falschesJahr = format("Betriebsstätte %s Nr. %s falsches Berichtsjahr", name, nr);
        var falschesLand = format("Betriebsstätte %s Nr. %s falsches Land", name, nr);

        when(bearbeiter.getLand()).thenReturn(HAMBURG);
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr(nr)
                                      .name(name)
                                      .land(Land.BERLIN)
                                      .berichtsjahr(aJahrReferenz(2019).build())
                                      .build(), List.of(error1, error2));
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                Gueltigkeitsjahr.of(2020),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);

        assertThrows(StammdatenImportException.class,
                () -> stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020", jobProtokoll));

        verifyNoInteractions(stammdatenService);
        verify(jobProtokoll).addEintrag(falschesJahr);
        verify(jobProtokoll).addEintrag(falschesLand);
        verify(jobProtokoll).addEintrag(error1);
        verify(jobProtokoll).addEintrag(error2);
        verifyNoMoreInteractions(jobProtokoll);
    }

    @Test
    void importiereBetriebsstaetteWithGesamtadminAnyLandShouldBeValid() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .land(Land.BERLIN)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020", jobProtokoll);

        verify(stammdatenService).createBetriebsstaette(betriebsstaette.entity);
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr. Fehlerfrei und angelegt");
    }

    @Test
    void importiereBetriebsstaetteShouldRemoveExistingBetriebstaetteBeforeCreation() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        var betriebsstaetteBuilder = BetriebsstaetteBuilder.aBetriebsstaette()
                                                           .betriebsstaetteNr("BsNr")
                                                           .name("Test")
                                                           .land(HAMBURG)
                                                           .berichtsjahr(aJahrReferenz(2020).build());
        Betriebsstaette betriebsstaette = betriebsstaetteBuilder.build();
        when(stammdatenService.findBetriebsstaetteByJahrLandNummer(betriebsstaette.getBerichtsjahr().getSchluessel(),
                betriebsstaette.getLand(), betriebsstaette.getBetriebsstaetteNr())).thenReturn(
                Optional.of(betriebsstaetteBuilder.id(1L).build()));
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        stammdatenImportExportService.importiereBetriebsstaette(new EntityWithError<>(betriebsstaette), "2020",
                jobProtokoll);

        verify(stammdatenService).deleteBetriebsstaette(1L);
        verify(stammdatenService).createBetriebsstaette(betriebsstaette);

        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr. Fehlerfrei und angelegt");
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr bereits vorhanden. Gelöscht");
    }

    @Test
    void importiereBetriebsstaetteShouldNotRemoveExistingBetriebstaetteBeforeCreation() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette()
                                                                .betriebsstaetteNr("BsNr")
                                                                .name("Test")
                                                                .land(HAMBURG)
                                                                .berichtsjahr(aJahrReferenz(2020).build())
                                                                .build();
        when(stammdatenService.findBetriebsstaetteByJahrLandNummer(betriebsstaette.getBerichtsjahr().getSchluessel(),
                betriebsstaette.getLand(), betriebsstaette.getBetriebsstaetteNr())).thenReturn(Optional.empty());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(), betriebsstaette.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());

        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        stammdatenImportExportService.importiereBetriebsstaette(new EntityWithError<>(betriebsstaette), "2020",
                jobProtokoll);

        verify(stammdatenService, never()).deleteBetriebsstaette(1L);
        verify(jobProtokoll, never()).addEintrag("Betriebsstätte Test Nr. BsNr bereits vorhanden. Gelöscht");
        verify(jobProtokoll).addEintrag("Betriebsstätte Test Nr. BsNr. Fehlerfrei und angelegt");
        verify(stammdatenService).createBetriebsstaette(betriebsstaette);
    }

    @Test
    void importiereBetriebsstaetteWithBetreiberShouldCreateBetreiber() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        Betreiber betreiber = BetreiberBuilder.aBetreiber()
                                              .jahr(aJahrReferenz(2020).build())
                                              .land(HAMBURG)
                                              .betreiberNummer("BtNr")
                                              .build();
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .betreiber(betreiber)
                                      .land(HAMBURG)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(stammdatenService.findBetreiberByJahrLandNummer(betreiber.getBerichtsjahr().getSchluessel(),
                betreiber.getLand(), betreiber.getBetreiberNummer())).thenReturn(Optional.empty());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());

        stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020",
                new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING));

        verify(stammdatenService).createBetreiber(betreiber);
    }

    @Test
    void importiereBetriebsstaetteWithBetreiberShouldUpdateBetreiber() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        var betreiberBuilder = BetreiberBuilder.aBetreiber()
                                               .jahr(aJahrReferenz(2020).build())
                                               .land(HAMBURG)
                                               .betreiberNummer("BtNr");
        Betreiber betreiber = betreiberBuilder.build();
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .betreiber(betreiber)
                                      .land(HAMBURG)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(stammdatenService.findBetreiberByJahrLandNummer(betreiber.getBerichtsjahr().getSchluessel(),
                betreiber.getLand(), betreiber.getBetreiberNummer())).thenReturn(
                Optional.of(betreiberBuilder.id(1L).build()));
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());

        stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020",
                new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING));

        verify(stammdatenService).updateBetreiber(betreiber);
    }

    @Test
    void importiereBetriebsstaetteWithBetreiberShouldUpdateBetreiberJahrAndLandAccordingToBetriebsstaette() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        Betreiber betreiber = BetreiberBuilder.aBetreiber()
                                              .jahr(aJahrReferenz(2012).build())
                                              .land(Land.BERLIN)
                                              .betreiberNummer("BtNr")
                                              .build();
        EntityWithError<Betriebsstaette> betriebsstaette = new EntityWithError<>(
                BetriebsstaetteBuilder.aBetriebsstaette()
                                      .betriebsstaetteNr("BsNr")
                                      .name("Test")
                                      .betreiber(betreiber)
                                      .land(HAMBURG)
                                      .berichtsjahr(aJahrReferenz(2020).build())
                                      .build());
        when(stammdatenService.findBetreiberByJahrLandNummer(any(), any(), any())).thenReturn(Optional.empty());
        when(parameterService.readParameterWert(Parameter.QUELLE_ZUORDNUNG,
                betriebsstaette.entity.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.entity.getLand())).thenReturn(
                ParameterWertBuilder.aParameterWert().wert(ParameterWert.QUELLE_ZUORDNUNG_BST).build());

        stammdatenImportExportService.importiereBetriebsstaette(betriebsstaette, "2020",
                new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING));

        verify(stammdatenService).createBetreiber(
                BetreiberBuilder.aBetreiber().jahr(aJahrReferenz(2020).build()).land(HAMBURG).build());
    }

}
