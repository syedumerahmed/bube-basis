package de.wps.bube.basis.euregistry.domain.vo;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBerichtsartReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;

import java.time.LocalDate;

import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public class BerichtsdatenBuilder {

    private Behoerde zustaendigeBehoerde;
    private String akz;
    private Referenz berichtsart;
    private Referenz bearbeitungsstatus;
    private LocalDate bearbeitungsstatusSeit;
    private String bemerkung;
    private String thematicId;
    private String schema;

    public static BerichtsdatenBuilder anEuRegAnlBerichtsdaten() {
        return aFullBerichtsdaten().berichtsart(aBerichtsartReferenz(Berichtsdaten.EUREG_ANL_SCHLUESSEL).build());
    }

    public static BerichtsdatenBuilder anEuRegFBerichtsdaten() {
        return aFullBerichtsdaten().berichtsart(aBerichtsartReferenz(Berichtsdaten.EUREG_F_SCHLUESSEL).build());
    }

    public static BerichtsdatenBuilder aFullBerichtsdaten() {
        return new BerichtsdatenBuilder().zustaendigeBehoerde(BehoerdeBuilder.aBehoerde("1234").id(1234L).build())
                                         .akz("akz")
                                         .berichtsart(aBerichtsartReferenz("ART01").build())
                                         .bearbeitungsstatus(aReferenz(RBEA, "BS01").build())
                                         .bearbeitungsstatusSeit(LocalDate.parse("2021-08-10"))
                                         .bemerkung("bemerkung")
                                         .thematicId("thematicId")
                                         .schema("schema");
    }

    public BerichtsdatenBuilder zustaendigeBehoerde(Behoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerde = zustaendigeBehoerde;
        return this;
    }

    public BerichtsdatenBuilder akz(String akz) {
        this.akz = akz;
        return this;
    }

    public BerichtsdatenBuilder berichtsart(Referenz berichtsart) {
        this.berichtsart = berichtsart;
        return this;
    }

    public BerichtsdatenBuilder bearbeitungsstatus(Referenz bearbeitungsstatus) {
        this.bearbeitungsstatus = bearbeitungsstatus;
        return this;
    }

    public BerichtsdatenBuilder bearbeitungsstatusSeit(LocalDate bearbeitungsstatusSeit) {
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
        return this;
    }

    public BerichtsdatenBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public BerichtsdatenBuilder thematicId(String thematicId) {
        this.thematicId = thematicId;
        return this;
    }

    public BerichtsdatenBuilder schema(String schema) {
        this.schema = schema;
        return this;
    }

    public Berichtsdaten build() {
        Berichtsdaten b = new Berichtsdaten(zustaendigeBehoerde, akz, berichtsart, bearbeitungsstatus, bearbeitungsstatusSeit);
        b.setBemerkung(bemerkung);
        b.setThematicId(thematicId);
        b.setSchema(schema);
        return b;
    }
}
