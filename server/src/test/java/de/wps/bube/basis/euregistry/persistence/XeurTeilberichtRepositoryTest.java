package de.wps.bube.basis.euregistry.persistence;

import static de.wps.bube.basis.base.vo.Land.*;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder.aXeurTeilbericht;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.anEuRegAnlBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class XeurTeilberichtRepositoryTest {

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;

    @Autowired
    private EURegBetriebsstaetteRepository euRegBetriebsstaetteRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Referenz jahr2019;
    private Referenz jahr2020;
    private Referenz jahr2021;
    private List<EURegBetriebsstaette> bstBerichte;

    @BeforeEach
    void setUp() {
        jahr2019 = testEntityManager.persist(aJahrReferenz(2019).build());
        jahr2020 = testEntityManager.persist(aJahrReferenz(2020).build());
        jahr2021 = testEntityManager.persist(aJahrReferenz(2021).build());
        Behoerde behoerde = testEntityManager.persist(aBehoerde("ZuBehörde").build());
        var bstBuilder = aBetriebsstaette().berichtsjahr(jahr2020)
                                           .gemeindekennziffer(jahr2020)
                                           .zustaendigeBehoerde(behoerde)
                                           .betriebsstatus(jahr2020);
        var betriebsstaetten = betriebsstaetteRepository.saveAll(List.of(
                bstBuilder.betriebsstaetteNr("1").localId("local1").build(),
                bstBuilder.betriebsstaetteNr("2").localId("local2").build(),
                bstBuilder.betriebsstaetteNr("3").localId("local3").build()
        ));
        var euRegBetriebsstaetteBuilder = aEURegBetriebsstaette().berichtsdaten(
                anEuRegAnlBerichtsdaten().zustaendigeBehoerde(behoerde)
                                         .berichtsart(jahr2020)
                                         .bearbeitungsstatus(jahr2020)
                                         .build());
        bstBerichte = euRegBetriebsstaetteRepository.saveAll(betriebsstaetten
                .stream()
                .map(b -> euRegBetriebsstaetteBuilder.betriebsstaette(b).build())
                .toList());
    }

    @Test
    void testSave() {
        var xeurTeilbericht = new XeurTeilbericht(jahr2020, HAMBURG);
        xeurTeilbericht.setXeurFile("<xeur />");
        xeurTeilbericht.setProtokoll("protokoll");
        xeurTeilbericht.setLocalIds(bstBerichte.stream().map(EURegBetriebsstaette::getLocalId).toList());

        var persistedXeurTeilbericht = testEntityManager.persistFlushFind(xeurTeilbericht);

        assertThat(persistedXeurTeilbericht).isNotNull();
        assertThat(persistedXeurTeilbericht.getId()).isNotNull();
        assertThat(persistedXeurTeilbericht.getLocalIds())
                .containsExactlyInAnyOrderElementsOf(xeurTeilbericht.getLocalIds());
    }

    @Nested
    class FindMethods {

        @Autowired
        XeurTeilberichtRepository xeurTeilberichtRepository;

        private XeurTeilbericht bremen2020;
        private XeurTeilbericht hessen2020;

        private XeurTeilbericht berlin2021Yesterday;
        private XeurTeilbericht hessen2021;
        private XeurTeilbericht hessen2021Yesterday;
        private XeurTeilbericht hessen2021Today;

        @BeforeEach
        void setUp() {

            Instant today = Instant.now();
            Instant yesterday = Instant.now().minus(1, ChronoUnit.DAYS);

            bremen2020 = xeurTeilberichtRepository.save(aXeurTeilbericht().land(BREMEN).jahr(jahr2020).build());
            hessen2020 = xeurTeilberichtRepository.save(aXeurTeilbericht().land(HESSEN).jahr(jahr2020).build());

            berlin2021Yesterday = xeurTeilberichtRepository.save(
                    aXeurTeilbericht().land(BERLIN).jahr(jahr2021).freigegebenAt(yesterday).build());
            hessen2021 = xeurTeilberichtRepository.save(aXeurTeilbericht().land(HESSEN).jahr(jahr2021).build());
            hessen2021Yesterday = xeurTeilberichtRepository.save(
                    aXeurTeilbericht().land(HESSEN).jahr(jahr2021).freigegebenAt(yesterday).build());
            hessen2021Today = xeurTeilberichtRepository.save(
                    aXeurTeilbericht().land(HESSEN).jahr(jahr2021).freigegebenAt(today).build());
        }

        @Test
        void findAllByLandAndJahr() {

            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BAYERN, jahr2019)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BERLIN, jahr2019)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BREMEN, jahr2019)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(HESSEN, jahr2019)).isEmpty();

            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BAYERN, jahr2020)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BERLIN, jahr2020)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BREMEN, jahr2020)).containsExactly(bremen2020);
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(HESSEN, jahr2020)).containsExactly(hessen2020);

            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BAYERN, jahr2021)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BERLIN, jahr2021)).containsExactly(
                    berlin2021Yesterday);
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(BREMEN, jahr2021)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByLandAndJahr(HESSEN, jahr2021)).containsExactlyInAnyOrder(
                    hessen2021, hessen2021Yesterday, hessen2021Today);

        }

        @Test
        void findAllByJahr() {

            assertThat(xeurTeilberichtRepository.findAllByJahr(jahr2019)).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllByJahr(jahr2020)).containsExactlyInAnyOrder(
                    bremen2020, hessen2020);
            assertThat(xeurTeilberichtRepository.findAllByJahr(jahr2021)).containsExactlyInAnyOrder(
                    berlin2021Yesterday, hessen2021, hessen2021Yesterday, hessen2021Today);

        }

        @Test
        void findAllLatestFreigegeben() {
            assertThat(xeurTeilberichtRepository.findAllLatestFreigegeben(jahr2019.getId())).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllLatestFreigegeben(jahr2020.getId())).isEmpty();
            assertThat(xeurTeilberichtRepository.findAllLatestFreigegeben(jahr2021.getId())).containsExactly(
                    hessen2021Today, berlin2021Yesterday
            );

        }

    }


}
