package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegAnl;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegF;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.aFullAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.aFullAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.xmlunit.builder.Input;

import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.euregistry.xml.EURegBstXmlMapperImpl;
import de.wps.bube.basis.euregistry.xml.EuRegBetriebsstaetteImportDtoMapper;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@SpringBootTest(classes = { EURegistryImportExportService.class, XMLService.class, EURegBstXmlMapperImpl.class })
class EURegistryImportExportServiceIntegrationTest {

    @MockBean
    private EURegistryService euRegistryService;
    @MockBean
    private AktiverBenutzer aktiverBenutzer;
    @MockBean
    private DateiService dateiService;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BehoerdenService behoerdenService;
    @MockBean
    private ReferenzResolverService referenzResolverService;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private EuRegBetriebsstaetteImportDtoMapper euRegBetriebsstaetteImportDtoMapper;

    @Autowired
    private EURegistryImportExportService service;

    @Test
    void export_fullBericht() throws IOException {
        var expectedXML = Input.fromStream(getResource("xml/euregistry/export_fullBericht.xml"));
        var ids = List.of(1L);


        var installationEuRegAnl = aFullAnlage().id(2L).anlageNr("I ERA").build();
        var installationEuRegF = aFullAnlage().id(3L).anlageNr("I ERF").add17BimschvVorschrift().build();
        var installationOhne = aFullAnlage().id(4L).anlageNr("I ohne").build();

        Stream.of(installationEuRegAnl, installationOhne).forEach(installation ->
                installation.setAnlagenteile(List.of(
                        aFullAnlagenteil().id(installation.getId() * 5L).anlagenteilNr("IP ERA").build(),
                        aFullAnlagenteil().id(installation.getId() * 7L).anlagenteilNr("IP ERF").add17BimschvVorschrift().build(),
                        aFullAnlagenteil().id(installation.getId() * 11L).anlagenteilNr("IP ohne").build())));

        var euRegBetriebsstaette = aEURegBetriebsstaette()
                .betriebsstaette(aFullBetriebsstaette()
                        .anlagen(installationEuRegAnl, installationEuRegF, installationOhne)
                        .build())
                .build();

        when(euRegistryService.haveSameJahr(ids)).thenReturn(true);
        when(euRegistryService.streamAllByBerichtIdIn(ids)).thenReturn(Stream.of(euRegBetriebsstaette));

        when(euRegistryService.findAnlagenBerichtByAnlageId(installationEuRegAnl.getId()))
                .thenReturn(Optional.of(aFullEURegAnl().anlage(installationEuRegAnl).build()));
        when(euRegistryService.findAnlagenBerichtByAnlageId(installationEuRegF.getId()))
                .thenReturn(Optional.of(aFullEURegF().anlage(installationEuRegF).build()));
        when(euRegistryService.findAnlagenBerichtByAnlageId(installationOhne.getId()))
                .thenReturn(Optional.empty());

        var anlagenteilAnl = installationEuRegAnl.getAnlagenteile().get(0);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilAnl.getId()))
                .thenReturn(Optional.of(aFullEURegAnl().anlagenteil(anlagenteilAnl).build()));
        var anlagenteilF = installationEuRegAnl.getAnlagenteile().get(1);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilF.getId()))
                .thenReturn(Optional.of(aFullEURegF().anlagenteil(anlagenteilF).build()));
        var anlagenteilOhne = installationEuRegAnl.getAnlagenteile().get(2);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilOhne.getId()))
                .thenReturn(Optional.empty());

        var anlagenteilOhneAnl = installationOhne.getAnlagenteile().get(0);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilOhneAnl.getId()))
                .thenReturn(Optional.of(aFullEURegAnl().anlagenteil(anlagenteilOhneAnl).build()));
        var anlagenteilOhneF = installationOhne.getAnlagenteile().get(1);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilOhneF.getId()))
                .thenReturn(Optional.of(aFullEURegF().anlagenteil(anlagenteilOhneF).build()));
        var anlagenteilOhneOhne = installationOhne.getAnlagenteile().get(2);
        when(euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilOhneOhne.getId()))
                .thenReturn(Optional.empty());

        var outputStream = new ByteArrayOutputStream();
        service.export(outputStream, ids);

//        org.apache.commons.io.FileUtils.writeByteArrayToFile(
//                org.apache.commons.io.FileUtils.getFile("src/test/resources/xml/euregistry/export_fullBericht.xml"),
//                outputStream.toByteArray());
        assertThat(Input.fromByteArray(outputStream.toByteArray()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
    }
}
