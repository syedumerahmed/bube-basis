package de.wps.bube.basis.referenzdaten.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLImportException;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.xml.referenz.dto.ReferenzlisteXDto;

@SpringBootTest(classes = { ReferenzXMLMapperImpl.class, CommonXMLMapperImpl.class })
class ReferenzXMLServiceReadTest {

    private static final String VALID_XML = "xml/referenzdaten_import_test.xml";
    private static final String INVALID_XML = "xml/invalid_referenzdaten_import_test.xml";
    private static final String INVALID_ELEMENT_XML = "xml/invalid_element_referenzdaten_import_test.xml";

    @Autowired
    private ReferenzXMLMapper referenzMapper;

    private final XMLService xmlService = new XMLService();

    @Test
    void test_validXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(VALID_XML), ReferenzlisteXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(0);
    }

    @Test
    void read_invalidXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_XML), ReferenzlisteXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(2);
    }

    @Test
    void read_invalidElementXml() throws IOException {

        List<Validierungshinweis> hinweise = new ArrayList<>();

        // Ungültig strukturiertes XML wird korrekt validiert ...
        xmlService.validate(getResource(INVALID_ELEMENT_XML), ReferenzlisteXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).isEmpty();

        // ... aber nicht importiert
        XMLImportException xmlImportException = assertThrows(XMLImportException.class,
                () -> xmlService.read(getResource(INVALID_ELEMENT_XML), ReferenzType.class, referenzMapper,
                        ReferenzlisteXDto.XML_ROOT_ELEMENT_NAME, null,
                        e -> fail("Validation expected to fail before import")));
        assertThat(xmlImportException.getMessage()).startsWith("Fehler beim Import: Erwartet: <referenzliste>;");
    }

    @Test
    void read_validXml() throws IOException {
        xmlService.read(getResource(VALID_XML), ReferenzType.class, referenzMapper,
                ReferenzlisteXDto.XML_ROOT_ELEMENT_NAME, null, rWithError -> {
                    var referenz = rWithError.entity;
                    assertThat(referenz).isNotNull();
                    assertEquals(Land.DEUTSCHLAND, referenz.getLand());
                    assertEquals(Referenzliste.REPSG, referenz.getReferenzliste());
                    assertEquals("0001", referenz.getSchluessel());
                    assertEquals("0001", referenz.getSortier());
                    assertEquals("0001", referenz.getKtext());
                    assertEquals(2020, referenz.getGueltigVon().getJahr());
                    assertEquals(2030, referenz.getGueltigBis().getJahr());
                    assertEquals("eu", referenz.getEu());
                    assertEquals("strg", referenz.getStrg());
                    assertEquals("2020-06-30T14:00:00Z", referenz.getLetzteAenderung().toString());
                    assertEquals("1234567890", referenz.getReferenznummer());
                });
    }

}
