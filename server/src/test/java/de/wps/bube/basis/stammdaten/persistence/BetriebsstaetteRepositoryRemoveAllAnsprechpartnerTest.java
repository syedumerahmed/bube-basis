package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class BetriebsstaetteRepositoryRemoveAllAnsprechpartnerTest {

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;

    @Autowired
    private ReferenzRepository referenzRepository;

    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, typKommunikationsverbindung, betriebsstatus;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("HHN0UHL").build());
        typKommunikationsverbindung = referenzRepository.save(aKommunikationsTypReferenz("Tel").build());
    }

    @Test
    public void testDeleteByRemoveFromList() {

        // SetUp
        Betriebsstaette b = this.getDefaultBetriebsstaette("1");
        b = betriebsstaetteRepository.save(b);

        // Test
        assertEquals(4, b.getAnsprechpartner().size());
        assertEquals(1, b.getAnsprechpartner().get(0).getKommunikationsverbindungen().size());
        assertEquals(1, b.getAnsprechpartner().get(1).getKommunikationsverbindungen().size());
        assertEquals(1, b.getAnsprechpartner().get(2).getKommunikationsverbindungen().size());
        assertEquals(1, b.getAnsprechpartner().get(3).getKommunikationsverbindungen().size());

        b.removeAllAnsprechpartner();
        b = betriebsstaetteRepository.save(b);

        b = betriebsstaetteRepository.findById(b.getId()).get();
        assertEquals(0, b.getAnsprechpartner().size());

    }

    @Test
    public void testFindAllByBerichtsjahr() {

        // SetUp
        for (int i = 0; i < 10; i++) {
            betriebsstaetteRepository.save(this.getDefaultBetriebsstaette(String.valueOf(i)));
        }

        // Test
        Stream<Betriebsstaette> stream = betriebsstaetteRepository.findAllByBerichtsjahr(jahr);
        assertEquals(10, stream.count());

    }

    @Test
    public void testDeleteAllByBerichtsjahr() {

        // SetUp
        for (int i = 0; i < 4; i++) {
            betriebsstaetteRepository.save(this.getDefaultBetriebsstaette(String.valueOf(i)));
        }

        // Act
        Stream<Betriebsstaette> stream = betriebsstaetteRepository.findAllByBerichtsjahr(jahr);

        stream.forEach(betriebsstaette -> {
            assertFalse(betriebsstaette.getAnsprechpartner().isEmpty());
            betriebsstaette.removeAllAnsprechpartner();
            betriebsstaetteRepository.saveAndFlush(betriebsstaette);
        });

        // Assert
        Stream<Betriebsstaette> stream2 = betriebsstaetteRepository.findAllByBerichtsjahr(jahr);

        stream2.forEach(betriebsstaette -> assertTrue(betriebsstaette.getAnsprechpartner().isEmpty()));
    }

    private Betriebsstaette getDefaultBetriebsstaette(String nr) {

        Betriebsstaette b = getDefaultBetriebsstaetteBuilder(nr).build();
        List<Ansprechpartner> liste = new ArrayList<>();
        liste.add(this.getAnsprechpartner());
        liste.add(this.getAnsprechpartner());
        liste.add(this.getAnsprechpartner());
        liste.add(this.getAnsprechpartner());
        b.setAnsprechpartner(liste);

        return b;
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder(String nr) {
        return BetriebsstaetteBuilder.aBetriebsstaette()
                                     .berichtsjahr(jahr)
                                     .land(Land.HAMBURG)
                                     .zustaendigeBehoerde(behoerde)
                                     .betriebsstatus(betriebsstatus)
                                     .betriebsstaetteNr(nr)
                                     .gemeindekennziffer(gemeindeKennziffer)
                                     .adresse(
                                             AdresseBuilder.anAdresse().strasse("Müllerweg").hausNr("2a").ort("Hamburg")
                                                           .plz("22152")
                                                           .build());
    }

    private Ansprechpartner getAnsprechpartner() {
        Ansprechpartner ap = new Ansprechpartner();
        ap.setNachname("Müller");

        List<Kommunikationsverbindung> liste = new ArrayList<>();
        liste.add(new Kommunikationsverbindung(typKommunikationsverbindung, "040/8768716831"));
        ap.setKommunikationsverbindungen(liste);

        return ap;

    }

}
