package de.wps.bube.basis.euregistry.domain.entity;

import static de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.vo.AuflageBuilder.aFullAuflage;
import static de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht.NICHT_ERFORDERLICH;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.anEuRegAnlBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.anEuRegFBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.vo.GenehmigungBuilder.aFullGenehmigung;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATC;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBATE;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRELCH;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.aFullAnlage;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import com.google.common.collect.Lists;

import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;

public class EURegAnlageBuilder {

    private Long id;
    private Anlage anlage;
    private Anlagenteil anlagenteil;
    private Berichtsdaten berichtsdaten;
    private Instant ersteErfassung;
    private Instant letzteAenderung;

    private String localId2;
    private Ausgangszustandsbericht report;
    private int visits;
    private String visitUrl;
    private String eSpirsNr;
    private String tehgNr;
    private String monitor;
    private String monitorUrl;
    private Genehmigung genehmigung;
    private List<BVTAusnahme> bvtAusnahmeList;
    private FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten;
    private List<Referenz> anwendbareBvt;
    private List<Referenz> kapitelIeRl;
    private List<Auflage> auflagen;

    public static EURegAnlageBuilder aFullEURegAnl() {
        return aFullEuRegAnlage().berichtsdaten(anEuRegAnlBerichtsdaten().build());
    }

    public static EURegAnlageBuilder aFullEURegF() {
        return aFullEuRegAnlage().feuerungsanlage(aFeuerungsanlageBerichtsdaten().build())
                                 .berichtsdaten(anEuRegFBerichtsdaten().build());
    }

    public static EURegAnlageBuilder aFullEuRegAnlage() {
        return anEURegAnlage().anlage(aFullAnlage().build())
                              .eSpirsNr("eSpirsNr")
                              .tehgNr("tehgNr")
                              .monitorUrl("http://monitor.url")
                              .localId2("localId2")
                              .bvtAusnahmen(new BVTAusnahme(
                                      aReferenz(RBATE, "bvtAusnahme").eu("bvtAusnahmeEU").build(),
                                      LocalDate.of(2021, 8, 12),
                                      "https://public.url"))
                              .anwendbareBvt(aReferenz(RBATC, "anwendbareBvt").build())
                              .auflagen(aFullAuflage().build())
                              .kapitelIeRl(aReferenz(RRELCH, "Kapitel IE-RL").build());
    }

    public static EURegAnlageBuilder anEURegAnlage() {
        return new EURegAnlageBuilder().anlage(AnlageBuilder.anAnlage().build())
                                       .ersteErfassung(Instant.parse("2021-08-09T16:48:09Z"))
                                       .letzteAenderung(Instant.parse("2021-08-10T16:48:09Z"))
                                       .visits(5)
                                       .monitor("monitor")
                                       .visitUrl("http://visit.url")
                                       .report(NICHT_ERFORDERLICH)
                                       .genehmigung(aFullGenehmigung().build());
    }

    public static EURegAnlageBuilder anEURegAnlage(Long id) {
        return anEURegAnlage().id(id);
    }

    public EURegAnlageBuilder bvtAusnahmen(BVTAusnahme... bvtAusnahmeList) {
        return bvtAusnahmen(List.of(bvtAusnahmeList));
    }

    public EURegAnlageBuilder bvtAusnahmen(List<BVTAusnahme> bvtAusnahmeList) {
        this.bvtAusnahmeList = bvtAusnahmeList;
        return this;
    }

    public EURegAnlageBuilder feuerungsanlage(FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten) {
        this.feuerungsanlageBerichtsdaten = feuerungsanlageBerichtsdaten;
        return this;
    }

    public EURegAnlageBuilder anwendbareBvt(List<Referenz> anwendbareBvt) {
        this.anwendbareBvt = anwendbareBvt;
        return this;
    }

    public EURegAnlageBuilder anwendbareBvt(Referenz... anwendbareBvt) {
        return anwendbareBvt(Lists.newArrayList(anwendbareBvt));
    }

    public EURegAnlageBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public EURegAnlageBuilder anlage(Anlage anlage) {
        this.anlage = anlage;
        this.anlagenteil = null;
        return this;
    }

    public EURegAnlageBuilder anlagenteil(Anlagenteil anlagenteil) {
        this.anlagenteil = anlagenteil;
        this.anlage = null;
        return this;
    }

    public EURegAnlageBuilder berichtsdaten(Berichtsdaten berichtsdaten) {
        this.berichtsdaten = berichtsdaten;
        return this;
    }

    public EURegAnlageBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public EURegAnlageBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public EURegAnlageBuilder localId2(String localId2) {
        this.localId2 = localId2;
        return this;
    }

    public EURegAnlageBuilder report(Ausgangszustandsbericht report) {
        this.report = report;
        return this;
    }

    public EURegAnlageBuilder visits(int visits) {
        this.visits = visits;
        return this;
    }

    public EURegAnlageBuilder visitUrl(String visitUrl) {
        this.visitUrl = visitUrl;
        return this;
    }

    public EURegAnlageBuilder eSpirsNr(String eSpirsNr) {
        this.eSpirsNr = eSpirsNr;
        return this;
    }

    public EURegAnlageBuilder tehgNr(String tehgNr) {
        this.tehgNr = tehgNr;
        return this;
    }

    public EURegAnlageBuilder monitor(String monitor) {
        this.monitor = monitor;
        return this;
    }

    public EURegAnlageBuilder monitorUrl(String monitorUrl) {
        this.monitorUrl = monitorUrl;
        return this;
    }

    public EURegAnlageBuilder genehmigung(Genehmigung genehmigung) {
        this.genehmigung = genehmigung;
        return this;
    }

    public EURegAnlageBuilder auflagen(Auflage... auflagen) {
        this.auflagen = Lists.newArrayList(auflagen);
        return this;
    }

    public EURegAnlageBuilder kapitelIeRl(Referenz... kapitelIeRl) {
        this.kapitelIeRl = Lists.newArrayList(kapitelIeRl);
        return this;
    }

    public EURegAnlage build() {
        EURegAnlage a = new EURegAnlage(anlage, anlagenteil, berichtsdaten);
        a.setId(id);
        a.setErsteErfassung(ersteErfassung);
        a.setLetzteAenderung(letzteAenderung);

        a.setBvtAusnahmen(bvtAusnahmeList);
        a.setFeuerungsanlage(feuerungsanlageBerichtsdaten);
        a.setAnwendbareBvt(anwendbareBvt);

        a.setLocalId2(localId2);
        a.setReport(report);
        a.setVisits(visits);
        a.setVisitUrl(visitUrl);
        a.seteSpirsNr(eSpirsNr);
        a.setTehgNr(tehgNr);
        a.setMonitor(monitor);
        a.setMonitorUrl(monitorUrl);
        a.setGenehmigung(genehmigung);
        a.setAuflagen(auflagen);
        a.setKapitelIeRl(kapitelIeRl);

        return a;
    }

}
