package de.wps.bube.basis.base.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import de.wps.bube.basis.base.web.openapi.model.BetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.PageDtoBetriebsstaetteListItemDto;
import de.wps.bube.basis.base.web.openapi.model.PageDtoEuRegBetriebsstaetteListItemDto;

class PageMapperTest {

    PageMapperImpl mapper;

    @BeforeEach
    void setUp() {
        mapper = new PageMapperImpl();
    }

    @Test
    void mapBetriebsstaetteListItemToDtomapBetriebsstaetteListItemToDto() {
        List<BetriebsstaetteListItemDto> betriebsstaetteListItemDtoList = new ArrayList<>();
        betriebsstaetteListItemDtoList.add(new BetriebsstaetteListItemDto());

        Page<BetriebsstaetteListItemDto> page = new PageImpl<>(betriebsstaetteListItemDtoList);

        PageDtoBetriebsstaetteListItemDto dto = mapper.mapBetriebsstaetteListItemToDto(page);

        assertThat(dto.getContent()).isEqualTo(page.getContent());
        assertThat(dto.getTotalElements()).isEqualTo(page.getTotalElements());
    }

    @Test
    void mapEuRegBetriebsstaetteListItemToDto() {
        List<EuRegBetriebsstaetteListItemDto> betriebsstaetteListItemDtoList = new ArrayList<>();
        betriebsstaetteListItemDtoList.add(new EuRegBetriebsstaetteListItemDto());

        Page<EuRegBetriebsstaetteListItemDto> page = new PageImpl<>(betriebsstaetteListItemDtoList);

        PageDtoEuRegBetriebsstaetteListItemDto dto = mapper.mapEuRegBetriebsstaetteListItemToDto(page);

        assertThat(dto.getContent()).isEqualTo(page.getContent());
        assertThat(dto.getTotalElements()).isEqualTo(page.getTotalElements());
    }
}