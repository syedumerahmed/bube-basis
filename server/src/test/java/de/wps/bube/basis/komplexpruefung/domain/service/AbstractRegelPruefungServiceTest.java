package de.wps.bube.basis.komplexpruefung.domain.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.euregistry.domain.service.BerichtPruefungService;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.euregistry.domain.service.XeurTeilberichtCrudService;
import de.wps.bube.basis.euregistry.rules.BerichtsdatenBeanMapperImpl;
import de.wps.bube.basis.euregistry.rules.EURegAnlageBeanMapperImpl;
import de.wps.bube.basis.euregistry.rules.EURegBeanMappers;
import de.wps.bube.basis.euregistry.rules.EURegBetriebsstaetteBeanMapperImpl;
import de.wps.bube.basis.euregistry.rules.EURegFBeanMapperImpl;
import de.wps.bube.basis.euregistry.rules.TeilberichtBeanMapperImpl;
import de.wps.bube.basis.komplexpruefung.persistence.KomplexpruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelPruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenPruefungService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.rules.AdresseBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.BehoerdeBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.KommunikationsverbindungBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapperImpl;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettePruefungService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.AnlageBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.AnlagenteilBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.AnsprechpartnerBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.BetreiberBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.BetriebsstaetteBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.GeoPunktBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.LeistungBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.QuelleBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.VorschriftBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.ZustaendigeBehoerdeBeanMapperImpl;

@SpringBootTest(classes = {
        BerichtPruefungService.class, BetriebsstaettePruefungService.class, RegelPruefungService.class,
        KomplexpruefungService.class,
        ReferenzBeanMapperImpl.class, BerichtsdatenBeanMapperImpl.class, EURegAnlageBeanMapperImpl.class,
        EURegBetriebsstaetteBeanMapperImpl.class, EURegFBeanMapperImpl.class, TeilberichtBeanMapperImpl.class,
        AdresseBeanMapperImpl.class, BehoerdeBeanMapperImpl.class, KommunikationsverbindungBeanMapperImpl.class,
        ReferenzBeanMapperImpl.class, AnlageBeanMapperImpl.class, AnlagenteilBeanMapperImpl.class,
        AnsprechpartnerBeanMapperImpl.class, BetreiberBeanMapperImpl.class, BetriebsstaetteBeanMapperImpl.class,
        GeoPunktBeanMapperImpl.class, LeistungBeanMapperImpl.class, QuelleBeanMapperImpl.class,
        VorschriftBeanMapperImpl.class, ZustaendigeBehoerdeBeanMapperImpl.class,
        EURegBeanMappers.class
})
@ExtendWith(MockitoExtension.class)
abstract class AbstractRegelPruefungServiceTest {

    @MockBean
    EURegistryService euRegistryService;
    @MockBean
    XeurTeilberichtCrudService xeurTeilberichtCrudService;
    @MockBean
    StammdatenService stammdatenService;
    @MockBean
    ReferenzenService referenzenService;
    @MockBean
    KomplexpruefungRepository komplexpruefungRepository;
    @MockBean
    KoordinatenPruefungService koordinatenPruefungService;
    @MockBean
    private RegelRepository regelRepository;
    @MockBean
    private RegelPruefungRepository regelPruefungRepository;
    @MockBean
    private ParameterService parameterService;

    @Autowired
    BerichtPruefungService berichtPruefungService;
    @Autowired
    BetriebsstaettePruefungService betriebsstaettePruefungService;
    @Autowired
    KomplexpruefungService komplexpruefungService;
    @Autowired
    RegelPruefungService regelPruefungService;

}
