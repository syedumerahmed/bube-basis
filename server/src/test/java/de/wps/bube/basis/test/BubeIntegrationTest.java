package de.wps.bube.basis.test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@Import(BenutzerTestConfig.class)
@Transactional
@AutoConfigureDataJpa
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@EnableJpaAuditing
@ActiveProfiles("unittest")
@Tag("INTEGRATION")
public abstract class BubeIntegrationTest {
    public static final class Helpers {
        private Helpers() {
        }

        public static InputStream getResource(String path) throws FileNotFoundException {
            var inputStream = Helpers.class.getClassLoader().getResourceAsStream(path);
            if (inputStream == null) {
                throw new FileNotFoundException(path);
            }
            return inputStream;
        }

        public static URL getResourceURL(String path) throws FileNotFoundException {
            var url = Helpers.class.getClassLoader().getResource(path);
            if (url == null) {
                throw new FileNotFoundException(path);
            }
            return url;
        }
    }
}
