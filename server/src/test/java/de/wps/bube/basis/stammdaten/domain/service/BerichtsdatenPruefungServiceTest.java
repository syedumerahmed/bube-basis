package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;

@ExtendWith(MockitoExtension.class)
class BerichtsdatenPruefungServiceTest {

    @Mock
    private StammdatenService stammdatenService;

    private BerichtsdatenPruefungService service;

    @BeforeEach
    void setUp() {
        service = new BerichtsdatenPruefungService(stammdatenService);
    }

    @Test
    void anlage_neu_brauchtKeineWarnung() {
        var anlage = anAnlage(null).build();
        assertThat(service.brauchtAnlageWarnungVorSpeicherung(anlage)).isFalse();
    }

    @Nested
    class AnlageBetriebsstaettenBericht {

        final Long id = 1L;
        BetriebsstaetteBuilder bstBuilder;
        AnlageBuilder builder;

        @BeforeEach
        void setUp() {
            bstBuilder = aBetriebsstaette(id).hat13BImSchV(false);
            builder = anAnlage(id).parentBetriebsstaetteId(id);
        }

        @Test
        void anlage_alt_brauchtKeinenBericht_keineWarnung() {
            var bst = bstBuilder.anlagen(builder.euRegAnl(false).build()).build();
            var updated = builder.build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlageWarnungVorSpeicherung(updated)).isFalse();
        }

        @Test
        void anlage_alt_brauchtBericht_updated_brauchtBericht_keineWarnung() {
            var bst = bstBuilder.anlagen(builder.euRegAnl(true).build()).build();
            var updated = builder.euRegAnl(true).build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlageWarnungVorSpeicherung(updated)).isFalse();
        }

        @Test
        void anlage_alt_brauchtBericht_updated_brauchtKeinenBericht_brauchtWarnung() {
            var bst = bstBuilder.anlagen(builder.euRegAnl(true).build()).build();
            var updated = builder.euRegAnl(false).build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlageWarnungVorSpeicherung(updated)).isTrue();
        }
    }

    @Test
    void anlagenteil_neu_brauchtKeineWarnung() {
        var anlagenteil = anAnlagenteil(null).build();
        assertThat(service.brauchtAnlagenteilWarnungVorSpeicherung(anlagenteil)).isFalse();
    }

    @Nested
    class AnlagenteilBetriebsstaettenBericht {

        final Long id = 1L;
        BetriebsstaetteBuilder bstBuilder;
        AnlageBuilder anlBuilder;
        AnlagenteilBuilder builder;

        @BeforeEach
        void setUp() {
            bstBuilder = aBetriebsstaette(id).hat13BImSchV(false);
            anlBuilder = anAnlage(id).parentBetriebsstaetteId(id).euRegAnl(false);
            builder = anAnlagenteil(id).parentAnlageId(id);
            when(stammdatenService.loadAnlage(id)).thenReturn(anlBuilder.build());
        }

        @Test
        void anlagenteil_alt_brauchtKeinenBericht_keineWarnung() {
            var bst = bstBuilder.anlagen(anlBuilder.anlagenteile(builder.euRegPflichtigBst(false).build()).build())
                                .build();
            var updated = builder.build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlagenteilWarnungVorSpeicherung(updated)).isFalse();
        }

        @Test
        void anlagenteil_alt_brauchtBericht_updated_brauchtBericht_keineWarnung() {
            var bst = bstBuilder.anlagen(anlBuilder.anlagenteile(builder.euRegPflichtigBst(true).build()).build())
                                .build();
            var updated = builder.euRegPflichtigBst(true).build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlagenteilWarnungVorSpeicherung(updated)).isFalse();
        }

        @Test
        void anlagenteil_alt_brauchtBericht_updated_brauchtKeinenBericht_brauchtWarnung() {
            var bst = bstBuilder.anlagen(anlBuilder.anlagenteile(builder.euRegPflichtigBst(true).build()).build())
                                .build();
            var updated = builder.euRegPflichtigBst(false).build();
            when(stammdatenService.loadBetriebsstaetteForRead(id)).thenReturn(bst);

            assertThat(service.brauchtAnlagenteilWarnungVorSpeicherung(updated)).isTrue();
        }
    }
}
