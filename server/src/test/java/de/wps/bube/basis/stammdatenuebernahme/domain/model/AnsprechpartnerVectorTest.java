package de.wps.bube.basis.stammdatenuebernahme.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.AnsprechpartnerBuilder;

class AnsprechpartnerVectorTest {

    @Test
    void eq_True() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();

        var apVector = new AnsprechpartnerVector(ap1, ap2);

        assertThat(apVector.isEqual()).isTrue();

    }

    @Test
    void eq_False() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname(null)
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();

        var apVector = new AnsprechpartnerVector(ap1, ap2);

        assertThat(apVector.isEqual()).isFalse();
    }

}