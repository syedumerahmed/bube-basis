package de.wps.bube.basis.stammdatenbetreiber.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlageBuilder.anSpbAnlage;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class SpbAnlageRepositoryTest {

    @Autowired
    private SpbAnlageRepository repository;

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    private AnlageRepository anlageRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Referenz vorschrift, leistungseinheit, jahr, gemeindeKennziffer, betriebsstatus;
    private Behoerde behoerde;

    private Anlage anlage;
    private Betriebsstaette bst;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        vorschrift = referenzRepository.save(aVorschriftReferenz("RVORS").build());
        leistungseinheit = referenzRepository.save(aVorschriftReferenz("REINH").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());

        bst = getDefaultBetriebsstaetteBuilder().build();
        betriebsstaetteRepository.save(bst);

        anlage = AnlageBuilder.anAnlage().name("Name").anlageNr("NUMMER").parentBetriebsstaetteId(bst.getId()).build();
        anlageRepository.save(anlage);
    }

    @Test
    void createSpbAnlage_FromAnlage_with_Leitung_and_Vorschrift() {

        SpbAnlage spbAnlage = new SpbAnlage(anlage);
        spbAnlage.setName("Neue Anlage");
        spbAnlage.setAnlageNr("876598-NEU");

        //Vorschriften
        spbAnlage.setVorschriften(List.of(VorschriftBuilder.aVorschrift().art(vorschrift).build()));

        //Leistungen
        var leistung = new Leistung(100.0, leistungseinheit, "Bezug",
                Leistungsklasse.GENEHMIGT);
        spbAnlage.setLeistungen(List.of(leistung));

        // Act
        var spbAnlageRead = repository.saveAndFlush(spbAnlage);

        // Assert
        assertNotNull(spbAnlageRead.getId());
        assertThat(spbAnlageRead.getAnlageId()).isEqualTo(spbAnlage.getAnlageId());
        assertThat(spbAnlageRead.getName()).isEqualTo(spbAnlageRead.getName());
        assertThat(spbAnlageRead.getAnlageNr()).isEqualTo(spbAnlageRead.getAnlageNr());

        assertThat(spbAnlageRead.getVorschriften().size()).isEqualTo(1);
        assertThat(spbAnlageRead.getVorschriften().get(0).getArt().getSchluessel()).isEqualTo(
                vorschrift.getSchluessel());

        assertThat(spbAnlageRead.getLeistungen().size()).isEqualTo(1);
        assertThat(spbAnlageRead.getLeistungen().get(0)).isEqualTo(
                leistung);

    }

    @Test
    void createSpbAnlage_New() {

        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setParentBetriebsstaetteId(bst.getId());
        spbAnlage.setName("Neue Anlage");
        spbAnlage.setAnlageNr("876598-NEU");

        // Act
        var spbAnlageRead = repository.saveAndFlush(spbAnlage);

        // Assert
        assertNotNull(spbAnlageRead.getId());
    }

    @Test
    void findOneByParentBetriebsstaetteIdAndAnlageNr() {
        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setParentBetriebsstaetteId(bst.getId());
        spbAnlage.setName("Neue Anlage");
        spbAnlage.setAnlageNr("876598-NEU");

        // Act
        var spbAnlageRead = repository.saveAndFlush(spbAnlage);

        // Assert
        assertThat(repository.findOneByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(spbAnlageRead.getParentBetriebsstaetteId(),
                spbAnlage.getAnlageNr()).get()).isEqualTo(spbAnlageRead);
    }

    @Test
    void findByParentBetriebsstaetteId() {
        SpbAnlage spbAnlage = new SpbAnlage();
        spbAnlage.setParentBetriebsstaetteId(bst.getId());
        spbAnlage.setName("Neue Anlage");
        spbAnlage.setAnlageNr("876598-NEU");

        // Act
        var spbAnlageRead = repository.saveAndFlush(spbAnlage);

        // Assert
        assertThat(
                repository.findByParentBetriebsstaetteId(spbAnlageRead.getParentBetriebsstaetteId()).size()).isEqualTo(
                1);
    }

    @Test
    void existsByParentBetriebsstaetteIdAndAnlageNr() {
        BetriebsstaetteBuilder bstBuilder
                = BetriebsstaetteBuilder.aBetriebsstaette()
                                        .zustaendigeBehoerde(behoerdenRepository.save(aBehoerde("BEHD1").build()))
                                        .betriebsstatus(referenzRepository.save(aBetriebsstatusReferenz("Neu").build()))
                                        .berichtsjahr(referenzRepository.save(aJahrReferenz(2020).build()))
                                        .gemeindekennziffer(referenzRepository.save(aGemeindeReferenz("G1").build()));

        Betriebsstaette bst1 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST1").build());
        Betriebsstaette bst2 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST2").build());
        repository.save(anSpbAnlage().parentBetriebsstaetteId(bst1.getId()).anlageNr("abc").build());

        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndAnlageNrIgnoreCase(bst1.getId(), "ABC")).isTrue();
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsstatus)
                                 .akz("AKZ")
                                 .zustaendigeBehoerde(this.behoerde)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(this.gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }
}
