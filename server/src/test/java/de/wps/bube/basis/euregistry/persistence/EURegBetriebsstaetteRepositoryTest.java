package de.wps.bube.basis.euregistry.persistence;

import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.querydsl.QPageRequest;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class EURegBetriebsstaetteRepositoryTest {

    @Autowired
    private EURegBetriebsstaetteRepository repository;

    @Autowired
    private ReferenzRepository referenzRepository;

    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, berichtsart, bearbeitungsstatus, betriebsstatus;
    private Betriebsstaette bst;

    @BeforeEach
    void setUp() {
        // Betriebsstätte
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        bst = betriebsstaetteRepository.save(getBetriebsstaetteBuilder().build());

        // Berichtsdaten
        berichtsart = referenzRepository.save(aReferenz(RRTYP, "PRTR").build());
        bearbeitungsstatus = referenzRepository.save(aReferenz(RBEA, "00").build());
    }

    @Test
    void testFindById() {

        final var bst = getEuRegBetriebsstaetteBuilder().build();
        final var bstRead = repository.save(bst);

        assertTrue(repository.findById(bstRead.getId()).isPresent());

    }

    @Test
    void testNotNull() {
        final var bst = new EURegBetriebsstaette(null, null);
        assertThrows(RuntimeException.class, () -> repository.saveAndFlush(bst));
    }

    @Test
    void testCreate() {

        // Setup
        final var bst = getEuRegBetriebsstaetteBuilder().build();

        // Test
        assertNull(bst.getId());
        final var bstRead = repository.saveAndFlush(bst);
        assertNotNull(bstRead.getId());

        assertEquals(bst.getBetriebsstaette(), bstRead.getBetriebsstaette());
        assertEquals(bst.getBerichtsdaten(), bstRead.getBerichtsdaten());
        assertEquals(bst.getErsteErfassung(), bstRead.getErsteErfassung());
        assertEquals(bst.getLetzteAenderung(), bstRead.getLetzteAenderung());
    }

    @Test
    void deleteByBetriebsstaetteId() {

        final var bst = getEuRegBetriebsstaetteBuilder().build();
        final var bstRead = repository.save(bst);

        assertThat(repository.findById(bstRead.getId())).isPresent();
        long count = repository.deleteByBetriebsstaetteId(bstRead.getBetriebsstaette().getId());
        assertThat(count).isEqualTo(1L);
        assertThat(repository.findById(bstRead.getId())).isEmpty();

    }

    @Test
    void existsByBetriebsstaetteId() {
        final var bst = getEuRegBetriebsstaetteBuilder().build();
        final var bstRead = repository.save(bst);

        assertTrue(repository.existsByBetriebsstaetteId(bstRead.getBetriebsstaette().getId()));
        repository.delete(bstRead);
        assertFalse(repository.existsByBetriebsstaetteId(bstRead.getBetriebsstaette().getId()));
    }

    @Test
    void findAllIds() {
        var special = getEuRegBetriebsstaetteBuilder().build();
        special.getBerichtsdaten().setAkz("AKZ");
        var betriebsstaetten = repository.saveAll(
                List.of(special, getEuRegBetriebsstaetteBuilder().build(), getEuRegBetriebsstaetteBuilder().build()));

        var ids = repository.findAllIds(eURegBetriebsstaette.berichtsdaten.akz.ne("AKZ"));

        assertThat(ids).containsExactlyInAnyOrderElementsOf(
                betriebsstaetten.subList(1, 3).stream().map(EURegBetriebsstaette::getId).collect(toList()));
    }

    @Test
    void findAllBstIds() {
        var special = getEuRegBetriebsstaetteBuilder().build();
        special.getBerichtsdaten().setAkz("AKZ");
        var betriebsstaetten = repository.saveAll(
                List.of(special, getEuRegBetriebsstaetteBuilder().build(), getEuRegBetriebsstaetteBuilder().build()));

        var ids = repository.findAllBstIds(eURegBetriebsstaette.berichtsdaten.akz.ne("AKZ"));

        assertThat(ids).containsExactlyInAnyOrderElementsOf(
                betriebsstaetten.subList(1, 3).stream().map((b) -> b.getBetriebsstaette().getId()).collect(toList()));
    }

    @Test
    void findListItemsPagedAndOrdered() {
        var special = getEuRegBetriebsstaetteBuilder().build();
        special.getBerichtsdaten().setAkz("AKZ");
        var behoerdeA = behoerdenRepository.save(aBehoerde("BHDA").build());
        var behoerdeB = behoerdenRepository.save(aBehoerde("BHDB").build());

        var euRegBetriebsstaetteBhdA = aEURegBetriebsstaette()
                .betriebsstaette(bst)
                .berichtsdaten(aFullBerichtsdaten()
                        .berichtsart(berichtsart)
                        .zustaendigeBehoerde(behoerdeA)
                        .bearbeitungsstatus(bearbeitungsstatus)
                        .build())
                .build();

        var euRegBetriebsstaetteBhdB = aEURegBetriebsstaette()
                .betriebsstaette(bst)
                .berichtsdaten(aFullBerichtsdaten()
                        .berichtsart(berichtsart)
                        .zustaendigeBehoerde(behoerdeB)
                        .bearbeitungsstatus(bearbeitungsstatus)
                        .build())
                .build();

        repository.saveAll(List.of(special, euRegBetriebsstaetteBhdA, euRegBetriebsstaetteBhdB));

        var pagerequest = QPageRequest.of(0, 1,
                eURegBetriebsstaette.berichtsdaten.zustaendigeBehoerde.bezeichnung.desc());
        var page1 = repository.findAllAsListItems(eURegBetriebsstaette.berichtsdaten.akz.ne("AKZ"),
                pagerequest.first());
        var page2 = repository.findAllAsListItems(eURegBetriebsstaette.berichtsdaten.akz.ne("AKZ"), pagerequest.next());

        assertThat(page1.map(listItem -> listItem.zustaendigeBehoerde)).isEqualTo(
                new PageImpl<>(List.of(behoerdeB.getBezeichnung()), pagerequest.first(), 2));
        assertThat(page2.map(listItem -> listItem.zustaendigeBehoerde)).isEqualTo(
                new PageImpl<>(List.of(behoerdeA.getBezeichnung()), pagerequest.next(), 2));
    }

    @Test
    void countDistinctByLandAndIdIn() {
        var builder = getBetriebsstaetteBuilder();
        var bstBerichte = betriebsstaetteRepository
                .saveAll(List.of(
                        builder.land(Land.SAARLAND).build(),
                        builder.land(Land.SAARLAND).betriebsstaetteNr("2").build(),
                        builder.land(Land.SCHLESWIG_HOLSTEIN).build(),

                        builder.land(Land.SAARLAND).betriebsstaetteNr("3").build(),
                        builder.land(Land.NIEDERSACHSEN).build()))
                .stream()
                .map(b -> repository.save(getEuRegBetriebsstaetteBuilder().betriebsstaette(b).build()))
                .collect(toList());

        var distinctCount = repository.countDistinctByLandAndIdIn(List.of(
                bstBerichte.get(0).getId(),
                bstBerichte.get(1).getId(),
                bstBerichte.get(2).getId()
        ));

        assertThat(distinctCount).isEqualTo(2);
    }

    @Test
    void countDistinctByJahrAndIdIn() {
        var builder = getBetriebsstaetteBuilder();
        var jahre = referenzRepository.saveAll(List.of(
                aJahrReferenz(2000).build(),
                aJahrReferenz(2001).build(),
                aJahrReferenz(2002).build()));
        var bstBerichte = betriebsstaetteRepository
                .saveAll(List.of(
                        builder.berichtsjahr(jahre.get(0)).build(),
                        builder.berichtsjahr(jahre.get(0)).betriebsstaetteNr("2").build(),
                        builder.berichtsjahr(jahre.get(1)).build(),

                        builder.berichtsjahr(jahre.get(0)).betriebsstaetteNr("3").build(),
                        builder.berichtsjahr(jahre.get(2)).build()))
                .stream()
                .map(b -> repository.save(getEuRegBetriebsstaetteBuilder().betriebsstaette(b).build()))
                .collect(toList());

        var distinctCount = repository.countDistinctByJahrAndIdIn(List.of(
                bstBerichte.get(0).getId(),
                bstBerichte.get(1).getId(),
                bstBerichte.get(2).getId()
        ));

        assertThat(distinctCount).isEqualTo(2);
    }

    private EURegBetriebsstaetteBuilder getEuRegBetriebsstaetteBuilder() {
        return aEURegBetriebsstaette().betriebsstaette(bst)
                                      .berichtsdaten(aFullBerichtsdaten()
                                              .berichtsart(berichtsart)
                                              .zustaendigeBehoerde(behoerde)
                                              .bearbeitungsstatus(bearbeitungsstatus)
                                              .build());
    }

    private BetriebsstaetteBuilder getBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(Land.HAMBURG)
                                 .zustaendigeBehoerde(behoerde)
                                 .betriebsstatus(betriebsstatus)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }
}
