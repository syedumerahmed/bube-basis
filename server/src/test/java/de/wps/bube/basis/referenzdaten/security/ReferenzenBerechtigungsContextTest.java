package de.wps.bube.basis.referenzdaten.security;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.sicherheit.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.test.BubeIntegrationTest;

@ExtendWith(MockitoExtension.class)
class ReferenzenBerechtigungsContextTest extends BubeIntegrationTest {

    ReferenzenBerechtigungsContext context;

    @Nested
    class AsGesamtAdmin {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.DEUTSCHLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(GESAMTADMIN,
                    REFERENZLISTEN_LAND_WRITE,
                    REFERENZLISTEN_BUND_WRITE,
                    REFERENZLISTEN_LAND_DELETE,
                    REFERENZLISTEN_BUND_DELETE).build();
            context = new ReferenzenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveWriteAccessToReferenzOfLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThatCode(() -> context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).doesNotThrowAnyException();
        }

        @Test
        void shouldHaveWriteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThatCode(() -> context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).doesNotThrowAnyException();
        }

        @Test
        void shouldHaveDeleteAccessToReferenzOfLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThatCode(() -> context.checkBerechtigungDelete(new ReferenzBerechtigungsAcessor(r))).doesNotThrowAnyException();
        }

        @Test
        void shouldHaveDeleteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThatCode(() -> context.checkBerechtigungDelete(new ReferenzBerechtigungsAcessor(r))).doesNotThrowAnyException();
        }
    }

    @Nested
    class AsLandUser {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).addRollen(REFERENZLISTEN_LAND_WRITE).build();
            context = new ReferenzenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldHaveWriteAccessToReferenzOfOwnLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.SAARLAND).build();

            assertThatCode(() -> context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).doesNotThrowAnyException();
        }

        @Test
        void shouldNotHaveWriteAccessToReferenzOfOtherLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveWriteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToReferenzOfOtherLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }
    }

    @Nested
    class AsUserWithoutRights {
        @BeforeEach
        void setUp() {
            var benutzer = aBehoerdenbenutzer().berechtigung(Datenberechtigung.DatenberechtigungBuilder
                    .land(Land.SAARLAND)
                    .behoerden("BEHD1", "BEHD2")
                    .akz("AKZ1", "AKZ2")
                    .verwaltungsgebiete("LLBKK1GM", "LLBKK2GM", "HHA")
                    .betriebsstaetten("BETRIEB1", "BETRIEB2")
                    .build()).build();
            context = new ReferenzenBerechtigungsContext(benutzer);
        }

        @Test
        void shouldNotHaveWriteAccessToReferenzOfOwnLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.SAARLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveWriteAccessToReferenzOfOtherLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveWriteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToReferenzOfLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.SAARLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToReferenzOfOtherLand() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.HAMBURG).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToReferenzOfBund() {
            Referenz r = aReferenz(Referenzliste.RJAHR, "2020").land(Land.DEUTSCHLAND).build();

            assertThat(context.checkBerechtigungWrite(new ReferenzBerechtigungsAcessor(r))).isFalse();
        }

        @Test
        void hatDatenberechtigung_nichts_shouldFail() {
            var r = aGemeindeReferenz("gmd").land(Land.HAMBURG).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.HAMBURG, false, false);

            assertThat(result).isFalse();
        }

        @Test
        void hatDatenberechtigung_bund_shouldFail() {
            var r = aGemeindeReferenz("gmd").land(Land.HAMBURG).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.HAMBURG, true, false);

            assertThat(result).isFalse();
        }

        @Test
        void hatDatenberechtigung_land() {
            var r = aGemeindeReferenz("gmd").land(Land.HAMBURG).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.HAMBURG, false, true);

            assertThat(result).isTrue();
        }

        @Test
        void hatDatenberechtigung_gesamtadmin() {
            var r = aGemeindeReferenz("gmd").land(Land.HAMBURG).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), true, Land.DEUTSCHLAND, true, true);

            assertThat(result).isTrue();
        }

        @Test
        void hatDatenberechtigung_falschesLand() {
            var r = aGemeindeReferenz("gmd").land(Land.HAMBURG).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.SCHLESWIG_HOLSTEIN, false, true);

            assertThat(result).isFalse();
        }

        @Test
        void hatDatenberechtigung_bundesliste_bund() {
            var r = aKommunikationsTypReferenz("tel").land(Land.DEUTSCHLAND).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.SCHLESWIG_HOLSTEIN, true, false);

            assertThat(result).isTrue();
        }

        @Test
        void hatDatenberechtigung_bundesliste_land_shouldFail() {
            var r = aKommunikationsTypReferenz("tel").land(Land.DEUTSCHLAND).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), false, Land.SCHLESWIG_HOLSTEIN, false, true);

            assertThat(result).isFalse();
        }

        @Test
        void hatDatenberechtigung_bundesliste_gesamtadmin() {
            var r = aKommunikationsTypReferenz("tel").land(Land.DEUTSCHLAND).build();

            var result = context.hatDatenberechtigung(new ReferenzBerechtigungsAcessor(r), true, Land.DEUTSCHLAND, true, true);

            assertThat(result).isTrue();
        }
    }
}