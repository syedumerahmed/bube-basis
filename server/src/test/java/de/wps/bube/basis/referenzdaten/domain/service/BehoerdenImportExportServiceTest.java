package de.wps.bube.basis.referenzdaten.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockMultipartFile;

import de.wps.bube.basis.base.mapping.EntityWithError;
import de.wps.bube.basis.base.mapping.MappingError;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.BehoerdeImportException;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.event.BehoerdenImportTriggeredEvent;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.xml.BehoerdeXMLMapper;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenJob;

@ExtendWith(MockitoExtension.class)
class BehoerdenImportExportServiceTest {

    private static final String USER = "username";

    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private DateiService dateiService;
    @Mock
    private XMLService xmlService;
    @Mock
    private BehoerdeXMLMapper behoerdeXMLMapper;
    @Mock
    private BehoerdenService behoerdenService;

    private BehoerdenImportExportService service;

    @BeforeEach
    void setUp() {
        service = new BehoerdenImportExportService(behoerdenService, xmlService, behoerdeXMLMapper, dateiService,
                eventPublisher);
    }

    @Test
    void triggerImport() {
        service.triggerImport(USER);
        verify(eventPublisher).publishEvent(any(BehoerdenImportTriggeredEvent.class));
    }

    @Test
    void dateiSpeichern() {
        byte[] bytes = {};
        var file = new MockMultipartFile("dateiName", bytes);

        service.dateiSpeichern(file, USER);

        verify(dateiService).dateiSpeichern(file, USER, DateiScope.BEHOERDE);
    }

    @Test
    void existierendeDatei() {
        service.existierendeDatei(USER);
        verify(dateiService).existingDatei(USER, DateiScope.BEHOERDE);
    }

    @Test
    void dateiLoeschen() {
        service.dateiLoeschen(USER);
        verify(dateiService).loescheDatei(USER, DateiScope.BEHOERDE);
    }

    @Test
    void importiereBenutzer_create() {
        EntityWithError<Behoerde> behoerdeWithError = new EntityWithError<>(
                BehoerdeBuilder.aBehoerde("01").land(Land.NORDRHEIN_WESTFALEN).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);

        service.importiereBehoerde(behoerdeWithError, jobProtokoll);

        verify(behoerdenService).createBehoerde(behoerdeWithError.entity);
        assertThat(behoerdeWithError.errorsOrEmpty()).isEmpty();
        verify(jobProtokoll).addEintrag("Behörde ID=null, Land=05, Schlüssel='01'. Fehlerfrei");
    }

    @Test
    void importiereBenutzer_update() {
        EntityWithError<Behoerde> behoerdeWithError = new EntityWithError<>(
                BehoerdeBuilder.aBehoerde("01").land(Land.NORDRHEIN_WESTFALEN).id(1L).build());
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);

        service.importiereBehoerde(behoerdeWithError, jobProtokoll);

        verify(behoerdenService).updateBehoerde(behoerdeWithError.entity);
        assertThat(behoerdeWithError.errorsOrEmpty()).isEmpty();
        verify(jobProtokoll).addEintrag("Behörde ID=1, Land=05, Schlüssel='01'. Aktualisiert");
    }

    @Test
    void importiereBenutzer_withErrors_shouldThrowExeption() {
        List<MappingError> errors = List.of(new MappingError("id", "fehler"));
        EntityWithError<Behoerde> behoerdeWithError = new EntityWithError<>(
                BehoerdeBuilder.aBehoerde("01").land(Land.NORDRHEIN_WESTFALEN).id(1L).build(), errors);
        JobProtokoll jobProtokoll = new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING);

        assertThrows(BehoerdeImportException.class, () -> service.importiereBehoerde(behoerdeWithError, jobProtokoll));
    }

}
