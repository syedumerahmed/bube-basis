package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.AnsprechpartnerBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.AdresseVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.AnsprechpartnerVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.BooleanVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.KommunikationsverbindungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbBetriebsstaetteVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;
import de.wps.bube.basis.test.BubeIntegrationTest;

class BetriebsstaetteVergleichServiceTest extends BubeIntegrationTest {

    public static final Betriebsstaette BETRIEBSSTAETTE = BetriebsstaetteBuilder.aFullBetriebsstaette().build();
    private BetriebsstaetteVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        CommonVergleichService commonVergleichService = new CommonVergleichService();
        vergleichService = new BetriebsstaetteVergleichService(commonVergleichService);
    }

    @Test
    void vergleicheGleich() {
        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);

        SpbBetriebsstaetteVergleich vergleich = vergleichService.vergleiche(BETRIEBSSTAETTE, spbBetriebsstaette);

        assertThat(vergleich.getId()).isEqualTo(BETRIEBSSTAETTE.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbBetriebsstaette.getId());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isEqualTo(inbetriebnahme.getSpb());

        adresseShouldBeEqual(vergleich.getAdresse());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isEqualTo(geoPunkt.getSpb());

        List<StringVector> abfallerzeugerNummern = vergleich.getAbfallerzeugerNummern();
        assertThat(abfallerzeugerNummern.stream().filter(StringVector::isEqual).count()).isEqualTo(2);

        List<AnsprechpartnerVector> ansprechpartner = vergleich.getAnsprechpartner();
        assertThat(ansprechpartner.stream().filter(AnsprechpartnerVector::isEqual).count()).isEqualTo(2);

        BooleanVector direkteinleiter = vergleich.getDirekteinleiter();
        assertThat(direkteinleiter.getSpl()).isEqualTo(direkteinleiter.getSpb());

        BooleanVector indirekteinleiter = vergleich.getIndirekteinleiter();
        assertThat(indirekteinleiter.getSpl()).isEqualTo(indirekteinleiter.getSpb());

        List<StringVector> einleiterNummern = vergleich.getEinleiterNummern();
        assertThat(einleiterNummern.stream().filter(StringVector::isEqual).count()).isEqualTo(2);

        ReferenzVector flusseinzuggebiet = vergleich.getFlusseinzuggebiet();
        assertThat(flusseinzuggebiet.getSpl()).isEqualTo(flusseinzuggebiet.getSpb());

        List<KommunikationsverbindungVector> kommunikationsverbindung = vergleich.getKommunikationsverbindung();
        assertThat(
                kommunikationsverbindung.stream().filter(KommunikationsverbindungVector::isEqual).count()).isEqualTo(
                2);

        ReferenzVector naceCode = vergleich.getNaceCode();
        assertThat(naceCode.getSpl()).isEqualTo(naceCode.getSpb());

        assertThat(name.isEqual()).isTrue();
        assertThat(betriebsstatus.isEqual()).isTrue();
        assertThat(betriebsstatusSeit.isEqual()).isTrue();
        assertThat(inbetriebnahme.isEqual()).isTrue();
        assertThat(geoPunkt.isEqual()).isTrue();
        assertThat(direkteinleiter.isEqual()).isTrue();
        assertThat(indirekteinleiter.isEqual()).isTrue();
        assertThat(flusseinzuggebiet.isEqual()).isTrue();
        assertThat(naceCode.isEqual()).isTrue();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    private void adresseShouldBeEqual(AdresseVergleich adresseVergleich) {
        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpl()).isEqualTo(strasse.getSpb());

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpl()).isEqualTo(hausNr.getSpb());

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpl()).isEqualTo(ort.getSpb());

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpl()).isEqualTo(ortsteil.getSpb());

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpl()).isEqualTo(plz.getSpb());

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpl()).isEqualTo(postfach.getSpb());

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpl()).isEqualTo(postfachPlz.getSpb());

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpl()).isEqualTo(landIsoCode.getSpb());
    }

    @Test
    void vergleicheUnterschiedlich() {
        Adresse adresse = AdresseBuilder.anAdresse()
                                        .strasse("test")
                                        .ort("test")
                                        .landIsoCode(ReferenzBuilder.aBetriebsstatusReferenz("test").id(564L).build())
                                        .ortsteil("test")
                                        .hausNr("test").postfach("test").postfachPlz("test").plz("test")
                                        .build();
        Betreiber betreiber = BetreiberBuilder.aBetreiber().adresse(adresse).name("test").build();
        Kommunikationsverbindung andereKommunikationsverbindung = new Kommunikationsverbindung(
                ReferenzBuilder.aKommunikationsTypReferenz("tel").id(159L).build(), "123456");
        Ansprechpartner ansprechpartner = AnsprechpartnerBuilder.anAnsprechpartner()
                                                                .nachname("test")
                                                                .vorname("test")
                                                                .funktion(
                                                                        ReferenzBuilder.aBetriebsstatusReferenz("test")
                                                                                       .id(546L)
                                                                                       .build())
                                                                .kommunikationsverbindungen(
                                                                        new Kommunikationsverbindung(
                                                                                ReferenzBuilder.aKommunikationsTypReferenz(
                                                                                        "test").id(21453L).build(),
                                                                                "test"))
                                                                .build();

        Betriebsstaette andereBetriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette()
                                                                      .betriebsstaetteNr("test")
                                                                      .name("test")
                                                                      .betriebsstatus(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "test").id(415L).build())
                                                                      .abfallerzeugerNummern(
                                                                              List.of(new AbfallerzeugerNr("15"),
                                                                                      new AbfallerzeugerNr("654")))
                                                                      .adresse(adresse)
                                                                      .betreiber(betreiber)
                                                                      .ansprechpartner(ansprechpartner)
                                                                      .betriebsstatusSeit(LocalDate.now())
                                                                      .inbetriebnahme(LocalDate.now())
                                                                      .istIndirekteinleiter(true)
                                                                      .istDirekteinleiter(false)
                                                                      .kommunikationsverbindungen(
                                                                              andereKommunikationsverbindung)
                                                                      .flusseinzugsgebiet(
                                                                              ReferenzBuilder.aBetriebsstatusReferenz(
                                                                                      "2020").id(15L).build())
                                                                      .build();

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(andereBetriebsstaette);

        SpbBetriebsstaetteVergleich vergleich = vergleichService.vergleiche(BETRIEBSSTAETTE, spbBetriebsstaette);

        assertThat(vergleich.getId()).isEqualTo(BETRIEBSSTAETTE.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbBetriebsstaette.getId());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isNotEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isNotEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isNotEqualTo(inbetriebnahme.getSpb());

        adresseShouldNotBeEqual(vergleich.getAdresse());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isNotEqualTo(geoPunkt.getSpb());

        List<StringVector> abfallerzeugerNummern = vergleich.getAbfallerzeugerNummern();
        assertThat(abfallerzeugerNummern.stream().filter(StringVector::isEqual).count()).isNotEqualTo(2);

        List<AnsprechpartnerVector> ansprechpartnerVectors = vergleich.getAnsprechpartner();
        assertThat(ansprechpartnerVectors.stream().filter(AnsprechpartnerVector::isEqual).count()).isNotEqualTo(2);

        BooleanVector direkteinleiter = vergleich.getDirekteinleiter();
        assertThat(direkteinleiter.getSpl()).isNotEqualTo(direkteinleiter.getSpb());

        BooleanVector indirekteinleiter = vergleich.getIndirekteinleiter();
        assertThat(indirekteinleiter.getSpl()).isNotEqualTo(indirekteinleiter.getSpb());

        List<StringVector> einleiterNummern = vergleich.getEinleiterNummern();
        assertThat(einleiterNummern.stream().filter(StringVector::isEqual).count()).isNotEqualTo(2);

        ReferenzVector flusseinzuggebiet = vergleich.getFlusseinzuggebiet();
        assertThat(flusseinzuggebiet.getSpl()).isNotEqualTo(flusseinzuggebiet.getSpb());

        List<KommunikationsverbindungVector> kommunikationsverbindung = vergleich.getKommunikationsverbindung();
        assertThat(
                kommunikationsverbindung.stream().filter(KommunikationsverbindungVector::isEqual).count()).isEqualTo(
                0);

        ReferenzVector naceCode = vergleich.getNaceCode();
        assertThat(naceCode.getSpl()).isNotEqualTo(naceCode.getSpb());

        assertThat(name.isEqual()).isFalse();
        assertThat(betriebsstatus.isEqual()).isFalse();
        assertThat(betriebsstatusSeit.isEqual()).isFalse();
        assertThat(inbetriebnahme.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();
        assertThat(direkteinleiter.isEqual()).isFalse();
        assertThat(indirekteinleiter.isEqual()).isFalse();
        assertThat(flusseinzuggebiet.isEqual()).isFalse();
        assertThat(naceCode.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }

    @Test
    void vergleicheNurSPL() {

        SpbBetriebsstaetteVergleich vergleich = vergleichService.vergleiche(BETRIEBSSTAETTE, null);

        assertThat(vergleich.getId()).isEqualTo(BETRIEBSSTAETTE.getId());
        assertThat(vergleich.getSpbId()).isNull();

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus).isNull();

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit).isNull();

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme).isNull();

        assertThat(vergleich.getAdresse()).isNull();

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt).isNull();

        List<StringVector> abfallerzeugerNummern = vergleich.getAbfallerzeugerNummern();
        assertThat(abfallerzeugerNummern).isNull();

        List<AnsprechpartnerVector> ansprechpartner = vergleich.getAnsprechpartner();
        assertThat(ansprechpartner).isNull();

        BooleanVector direkteinleiter = vergleich.getDirekteinleiter();
        assertThat(direkteinleiter).isNull();

        BooleanVector indirekteinleiter = vergleich.getIndirekteinleiter();
        assertThat(indirekteinleiter).isNull();

        List<StringVector> einleiterNummern = vergleich.getEinleiterNummern();
        assertThat(einleiterNummern).isNull();

        ReferenzVector flusseinzuggebiet = vergleich.getFlusseinzuggebiet();
        assertThat(flusseinzuggebiet).isNull();

        List<KommunikationsverbindungVector> kommunikationsverbindung = vergleich.getKommunikationsverbindung();
        assertThat(kommunikationsverbindung).isNull();

        ReferenzVector naceCode = vergleich.getNaceCode();
        assertThat(naceCode).isNull();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheNurSPB() {
        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);
        SpbBetriebsstaetteVergleich vergleich = vergleichService.vergleiche(null, spbBetriebsstaette);

        assertThat(vergleich.getSpbId()).isEqualTo(spbBetriebsstaette.getId());
        assertThat(vergleich.getId()).isNull();

        StringVector name = vergleich.getName();
        assertThat(name.getSpb()).isNotEqualTo(name.getSpl());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus).isNull();

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit).isNull();

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme).isNull();

        assertThat(vergleich.getAdresse()).isNull();

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt).isNull();

        List<StringVector> abfallerzeugerNummern = vergleich.getAbfallerzeugerNummern();
        assertThat(abfallerzeugerNummern).isNull();

        List<AnsprechpartnerVector> ansprechpartner = vergleich.getAnsprechpartner();
        assertThat(ansprechpartner).isNull();

        BooleanVector direkteinleiter = vergleich.getDirekteinleiter();
        assertThat(direkteinleiter).isNull();

        BooleanVector indirekteinleiter = vergleich.getIndirekteinleiter();
        assertThat(indirekteinleiter).isNull();

        List<StringVector> einleiterNummern = vergleich.getEinleiterNummern();
        assertThat(einleiterNummern).isNull();

        ReferenzVector flusseinzuggebiet = vergleich.getFlusseinzuggebiet();
        assertThat(flusseinzuggebiet).isNull();

        List<KommunikationsverbindungVector> kommunikationsverbindung = vergleich.getKommunikationsverbindung();
        assertThat(
                kommunikationsverbindung).isNull();

        ReferenzVector naceCode = vergleich.getNaceCode();
        assertThat(naceCode).isNull();

        assertThat(name.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }

    private void adresseShouldNotBeEqual(AdresseVergleich adresseVergleich) {
        StringVector strasse = adresseVergleich.getStrasse();
        assertThat(strasse.getSpl()).isNotEqualTo(strasse.getSpb());

        StringVector hausNr = adresseVergleich.getHausNr();
        assertThat(hausNr.getSpl()).isNotEqualTo(hausNr.getSpb());

        StringVector ort = adresseVergleich.getOrt();
        assertThat(ort.getSpl()).isNotEqualTo(ort.getSpb());

        StringVector ortsteil = adresseVergleich.getOrtsteil();
        assertThat(ortsteil.getSpl()).isNotEqualTo(ortsteil.getSpb());

        StringVector plz = adresseVergleich.getPlz();
        assertThat(plz.getSpl()).isNotEqualTo(plz.getSpb());

        StringVector postfach = adresseVergleich.getPostfach();
        assertThat(postfach.getSpl()).isNotEqualTo(postfach.getSpb());

        StringVector postfachPlz = adresseVergleich.getPostfachPlz();
        assertThat(postfachPlz.getSpl()).isNotEqualTo(postfachPlz.getSpb());

        ReferenzVector landIsoCode = adresseVergleich.getLandIsoCode();
        assertThat(landIsoCode.getSpl()).isNotEqualTo(landIsoCode.getSpb());
    }
}
