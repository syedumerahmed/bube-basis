package de.wps.bube.basis.referenzdaten.domain.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockMultipartFile;

import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.datei.domain.entity.DateiScope;
import de.wps.bube.basis.datei.domain.service.DateiService;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.event.ReferenzdatenImportTriggeredEvent;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.xml.ReferenzXMLMapper;

@ExtendWith(MockitoExtension.class)
class ReferenzdatenImportExportServiceTest {

    private static final String USER = "username";

    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private DateiService dateiService;
    @Mock
    private XMLService xmlService;
    @Mock
    private ReferenzXMLMapper referenzXMLMapper;
    @Mock
    private ReferenzenService referenzenService;

    private ReferenzdatenImportExportService service;

    @BeforeEach
    void setUp() {
        service = new ReferenzdatenImportExportService(referenzenService, xmlService, referenzXMLMapper, eventPublisher,
                dateiService);
    }

    @Test
    void triggerImport() {
        service.triggerImport(USER);
        verify(eventPublisher).publishEvent(any(ReferenzdatenImportTriggeredEvent.class));
    }

    @Test
    void dateiSpeichern() {
        byte[] bytes = {};
        var file = new MockMultipartFile("dateiName", bytes);

        service.dateiSpeichern(file, USER);

        verify(dateiService).dateiSpeichern(file, USER, DateiScope.REFERENZDATEN);
    }

    @Test
    void existierendeDatei() {
        service.existierendeDatei(USER);
        verify(dateiService).existingDatei(USER, DateiScope.REFERENZDATEN);
    }

    @Test
    void dateiLoeschen() {
        service.dateiLoeschen(USER);
        verify(dateiService).loescheDatei(USER, DateiScope.REFERENZDATEN);
    }

    @Test
    void importiere_create_no_referenznummer() {
        Referenz referenz = ReferenzBuilder.aBetriebsstatusReferenz("status").land(Land.NORDRHEIN_WESTFALEN).build();
        referenz.setReferenznummer(null);
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);

        service.importiereReferenz(referenz, jobProtokoll);

        verify(referenzenService).createReferenz(referenz);
        verify(jobProtokoll).addEintrag(
                "Referenz Referenznummer=null, Land=05, Referenzliste=RBETZ, Schlüssel='status'. Fehlerfrei");
    }

    @Test
    void importiere_create_referenznummer() {
        Referenz referenz = ReferenzBuilder.aBetriebsstatusReferenz("status")
                                           .referenznummer("123")
                                           .id(1L)
                                           .land(Land.NORDRHEIN_WESTFALEN)
                                           .build();
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        when(referenzenService.findByReferenznummer("123")).thenReturn(Optional.empty());

        service.importiereReferenz(referenz, jobProtokoll);

        verify(referenzenService).createReferenz(referenz);
        verify(jobProtokoll).addEintrag(
                "Referenz Referenznummer=123, Land=05, Referenzliste=RBETZ, Schlüssel='status'. Fehlerfrei");
    }

    @Test
    void importiere_update_referenznummer_exists() {
        Referenz referenz = ReferenzBuilder.aBetriebsstatusReferenz("status")
                                           .referenznummer("123")
                                           .id(1L)
                                           .land(Land.NORDRHEIN_WESTFALEN)
                                           .build();
        JobProtokoll jobProtokoll = Mockito.mock(JobProtokoll.class);
        when(referenzenService.findByReferenznummer("123")).thenReturn(Optional.of(referenz));

        service.importiereReferenz(referenz, jobProtokoll);

        verify(referenzenService).updateReferenz(referenz);
        verify(jobProtokoll).addEintrag(
                "Referenz Referenznummer=123, Land=05, Referenzliste=RBETZ, Schlüssel='status'. Aktualisiert");
    }

}
