package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aTaetigkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RPRTR;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.*;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel;

class BetriebsstaetteTest {

    @Test
    void hasSameKey() {
        var b1 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .build();
        var b2 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .build();

        assertThat(b1.hasSameKey(b2)).isTrue();
    }

    @Test
    void hasSameKey_otherFieldChanged() {
        var b1 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .localId("local")
                                   .build();
        var b2 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .localId("id")
                                   .build();

        assertThat(b1.hasSameKey(b2)).isTrue();
    }

    @Test
    void hasSameKey_differentLand() {
        var b1 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .build();
        var b2 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.HAMBURG)
                                   .betriebsstaetteNr("nummer")
                                   .build();

        assertThat(b1.hasSameKey(b2)).isFalse();
    }

    @Test
    void hasSameKey_differentJahr() {
        var b1 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .build();
        var b2 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2016).build())
                                   .land(Land.HAMBURG)
                                   .betriebsstaetteNr("nummer")
                                   .build();

        assertThat(b1.hasSameKey(b2)).isFalse();
    }

    @Test
    void hasSameKey_differentNummer() {
        var b1 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.BAYERN)
                                   .betriebsstaetteNr("nummer")
                                   .build();
        var b2 = aBetriebsstaette().berichtsjahr(aJahrReferenz(2019).build())
                                   .land(Land.HAMBURG)
                                   .betriebsstaetteNr("nummer2")
                                   .build();

        assertThat(b1.hasSameKey(b2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull() {
        var b1 = aBetriebsstaette().localId("local").build();
        var b2 = aBetriebsstaette().localId("local").build();

        assertThat(b1.hasSameLocalIdOrNull(b2)).isTrue();
    }

    @Test
    void hasSameLocalIdOrNull_otherId() {
        var b1 = aBetriebsstaette().localId("local").build();
        var b2 = aBetriebsstaette().localId("local-id").build();

        assertThat(b1.hasSameLocalIdOrNull(b2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull_FirstNull() {
        var b1 = aBetriebsstaette().localId(null).build();
        var b2 = aBetriebsstaette().localId("local-id").build();

        assertThat(b1.hasSameLocalIdOrNull(b2)).isFalse();
    }

    @Test
    void hasSameLocalIdOrNull_SecondNull() {
        var b1 = aBetriebsstaette().localId("local-id").build();
        var b2 = aBetriebsstaette().localId(null).build();

        assertThat(b1.hasSameLocalIdOrNull(b2)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = { "13BV", "17BV", "IE-RL", "RPRTR" })
    void hatEuRegBst_BetriebsstaettenVorschriften(String schluessel) {
        var b = aBetriebsstaette().anlagen(emptyList()).vorschriften(aVorschriftReferenz(schluessel).build()).build();

        assertThat(b.hatEURegBst()).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = { "13BV", "17BV", "IE-RL", "RPRTR" })
    void hatEuRegBst_AnlageVorschriften(String schluessel) {
        var anlageOhneEuRegVorschrift = anAnlage().vorschriften(
                aVorschrift().art(aVorschriftReferenz("R4BV").build()).build()).build();
        var anlageMitEuRegVorschrift = anAnlage().vorschriften(
                aVorschrift().art(aVorschriftReferenz(schluessel).build()).build()).build();
        var b = aBetriebsstaette().vorschriften(emptyList())
                                  .anlagen(anlageOhneEuRegVorschrift, anlageMitEuRegVorschrift)
                                  .build();

        assertThat(b.hatEURegBst()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = { "13BV", "17BV", "IE-RL", "RPRTR" })
    void hatEuRegBst_AnlagenteilVorschriften(String schluessel) {
        var anlagenteilOhneEuRegVorschrift = anAnlagenteil().vorschriften(
                aVorschrift().art(aVorschriftReferenz("R4BV").build()).build()).build();
        var anlagenteilMitEuRegVorschrift = anAnlagenteil().vorschriften(
                aVorschrift().art(aVorschriftReferenz(schluessel).build()).build()).build();
        var b = aBetriebsstaette().vorschriften(emptyList())
                                  .anlagen(anAnlage().vorschriften(emptyList())
                                                     .anlagenteile(anlagenteilMitEuRegVorschrift,
                                                             anlagenteilOhneEuRegVorschrift)
                                                     .build())
                                  .build();

        assertThat(b.hatEURegBst()).isTrue();
    }

    @Test
    void hatEuRegBst_keineRelevanteVorschrift_R4BV() {
        var r4bv = aVorschriftReferenz(VorschriftSchluessel.V_R4BV.getSchluessel()).build();
        var b = aBetriebsstaette().vorschriften(r4bv)
                                  .anlagen(anAnlage().vorschriften(aVorschrift().art(r4bv).build())
                                                     .anlagenteile(anAnlagenteil().vorschriften(
                                                             aVorschrift().art(r4bv).build()).build())
                                                     .build())
                                  .build();
        assertThat(b.hatEURegBst()).isFalse();
    }

    @Test
    void hatEuRegBst_keineRelevanteVorschrift_11BV() {
        var r11bv = aVorschriftReferenz(VorschriftSchluessel.V_11BV.getSchluessel()).build();
        var b = aBetriebsstaette().vorschriften(r11bv)
                                  .anlagen(anAnlage().vorschriften(aVorschrift().art(r11bv).build())
                                                     .anlagenteile(anAnlagenteil().vorschriften(
                                                             aVorschrift().art(r11bv).build()).build())
                                                     .build())
                                  .build();
        assertThat(b.hatEURegBst()).isFalse();
    }

    @Test
    void getPrtrTaetigkeiten() {
        var haupttaetigkeit1 = aTaetigkeitsReferenz(RPRTR, "haupt1").id(11L).build();
        var nebentaetigkeit1 = aTaetigkeitsReferenz(RPRTR, "neben1").id(12L).build();
        var haupttaetigkeit2 = aTaetigkeitsReferenz(RPRTR, "haupt2").id(21L).build();
        var nebentaetigkeit2 = aTaetigkeitsReferenz(RPRTR, "neben2").id(22L).build();
        var prtrVorschrift1 = aPrtrVorschrift()
                .taetigkeiten(haupttaetigkeit1, nebentaetigkeit1)
                .haupttaetigkeit(haupttaetigkeit1).build();
        var prtrVorschrift2 = aPrtrVorschrift()
                .taetigkeiten(haupttaetigkeit2, nebentaetigkeit2)
                .haupttaetigkeit(haupttaetigkeit2).build();
        var bst = aBetriebsstaette().anlagen(
                anAnlage().vorschriften(
                                  a13BImSchVVorschrift().build(),
                                  a17BImSchVVorschrift().build(),
                                  anIerlVorschrift().build())
                          .build(),
                anAnlage().vorschriften(
                                  a13BImSchVVorschrift().build(),
                                  a17BImSchVVorschrift().build(),
                                  anIerlVorschrift().build())
                          .anlagenteile(anAnlagenteil()
                                  .vorschriften(
                                          a13BImSchVVorschrift().build(),
                                          a17BImSchVVorschrift().build(),
                                          anIerlVorschrift().build(),
                                          prtrVorschrift1)
                                  .build())
                          .build(),
                anAnlage().vorschriften(
                                  a13BImSchVVorschrift().build(),
                                  a17BImSchVVorschrift().build(),
                                  anIerlVorschrift().build(),
                                  prtrVorschrift2)
                          .build()
        ).build();

        assertThat(bst.getPrtrVorschriften()).containsExactlyInAnyOrder(prtrVorschrift1, prtrVorschrift2);
        assertThat(bst.getPrtrHaupttaetigkeit()).isSameAs(haupttaetigkeit1);
        assertThat(bst.getPrtrTaetigkeitenOhneHaupttaetigkeit())
                .containsExactlyInAnyOrder(nebentaetigkeit1, nebentaetigkeit2);
    }

    @Test
    void hasDoppelteVorschriften_negativ() {
        var v1 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();
        var v2 = aVorschriftReferenz("TestSchlüssel2").id(2L).build();

        var bst = aBetriebsstaette().anlagen(
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v2).build())
                        .build(),
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v2).build())
                        .anlagenteile(anAnlagenteil()
                                .vorschriften(
                                        aVorschrift().art(v1).build(),
                                        aVorschrift().art(v2).build())
                                .build())
                        .build()
        ).build();

        assertThat(bst.hasDoppelteVorschriften()).isFalse();
    }

    @Test
    void hasDoppelteVorschriften_inDerAnlage() {
        var v1 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();
        var v2 = aVorschriftReferenz("TestSchlüssel2").id(2L).build();

        var bst = aBetriebsstaette().anlagen(
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v1).build())
                        .build(),
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v2).build())
                        .anlagenteile(anAnlagenteil()
                                .vorschriften(
                                        aVorschrift().art(v1).build(),
                                        aVorschrift().art(v2).build())
                                .build())
                        .build()
        ).build();

        assertThat(bst.hasDoppelteVorschriften()).isTrue();
    }

    @Test
    void hasDoppelteVorschriften_imAnlagenteil() {
        var v1 = aVorschriftReferenz("TestSchlüssel1").id(1L).build();
        var v2 = aVorschriftReferenz("TestSchlüssel2").id(2L).build();

        var bst = aBetriebsstaette().anlagen(
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v2).build())
                        .build(),
                anAnlage().vorschriften(
                                aVorschrift().art(v1).build(),
                                aVorschrift().art(v2).build())
                        .anlagenteile(anAnlagenteil()
                                .vorschriften(
                                        aVorschrift().art(v1).build(),
                                        aVorschrift().art(v1).build())
                                .build())
                        .build()
        ).build();

        assertThat(bst.hasDoppelteVorschriften()).isTrue();
    }

    @Test
    void hasDoppelteVorschriften_Empty() {
        var bst = aBetriebsstaette().build();

        assertThat(bst.hasDoppelteVorschriften()).isFalse();
    }

}
