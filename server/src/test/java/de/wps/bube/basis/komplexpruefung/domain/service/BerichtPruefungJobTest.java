package de.wps.bube.basis.komplexpruefung.domain.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.domain.service.BerichtPruefungJob;
import de.wps.bube.basis.euregistry.domain.service.BerichtPruefungService;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BerichtPruefungJobTest {

    private static final String USER = "username";

    @Mock
    private BerichtPruefungService berichtPruefungService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private EURegistryService euRegistryService;
    @Mock
    private BenutzerJobRegistry benutzerJobRegistry;

    private BerichtPruefungJob berichtPruefungJob;

    @BeforeEach
    void setUp() {
        berichtPruefungJob = new BerichtPruefungJob(berichtPruefungService, euRegistryService,
                benutzerJobRegistry);
    }

    @Test
    void emptyInvalidParams() {

        berichtPruefungJob.runAsync(Lists.emptyList(), USER);

        verifyNoInteractions(berichtPruefungService);
        verifyNoInteractions(stammdatenService);
    }

    @Test
    void singleEuRegBetriebsstaette() {

        Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(1L).build();
        EURegBetriebsstaette euRegBetriebsstaette
                = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(2L).betriebsstaette(betriebsstaette).build();
        when(euRegistryService.streamAllByBetriebsstaetteIdIn(List.of(1L))).thenReturn(Stream.of(euRegBetriebsstaette));

        berichtPruefungJob.runAsync(List.of(1L), USER);

        verify(berichtPruefungService).komplexpruefung(euRegBetriebsstaette);
        verifyNoMoreInteractions(berichtPruefungService);

        verifyNoInteractions(stammdatenService);

    }

}