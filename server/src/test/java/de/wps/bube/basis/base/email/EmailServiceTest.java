package de.wps.bube.basis.base.email;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@Tag("INTEGRATION")
@ActiveProfiles("unittest")
@Disabled
class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Test
    void sendSimpleMessage() {
        emailService.sendSimpleMessage("bube@umweltschutz-niedersachsen.de",
                "sendSimpleMessage TEST",
                "This is a TEST!", "tok@wps.de", "bube-dev@wps.de");
    }
}