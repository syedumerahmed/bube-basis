package de.wps.bube.basis.stammdaten.domain.entity;

import java.time.Instant;
import java.time.LocalDate;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public final class BerichtsobjektBuilder {

    private Long id;
    private Long betriebsstaetteId;
    private Referenz berichtsart;
    private Referenz bearbeitungsstatus;
    private LocalDate bearbeitungsstatusSeit;
    private String url;
    private Instant ersteErfassung;
    private Instant letzteAenderung;

    private BerichtsobjektBuilder() {
    }

    public static BerichtsobjektBuilder aBerichtsobjekt() {
        return new BerichtsobjektBuilder();
    }

    public BerichtsobjektBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public BerichtsobjektBuilder betriebsstaetteId(Long betriebsstaetteId) {
        this.betriebsstaetteId = betriebsstaetteId;
        return this;
    }

    public BerichtsobjektBuilder berichtsart(Referenz berichtsart) {
        this.berichtsart = berichtsart;
        return this;
    }

    public BerichtsobjektBuilder bearbeitungsstatus(Referenz bearbeitungsstatus) {
        this.bearbeitungsstatus = bearbeitungsstatus;
        return this;
    }

    public BerichtsobjektBuilder bearbeitungsstatusSeit(LocalDate bearbeitungsstatusSeit) {
        this.bearbeitungsstatusSeit = bearbeitungsstatusSeit;
        return this;
    }

    public BerichtsobjektBuilder url(String url) {
        this.url = url;
        return this;
    }

    public BerichtsobjektBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public BerichtsobjektBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public Berichtsobjekt build() {
        Berichtsobjekt berichtsobjekt = new Berichtsobjekt(betriebsstaetteId, berichtsart, bearbeitungsstatus,
                bearbeitungsstatusSeit, url);
        berichtsobjekt.setId(id);
        berichtsobjekt.setErsteErfassung(ersteErfassung);
        berichtsobjekt.setLetzteAenderung(letzteAenderung);
        return berichtsobjekt;
    }
}
