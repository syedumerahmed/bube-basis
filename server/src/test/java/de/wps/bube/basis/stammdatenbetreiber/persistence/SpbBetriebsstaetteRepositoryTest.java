package de.wps.bube.basis.stammdatenbetreiber.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Ansprechpartner;
import de.wps.bube.basis.stammdaten.domain.entity.AnsprechpartnerBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.AbfallerzeugerNr;
import de.wps.bube.basis.stammdaten.domain.entity.EinleiterNr;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class SpbBetriebsstaetteRepositoryTest {

    @Autowired
    private SpbBetriebsstaetteRepository repository;

    @Autowired
    private BetriebsstaetteRepository bstRepository;

    private Betriebsstaette bst;

    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, betriebsstatus, komTyp, funktion;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
        komTyp = referenzRepository.save(aKommunikationsTypReferenz("FAX").build());
        funktion = referenzRepository.save(aFunktionsReferenz("Chef").build());

        bst = getDefaultBetriebsstaetteBuilder().build();
        bstRepository.save(bst);
    }

    @Test
    void createSpbBetriebsstaette() {

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(bst);
        spbBetriebsstaette.setName("Mein Name");

        spbBetriebsstaette.setKommunikationsverbindungen(List.of(new Kommunikationsverbindung(komTyp, "04025312651")));
        spbBetriebsstaette.setEinleiterNummern(List.of(new EinleiterNr("123")));
        spbBetriebsstaette.setAbfallerzeugerNummern(List.of(new AbfallerzeugerNr("321")));

        Ansprechpartner ansprechpartner = new Ansprechpartner();
        ansprechpartner.setVorname("Tiberius");
        ansprechpartner.setNachname("Schulze");
        spbBetriebsstaette.setAnsprechpartner(List.of(ansprechpartner));

        // Act
        var spbBetriebsstaetteRead = repository.saveAndFlush(spbBetriebsstaette);

        // Assert
        assertNotNull(spbBetriebsstaetteRead.getId());
        assertThat(spbBetriebsstaetteRead.getName()).isEqualTo(spbBetriebsstaette.getName());
        assertThat(spbBetriebsstaetteRead.getKommunikationsverbindungen().size()).isEqualTo(1);
        assertThat(spbBetriebsstaetteRead.getEinleiterNummern().size()).isEqualTo(1);
        assertThat(spbBetriebsstaetteRead.getAbfallerzeugerNummern().size()).isEqualTo(1);

        assertThat(spbBetriebsstaetteRead.getAnsprechpartner().get(0).getVorname()).isEqualTo("Tiberius");

    }

    @Test
    void findOneByBetriebsstaetteId() {
        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(bst);

        var spbBetriebsstaetteRead = repository.saveAndFlush(spbBetriebsstaette);

        assertThat(repository.findOneByBetriebsstaetteId(spbBetriebsstaette.getBetriebsstaette().getId())).isPresent();
        assertThat(
                repository.findOneByBetriebsstaetteId(spbBetriebsstaette.getBetriebsstaette().getId()).get()).isEqualTo(
                spbBetriebsstaette);

    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsstatus)
                                 .akz("AKZ")
                                 .zustaendigeBehoerde(this.behoerde)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(this.gemeindeKennziffer)
                                 .ansprechpartner(AnsprechpartnerBuilder.anAnsprechpartner().vorname("Walter")
                                                                        .nachname("Wichtig")
                                                                        .funktion(funktion)
                                                                        .build())
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }
}
