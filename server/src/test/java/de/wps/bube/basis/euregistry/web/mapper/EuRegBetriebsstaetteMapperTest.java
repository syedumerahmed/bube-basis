package de.wps.bube.basis.euregistry.web.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.domain.service.EURegistryService;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapperImpl;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { EuRegBetriebsstaetteMapperImpl.class, BerichtsdatenMapperImpl.class,
        ReferenzMapperImpl.class, BehoerdenMapperImpl.class })
class EuRegBetriebsstaetteMapperTest {

    @Autowired
    EuRegBetriebsstaetteMapper mapper;

    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BehoerdenService behoerdenService;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private EURegistryService euRegistryService;

    @Test
    void toDto() {


        // Setup
        var bericht = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        when(euRegistryService.canWrite(bericht)).thenReturn(false);
        var dto = mapper.toDto(bericht);

        // Assert
        assertThat(dto.getCanWrite()).isFalse();
        assertEquals(bericht.getId(), dto.getId());
        assertEquals(bericht.getErsteErfassung().toString(), dto.getErsteErfassung());
        assertEquals(bericht.getLetzteAenderung().toString(), dto.getLetzteAenderung());

        // Assert Berichtsdaten
        assertEquals(bericht.getBerichtsdaten().getAkz(), dto.getBerichtsdaten().getAkz());
        assertEquals(bericht.getBerichtsdaten().getZustaendigeBehoerde().getId(),
                dto.getBerichtsdaten().getZustaendigeBehoerde().getId());
        assertEquals(bericht.getBerichtsdaten().getBearbeitungsstatus().getId(),
                dto.getBerichtsdaten().getBearbeitungsstatus().getId());
        assertEquals(bericht.getBerichtsdaten().getBearbeitungsstatusSeit(), dto.getBerichtsdaten().getBearbeitungsstatusSeit());
        assertEquals(bericht.getBerichtsdaten().getBemerkung(), dto.getBerichtsdaten().getBemerkung());
        assertEquals(bericht.getBerichtsdaten().getBerichtsart().getId(), dto.getBerichtsdaten().getBerichtsart().getId());

        // Assert Betriebsstätte
        assertEquals(bericht.getBetriebsstaette().getBerichtsjahr().getSchluessel(),
                dto.getBetriebsstaetteInfo().getBerichtsjahr());
        assertEquals(bericht.getBetriebsstaette().getBetriebsstatus().getKtext(),
                dto.getBetriebsstaetteInfo().getBetriebsstatus());
        assertEquals(bericht.getBetriebsstaette().getBetriebsstatusSeit(), dto.getBetriebsstaetteInfo().getBetriebsstatusSeit());
        assertEquals(bericht.getBetriebsstaette().getLand().getNr(), dto.getBetriebsstaetteInfo().getLand());
        assertEquals(bericht.getBetriebsstaette().getAdresse().getOrt(), dto.getBetriebsstaetteInfo().getOrt());
        assertEquals(bericht.getBetriebsstaette().getName(), dto.getBetriebsstaetteInfo().getName());
        assertEquals(bericht.getBetriebsstaette().getBetriebsstaetteNr(), dto.getBetriebsstaetteInfo().getBetriebsstaetteNr());
    }

    @Test
    void fromDto() {
        // Setup
        var berichtOrig = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        var dto = mapper.toDto(berichtOrig);

        when(behoerdenService.readBehoerde(anyLong())).thenAnswer(
                i -> BehoerdeBuilder.aBehoerde("BHD").id(i.getArgument(0)).build());
        when(referenzenService.findReferenz(any(Referenzliste.class), anyString(), any(Gueltigkeitsjahr.class),
                any(Land.class))).thenAnswer(
                i -> Optional.of(ReferenzBuilder.aReferenz(i.getArgument(0), i.getArgument(1)).build()));
        when(stammdatenService.loadBetriebsstaetteByJahrLandNummer(
                berichtOrig.getBetriebsstaette().getBerichtsjahr().getSchluessel(),
                berichtOrig.getBetriebsstaette().getLand(),
                berichtOrig.getBetriebsstaette().getBetriebsstaetteNr())).thenReturn(berichtOrig.getBetriebsstaette());

        // Act
        var bericht = mapper.fromDto(dto);

        // Assert
        assertEquals(bericht.getId(), dto.getId());
        assertEquals(bericht.getErsteErfassung().toString(), dto.getErsteErfassung());
        assertEquals(bericht.getLetzteAenderung().toString(), dto.getLetzteAenderung());

        // Assert Berichtsdaten
        assertEquals(bericht.getBerichtsdaten().getAkz(), dto.getBerichtsdaten().getAkz());
        assertEquals(bericht.getBerichtsdaten().getZustaendigeBehoerde().getId(),
                dto.getBerichtsdaten().getZustaendigeBehoerde().getId());
        assertEquals(bericht.getBerichtsdaten().getBearbeitungsstatus().getId(),
                dto.getBerichtsdaten().getBearbeitungsstatus().getId());
        assertEquals(bericht.getBerichtsdaten().getBearbeitungsstatusSeit(), dto.getBerichtsdaten().getBearbeitungsstatusSeit());
        assertEquals(bericht.getBerichtsdaten().getBemerkung(), dto.getBerichtsdaten().getBemerkung());
        assertEquals(bericht.getBerichtsdaten().getBerichtsart().getId(), dto.getBerichtsdaten().getBerichtsart().getId());

        // Assert Betriebsstätte
        assertNull(bericht.getBetriebsstaette());
    }

    @Test
    void canDeleteListItem_accessDenied_shouldBeFalse() {
        var bst = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        when(euRegistryService.canWrite(bst)).thenThrow(new AccessDeniedException(""));

        var dto = mapper.toDto(bst);

        assertThat(dto.getCanWrite()).isFalse();
    }

    @Test
    void canDeleteListItem_allowed() {
        var bst = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        when(euRegistryService.canWrite(bst)).thenReturn(true);

        var dto = mapper.toDto(bst);

        assertThat(dto.getCanWrite()).isTrue();
    }

    @Test
    void canDeleteListItem_notAllowed() {
        var bst = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        when(euRegistryService.canWrite(bst)).thenReturn(false);

        var dto = mapper.toDto(bst);

        assertThat(dto.getCanWrite()).isFalse();
    }
}
