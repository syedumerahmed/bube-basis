package de.wps.bube.basis.stammdatenbetreiber.web;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.condition.AllOf.allOf;

import java.util.Objects;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.base.web.openapi.model.SpbBetriebsstaetteDto;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAdresse;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;

@ExtendWith({ SpringExtension.class })
@SpringBootTest(classes = { SpbBetriebsstaetteMapperImpl.class, ReferenzMapperImpl.class })
class SpbBetriebsstaetteMapperTest {

    @Autowired
    private SpbBetriebsstaetteMapper mapper;

    @MockBean
    private StammdatenService stammdatenService;

    @Test
    void toDto() {

        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BetriebsstaetteBuilder.aFullBetriebsstaette()
                                                                                             .build());
        spbBetriebsstaette.setName("Mein Name");
        SpbAdresse adr = new SpbAdresse("Neue Straße", "11", "Neuer Ort", "23456");
        spbBetriebsstaette.setAdresse(adr);

        SpbBetriebsstaetteDto dto = mapper.toDto(spbBetriebsstaette);

        assertThat(dto.getId()).isEqualTo(spbBetriebsstaette.getId());
        assertThat(dto.getBetriebsstaetteId()).isEqualTo(spbBetriebsstaette.getBetriebsstaette().getId());
        assertThat(dto.getBerichtsjahr().getSchluessel()).isEqualTo(
                spbBetriebsstaette.getBetriebsstaette().getBerichtsjahr().getSchluessel());
        assertThat(dto.getLand()).isEqualTo(spbBetriebsstaette.getBetriebsstaette().getLand().getNr());
        assertThat(dto.getZustaendigeBehoerde()).isEqualTo(
                spbBetriebsstaette.getBetriebsstaette().getZustaendigeBehoerde().getSchluessel() + " - " +
                        spbBetriebsstaette.getBetriebsstaette().getZustaendigeBehoerde().getBezeichnung());
        assertThat(dto.getAkz()).isEqualTo(spbBetriebsstaette.getBetriebsstaette().getAkz());
        assertThat(dto.getBetriebsstaetteNr()).isEqualTo(spbBetriebsstaette.getBetriebsstaette().getBetriebsstaetteNr());
        assertThat(dto.getName()).isEqualTo(spbBetriebsstaette.getName());
        assertThat(dto.getBetriebsstatus()).is(equalTo(spbBetriebsstaette.getBetriebsstatus()));
        assertThat(dto.getBetriebsstatusSeit()).isEqualTo(spbBetriebsstaette.getBetriebsstatusSeit());
        assertThat(dto.getInbetriebnahme()).isEqualTo(spbBetriebsstaette.getInbetriebnahme());

        assertThat(dto.getAdresse().getStrasse()).isEqualTo(spbBetriebsstaette.getAdresse().getStrasse());
        assertThat(dto.getAdresse().getHausNr()).isEqualTo(spbBetriebsstaette.getAdresse().getHausNr());

        assertThat(dto.getAnsprechpartner().size()).isEqualTo(
                spbBetriebsstaette.getBetriebsstaette().getAnsprechpartner().size());

        assertThat(dto.getDirekteinleiter()).isEqualTo(spbBetriebsstaette.isDirekteinleiter());
        assertThat(dto.getIndirekteinleiter()).isEqualTo(spbBetriebsstaette.isIndirekteinleiter());

        assertThat(dto.getFlusseinzugsgebiet()).is(equalTo(spbBetriebsstaette.getFlusseinzugsgebiet()));
        assertThat(dto.getNaceCode()).is(equalTo(spbBetriebsstaette.getNaceCode()));
        assertThat(dto.getErsteErfassung()).isEqualTo(Objects.toString(spbBetriebsstaette.getErsteErfassung(), null));
        assertThat(dto.getLetzteAenderung()).isEqualTo(Objects.toString(spbBetriebsstaette.getLetzteAenderung(), null));

    }

    private Condition<ReferenzDto> equalTo(Referenz referenz) {
        return allOf(
                new Condition<>(dto -> Objects.equals(dto.getId(), referenz.getId()), "Id"),
                new Condition<>(dto -> Objects.equals(dto.getLand(), referenz.getLand().getNr()), "Land"),
                new Condition<>(dto -> Objects.equals(dto.getSchluessel(), referenz.getSchluessel()), "Schlüssel"),
                new Condition<>(dto -> Objects.equals(dto.getKtext(), referenz.getKtext()), "Ktext"),
                new Condition<>(dto -> Objects.equals(dto.getLtext(), referenz.getLtext()), "Ltext"),
                new Condition<>(dto -> Objects.equals(dto.getGueltigVon(), referenz.getGueltigVon().getJahr()),
                        "GueltigVon"),
                new Condition<>(dto -> Objects.equals(dto.getGueltigBis(), referenz.getGueltigBis().getJahr()),
                        "GueltigBis"),
                new Condition<>(dto -> Objects.equals(dto.getLetzteAenderung(), referenz.getLetzteAenderung().toString()),
                        "LetzteAenderung"),
                new Condition<>(dto -> Objects.equals(dto.getEu(), referenz.getEu()), "Eu"),
                new Condition<>(dto -> Objects.equals(dto.getReferenzliste().toString(), referenz.getReferenzliste().toString()), "Referenzliste"),
                new Condition<>(dto -> Objects.equals(dto.getReferenznummer(), referenz.getReferenznummer()),
                        "Referenznummer"),
                new Condition<>(dto -> Objects.equals(dto.getSortier(), referenz.getSortier()), "Sortier"),
                new Condition<>(dto -> Objects.equals(dto.getStrg(), referenz.getStrg()), "Strg")
        );

    }

}
