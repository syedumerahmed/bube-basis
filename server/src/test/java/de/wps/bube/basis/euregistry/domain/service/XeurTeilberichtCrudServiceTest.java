package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@SpringBootTest(classes = XeurTeilberichtCrudService.class)
@ActiveProfiles("noop-crypto")
class XeurTeilberichtCrudServiceTest {

    @MockBean
    private XeurTeilberichtRepository xeurTeilberichtRepository;
    @MockBean
    EURegistryService euRegistryService;
    @MockBean
    private AktiverBenutzer aktiverBenutzer;

    @Autowired
    XeurTeilberichtCrudService crudService;

    @Test
    void downloadTeilbericht() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(NORDRHEIN_WESTFALEN)
                                                                .build();
        when(aktiverBenutzer.isGesamtadmin()).thenReturn(false);
        when(aktiverBenutzer.getLand()).thenReturn(NORDRHEIN_WESTFALEN);
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);

        var out = new ByteArrayOutputStream();
        crudService.downloadTeilbericht(out, xeurTeilbericht.getId());

        assertThat(out.size()).isGreaterThan(0);
    }

}
