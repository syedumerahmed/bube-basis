package de.wps.bube.basis.benutzerverwaltung.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBehoerdenbenutzer;
import static de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerBuilder.aBetreiberbenutzer;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.benutzerverwaltung.domain.BenutzerException;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.benutzerverwaltung.security.BenutzerBerechtigungsAdapter;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BenutzerverwaltungServiceTest {

    @Mock
    private KeycloakService keycloakService;
    @Mock
    private StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    private PasswordGenerator passwordGenerator;
    @Mock
    private AktiverBenutzer bearbeiter;
    @Mock
    private StammdatenService stammdatenService;

    private BenutzerverwaltungService service;

    @BeforeEach
    void setUp() {
        service = new BenutzerverwaltungService(keycloakService, stammdatenBerechtigungsContext, stammdatenService,
                bearbeiter, passwordGenerator);
    }

    @Test
    void deleteBenutzer_canNotDeleteSelf() {
        when(bearbeiter.getUsername()).thenReturn("myself");

        assertThrows(BenutzerException.class, () -> service.deleteBenutzer("myself"));
    }

    @Test
    void createBenutzer_betreiberbenutzerMustHaveValidBetriebsstaettennr() {
        when(bearbeiter.getBerechtigung()).thenReturn(
                Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND).build());
        when(stammdatenService.allBetriebsstaettenNrForCurrentUser()).thenReturn(List.of("13", "14"));

        assertThrows(BenutzerException.class, () -> service.createBenutzer(
                aBetreiberbenutzer("betreiber", HAMBURG).betriebsstaetten("12", "13").build()));
    }

    @Test
    void createBenutzer_withoutEMail() {
        var b = aBehoerdenbenutzer("moin", Land.DEUTSCHLAND).email(null).addRollen(GESAMTADMIN).build();
        when(bearbeiter.hatRolle(GESAMTADMIN)).thenReturn(true);
        when(bearbeiter.hatRolle(BEHOERDENBENUTZER)).thenReturn(true);
        when(bearbeiter.getBerechtigung()).thenReturn(
                Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND).build());

        service.createBenutzer(b);
        verify(keycloakService).createUser(b);
        verify(keycloakService, never()).resetPassword(b);

    }

    @Test
    void createBenutzer_withEMail() {
        var b = aBehoerdenbenutzer("moin", Land.DEUTSCHLAND).id("ID")
                                                            .email("moin@web.de")
                                                            .addRollen(GESAMTADMIN)
                                                            .build();
        when(bearbeiter.hatRolle(GESAMTADMIN)).thenReturn(true);
        when(bearbeiter.hatRolle(BEHOERDENBENUTZER)).thenReturn(true);
        when(bearbeiter.getBerechtigung()).thenReturn(
                Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND).build());
        when(keycloakService.createUser(b)).thenReturn(b);

        service.createBenutzer(b);
        verify(keycloakService).createUser(b);
        verify(keycloakService).resetPassword(b);
    }

    @Test
    void updateProfile() {
        var b = aBehoerdenbenutzer("moin", Land.DEUTSCHLAND).email(null).addRollen(GESAMTADMIN).build();
        when(bearbeiter.getUsername()).thenReturn("moin");
        when(bearbeiter.hatRolle(GESAMTADMIN)).thenReturn(true);
        when(bearbeiter.hatRolle(BEHOERDENBENUTZER)).thenReturn(true);
        when(bearbeiter.getBerechtigung()).thenReturn(Datenberechtigung.DatenberechtigungBuilder.land(Land.DEUTSCHLAND)
                                                                                                .themen(new HashSet<>())
                                                                                                .fachmodule(
                                                                                                        new HashSet<>())
                                                                                                .build());
        when(keycloakService.findUserByUsername("moin")).thenReturn(Optional.of(b));
        when(stammdatenBerechtigungsContext.hasAccessToRead(any(BerechtigungsInMemoryAccessor.class))).thenReturn(true);

        service.updateProfile("neue@email");

        assertThat(b.getEmail()).isEqualTo("neue@email");
        verify(keycloakService).updateUser(b);
    }

    @Nested
    class CreateTemporaryPasswordsForBenutzer {

        @Test
        void createNewPasswordForBetreiberOhnePasswort() {
            var betreiberOhnePasswort = aBetreiberbenutzer("betreiberOhnePasswort", HAMBURG)
                    .needsTempPassword(true)
                    .build();
            var temporaryPassword = "S0_s3cr3t!";
            when(passwordGenerator.generatePassword()).thenReturn(temporaryPassword);
            when(keycloakService.getUserById(betreiberOhnePasswort.getId())).thenReturn(betreiberOhnePasswort);
            when(stammdatenBerechtigungsContext.hasAccessToRead(any(BenutzerBerechtigungsAdapter.class)))
                    .thenReturn(true);

            var updatedUsers = service.createTemporaryPasswordsForBenutzer(List.of(betreiberOhnePasswort.getId()));

            assertThat(updatedUsers).singleElement()
                                    .extracting(Benutzer::getTempPassword)
                                    .isEqualTo(temporaryPassword);
            verify(keycloakService).updateTemporaryPassword(betreiberOhnePasswort);
        }

        @Test
        void createNewPasswordForExistingBetreiber_shouldFail() {
            var betreiberMitVorhandenemPasswort = aBetreiberbenutzer("betreiberMitVorhandenemPasswort", HAMBURG)
                    .email("betreiber@betrieb.de")
                    .needsTempPassword(false)
                    .build();

            when(keycloakService.getUserById(betreiberMitVorhandenemPasswort.getId()))
                    .thenReturn(betreiberMitVorhandenemPasswort);
            when(stammdatenBerechtigungsContext.hasAccessToRead(any(BenutzerBerechtigungsAdapter.class)))
                    .thenReturn(true);

            assertThrows(BenutzerException.class, () -> service.createTemporaryPasswordsForBenutzer(
                    List.of(betreiberMitVorhandenemPasswort.getId())));

            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void createNewPasswordForBehoerde_shouldFail() {
            var behoerdenBenutzer = aBehoerdenbenutzer("behoerdenBenutzer", HAMBURG).build();

            when(keycloakService.getUserById(behoerdenBenutzer.getId())).thenReturn(behoerdenBenutzer);
            when(stammdatenBerechtigungsContext.hasAccessToRead(any(BenutzerBerechtigungsAdapter.class)))
                    .thenReturn(true);

            assertThrows(BenutzerException.class,
                    () -> service.createTemporaryPasswordsForBenutzer(List.of(behoerdenBenutzer.getId())));

            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void createNewPasswordForNonExistentUser_shouldFail() {
            when(keycloakService.getUserById(anyString())).thenThrow(BubeEntityNotFoundException.class);

            assertThrows(BubeEntityNotFoundException.class,
                    () -> service.createTemporaryPasswordsForBenutzer(List.of("some-user-id")));

            verifyNoMoreInteractions(keycloakService);
        }

        @Test
        void createNewPasswordForUnberechtigtenBenutzer_shouldFail() {
            var behoerdenBenutzer = aBehoerdenbenutzer("behoerdenBenutzer", HAMBURG).build();

            when(keycloakService.getUserById(behoerdenBenutzer.getId())).thenReturn(behoerdenBenutzer);
            when(stammdatenBerechtigungsContext.hasAccessToRead(any(BenutzerBerechtigungsAdapter.class)))
                    .thenReturn(false);

            assertThrows(BubeEntityNotFoundException.class,
                    () -> service.createTemporaryPasswordsForBenutzer(List.of(behoerdenBenutzer.getId())));

            verifyNoMoreInteractions(keycloakService);
        }
    }
}
