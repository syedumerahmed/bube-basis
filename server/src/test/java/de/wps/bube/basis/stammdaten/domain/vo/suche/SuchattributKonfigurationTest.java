package de.wps.bube.basis.stammdaten.domain.vo.suche;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.R4BV;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RPRTR;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.*;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.querydsl.core.BooleanBuilder;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.persistence.BetreiberRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class SuchattributKonfigurationTest {

    @Autowired
    private BetriebsstaetteRepository repository;
    @Autowired
    private BetreiberRepository betreiberRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, inBetrieb;

    @BeforeEach
    void setUp() {
        this.behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        this.jahr = referenzRepository.save(ReferenzBuilder.aJahrReferenz(2020).build());
        this.gemeindeKennziffer = referenzRepository.save(ReferenzBuilder.aGemeindeReferenz("HHN0UHL").build());
        this.inBetrieb = referenzRepository.save(ReferenzBuilder.aBetriebsstatusReferenz("in Betrieb").build());
    }

    @Test
    void testBetriebsstaettenContainsLand() {
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder().land(Land.BERLIN).build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder().land(Land.HAMBURG).build());

        var betriebsstaetten1 = repository.findAll(
                BETRIEBSSTAETTEN_LAND.createPredicate(new Suchattribute() {{
                    betriebsstaette.land = betriebsstaette1.getLand().getNr();
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETRIEBSSTAETTEN_LAND.createPredicate(new Suchattribute() {{
                    betriebsstaette.land = betriebsstaette2.getLand().getNr();
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsBetriebstaetteNr() {
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("23").build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("34").build());

        var betriebsstaetten1 = repository.findAll(
                BETRIEBSSTAETTEN_NUMMER.createPredicate(new Suchattribute() {{
                    betriebsstaette.nummer = "23";
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETRIEBSSTAETTEN_NUMMER.createPredicate(new Suchattribute() {{
                    betriebsstaette.nummer = "34";
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETRIEBSSTAETTEN_NUMMER.createPredicate(new Suchattribute() {{
                    betriebsstaette.nummer = "*3*";
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsBetriebstaetteLocalId() {
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("1").localId("23").build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2").localId("34").build());

        var betriebsstaetten1 = repository.findAll(
                BETRIEBSSTAETTEN_LOCAL_ID.createPredicate(new Suchattribute() {{
                    betriebsstaette.localId = "23";
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETRIEBSSTAETTEN_LOCAL_ID.createPredicate(new Suchattribute() {{
                    betriebsstaette.localId = "34";
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETRIEBSSTAETTEN_LOCAL_ID.createPredicate(new Suchattribute() {{
                    betriebsstaette.localId = "*3*";
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsName() {
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").name("test").build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").name("name").build());

        var betriebsstaetten1 = repository.findAll(NAME.createPredicate(new Suchattribute() {{
            betriebsstaette.name = "test";
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(NAME.createPredicate(new Suchattribute() {{
            betriebsstaette.name = "name";
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(NAME.createPredicate(new Suchattribute() {{
            betriebsstaette.name = "*e*";
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsPlz() {
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("1")
                .adresse(getDefaultAdresseBuilder().plz("23456").build())
                .build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2").adresse(getDefaultAdresseBuilder().plz("6789").build()).build());
        var betriebsstaetten1 = repository.findAll(PLZ.createPredicate(new Suchattribute() {{
            betriebsstaette.plz = "23456";
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(PLZ.createPredicate(new Suchattribute() {{
            betriebsstaette.plz = "6789";
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(PLZ.createPredicate(new Suchattribute() {{
            betriebsstaette.plz = "*6*";
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsOrt() {
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1")
                                                  .adresse(getDefaultAdresseBuilder().ort("23456").build())
                                                  .build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2")
                                                  .adresse(getDefaultAdresseBuilder().ort("6789").build())
                                                  .build());

        var betriebsstaetten1 = repository.findAll(ORT.createPredicate(new Suchattribute() {{
            betriebsstaette.ort = "23456";
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(ORT.createPredicate(new Suchattribute() {{
            betriebsstaette.ort = "6789";
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(ORT.createPredicate(new Suchattribute() {{
            betriebsstaette.ort = "*6*";
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenInZustaendigeBehoerde() {
        var behoerde1 = behoerdenRepository.save(getDefaultBehoerdeBuilder().schluessel("test").build());
        var behoerde2 = behoerdenRepository.save(getDefaultBehoerdeBuilder().schluessel("name").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").zustaendigeBehoerde(behoerde1).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").zustaendigeBehoerde(behoerde2).build());

        var betriebsstaetten1 = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde1.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde2.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde1.getId(), behoerde2.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsAkz() {
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").akz("12").build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").akz("23").build());

        var betriebsstaetten1 = repository.findAll(AKZ.createPredicate(new Suchattribute() {{
            betriebsstaette.akz = "12";
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(AKZ.createPredicate(new Suchattribute() {{
            betriebsstaette.akz = "23";
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(AKZ.createPredicate(new Suchattribute() {{
            betriebsstaette.akz = "*2*";
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenContainsAkzNull() {
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").akz(null).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").akz("23").build());

        var all = repository.findAll(AKZ.createPredicate(new Suchattribute() {{
            betriebsstaette.akz = "*";
        }}, betriebsstaette));
        assertThat(all).containsExactlyInAnyOrder(betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenInBetriebsstatus() {
        var betriebsstatus1 = referenzRepository.save(getDefaultBetriebsstatusBuilder().schluessel("test").build());
        var betriebsstatus2 = referenzRepository.save(getDefaultBetriebsstatusBuilder().schluessel("name").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betriebsstatus(betriebsstatus1).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betriebsstatus(betriebsstatus2).build());

        var betriebsstaetten1 = repository.findAll(
                BETRIEBSSTATUS.createPredicate(new Suchattribute() {{
                    betriebsstaette.statusIn = List.of(betriebsstatus1.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETRIEBSSTATUS.createPredicate(new Suchattribute() {{
                    betriebsstaette.statusIn = List.of(betriebsstatus2.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETRIEBSSTATUS.createPredicate(new Suchattribute() {{
                    betriebsstaette.statusIn = List.of(betriebsstatus1.getId(), betriebsstatus2.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenInNaceCode() {
        var nace01 = referenzRepository.save(aNaceCodeReferenz("NACE01").build());
        var nace02 = referenzRepository.save(aNaceCodeReferenz("NACE02").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").naceCode(nace01).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").naceCode(nace02).build());

        var betriebsstaetten1 = repository.findAll(NACE.createPredicate(new Suchattribute() {{
            betriebsstaette.naceIn = List.of(nace01.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(NACE.createPredicate(new Suchattribute() {{
            betriebsstaette.naceIn = List.of(nace02.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(NACE.createPredicate(new Suchattribute() {{
            betriebsstaette.naceIn = List.of(nace01.getId(), nace02.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenInGemeinde() {
        var gemeinde01 = referenzRepository.save(aGemeindeReferenz("RGMD01").build());
        var gemeinde02 = referenzRepository.save(aGemeindeReferenz("RGMD02").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").gemeindekennziffer(gemeinde01).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").gemeindekennziffer(gemeinde02).build());

        var betriebsstaetten1 = repository.findAll(GEMEINDE.createPredicate(new Suchattribute() {{
            betriebsstaette.gemeindeIn = List.of(gemeinde01.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(GEMEINDE.createPredicate(new Suchattribute() {{
            betriebsstaette.gemeindeIn = List.of(gemeinde02.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(GEMEINDE.createPredicate(new Suchattribute() {{
            betriebsstaette.gemeindeIn = List.of(gemeinde01.getId(), gemeinde02.getId());
        }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenInBehoerde() {
        var behoerde01 = behoerdenRepository.save(aBehoerde("RBEHD01").build());
        var behoerde02 = behoerdenRepository.save(aBehoerde("RBEHD02").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").zustaendigeBehoerde(behoerde01).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").zustaendigeBehoerde(behoerde02).build());

        var betriebsstaetten1 = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde01.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde02.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
                    betriebsstaette.behoerdeIn = List.of(behoerde01.getId(), behoerde02.getId());
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaettenUebernommen() {
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiberdatenUebernommen(true).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiberdatenUebernommen(false).build());

        var betriebsstaetten1 = repository.findAll(UEBERNOMMEN.createPredicate(new Suchattribute() {{
            betriebsstaette.uebernommen = true;
        }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(UEBERNOMMEN.createPredicate(new Suchattribute() {{
            betriebsstaette.uebernommen = false;
        }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                new BooleanBuilder(UEBERNOMMEN.createPredicate(new Suchattribute(), betriebsstaette)));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetreiberContainsName() {
        var betreiber1 = betreiberRepository.save(getDefaultBetreiberBuilder().name("name1").build());
        var betreiber2 = betreiberRepository.save(getDefaultBetreiberBuilder().name("name2").build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber1).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber2).build());

        var betriebsstaetten1 = repository.findAll(
                BETREIBER_NAME.createPredicate(new Suchattribute() {{
                    betreiber.name = "name1";
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETREIBER_NAME.createPredicate(new Suchattribute() {{
                    betreiber.name = "name2";
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETREIBER_NAME.createPredicate(new Suchattribute() {{
                    betreiber.name = "*m*";
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetreiberContainsPLZ() {
        var betreiber1 = betreiberRepository.save(
                getDefaultBetreiberBuilder().adresse(getDefaultAdresseBuilder().plz("54321").build()).build());
        var betreiber2 = betreiberRepository.save(
                getDefaultBetreiberBuilder().adresse(getDefaultAdresseBuilder().plz("12345").build()).build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber1).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber2).build());

        var betriebsstaetten1 = repository.findAll(
                BETREIBER_PLZ.createPredicate(new Suchattribute() {{
                    betreiber.plz = "54321";
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETREIBER_PLZ.createPredicate(new Suchattribute() {{
                    betreiber.plz = "12345";
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETREIBER_PLZ.createPredicate(new Suchattribute() {{
                    betreiber.plz = "*2*";
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetreiberContainsOrt() {
        var betreiber1 = betreiberRepository.save(
                getDefaultBetreiberBuilder().adresse(getDefaultAdresseBuilder().ort("ort1").build()).build());
        var betreiber2 = betreiberRepository.save(
                getDefaultBetreiberBuilder().adresse(getDefaultAdresseBuilder().ort("ort2").build()).build());
        var betriebsstaette1 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(betreiber1).build());
        var betriebsstaette2 = repository.save(
                getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("2").betreiber(betreiber2).build());

        var betriebsstaetten1 = repository.findAll(
                BETREIBER_ORT.createPredicate(new Suchattribute() {{
                    betreiber.ort = "ort1";
                }}, betriebsstaette));
        assertThat(betriebsstaetten1).containsExactlyInAnyOrder(betriebsstaette1);

        var betriebsstaetten2 = repository.findAll(
                BETREIBER_ORT.createPredicate(new Suchattribute() {{
                    betreiber.ort = "ort2";
                }}, betriebsstaette));
        assertThat(betriebsstaetten2).containsExactlyInAnyOrder(betriebsstaette2);

        var betriebsstaettenAll = repository.findAll(
                BETREIBER_ORT.createPredicate(new Suchattribute() {{
                    betreiber.ort = "*r*";
                }}, betriebsstaette));
        assertThat(betriebsstaettenAll).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
    }

    @Test
    void testBetriebsstaetteWithoutBetreiberSuche() {
        var betreiber = betreiberRepository.save(getDefaultBetreiberBuilder().name("Name").build());
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").betreiber(null).build());
        var betriebsstaette = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2").betreiber(betreiber).build());
        var all = repository.findAll(BETREIBER_NAME.createPredicate(new Suchattribute() {{
            betreiber.name = "Name";
        }}, QBetriebsstaette.betriebsstaette));
        assertThat(all).containsExactlyInAnyOrder(betriebsstaette);
    }

    @Test
    void testSearchAnlageNr() {
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2")
                .addAnlage(anAnlage().anlageNr("ANL123"))
                .build());
        var betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("3")
                .addAnlage(anAnlage().addAnlagenteil(anAnlagenteil().anlagenteilNr("ANL123")))
                .build());

        assertThat(repository.findAll(ANLAGE_NUMMER.createPredicate(new Suchattribute() {{
            anlage.nummer = "ANL123";
            anlage.searchAnlagenteile = false;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2);

        assertThat(repository.findAll(ANLAGE_NUMMER.createPredicate(new Suchattribute() {{
            anlage.nummer = "ANL123";
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2, betriebsstaette3);
    }

    @Test
    void testSearchAnlageLocalId() {
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2")
                .addAnlage(anAnlage().localId("ANL123"))
                .build());
        var betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("3")
                .addAnlage(anAnlage().addAnlagenteil(anAnlagenteil().localId("ANL123")))
                .build());

        assertThat(repository.findAll(ANLAGE_LOCAL_ID.createPredicate(new Suchattribute() {{
            anlage.localId = "ANL123";
            anlage.searchAnlagenteile = false;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2);

        assertThat(repository.findAll(ANLAGE_LOCAL_ID.createPredicate(new Suchattribute() {{
            anlage.localId = "ANL123";
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2, betriebsstaette3);
    }

    @Test
    void testSearchAnlageName() {
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2").addAnlage(anAnlage().name("ANL123")).build());
        var betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("3")
                .addAnlage(anAnlage().addAnlagenteil(anAnlagenteil().name("ANL123")))
                .build());

        assertThat(repository.findAll(ANLAGE_NAME.createPredicate(new Suchattribute() {{
            anlage.name = "ANL123";
            anlage.searchAnlagenteile = false;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2);

        assertThat(repository.findAll(ANLAGE_NAME.createPredicate(new Suchattribute() {{
            anlage.name = "ANL123";
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2, betriebsstaette3);
    }

    @Test
    void testSearchAnlageBetriebsstatusIn() {
        var betriebsstatus1 = referenzRepository.save(getDefaultBetriebsstatusBuilder().schluessel("test").build());
        var betriebsstatus2 = referenzRepository.save(getDefaultBetriebsstatusBuilder().schluessel("name").build());
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2")
                .addAnlage(anAnlage().betriebsstatus(betriebsstatus1)
                                     .anlagenteile(emptyList()))
                .build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("3")
                .addAnlage(anAnlage().addAnlagenteil(anAnlagenteil().anlagenteilNr("1").betriebsstatus(betriebsstatus2))
                                     .addAnlagenteil(anAnlagenteil().anlagenteilNr("2").betriebsstatus(null)))
                .build());

        assertThat(repository.findAll(ANLAGE_BETRIEBSSTATUS.createPredicate(new Suchattribute() {{
            anlage.statusIn = List.of(betriebsstatus1.getId(), betriebsstatus2.getId());
            anlage.searchAnlagenteile = false;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette1);

        assertThat(repository.findAll(ANLAGE_BETRIEBSSTATUS.createPredicate(new Suchattribute() {{
            anlage.statusIn = List.of(betriebsstatus2.getId());
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2);
    }

    @Test
    void testSearchAnlageBetreiberdatenUebernommen() {
        repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
        var betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("2").addAnlage(anAnlage().uebernommen(true).anlagenteile(emptyList())).build());
        var betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("3")
                .addAnlage(anAnlage().uebernommen(true).addAnlagenteil(anAnlagenteil().uebernommen(false)))
                .build());
        var betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                .betriebsstaetteNr("4")
                .addAnlage(anAnlage().uebernommen(false).addAnlagenteil(anAnlagenteil().uebernommen(true)))
                .build());

        assertThat(repository.findAll(ANLAGE_UEBERNOMMEN.createPredicate(new Suchattribute() {{
            anlage.uebernommen = false;
            anlage.searchAnlagenteile = false;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette3);

        assertThat(repository.findAll(ANLAGE_UEBERNOMMEN.createPredicate(new Suchattribute() {{
            anlage.uebernommen = true;
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2, betriebsstaette3);

        assertThat(repository.findAll(ANLAGE_UEBERNOMMEN.createPredicate(new Suchattribute() {{
            anlage.uebernommen = false;
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2, betriebsstaette3);
    }

    @Nested
    class SearchAnlageZustaendigeBehoerden {

        private Behoerde behoerde01, behoerde02;
        private Referenz zustaendigkeit1, zustaendigkeit2;
        private Betriebsstaette betriebsstaette1, betriebsstaette2, betriebsstaette3;

        @BeforeEach
        void setUp() {
            behoerde01 = behoerdenRepository.save(aBehoerde("RBEHD01").build());
            behoerde02 = behoerdenRepository.save(aBehoerde("RBEHD02").build());
            zustaendigkeit1 = referenzRepository.save(aZustaendigkeitsReferenz("RZUST01").build());
            zustaendigkeit2 = referenzRepository.save(aZustaendigkeitsReferenz("RZUST02").build());
            Supplier<ZustaendigeBehoerde> zustaendigeBehoerde1 = () -> new ZustaendigeBehoerde(behoerde01,
                    zustaendigkeit1);
            Supplier<ZustaendigeBehoerde> zustaendigeBehoerde2 = () -> new ZustaendigeBehoerde(behoerde02,
                    zustaendigkeit2);
            repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
            repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("2")
                    .addAnlage(anAnlage().zustaendigeBehoerden(emptyList()).anlagenteile(emptyList()))
                    .build());
            betriebsstaette1 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("3")
                    .addAnlage(anAnlage().addZustaendigeBehoerde(zustaendigeBehoerde2.get())
                                         .addAnlagenteil(anAnlagenteil().zustaendigeBehoerden(emptyList())))
                    .build());
            betriebsstaette2 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("4")
                    .addAnlage(anAnlage().addZustaendigeBehoerde(zustaendigeBehoerde1.get())
                                         .addAnlagenteil(
                                                 anAnlagenteil().addZustaendigeBehoerde(zustaendigeBehoerde2.get())))
                    .build());
            betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("5")
                    .addAnlage(anAnlage().zustaendigeBehoerden(emptyList())
                                         .addAnlagenteil(
                                                 anAnlagenteil().addZustaendigeBehoerde(zustaendigeBehoerde1.get())
                                                                .addZustaendigeBehoerde(zustaendigeBehoerde2.get())))
                    .build());
        }

        @Test
        void shouldFindInAnlageOnly() {
            var filter = ANLAGE_BEHOERDEN.createPredicate(new Suchattribute() {{
                anlage.behoerdeIn = List.of(behoerde02.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter)).containsExactlyInAnyOrder(betriebsstaette1);
        }

        @Test
        void shouldFindInAnlageAndAnlagenteile() {
            var filter = ANLAGE_BEHOERDEN.createPredicate(new Suchattribute() {{
                anlage.behoerdeIn = List.of(behoerde02.getId());
                anlage.searchAnlagenteile = true;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter))
                    .containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2, betriebsstaette3);
        }

        @Test
        void shouldFindAllWithEitherBehoerde() {
            var filter = ANLAGE_BEHOERDEN.createPredicate(new Suchattribute() {{
                anlage.behoerdeIn = List.of(behoerde01.getId(), behoerde02.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter)).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
        }

        @Test
        void shouldFindZustaendigkeitInAnlageOnly() {
            assertThat(repository.findAll(ANLAGE_ZUSTAENDIGKEITEN.createPredicate(new Suchattribute() {{
                anlage.zustaendigkeitIn = List.of(zustaendigkeit1.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2);
        }

        @Test
        void shouldFindZustaendigkeitInAnlagenteilen() {
            assertThat(repository.findAll(ANLAGE_ZUSTAENDIGKEITEN.createPredicate(new Suchattribute() {{
                anlage.zustaendigkeitIn = List.of(zustaendigkeit1.getId());
                anlage.searchAnlagenteile = true;
            }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette2, betriebsstaette3);
        }

        @Test
        void shouldFindAllWithEitherZustaendigkeit() {
            assertThat(repository.findAll(ANLAGE_ZUSTAENDIGKEITEN.createPredicate(new Suchattribute() {{
                anlage.zustaendigkeitIn = List.of(zustaendigkeit1.getId(), zustaendigkeit2.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette))).containsExactlyInAnyOrder(betriebsstaette1, betriebsstaette2);
        }
    }

    @Nested
    class SearchAnlageVorschriften {

        private Referenz vorschriftRef01, vorschriftRef02, taetigkeit01, taetigkeit02, taetigkeit03;
        private Betriebsstaette betriebsstaette3, betriebsstaette4, betriebsstaette5;

        @BeforeEach
        void setUp() {
            vorschriftRef01 = referenzRepository.save(aVorschriftReferenz(R4BV.name()).build());
            vorschriftRef02 = referenzRepository.save(aVorschriftReferenz(RPRTR.name()).build());
            taetigkeit01 = referenzRepository.save(aTaetigkeitsReferenz(R4BV, "R4BV01").build());
            taetigkeit02 = referenzRepository.save(aTaetigkeitsReferenz(RPRTR, "RPRTR01").build());
            taetigkeit03 = referenzRepository.save(aTaetigkeitsReferenz(RPRTR, "RPRTR02").build());
            Supplier<Vorschrift> vorschrift1 = () -> new Vorschrift(vorschriftRef01,
                    List.of(taetigkeit01), taetigkeit01);
            Supplier<Vorschrift> vorschrift2 = () -> new Vorschrift(vorschriftRef02,
                    List.of(taetigkeit02, taetigkeit03), taetigkeit02);

            repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").anlagen(emptyList()).build());
            repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("2")
                    .addAnlage(anAnlage().anlagenteile(emptyList()))
                    .build());
            betriebsstaette3 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("3")
                    .addAnlage(anAnlage().vorschriften(vorschrift1.get())
                                         .addAnlagenteil(anAnlagenteil().vorschriften(emptyList())))
                    .build());
            betriebsstaette4 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("4")
                    .addAnlage(anAnlage().vorschriften(vorschrift2.get())
                                         .addAnlagenteil(anAnlagenteil().vorschriften(vorschrift1.get())))
                    .build());
            betriebsstaette5 = repository.save(getDefaultBetriebsstaetteBuilder()
                    .betriebsstaetteNr("5")
                    .addAnlage(anAnlage()
                            .vorschriften(emptyList())
                            .addAnlagenteil(anAnlagenteil().vorschriften(vorschrift1.get(), vorschrift2.get())))
                    .build());
        }

        @Test
        void shouldFindVorschriftInAnlageOnly() {
            var filter = ANLAGE_VORSCHRIFTEN.createPredicate(new Suchattribute() {{
                anlage.vorschriftenIn = List.of(vorschriftRef01.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter)).containsExactlyInAnyOrder(betriebsstaette3);
        }

        @Test
        void shouldFindVorschriftInAnlagenteilen() {
            var filter = ANLAGE_VORSCHRIFTEN.createPredicate(new Suchattribute() {{
                anlage.vorschriftenIn = List.of(vorschriftRef01.getId());
                anlage.searchAnlagenteile = true;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter))
                    .containsExactlyInAnyOrder(betriebsstaette3, betriebsstaette4, betriebsstaette5);
        }

        @Test
        void shouldOnlyFindVorschriftenAllPresent() {
            // Vorschriften haben UND-Verknüpfung
            var filter = ANLAGE_VORSCHRIFTEN.createPredicate(new Suchattribute() {{
                anlage.vorschriftenIn = List.of(vorschriftRef01.getId(), vorschriftRef02.getId());
                anlage.searchAnlagenteile = true;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter)).containsExactlyInAnyOrder(betriebsstaette5);
        }

        @Test
        void shouldFindTaetigkeitInAnlageOnly() {
            var filter = ANLAGE_4BIMSCHV_TAETIGKEITEN.createPredicate(new Suchattribute() {{
                anlage.taetigkeiten.nr4BImSchV = List.of(taetigkeit01.getId());
                anlage.searchAnlagenteile = false;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter)).containsExactlyInAnyOrder(betriebsstaette3);
        }

        @Test
        void shouldFindTaetigkeitInAnlageAndAnlagenteilen() {
            var filter = ANLAGE_4BIMSCHV_TAETIGKEITEN.createPredicate(new Suchattribute() {{
                anlage.taetigkeiten.nr4BImSchV = List.of(taetigkeit01.getId());
                anlage.searchAnlagenteile = true;
            }}, betriebsstaette);
            assertThat(repository.findAll(filter))
                    .containsExactlyInAnyOrder(betriebsstaette3, betriebsstaette4, betriebsstaette5);
        }
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(Land.HAMBURG)
                                 .zustaendigeBehoerde(behoerde)
                                 .betriebsstatus(inBetrieb)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }

    private BetreiberBuilder getDefaultBetreiberBuilder() {
        return aBetreiber().jahr(jahr).land(Land.HAMBURG);
    }

    private static AdresseBuilder getDefaultAdresseBuilder() {
        return AdresseBuilder.anAdresse().strasse("Müllersweg").hausNr("2A").ort("Hamburg").plz("22152");
    }

    private static BehoerdeBuilder getDefaultBehoerdeBuilder() {
        return BehoerdeBuilder.aBehoerde("Bez")
                              .land(Land.HESSEN)
                              .letzteAenderung(Instant.ofEpochMilli(1L))
                              .gueltigVon(Gueltigkeitsjahr.of(2017))
                              .gueltigBis(Gueltigkeitsjahr.of(2099));
    }

    private static ReferenzBuilder getDefaultBetriebsstatusBuilder() {
        return ReferenzBuilder.aBetriebsstatusReferenz("Status 1")
                              .land(Land.HESSEN)
                              .letzteAenderung(Instant.ofEpochMilli(1L))
                              .sortier("s")
                              .gueltigVon(Gueltigkeitsjahr.of(2017))
                              .gueltigBis(Gueltigkeitsjahr.of(2099));
    }

}
