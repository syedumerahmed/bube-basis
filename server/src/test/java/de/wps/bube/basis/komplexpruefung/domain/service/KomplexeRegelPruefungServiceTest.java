package de.wps.bube.basis.komplexpruefung.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.Auflage;
import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder;
import de.wps.bube.basis.euregistry.domain.vo.AuflageBuilder;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelBuilder;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import liquibase.change.Change;
import liquibase.change.ColumnConfig;
import liquibase.change.core.InsertDataChange;
import liquibase.changelog.ChangeSet;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.exception.ChangeLogParseException;
import liquibase.parser.core.xml.XMLChangeLogSAXParser;
import liquibase.resource.ClassLoaderResourceAccessor;

/**
 * Die Klasse KomplexeRegelPruefungServiceTest kapselt mehrere Unterklassen, in denen die Funktionalitäten
 * der einzelnen im Kontext von https://wps-bube.atlassian.net/browse/BUBE-642 umgesetzten Prüfregeln getestet
 * werden.
 * <p>
 * Jede einzelne Prüfregel wird in einer separaten Unterklasse (die mit @Nested annotiert ist), implementiert.
 * Da jede dieser Unterklassen von {@link AbstractRegelTest} erbt, übernimmt sie damit auch die Testmethode
 * {@link AbstractRegelTest#testRegel(TestHarness)}, die durch eine Instanz des Interface {@link TestHarness}
 * parametrisiert und als {@link MethodSource} annotiert ist.
 * <p>
 * Da die Oberklasse {@link AbstractRegelTest} selbst keine Methode bereitstellt, die Instanzen von
 * {@link TestHarness} liefert, fällt jeder konkreten Testklasse diese Aufgabe zu; die Bereitstellung
 * von {@link TestHarness}-Instanzen erfolgt dabei jeweils durch eine statische Methode mit dem gleichen
 * Namen {@code testRegel()}.
 */
@ExtendWith(MockitoExtension.class)
class KomplexeRegelPruefungServiceTest extends AbstractRegelPruefungServiceTest {

    /**
     * Dies Interface modelliert ein Test-Szenario, in dem eine bestimmte Prüfregel untersucht wird.
     */
    interface TestHarness {

        /**
         * Diese Methode dient der Bereitstellung einer Betriebsstaette, für die die Methode
         * {@code RegelPruefungService#pruefeRegeln(Regel, Land, Gueltigkeitsjahr, java.util.function.Function}
         * an der zu testenden Prüfregel ausgeführt wird.
         *
         * @param test Da bei der Bereitstellung z.T. auf Ressourcen wie z.B. den
         *             {@link StammdatenService} zurückgegriffen werden muss, wird als Parameter
         *             die Instanz von {@link KomplexeRegelPruefungServiceTest} übergeben, welche die
         *             nötigen Ressourcen via Autoinjection enthält.
         *
         * @return Die {@link Betriebsstaette} für die Prüfung der {@link Regel}.
         */
        Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test);

        /**
         * Für die Analyse des Prüfergebnisses liefert diese Methode die ID des fraglichen
         * Prüfobjekts zurück, also desjenigen Objekts, das im {@link RegelPruefungErgebnis}
         * entweder als gültig oder als ungültig gekennzeichnet ist.
         *
         * @param betriebsstaette Die im Test der Regel verwendete {@link Betriebsstaette}
         * @param test            Die umgebende {@link KomplexeRegelPruefungServiceTest}-Instanz (zwecks
         *                        Zugriff auf die autoinjizierten Ressourcen
         *
         * @return Die ID des in der Methode {@link #assertErgebnis(RegelPruefungErgebnis, Long)}
         * untersuchten Prüfobjekts
         */
        Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

        /**
         * Wird in dem Setup ein Verstoß gegen die Prüfregel erwartet? Defaultmäßig ist dies
         * nicht der Fall.
         *
         * @return <b>true</b>, falls mit dem Setup ein Regelverstoß getestet wird; sonst
         * <b>false</b>.
         */
        default boolean expectFailure() {
            return false;
        }

        default void assertErgebnis(RegelPruefungErgebnis ergebnis, Long id) {
            assertThat(ergebnis).isNotNull();
            assertThat(ergebnis.getPruefungsFehler()).isEmpty();
            if (expectFailure()) {
                assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
                assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(id);
            } else {
                assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();
                assertThat(ergebnis.getGueltigeObjekte().stream().map(PruefObjekt::getId).filter(id::equals)).hasSize(
                        1);
            }
        }

    }

    /**
     * Diese Klasse ist die gemeinsame (abstrakte) Oberklasse aller Regel-Test-Klassen.
     *
     * @param <H> Typ einer enum von {@link TestHarness}-Instanzen, die die betreffende Regel testet.
     */
    abstract class AbstractRegelTest<H extends Enum<H> & TestHarness> {

        private static Class<? extends TestHarness> testHarnessType;

        /**
         * Diese Methode ermittelt den konkreten Typ der TestHarness-enum anhand der injizierten TestInfo.
         *
         * @param testInfo
         */
        @SuppressWarnings("unchecked")
        @BeforeAll
        static void setTestHarnessType(TestInfo testInfo) {
            Class<?> testClass = testInfo.getTestClass().get();
            ParameterizedType genericSuperclass = (ParameterizedType) testClass.getGenericSuperclass();
            testHarnessType = (Class<? extends TestHarness>) genericSuperclass.getActualTypeArguments()[0];
        }

        @AfterAll
        static void removeTestHarnessType() {
            testHarnessType = null;
        }

        private final long regelNummer;

        protected AbstractRegelTest() {
            String className = this.getClass().getName();
            this.regelNummer = Long.parseLong(className.substring(className.length() - 2));
        }

        @ParameterizedTest(name = "{0}")
        @MethodSource("getTestHarnessInstances")
        void testRegel(TestHarness harness) {
            KomplexeRegelPruefungServiceTest test = KomplexeRegelPruefungServiceTest.this;
            Betriebsstaette betriebsstaette = harness.setupBetriebsstaette(test);
            RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regeln.get(regelNummer),
                    betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                    betriebsstaette.getZustaendigeBehoerde().getId(),
                    regelObjekt -> berichtPruefungService.getPruefObjekte(betriebsstaette, regelObjekt,
                            new HashMap<>()));
            Long id = harness.getPruefObjektId(betriebsstaette, test);
            harness.assertErgebnis(ergebnis, id);
        }

        /**
         * Diese Methode liefert ein Array aller TestHarness-Instanzen zurück, die in der Unterklasse
         * zur Testausführung an {@link #testRegel(TestHarness)} übergeben werden sollen.
         *
         * @return Ein Array aller TestHarness-Instanzen für die jeweilige konkrete Testklasse.
         * @throws NoSuchMethodException
         * @throws InvocationTargetException
         * @throws IllegalAccessException
         */
        static TestHarness[] getTestHarnessInstances() throws NoSuchMethodException, InvocationTargetException,
                IllegalAccessException {
            Method values = testHarnessType.getMethod("values");
            return (TestHarness[]) values.invoke(null);
        }

    }

    private static long id = 1;

    private static Map<Long, Regel> regeln;

    private static final Referenz JAHR = aJahrReferenz(2020).build();
    private static final Referenz VORJAHR = aJahrReferenz(2019).build();
    private static final Referenz STATUS_IN_BETRIEB = ReferenzBuilder.aBetriebsstatusReferenz("01")
                                                                     .id(id++)
                                                                     .ktext("in Betrieb")
                                                                     .build();
    private static final Referenz STATUS_AUSSER_BETRIEB = ReferenzBuilder.aBetriebsstatusReferenz("02")
                                                                         .id(id++)
                                                                         .ktext("außer Betrieb")
                                                                         .build();
    private static final Referenz STATUS_STILLGELEGT = ReferenzBuilder.aBetriebsstatusReferenz("03")
                                                                      .id(id++)
                                                                      .ktext("dauerhaft stillgelegt/abgebaut")
                                                                      .build();
    private static final Referenz EPSG25832 = ReferenzBuilder.anEpsgReferenz("3")
                                                             .id(id++)
                                                             .ktext("ETRS89/UTM Zone 32N (EPSG: 25832)")
                                                             .build();

    @BeforeAll
    static void setUpRegeln() throws ChangeLogParseException {
        regeln = new HashMap<>();

        Map<Integer, String> changeSets = Map.of(
                23, "BUBE-642",
                24, "BUBE-657 Regel fuer Koordinatenpruefung",
                25, "BUBE-659 Regeln fuer EURegF"
        );
        for (Map.Entry<Integer, String> entry : changeSets.entrySet()) {

            String path = "database/changelog/changelog-0.0." + entry.getKey() + ".xml";

            XMLChangeLogSAXParser parser = new XMLChangeLogSAXParser();
            DatabaseChangeLog databaseChangeLog = parser.parse(path, null, new ClassLoaderResourceAccessor());

            ChangeSet changeSet = databaseChangeLog.getChangeSet(path, "jv", entry.getValue());
            assertThat(changeSet).isNotNull();

            for (Change change : changeSet.getChanges()) {
                if (change instanceof InsertDataChange insert)
                    if ("regel".equals(insert.getTableName())) {
                        RegelBuilder builder = RegelBuilder.aRegel().objekte(new ArrayList<>());
                        for (ColumnConfig column : insert.getColumns()) {
                            String val = column.getValue();
                            switch (column.getName()) {
                            case "nummer" -> builder.nummer(Long.parseLong(val));
                            case "land" -> builder.land(Land.of(val));
                            case "beschreibung" -> builder.beschreibung(val);
                            case "code" -> builder.code(val);
                            case "fehlertext" -> builder.fehlertext(val);
                            case "gruppe" -> builder.gruppe(RegelGruppe.valueOf(val));
                            case "typ" -> builder.typ(RegelTyp.valueOf(val));
                            case "typ_bundeseinheitlich" -> builder.typBundeseinheitlich(Boolean.parseBoolean(val));
                            case "gueltig_von" -> builder.gueltigVon(Gueltigkeitsjahr.of(Integer.parseInt(val)));
                            case "gueltig_bis" -> builder.gueltigBis(Gueltigkeitsjahr.of(Integer.parseInt(val)));
                            case "letzte_aenderung" -> builder.letzteAenderung(Instant.now());
                            default -> throw new IllegalArgumentException(column.getName());
                            }
                        }
                        Regel regel = builder.build();
                        regeln.put(regel.getNummer(), regel);
                    } else if ("regel_objekte".equals(insert.getTableName())) {
                        long regelNummer = 0;
                        RegelObjekt regelObjekt = null;
                        for (ColumnConfig column : insert.getColumns()) {
                            if ("regel_nummer".equals(column.getName())) {
                                regelNummer = Long.parseLong(column.getValue());
                            } else if ("objekte".equals(column.getName())) {
                                regelObjekt = RegelObjekt.valueOf(column.getValue());
                            }
                        }
                        regeln.get(regelNummer).getObjekte().add(regelObjekt);
                    }
            }

        }

        // Die Codes für Regeln 37 und 38 wurden in BUBE-867 aktualisiert.
        regeln.get(37L).setCode("""
                #OBJ.eURegBetriebsstaette.betriebsstaette.vorgaenger == null ||
                #OBJ.eURegBetriebsstaette.vorgaenger?.teilbericht == null ||
                #OBJ.eURegBetriebsstaette.betriebsstaette.vorgaenger.anlagen.?[
                  betriebsstatus?.schluessel != "03" && betriebsstatus?.schluessel != "04" && nachfolger == null
                ].isEmpty()""");
        regeln.get(38L).setCode("""
                #OBJ.eURegBetriebsstaette.betriebsstaette.vorgaenger == null ||
                #OBJ.eURegBetriebsstaette.vorgaenger?.teilbericht == null ||
                #OBJ.eURegBetriebsstaette.betriebsstaette.vorgaenger.anlagen.?[
                  !anlagenteile.?[
                    betriebsstatus?.schluessel != "03" && betriebsstatus?.schluessel != "04" && nachfolger == null
                  ].isEmpty()
                ].isEmpty()""");
    }

    /**
     * Diese Testklasse testet die Prüfregel 15 mit dem Kriterium
     * <blockquote>
     * Leistung.LEISTUNG größer 0 und  Leistung. EINHEIT  ist nicht MW
     * obwohl erste vier Zeichen 4.BImSchV-Nr. aus {1.1E, 1.2., 1.3., 1.4., 8.2.}
     * </blockquote>
     * Getestet werden mehrere unkritische sowie eine kritische Kombination aus
     * Vorschrift und Leistung (implementiert durch den enum-Typ {@link VorschriftUndLeistung})
     * sowie den Prüfobjekttyp (entweder Anlage oder Anlagenteil; implementiert durch den
     * enum-Typ {@link Typ}); als {@link Regel15Harness}-Instanzen fungieren alle Kombinationen von
     * {@link VorschriftUndLeistung} und {@link Typ}.
     */
    @Nested
    class Regel15 extends AbstractRegelTest<Regel15.Regel15Harness> {

        private static final Vorschrift KRITISCHE_VORSCHRIFT;
        private static final Vorschrift UNKRITISCH_WEGEN_ART;
        private static final Vorschrift UNKRITISCH_WEGEN_TAETIGKEIT;
        private static final Leistung KRITISCHE_LEISTUNG;
        private static final Leistung UNKRITISCH_WEGEN_WERT;
        private static final Leistung UNKRITISCH_WEGEN_EINHEIT;

        static {

            Referenz vorschriftArt11BV = ReferenzBuilder.aVorschriftReferenz("11BV").build();
            Referenz vorschriftArt4BV = ReferenzBuilder.aVorschriftReferenz("R4BV").build();
            Referenz taetigkeit10 = ReferenzBuilder.aTaetigkeitsReferenz("1.0.").build();
            Referenz taetigkeit12 = ReferenzBuilder.aTaetigkeitsReferenz("1.2.").build();

            KRITISCHE_VORSCHRIFT = VorschriftBuilder.aVorschrift()
                                                    .art(vorschriftArt4BV)
                                                    .taetigkeiten(taetigkeit12)
                                                    .haupttaetigkeit(taetigkeit12)
                                                    .build();
            UNKRITISCH_WEGEN_ART = VorschriftBuilder.aVorschrift()
                                                    .art(vorschriftArt11BV)
                                                    .taetigkeiten(taetigkeit12)
                                                    .haupttaetigkeit(taetigkeit12)
                                                    .build();
            UNKRITISCH_WEGEN_TAETIGKEIT = VorschriftBuilder.aVorschrift()
                                                           .art(vorschriftArt4BV)
                                                           .taetigkeiten(taetigkeit10)
                                                           .haupttaetigkeit(taetigkeit10)
                                                           .build();

            KRITISCHE_LEISTUNG = new Leistung(100.0, ReferenzBuilder.aLeistungseinheitReferenz("W").build(),
                    "-", Leistungsklasse.BETRIEBEN);
            UNKRITISCH_WEGEN_WERT = new Leistung(0.0, ReferenzBuilder.aLeistungseinheitReferenz("TW").build(),
                    "-", Leistungsklasse.BETRIEBEN);
            UNKRITISCH_WEGEN_EINHEIT = new Leistung(100.0, ReferenzBuilder.aLeistungseinheitReferenz("MW").build(),
                    "-", Leistungsklasse.BETRIEBEN);
        }

        enum VorschriftUndLeistung {

            OHNE_VORSCHRIFT_UND_LEISTUNG(null, null),
            OHNE_VORSCHRIFT(null, KRITISCHE_LEISTUNG),
            MIT_UNKRITISCHER_VORSCHRIFT_ART(UNKRITISCH_WEGEN_ART, KRITISCHE_LEISTUNG),
            MIT_UNKRITISCHER_VORSCHRIFT_TAETIGKEIT(UNKRITISCH_WEGEN_TAETIGKEIT, KRITISCHE_LEISTUNG),
            OHNE_LEISTUNG(KRITISCHE_VORSCHRIFT, null),
            MIT_UNKRITISCHEM_LEISTUNGS_WERT(KRITISCHE_VORSCHRIFT, UNKRITISCH_WEGEN_WERT),
            MIT_UNKRITISCHER_LEISTUNGS_EINHEIT(KRITISCHE_VORSCHRIFT, UNKRITISCH_WEGEN_EINHEIT),
            MIT_KRITISCHER_VORSCHRIFT_UND_LEISTUNGS(KRITISCHE_VORSCHRIFT, KRITISCHE_LEISTUNG) {
                @Override
                boolean expectFailure() {
                    return true;
                }
            };

            private final Vorschrift vorschrift;
            private final Leistung leistung;

            VorschriftUndLeistung(Vorschrift vorschrift, Leistung leistung) {
                this.vorschrift = vorschrift;
                this.leistung = leistung;
            }

            boolean expectFailure() {
                return false;
            }

        }

        enum Typ {
            ANLAGE {
                @Override
                Betriebsstaette getBetriebsstaette(VorschriftUndLeistung vorschriftUndLeistung) {
                    AnlageBuilder anlage = AnlageBuilder.anAnlage(id++);
                    if (vorschriftUndLeistung.vorschrift != null) {
                        anlage.vorschriften(vorschriftUndLeistung.vorschrift);
                    }
                    if (vorschriftUndLeistung.leistung != null) {
                        anlage.leistungen(Collections.singletonList(vorschriftUndLeistung.leistung));
                    }
                    return BetriebsstaetteBuilder.aBetriebsstaette(id++).anlagen(anlage.build()).build();
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getAnlagen().get(0).getId();
                }
            },
            ANLAGENTEIL {
                @Override
                Betriebsstaette getBetriebsstaette(VorschriftUndLeistung vorschriftUndLeistung) {
                    AnlagenteilBuilder anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++);
                    if (vorschriftUndLeistung.vorschrift != null) {
                        anlagenteil.vorschriften(vorschriftUndLeistung.vorschrift);
                    }
                    if (vorschriftUndLeistung.leistung != null) {
                        anlagenteil.leistungen(Collections.singletonList(vorschriftUndLeistung.leistung));
                    }
                    Anlage anlage = AnlageBuilder.anAnlage(id++).anlagenteile(anlagenteil.build()).build();
                    return BetriebsstaetteBuilder.aBetriebsstaette(id++).anlagen(anlage).build();
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId();
                }
            };

            abstract Betriebsstaette getBetriebsstaette(VorschriftUndLeistung vorschriftUndLeistung);

            abstract Long getPruefObjektId(Betriebsstaette betriebsstaette);
        }

        enum Regel15Harness implements TestHarness {

            ANLAGE_OHNE_VORSCHRIFT_UND_LEISTUNG(VorschriftUndLeistung.OHNE_VORSCHRIFT_UND_LEISTUNG, Typ.ANLAGE),
            ANLAGE_OHNE_VORSCHRIFT(VorschriftUndLeistung.OHNE_VORSCHRIFT, Typ.ANLAGE),
            ANLAGE_MIT_UNKRITISCHER_VORSCHRIFT_ART(VorschriftUndLeistung.MIT_UNKRITISCHER_VORSCHRIFT_ART, Typ.ANLAGE),
            ANLAGE_MIT_UNKRITISCHER_VORSCHRIFT_TAETIGKEIT(VorschriftUndLeistung.MIT_UNKRITISCHER_VORSCHRIFT_TAETIGKEIT,
                    Typ.ANLAGE),
            ANLAGE_OHNE_LEISTUNG(VorschriftUndLeistung.OHNE_LEISTUNG, Typ.ANLAGE),
            ANLAGE_MIT_UNKRITISCHEM_LEISTUNGS_WERT(VorschriftUndLeistung.MIT_UNKRITISCHEM_LEISTUNGS_WERT, Typ.ANLAGE),
            ANLAGE_MIT_UNKRITISCHER_LEISTUNGS_EINHEIT(VorschriftUndLeistung.MIT_UNKRITISCHER_LEISTUNGS_EINHEIT,
                    Typ.ANLAGE),
            ANLAGE_MIT_KRITISCHER_VORSCHRIFT_UND_LEISTUNGS(
                    VorschriftUndLeistung.MIT_KRITISCHER_VORSCHRIFT_UND_LEISTUNGS, Typ.ANLAGE),
            ANLAGENTEIL_OHNE_VORSCHRIFT_UND_LEISTUNG(VorschriftUndLeistung.OHNE_VORSCHRIFT_UND_LEISTUNG,
                    Typ.ANLAGENTEIL),
            ANLAGENTEIL_OHNE_VORSCHRIFT(VorschriftUndLeistung.OHNE_VORSCHRIFT, Typ.ANLAGENTEIL),
            ANLAGENTEIL_MIT_UNKRITISCHER_VORSCHRIFT_ART(VorschriftUndLeistung.MIT_UNKRITISCHER_VORSCHRIFT_ART,
                    Typ.ANLAGENTEIL),
            ANLAGENTEIL_MIT_UNKRITISCHER_VORSCHRIFT_TAETIGKEIT(
                    VorschriftUndLeistung.MIT_UNKRITISCHER_VORSCHRIFT_TAETIGKEIT, Typ.ANLAGENTEIL),
            ANLAGENTEIL_OHNE_LEISTUNG(VorschriftUndLeistung.OHNE_LEISTUNG, Typ.ANLAGENTEIL),
            ANLAGENTEIL_MIT_UNKRITISCHEM_LEISTUNGS_WERT(VorschriftUndLeistung.MIT_UNKRITISCHEM_LEISTUNGS_WERT,
                    Typ.ANLAGENTEIL),
            ANLAGENTEIL_MIT_UNKRITISCHER_LEISTUNGS_EINHEIT(VorschriftUndLeistung.MIT_UNKRITISCHER_LEISTUNGS_EINHEIT,
                    Typ.ANLAGENTEIL),
            ANLAGENTEIL_MIT_KRITISCHER_VORSCHRIFT_UND_LEISTUNGS(
                    VorschriftUndLeistung.MIT_KRITISCHER_VORSCHRIFT_UND_LEISTUNGS, Typ.ANLAGENTEIL),
            ;

            private final VorschriftUndLeistung vorschriftUndLeistung;
            private final Typ typ;

            Regel15Harness(VorschriftUndLeistung vorschriftUndLeistung, Typ typ) {
                this.vorschriftUndLeistung = vorschriftUndLeistung;
                this.typ = typ;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                return typ.getBetriebsstaette(vorschriftUndLeistung);
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return typ.getPruefObjektId(betriebsstaette);
            }

            @Override
            public boolean expectFailure() {
                return vorschriftUndLeistung.expectFailure();
            }

            @Override
            public String toString() {
                return vorschriftUndLeistung + " / " + typ;
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 37 mit dem Kriterium
     * <blockquote>
     * Geo-Punkt des Objektes liegt außerhalb des Gemeinde-Polygons, abzuprüfen via einzubindendem WFS-Dienst.
     * </blockquote>
     */
    @Nested
    class Regel31 extends AbstractRegelTest<Regel31.Regel31Harness> {

        private static final Referenz GEMEINDE_HAMBURG;
        private static final Referenz EPSG_25832;
        private static final Referenz EPSG_25833;
        private static final GeoPunkt UNGUELTIG;
        private static final GeoPunkt GUELTIG;

        static {
            GEMEINDE_HAMBURG = ReferenzBuilder.aGemeindeReferenz("02000000").id(id++).build();
            EPSG_25832 = ReferenzBuilder.anEpsgReferenz("25832").build();
            EPSG_25833 = ReferenzBuilder.anEpsgReferenz("25833").build();
            UNGUELTIG = new GeoPunkt(5818178, 385729, EPSG_25833);
            GUELTIG = new GeoPunkt(5937359, 567208, EPSG_25832);
        }

        /**
         * Dieser Aufzählungstyp modelliert die unterschiedlichen "Ansatzpunkte" der Prüfregel 31.
         */
        enum Typ {
            BST {
                @Override
                Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    return BetriebsstaetteBuilder.aBetriebsstaette(id++).build();
                }

                @Override
                void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt) {
                    betriebsstaette.setGeoPunkt(geoPunkt);
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getId();
                }
            },
            ANLAGE {
                @Override
                Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BST.createBetriebsstaette(test);
                    lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                             .thenReturn(betriebsstaette);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(betriebsstaette.getId())
                                                 .build();
                    betriebsstaette.setAnlagen(List.of(anlage));
                    return betriebsstaette;
                }

                @Override
                void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt) {
                    betriebsstaette.getAnlagen().get(0).setGeoPunkt(geoPunkt);
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getAnlagen().get(0).getId();
                }
            },
            ANLAGENTEIL {
                @Override
                Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = ANLAGE.createBetriebsstaette(test);
                    Anlage anlage = betriebsstaette.getAnlagen().get(0);
                    lenient().when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(List.of(anlagenteil));
                    return betriebsstaette;
                }

                @Override
                void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt) {
                    betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).setGeoPunkt(geoPunkt);
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId();
                }
            },
            QUELLE1 {
                @Override
                Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BST.createBetriebsstaette(test);
                    lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                             .thenReturn(betriebsstaette);
                    Quelle quelle = QuelleBuilder.aQuelle(id++)
                                                 .parentBetriebsstaetteId(betriebsstaette.getId())
                                                 .build();
                    betriebsstaette.setQuellen(List.of(quelle));
                    return betriebsstaette;
                }

                @Override
                void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt) {
                    betriebsstaette.getQuellen().get(0).setGeoPunkt(geoPunkt);
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getQuellen().get(0).getId();
                }
            },
            QUELLE2 {
                @Override
                Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = ANLAGE.createBetriebsstaette(test);
                    Anlage anlage = betriebsstaette.getAnlagen().get(0);
                    lenient().when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    Quelle quelle = QuelleBuilder.aQuelle(id++).parentAnlageId(anlage.getId()).build();
                    anlage.setQuellen(List.of(quelle));
                    return betriebsstaette;
                }

                @Override
                void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt) {
                    betriebsstaette.getAnlagen().get(0).getQuellen().get(0).setGeoPunkt(geoPunkt);
                }

                @Override
                Long getPruefObjektId(Betriebsstaette betriebsstaette) {
                    return betriebsstaette.getAnlagen().get(0).getQuellen().get(0).getId();
                }
            },
            ;

            abstract Betriebsstaette createBetriebsstaette(KomplexeRegelPruefungServiceTest test);

            abstract void setGeoPunkt(Betriebsstaette betriebsstaette, GeoPunkt geoPunkt);

            abstract Long getPruefObjektId(Betriebsstaette betriebsstaette);

        }

        /**
         * Dieser Aufzählungstyp modelliert die unterschiedlichen Kombinationen von Gemeindekennziffer
         * der Betriebsstätte und GeoPunkt des jeweils untersuchten Objekts.
         */
        enum Variant {
            OHNE_GEO_PUNKT {
                @Override
                public void setupBetriebsstaette(Betriebsstaette betriebsstaette,
                        KomplexeRegelPruefungServiceTest test,
                        Typ typ) {
                    betriebsstaette.setGemeindekennziffer(GEMEINDE_HAMBURG);
                    typ.setGeoPunkt(betriebsstaette, null);
                }
            },
            OHNE_GEMEINDE {
                @Override
                public void setupBetriebsstaette(Betriebsstaette betriebsstaette,
                        KomplexeRegelPruefungServiceTest test,
                        Typ typ) {
                    betriebsstaette.setGemeindekennziffer(null);
                    typ.setGeoPunkt(betriebsstaette, Regel31.UNGUELTIG);
                }
            },
            UNGUELTIG {
                @Override
                public void setupBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test,
                        Typ typ) {
                    betriebsstaette.setGemeindekennziffer(GEMEINDE_HAMBURG);
                    typ.setGeoPunkt(betriebsstaette, Regel31.UNGUELTIG);
                }
            },
            GUELTIG {
                @Override
                public void setupBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test,
                        Typ typ) {
                    betriebsstaette.setGemeindekennziffer(GEMEINDE_HAMBURG);
                    typ.setGeoPunkt(betriebsstaette, Regel31.GUELTIG);
                    when(test.koordinatenPruefungService.isGeoPunktInAgsGeometrie(anyString(),
                            any(), any(), any(), any())).thenReturn(true);
                }
            };

            public abstract void setupBetriebsstaette(Betriebsstaette betriebsstaette,
                    KomplexeRegelPruefungServiceTest test,
                    Typ typ);

        }

        /**
         * Dieser Aufzählungstyp bildet das kartesiche Produkt aus {@link Typ} und {@link Variant}. Die
         * beiden "Koeffizienten" dieses Produkts werden anhand des Namens der jeweiligen Konstante ermittelt.
         */
        enum Regel31Harness implements TestHarness {

            BST_OHNE_GEO_PUNKT,
            BST_OHNE_GEMEINDE,
            BST_GUELTIG,
            BST_UNGUELTIG,
            ANLAGE_OHNE_GEO_PUNKT,
            ANLAGE_OHNE_GEMEINDE,
            ANLAGE_GUELTIG,
            ANLAGE_UNGUELTIG,
            ANLAGENTEIL_OHNE_GEO_PUNKT,
            ANLAGENTEIL_OHNE_GEMEINDE,
            ANLAGENTEIL_GUELTIG,
            ANLAGENTEIL_UNGUELTIG,
            QUELLE1_OHNE_GEO_PUNKT,
            QUELLE1_OHNE_GEMEINDE,
            QUELLE1_GUELTIG,
            QUELLE1_UNGUELTIG,
            QUELLE2_OHNE_GEO_PUNKT,
            QUELLE2_OHNE_GEMEINDE,
            QUELLE2_GUELTIG,
            QUELLE2_UNGUELTIG,
            ;

            private final Typ typ;
            private final Variant variant;

            Regel31Harness() {
                String[] parts = name().split("_", 2);
                typ = Typ.valueOf(parts[0]);
                variant = Variant.valueOf(parts[1]);
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Betriebsstaette betriebsstaette = typ.createBetriebsstaette(test);
                variant.setupBetriebsstaette(betriebsstaette, test, typ);
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return typ.getPruefObjektId(betriebsstaette);
            }

            @Override
            public boolean expectFailure() {
                return variant == Variant.UNGUELTIG;
            }

        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 37 mit dem Kriterium
     * <blockquote>
     * Anlage im &lt;Jahr-1> vorhanden und im &lt;Jahr> nicht vorhanden und Betriebsstatus von
     * &lt;Jahr-1> nicht  "dauerhaft stillgelegt/abgebaut" und nicht "unterliegt nicht der IE-RL"
     * </blockquote>
     */
    @Nested
    class Regel37 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel37.Regel37Harness> {

        /**
         * Dieser Aufzählungstyp modelliert die einzelnen Szenarien, in denen die Prüfregel 37
         * getestet wird. Dabei bauen die  Fälle jeweils aufeinander auf, d.h. die Methode
         * {@link #setupBetriebsstaette(KomplexeRegelPruefungServiceTest)} ruft jeweils zunächst dieselbe
         * Methode eines Vorgängers (standardmäßig bestimmt durch die Reihenfolge des enum-Typs;
         * dies Verhalten ist aber überschreibbar) auf und modifiziert diese in der Methode
         * {@link #extendBetriebsstaette(Betriebsstaette, KomplexeRegelPruefungServiceTest)}.
         */
        enum Regel37Harness implements TestHarness {
            BETRIEBSSTAETTE_OHNE_VORGAENGER {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId())).thenReturn(
                            betriebsstaette);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())).thenReturn(
                            Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            VORGAENGER_OHNE_ANLAGEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                       .berichtsjahr(VORJAHR)
                                                                       .land(betriebsstaette.getLand())
                                                                       .localId(betriebsstaette.getLocalId())
                                                                       .build();
                    when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                            betriebsstaette.getLand(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(vorgaenger));
                    EURegBetriebsstaette vorgaengerBericht = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                        .id(id++)
                                                                                        .betriebsstaette(vorgaenger)
                                                                                        .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(VORJAHR.getSchluessel(),
                            betriebsstaette.getLocalId())).thenReturn(Optional.of(vorgaengerBericht));
                }
            },
            VORGAENGER_ANLAGE_STILLGELEGT_OHNE_NACHFOLGER {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(vorgaenger.getId())
                                                 .betriebsstatus(STATUS_STILLGELEGT)
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    vorgaenger.setAnlagen(Collections.singletonList(anlage));
                }
            },
            VORGAENGER_ANLAGE_IN_BETRIEB_NICHT_BERICHTET_OHNE_NACHFOLGER {
                @Override
                Regel37Harness getPredecessor() {
                    return VORGAENGER_OHNE_ANLAGEN;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(vorgaenger.getId())
                                                 .betriebsstatus(STATUS_IN_BETRIEB)
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(vorgaenger.getId())).thenReturn(vorgaenger);
                    vorgaenger.setAnlagen(Collections.singletonList(anlage));
                }

            },
            VORGAENGER_ANLAGE_IN_BETRIEB_BERICHTET_OHNE_NACHFOLGER {
                @Override
                Regel37Harness getPredecessor() {
                    return VORGAENGER_OHNE_ANLAGEN;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(vorgaenger.getId())
                                                 .betriebsstatus(STATUS_IN_BETRIEB)
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(vorgaenger.getId())).thenReturn(vorgaenger);
                    vorgaenger.setAnlagen(Collections.singletonList(anlage));
                    XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                            .id(id++)
                                                                            .localIds(List.of(betriebsstaette.getLocalId()))
                                                                            .build();
                    when(test.xeurTeilberichtCrudService.findLatestFreigegeben(vorgaenger.getBerichtsjahr().getId(),
                            betriebsstaette.getLand().getNr(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(xeurTeilbericht));
                }

                @Override
                public boolean expectFailure() {
                    return true;
                }
            },
            VORGAENGER_ANLAGE_IN_BETRIEB_MIT_NACHFOLGER {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(JAHR.getSchluessel(),
                            betriebsstaette.getLand(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(betriebsstaette));
                    Anlage vorgaengerAnlage = vorgaenger.getAnlagen().get(0);
                    String localId = vorgaengerAnlage.getLocalId();
                    Anlage nachfolgerAnlage = AnlageBuilder.anAnlage(id++)
                                                           .parentBetriebsstaetteId(betriebsstaette.getId())
                                                           .localId(localId)
                                                           .build();
                    when(test.stammdatenService.findAnlageByLocalId(betriebsstaette.getId(), localId)).thenReturn(
                            Optional.of(nachfolgerAnlage));
                }
            };

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Regel37Harness predecessor = getPredecessor();
                Betriebsstaette betriebsstaette = predecessor.setupBetriebsstaette(test);
                this.extendBetriebsstaette(betriebsstaette, test);
                return betriebsstaette;
            }

            Regel37Harness getPredecessor() {
                return Regel37Harness.values()[this.ordinal() - 1];
            }

            abstract void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                Long anlageId = betriebsstaette.getAnlagen().get(0).getId();
                return test.euRegistryService.findAnlagenBerichtByAnlageId(anlageId).get().getId();
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 38 mit dem Kriterium
     * <blockquote>
     * Anlagenteil im &lt;Jahr-1> vorhanden und im &lt;Jahr> nicht vorhanden und Betriebsstatus von
     * &lt;Jahr-1> nicht  "dauerhaft stillgelegt/abgebaut" und nicht "unterliegt nicht der IE-RL".
     * </blockquote>
     * Aufgrund der Ähnlichkeit zu Regel 37 ist ihr Prüfmechanismus völlig analog aufgebaut.
     */
    @Nested
    class Regel38 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel38.Regel38Harness> {

        /**
         * Dieser Aufzählungstyp modelliert die einzelnen Szenarien, in denen die Prüfregel 38
         * getestet wird. Dabei bauen die  Fälle jeweils aufeinander auf, d.h. die Methode
         * {@link #setupBetriebsstaette(KomplexeRegelPruefungServiceTest)} ruft jeweils zunächst dieselbe
         * Methode eines Vorgängers (standardmäßig bestimmt durch die Reihenfolge des enum-Typs;
         * dies Verhalten ist aber überschreibbar) auf und modifiziert diese in der Methode
         * {@link #extendBetriebsstaette(Betriebsstaette, KomplexeRegelPruefungServiceTest)}.
         */
        enum Regel38Harness implements TestHarness {
            BETRIEBSSTAETTE_OHNE_VORGAENGER {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {

                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++).build();

                    Anlage anlage = AnlageBuilder.anAnlage(id++).anlagenteile(anlagenteil).build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    anlagenteil.setParentAnlageId(anlage.getId());
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);

                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId())).thenReturn(
                            betriebsstaette);

                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())).thenReturn(
                            Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            VORGAENGER_OHNE_ANLAGEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                       .berichtsjahr(VORJAHR)
                                                                       .land(betriebsstaette.getLand())
                                                                       .localId(betriebsstaette.getLocalId())
                                                                       .build();
                    when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                            betriebsstaette.getLand(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(vorgaenger));
                    EURegBetriebsstaette vorgaengerBericht = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                        .id(id++)
                                                                                        .betriebsstaette(vorgaenger)
                                                                                        .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(VORJAHR.getSchluessel(),
                            betriebsstaette.getLocalId())).thenReturn(Optional.of(vorgaengerBericht));
                }
            },
            VORGAENGER_OHNE_ANLAGENTEILE {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(vorgaenger.getId())
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    vorgaenger.setAnlagen(Collections.singletonList(anlage));
                }
            },
            VORGAENGER_ANLAGENTEIL_STILLGELEGT_OHNE_NACHFOLGER {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = vorgaenger.getAnlagen().get(0);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .betriebsstatus(STATUS_STILLGELEGT)
                                                                .build();
                    anlagenteil.setLocalId(String.valueOf(anlagenteil.getId()));
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                }
            },
            VORGAENGER_ANLAGENTEIL_IN_BETRIEB_NICHT_BERICHTET_OHNE_NACHFOLGER {
                @Override
                Regel38Harness getPredecessor() {
                    return VORGAENGER_OHNE_ANLAGENTEILE;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = vorgaenger.getAnlagen().get(0);
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .betriebsstatus(STATUS_IN_BETRIEB)
                                                                .build();
                    anlagenteil.setLocalId(String.valueOf(anlagenteil.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(vorgaenger.getId())).thenReturn(vorgaenger);
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                }

            },
            VORGAENGER_ANLAGENTEIL_IN_BETRIEB_BERICHTET_OHNE_NACHFOLGER {
                @Override
                Regel38Harness getPredecessor() {
                    return VORGAENGER_OHNE_ANLAGENTEILE;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    Anlage anlage = vorgaenger.getAnlagen().get(0);
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .betriebsstatus(STATUS_IN_BETRIEB)
                                                                .build();
                    anlagenteil.setLocalId(String.valueOf(anlagenteil.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(vorgaenger.getId())).thenReturn(vorgaenger);
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                    XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                            .id(id++)
                                                                            .localIds(List.of(betriebsstaette.getLocalId()))
                                                                            .build();
                    when(test.xeurTeilberichtCrudService.findLatestFreigegeben(vorgaenger.getBerichtsjahr().getId(),
                            betriebsstaette.getLand().getNr(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(xeurTeilbericht));
                }

                @Override
                public boolean expectFailure() {
                    return true;
                }
            },
            VORGAENGER_ANLAGENTEIL_IN_BETRIEB_MIT_NACHFOLGER {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette vorgaenger = test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(
                                                             VORJAHR.getSchluessel(), betriebsstaette.getLand(), betriebsstaette.getLocalId())
                                                                       .orElse(null);
                    when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(JAHR.getSchluessel(),
                            betriebsstaette.getLand(), betriebsstaette.getLocalId())).thenReturn(
                            Optional.of(betriebsstaette));

                    Anlage anlage = vorgaenger.getAnlagen().get(0);
                    Anlage nachfolgerAnlage = AnlageBuilder.anAnlage(id++)
                                                           .parentBetriebsstaetteId(betriebsstaette.getId())
                                                           .localId(anlage.getLocalId())
                                                           .build();
                    when(test.stammdatenService.findAnlageByLocalId(betriebsstaette.getId(),
                            nachfolgerAnlage.getLocalId())).thenReturn(Optional.of(nachfolgerAnlage));

                    Anlagenteil anlagenteil = anlage.getAnlagenteile().get(0);

                    Anlagenteil nachfolgerAnlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                          .parentAnlageId(nachfolgerAnlage.getId())
                                                                          .localId(anlagenteil.getLocalId())
                                                                          .build();
                    when(test.stammdatenService.findAnlagenteilByLocalId(nachfolgerAnlage.getId(),
                            betriebsstaette.getId(), anlagenteil.getLocalId())).thenReturn(
                            Optional.of(nachfolgerAnlagenteil));
                }
            };

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Regel38Harness predecessor = getPredecessor();
                Betriebsstaette betriebsstaette = predecessor.setupBetriebsstaette(test);
                this.extendBetriebsstaette(betriebsstaette, test);
                return betriebsstaette;
            }

            Regel38Harness getPredecessor() {
                return Regel38Harness.values()[this.ordinal() - 1];
            }

            abstract void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                Long anlagenteilId = betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId();
                return test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilId).get().getId();
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 39 mit dem Kriterium
     * <blockquote>
     * Status BSt ist "außer Betrieb" und Betriebsstatus mindestens einer der zugehörigen
     * Anlagen nicht "außer Betrieb" oder "dauerhaft stillgelegt/abgebaut".
     * </blockquote>
     * Geprüft werden alle Kombinationen aus "Betriebsstätte in Betrieb / außer Betrieb"
     * einerseits und "Anlage nicht vorhanden / in Betrieb / außer Betrieb" andererseits.
     */
    @Nested
    class Regel39 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel39.Regel39Harness> {

        enum Regel39Harness implements TestHarness {

            BST_IN_BETRIEB_OHNE_ANLAGE(STATUS_IN_BETRIEB, null),
            BST_AUSSER_BETRIEB_OHNE_ANLAGE(STATUS_AUSSER_BETRIEB, null),
            BST_IN_BETRIEB_ANLAGE_IN_BETRIEB(STATUS_IN_BETRIEB, STATUS_IN_BETRIEB),
            BST_AUSSER_BETRIEB_ANLAGE_IN_BETRIEB(STATUS_AUSSER_BETRIEB, STATUS_IN_BETRIEB) {
                @Override
                public boolean expectFailure() {
                    return true;
                }
            },
            BST_IN_BETRIEB_ANLAGE_AUSSER_BETRIEB(STATUS_IN_BETRIEB, STATUS_AUSSER_BETRIEB),
            BST_AUSSER_BETRIEB_ANLAGE_AUSSER_BETRIEB(STATUS_AUSSER_BETRIEB, STATUS_AUSSER_BETRIEB),
            ;

            private final Referenz statusBst;
            private final Referenz statusAnlage;

            Regel39Harness(Referenz statusBst, Referenz statusAnlage) {
                this.statusBst = statusBst;
                this.statusAnlage = statusAnlage;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                        .betriebsstatus(statusBst)
                                                                        .build();
                EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                       .id(id++)
                                                                                       .betriebsstaette(betriebsstaette)
                                                                                       .build();
                when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())).thenReturn(
                        Optional.of(euRegBetriebsstaette));
                if (statusAnlage != null) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .parentBetriebsstaetteId(betriebsstaette.getId())
                                                 .betriebsstatus(statusAnlage)
                                                 .build();
                    betriebsstaette.setAnlagen(Collections.singletonList(anlage));
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())
                                             .map(EURegBetriebsstaette::getId)
                                             .orElse(null);
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 40 mit dem Kriterium
     * <blockquote>
     * Status Anlage ist "außer Betrieb" und Betriebsstatus mindestens einer der zugehörigen
     * Anlagenteile nicht "außer Betrieb" oder "dauerhaft stillgelegt/abgebaut".
     * </blockquote>
     * Die Testlogik erfolgt analog zu {@link Regel39}.
     */
    @Nested
    class Regel40 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel40.Regel40Harness> {

        enum Regel40Harness implements TestHarness {

            ANLAGE_AUSSER_BETRIEB_ANLAGENTEIL_NICHT_VORHANDEN(STATUS_AUSSER_BETRIEB, null),
            ANLAGE_IN_BETRIEB_ANLAGENTEIL_IN_BETRIEB(STATUS_IN_BETRIEB, STATUS_IN_BETRIEB),
            ANLAGE_AUSSER_BETRIEB_ANLAGENTEIL_IN_BETRIEB(STATUS_AUSSER_BETRIEB, STATUS_IN_BETRIEB) {
                @Override
                public boolean expectFailure() {
                    return true;
                }
            },
            ANLAGE_IN_BETRIEB_ANLAGENTEIL_AUSSER_BETRIEB(STATUS_IN_BETRIEB, STATUS_AUSSER_BETRIEB),
            ANLAGE_AUSSER_BETRIEB_ANLAGENTEIL_AUSSER_BETRIEB(STATUS_AUSSER_BETRIEB, STATUS_AUSSER_BETRIEB),
            ;

            private final Referenz statusAnlage;
            private final Referenz statusAnlagenteil;

            Regel40Harness(Referenz statusAnlage, Referenz statusAnlagenteil) {
                this.statusAnlage = statusAnlage;
                this.statusAnlagenteil = statusAnlagenteil;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++).build();
                Anlage anlage = AnlageBuilder.anAnlage(id++)
                                             .parentBetriebsstaetteId(betriebsstaette.getId())
                                             .betriebsstatus(statusAnlage)
                                             .build();
                betriebsstaette.setAnlagen(Collections.singletonList(anlage));
                EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).build();
                when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                        Optional.ofNullable(euRegAnlage));

                if (statusAnlagenteil != null) {
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .betriebsstatus(statusAnlagenteil)
                                                                .build();
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return test.euRegistryService.findAnlagenBerichtByAnlageId(
                        betriebsstaette.getAnlagen().get(0).getId()).map(EURegAnlage::getId).orElse(null);
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 41 mit dem Kriterium
     * <blockquote>
     * Betriebsstatus einer BSt im &lt;Jahr-1> "dauerhaft stillgelegt/abgebaut" und im &lt;Jahr> "in Betrieb".
     * </blockquote>
     * Es werden alle Kombinationen aus "Betriebsstätte in Betrieb / stillgelegt" einerseits
     * und "Vorgänger-BST nicht vorhanden / in Betrieb / stillgelegt" getestet.
     */
    @Nested
    class Regel41 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel41.Regel41Harness> {

        enum Regel41Harness implements TestHarness {

            BST_IN_BETRIEB_VORGAENGER_NICHT_VORHANDEN(STATUS_IN_BETRIEB, null),
            BST_STILLGELEGT_VORGAENGER_NICHT_VORHANDEN(STATUS_STILLGELEGT, null),
            BST_IN_BETRIEB_VORGAENGER_IN_BETRIEB(STATUS_IN_BETRIEB, STATUS_IN_BETRIEB),
            BST_STILLGELEGT_VORGAENGER_IN_BETRIEB(STATUS_STILLGELEGT, STATUS_IN_BETRIEB),
            BST_IN_BETRIEB_VORGAENGER_STILLGELEGT(STATUS_IN_BETRIEB, STATUS_STILLGELEGT) {
                @Override
                public boolean expectFailure() {
                    return true;
                }
            },
            BST_STILLGELEGT_VORGAENGER_STILLGELEGT(STATUS_STILLGELEGT, STATUS_STILLGELEGT),
            ;

            private final Referenz statusBetriebsstaette;
            private final Referenz statusVorgaenger;

            Regel41Harness(Referenz statusBetriebsstaette,
                    Referenz statusVorgaenger) {
                this.statusBetriebsstaette = statusBetriebsstaette;
                this.statusVorgaenger = statusVorgaenger;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                        .betriebsstatus(statusBetriebsstaette)
                                                                        .berichtsjahr(JAHR)
                                                                        .build();
                betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                       .id(id++)
                                                                                       .betriebsstaette(betriebsstaette)
                                                                                       .build();
                when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())).thenReturn(
                        Optional.of(euRegBetriebsstaette));
                if (statusVorgaenger != null) {
                    Betriebsstaette vorgaenger = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                       .localId(betriebsstaette.getLocalId())
                                                                       .betriebsstatus(statusVorgaenger)
                                                                       .berichtsjahr(VORJAHR)
                                                                       .build();
                    if (statusBetriebsstaette.equals(STATUS_IN_BETRIEB)
                            && statusVorgaenger.equals(STATUS_STILLGELEGT)) {
                        when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                                betriebsstaette.getLand(), vorgaenger.getLocalId())).thenReturn(
                                Optional.of(vorgaenger));
                    }
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())
                                             .map(EURegBetriebsstaette::getId)
                                             .orElse(null);
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 42 mit dem Kriterium
     * <blockquote>
     * Betriebsstatus einer Anlage im &lt;Jahr-1> "dauerhaft stillgelegt/abgebaut" und im &lt;Jahr> "in Betrieb".
     * </blockquote>
     * Sie überprüft alle (sinnvollen) Kombinationen aus "Anlage nicht vorhanden / in Betrieb / außer Betrieb"
     * einerseits und "Vorgänger-Anlage nicht vorhanden / in Betrieb / außer Betrieb" andererseits.
     */
    @Nested
    class Regel42 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel42.Regel42Harness> {

        enum Regel42Harness implements TestHarness {

            ANLAGE_NICHT_VORHANDEN(false, null, false, null),

            ANLAGE_KEIN_STATUS_VORGAENGER_NICHT_VORHANDEN(true, null, false, null),
            ANLAGE_IN_BETRIEB_VORGAENGER_NICHT_VORHANDEN(true, STATUS_IN_BETRIEB, false, null),
            ANLAGE_STILLGELEGT_VORGAENGER_NICHT_VORHANDEN(true, STATUS_STILLGELEGT, false, null),

            ANLAGE_KEIN_STATUS_VORGAENGER_KEIN_STATUS(true, null, true, null),
            ANLAGE_IN_BETRIEB_VORGAENGER_KEIN_STATUS(true, STATUS_IN_BETRIEB, true, null),
            ANLAGE_STILLGELEGT_VORGAENGER_KEIN_STATUS(true, STATUS_STILLGELEGT, true, null),

            ANLAGE_KEIN_STATUS_VORGAENGER_IN_BETRIEB(true, null, true, STATUS_IN_BETRIEB),
            ANLAGE_IN_BETRIEB_VORGAENGER_IN_BETRIEB(true, STATUS_IN_BETRIEB, true, STATUS_IN_BETRIEB),
            ANLAGE_STILLGELEGT_VORGAENGER_IN_BETRIEB(true, STATUS_STILLGELEGT, true, STATUS_IN_BETRIEB),

            ANLAGE_KEIN_STATUS_VORGAENGER_STILLGELEGT(true, null, true, STATUS_STILLGELEGT),
            ANLAGE_IN_BETRIEB_VORGAENGER_STILLGELEGT(true, STATUS_IN_BETRIEB, true, STATUS_STILLGELEGT),
            ANLAGE_STILLGELEGT_VORGAENGER_STILLGELEGT(true, STATUS_STILLGELEGT, true, STATUS_STILLGELEGT),
            ;

            private final boolean anlageVorhanden;
            private final Referenz statusAnlage;
            private final boolean vorgaengerVorhanden;
            private final Referenz statusVorgaenger;

            Regel42Harness(boolean anlageVorhanden, Referenz statusAnlage, boolean vorgaengerVorhanden,
                    Referenz statusVorgaenger) {
                this.anlageVorhanden = anlageVorhanden;
                this.statusAnlage = statusAnlage;
                this.vorgaengerVorhanden = vorgaengerVorhanden;
                this.statusVorgaenger = statusVorgaenger;
            }

            @Override
            public boolean expectFailure() {
                return this == ANLAGE_IN_BETRIEB_VORGAENGER_STILLGELEGT;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {

                Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                        .berichtsjahr(JAHR)
                                                                        .build();
                betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));

                Anlage anlage = AnlageBuilder.anAnlage(id++)
                                             .betriebsstatus(statusAnlage)
                                             .parentBetriebsstaetteId(betriebsstaette.getId())
                                             .build();
                anlage.setLocalId(String.valueOf(anlage.getId()));
                betriebsstaette.setAnlagen(Collections.singletonList(anlage));
                EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).build();

                if (anlageVorhanden) {
                    lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                             .thenReturn(betriebsstaette);
                    euRegAnlage.setAnlage(anlage);
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                } else {
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                    euRegAnlage.setAnlagenteil(anlagenteil);
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                }
                if (vorgaengerVorhanden) {
                    Betriebsstaette vorgaengerBst = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                          .localId(betriebsstaette.getLocalId())
                                                                          .berichtsjahr(VORJAHR)
                                                                          .build();
                    lenient().when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                                     vorgaengerBst.getLand(), vorgaengerBst.getLocalId()))
                             .thenReturn(Optional.of(vorgaengerBst));
                    Anlage vorgaengerAnlage = AnlageBuilder.anAnlage(id++)
                                                           .localId(anlage.getLocalId())
                                                           .betriebsstatus(statusVorgaenger)
                                                           .parentBetriebsstaetteId(vorgaengerBst.getId())
                                                           .build();
                    lenient().when(test.stammdatenService.findAnlageByLocalId(vorgaengerBst.getId(),
                            vorgaengerAnlage.getLocalId())).thenReturn(Optional.of(vorgaengerAnlage));
                    vorgaengerBst.setAnlagen(Collections.singletonList(vorgaengerAnlage));
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                if (anlageVorhanden) {
                    return test.euRegistryService.findAnlagenBerichtByAnlageId(
                            betriebsstaette.getAnlagen().get(0).getId()).map(EURegAnlage::getId).orElse(null);
                } else {
                    return test.euRegistryService.findAnlagenBerichtByAnlagenteilId(
                                       betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId())
                                                 .map(EURegAnlage::getId)
                                                 .orElse(null);
                }
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 43 mit dem Kriterium
     * <blockquote>
     * Betriebsstatus eines Anlagenteils im &lt;Jahr-1> "dauerhaft stillgelegt/abgebaut"
     * und im &lt;Jahr> "in Betrieb".
     * </blockquote>
     * Die Logik verläuft analog zu {@link Regel42}.
     */
    @Nested
    class Regel43 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel43.Regel43Harness> {

        enum Regel43Harness implements TestHarness {

            ANLAGENTEIL_NICHT_VORHANDEN(false, null, false, null),

            ANLAGENTEIL_KEIN_STATUS_VORGAENGER_NICHT_VORHANDEN(true, null, false, null),
            ANLAGENTEIL_IN_BETRIEB_VORGAENGER_NICHT_VORHANDEN(true, STATUS_IN_BETRIEB, false, null),
            ANLAGENTEIL_STILLGELEGT_VORGAENGER_NICHT_VORHANDEN(true, STATUS_STILLGELEGT, false, null),

            ANLAGENTEIL_KEIN_STATUS_VORGAENGER_KEIN_STATUS(true, null, true, null),
            ANLAGENTEIL_IN_BETRIEB_VORGAENGER_KEIN_STATUS(true, STATUS_IN_BETRIEB, true, null),
            ANLAGENTEIL_STILLGELEGT_VORGAENGER_KEIN_STATUS(true, STATUS_STILLGELEGT, true, null),

            ANLAGENTEIL_KEIN_STATUS_VORGAENGER_IN_BETRIEB(true, null, true, STATUS_IN_BETRIEB),
            ANLAGENTEIL_IN_BETRIEB_VORGAENGER_IN_BETRIEB(true, STATUS_IN_BETRIEB, true, STATUS_IN_BETRIEB),
            ANLAGENTEIL_STILLGELEGT_VORGAENGER_IN_BETRIEB(true, STATUS_STILLGELEGT, true, STATUS_IN_BETRIEB),

            ANLAGENTEIL_KEIN_STATUS_VORGAENGER_STILLGELEGT(true, null, true, STATUS_STILLGELEGT),
            ANLAGENTEIL_IN_BETRIEB_VORGAENGER_STILLGELEGT(true, STATUS_IN_BETRIEB, true, STATUS_STILLGELEGT),
            ANLAGENTEIL_STILLGELEGT_VORGAENGER_STILLGELEGT(true, STATUS_STILLGELEGT, true, STATUS_STILLGELEGT),
            ;

            private final boolean anlagenteilVorhanden;
            private final Referenz statusAnlagenteil;
            private final boolean vorgaengerVorhanden;
            private final Referenz statusVorgaenger;

            Regel43Harness(boolean anlagenteilVorhanden, Referenz statusAnlagenteil, boolean vorgaengerVorhanden,
                    Referenz statusVorgaenger) {
                this.anlagenteilVorhanden = anlagenteilVorhanden;
                this.statusAnlagenteil = statusAnlagenteil;
                this.vorgaengerVorhanden = vorgaengerVorhanden;
                this.statusVorgaenger = statusVorgaenger;
            }

            @Override
            public boolean expectFailure() {
                return this == ANLAGENTEIL_IN_BETRIEB_VORGAENGER_STILLGELEGT;
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {

                Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                        .berichtsjahr(JAHR)
                                                                        .build();
                betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));

                Anlage anlage = AnlageBuilder.anAnlage(id++)
                                             .parentBetriebsstaetteId(betriebsstaette.getId())
                                             .build();
                anlage.setLocalId(String.valueOf(anlage.getId()));
                betriebsstaette.setAnlagen(Collections.singletonList(anlage));
                lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                         .thenReturn(betriebsstaette);

                EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).build();

                if (!anlagenteilVorhanden) {
                    euRegAnlage.setAnlage(anlage);
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                } else {
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .parentAnlageId(anlage.getId())
                                                                .betriebsstatus(statusAnlagenteil)
                                                                .build();
                    anlagenteil.setLocalId(String.valueOf(anlagenteil.getId()));
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));
                    lenient().when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    euRegAnlage.setAnlagenteil(anlagenteil);
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    if (vorgaengerVorhanden) {
                        Betriebsstaette vorgaengerBst = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                              .localId(betriebsstaette.getLocalId())
                                                                              .berichtsjahr(VORJAHR)
                                                                              .build();
                        lenient().when(
                                test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                                        vorgaengerBst.getLand(), vorgaengerBst.getLocalId())).thenReturn(
                                Optional.of(vorgaengerBst));
                        Anlage vorgaengerAnlage = AnlageBuilder.anAnlage(id++)
                                                               .localId(anlage.getLocalId())
                                                               .parentBetriebsstaetteId(vorgaengerBst.getId())
                                                               .build();
                        vorgaengerBst.setAnlagen(Collections.singletonList(vorgaengerAnlage));
                        lenient().when(test.stammdatenService.findAnlageByLocalId(vorgaengerBst.getId(),
                                vorgaengerAnlage.getLocalId())).thenReturn(Optional.of(vorgaengerAnlage));
                        Anlagenteil vorgaengerAnlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                              .localId(anlagenteil.getLocalId())
                                                                              .parentAnlageId(vorgaengerAnlage.getId())
                                                                              .betriebsstatus(statusVorgaenger)
                                                                              .build();
                        vorgaengerAnlage.setAnlagenteile(Collections.singletonList(vorgaengerAnlagenteil));
                        lenient().when(test.stammdatenService.findAnlagenteilByLocalId(vorgaengerAnlage.getId(),
                                         vorgaengerBst.getId(), vorgaengerAnlagenteil.getLocalId()))
                                 .thenReturn(Optional.of(vorgaengerAnlagenteil));

                    }
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                if (anlagenteilVorhanden) {
                    return test.euRegistryService.findAnlagenBerichtByAnlagenteilId(
                                       betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId())
                                                 .map(EURegAnlage::getId)
                                                 .orElse(null);
                } else {
                    return test.euRegistryService.findAnlagenBerichtByAnlageId(
                                       betriebsstaette.getAnlagen().get(0).getId())
                                                 .map(EURegAnlage::getId)
                                                 .orElse(null);
                }
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 45 mit dem Kriterium
     * <blockquote>
     * |Rechts-/Ostwert <Jahr> - Rechts-/Ostwert <Jahr-1>| gt 100m
     * oder |Hoch-/Nordwert <Jahr-1> - Hoch-/Nordwert <Jahr>| gt 100m.
     * </blockquote>
     */
    @Nested
    class Regel45 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel45.Regel45Harness> {

        enum Regel45Harness implements TestHarness {

            BST_OHNE_GEO_PUNKT {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR)
                                                                            .geoPunkt(null)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .id(id++)
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())).thenReturn(
                            Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }
            },
            BST_OHNE_VORGAENGER {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BST_OHNE_GEO_PUNKT.setupBetriebsstaette(test);
                    betriebsstaette.setGeoPunkt(randomGeoPunkt());
                    return betriebsstaette;
                }
            },
            VORGAENGER_BST__OHNE_GEO_PUNKT,
            VORGAENGER_BST__OST_ZU_GROSS,
            VORGAENGER_BST__OST_ZU_KLEIN,
            VORGAENGER_BST__NORD_ZU_GROSS,
            VORGAENGER_BST__NORD_ZU_KLEIN,
            VORGAENGER_BST__GEO_PUNKT_OKAY,
            ANLAGE_OHNE_GEO_PUNKT {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR)
                                                                            .geoPunkt(null)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                             .thenReturn(betriebsstaette);
                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .geoPunkt(null)
                                                 .parentBetriebsstaetteId(betriebsstaette.getId())
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    betriebsstaette.setAnlagen(Collections.singletonList(anlage));

                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.ofNullable(euRegAnlage));

                    return betriebsstaette;
                }
            },
            ANLAGE_OHNE_VORGAENGER {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = ANLAGE_OHNE_GEO_PUNKT.setupBetriebsstaette(test);
                    betriebsstaette.getAnlagen().get(0).setGeoPunkt(randomGeoPunkt());
                    return betriebsstaette;
                }
            },
            VORGAENGER_ANLAGE__OHNE_GEO_PUNKT,
            VORGAENGER_ANLAGE__OST_ZU_GROSS,
            VORGAENGER_ANLAGE__OST_ZU_KLEIN,
            VORGAENGER_ANLAGE__NORD_ZU_GROSS,
            VORGAENGER_ANLAGE__NORD_ZU_KLEIN,
            VORGAENGER_ANLAGE__GEO_PUNKT_OKAY,
            ANLAGENTEIL_OHNE_GEO_PUNKT {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR)
                                                                            .geoPunkt(null)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    lenient().when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                             .thenReturn(betriebsstaette);

                    Anlage anlage = AnlageBuilder.anAnlage(id++)
                                                 .geoPunkt(null)
                                                 .parentBetriebsstaetteId(betriebsstaette.getId())
                                                 .build();
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    betriebsstaette.setAnlagen(Collections.singletonList(anlage));
                    lenient().when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);

                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                .geoPunkt(null)
                                                                .parentAnlageId(anlage.getId())
                                                                .build();
                    anlagenteil.setLocalId(String.valueOf(anlagenteil.getId()));
                    anlage.setAnlagenteile(Collections.singletonList(anlagenteil));

                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.ofNullable(euRegAnlage));

                    return betriebsstaette;
                }
            },
            ANLAGENTEIL_OHNE_VORGAENGER {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Betriebsstaette betriebsstaette = ANLAGENTEIL_OHNE_GEO_PUNKT.setupBetriebsstaette(test);
                    betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).setGeoPunkt(randomGeoPunkt());
                    return betriebsstaette;
                }
            },
            VORGAENGER_ANLAGENTEIL__OHNE_GEO_PUNKT,
            VORGAENGER_ANLAGENTEIL__OST_ZU_GROSS,
            VORGAENGER_ANLAGENTEIL__OST_ZU_KLEIN,
            VORGAENGER_ANLAGENTEIL__NORD_ZU_GROSS,
            VORGAENGER_ANLAGENTEIL__NORD_ZU_KLEIN,
            VORGAENGER_ANLAGENTEIL__GEO_PUNKT_OKAY,
            ;

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                if (name().contains("BST_")) {
                    return test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId())
                                                 .map(EURegBetriebsstaette::getId)
                                                 .orElse(null);
                } else if (name().contains("ANLAGE_")) {
                    Anlage anlage = betriebsstaette.getAnlagen().get(0);
                    return test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())
                                                 .map(EURegAnlage::getId)
                                                 .orElse(null);
                } else if (name().contains("ANLAGENTEIL_")) {
                    Anlagenteil anlagenteil = betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0);
                    return test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())
                                                 .map(EURegAnlage::getId)
                                                 .orElse(null);
                } else {
                    throw new IllegalStateException(name());
                }
            }

            enum GeoPunktSetup {
                OHNE_GEO_PUNKT {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        return null;
                    }
                },
                OST_ZU_GROSS {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        long ost = original.getOst() + 101;
                        return new GeoPunkt(original.getNord(), ost, original.getEpsgCode());
                    }
                },
                OST_ZU_KLEIN {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        long ost = original.getOst() - 101;
                        return new GeoPunkt(original.getNord(), ost, original.getEpsgCode());
                    }
                },
                NORD_ZU_GROSS {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        long nord = original.getNord() + 101;
                        return new GeoPunkt(nord, original.getOst(), original.getEpsgCode());
                    }
                },
                NORD_ZU_KLEIN {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        long nord = original.getNord() - 101;
                        return new GeoPunkt(nord, original.getOst(), original.getEpsgCode());
                    }
                },
                GEO_PUNKT_OKAY {
                    @Override
                    GeoPunkt setup(GeoPunkt original) {
                        long nord = original.getNord() + 100;
                        long ost = original.getOst() - 100;
                        return new GeoPunkt(nord, ost, original.getEpsgCode());
                    }
                };

                abstract GeoPunkt setup(GeoPunkt original);
            }

            GeoPunkt randomGeoPunkt() {
                long nord = (long) (1_000_000 * Math.random());
                long ost = (long) (1_000_000 * Math.random());
                return new GeoPunkt(nord, ost, EPSG25832);
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {

                String[] split = name().split("__");
                if (split.length == 1) {
                    throw new UnsupportedOperationException(name());
                }

                String typ = split[0].substring("VORGAENGER_".length());
                Betriebsstaette betriebsstaette = valueOf(typ + "_OHNE_VORGAENGER").setupBetriebsstaette(test);

                VorgaengerSetup vorgaengerSetup = VorgaengerSetup.valueOf(typ);
                GeoPunktSetup geoPunktSetup = GeoPunktSetup.valueOf(split[1]);

                GeoPunkt original = vorgaengerSetup.readGeoPunktFromPruefObjekt(betriebsstaette);
                GeoPunkt newGeoPunkt = geoPunktSetup.setup(original);
                vorgaengerSetup.setup(betriebsstaette, newGeoPunkt, test);

                return betriebsstaette;

            }

            enum VorgaengerSetup {

                BST {
                    @Override
                    GeoPunkt readGeoPunktFromPruefObjekt(Betriebsstaette betriebsstaette) {
                        return betriebsstaette.getGeoPunkt();
                    }

                    @Override
                    public Betriebsstaette setup(Betriebsstaette betriebsstaette, GeoPunkt newGeoPunkt,
                            KomplexeRegelPruefungServiceTest test) {
                        Betriebsstaette vorgaenger = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                           .berichtsjahr(VORJAHR)
                                                                           .localId(betriebsstaette.getLocalId())
                                                                           .geoPunkt(newGeoPunkt)
                                                                           .build();
                        when(test.stammdatenService.findBetriebsstaetteByJahrLandLocalId(VORJAHR.getSchluessel(),
                                vorgaenger.getLand(), vorgaenger.getLocalId())).thenReturn(Optional.of(vorgaenger));
                        return vorgaenger;
                    }
                },
                ANLAGE {
                    @Override
                    GeoPunkt readGeoPunktFromPruefObjekt(Betriebsstaette betriebsstaette) {
                        return betriebsstaette.getAnlagen().get(0).getGeoPunkt();
                    }

                    @Override
                    Betriebsstaette setup(Betriebsstaette betriebsstaette, GeoPunkt newGeoPunkt,
                            KomplexeRegelPruefungServiceTest test) {

                        Anlage anlage = betriebsstaette.getAnlagen().get(0);
                        Betriebsstaette vorgaengerBst = BST.setup(betriebsstaette, null, test);
                        Anlage vorgaengerAnlage = AnlageBuilder.anAnlage(id++)
                                                               .localId(anlage.getLocalId())
                                                               .geoPunkt(newGeoPunkt)
                                                               .parentBetriebsstaetteId(vorgaengerBst.getId())
                                                               .build();
                        vorgaengerBst.setAnlagen(Collections.singletonList(vorgaengerAnlage));
                        when(test.stammdatenService.findAnlageByLocalId(vorgaengerBst.getId(),
                                vorgaengerAnlage.getLocalId())).thenReturn(
                                Optional.of(vorgaengerAnlage));
                        return vorgaengerBst;
                    }
                },
                ANLAGENTEIL {
                    @Override
                    GeoPunkt readGeoPunktFromPruefObjekt(Betriebsstaette betriebsstaette) {
                        return betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getGeoPunkt();
                    }

                    @Override
                    Betriebsstaette setup(Betriebsstaette betriebsstaette, GeoPunkt newGeoPunkt,
                            KomplexeRegelPruefungServiceTest test) {
                        Anlage anlage = betriebsstaette.getAnlagen().get(0);
                        Anlagenteil anlagenteil = anlage.getAnlagenteile().get(0);
                        Betriebsstaette vorgaengerBst = ANLAGE.setup(betriebsstaette, null, test);
                        Anlage vorgaengerAnlage = vorgaengerBst.getAnlagen().get(0);
                        Anlagenteil vorgaengerAnlagenteil = AnlagenteilBuilder.anAnlagenteil(id++)
                                                                              .localId(anlagenteil.getLocalId())
                                                                              .geoPunkt(newGeoPunkt)
                                                                              .parentAnlageId(vorgaengerAnlage.getId())
                                                                              .build();
                        vorgaengerAnlage.setAnlagenteile(Collections.singletonList(vorgaengerAnlagenteil));
                        when(test.stammdatenService.findAnlagenteilByLocalId(vorgaengerAnlage.getId(),
                                vorgaengerBst.getId(), vorgaengerAnlagenteil.getLocalId())).thenReturn(
                                Optional.of(vorgaengerAnlagenteil));
                        return vorgaengerBst;
                    }
                };

                abstract GeoPunkt readGeoPunktFromPruefObjekt(Betriebsstaette betriebsstaette);

                abstract Betriebsstaette setup(Betriebsstaette betriebsstaette, GeoPunkt newGeoPunkt,
                        KomplexeRegelPruefungServiceTest test);

            }

            @Override
            public boolean expectFailure() {
                return name().contains("_ZU_");
            }

        }

    }

    /**
     * Diese Klasse testet die Prüfregel 51 mit dem Kriterium
     * <blockquote>
     * Wenn abweichende Regelung = ja und [Regelung beginnt am ist leer oder Regelung endet am ist leer
     * oder BVT-assoziierter Emissionswert ist leer oder URL öffentliche Begründung ist leer] -> BATDerogation.
     * </blockquote>
     * Ein Teil der Prüfung ist eigentlich überflüssig, da die Attribute "Emissionswert", "Startdatum" und
     * "URL" bereits in der Anwendung (wie auch in der Datenbank) als Pflichattribute eingerichtet sind.
     */
    @Nested
    class Regel51 extends AbstractRegelTest<Regel51.Regel51Harness> {

        private static final Referenz EMISSIONSWERT = ReferenzBuilder.aEmissionswertReferenz("BVT-SF 20-XIV").build();
        private static final LocalDate START = LocalDate.of(2020, 1, 1);
        private static final LocalDate ENDE = LocalDate.of(2022, 12, 31);
        private static final String URL = "http://www.test.de";

        enum Stammdaten {
            ANLAGE {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).berichtsdaten(
                            BerichtsdatenBuilder.anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                public EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette,
                        KomplexeRegelPruefungServiceTest test) {
                    Long anlageId = betriebsstaette.getAnlagen().get(0).getId();
                    return test.euRegistryService.findAnlagenBerichtByAnlageId(anlageId).get();
                }
            },
            ANLAGENTEIL {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++).parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(List.of(anlagenteil));
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .berichtsdaten(BerichtsdatenBuilder
                                                                        .anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                public EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette,
                        KomplexeRegelPruefungServiceTest test) {
                    Long anlagenteilId = betriebsstaette.getAnlagen().get(0).getAnlagenteile().get(0).getId();
                    return test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilId).get();
                }
            };

            public abstract Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test);

            public abstract EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette,
                    KomplexeRegelPruefungServiceTest test);

        }

        enum Typ {
            OHNE_AUSNAHMEN {
                @Override
                public BVTAusnahme getAusnahme() {
                    return null;
                }
            },
            MIT_AUSNAHME {
                @Override
                public BVTAusnahme getAusnahme() {
                    BVTAusnahme ausnahme = new BVTAusnahme(EMISSIONSWERT, START, URL);
                    ausnahme.setEndetAm(ENDE);
                    return ausnahme;
                }
            },
            MIT_AUSNAHME_OHNE_EMISSIONSWERT {
                @Override
                public BVTAusnahme getAusnahme() {
                    BVTAusnahme ausnahme = new BVTAusnahme(null, START, URL);
                    ausnahme.setEndetAm(ENDE);
                    return ausnahme;
                }
            },
            MIT_AUSNAHME_OHNE_START {
                @Override
                public BVTAusnahme getAusnahme() {
                    BVTAusnahme ausnahme = new BVTAusnahme(EMISSIONSWERT, null, URL);
                    ausnahme.setEndetAm(ENDE);
                    return ausnahme;
                }
            },
            MIT_AUSNAHME_OHNE_ENDE {
                @Override
                public BVTAusnahme getAusnahme() {
                    return new BVTAusnahme(EMISSIONSWERT, START, URL);
                }
            },
            MIT_AUSNAHME_OHNE_URL {
                @Override
                public BVTAusnahme getAusnahme() {
                    BVTAusnahme ausnahme = new BVTAusnahme(EMISSIONSWERT, START, null);
                    ausnahme.setEndetAm(ENDE);
                    return ausnahme;
                }
            },
            ;

            public abstract BVTAusnahme getAusnahme();
        }

        enum Regel51Harness implements TestHarness {

            ANLAGE_OHNE_AUSNAHMEN,
            ANLAGE_MIT_AUSNAHME,
            ANLAGE_MIT_AUSNAHME_OHNE_EMISSIONSWERT,
            ANLAGE_MIT_AUSNAHME_OHNE_START,
            ANLAGE_MIT_AUSNAHME_OHNE_ENDE,
            ANLAGE_MIT_AUSNAHME_OHNE_URL,
            ANLAGENTEIL_OHNE_AUSNAHMEN,
            ANLAGENTEIL_MIT_AUSNAHME,
            ANLAGENTEIL_MIT_AUSNAHME_OHNE_EMISSIONSWERT,
            ANLAGENTEIL_MIT_AUSNAHME_OHNE_START,
            ANLAGENTEIL_MIT_AUSNAHME_OHNE_ENDE,
            ANLAGENTEIL_MIT_AUSNAHME_OHNE_URL,
            ;

            private final Stammdaten stammdaten;
            private final Typ typ;

            Regel51Harness() {
                String[] split = name().split("_", 2);
                stammdaten = Stammdaten.valueOf(split[0]);
                typ = Typ.valueOf(split[1]);
            }

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Betriebsstaette betriebsstaette = stammdaten.setupBetriebsstaette(test);
                BVTAusnahme ausnahme = typ.getAusnahme();
                if (ausnahme != null) {
                    stammdaten.getEuRegAnlage(betriebsstaette, test).setBvtAusnahmen(List.of(ausnahme));
                }
                return betriebsstaette;
            }

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                return stammdaten.getEuRegAnlage(betriebsstaette, test).getId();
            }

            @Override
            public boolean expectFailure() {
                return typ != Typ.OHNE_AUSNAHMEN && typ != Typ.MIT_AUSNAHME;
            }

        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 57 mit dem Kriterium
     * <blockquote>
     * Feld strengere Auflagen vorhanden = ja und Art. 18 IE-RL leer und Art 14.4 IE-RL leer.
     * </blockquote>
     */
    @Nested
    class Regel57 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel57.Regel57Harness> {

        enum Regel57Harness implements TestHarness {

            ANLAGE_OHNE_AUFLAGEN {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).berichtsdaten(
                            BerichtsdatenBuilder.anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGE_MIT_ARTIKEL14 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), true, false);
                }
            },
            ANLAGE_MIT_ARTIKEL18 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), false, true);
                }
            },
            ANLAGE_MIT_ARTIKEL14_18 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), true, true);
                }
            },
            ANLAGE_OHNE_ARTIKEL {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), false, false);
                }
            },
            ANLAGENTEIL_OHNE_AUFLAGEN {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++).parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(List.of(anlagenteil));
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .berichtsdaten(BerichtsdatenBuilder
                                                                        .anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGENTEIL_MIT_ARTIKEL14 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), true, false);
                }
            },
            ANLAGENTEIL_MIT_ARTIKEL18 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), false, true);
                }
            },
            ANLAGENTEIL_MIT_ARTIKEL14_18 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), true, true);
                }
            },
            ANLAGENTEIL_OHNE_ARTIKEL {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    addAuflage(getEuRegAnlage(betriebsstaette, test), false, false);
                }
            },
            ;

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Regel57Harness predecessor = Regel57Harness.values()[this.ordinal() - 1];
                Betriebsstaette betriebsstaette = predecessor.setupBetriebsstaette(test);
                this.extendBetriebsstaette(betriebsstaette, test);
                return betriebsstaette;
            }

            abstract void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                return euRegAnlage.getId();
            }

            EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                Anlage anlage = betriebsstaette.getAnlagen().get(0);
                EURegAnlage euRegAnlage;
                if (anlage.getAnlagenteile().isEmpty()) {
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId()).get();
                } else {
                    Long anlagenteilId = anlage.getAnlagenteile().get(0).getId();
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilId).get();
                }
                return euRegAnlage;
            }

            void addAuflage(EURegAnlage euRegAnlage, boolean art14, boolean art18) {
                List<Auflage> auflagen = euRegAnlage.getAuflagen() != null ?
                        new ArrayList<>(euRegAnlage.getAuflagen()) :
                        new ArrayList<>();
                auflagen.add(AuflageBuilder.anAuflage().nachArt14(art14).nachArt18(art18).build());
                euRegAnlage.setAuflagen(auflagen);
            }

            @Override
            public boolean expectFailure() {
                return name().contains("_OHNE_ARTIKEL");
            }

        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 61 mit dem Kriterium
     * <blockquote>
     * EURegF.derogation.DEROGATION = "Ausnahme nach Art. 33 IE-RL -  Ausnahme für beschränkte Laufzeit"
     * und (EURegF.derogation.JAHRBIS oder JAHR) gt 2023.
     * </blockquote>
     */
    @Nested
    class Regel61 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel61.Regel61Harness> {

        static final Referenz ARTIKEL31 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article31").build();
        static final Referenz ARTIKEL32 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article32").build();
        static final Referenz ARTIKEL33 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article33").build();

        static final Referenz JAHR2023 = ReferenzBuilder.aJahrReferenz(2023).build();
        static final Referenz JAHR2024 = ReferenzBuilder.aJahrReferenz(2024).build();

        enum Regel61Harness implements TestHarness {
            ANLAGE_OHNE_FEUERUNGSGANLAGE {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR2024)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).berichtsdaten(
                            BerichtsdatenBuilder.anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGE_OHNE_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    euRegAnlage.setFeuerungsanlage(
                            FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten().build());
                }
            },
            ANLAGE_MIT_UNKRITISCHEN_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    FeuerungsanlageBerichtsdaten feuerungsanlage = euRegAnlage.getFeuerungsanlage();
                    feuerungsanlage.setIeAusnahmen(List.of(ARTIKEL31, ARTIKEL32));
                }
            },
            ANLAGE_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL33 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    FeuerungsanlageBerichtsdaten feuerungsanlage = euRegAnlage.getFeuerungsanlage();
                    feuerungsanlage.setIeAusnahmen(List.of(ARTIKEL32, ARTIKEL33));
                }
            },
            ANLAGE_MIT_UNKRITISCHM_BERICHTSJAHR {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    betriebsstaette.setBerichtsjahr(JAHR2023);
                }
            },
            ANLAGENTEIL_OHNE_FEUERUNGSGANLAGE {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR2024)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++).parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(List.of(anlagenteil));
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .berichtsdaten(BerichtsdatenBuilder
                                                                        .anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.ofNullable(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGENTEIL_OHNE_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_OHNE_IE_AUSNAHMEN.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_UNKRITISCHEN_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_UNKRITISCHEN_IE_AUSNAHMEN.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL33 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL33.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_UNKRITISCHM_BERICHTSJAHR {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_UNKRITISCHM_BERICHTSJAHR.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ;

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Regel61Harness predecessor = Regel61Harness.values()[this.ordinal() - 1];
                Betriebsstaette betriebsstaette = predecessor.setupBetriebsstaette(test);
                this.extendBetriebsstaette(betriebsstaette, test);
                return betriebsstaette;
            }

            abstract void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                return euRegAnlage.getId();
            }

            EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                Anlage anlage = betriebsstaette.getAnlagen().get(0);
                EURegAnlage euRegAnlage;
                if (anlage.getAnlagenteile().isEmpty()) {
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId()).get();
                } else {
                    Long anlagenteilId = anlage.getAnlagenteile().get(0).getId();
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilId).get();
                }
                return euRegAnlage;
            }

            @Override
            public boolean expectFailure() {
                return name().contains("_KRITISCH");
            }
        }

    }

    /**
     * Diese Testklasse testet die Prüfregel 61 mit dem Kriterium
     * <blockquote>
     * EURegF.derogation.DEROGATION = "Ausnahme nach Art. 35 IE-RL - Fernwärmeanlagen" und JAHR gt 2022.
     * </blockquote>
     */
    @Nested
    class Regel62 extends KomplexeRegelPruefungServiceTest.AbstractRegelTest<Regel62.Regel62Harness> {

        static final Referenz ARTIKEL33 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article33").build();
        static final Referenz ARTIKEL34 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article34").build();
        static final Referenz ARTIKEL35 = ReferenzBuilder.aReferenz(Referenzliste.RDERO, "Article35").build();

        static final Referenz JAHR2022 = ReferenzBuilder.aJahrReferenz(2022).build();
        static final Referenz JAHR2023 = ReferenzBuilder.aJahrReferenz(2023).build();

        enum Regel62Harness implements TestHarness {
            ANLAGE_OHNE_FEUERUNGSGANLAGE {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR2023)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++).anlage(anlage).berichtsdaten(
                            BerichtsdatenBuilder.anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGE_OHNE_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    euRegAnlage.setFeuerungsanlage(
                            FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten().build());
                }
            },
            ANLAGE_MIT_UNKRITISCHEN_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    FeuerungsanlageBerichtsdaten feuerungsanlage = euRegAnlage.getFeuerungsanlage();
                    feuerungsanlage.setIeAusnahmen(List.of(ARTIKEL33, ARTIKEL34));
                }
            },
            ANLAGE_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL35 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                    FeuerungsanlageBerichtsdaten feuerungsanlage = euRegAnlage.getFeuerungsanlage();
                    feuerungsanlage.setIeAusnahmen(List.of(ARTIKEL34, ARTIKEL35));
                }
            },
            ANLAGE_MIT_UNKRITISCHM_BERICHTSJAHR {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    betriebsstaette.setBerichtsjahr(JAHR2022);
                }
            },
            ANLAGENTEIL_OHNE_FEUERUNGSGANLAGE {
                @Override
                public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                    Anlage anlage = AnlageBuilder.anAnlage(id++).build();
                    Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(id++)
                                                                            .berichtsjahr(JAHR2023)
                                                                            .anlagen(anlage)
                                                                            .build();
                    betriebsstaette.setLocalId(String.valueOf(betriebsstaette.getId()));
                    anlage.setParentBetriebsstaetteId(betriebsstaette.getId());
                    anlage.setLocalId(String.valueOf(anlage.getId()));
                    when(test.stammdatenService.loadBetriebsstaetteForRead(betriebsstaette.getId()))
                            .thenReturn(betriebsstaette);
                    Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil(id++).parentAnlageId(anlage.getId())
                                                                .build();
                    anlage.setAnlagenteile(List.of(anlagenteil));
                    when(test.stammdatenService.loadAnlage(anlage.getId())).thenReturn(anlage);
                    EURegAnlage euRegAnlage = EURegAnlageBuilder.anEURegAnlage(id++)
                                                                .anlage(null)
                                                                .anlagenteil(anlagenteil)
                                                                .berichtsdaten(BerichtsdatenBuilder
                                                                        .anEuRegFBerichtsdaten().build()).build();
                    when(test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                            Optional.of(euRegAnlage));
                    EURegBetriebsstaette euRegBetriebsstaette = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette()
                                                                                           .betriebsstaette(
                                                                                                   betriebsstaette)
                                                                                           .build();
                    when(test.euRegistryService.findBetriebsstaettenBerichtByBstId(betriebsstaette.getId()))
                            .thenReturn(Optional.of(euRegBetriebsstaette));
                    return betriebsstaette;
                }

                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    throw new UnsupportedOperationException();
                }
            },
            ANLAGENTEIL_OHNE_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_OHNE_IE_AUSNAHMEN.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_UNKRITISCHEN_IE_AUSNAHMEN {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_UNKRITISCHEN_IE_AUSNAHMEN.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL35 {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_KRITISCHEM_JAHR_BEI_ARTIKEL35.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ANLAGENTEIL_MIT_UNKRITISCHM_BERICHTSJAHR {
                @Override
                void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                    ANLAGE_MIT_UNKRITISCHM_BERICHTSJAHR.extendBetriebsstaette(betriebsstaette, test);
                }
            },
            ;

            @Override
            public Betriebsstaette setupBetriebsstaette(KomplexeRegelPruefungServiceTest test) {
                Regel62Harness predecessor = Regel62Harness.values()[this.ordinal() - 1];
                Betriebsstaette betriebsstaette = predecessor.setupBetriebsstaette(test);
                this.extendBetriebsstaette(betriebsstaette, test);
                return betriebsstaette;
            }

            abstract void extendBetriebsstaette(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test);

            @Override
            public Long getPruefObjektId(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                EURegAnlage euRegAnlage = getEuRegAnlage(betriebsstaette, test);
                return euRegAnlage.getId();
            }

            EURegAnlage getEuRegAnlage(Betriebsstaette betriebsstaette, KomplexeRegelPruefungServiceTest test) {
                Anlage anlage = betriebsstaette.getAnlagen().get(0);
                EURegAnlage euRegAnlage;
                if (anlage.getAnlagenteile().isEmpty()) {
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlageId(anlage.getId()).get();
                } else {
                    Long anlagenteilId = anlage.getAnlagenteile().get(0).getId();
                    euRegAnlage = test.euRegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteilId).get();
                }
                return euRegAnlage;
            }

            @Override
            public boolean expectFailure() {
                return name().contains("_KRITISCH");
            }
        }

    }

}
