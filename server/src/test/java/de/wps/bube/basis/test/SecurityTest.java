package de.wps.bube.basis.test;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@EnableGlobalMethodSecurity(securedEnabled = true)
@ContextConfiguration(classes = BenutzerTestConfig.class)
@ActiveProfiles("unittest")
public abstract class SecurityTest {

}
