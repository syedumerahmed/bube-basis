package de.wps.bube.basis.referenzdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.*;

import java.time.Instant;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

public final class ReferenzBuilder {
    private static final Instant NOW = Instant.now();
    private Long id;
    private String referenznummer;
    private Land land;
    private Referenzliste referenzliste;
    private String schluessel;
    private String sortier;
    private String ktext;
    private String ltext;
    private Gueltigkeitsjahr gueltigVon;
    private Gueltigkeitsjahr gueltigBis;
    private Instant letzteAenderung;
    private String eu;
    private String strg;

    private ReferenzBuilder() {
    }

    public static ReferenzBuilder aReferenz(Referenzliste referenzliste, String schluessel) {
        return new ReferenzBuilder().referenzliste(referenzliste).referenznummer(Referenz.generateNewReferenznummer())
                                    .land(Land.DEUTSCHLAND)
                                    .letzteAenderung(NOW)
                                    .ktext(schluessel)
                                    .sortier(schluessel)
                                    .schluessel(schluessel)
                                    .eu(schluessel + "EU")
                                    .gueltigVon(Gueltigkeitsjahr.of(2020))
                                    .gueltigBis(Gueltigkeitsjahr.of(2030));
    }

    public static ReferenzBuilder aJahrReferenz(int jahr) {
        return aReferenz(RJAHR, String.valueOf(jahr));
    }

    public static ReferenzBuilder aBearbeitungsstatusReferenz(String status) {
        return aReferenz(RBEA, status);
    }

    public static ReferenzBuilder aBetriebsstatusReferenz(String status) {
        return aReferenz(RBETZ, status).eu("EU" + status);
    }

    public static ReferenzBuilder aKommunikationsTypReferenz(String schluessel) {
        return aReferenz(RKOM, schluessel);
    }

    public static ReferenzBuilder aNaceCodeReferenz(String code) {
        return aReferenz(RNACE, code).eu("EU" + code);
    }

    public static ReferenzBuilder aGemeindeReferenz(String gemeindeKennziffer) {
        return aReferenz(RGMD, gemeindeKennziffer);
    }

    public static ReferenzBuilder aVertraulichkeitsReferenz(String grund) {
        return aReferenz(RCONF, grund).eu("EU" + grund);
    }

    public static ReferenzBuilder aVorschriftReferenz(String vorschrift) {
        return aReferenz(RVORS, vorschrift);
    }

    public static ReferenzBuilder aZustaendigkeitsReferenz(String zustaendigkeit) {
        return aReferenz(RZUST, zustaendigkeit);
    }

    public static ReferenzBuilder anEpsgReferenz(String epsg) {
        return aReferenz(REPSG, epsg);
    }

    public static ReferenzBuilder aTaetigkeitsReferenz(String taetigkeit) {
        return aTaetigkeitsReferenz(RPRTR, taetigkeit);
    }

    public static ReferenzBuilder aTaetigkeitsReferenz(Referenzliste referenzliste, String taetigkeit) {
        return aReferenz(referenzliste, taetigkeit).eu("EU" + taetigkeit);
    }

    public static ReferenzBuilder aEmissionswertReferenz(String emissionswert) {
        return aReferenz(RBATE, emissionswert);
    }

    public static ReferenzBuilder aLeistungseinheitReferenz(String einheit) {
        return aReferenz(REINH, einheit);
    }

    public static ReferenzBuilder aFunktionsReferenz(String funktion) {
        return aReferenz(RFKN, funktion);
    }

    public static ReferenzBuilder aBerichtsartReferenz(String berichtsart) {
        return aReferenz(RRTYP, berichtsart).eu("EU" + berichtsart);
    }

    public static ReferenzBuilder aFlusseinzugsgebiet(String flusseinzugsgebiet) {
        return aReferenz(RFLEZ, flusseinzugsgebiet).eu("EU" + flusseinzugsgebiet);
    }

    public static ReferenzBuilder aIEAusnahmeReferenz(String schluessel) {
        return aReferenz(RDERO, schluessel);
    }

    public static ReferenzBuilder aSpezielleBedingungReferenz(String schluessel) {
        return aReferenz(RSPECC, schluessel);
    }

    public ReferenzBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public ReferenzBuilder referenznummer(String referenznummer) {
        this.referenznummer = referenznummer;
        return this;
    }

    public ReferenzBuilder genNewReferenznummer() {
        this.referenznummer = Referenz.generateNewReferenznummer();
        return this;
    }

    public ReferenzBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public ReferenzBuilder referenzliste(Referenzliste referenzliste) {
        this.referenzliste = referenzliste;
        return this;
    }

    public ReferenzBuilder schluessel(String schluessel) {
        this.schluessel = schluessel;
        return this;
    }

    public ReferenzBuilder sortier(String sortier) {
        this.sortier = sortier;
        return this;
    }

    public ReferenzBuilder ktext(String ktext) {
        this.ktext = ktext;
        return this;
    }

    public ReferenzBuilder ltext(String ltext) {
        this.ltext = ltext;
        return this;
    }

    public ReferenzBuilder gueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
        return this;
    }

    public ReferenzBuilder gueltigVon(int jahr) {
        return gueltigVon(Gueltigkeitsjahr.of(jahr));
    }

    public ReferenzBuilder gueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
        return this;
    }

    public ReferenzBuilder gueltigBis(int jahr) {
        return gueltigBis(Gueltigkeitsjahr.of(jahr));
    }

    public ReferenzBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public ReferenzBuilder eu(String eu) {
        this.eu = eu;
        return this;
    }

    public ReferenzBuilder strg(String strg) {
        this.strg = strg;
        return this;
    }

    public Referenz build() {
        Referenz referenz = new Referenz(referenzliste, land, schluessel, sortier, ktext, gueltigVon, gueltigBis);
        referenz.setId(id);
        referenz.setReferenznummer(referenznummer);
        referenz.setLtext(ltext);
        referenz.setLetzteAenderung(letzteAenderung);
        referenz.setEu(eu);
        referenz.setStrg(strg);
        return referenz;
    }
}
