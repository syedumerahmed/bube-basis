package de.wps.bube.basis.jobs;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@EnableGlobalMethodSecurity(securedEnabled = true)
@SpringBootTest(classes = { SpringAsyncConfig.class, PropertySourcesPlaceholderConfigurer.class }, properties = {
        "thread.pool.max-pool-size=1",
        "thread.pool.core-pool-size=1",
        "thread.pool.queue-capacity=0",
        "thread.pool.await-termination-seconds=0" })
class AsyncJobTest {

    interface SampleService {
        void record(Thread callingThread);

        void record(SecurityContext securityContext);
    }

    @Secured("ROLE_SECURED")
    @Service
    static class SecuredService {
        private final SampleService sampleService;

        SecuredService(SampleService service) {
            sampleService = service;
        }

        public void secured(SecurityContext securityContext) {
            sampleService.record(securityContext);
        }
    }

    @Component
    static class SampleJobExecutor {

        private final SampleService sampleService;
        private final SecuredService securedService;

        public SampleJobExecutor(SampleService sampleService, SecuredService securedService) {
            this.sampleService = sampleService;
            this.securedService = securedService;
        }

        @Async
        public void execute(long execTime) throws InterruptedException {
            Thread.sleep(execTime);
            sampleService.record(Thread.currentThread());
        }

        @Async
        public void executeSecured() {
            sampleService.record(Thread.currentThread());
            securedService.secured(SecurityContextHolder.getContext());
        }
    }

    @Autowired
    SampleJobExecutor sampleJobExecutor;

    @MockBean
    SampleService sampleService;

    @MockBean
    AsyncUncaughtExceptionHandler exceptionHandler;

    @Test
    void shouldRunAsynchronously() throws InterruptedException {
        final var threadCaptor = ArgumentCaptor.forClass(Thread.class);

        sampleJobExecutor.execute(0);

        verify(sampleService, timeout(100)).record(threadCaptor.capture());
        assertThat(threadCaptor.getValue()).isNotSameAs(Thread.currentThread());
    }

    @Test
    @WithMockUser(username = "admin", roles = "SECURED")
    void shouldBeAuthorized() {
        ArgumentCaptor<SecurityContext> contextArgumentCaptor = ArgumentCaptor.forClass(SecurityContext.class);

        sampleJobExecutor.executeSecured();

        verify(sampleService, timeout(100)).record(contextArgumentCaptor.capture());
        assertThat(contextArgumentCaptor.getValue()).isEqualTo(SecurityContextHolder.getContext());
    }

    @Test
    @WithMockUser
    void shouldNotBeAuthorized() {
        sampleJobExecutor.executeSecured();

        verify(exceptionHandler, timeout(100)).handleUncaughtException(any(AccessDeniedException.class), any(), any());
        verify(sampleService, never()).record(any(SecurityContext.class));
    }

    @Test
    @WithMockUser(username = "admin", roles = "SECURED")
    void shouldNotReuseContextInSameThreadOfPool() {
        final var threadCaptor = ArgumentCaptor.forClass(Thread.class);
        final var previousContext = SecurityContextHolder.getContext();

        sampleJobExecutor.executeSecured();
        verify(sampleService, timeout(200)).record(same(previousContext));

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        final var newContext = SecurityContextHolder.createEmptyContext();
        newContext.setAuthentication(previousContext.getAuthentication());
        SecurityContextHolder.setContext(newContext);

        sampleJobExecutor.executeSecured();
        verify(sampleService, timeout(200)).record(same(newContext));

        verify(sampleService, times(2)).record(threadCaptor.capture());
        assertThat(threadCaptor.getAllValues()).allMatch(threadCaptor.getValue()::equals);
    }
}
