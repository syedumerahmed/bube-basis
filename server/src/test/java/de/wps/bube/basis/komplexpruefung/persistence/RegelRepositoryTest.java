package de.wps.bube.basis.komplexpruefung.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelBuilder;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class RegelRepositoryTest {

    @Autowired
    private RegelRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void save() {

        var regel = RegelBuilder.aRegel().build();
        assertNull(regel.getNummer());

        var regelRead = repository.saveAndFlush(regel);

        assertNotNull(regelRead.getNummer());
        assertEquals(regel.getObjekte(), regelRead.getObjekte());
        assertEquals(regel.getTyp(), regelRead.getTyp());
        assertEquals(regel.getGruppe(), regelRead.getGruppe());
        assertEquals(regel.getLand(), regelRead.getLand());
        assertEquals(regel.getBeschreibung(), regelRead.getBeschreibung());
        assertEquals(regel.getGueltigVon(), regelRead.getGueltigVon());
        assertEquals(regel.getGueltigBis(), regelRead.getGueltigBis());
        assertEquals(regel.getCode(), regelRead.getCode());
    }

    @Test
    void save_aendereTyp() {

        var regel = RegelBuilder.aRegel()
                                .land(Land.DEUTSCHLAND)
                                .typ(RegelTyp.FEHLER)
                                .typBundeseinheitlich(false)
                                .build();

        regel.aendereTyp(Land.BAYERN, RegelTyp.WARNUNG);

        var regelRead = repository.saveAndFlush(regel);

        assertEquals(RegelTyp.WARNUNG, regelRead.getTyp(Land.BAYERN));
    }

    @Test
    void findByNummer() {
        var regel = RegelBuilder.aRegel().build();
        var regelRead = repository.saveAndFlush(regel);

        Long nummer = regelRead.getNummer();

        assertTrue(repository.findByNummer(nummer).isPresent());
    }

    @Test
    void streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit() {

        Regel gueltig = RegelBuilder.aRegel()
                                    .typ(RegelTyp.FEHLER)
                                    .land(Land.HAMBURG)
                                    .gruppe(RegelGruppe.GRUPPE_A)
                                    .gueltigVon(2007)
                                    .gueltigBis(2099)
                                    .build();
        Regel ignoriert = RegelBuilder.aRegel()
                                      .typ(RegelTyp.IGNORIERT)
                                      .land(Land.HAMBURG)
                                      .gruppe(RegelGruppe.GRUPPE_A)
                                      .gueltigVon(2007)
                                      .gueltigBis(2099)
                                      .build();
        Regel ausland = RegelBuilder.aRegel()
                                    .typ(RegelTyp.FEHLER)
                                    .land(Land.SAARLAND)
                                    .gruppe(RegelGruppe.GRUPPE_A)
                                    .gueltigVon(2007)
                                    .gueltigBis(2099)
                                    .build();
        Regel gruppeB = RegelBuilder.aRegel()
                                    .typ(RegelTyp.FEHLER)
                                    .land(Land.HAMBURG)
                                    .gruppe(RegelGruppe.GRUPPE_B)
                                    .gueltigVon(2007)
                                    .gueltigBis(2099)
                                    .build();
        Regel veraltet = RegelBuilder.aRegel()
                                     .typ(RegelTyp.FEHLER)
                                     .land(Land.HAMBURG)
                                     .gruppe(RegelGruppe.GRUPPE_A)
                                     .gueltigVon(2007)
                                     .gueltigBis(2009)
                                     .build();
        repository.saveAll(List.of(gueltig, ignoriert, ausland, gruppeB, veraltet));

        List<Regel> regeln = repository.streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(Land.HAMBURG,
                List.of(RegelGruppe.GRUPPE_A), Gueltigkeitsjahr.of(2021)).collect(Collectors.toList());

        assertThat(regeln).hasSize(1);
        assertThat(regeln.get(0)).isEqualTo(gueltig);

    }

    @Test
    void streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit_mitTypaenderungen() {

        Regel fehlerFuerHamburg = RegelBuilder.aRegel()
                                              .typ(RegelTyp.FEHLER)
                                              .land(Land.HAMBURG)
                                              .typBundeseinheitlich(false)
                                              .gruppe(RegelGruppe.GRUPPE_A)
                                              .gueltigVon(2007)
                                              .gueltigBis(2099)
                                              .build();
        Regel ignoriertFuerHamburg = RegelBuilder.aRegel()
                                                 .typ(RegelTyp.IGNORIERT)
                                                 .land(Land.HAMBURG)
                                                 .typBundeseinheitlich(false)
                                                 .gruppe(RegelGruppe.GRUPPE_A)
                                                 .gueltigVon(2007)
                                                 .gueltigBis(2099)
                                                 .build();
        Regel fehlerFuerDeutschland = RegelBuilder.aRegel()
                                                  .typ(RegelTyp.FEHLER)
                                                  .land(Land.DEUTSCHLAND)
                                                  .typBundeseinheitlich(false)
                                                  .gruppe(RegelGruppe.GRUPPE_A)
                                                  .gueltigVon(2007)
                                                  .gueltigBis(2099)
                                                  .build();
        Regel ignoriertFuerDeutschland = RegelBuilder.aRegel()
                                                     .typ(RegelTyp.IGNORIERT)
                                                     .land(Land.DEUTSCHLAND)
                                                     .typBundeseinheitlich(false)
                                                     .gruppe(RegelGruppe.GRUPPE_A)
                                                     .gueltigVon(2007)
                                                     .gueltigBis(2099)
                                                     .build();
        Regel fehlerMitAenderung = RegelBuilder.aRegel()
                                               .typ(RegelTyp.FEHLER)
                                               .land(Land.DEUTSCHLAND)
                                               .typBundeseinheitlich(false)
                                               .gruppe(RegelGruppe.GRUPPE_A)
                                               .gueltigVon(2007)
                                               .gueltigBis(2099)
                                               .build();
        Regel ignoriertMitAenderung = RegelBuilder.aRegel()
                                                  .typ(RegelTyp.IGNORIERT)
                                                  .land(Land.DEUTSCHLAND)
                                                  .typBundeseinheitlich(false)
                                                  .gruppe(RegelGruppe.GRUPPE_A)
                                                  .gueltigVon(2007)
                                                  .gueltigBis(2099)
                                                  .build();

        fehlerMitAenderung.aendereTyp(Land.HAMBURG, RegelTyp.IGNORIERT);
        ignoriertMitAenderung.aendereTyp(Land.HAMBURG, RegelTyp.FEHLER);

        repository.saveAll(
                List.of(fehlerFuerHamburg, ignoriertFuerHamburg, fehlerFuerDeutschland, ignoriertFuerDeutschland,
                        fehlerMitAenderung, ignoriertMitAenderung));

        List<Regel> regelnFuerHessen = repository.streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(Land.HESSEN,
                List.of(RegelGruppe.GRUPPE_A), Gueltigkeitsjahr.of(2021)).collect(Collectors.toList());
        assertThat(regelnFuerHessen).hasSize(2);
        assertThat(regelnFuerHessen).containsExactlyInAnyOrder(fehlerFuerDeutschland, fehlerMitAenderung);

        List<Regel> regelnFuerHamburg = repository.streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(Land.HAMBURG,
                List.of(RegelGruppe.GRUPPE_A), Gueltigkeitsjahr.of(2021)).collect(Collectors.toList());
        assertThat(regelnFuerHamburg).hasSize(3);
        assertThat(regelnFuerHamburg).containsExactlyInAnyOrder(fehlerFuerHamburg, fehlerFuerDeutschland,
                ignoriertMitAenderung);

    }

    @Test
    void streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit_mehrfacheTypaenderungen() {

        Regel ignoriertFuerDeutschland = RegelBuilder.aRegel()
                                                     .typ(RegelTyp.IGNORIERT)
                                                     .land(Land.DEUTSCHLAND)
                                                     .typBundeseinheitlich(false)
                                                     .gruppe(RegelGruppe.GRUPPE_A)
                                                     .gueltigVon(2007)
                                                     .gueltigBis(2099)
                                                     .build();

        ignoriertFuerDeutschland.aendereTyp(Land.HAMBURG, RegelTyp.FEHLER);
        ignoriertFuerDeutschland.aendereTyp(Land.BREMEN, RegelTyp.WARNUNG);

        repository.save(ignoriertFuerDeutschland);

        List<Regel> regelnFuerHamburg = repository.streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(Land.HAMBURG,
                List.of(RegelGruppe.GRUPPE_A), Gueltigkeitsjahr.of(2021)).collect(Collectors.toList());
        assertThat(regelnFuerHamburg).hasSize(1);
        assertThat(regelnFuerHamburg).containsExactly(ignoriertFuerDeutschland);

    }



}
