package de.wps.bube.basis.arch;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.plantuml.PlantUmlArchCondition.Configurations.consideringOnlyDependenciesInDiagram;
import static com.tngtech.archunit.library.plantuml.PlantUmlArchCondition.adhereToPlantUmlDiagram;

import java.net.URL;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import de.wps.bube.basis.BasisApplication;

/**
 * Diese Klasse testet die Abhängigkeiten der Packages der BUBE-Backend-Anwendung auf der Basis des
 * PlantUML-Diagramms DomainLayers.puml.
 */
@AnalyzeClasses(packages = DomainLayerTest.PACKAGE_BASE, importOptions = ImportOption.DoNotIncludeTests.class)
class PlantUMLDomainLayerTest {

    private static URL diagram = PlantUMLDomainLayerTest.class.getResource("/plantuml/DomainLayers.puml");


    @ArchTest
    public static final ArchRule domainLayers =
            classes().that().doNotBelongToAnyOf(BasisApplication.class)
            .should(adhereToPlantUmlDiagram(diagram, consideringOnlyDependenciesInDiagram()));

}
