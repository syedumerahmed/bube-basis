package de.wps.bube.basis.jobs.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

class DateTimeTest {

    @Test
    void test() {
        var date = ZonedDateTime.of(LocalDate.of(2021, 3, 22), LocalTime.of(9, 55, 02), ZoneId.of("Europe/Berlin"));

        assertThat(DateTime.format(date)).isEqualTo("2021-03-22T09:55:02");
    }

    @Test
    void nowCET() {
        assertThat(DateTime.nowCET()).isEqualTo(DateTime.format(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))));
    }

    @Test
    void atCET() {
        assertThat(DateTime.atCET(Instant.now())).isEqualTo(DateTime.format(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))));
    }
}