package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegAnl;
import static de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.euregistry.domain.vo.FeuerungsanlageTyp.WI;
import static de.wps.bube.basis.euregistry.domain.vo.GenehmigungBuilder.aFullGenehmigung;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

@ExtendWith({ SpringExtension.class })
@SpringBootTest(classes = { EuRegistryBerichtsjahrwechselMapperImpl.class, ReferenzResolverService.class })
class EuRegistryBerichtsjahrwechselMapperTest {

    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private BehoerdenService behoerdenService;

    @Autowired
    private EuRegistryBerichtsjahrwechselMapper mapper;

    @Test
    void copyEuRegBerichtsdaten() {
        var baRef = aReferenz(RRTYP, "ART01").land(HAMBURG).build();
        var bsRef = aReferenz(RBEA, "BS01").land(HAMBURG).build();
        var berichtsdaten = aFullBerichtsdaten()
                .berichtsart(baRef)
                .bearbeitungsstatus(bsRef)
                .build();
        var bst = aBetriebsstaette().land(HAMBURG).build();
        var zieljahr = aJahrReferenz(2020).build();
        mockRepositoryReferenz(baRef, zieljahr);
        mockRepositoryReferenz(bsRef, zieljahr);

        var copy = mapper.copyEuRegBerichtsdaten(berichtsdaten, zieljahr, bst).entity;

        assertThat(copy).isNotSameAs(berichtsdaten);
        assertThat(copy).usingRecursiveComparison().isEqualTo(berichtsdaten);
    }

    @Test
    void copyEuRegBerichtsdaten_preferLandReferenz() {
        var baRef = aReferenz(RRTYP, "ART01").land(HAMBURG).build();
        var bsRef = aReferenz(RBEA, "BS01").land(HAMBURG).build();
        var bsBundRef = aReferenz(RBEA, "BS01").land(DEUTSCHLAND).build();
        var berichtsdaten = aFullBerichtsdaten()
                .berichtsart(baRef)
                .bearbeitungsstatus(bsBundRef)
                .build();
        var bst = aBetriebsstaette().land(HAMBURG).build();
        var zieljahr = aJahrReferenz(2020).build();
        mockRepositoryReferenz(baRef, zieljahr);
        when(referenzenService.findReferenz(bsBundRef.getReferenzliste(), bsBundRef.getSchluessel(),
                Gueltigkeitsjahr.of(zieljahr.getSchluessel()), bst.getLand())).thenReturn(
                Optional.of(bsRef));

        var copy = mapper.copyEuRegBerichtsdaten(berichtsdaten, zieljahr, bst).entity;

        assertThat(copy.getBearbeitungsstatus()).isSameAs(bsRef);
    }

    @Test
    void copyEuRegAnlage() {
        when(referenzenService.findReferenz(any(Referenzliste.class), anyString(), any(Gueltigkeitsjahr.class),
                any(Land.class)))
                .thenAnswer(
                        i -> Optional.of(aReferenz(i.getArgument(0), i.getArgument(1)).land(i.getArgument(3)).build()));
        var bd = aFullBerichtsdaten().build();

        var feuerungsanlage = aFeuerungsanlageBerichtsdaten().feuerungsanlageTyp(WI).build();
        var genehmigung = aFullGenehmigung().build();

        var anwendbareBVT = aGemeindeReferenz("gem").build();
        var bvtAusnahme = new BVTAusnahme(anwendbareBVT, LocalDate.now(), "url");

        var or = aFullEURegAnl()
                .berichtsdaten(bd)
                .genehmigung(genehmigung)
                .anwendbareBvt(anwendbareBVT)
                .bvtAusnahmen(bvtAusnahme)
                .feuerungsanlage(feuerungsanlage)
                .visitUrl("visit")
                .eSpirsNr("eSpir")
                .tehgNr("teh")
                .monitor("mo")
                .monitorUrl("moURL")
                .build();

        var de = aFullEURegAnl()
                .bvtAusnahmen(new ArrayList<>())
                .anlage(or.getAnlage())
                .build();

        var bst = aBetriebsstaette().build();
        var zieljahr = aJahrReferenz(2020).build();

        mapper.uebernimmBerichtsdaten(or, de, zieljahr, bst);

        assertThat(de).isNotSameAs(or);
        assertThat(de).usingRecursiveComparison()
                      .ignoringFieldsMatchingRegexes(".*\\.referenznummer", ".*\\.land", ".*\\.eu")
                      .isEqualTo(or);
    }

    private void mockRepositoryReferenz(Referenz baRef, Referenz zieljahr) {
        when(referenzenService.findReferenz(baRef.getReferenzliste(), baRef.getSchluessel(),
                Gueltigkeitsjahr.of(zieljahr.getSchluessel()), baRef.getLand())).thenReturn(
                Optional.of(baRef));
    }
}
