package de.wps.bube.basis.jobs;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.jobs.enums.JobStatusEnum;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.jobs.persistence.JobStatusRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@ExtendWith(MockitoExtension.class)
class AktiverBenutzerJobRegistryTest {

    @Mock
    private JobStatusRepository jobStatusRepository;
    @Mock
    private AktiverBenutzer aktiverBenutzer;

    private BenutzerJobRegistry benutzerJobRegistry;

    @BeforeEach
    void setUp() {
        when(aktiverBenutzer.getUsername()).thenReturn("username");
        benutzerJobRegistry = new BenutzerJobRegistry(jobStatusRepository, aktiverBenutzer);
    }

    @Test
    void hasNoJob() {
        assertFalse(benutzerJobRegistry.hasRunningJob());
    }

    @Test
    void startJob() {
        // Arrage
        final JobStatus newStatus = AktiverBenutzerJobRegistryTest.getStatus();
        benutzerJobRegistry.jobStarted(newStatus);

        // Assert
        verify(jobStatusRepository).save(newStatus);
    }

    @Test
    void runJobCompleted() {
        // Arrage
        final JobStatus status = AktiverBenutzerJobRegistryTest.getStatus();
        when(jobStatusRepository.findById(status.getUsername())).thenReturn(Optional.of(status));

        // Act
        benutzerJobRegistry.jobCompleted(false);

        // Assert
        verify(jobStatusRepository).save(status);
        assertThat(status.isCompleted()).isTrue();
        assertThat(status.getError()).isNull();
    }

    @Test
    void runJobError() {
        // Arrage
        final JobStatus status = AktiverBenutzerJobRegistryTest.getStatus();
        when(jobStatusRepository.findById(status.getUsername())).thenReturn(Optional.of(status));

        // Act
        benutzerJobRegistry.jobError(new Exception());

        // Assert
        verify(jobStatusRepository).save(status);
        assertThat(status.isCompleted()).isFalse();
        assertThat(status.getError()).isNotNull();
    }

    private static JobStatus getStatus() {
        var status = new JobStatus(JobStatusEnum.BENUTZER_IMPORTIEREN);
        status.setUsername("username");
        return status;
    }
}
