package de.wps.bube.basis.komplexpruefung.domain.entity;

import java.time.Instant;
import java.util.List;

import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;

public final class RegelBuilder {
    private static final Instant NOW = Instant.now();

    private Long nummer;
    private Land land;
    private List<RegelObjekt> objekte;
    private RegelGruppe gruppe;
    private RegelTyp typ;
    private boolean typBundeseinheitlich;
    private String beschreibung;
    private String fehlertext;
    private Gueltigkeitsjahr gueltigVon;
    private Gueltigkeitsjahr gueltigBis;
    private Instant letzteAenderung;
    private String code;

    private RegelBuilder() {
    }

    public static RegelBuilder aRegel() {
        return new RegelBuilder().land(Land.HAMBURG)
                                 .objekte(List.of(RegelObjekt.BST, RegelObjekt.ANLAGE))
                                 .gruppe(RegelGruppe.GRUPPE_A)
                                 .typ(RegelTyp.FEHLER)
                                 .typBundeseinheitlich(false)
                                 .beschreibung("beschreibung")
                                 .fehlertext("fehlertext")
                                 .code("code")
                                 .gueltigVon(2007)
                                 .gueltigBis(2099)
                                 .letzteAenderung(NOW);
    }

    public RegelBuilder nummer(Long nummer) {
        this.nummer = nummer;
        return this;
    }

    public RegelBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public RegelBuilder gueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
        return this;
    }

    public RegelBuilder gueltigVon(int jahr) {
        return gueltigVon(Gueltigkeitsjahr.of(jahr));
    }

    public RegelBuilder gueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
        return this;
    }

    public RegelBuilder gueltigBis(int jahr) {
        return gueltigBis(Gueltigkeitsjahr.of(jahr));
    }

    public RegelBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public RegelBuilder objekte(List<RegelObjekt> objekte) {
        this.objekte = objekte;
        return this;
    }

    public RegelBuilder objekte(RegelObjekt... objekte) {
        return objekte(List.of(objekte));
    }

    public RegelBuilder gruppe(RegelGruppe gruppe) {
        this.gruppe = gruppe;
        return this;
    }

    public RegelBuilder typ(RegelTyp typ) {
        this.typ = typ;
        return this;
    }

    public RegelBuilder typBundeseinheitlich(boolean typBundeseinheitlich) {
        this.typBundeseinheitlich = typBundeseinheitlich;
        return this;
    }

    public RegelBuilder beschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
        return this;
    }

    public RegelBuilder code(String code) {
        this.code = code;
        return this;
    }

    public RegelBuilder fehlertext(String fehlertext) {
        this.fehlertext = fehlertext;
        return this;
    }

    public Regel build() {
        Regel regel = new Regel(land, objekte, gruppe, typ, typBundeseinheitlich, beschreibung, fehlertext, gueltigVon,
                gueltigBis, code);
        regel.setNummer(nummer);
        regel.setLetzteAenderung(letzteAenderung);
        return regel;
    }
}
