package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
public class QuellenRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private QuelleRepository repository;

    @Autowired
    private BetriebsstaetteRepository betriebsstaetteRepository;

    @Autowired
    private AnlageRepository anlageRepository;

    @Autowired
    private ReferenzRepository referenzRepository;

    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @Test
    public void testFindById() {

        final var quelle = getQuelleBuilder().build();
        final var quelleRead = repository.save(quelle);

        assertTrue(repository.findById(quelleRead.getId()).isPresent());

    }

    @Test
    public void testNotNull() {
        final var quelle = new Quelle("Neue Quelle", "0100");
        quelle.setQuelleNr(null);
        assertThrows(RuntimeException.class, () -> repository.saveAndFlush(quelle));
    }

    @Test
    public void testCreate() {

        final var quelle = getQuelleBuilder().build();

        assertNull(quelle.getId());
        final var quelleRead = repository.save(quelle);
        assertNotNull(quelleRead.getId());
        assertEquals(quelle.getQuelleNr(), quelleRead.getQuelleNr());
        assertEquals(quelle.getErsteErfassung(), quelleRead.getErsteErfassung());
        assertEquals(quelle.getLetzteAenderung(), quelleRead.getLetzteAenderung());
    }

    @Test
    public void testDelete() {

        final var quelle = getQuelleBuilder().build();
        final var quelleRead = repository.save(quelle);

        assertTrue(repository.findById(quelleRead.getId()).isPresent());
        repository.delete(quelleRead);
        assertFalse(repository.findById(quelleRead.getId()).isPresent());

    }

    @Test
    void testFindByParentBetriebsstaetteIdAndQuelleNr() {
        final Betriebsstaette betriebsstaette = storeBetriebsstaette();
        storeQuelle(1L, betriebsstaette.getId());
        final var quelle2 = storeQuelle(2L, betriebsstaette.getId());

        assertThat(
                repository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(betriebsstaette.getId(), "2")).hasValue(
                quelle2);
    }

    @Test
    void testFindByParentBetriebsstaetteIdAndQuelleNr_andererParent() {
        final Betriebsstaette betriebsstaette = storeBetriebsstaette(1L);
        storeQuelle(1L, betriebsstaette.getId());
        storeQuelle(2L, betriebsstaette.getId());

        assertThat(repository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(2L, "1")).isEmpty();
    }

    @Test
    void testFindByParentBetriebsstaetteIdAndQuelleNr_selbeNummer_andererParent() {
        final var b1 = storeBetriebsstaette();
        final var b2 = storeBetriebsstaette();
        storeQuelle(1L, "5", b2.getId());
        storeQuelle(2L, "5", b1.getId());

        final Optional<Quelle> actualQuelle1 = repository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(b1.getId(),
                "5");
        final Optional<Quelle> actualQuelle2 = repository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(b2.getId(),
                "5");

        assertThat(actualQuelle1).isPresent();
        assertThat(actualQuelle2).isPresent();
        assertNotEquals(actualQuelle1, actualQuelle2);
    }

    @Test
    void testFindByParentBetriebsstaetteIdAndQuelleNr_notFound() {
        var b = storeBetriebsstaette();
        storeQuelle(1L, "44", b.getId());

        final var quelleOptional = repository.findByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(b.getId() + 1, "44");

        assertThat(quelleOptional).isEmpty();
    }

    private Quelle storeQuelle(final Long id, final Long parentBetriebsstaette) {
        return storeQuelle(id, String.valueOf(id), parentBetriebsstaette);
    }

    private Quelle storeQuelle(final Long id, final String quelleNr, final Long parentBetriebsstaette) {
        return repository.save(QuelleBuilder.aQuelle()
                                            .id(id)
                                            .quelleNr(quelleNr)
                                            .parentBetriebsstaetteId(parentBetriebsstaette)
                                            .build());
    }

    private Betriebsstaette storeBetriebsstaette() {
        return storeBetriebsstaette(null);
    }

    private Betriebsstaette storeBetriebsstaette(final Long id) {
        final var behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        final var jahr = referenzRepository.save(aJahrReferenz(2020).build());
        final var gemeinde = referenzRepository.save(aGemeindeReferenz("G1").build());
        final var betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        return betriebsstaetteRepository.save(BetriebsstaetteBuilder.aBetriebsstaette(id)
                                                                    .zustaendigeBehoerde(behoerde)
                                                                    .betriebsstatus(betriebsstatus)
                                                                    .berichtsjahr(jahr)
                                                                    .gemeindekennziffer(gemeinde)
                                                                    .build());
    }

    @Test
    void existsByParentBetriebsstaetteIdAndQuelleNr() {
        BetriebsstaetteBuilder bstBuilder
                = BetriebsstaetteBuilder.aBetriebsstaette()
                                        .zustaendigeBehoerde(behoerdenRepository.save(aBehoerde("BEHD1").build()))
                                        .betriebsstatus(referenzRepository.save(aBetriebsstatusReferenz("Neu").build()))
                                        .berichtsjahr(referenzRepository.save(aJahrReferenz(2020).build()))
                                        .gemeindekennziffer(referenzRepository.save(aGemeindeReferenz("G1").build()));

        Betriebsstaette bst1 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST1").build());
        Betriebsstaette bst2 = betriebsstaetteRepository.save(bstBuilder.betriebsstaetteNr("BST2").build());
        repository.save(aQuelle().parentBetriebsstaetteId(bst1.getId()).quelleNr("abc").build());

        assertThat(repository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(bst1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(bst1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(bst2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentBetriebsstaetteIdAndQuelleNrIgnoreCase(bst1.getId(), "ABC")).isTrue();
    }

    @Test
    void existsByParentAnlageIdAndQuelleNr() {
        var anlage1 = anlageRepository.save(anAnlage().build());
        var anlage2 = anlageRepository.save(anAnlage().build());
        repository.save(aQuelle().parentAnlageId(anlage1.getId()).quelleNr("abc").build());

        assertThat(repository.existsByParentAnlageIdAndQuelleNrIgnoreCase(anlage1.getId(), "abc")).isTrue();
        assertThat(repository.existsByParentAnlageIdAndQuelleNrIgnoreCase(anlage1.getId(), "xyz")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndQuelleNrIgnoreCase(anlage2.getId(), "abc")).isFalse();
        assertThat(repository.existsByParentAnlageIdAndQuelleNrIgnoreCase(anlage1.getId(), "ABC")).isTrue();
    }

    @Test
    void aenderungDurchBetreiber() {
        var quelle = entityManager.persistFlushFind(getQuelleBuilder()
                .betreiberdatenUebernommen(true)
                .letzteAenderungBetreiber(null)
                .build());
        var modifiedDateSpl = quelle.getLetzteAenderung();

        repository.aenderungDurchBetreiber(quelle.getId());
        entityManager.clear();
        var updatedQuelle = repository.getById(quelle.getId());

        assertThat(updatedQuelle.getLetzteAenderung()).isCloseTo(modifiedDateSpl, within(1, MILLIS));
        assertThat(updatedQuelle.isBetreiberdatenUebernommen()).isFalse();
        assertThat(updatedQuelle.getLetzteAenderungBetreiber()).isAfter(quelle.getErsteErfassung());
    }

    private static QuelleBuilder getQuelleBuilder() {
        return QuelleBuilder.aQuelle().name("Name").quelleNr("0112");
    }

}
