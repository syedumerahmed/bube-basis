package de.wps.bube.basis.test;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.StringJoiner;

import org.springframework.security.access.hierarchicalroles.RoleHierarchyAuthoritiesMapper;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;
import org.springframework.util.Assert;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Rollen;

/**
 * Erzeugt eine Rollenhierarchie, wie sie im Keycloak hinterlegt ist. Eine
 * roleChain ist zu lesen als "linkes Recht" inkludiert Recht rechts davon.
 * <p>
 * Details s. {@link org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl}
 */
public class WithBubeMockUserFactory implements WithSecurityContextFactory<WithBubeMockUser> {

    private static final GrantedAuthoritiesMapper authoritiesMapper;

    static {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

        StringJoiner roleHierarchies = new StringJoiner("\n");
        roleHierarchies.add(
                createRoleChain(BEHOERDE_DELETE, BEHOERDE_WRITE, BEHOERDE_READ, REFERENZLISTEN_USE, BEHOERDENBENUTZER));
        roleHierarchies.add(createRoleChain(LAND_DELETE, LAND_WRITE, LAND_READ, REFERENZLISTEN_USE, BEHOERDENBENUTZER));

        roleHierarchies.add(createRoleChain(ADMIN, BEHOERDE_DELETE));
        roleHierarchies.add(createRoleChain(ADMIN, BENUTZERVERWALTUNG_DELETE));
        roleHierarchies.add(createRoleChain(ADMIN, LAND_DELETE));
        roleHierarchies.add(createRoleChain(ADMIN, REFERENZLISTEN_LAND_DELETE));

        roleHierarchies.add(createRoleChain(GESAMTADMIN, ADMIN));
        roleHierarchies.add(createRoleChain(GESAMTADMIN, REFERENZLISTEN_BUND_DELETE));

        roleHierarchies.add(
                createRoleChain(REFERENZLISTEN_BUND_DELETE, REFERENZLISTEN_BUND_WRITE, REFERENZLISTEN_BUND_READ,
                        REFERENZLISTEN_USE));
        roleHierarchies.add(
                createRoleChain(REFERENZLISTEN_LAND_DELETE, REFERENZLISTEN_LAND_WRITE, REFERENZLISTEN_LAND_READ,
                        REFERENZLISTEN_USE));

        roleHierarchies.add(
                createRoleChain(BENUTZERVERWALTUNG_DELETE, BENUTZERVERWALTUNG_WRITE, BENUTZERVERWALTUNG_READ));

        roleHierarchies.add(createRoleChain(BETREIBER, BENUTZERVERWALTUNG_DELETE));
        roleHierarchies.add(createRoleChain(BETREIBER, REFERENZLISTEN_USE));

        roleHierarchy.setHierarchy(roleHierarchies.toString());

        authoritiesMapper = new RoleHierarchyAuthoritiesMapper(roleHierarchy);
    }

    private static CharSequence createRoleChain(Rolle... rollen) {
        Assert.isTrue(rollen != null && rollen.length >= 2, "RoleChain must have at least two roles");
        return Arrays.stream(rollen).map(Rolle::getAuthority).collect(joining(" > "));
    }

    private static Datenberechtigung getDatenberechtigung(WithBubeMockUser annotation) {
        return Datenberechtigung.DatenberechtigungBuilder
                .land(annotation.land())
                .behoerden(annotation.behoerden())
                .akz(annotation.akz())
                .verwaltungsgebiete(annotation.verwaltungsgebiete())
                .betriebsstaetten(annotation.betriebsstaetten())
                .fachmodule(Set.of(annotation.fachmodule()))
                .themen(Set.of(annotation.themen()))
                .build();
    }

    private static Set<Rolle> getRollen(Collection<? extends GrantedAuthority> authorities) {
        return authorities.stream()
                          .map(GrantedAuthority::getAuthority)
                          .map(authority -> authority.substring(Rollen.ROLE_PREFIX.length()))
                          .map(Rolle::of)
                          .collect(toSet());
    }

    @Override
    public SecurityContext createSecurityContext(WithBubeMockUser annotation) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        var authorities = authoritiesMapper.mapAuthorities(Set.of(annotation.roles()));
        var principal = new AktiverBenutzer(annotation.name(), getRollen(authorities), getDatenberechtigung(annotation));
        Authentication auth = new UsernamePasswordAuthenticationToken(principal, null, authorities);

        context.setAuthentication(auth);
        return context;
    }
}
