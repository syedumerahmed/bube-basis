package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.anEpsgReferenz;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.NumberVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbQuelleVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

class QuelleVergleichServiceTest {
    private QuelleVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        vergleichService = new QuelleVergleichService();
    }

    @Test
    void vergleicheGleich() {
        Quelle quelle = QuelleBuilder.aFullQuelle().build();
        SpbQuelle spbQuelle = new SpbQuelle(quelle);
        spbQuelle.setId(10L);

        SpbQuelleVergleich vergleich = vergleichService.vergleiche(quelle, spbQuelle);

        assertThat(vergleich.getSpbId()).isEqualTo(spbQuelle.getId());
        assertThat(vergleich.getId()).isEqualTo(quelle.getId());

        StringVector quelleNr = vergleich.getQuelleNr();
        assertThat(quelleNr.getSpb()).isEqualTo(vergleich.getQuelleNr().getSpl());

        StringVector name = vergleich.getName();
        assertThat(name.getSpb()).isEqualTo(vergleich.getName().getSpl());

        ReferenzVector quelleArt = vergleich.getQuelleArt();
        assertThat(quelleArt.getSpb().getSchluessel()).isEqualTo(quelleArt.getSpl().getSchluessel());

        NumberVector bauhoehe = vergleich.getBauhoehe();
        assertThat(bauhoehe.getSpb()).isEqualTo(vergleich.getBauhoehe().getSpl());

        NumberVector breite = vergleich.getBreite();
        assertThat(breite.getSpb()).isEqualTo(vergleich.getBreite().getSpl());

        NumberVector laenge = vergleich.getLaenge();
        assertThat(laenge.getSpb()).isEqualTo(vergleich.getLaenge().getSpl());

        NumberVector durchmesser = vergleich.getDurchmesser();
        assertThat(durchmesser.getSpb()).isEqualTo(vergleich.getDurchmesser().getSpl());

        NumberVector flaeche = vergleich.getFlaeche();
        assertThat(flaeche.getSpb()).isEqualTo(vergleich.getFlaeche().getSpl());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpb().getEpsgCode().getSchluessel()).isEqualTo(
                vergleich.getGeoPunkt().getSpl().getEpsgCode().getSchluessel());

        assertThat(name.isEqual()).isTrue();
        assertThat(quelleArt.isEqual()).isTrue();
        assertThat(durchmesser.isEqual()).isTrue();
        assertThat(bauhoehe.isEqual()).isTrue();
        assertThat(flaeche.isEqual()).isTrue();
        assertThat(breite.isEqual()).isTrue();
        assertThat(quelleNr.isEqual()).isTrue();
        assertThat(laenge.isEqual()).isTrue();
        assertThat(geoPunkt.isEqual()).isTrue();

        assertThat(vergleich.isHasChanges()).isFalse();

    }

    @Test
    void vergleicheUnterschiedlich() {
        Quelle quelle = QuelleBuilder.aFullQuelle().build();
        Quelle quelleAnders = QuelleBuilder.aQuelle().id(33L)
                                           .parentAnlageId(222L)
                                           .parentBetriebsstaetteId(111L)
                                           .bemerkung("bemerkung2")
                                           .quelleNr("nummer2")
                                           .betreiberdatenUebernommen(false)
                                           .breite(12.12)
                                           .bauhoehe(22.22)
                                           .durchmesser(23.32)
                                           .flaeche(42.42)
                                           .laenge(52.52)
                                           .geoPunkt(new GeoPunkt(21, 22, anEpsgReferenz("2epsg").build()))
                                           .referenzAnlagenkataster("kataster2")
                                           .name("quelle2")
                                           .quellenArt(ReferenzBuilder.aReferenz(Referenzliste.RQUEA, "quelle2")
                                                                      .id(15987L)
                                                                      .build())
                                           .build();

        SpbQuelle spbQuelle = new SpbQuelle(quelleAnders);
        spbQuelle.setId(10L);

        SpbQuelleVergleich vergleich = vergleichService.vergleiche(quelle, spbQuelle);

        assertThat(vergleich.getSpbId()).isEqualTo(spbQuelle.getId());
        assertThat(vergleich.getId()).isEqualTo(quelle.getId());

        StringVector quelleNr = vergleich.getQuelleNr();
        assertThat(quelleNr.getSpb()).isNotEqualTo(vergleich.getQuelleNr().getSpl());

        StringVector name = vergleich.getName();
        assertThat(name.getSpb()).isNotEqualTo(vergleich.getName().getSpl());

        ReferenzVector quelleArt = vergleich.getQuelleArt();
        assertThat(quelleArt.getSpb().getSchluessel()).isNotEqualTo(quelleArt.getSpl().getSchluessel());

        NumberVector bauhoehe = vergleich.getBauhoehe();
        assertThat(bauhoehe.getSpb()).isNotEqualTo(vergleich.getBauhoehe().getSpl().longValue());

        NumberVector breite = vergleich.getBreite();
        assertThat(breite.getSpb()).isNotEqualTo(vergleich.getBreite().getSpl().longValue());

        NumberVector laenge = vergleich.getLaenge();
        assertThat(laenge.getSpb()).isNotEqualTo(vergleich.getLaenge().getSpl().longValue());

        NumberVector durchmesser = vergleich.getDurchmesser();
        assertThat(durchmesser.getSpb()).isNotEqualTo(vergleich.getDurchmesser().getSpl().longValue());

        NumberVector flaeche = vergleich.getFlaeche();
        assertThat(flaeche.getSpb()).isNotEqualTo(vergleich.getFlaeche().getSpl().longValue());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpb().getEpsgCode().getSchluessel()).isNotEqualTo(
                vergleich.getGeoPunkt().getSpl().getEpsgCode().getSchluessel());

        assertThat(name.isEqual()).isFalse();
        assertThat(quelleArt.isEqual()).isFalse();
        assertThat(durchmesser.isEqual()).isFalse();
        assertThat(bauhoehe.isEqual()).isFalse();
        assertThat(flaeche.isEqual()).isFalse();
        assertThat(breite.isEqual()).isFalse();
        assertThat(quelleNr.isEqual()).isFalse();
        assertThat(laenge.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }

    @Test
    void vergleicheNurSPL() {
        Quelle quelle = QuelleBuilder.aFullQuelle().build();

        SpbQuelleVergleich vergleich = vergleichService.vergleiche(quelle, null);

        assertThat(vergleich.getSpbId()).isNull();
        assertThat(vergleich.getId()).isEqualTo(quelle.getId());

        StringVector quelleNr = vergleich.getQuelleNr();
        assertThat(quelleNr.getSpb()).isNull();
        assertThat(quelleNr.getSpl()).isEqualTo(quelle.getQuelleNr());

        StringVector name = vergleich.getName();
        assertThat(name.getSpb()).isNull();
        assertThat(name.getSpl()).isEqualTo(quelle.getName());

        ReferenzVector quelleArt = vergleich.getQuelleArt();
        assertThat(quelleArt.getSpb()).isNull();
        assertThat(quelleArt.getSpl().getSchluessel()).isEqualTo(quelle.getQuellenArt().getSchluessel());

        NumberVector bauhoehe = vergleich.getBauhoehe();
        assertThat(bauhoehe.getSpb()).isNull();
        assertThat(bauhoehe.getSpl()).isEqualTo(quelle.getBauhoehe());

        NumberVector breite = vergleich.getBreite();
        assertThat(breite.getSpb()).isNull();
        assertThat(breite.getSpl()).isEqualTo(quelle.getBreite());

        NumberVector laenge = vergleich.getLaenge();
        assertThat(laenge.getSpb()).isNull();
        assertThat(laenge.getSpl()).isEqualTo(quelle.getLaenge());

        NumberVector durchmesser = vergleich.getDurchmesser();
        assertThat(durchmesser.getSpb()).isNull();
        assertThat(durchmesser.getSpl()).isEqualTo(quelle.getDurchmesser());

        NumberVector flaeche = vergleich.getFlaeche();
        assertThat(flaeche.getSpb()).isNull();
        assertThat(flaeche.getSpl()).isEqualTo(quelle.getFlaeche());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpb()).isNull();
        assertThat(geoPunkt.getSpl().getEpsgCode().getSchluessel()).isEqualTo(
                quelle.getGeoPunkt().getEpsgCode().getSchluessel());

        assertThat(name.isEqual()).isFalse();
        assertThat(quelleArt.isEqual()).isFalse();
        assertThat(durchmesser.isEqual()).isFalse();
        assertThat(bauhoehe.isEqual()).isFalse();
        assertThat(flaeche.isEqual()).isFalse();
        assertThat(breite.isEqual()).isFalse();
        assertThat(quelleNr.isEqual()).isFalse();
        assertThat(laenge.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheNurSPB() {
        SpbQuelle spbQuelle = new SpbQuelle(QuelleBuilder.aFullQuelle().build());
        spbQuelle.setQuelleId(null);
        spbQuelle.setId(10L);

        SpbQuelleVergleich vergleich = vergleichService.vergleiche(null, spbQuelle);

        assertThat(vergleich.getId()).isNull();
        assertThat(vergleich.getSpbId()).isEqualTo(spbQuelle.getId());

        StringVector quelleNr = vergleich.getQuelleNr();
        assertThat(quelleNr.getSpl()).isNull();
        assertThat(quelleNr.getSpb()).isEqualTo(spbQuelle.getQuelleNr());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNull();
        assertThat(name.getSpb()).isEqualTo(spbQuelle.getName());

        ReferenzVector quelleArt = vergleich.getQuelleArt();
        assertThat(quelleArt.getSpl()).isNull();
        assertThat(quelleArt.getSpb().getSchluessel()).isEqualTo(spbQuelle.getQuellenArt().getSchluessel());

        NumberVector bauhoehe = vergleich.getBauhoehe();
        assertThat(bauhoehe.getSpl()).isNull();
        assertThat(bauhoehe.getSpb()).isEqualTo(spbQuelle.getBauhoehe());

        NumberVector breite = vergleich.getBreite();
        assertThat(breite.getSpl()).isNull();
        assertThat(breite.getSpb()).isEqualTo(spbQuelle.getBreite());

        NumberVector laenge = vergleich.getLaenge();
        assertThat(laenge.getSpl()).isNull();
        assertThat(laenge.getSpb()).isEqualTo(spbQuelle.getLaenge());

        NumberVector durchmesser = vergleich.getDurchmesser();
        assertThat(durchmesser.getSpl()).isNull();
        assertThat(durchmesser.getSpb()).isEqualTo(spbQuelle.getDurchmesser());

        NumberVector flaeche = vergleich.getFlaeche();
        assertThat(flaeche.getSpl()).isNull();
        assertThat(flaeche.getSpb()).isEqualTo(spbQuelle.getFlaeche());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isNull();
        assertThat(geoPunkt.getSpb().getEpsgCode().getSchluessel()).isEqualTo(
                spbQuelle.getGeoPunkt().getEpsgCode().getSchluessel());

        assertThat(name.isEqual()).isFalse();
        assertThat(quelleArt.isEqual()).isFalse();
        assertThat(durchmesser.isEqual()).isFalse();
        assertThat(bauhoehe.isEqual()).isFalse();
        assertThat(flaeche.isEqual()).isFalse();
        assertThat(breite.isEqual()).isFalse();
        assertThat(quelleNr.isEqual()).isFalse();
        assertThat(laenge.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }
}
