package de.wps.bube.basis.stammdaten.security;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.HESSEN;
import static de.wps.bube.basis.base.vo.Land.SAARLAND;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = BetriebsstaettenBerechtigungsAccessor.class)
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.stammdaten.persistence",
        "de.wps.bube.basis.referenzdaten.persistence"
}, repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.stammdaten.domain",
        "de.wps.bube.basis.komplexpruefung.domain",
        "de.wps.bube.basis.referenzdaten.domain"
})
public class BetriebsstaettenBerechtigungsAccessorTest extends BubeIntegrationTest {

    @Autowired
    private BetriebsstaetteRepository repository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, betriebsstatus;

    @Autowired
    BetriebsstaettenBerechtigungsAccessor accessor;
    @Autowired
    StammdatenBerechtigungsContext stammdatenBerechtigungsContext;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
    }

    @Test
    void betriebsstaettenIn() {
        assertThat(accessor.betriebsstaettenIn(Set.of())).isNull();
        assertThat(accessor.betriebsstaettenIn(Set.of("abc"))).hasToString(
                "upper(betriebsstaette.betriebsstaetteNr) = ABC");
    }

    @Nested
    @WithBubeMockUser(roles = GESAMTADMIN,
            land = DEUTSCHLAND,
            behoerden = { "BEHD1", "BEHD2" },
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = { "LLBKK1GM", "LLBKK2GM", "HHA" })
    class AsGesamtadminDeutschland {

        @Test
        void shouldHaveReadAccessToBetriebsstaetteOfLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldHaveWriteAccessToBetriebsstaetteOfLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldHaveDeleteAccessToBetriebsstaetteOfLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("1").land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isTrue();
        }
    }

    @Nested
    @WithBubeMockUser(roles = LAND_READ,
            land = SAARLAND,
            behoerden = { "BEHD1", "BEHD2" },
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = { "LLBKK1GM", "LLBKK2GM", "HHA" })
    class AsLandReadUserBundesland {
        @Test
        void shouldHaveAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveWriteAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isFalse();
        }
    }

    @Nested
    @WithBubeMockUser(roles = LAND_WRITE,
            land = SAARLAND,
            behoerden = { "BEHD1", "BEHD2" },
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = { "LLBKK1GM", "LLBKK2GM", "HHA" })
    class AsLandWriteUserBundesland {
        @Test
        void shouldHaveAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldHaveReadAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveDeleteAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isFalse();
        }
    }

    @Nested
    @WithBubeMockUser(roles = LAND_DELETE,
            land = SAARLAND,
            behoerden = { "BEHD1", "BEHD2" },
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = { "LLBKK1GM", "LLBKK2GM", "HHA" })
    class AsLandDeleteUserBundesland {
        @Test
        void shouldHaveAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(HESSEN).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldHaveWriteAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldHaveReadAccessToBetriebsstaetteOfOwnLand() {
            repository.save(getDefaultBetriebsstaetteBuilder().land(SAARLAND).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }
    }

    @Nested
    @WithBubeMockUser(roles = BEHOERDE_READ,
            land = HAMBURG,
            behoerden = "BEHD1",
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = "HHN")
    class AsBehoerdeUserBundesland {
        BetriebsstaetteBuilder matchingBetriebsstaetteBuilder;

        @BeforeEach
        void setUp() {
            matchingBetriebsstaetteBuilder = getMatchingBetriebsstaetteBuilder();
        }

        @Test
        void shouldHaveAccessToBetriebsstaetteMatchingAllPredicates() {
            repository.save(matchingBetriebsstaetteBuilder.build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherLand() {
            repository.save(matchingBetriebsstaetteBuilder.land(Land.SACHSEN_ANHALT).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherBehoerde() {
            var behoerde = behoerdenRepository.save(aBehoerde("fremd").build());
            repository.save(matchingBetriebsstaetteBuilder.zustaendigeBehoerde(behoerde).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherAkz() {
            repository.save(matchingBetriebsstaetteBuilder.akz("AKZ3").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveAccessToBetriebsstaetteOfOtherGemeinde() {
            var gemeindekennziffer = referenzRepository.save(aGemeindeReferenz("LLBKKGEM").build());
            repository.save(matchingBetriebsstaetteBuilder.gemeindekennziffer(gemeindekennziffer).build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveAccessToOtherBetriebsstaette() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isFalse();
        }
    }

    @Nested
    @WithBubeMockUser(roles = { LAND_READ, BEHOERDE_WRITE },
            land = HAMBURG,
            behoerden = "BEHD1",
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = "HHN")
    class AsLandReadBehoerdeWriteUser {
        BetriebsstaetteBuilder matchingBetriebsstaetteBuilder;

        @BeforeEach
        void setUp() {
            matchingBetriebsstaetteBuilder = getMatchingBetriebsstaetteBuilder();
        }

        @Test
        void shouldHaveReadAccessToNotMatchingDaten() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateRead(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldHaveWriteAccessToBetriebsstaetteMatchingAllPredicates() {
            repository.save(matchingBetriebsstaetteBuilder.build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveWriteAccessToNotMatchingDaten() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isFalse();
        }

        @Test
        void shouldNotHaveDeleteAccessToNotMatchingDaten() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isFalse();
        }
    }

    @Nested
    @WithBubeMockUser(roles = { LAND_WRITE, BEHOERDE_DELETE },
            land = HAMBURG,
            behoerden = "BEHD1",
            akz = { "AKZ1", "AKZ2" },
            betriebsstaetten = { "BETRIEB1", "BETRIEB2" },
            verwaltungsgebiete = "HHN")
    class AsLandWriteBehoerdeDeleteUser {
        BetriebsstaetteBuilder matchingBetriebsstaetteBuilder;

        @BeforeEach
        void setUp() {
            matchingBetriebsstaetteBuilder = getMatchingBetriebsstaetteBuilder();
        }

        @Test
        void shouldHaveWriteAccessToNotMatchingDaten() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateWrite(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldHaveDeleteAccessToBetriebsstaetteMatchingAllPredicates() {
            repository.save(matchingBetriebsstaetteBuilder.build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isTrue();
        }

        @Test
        void shouldNotHaveDeleteAccessToNotMatchingDaten() {
            repository.save(matchingBetriebsstaetteBuilder.betriebsstaetteNr("BETRIEB5").build());

            var exists = repository.exists(stammdatenBerechtigungsContext.createPredicateDelete(accessor));

            assertThat(exists).isFalse();
        }
    }

    private BetriebsstaetteBuilder getMatchingBetriebsstaetteBuilder() {
        return getDefaultBetriebsstaetteBuilder()
                .land(HAMBURG)
                .zustaendigeBehoerde(behoerde)
                .betriebsstatus(betriebsstatus)
                .akz("AKZ2")
                .gemeindekennziffer(gemeindeKennziffer)
                .betriebsstaetteNr("BETRIEB2");
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .zustaendigeBehoerde(behoerde)
                                 .betriebsstatus(betriebsstatus)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }

}
