package de.wps.bube.basis.euregistry.xml.bericht;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;

@SpringBootTest(classes = { ProductionInstallationPartMapperImpl.class, CommonEURegMapperImpl.class })
class ProductionInstallationPartMapperTest {

    @MockBean
    private KoordinatenTransformationService koordinatenTransformationService;

    @Autowired
    private ProductionInstallationPartMapper mapper;

    @BeforeEach
    void setUp() {
        when(koordinatenTransformationService.transformToEPSG4258(any())).thenReturn(new double[] {0.0, 0.0});
    }

    @Test
    void toXDto() {
        var euRegAnlage = EURegAnlageBuilder.aFullEURegF().build();

        var ieAusnahmen = List.of(ReferenzBuilder.aIEAusnahmeReferenz("IE1").build(),
                ReferenzBuilder.aIEAusnahmeReferenz("IE2").build());
        euRegAnlage.getFeuerungsanlage().setIeAusnahmen(ieAusnahmen);

        var spezielleBedingungen = List.of(
                new SpezielleBedingungIE51(ReferenzBuilder.aSpezielleBedingungReferenz("SB1").build(), "info",
                        "genehmigungUrl"));
        euRegAnlage.getFeuerungsanlage().setSpezielleBedingungen(spezielleBedingungen);

        var inspireParam = new InspireParam("insp_ns", "insp_site", "insp_extension");
        var productionSite = new ProductionSite();
        productionSite.setGmlId("siteGmlId");
        var context = mock(ProductionInstallationMapperContext.class);
        when(context.getProductionSite()).thenReturn(productionSite);
        when(context.getInspireParam()).thenReturn(inspireParam);
        var xdto = mapper.toXDto(euRegAnlage, context);

        assertThat(xdto.getInspireID().getIdentifier().getLocalId()).isEqualTo(
                euRegAnlage.getLocalId() + inspireParam.getExtension());
        assertThat(xdto.getInspireID().getIdentifier().getNamespace()).isEqualTo(inspireParam.getNamespace());

        assertThat(xdto.getThematicID().getIdentifier()).isEqualTo(euRegAnlage.getBerichtsdaten().getThematicId());
        assertThat(xdto.getThematicID().getIdentifierScheme()).isEqualTo(
                euRegAnlage.getBerichtsdaten().getSchema());

        assertThat(xdto.getGeometry().getPos()).isEqualTo("0.0 0.0");
        assertThat(xdto.getGeometry().getSrsDimension()).isEqualTo(2);

        assertThat(xdto.getStatus().getStatusType().getCode()).isEqualTo("EUbetrieb");
        assertThat(xdto.getStatus().getStatusType().getSchema()).isEqualTo(SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES);
        assertThat(xdto.getStatus().getValidFrom().toString()).isEqualTo("2019-10-12");
        assertThat(xdto.getStatus().getDescription()).isNull();
        assertThat(xdto.getStatus().getValidTo()).isNull();

        assertThat(xdto.getInstallationPartName().getNameOfFeature()).isEqualTo(euRegAnlage.getName());
        assertThat(xdto.getInstallationPartName().getConfidentialityReason().getCode()).isEqualTo(
                euRegAnlage.getVertraulichkeitsgrund().getEu());
        assertThat(xdto.getInstallationPartName().getConfidentialityReason().getSchema()).isEqualTo(
                SchemaSet.REASON_VALUE_CODES);

        assertThat(xdto.getPlantType().getCode()).isEqualTo("LCP");
        assertThat(xdto.getPlantType().getSchema()).isEqualTo(SchemaSet.PLANT_TYPE_VALUE_CODE);

        assertThat(xdto.getTotalRatedThermalInput()).isEqualTo(0);
        assertThat(xdto.getDerogations()).isEqualTo(List.of("IE1EU", "IE2EU"));
        assertThat(xdto.getRemarks()).isEqualTo(euRegAnlage.getBerichtsdaten().getBemerkung());

        assertThat(xdto.getNominalCapacity().getTotalNormalCapacity()).isEqualTo(6.0);
        assertThat(xdto.getNominalCapacity().getPermittedCapacityHazardous()).isEqualTo(2.0);
        assertThat(xdto.getNominalCapacity().getPermittedCapacityNonHazardous()).isEqualTo(4.0);

        assertThat(xdto.getDateOfStartOfOperation()).isEqualTo("2017-01-01");

        assertThat(xdto.getSpecificConditions().getSpecificConditions()).isEqualTo("SB1EU");
        assertThat(xdto.getSpecificConditions().getConditionsInformation()).isEqualTo("info");
        assertThat(xdto.getSpecificConditions().getSpecificConditionsPermitURL()).isEqualTo("genehmigungUrl");

        assertThat(xdto.isHeatReleaseHazardousWaste()).isEqualTo(
                euRegAnlage.getFeuerungsanlage().isMehrWaermeVonGefAbfall());
        assertThat(xdto.isUntreatedMunicipalWaste()).isEqualTo(
                euRegAnlage.getFeuerungsanlage().isMitverbrennungVonUnbehandeltemAbfall());

        assertThat(xdto.getPublicDisclosure()).isEqualTo(
                euRegAnlage.getFeuerungsanlage().getOeffentlicheBekanntmachung());
        assertThat(xdto.getPublicDisclosureURL()).isEqualTo(
                euRegAnlage.getFeuerungsanlage().getOeffentlicheBekanntmachungUrl());

    }

    @Test
    void toXDto_no_insp_extension() {
        var euRegAnlage = EURegAnlageBuilder.aFullEURegF().build();

        var inspireParam = new InspireParam("insp_ns", "insp_site", null);
        var productionSite = new ProductionSite();
        productionSite.setGmlId("siteGmlId");
        var context = mock(ProductionInstallationMapperContext.class);
        when(context.getProductionSite()).thenReturn(productionSite);
        when(context.getInspireParam()).thenReturn(inspireParam);
        var xdto = mapper.toXDto(euRegAnlage, context);

        assertThat(xdto.getInspireID().getIdentifier().getLocalId()).isEqualTo(
                euRegAnlage.getLocalId2());
        assertThat(xdto.getInspireID().getIdentifier().getNamespace()).isEqualTo(inspireParam.getNamespace());
    }

}
