package de.wps.bube.basis.referenzdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.base.vo.Land;

class BehoerdeTest {

    @ParameterizedTest
    @CsvSource({ "2007,2099", "2019,2019", "2010,2011" })
    void pruefeGueltigkeit_gueltig(int von, int bis) {
        var b = aBehoerde("beh").gueltigVon(von).gueltigBis(bis).build();
        assertDoesNotThrow(b::pruefeGueltigkeit);
    }

    @ParameterizedTest
    @CsvSource({ "2007,2006", "2019,2000", "3000,2000" })
    void pruefeGueltigkeit_ungueltig(int von, int bis) {
        var b = aBehoerde("beh").gueltigVon(von).gueltigBis(bis).build();
        assertThrows(ReferenzdatenException.class, b::pruefeGueltigkeit);
    }

    @Test
    void behoerde_deutschland() {
        assertThrows(ReferenzdatenException.class, () -> new Behoerde().setLand(Land.DEUTSCHLAND));
    }

}
