package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RJAHR;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.function.ThrowingConsumer;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzlisteMetadata;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzlisteMetadataRepository;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = ReferenzenService.class)
class ReferenzenServiceSecurityTest extends SecurityTest {

    /**
     * Ermöglicht typsichere Erzeugung der Argumente für Testfälle dieser Klasse
     *
     * @param methodName Name der zu testenden Methode
     * @param method     Beschreibung des Service-Aufrufs
     *
     * @return Argumentdefinition für einen Testfall
     */
    private static Arguments createTestArguments(String methodName, ThrowingConsumer<ReferenzenService> method) {
        return arguments(methodName, method);
    }

    private static Stream<? extends Arguments> writeMethods() {
        return Stream.of(
                createTestArguments("createReferenz", service -> service.createReferenz(referenzBuilder.build())),
                createTestArguments("updateReferenz",
                        service -> service.updateReferenz(referenzBuilder.id(15L).build())));
    }

    private static Stream<? extends Arguments> useMethods() {
        return Stream.of(createTestArguments("readReferenzliste",
                service -> service.readReferenzliste(Referenzliste.RBATE, Gueltigkeitsjahr.of(2020), HAMBURG)),
                createTestArguments("readReferenz",
                        service -> service.readReferenz(Referenzliste.RBATE, "SCH", Gueltigkeitsjahr.of(2020),
                                Land.DEUTSCHLAND)),
                createTestArguments("getAktuellesBerichtsjahr", ReferenzenService::getAktuellesBerichtsjahr));
    }

    private static Stream<? extends Arguments> readMethods() {
        return Stream.of(createTestArguments("readReferenzlisteForEdit",
                service -> service.readReferenzlisteForEdit(Referenzliste.RBATE, Function.identity())),
                createTestArguments("streamReferenzlisteForEdit",
                        service -> service.streamReferenzlisteForEdit(Referenzliste.RBATE)),
                createTestArguments("readAllReferenzlistenMetadata",
                        ReferenzenService::readAllReferenzlistenMetadata),
                createTestArguments("findByReferenznummer", service -> service.findByReferenznummer("1234567890")),
                createTestArguments("readReferenzlisteMetadata",
                        service -> service.readReferenzlisteMetadata(Referenzliste.RBATE)));
    }

    private static Stream<? extends Arguments> deleteMethods() {
        return Stream.of(createTestArguments("deleteReferenz", service -> service.deleteReferenz(5L)));
    }

    @MockBean
    private ReferenzRepository referenzRepository;

    @MockBean
    private BehoerdenRepository behoerdenRepository;

    @MockBean
    private ReferenzlisteMetadataRepository referenzlisteMetadataRepository;

    @MockBean
    private ReferenzenBerechtigungsContext referenzenBerechtigungsContext;

    @Autowired
    private ReferenzenService service;

    private static ReferenzBuilder referenzBuilder;

    @BeforeEach
    void setUp() {
        referenzBuilder = aGemeindeReferenz("RGMD").land(HAMBURG);
        when(referenzRepository.findById(anyLong())).then(
                i -> Optional.of(referenzBuilder.id(i.getArgument(0)).build()));
        when(referenzRepository.findByReferenznummer(anyString())).then(
                i -> Optional.of(referenzBuilder.referenznummer(i.getArgument(0)).build()));
        when(referenzRepository.findByKey(any(), any(), any(), any())).thenReturn(Optional.of(referenzBuilder.build()));
        when(referenzRepository.findFirstByReferenzlisteOrderBySortierDesc(any())).thenReturn(
                Optional.of(referenzBuilder.build()));
        when(referenzlisteMetadataRepository.findById(any())).thenReturn(
                Optional.of(new ReferenzlisteMetadata(Referenzliste.RBATE, "BVT-assoziierter Emissionswert",
                        "BVT-assoziierter Emissionswert")));
    }

    @DisplayName("withoutAuthorities -> Deny All")
    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods", "deleteMethods", "useMethods" })
    @WithBubeMockUser
    void withoutAuthoritiesDenyAll(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(referenzRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "writeMethods", "deleteMethods" })
    @WithBubeMockUser(roles = { REFERENZLISTEN_USE, REFERENZLISTEN_BUND_READ, REFERENZLISTEN_LAND_READ })
    void readonlyDenyWriteAndDelete(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(referenzRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "useMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_USE)
    void useAllowRead(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_BUND_READ)
    void bundAllowRead(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        referenzBuilder.referenzliste(RJAHR).land(DEUTSCHLAND);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_READ)
    void landAllowRead(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = { REFERENZLISTEN_BUND_WRITE, REFERENZLISTEN_LAND_WRITE })
    void writeDenyDelete(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(referenzRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_WRITE)
    void landWriteAllowWriteAndRead(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(true);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_BUND_WRITE)
    void bundWriteAllowWriteAndRead(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(true);
        referenzBuilder.referenzliste(RJAHR).land(DEUTSCHLAND);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_LAND_DELETE)
    void landDeleteAllowDelete(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = REFERENZLISTEN_BUND_DELETE)
    void bundDeleteAllowDelete(String methodName, ThrowingConsumer<ReferenzenService> method) {
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);
        referenzBuilder.referenzliste(RJAHR).land(DEUTSCHLAND);
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

}
