package de.wps.bube.basis.stammdaten.xml;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaetteType;
import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.vo.Validierungshinweis;
import de.wps.bube.basis.base.xml.CommonXMLMapperImpl;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;

@SpringBootTest(classes = { BetriebsstaetteXMLMapperImpl.class, CommonXMLMapperImpl.class,
        ReferenzResolverService.class })
class BetriebsstaetteXMLServiceReadTest {

    private static final String VALID_XML = "xml/stammdaten_import_test_full.xml";
    private static final String INVALID_XML = "xml/invalid_stammdaten_import_test.xml";
    private static final String INVALID2_XML = "xml/invalid_stammdaten2_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Autowired
    private BetriebsstaetteXMLMapper mapper;
    @MockBean
    private BehoerdenService behoerdenService;
    @MockBean
    private ReferenzenService referenzenService;

    @Test
    void test_validXml() throws IOException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(VALID_XML), BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).isEmpty();
    }

    @Test
    void read_invalidXml() throws FileNotFoundException {

        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_XML), BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);
        assertFalse(hinweise.isEmpty());

        assertEquals(
                "XML-Import Fehler Zeile 8: cvc-complex-type.4: Attribut 'name' muss in Element 'betriebsstaette' vorkommen.",
                hinweise.get(0).hinweis);
        assertTrue(hinweise.get(0).isError());
    }

    @Test
    void read_invalidXml_23Errors() throws FileNotFoundException {
        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID2_XML), BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);
        assertThat(hinweise).hasSize(27);
    }

    @Test
    void read_validXml() throws IOException {
        var betriebsstaetteRead = new AtomicBoolean();
        when(behoerdenService.findBehoerde(anyString(), eq(Gueltigkeitsjahr.of(2018)), eq(Land.of("05")))).thenAnswer(
                i ->
                        Optional.of(aBehoerde(i.getArgument(0)).build()));
        when(referenzenService.findReferenz(any(Referenzliste.class), anyString(), any(Gueltigkeitsjahr.class),
                any(Land.class))).thenAnswer(
                i -> Optional.of(
                        ReferenzBuilder.aReferenz(i.getArgument(0), i.getArgument(1)).land(i.getArgument(3)).build()));
        xmlService.read(getResource(VALID_XML), BetriebsstaetteType.class, mapper,
                BetriebsstaettenXDto.XML_ROOT_ELEMENT_NAME, null, bstWithErrors -> {
                    var betriebsstaette = bstWithErrors.entity;
                    assertThat(betriebsstaette).isNotNull();

                    assertEquals("2018", betriebsstaette.getBerichtsjahr().getSchluessel());
                    assertEquals("05", betriebsstaette.getLand().getNr());
                    assertEquals("nummer", betriebsstaette.getBetriebsstaetteNr());
                    assertEquals("Halsbrecher Betrieb", betriebsstaette.getName());
                    assertEquals("behoerde", betriebsstaette.getZustaendigeBehoerde().getSchluessel());
                    assertEquals("betrieb", betriebsstaette.getBetriebsstatus().getSchluessel());
                    assertEquals("AKZ", betriebsstaette.getAkz());
                    assertEquals("localId", betriebsstaette.getLocalId());
                    assertEquals("gemeinde", betriebsstaette.getGemeindekennziffer().getSchluessel());
                    assertEquals("vertraulich", betriebsstaette.getVertraulichkeitsgrund().getSchluessel());
                    assertEquals("FLEZ", betriebsstaette.getFlusseinzugsgebiet().getSchluessel());
                    assertEquals("nace", betriebsstaette.getNaceCode().getSchluessel());
                    assertTrue(betriebsstaette.isDirekteinleiter());
                    assertFalse(betriebsstaette.isIndirekteinleiter());
                    assertEquals(LocalDate.of(2018, 10, 10), betriebsstaette.getBetriebsstatusSeit());
                    assertEquals(LocalDate.of(2018, 12, 15), betriebsstaette.getInbetriebnahme());
                    assertTrue(betriebsstaette.isBetreiberdatenUebernommen());
                    assertEquals("2020-02-20T14:00:00Z", betriebsstaette.getLetzteAenderung().toString());
                    assertEquals("2020-02-20T14:00:00Z", betriebsstaette.getErsteErfassung().toString());

                    assertThat(betriebsstaette.getAdresse()).isNotNull();

                    assertThat(betriebsstaette.getBetreiber()).isNotNull();
                    assertEquals("betreiber", betriebsstaette.getBetreiber().getName());
                    assertEquals("2020-02-20T14:00:00Z",
                            betriebsstaette.getBetreiber().getLetzteAenderung().toString());
                    assertEquals("2020-02-20T14:00:00Z", betriebsstaette.getBetreiber().getErsteErfassung().toString());
                    assertThat(betriebsstaette.getBetreiber().getAdresse()).isNotNull();

                    assertEquals(2, betriebsstaette.getAnsprechpartner().size());
                    assertThat(betriebsstaette.getKommunikationsverbindungen()).hasSize(2);

                    assertEquals(2, betriebsstaette.getEinleiterNummern().size());
                    assertEquals(2, betriebsstaette.getAbfallerzeugerNummern().size());

                    assertEquals(2, betriebsstaette.getZustaendigeBehoerden().size());
                    assertEquals(2, betriebsstaette.getQuellen().size());
                    assertEquals(2, betriebsstaette.getAnlagen().size());
                    betriebsstaetteRead.set(true);

                });
        assertThat(betriebsstaetteRead).isTrue();
    }

}
