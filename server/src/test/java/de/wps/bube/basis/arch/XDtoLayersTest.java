package de.wps.bube.basis.arch;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = XDtoLayersTest.PACKAGE_BASE, importOptions = ImportOption.DoNotIncludeTests.class)
class XDtoLayersTest {

    static final String PACKAGE_BASE = "de.wps.bube.basis";

    @ArchTest
    public static final ArchRule stammdatenXDtoRule = xdtoRule("stammdaten");

    @ArchTest
    public static final ArchRule euregistryXDtoRule = xdtoRule("euregistry");

    @ArchTest
    public static final ArchRule benutzerXDtoRule = xdtoRule("benutzer");

    @ArchTest
    @ArchIgnore(reason = "Stammdaten uses ReferenzdatenXDtos")
    public static final ArchRule referenzdatenXDtoRule = xdtoRule("referenzdaten");


    private static ArchRule xdtoRule(String domain) {
        return classes().that().resideInAPackage("%s.%s.xml.dto".formatted(PACKAGE_BASE, domain))
                        .should().onlyBeAccessed().byAnyPackage("%s.%s..".formatted(PACKAGE_BASE, domain));
    }

}
