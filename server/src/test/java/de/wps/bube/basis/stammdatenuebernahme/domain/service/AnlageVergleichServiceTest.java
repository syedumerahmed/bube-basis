package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.GeoPunktVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LocalDateVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.SpbAnlageVergleich;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;
import de.wps.bube.basis.test.BubeIntegrationTest;

class AnlageVergleichServiceTest extends BubeIntegrationTest {

    public static final Anlage ANLAGE = AnlageBuilder.aFullAnlage().id(564L).build();
    private AnlageVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        this.vergleichService = new AnlageVergleichService(new CommonVergleichService());
    }

    @Test
    void vergleicheGleich() {

        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(564L);

        SpbAnlageVergleich vergleich = vergleichService.vergleiche(ANLAGE, spbAnlage);

        assertThat(vergleich.getId()).isEqualTo(ANLAGE.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbAnlage.getId());

        StringVector anlageNr = vergleich.getAnlageNr();
        assertThat(anlageNr.getSpl()).isEqualTo(anlageNr.getSpb());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isEqualTo(inbetriebnahme.getSpb());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isEqualTo(geoPunkt.getSpb());

        List<LeistungVector> leistungen = vergleich.getLeistungen();
        assertThat(leistungen.stream().filter(LeistungVector::isEqual).count()).isEqualTo(2);

        assertThat(anlageNr.isEqual()).isTrue();
        assertThat(name.isEqual()).isTrue();
        assertThat(betriebsstatus.isEqual()).isTrue();
        assertThat(betriebsstatusSeit.isEqual()).isTrue();
        assertThat(inbetriebnahme.isEqual()).isTrue();
        assertThat(geoPunkt.isEqual()).isTrue();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheUnterschiedlich() {
        Anlage andereAnlage = AnlageBuilder.anAnlage(12L)
                                           .betriebsstatusSeit(LocalDate.now())
                                           .inbetriebnahme(LocalDate.now())
                                           .anlageNr("test")
                                           .name("test")
                                           .betriebsstatus(
                                                   ReferenzBuilder.aBetriebsstatusReferenz("test").id(546L).build())
                                           .geoPunkt(new GeoPunkt(15, 15,
                                                   ReferenzBuilder.anEpsgReferenz("test").id(456L).build()))
                                           .leistungen(Collections.emptyList())
                                           .build();

        SpbAnlage spbAnlage = new SpbAnlage(andereAnlage);
        spbAnlage.setId(564L);

        SpbAnlageVergleich vergleich = vergleichService.vergleiche(ANLAGE, spbAnlage);

        assertThat(vergleich.getId()).isEqualTo(ANLAGE.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(spbAnlage.getId());

        StringVector anlageNr = vergleich.getAnlageNr();
        assertThat(anlageNr.getSpl()).isNotEqualTo(anlageNr.getSpb());

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus.getSpl()).isNotEqualTo(betriebsstatus.getSpb());

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit.getSpl()).isNotEqualTo(betriebsstatusSeit.getSpb());

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme.getSpl()).isNotEqualTo(inbetriebnahme.getSpb());

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt.getSpl()).isNotEqualTo(geoPunkt.getSpb());

        List<LeistungVector> leistungen = vergleich.getLeistungen();
        assertThat(leistungen.stream().filter(l -> !l.isEqual()).count()).isEqualTo(2);

        assertThat(anlageNr.isEqual()).isFalse();
        assertThat(name.isEqual()).isFalse();
        assertThat(betriebsstatus.isEqual()).isFalse();
        assertThat(betriebsstatusSeit.isEqual()).isFalse();
        assertThat(inbetriebnahme.isEqual()).isFalse();
        assertThat(geoPunkt.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }

    @Test
    void vergleicheNurSPL() {
        SpbAnlageVergleich vergleich = vergleichService.vergleiche(ANLAGE, null);

        assertThat(vergleich.getId()).isEqualTo(ANLAGE.getId());
        assertThat(vergleich.getSpbId()).isEqualTo(null);

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        StringVector anlageNr = vergleich.getAnlageNr();
        assertThat(anlageNr).isNull();

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus).isNull();

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit).isNull();

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme).isNull();

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt).isNull();

        List<LeistungVector> leistungen = vergleich.getLeistungen();
        assertThat(leistungen).isNull();

        assertThat(vergleich.isHasChanges()).isFalse();
    }

    @Test
    void vergleicheNurSPB() {
        SpbAnlage spbAnlage = new SpbAnlage(ANLAGE);
        spbAnlage.setId(564L);
        SpbAnlageVergleich vergleich = vergleichService.vergleiche(null, spbAnlage);

        assertThat(vergleich.getId()).isEqualTo(null);
        assertThat(vergleich.getSpbId()).isEqualTo(spbAnlage.getId());

        StringVector anlageNr = vergleich.getAnlageNr();
        assertThat(anlageNr).isNull();

        StringVector name = vergleich.getName();
        assertThat(name.getSpl()).isNotEqualTo(name.getSpb());

        ReferenzVector betriebsstatus = vergleich.getBetriebsstatus();
        assertThat(betriebsstatus).isNull();

        LocalDateVector betriebsstatusSeit = vergleich.getBetriebsstatusSeit();
        assertThat(betriebsstatusSeit).isNull();

        LocalDateVector inbetriebnahme = vergleich.getInbetriebnahme();
        assertThat(inbetriebnahme).isNull();

        GeoPunktVector geoPunkt = vergleich.getGeoPunkt();
        assertThat(geoPunkt).isNull();

        List<LeistungVector> leistungen = vergleich.getLeistungen();
        assertThat(leistungen).isNull();

        assertThat(name.isEqual()).isFalse();

        assertThat(vergleich.isHasChanges()).isTrue();
    }
}
