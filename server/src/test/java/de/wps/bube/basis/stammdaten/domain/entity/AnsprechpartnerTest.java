package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.Kommunikationsverbindung;

class AnsprechpartnerTest {

    @Test
    void equalValues_emptyKommunikation_true() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").id(1L).build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();

        assertThat(ap1.equalValues(ap2)).isTrue();
    }

    @Test
    void equalValues_emptyKommunikation_false() {

        var funktion1 = ReferenzBuilder.aFunktionsReferenz("Funktion1").id(1L).build();
        var funktion2 = ReferenzBuilder.aFunktionsReferenz("Funktion2").id(2L).build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion1)
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion2)
                                        .build();

        assertThat(ap1.equalValues(ap2)).isFalse();
    }

    @Test
    void equalValues_ap1WithKommunikation_false() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").id(1L).build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .kommunikationsverbindungen(
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                        "123"),
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                        "321"))
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .build();

        assertThat(ap1.equalValues(ap2)).isFalse();
    }

    @Test
    void equalValues_WithOtherKommunikation_false() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").id(1L).build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .kommunikationsverbindungen(
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                        "123"),
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                        "321"))
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .kommunikationsverbindungen(
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                        "99999999"),
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                        "321"))
                                        .build();

        assertThat(ap1.equalValues(ap2)).isFalse();
    }

    @Test
    void equalValues_withKommunikation_true() {

        var funktion = ReferenzBuilder.aFunktionsReferenz("Funktion").id(1L).build();
        var ap1 = AnsprechpartnerBuilder.anAnsprechpartner(1L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .kommunikationsverbindungen(
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                        "123"),
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                        "321"))
                                        .build();
        var ap2 = AnsprechpartnerBuilder.anAnsprechpartner(2L)
                                        .vorname("Vorname")
                                        .nachname("Nachname")
                                        .funktion(funktion)
                                        .kommunikationsverbindungen(
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("tel").build(),
                                                        "123"),
                                                new Kommunikationsverbindung(aKommunikationsTypReferenz("fax").build(),
                                                        "321"))
                                        .build();

        assertThat(ap1.equalValues(ap2)).isTrue();
    }
}
