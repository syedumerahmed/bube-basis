package de.wps.bube.basis.stammdaten.domain.service;

import static org.mockito.Mockito.verifyNoInteractions;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.euregistry.domain.service.EURegistryService;

@ExtendWith(MockitoExtension.class)
class BetriebsstaettePruefungJobTest {

    private static final String USER = "username";

    @Mock
    private BetriebsstaettePruefungJob betriebsstaettePruefungJob;
    @Mock
    private BetriebsstaettePruefungService betriebsstaettePruefungService;
    @Mock
    private EURegistryService euRegistryService;

    @Test
    void singleBetriebsstaette() {

        betriebsstaettePruefungJob.runAsync(List.of(1L), USER);

        verifyNoInteractions(betriebsstaettePruefungService);
        verifyNoInteractions(euRegistryService);

    }


}
