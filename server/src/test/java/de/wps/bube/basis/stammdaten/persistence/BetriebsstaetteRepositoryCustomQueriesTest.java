package de.wps.bube.basis.stammdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aZustaendigkeitsReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NIEDERSACHSEN;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Pageable;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class BetriebsstaetteRepositoryCustomQueriesTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private BetriebsstaetteRepository repository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @Autowired
    private BehoerdenRepository behoerdenRepository;
    @Autowired
    private AnlageRepository anlageRepository;
    @Autowired
    private AnlagenteilRepository anlagenteilRepository;

    private Behoerde behoerde;
    private Referenz jahr, gemeindeKennziffer, zustaendigkeit, betriebsstatus;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        jahr = referenzRepository.save(aJahrReferenz(2020).build());
        betriebsstatus = referenzRepository.save(aBetriebsstatusReferenz("STATUS").build());
        gemeindeKennziffer = referenzRepository.save(aGemeindeReferenz("HHN0UHL").build());
        zustaendigkeit = referenzRepository.save(aZustaendigkeitsReferenz("ZUST").build());
    }

    @Test
    void findAllIds() {
        var betriebsstaettenIds = Stream.of("1", "2", "3")
                                        .map(nr -> getDefaultBetriebsstaetteBuilder().betriebsstaetteNr(nr).build())
                                        .map(repository::save)
                                        .map(Betriebsstaette::getId)
                                        .collect(toList());

        assertThat(repository.findAllIds(null)).containsExactlyInAnyOrderElementsOf(betriebsstaettenIds);

    }

    @Test
    void findAllBetriebsstaettenNummern() {
        var betriebsstaettenNummern = Stream.of("1", "2", "3")
                                            .map(nr -> getDefaultBetriebsstaetteBuilder().betriebsstaetteNr(nr).build())
                                            .map(repository::save)
                                            .map(Betriebsstaette::getBetriebsstaetteNr)
                                            .collect(toList());

        assertThat(repository.findAllBetriebsstaettenNr(null)).containsExactlyInAnyOrderElementsOf(
                betriebsstaettenNummern);

    }

    @Test
    void findAllAsListItems() {
        var expectedBetriebsstaettenItems = Stream.of("1", "2", "3")
                                                  .map(nr -> getDefaultBetriebsstaetteBuilder().betriebsstaetteNr(nr)
                                                                                               .build())
                                                  .map(repository::save)
                                                  .map(bst -> new BetriebsstaetteListItemView() {{
                                                      id = bst.getId();
                                                      zustaendigeBehoerde = bst.getZustaendigeBehoerde();
                                                      name = bst.getName();
                                                      land = bst.getLand();
                                                      betriebsstaetteNr = bst.getBetriebsstaetteNr();
                                                      betriebsstatus = bst.getBetriebsstatus();
                                                      ort = bst.getAdresse().getOrt();
                                                      plz = bst.getAdresse().getPlz();
                                                      strasse = bst.getAdresse().getStrasse();
                                                      hausNr = bst.getAdresse().getHausNr();
                                                      akz = bst.getAkz();
                                                      gemeindekennziffer = bst.getGemeindekennziffer();
                                                      betreiberdatenUebernommen = bst.isBetreiberdatenUebernommen();
                                                  }}).collect(toList());

        assertThat(repository.findAllAsListItems(null, Pageable.unpaged()))
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrderElementsOf(expectedBetriebsstaettenItems);

    }

    @Test
    void existsByBerichtsjahrAndLandAndBetriebsstaetteNr() {
        var b = repository.save(getDefaultBetriebsstaetteBuilder().build());

        assertTrue(
                repository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(b.getBerichtsjahr().getId(),
                        b.getLand(), b.getBetriebsstaetteNr()));
    }

    @Test
    void existsByBerichtsjahrAndLandAndBetriebsstaetteNr_OtherLand() {
        var b = repository.save(getDefaultBetriebsstaetteBuilder().land(Land.SAARLAND).build());

        assertFalse(
                repository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(b.getBerichtsjahr().getId(),
                        Land.BAYERN, b.getBetriebsstaetteNr()));
    }

    @Test
    void existsByBerichtsjahrAndLandAndBetriebsstaetteNr_OtherNummer() {
        var b = repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("11-22-11").build());

        assertFalse(
                repository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(b.getBerichtsjahr().getId(),
                        b.getLand(), "555"));
    }

    @Test
    void existsByBerichtsjahrAndLandAndBetriebsstaetteNr_OtherJahr() {
        var jahr2018 = referenzRepository.save(aJahrReferenz(2018).build());
        var jahr2020 = referenzRepository.save(aJahrReferenz(2020).build());
        var b = repository.save(getDefaultBetriebsstaetteBuilder().berichtsjahr(jahr2018).build());

        assertFalse(
                repository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(jahr2020.getId(), b.getLand(),
                        b.getBetriebsstaetteNr()));
    }

    @Test
    void existsByBerichtsjahrAndLandAndBetriebsstaetteNr_otherCase() {
        var b = repository.save(getDefaultBetriebsstaetteBuilder().betriebsstaetteNr("xyz").build());

        assertTrue(
                repository.existsByBerichtsjahrIdAndLandAndBetriebsstaetteNrIgnoreCase(b.getBerichtsjahr().getId(),
                        b.getLand(), "Xyz"));
    }

    @Nested
    class ZustaendigeBehoerdenForBetriebsstaetten {
        Behoerde behoerdeA, behoerdeB, behoerdeC, behoerdeD;
        Referenz jahr2021;
        BetriebsstaetteBuilder builder;

        @BeforeEach
        void setUp() {
            jahr2021 = referenzRepository.save(aJahrReferenz(2021).build());
            behoerdeA = aBehoerde("A").build();
            behoerdeB = aBehoerde("B").build();
            behoerdeC = aBehoerde("C").build();
            behoerdeD = aBehoerde("D").build();
            var behoerden = new ArrayList<>(List.of(behoerdeA, behoerdeB, behoerdeC, behoerdeD));
            Collections.shuffle(behoerden);
            behoerdenRepository.saveAll(behoerden);

            builder = aBetriebsstaette()
                    .land(HAMBURG)
                    .betriebsstaetteNr("1")
                    .berichtsjahr(jahr)
                    .betriebsstatus(betriebsstatus)
                    .akz("AKZ")
                    .gemeindekennziffer(gemeindeKennziffer)
                    .adresse(AdresseBuilder.anAdresse()
                                           .strasse("Müllerweg")
                                           .hausNr("2a")
                                           .ort("Hamburg")
                                           .plz("22152")
                                           .build());
        }

        @Test
        void shouldFindIndependentOfBerichtsjahr() {
            var bst2020 = builder.zustaendigeBehoerde(behoerdeA).build();
            var bst2021 = builder.berichtsjahr(jahr2021).zustaendigeBehoerde(behoerdeB).build();
            var bst2021_2 = builder.betriebsstaetteNr("2")
                                   .berichtsjahr(jahr2021)
                                   .zustaendigeBehoerde(behoerdeC)
                                   .build();
            var bstIrrelevantNummer = builder.betriebsstaetteNr("3")
                                             .berichtsjahr(jahr2021)
                                             .zustaendigeBehoerde(behoerdeD)
                                             .build();
            var bstDifferentLand = builder.betriebsstaetteNr("1")
                                          .berichtsjahr(jahr)
                                          .zustaendigeBehoerde(behoerdeD)
                                          .land(NIEDERSACHSEN)
                                          .build();
            repository.saveAll(List.of(bst2021, bst2020, bst2021_2, bstIrrelevantNummer, bstDifferentLand));

            var behoerdenSchluessel = repository.findAllZustaendigeBehoerdenSchluessel(List.of("1", "2"), HAMBURG);

            assertThat(behoerdenSchluessel).containsExactly("A", "B", "C");
        }

        @Test
        void shouldBeDistinctAndOrdered() {
            List<Betriebsstaette> betriebsstaetten = new ArrayList<>(List.of(
                    builder.betriebsstaetteNr("1").zustaendigeBehoerde(behoerdeA).build(),
                    builder.betriebsstaetteNr("2").zustaendigeBehoerde(behoerdeA).build(),
                    builder.betriebsstaetteNr("3").zustaendigeBehoerde(behoerdeB).build(),
                    builder.betriebsstaetteNr("4").zustaendigeBehoerde(behoerdeC).build(),
                    builder.betriebsstaetteNr("5").zustaendigeBehoerde(behoerdeD).build()
            ));
            Collections.shuffle(betriebsstaetten);
            repository.saveAll(betriebsstaetten);

            var behoerdenSchluessel = repository.findAllZustaendigeBehoerdenSchluessel(
                    List.of("5", "4", "1", "2", "3"), HAMBURG);

            assertThat(behoerdenSchluessel).containsExactly("A", "B", "C", "D");
        }

        @Test
        void shouldBeCaseInsensitive() {
            List<Betriebsstaette> betriebsstaetten = new ArrayList<>(List.of(
                    builder.betriebsstaetteNr("BST1").zustaendigeBehoerde(behoerdeA).build(),
                    builder.betriebsstaetteNr("BST2").zustaendigeBehoerde(behoerdeB).build(),
                    builder.betriebsstaetteNr("BST3").zustaendigeBehoerde(behoerdeC).build(),
                    builder.betriebsstaetteNr("BST4").zustaendigeBehoerde(behoerdeD).build()
            ));
            repository.saveAll(betriebsstaetten);

            var behoerdenSchluessel = repository.findAllZustaendigeBehoerdenSchluessel(
                    List.of("BST1", "bst2", "bSt3", "bst 4"), HAMBURG);

            assertThat(behoerdenSchluessel).containsExactly("A", "B", "C");
        }
    }

    @Test
    void betriebsstaetteLocalIdNull() {

        Betriebsstaette b = this.getDefaultBetriebsstaetteBuilder().localId(null).build();
        b = repository.save(b);

        boolean result = repository.localIdExists("12345", b.getLand().getNr(),
                b.getBerichtsjahr().getId());
        assertThat(result).isFalse();
    }

    @Test
    void testNotUsed() {
        var beh = behoerdenRepository.save(aBehoerde("moin").build());
        assertThat(repository.behoerdeUsed(beh.getId())).isFalse();
    }

    @Test
    void testUsedInBST() {
        var beh = behoerdenRepository.save(aBehoerde("moin").build());
        repository.save(BetriebsstaetteBuilder.aBetriebsstaette()
                                              .zustaendigeBehoerde(beh)
                                              .berichtsjahr(jahr)
                                              .betriebsstatus(betriebsstatus)
                                              .gemeindekennziffer(gemeindeKennziffer)
                                              .build());

        assertThat(repository.behoerdeUsed(beh.getId())).isTrue();
    }

    @Test
    void testUsedInBST_weitereBehoerden() {
        var beh = behoerdenRepository.save(aBehoerde("moin").build());
        var beh2 = behoerdenRepository.save(aBehoerde("behoerde2").build());
        repository.save(BetriebsstaetteBuilder.aBetriebsstaette()
                                              .zustaendigeBehoerde(beh)
                                              .berichtsjahr(jahr)
                                              .gemeindekennziffer(gemeindeKennziffer)
                                              .betriebsstatus(betriebsstatus)
                                              .zustaendigeBehoerden(new ZustaendigeBehoerde(beh2, zustaendigkeit))
                                              .build());

        assertThat(repository.behoerdeUsed(beh2.getId())).isTrue();
    }

    @Test
    void testUsedInAnlage() {
        var beh = behoerdenRepository.save(aBehoerde("moin").build());
        var betriebsstaette = repository.save(getDefaultBetriebsstaetteBuilder().build());
        anlageRepository.save(anAnlage().parentBetriebsstaetteId(betriebsstaette.getId())
                                        .addZustaendigeBehoerde(new ZustaendigeBehoerde(beh, zustaendigkeit))
                                        .build());

        assertThat(repository.behoerdeUsed(beh.getId())).isTrue();
    }

    @Test
    void usedInAnlagenteil() {
        var beh = behoerdenRepository.save(aBehoerde("moin").build());
        var betriebsstaette = repository.save(getDefaultBetriebsstaetteBuilder().build());
        var anlage = anlageRepository.save(anAnlage().parentBetriebsstaetteId(betriebsstaette.getId())
                                                     .build());
        anlagenteilRepository.save(
                anAnlagenteil().parentAnlageId(anlage.getId())
                               .addZustaendigeBehoerde(new ZustaendigeBehoerde(beh, zustaendigkeit))
                               .build());

        assertThat(repository.behoerdeUsed(beh.getId())).isTrue();
    }

    @Nested
    class BetriebsstaetteLocalIdNotNull {
        Betriebsstaette b;

        @BeforeEach
        void setUp() {
            b = getDefaultBetriebsstaetteBuilder().localId("FINDEMICH").build();
            repository.save(b);
        }

        @Test
        void andereLocalId() {
            assertThat(repository.localIdExists("12345", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void shouldFind() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isTrue();
        }

        @Test
        void anderesLand() {
            assertThat(repository.localIdExists("FINDEMICH", "XX", b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void anderesJahr() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(), 95237292343632L)).isFalse();
        }
    }

    @Test
    void anlageLocalIdNull() {
        Betriebsstaette b = this.getDefaultBetriebsstaetteBuilder().build();
        b = repository.save(b);

        anlageRepository.save(anAnlage().parentBetriebsstaetteId(b.getId()).localId(null).build());

        boolean result = repository.localIdExists("FINDEMICH", b.getLand().getNr(),
                b.getBerichtsjahr().getId());
        assertThat(result).isFalse();
    }

    @Nested
    class AnlageLocalIdNotNull {
        Betriebsstaette b;

        @BeforeEach
        void setUp() {
            b = repository.save(getDefaultBetriebsstaetteBuilder().build());
            anlageRepository.save(anAnlage().parentBetriebsstaetteId(b.getId()).localId("FINDEMICH").build());
        }

        @Test
        void andereLocalId() {
            assertThat(repository.localIdExists("FINDEMICHNICHT", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void shouldFind() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isTrue();
        }

        @Test
        void anderesLand() {
            assertThat(repository.localIdExists("FINDEMICH", "??", b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void anderesJahr() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(), 6125819103273253L)).isFalse();
        }
    }

    @Nested
    class AnlagenteilAndBetriebsstaetteLocalIdNotNull {
        Betriebsstaette b;

        @BeforeEach
        void setUp() {
            b = repository.save(getDefaultBetriebsstaetteBuilder().localId(null).build());
            var a = anlageRepository.save(anAnlage().parentBetriebsstaetteId(b.getId()).build());
            anlagenteilRepository.save(anAnlagenteil().parentAnlageId(a.getId()).localId("FINDEMICH").build());
        }

        @Test
        void andereLocalId() {
            assertThat(repository.localIdExists("FINDEMICHNICHT", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void shouldFind() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(),
                    b.getBerichtsjahr().getId())).isTrue();
        }

        @Test
        void anderesLand() {
            assertThat(repository.localIdExists("FINDEMICH", "??", b.getBerichtsjahr().getId())).isFalse();
        }

        @Test
        void anderesJahr() {
            assertThat(repository.localIdExists("FINDEMICH", b.getLand().getNr(), 6125819103273253L)).isFalse();
        }
    }

    @Test
    void updateKomplexpruefung() {
        var bst = repository.save(getDefaultBetriebsstaetteBuilder().build());

        assertThat(bst.isPruefungVorhanden()).isFalse();
        assertThat(bst.isPruefungsfehler()).isFalse();

        repository.updateKomplexpruefung(bst.getId(), true);
        entityManager.clear();

        var afterUpdate = repository.findById(bst.getId());
        assertThat(afterUpdate.get().isPruefungVorhanden()).isTrue();
        assertThat(afterUpdate.get().isPruefungsfehler()).isTrue();
    }

    @Test
    void updateSchreibsperreBetreiber() {
        var bst = repository.save(getDefaultBetriebsstaetteBuilder().build());

        assertThat(bst.isSchreibsperreBetreiber()).isFalse();

        repository.updateSchreibsperreBetreiber(bst.getId(), true);
        entityManager.clear();

        var afterUpdate = repository.findById(bst.getId());
        assertThat(afterUpdate.get().isSchreibsperreBetreiber()).isTrue();
    }

    @Test
    void aenderungDurchBetreiber() {
        var bst = entityManager.persistFlushFind(getDefaultBetriebsstaetteBuilder()
                .betreiberdatenUebernommen(true)
                .letzteAenderungBetreiber(null)
                .build());
        var modifiedDateSpl = bst.getLetzteAenderung();

        repository.aenderungDurchBetreiber(bst.getId());
        entityManager.clear();
        var updatedBst = repository.getById(bst.getId());

        assertThat(updatedBst.getLetzteAenderung()).isCloseTo(modifiedDateSpl, within(1, MILLIS));
        assertThat(updatedBst.isBetreiberdatenUebernommen()).isFalse();
        assertThat(updatedBst.getLetzteAenderungBetreiber()).isAfter(bst.getErsteErfassung());
    }

    private BetriebsstaetteBuilder getDefaultBetriebsstaetteBuilder() {
        return aBetriebsstaette().berichtsjahr(jahr)
                                 .land(HAMBURG)
                                 .betriebsstatus(betriebsstatus)
                                 .akz("AKZ")
                                 .zustaendigeBehoerde(behoerde)
                                 .betriebsstaetteNr("1234AA-12")
                                 .gemeindekennziffer(gemeindeKennziffer)
                                 .adresse(AdresseBuilder.anAdresse()
                                                        .strasse("Müllerweg")
                                                        .hausNr("2a")
                                                        .ort("Hamburg")
                                                        .plz("22152")
                                                        .build());
    }

}
