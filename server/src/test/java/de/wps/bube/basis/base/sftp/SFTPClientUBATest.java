package de.wps.bube.basis.base.sftp;

import java.io.IOException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class SFTPClientUBATest {

    @Test
    @Disabled
    void uploadFile() throws IOException {

        SFTPClientUBA client = new SFTPClientUBA();

        client.uploadFile();
    }
}