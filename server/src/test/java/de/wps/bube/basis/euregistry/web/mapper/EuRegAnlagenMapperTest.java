package de.wps.bube.basis.euregistry.web.mapper;

import static de.wps.bube.basis.euregistry.domain.vo.AuflageBuilder.anAuflage;
import static de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht.LIEGT_NICHT_VOR;
import static de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht.NICHT_ERFORDERLICH;
import static de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder.aFullBerichtsdaten;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aEmissionswertReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aTaetigkeitsReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aZustaendigkeitsReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.aFullAnlage;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.aFullAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_13BV;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_IE_RL;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_R4BV;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_RPRTR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.base.web.openapi.model.AnlagenInfoDto;
import de.wps.bube.basis.base.web.openapi.model.AuflageDto;
import de.wps.bube.basis.base.web.openapi.model.BehoerdeListItemDto;
import de.wps.bube.basis.base.web.openapi.model.BerichtsdatenDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegAnlageDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegReportEnum;
import de.wps.bube.basis.base.web.openapi.model.ReferenzDto;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapperImpl;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { EuRegAnlagenMapperImpl.class, BerichtsdatenMapperImpl.class, ReferenzMapperImpl.class,
        BehoerdenMapperImpl.class })
class EuRegAnlagenMapperTest {

    @Autowired
    private EuRegAnlagenMapper mapper;

    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BehoerdenService behoerdenService;

    @Test
    void fromDto() {
        var berichtsdatenDto = new BerichtsdatenDto() {{
            setAkz("akz7");
            setBemerkung("moin");
            setZustaendigeBehoerde(new BehoerdeListItemDto() {{
                setId(500L);
                setSchluessel("beh2");
            }});
            setBerichtsart(new ReferenzDto() {{
                setLand("00");
                setGueltigVon(1991);
                setGueltigBis(1991);
            }});
            setBearbeitungsstatus(new ReferenzDto() {{
                setLand("00");
                setGueltigVon(1991);
                setGueltigBis(1991);
            }});
            setBearbeitungsstatusSeit(LocalDate.now());
        }};

        var anlagenInfoDto = new AnlagenInfoDto();
        anlagenInfoDto.setParentBetriebsstaetteId(5L);

        var dto = new EuRegAnlageDto();
        dto.setId(2L);
        dto.setBerichtsdaten(berichtsdatenDto);
        dto.setAnlagenInfo(anlagenInfoDto);
        dto.seteSpirsNr("spirs");
        dto.setMonitor("monitor");
        dto.setMonitorUrl("monitorUrl");
        dto.setReport(EuRegReportEnum.NICHT_ERFORDERLICH);
        dto.setTehgNr("tehg");
        dto.setVisits(2);
        dto.setVisitUrl("visitUrl");

        var auflage1 = new AuflageDto() {{
            setEmissionswert(new ReferenzDto() {{
                setLand("00");
                setGueltigVon(1991);
                setGueltigBis(1991);
                setSchluessel("em1");
            }});
        }};

        var auflage2 = new AuflageDto() {{
            setEmissionswert(new ReferenzDto() {{
                setLand("00");
                setGueltigVon(1991);
                setGueltigBis(1991);
                setSchluessel("em2");
            }});
            setNachArt14(true);
            setNachArt18(true);
        }};

        dto.setAuflagen(List.of(auflage1, auflage2));

        var beh2 = aBehoerde("beh2").build();
        when(behoerdenService.readBehoerde(berichtsdatenDto.getZustaendigeBehoerde().getId())).thenReturn(beh2);

        var entity = mapper.fromDto(dto);

        assertThat(entity.getBerichtsdaten().getAkz()).isEqualTo("akz7");
        assertThat(entity.getBerichtsdaten().getBemerkung()).isEqualTo("moin");
        assertThat(entity.getBerichtsdaten().getZustaendigeBehoerde()).isEqualTo(beh2);
        assertThat(entity.getId()).isEqualTo(2L);
        assertThat(entity.geteSpirsNr()).isEqualTo("spirs");
        assertThat(entity.getMonitor()).isEqualTo("monitor");
        assertThat(entity.getMonitorUrl()).isEqualTo("monitorUrl");
        assertThat(entity.getReport()).isEqualTo(NICHT_ERFORDERLICH);
        assertThat(entity.getTehgNr()).isEqualTo("tehg");
        assertThat(entity.getVisits()).isEqualTo(2);
        assertThat(entity.getVisitUrl()).isEqualTo("visitUrl");
        assertThat(entity.getAuflagen()).element(0).satisfies(auflage -> {
            assertThat(auflage.getEmissionswert().getSchluessel()).isEqualTo("em1");
            assertThat(auflage.isNachArt14()).isFalse();
            assertThat(auflage.isNachArt18()).isFalse();
        });
        assertThat(entity.getAuflagen()).element(1).satisfies(auflage -> {
            assertThat(auflage.getEmissionswert().getSchluessel()).isEqualTo("em2");
            assertThat(auflage.isNachArt14()).isTrue();
            assertThat(auflage.isNachArt18()).isTrue();
        });
    }

    @Test
    void toDto() {
        var genehmZBeh = new ZustaendigeBehoerde(aBehoerde("11").bezeichnung("genehm").build(),
                aZustaendigkeitsReferenz("Gen").schluessel("04").build());
        var ueberwZBeh = new ZustaendigeBehoerde(aBehoerde("12").bezeichnung("ueberw").build(),
                aZustaendigkeitsReferenz("Ueb").schluessel("05").build());

        var r4bv = aVorschrift().art(aVorschriftReferenz("4bv").schluessel(V_R4BV.getSchluessel()).build())
                                .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                        aTaetigkeitsReferenz("bimsch4Haupt").build())
                                .haupttaetigkeit(aTaetigkeitsReferenz("bimsch4Haupt").build())
                                .build();

        var r13bv = aVorschrift().art(aVorschriftReferenz("13bv").schluessel(V_13BV.getSchluessel()).build()).build();

        var ied = aVorschrift().art(aVorschriftReferenz("ied").schluessel(V_IE_RL.getSchluessel()).build())
                               .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                       aTaetigkeitsReferenz("iedHaupt").build())
                               .haupttaetigkeit(aTaetigkeitsReferenz("iedHaupt").build())
                               .build();

        var prtr = aVorschrift().art(aVorschriftReferenz("prtr").schluessel(V_RPRTR.getSchluessel()).build())
                                .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                        aTaetigkeitsReferenz("prtrHaupt").build())
                                .haupttaetigkeit(aTaetigkeitsReferenz("prtrHaupt").build())
                                .build();

        var anlage = aFullAnlage().addZustaendigeBehoerde(genehmZBeh)
                                  .addZustaendigeBehoerde(ueberwZBeh)
                                  .vorschriften(r4bv, r13bv, ied, prtr)
                                  .build();
        var berichtsdaten = aFullBerichtsdaten().build();

        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().anlage(anlage)
                                            .berichtsdaten(berichtsdaten)
                                            .auflagen(
                                                    anAuflage().emissionswert(aEmissionswertReferenz("em1").build())
                                                               .build(),
                                                    anAuflage().emissionswert(aEmissionswertReferenz("em2").build())
                                                               .nachArt14(true)
                                                               .nachArt18(true)
                                                               .build()
                                            )
                                            .eSpirsNr("spirs")
                                            .monitor("monitor")
                                            .monitorUrl("monitorUrl")
                                            .report(LIEGT_NICHT_VOR)
                                            .tehgNr("tehg")
                                            .visits(5)
                                            .visitUrl("visitUrl")
                                            .build();

        when(stammdatenService.canWriteBetriebsstaette(anlage.getParentBetriebsstaetteId())).thenReturn(true);

        var dto = mapper.toDto(euRegAnlage);

        assertThat(dto.getId()).isEqualTo(euRegAnlage.getId());
        assertThat(dto.getCanWrite()).isTrue();
        assertThat(dto.geteSpirsNr()).isEqualTo("spirs");
        assertThat(dto.getMonitor()).isEqualTo("monitor");
        assertThat(dto.getMonitorUrl()).isEqualTo("monitorUrl");
        assertThat(dto.getReport()).isEqualTo(EuRegReportEnum.LIEGT_NICHT_VOR);
        assertThat(dto.getTehgNr()).isEqualTo("tehg");
        assertThat(dto.getVisits()).isEqualTo(5);
        assertThat(dto.getVisitUrl()).isEqualTo("visitUrl");

        assertThat(dto.getBerichtsdaten().getAkz()).isEqualTo(berichtsdaten.getAkz());
        assertThat(dto.getBerichtsdaten().getBemerkung()).isEqualTo(berichtsdaten.getBemerkung());
        assertThat(dto.getBerichtsdaten().getZustaendigeBehoerde().getSchluessel()).isEqualTo(
                berichtsdaten.getZustaendigeBehoerde().getSchluessel());

        assertThat(dto.getAnlagenInfo().getAnlageNr()).isEqualTo(anlage.getAnlageNr());
        assertThat(dto.getAnlagenInfo().getLocalId()).isEqualTo(anlage.getLocalId());
        assertThat(dto.getAnlagenInfo().getName()).isEqualTo(anlage.getName());
        assertThat(dto.getAnlagenInfo().getParentBetriebsstaetteId()).isEqualTo(anlage.getParentBetriebsstaetteId());
        assertThat(dto.getAnlagenInfo().getBetriebsstatus()).isEqualTo(anlage.getBetriebsstatus().getKtext());
        assertThat(dto.getAnlagenInfo().getBetriebsstatusSeit()).isEqualTo(anlage.getBetriebsstatusSeit());
        assertThat(dto.getAnlagenInfo().getBimschv4()).isEqualTo("bimsch4Haupt");
        assertThat(dto.getAnlagenInfo().getBimschv13()).isEqualTo(true);
        assertThat(dto.getAnlagenInfo().getBimschv17()).isEqualTo(false);
        assertThat(dto.getAnlagenInfo().getGenehmigungsBehoerde()).isEqualTo("11 - genehm");
        assertThat(dto.getAnlagenInfo().getUeberwachungsBehoerde()).isEqualTo("12 - ueberw");
        assertThat(dto.getAnlagenInfo().getIedTaetigkeit()).isEqualTo("iedHaupt");
        assertThat(dto.getAnlagenInfo().getPrtrTaetigkeit()).isEqualTo("prtrHaupt");

        assertThat(dto.getAuflagen()).element(0).satisfies(auflageDto -> {
            assertThat(auflageDto.getEmissionswert().getSchluessel()).isEqualTo("em1");
            assertThat(auflageDto.getNachArt14()).isFalse();
            assertThat(auflageDto.getNachArt18()).isFalse();
        });
        assertThat(dto.getAuflagen()).element(1).satisfies(auflageDto -> {
            assertThat(auflageDto.getEmissionswert().getSchluessel()).isEqualTo("em2");
            assertThat(auflageDto.getNachArt14()).isTrue();
            assertThat(auflageDto.getNachArt18()).isTrue();
        });
    }

    @Test
    void toDtoWithAnlagenteil() {
        var genehmZBeh = new ZustaendigeBehoerde(aBehoerde("11").bezeichnung("genehm").build(),
                aZustaendigkeitsReferenz("Gen").schluessel("04").build());
        var ueberwZBeh = new ZustaendigeBehoerde(aBehoerde("12").bezeichnung("ueberw").build(),
                aZustaendigkeitsReferenz("Ueb").schluessel("05").build());

        var r4bv = aVorschrift().art(aVorschriftReferenz("4bv").schluessel(V_R4BV.getSchluessel()).build())
                                .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                        aTaetigkeitsReferenz("bimsch4Haupt").build())
                                .haupttaetigkeit(aTaetigkeitsReferenz("bimsch4Haupt").build())
                                .build();

        var r13bv = aVorschrift().art(aVorschriftReferenz("13bv").schluessel(V_13BV.getSchluessel()).build()).build();

        var ied = aVorschrift().art(aVorschriftReferenz("ied").schluessel(V_IE_RL.getSchluessel()).build())
                               .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                       aTaetigkeitsReferenz("iedHaupt").build())
                               .haupttaetigkeit(aTaetigkeitsReferenz("iedHaupt").build())
                               .build();

        var prtr = aVorschrift().art(aVorschriftReferenz("prtr").schluessel(V_RPRTR.getSchluessel()).build())
                                .taetigkeiten(aTaetigkeitsReferenz("neben").build(),
                                        aTaetigkeitsReferenz("prtrHaupt").build())
                                .haupttaetigkeit(aTaetigkeitsReferenz("prtrHaupt").build())
                                .build();

        var anlagenteil = aFullAnlagenteil().addZustaendigeBehoerde(genehmZBeh)
                                            .addZustaendigeBehoerde(ueberwZBeh)
                                            .vorschriften(r4bv, r13bv, ied, prtr)
                                            .build();
        var berichtsdaten = aFullBerichtsdaten().build();
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().anlagenteil(anlagenteil)
                                            .berichtsdaten(berichtsdaten)
                                            .eSpirsNr("spirs")
                                            .monitor("monitor")
                                            .monitorUrl("monitorUrl")
                                            .report(LIEGT_NICHT_VOR)
                                            .tehgNr("tehg")
                                            .visits(5)
                                            .visitUrl("visitUrl")
                                            .build();

        var mockAnlage = Mockito.mock(Anlage.class);
        when(mockAnlage.hatEURegAnl()).thenReturn(true);
        when(stammdatenService.canWriteAnlage(anlagenteil.getParentAnlageId())).thenReturn(true);
        when(stammdatenService.loadAnlage(anlagenteil.getParentAnlageId())).thenReturn(mockAnlage);

        var dto = mapper.toDto(euRegAnlage);

        assertThat(dto.getId()).isEqualTo(euRegAnlage.getId());
        assertThat(dto.getCanWrite()).isTrue();
        assertThat(dto.geteSpirsNr()).isEqualTo("spirs");
        assertThat(dto.getMonitor()).isEqualTo("monitor");
        assertThat(dto.getMonitorUrl()).isEqualTo("monitorUrl");
        assertThat(dto.getReport()).isEqualTo(EuRegReportEnum.LIEGT_NICHT_VOR);
        assertThat(dto.getTehgNr()).isEqualTo("tehg");
        assertThat(dto.getVisits()).isEqualTo(5);
        assertThat(dto.getVisitUrl()).isEqualTo("visitUrl");

        assertThat(dto.getBerichtsdaten().getAkz()).isEqualTo(berichtsdaten.getAkz());
        assertThat(dto.getBerichtsdaten().getBemerkung()).isEqualTo(berichtsdaten.getBemerkung());
        assertThat(dto.getBerichtsdaten().getZustaendigeBehoerde().getId()).isEqualTo(
                berichtsdaten.getZustaendigeBehoerde().getId());

        assertThat(dto.getAnlagenInfo().getAnlageNr()).isEqualTo(anlagenteil.getAnlagenteilNr());
        assertThat(dto.getAnlagenInfo().getLocalId()).isEqualTo(anlagenteil.getLocalId());
        assertThat(dto.getAnlagenInfo().getName()).isEqualTo(anlagenteil.getName());
        assertThat(dto.getAnlagenInfo().getParentBetriebsstaetteId()).isNull();
        assertThat(dto.getAnlagenInfo().getParentAnlageId()).isEqualTo(anlagenteil.getParentAnlageId());
        assertThat(dto.getAnlagenInfo().getBetriebsstatus()).isEqualTo(anlagenteil.getBetriebsstatus().getKtext());
        assertThat(dto.getAnlagenInfo().getBetriebsstatusSeit()).isEqualTo(anlagenteil.getBetriebsstatusSeit());
        assertThat(dto.getAnlagenInfo().getBimschv4()).isEqualTo("bimsch4Haupt");
        assertThat(dto.getAnlagenInfo().getBimschv13()).isEqualTo(true);
        assertThat(dto.getAnlagenInfo().getBimschv17()).isEqualTo(false);
        assertThat(dto.getAnlagenInfo().getGenehmigungsBehoerde()).isEqualTo("11 - genehm");
        assertThat(dto.getAnlagenInfo().getUeberwachungsBehoerde()).isEqualTo("12 - ueberw");
        assertThat(dto.getAnlagenInfo().getIedTaetigkeit()).isEqualTo("iedHaupt");
        assertThat(dto.getAnlagenInfo().getPrtrTaetigkeit()).isEqualTo("prtrHaupt");
        assertThat(dto.getAnlagenInfo().getParentAnlageHatEuRegistryBericht()).isEqualTo(true);
    }

    @Test
    void fwlBerechnen_Value_0() {
        var anlage = aFullAnlage().build();
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().anlage(anlage).build();
        when(stammdatenService.canWriteBetriebsstaette(anlage.getParentBetriebsstaetteId())).thenReturn(true);

        var dto = mapper.toDto(euRegAnlage);

        assertThat(dto.getAnlagenInfo().getFwl()).isEqualTo(0.0);

    }

    @Test
    void fwlBerechnen() {
        var anlage = aFullAnlage().build();
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().anlage(anlage).build();

        anlage.setLeistungen(
                List.of(new Leistung(1033.12, ReferenzBuilder.aReferenz(Referenzliste.REINH, "MW").ktext("MW").build(),
                        "fwl", Leistungsklasse.INSTALLIERT)));

        when(stammdatenService.canWriteBetriebsstaette(anlage.getParentBetriebsstaetteId())).thenReturn(true);

        var dto = mapper.toDto(euRegAnlage);

        assertThat(dto.getAnlagenInfo().getFwl()).isEqualTo(1033.12);
    }

    @Test
    void fwlBerechnen_prio_klasse() {

        // Es ist die FWL zu nehmen mit der Klasse “genehmigt”,
        // wenn die nicht vorhanden, dann mit der Klasse “installiert” und
        // wenn die nicht vorhanden, dann mit der Klasse “betrieben”.

        var anlage = aFullAnlage().build();
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().anlage(anlage).build();

        anlage.setLeistungen(
                List.of(new Leistung(10.00, ReferenzBuilder.aReferenz(Referenzliste.REINH, "MW").ktext("MW").build(),
                                "fwl", Leistungsklasse.INSTALLIERT),
                        new Leistung(20.00, ReferenzBuilder.aReferenz(Referenzliste.REINH, "MW").ktext("MW").build(),
                                "fwl", Leistungsklasse.GENEHMIGT),
                        new Leistung(30.00, ReferenzBuilder.aReferenz(Referenzliste.REINH, "MW").ktext("MW").build(),
                                "fwl", Leistungsklasse.BETRIEBEN)));

        when(stammdatenService.canWriteBetriebsstaette(anlage.getParentBetriebsstaetteId())).thenReturn(true);

        var dto = mapper.toDto(euRegAnlage);

        assertThat(dto.getAnlagenInfo().getFwl()).isEqualTo(20.00);
    }

}
