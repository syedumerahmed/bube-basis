package de.wps.bube.basis.euregistry.persistence;

import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegAnl;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegF;
import static de.wps.bube.basis.euregistry.domain.vo.AuflageBuilder.aFullAuflage;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RBEA;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RRTYP;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder.anAnlage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.vo.AuflageBuilder;
import de.wps.bube.basis.euregistry.domain.vo.Ausgangszustandsbericht;
import de.wps.bube.basis.euregistry.domain.entity.BVTAusnahme;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.domain.entity.Genehmigung;
import de.wps.bube.basis.euregistry.domain.entity.SpezielleBedingungIE51;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class EURegAnlageRepositoryTest {

    @Autowired
    private EuRegAnlageRepository repository;

    @Autowired
    private ReferenzRepository referenzRepository;

    @Autowired
    private BehoerdenRepository behoerdenRepository;

    @Autowired
    private AnlageRepository anlageRepository;

    private Behoerde behoerde;
    private Referenz berichtsart, bearbeitungsstatus, RBATC, RDERO, RBATE, RSPECC, RRELCH;
    private Anlage anl;

    @BeforeEach
    void setUp() {
        behoerde = behoerdenRepository.save(aBehoerde("BEHD1").build());
        anl = anlageRepository.save(anAnlage().build());
        RBATC = referenzRepository.save(aReferenz(Referenzliste.RBATC, "RBATC").build());
        RDERO = referenzRepository.save(aReferenz(Referenzliste.RDERO, "RDERO").build());
        RBATE = referenzRepository.save(aReferenz(Referenzliste.RBATE, "RBATE").build());
        RSPECC = referenzRepository.save(aReferenz(Referenzliste.RSPECC, "RSPECC").build());
        RRELCH = referenzRepository.save(aReferenz(Referenzliste.RRELCH, "Kapitel IE-RL").build());
        // Berichtsdaten
        berichtsart = referenzRepository.save(aReferenz(RRTYP, "PRTR").build());
        bearbeitungsstatus = referenzRepository.save(aReferenz(RBEA, "00").build());
    }

    @Test
    void testFindById() {
        var anl = getEuRegAnlBuilder().build();
        var anlRead = repository.save(anl);

        assertTrue(repository.findById(anlRead.getId()).isPresent());
    }

    @Test
    void testNotNull() {
        var anl = new EURegAnlage(null, null, null);
        assertThrows(RuntimeException.class, () -> repository.saveAndFlush(anl));
    }

    @Test
    void testCreate() {

        // Setup
        var anl = getEuRegAnlBuilder().build();
        anl.setReport(Ausgangszustandsbericht.LIEGT_VOR);
        anl.setTehgNr("123456");

        // Test
        assertNull(anl.getId());
        var anlRead = repository.saveAndFlush(anl);
        assertNotNull(anlRead.getId());

        assertThat(anl.getAnlage()).isEqualTo(anlRead.getAnlage());
        assertThat(anl.getBerichtsdaten()).isEqualTo(anlRead.getBerichtsdaten());
        assertThat(anl.getErsteErfassung()).isEqualTo(anlRead.getErsteErfassung());
        assertThat(anl.getLetzteAenderung()).isEqualTo(anlRead.getLetzteAenderung());
        assertThat(anl.getReport()).isEqualTo(anlRead.getReport());
        assertThat(anl.getTehgNr()).isEqualTo(anlRead.getTehgNr());
    }

    @Test
    void testCreate_anwendbareBVT() {

        // Setup
        var anl = getEuRegAnlBuilder().build();

        List<Referenz> anwendbareBVT = List.of(RBATC);

        anl.setAnwendbareBvt(anwendbareBVT);

        // Test
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getAnwendbareBvt()).isEqualTo(anlRead.getAnwendbareBvt());
    }

    @Test
    void testCreate_kapitelIERL() {

        // Setup
        var anl = getEuRegAnlBuilder().build();

        List<Referenz> kapitelIERL = List.of(RDERO);

        anl.setKapitelIeRl(kapitelIERL);

        // Test
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getKapitelIeRl()).isEqualTo(anlRead.getKapitelIeRl());
    }

    @Test
    void testCreate_Feuerungsanlage() {

        // Setup
        var anl = getEuRegFBuilder().build();

        anl.setReport(Ausgangszustandsbericht.LIEGT_VOR);
        anl.setTehgNr("123456");

        anl.getFeuerungsanlage().setIeAusnahmen(List.of(RDERO));

        // Test
        assertNull(anl.getId());
        var anlRead = repository.saveAndFlush(anl);
        assertNotNull(anlRead.getId());

        assertThat(anl.getFeuerungsanlage()).isEqualTo(anlRead.getFeuerungsanlage());
    }

    @Test
    void deleteByAnlageId() {
        var anl = getEuRegAnlBuilder().build();
        var anlRead = repository.save(anl);

        assertThat(repository.findById(anlRead.getId())).isPresent();
        long count = repository.deleteByAnlageId(anlRead.getAnlage().getId());
        assertThat(count).isEqualTo(1L);
        assertThat(repository.findById(anlRead.getId())).isEmpty();

    }

    @Test
    void existsByAnlageId() {
        var anl = getEuRegAnlBuilder().build();
        var anlRead = repository.save(anl);

        assertTrue(repository.existsByAnlageId(anlRead.getAnlage().getId()));
        repository.delete(anlRead);
        assertFalse(repository.existsByAnlageId(anlRead.getAnlage().getId()));
    }

    @Test
    void testCreate_BVTAusnahmen() {

        // Setup
        var anl = getEuRegAnlBuilder().build();

        List<BVTAusnahme> ausnahmen = List.of(new BVTAusnahme(RBATE, LocalDate.now(), "oeffentlicheBegruendungUrl"));

        anl.setBvtAusnahmen(ausnahmen);

        // Test
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getBvtAusnahmen()).isEqualTo(anlRead.getBvtAusnahmen());
    }

    @Test
    void testCreate_Genehmigung() {
        // SETUP
        var anl = getEuRegAnlBuilder().build();

        Genehmigung genehmigung = new Genehmigung(true, false, false, LocalDate.now(), null, null, "testURL",
                "testMaßnahme");

        anl.setGenehmigung(genehmigung);

        // TEST
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getGenehmigung()).isEqualTo(anlRead.getGenehmigung());
    }

    @Test
    void testCreate_SpezielleBedingungIE51() {

        // Setup
        var anl = getEuRegFBuilder().build();

        List<SpezielleBedingungIE51> bedingungen = List.of(
                new SpezielleBedingungIE51(RSPECC, "info", "genehmigungUrl"));

        anl.getFeuerungsanlage().setSpezielleBedingungen(bedingungen);

        // Test
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getFeuerungsanlage().getSpezielleBedingungen())
                .isEqualTo(anlRead.getFeuerungsanlage().getSpezielleBedingungen());
    }

    @Test
    void testCreate_Auflagen() {
        // SETUP
        var anl = getEuRegAnlBuilder().build();

        anl.setAuflagen(
                List.of(
                        AuflageBuilder.anAuflage().emissionswert(RBATE).build(),
                        AuflageBuilder.anAuflage()
                                      .emissionswert(RBATE)
                                      .nachArt14(true)
                                      .nachArt18(true)
                                      .build()
                )
        );

        // TEST
        var anlRead = repository.saveAndFlush(anl);
        assertThat(anl.getAuflagen()).isEqualTo(anlRead.getAuflagen());
    }

    private EURegAnlageBuilder getEuRegAnlBuilder() {
        return aFullEURegAnl().anlage(anl).berichtsdaten(getBerichtsdaten())
                              .bvtAusnahmen(new BVTAusnahme(
                                      RBATE,
                                      LocalDate.of(2021, 8, 12),
                                      "https://public.url"))
                              .anwendbareBvt(RBATC)
                              .auflagen(aFullAuflage().emissionswert(RBATE).build())
                              .kapitelIeRl(RRELCH);
    }

    private EURegAnlageBuilder getEuRegFBuilder() {
        return aFullEURegF().anlage(anl).berichtsdaten(getBerichtsdaten())
                            .bvtAusnahmen(new BVTAusnahme(
                                    RBATE,
                                    LocalDate.of(2021, 8, 12),
                                    "https://public.url"))
                            .anwendbareBvt(RBATC)
                            .auflagen(aFullAuflage().emissionswert(RBATE).build())
                            .kapitelIeRl(RRELCH);

    }

    private Berichtsdaten getBerichtsdaten() {
        return BerichtsdatenBuilder.aFullBerichtsdaten()
                                   .berichtsart(berichtsart)
                                   .zustaendigeBehoerde(behoerde)
                                   .bearbeitungsstatus(bearbeitungsstatus)
                                   .build();
    }
}
