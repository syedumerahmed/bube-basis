package de.wps.bube.basis.euregistry.domain.entity;

import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;

import java.time.Instant;
import java.util.List;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;

public class XeurTeilberichtBuilder {

    private Long id;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private Instant freigegebenAt;
    private boolean abgabefaehig;
    private Referenz jahr;
    private Land land;
    private List<String> localIds;
    private String xeurFile;
    private String protokoll;

    public static XeurTeilberichtBuilder aXeurTeilbericht() {
        return new XeurTeilberichtBuilder()
                .land(NORDRHEIN_WESTFALEN)
                .jahr(ReferenzBuilder.aJahrReferenz(2020).build())
                .xeurFile("Testbericht")
                .protokoll("Protokoll")
                .ersteErfassung(Instant.now());
    }

    public XeurTeilberichtBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public XeurTeilberichtBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public XeurTeilberichtBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public XeurTeilberichtBuilder freigegebenAt(Instant freigegebenAt) {
        this.freigegebenAt = freigegebenAt;
        return this;
    }

    public XeurTeilberichtBuilder abgabefaehig(boolean abgabefaehig) {
        this.abgabefaehig = abgabefaehig;
        return this;
    }

    public XeurTeilberichtBuilder jahr(Referenz jahr) {
        this.jahr = jahr;
        return this;
    }

    public XeurTeilberichtBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public XeurTeilberichtBuilder localIds(List<String> localIds) {
        this.localIds = localIds;
        return this;
    }

    public XeurTeilberichtBuilder xeurFile(String xeurFile) {
        this.xeurFile = xeurFile;
        return this;
    }

    public XeurTeilberichtBuilder protokoll(String protokoll) {
        this.protokoll = protokoll;
        return this;
    }

    public XeurTeilbericht build() {
        final XeurTeilbericht tb = new XeurTeilbericht(jahr, land);

        tb.setId(id);
        tb.setErsteErfassung(ersteErfassung);
        tb.setLetzteAenderung(letzteAenderung);
        tb.setFreigegebenAt(freigegebenAt);
        tb.setAbgabefaehig(abgabefaehig);
        tb.setLocalIds(localIds);
        tb.setXeurFile(xeurFile);
        tb.setProtokoll(protokoll);

        return tb;
    }
}
