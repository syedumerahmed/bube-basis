package de.wps.bube.basis;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@AutoConfigureTestDatabase
@ActiveProfiles("unittest")
@ContextConfiguration(classes = BasisApplication.class)
@Tag("INTEGRATION")
class BasisApplicationTest {

    @Autowired
    private ApplicationContext context;

    /**
     * Testet, dass der vollständige Applikationskontext erfolgreich
     * (ohne zyklische Abhängigkeiten etc.) gestartet werden kann
     */
    @Test
    void shouldBootstrap() {
        assertThat(context).isNotNull();
    }
}
