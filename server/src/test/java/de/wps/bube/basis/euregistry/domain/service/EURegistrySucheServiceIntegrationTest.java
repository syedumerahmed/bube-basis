package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NIEDERSACHSEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_READ;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.service.SuchattributePredicateFactory;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = {
        EURegistrySucheService.class,
        EuRegBstBerechtigungsAccessor.class,
        SuchattributePredicateFactory.class })
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.euregistry.persistence",
        "de.wps.bube.basis.stammdaten.persistence",
        "de.wps.bube.basis.komplexpruefung.persistence",
        "de.wps.bube.basis.referenzdaten.persistence" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.euregistry.domain",
        "de.wps.bube.basis.stammdaten.domain",
        "de.wps.bube.basis.komplexpruefung.domain",
        "de.wps.bube.basis.referenzdaten" })
class EURegistrySucheServiceIntegrationTest extends BubeIntegrationTest {

    @Autowired
    EURegistrySucheService service;
    @Autowired
    EURegBetriebsstaetteRepository euRegBstRepository;
    @Autowired
    BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    ReferenzRepository referenzRepository;
    @Autowired
    BehoerdenRepository behoerdenRepository;

    @MockBean
    ReferenzenService referenzenService;
    @MockBean
    StammdatenService stammdatenService;

    private Referenz jahr2020;
    private Behoerde behoerdeBst;
    private Betriebsstaette betriebsstaette2019_02, betriebsstaette2020_02, betriebsstaette2020_03;

    @BeforeEach
    void setUp() {
        Referenz jahr2019 = referenzRepository.save(aJahrReferenz(2019).build());
        jahr2020 = referenzRepository.save(aJahrReferenz(2020).build());
        Referenz inBetrieb = referenzRepository.save(aBetriebsstatusReferenz("in Betrieb").build());
        Referenz agsHH = referenzRepository.save(aGemeindeReferenz("0200000").build());
        behoerdeBst = behoerdenRepository.save(aBehoerde("HHBST").land(HAMBURG).build());
        var betriebsstaetteBuilder = aBetriebsstaette().land(HAMBURG)
                                                       .betriebsstatus(inBetrieb)
                                                       .akz("AKZ")
                                                       .gemeindekennziffer(agsHH)
                                                       .zustaendigeBehoerde(behoerdeBst);
        betriebsstaette2020_02 = betriebsstaetteRepository.save(
                betriebsstaetteBuilder.betriebsstaetteNr("2020_02").berichtsjahr(jahr2020).build());
        betriebsstaette2019_02 = betriebsstaetteRepository.save(
                betriebsstaetteBuilder.betriebsstaetteNr("2019_02").berichtsjahr(jahr2019).build());
        betriebsstaette2020_03 = betriebsstaetteRepository.save(
                betriebsstaetteBuilder.betriebsstaetteNr("2020_03").berichtsjahr(jahr2020).land(NIEDERSACHSEN).build());

    }

    @Nested
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    class WithEmptyRepository {

        @Test
        void shouldFindNone() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte).isEmpty();
        }

        @Test
        void shouldFindNoId() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).isEmpty();
        }
    }

    @Nested
    class WithBerichte {

        private EURegBetriebsstaette bericht2020_02, bericht2020_03;

        @BeforeEach
        void setUp() {
            var berichtsart = referenzRepository.save(ReferenzBuilder.aReferenz(Referenzliste.RRTYP, "ART01").build());
            var bearbeitungsstatus = referenzRepository.save(
                    ReferenzBuilder.aReferenz(Referenzliste.RBEA, "BS01").build());
            var behoerdeBericht = behoerdenRepository.save(aBehoerde("HHEUREG").land(HAMBURG).build());
            var berichtsdaten = BerichtsdatenBuilder.aFullBerichtsdaten()
                                                    .zustaendigeBehoerde(behoerdeBericht)
                                                    .akz("BERICHT_AKZ")
                                                    .berichtsart(berichtsart)
                                                    .bearbeitungsstatus(bearbeitungsstatus);
            bericht2020_02 = euRegBstRepository.save(aEURegBetriebsstaette().betriebsstaette(betriebsstaette2020_02)
                                                                            .berichtsdaten(berichtsdaten.build())
                                                                            .build());
            bericht2020_03 = euRegBstRepository.save(aEURegBetriebsstaette().betriebsstaette(betriebsstaette2020_03)
                                                                            .berichtsdaten(berichtsdaten.build())
                                                                            .build());
            euRegBstRepository.save(aEURegBetriebsstaette().betriebsstaette(betriebsstaette2019_02)
                                                           .berichtsdaten(berichtsdaten.zustaendigeBehoerde(behoerdeBst)
                                                                                       .build())
                                                           .build());
        }

        @Test
        @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
        void shouldFindAllAsGesamtadmin() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());
            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02", "2020_03");
        }

        @Test
        @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
        void shouldFindIdsAsGesamtadmin() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId(), bericht2020_03.getId());
        }

        @Test
        @WithBubeMockUser(roles = LAND_READ, land = HAMBURG)
        void shouldFindOfLandOnlyAsLandUser() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = LAND_READ, land = HAMBURG)
        void shouldFindIdAsLandUser() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = { "HHEUREG", "ANOTHER" })
        void shouldFindBerichtAsBehoerdeUserOfBericht() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = { "HHEUREG", "ANOTHER" })
        void shouldFindIdAsBehoerdeUserOfBericht() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = { "BERICHT_AKZ", "ANOTHER" })
        void shouldFindBerichtAsBehoerdeUserWithAkzOfBericht() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = { "BERICHT_AKZ", "ANOTHER" })
        void shouldFindIdAsBehoerdeUserWithAkzOfBericht() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = { "HHBST", "ANOTHER" })
        void shouldFindBerichtAsBehoerdeUserOfBetriebsstaette() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = { "HHBST", "ANOTHER" })
        void shouldFindIdAsBehoerdeUserOfBetriebsstaette() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = "HHUNRELATED")
        void shouldNotFindAnyBerichtAsBehoerdeUserOfUnrelatedBehoerde() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, behoerden = "HHUNRELATED")
        void shouldNotFindAnyIdAsBehoerdeUserOfUnrelatedBehoerde() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = "UNRELATED")
        void shouldNotFindAnyBerichtAsBehoerdeUserOfUnrelatedAkz() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = "UNRELATED")
        void shouldNotFindAnyIdAsBehoerdeUserOfUnrelatedAkz() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = { "UNRELATED", "AKZ" })
        void shouldFindBerichtAsBehoerdeUserOfBetriebsstaetteAkz() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, akz = { "UNRELATED", "AKZ" })
        void shouldFindIdAsBehoerdeUserOfBetriebsstaetteAkz() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, verwaltungsgebiete = { "UNRELATED", "02" })
        void shouldFindBerichtAsBehoerdeUserOfBetriebsstaettegemeinde() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, verwaltungsgebiete = { "UNRELATED", "02" })
        void shouldFindIdAsBehoerdeUserOfBetriebsstaettegemeinde() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, verwaltungsgebiete = { "UNRELATED" })
        void shouldNotFindAnyBerichtAsBehoerdeUserOfWrongVerwaltungsgebiet() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, verwaltungsgebiete = { "UNRELATED" })
        void shouldNotFindAnyIdAsBehoerdeUserOfWrongVerwaltungsgebiet() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, betriebsstaetten = { "2020_02" })
        void shouldFindBerichtAsBehoerdeUserOfBetriebsstaetteNr() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte.map(i -> i.betriebsstaetteNr)).containsExactlyInAnyOrder("2020_02");
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, betriebsstaetten = { "2020_02" })
        void shouldFindIdAsBehoerdeUserOfBetriebsstaetteNr() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).containsExactlyInAnyOrder(bericht2020_02.getId());
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, betriebsstaetten = { "0200" })
        void shouldNotFindAnyBerichtAsBehoerdeUserOfWrongBetriebsstaetteNr() {
            var berichte = service.readAllEuRegBetriebsstaette(jahr2020.getId(), null, Pageable.unpaged());

            assertThat(berichte).isEmpty();
        }

        @Test
        @WithBubeMockUser(roles = BEHOERDE_READ, land = HAMBURG, betriebsstaetten = { "0200" })
        void shouldNotFindAnyIdAsBehoerdeUserOfWrongBetriebsstaetteNr() {
            var ids = service.readAllEuRegBetriebsstaetteIds(jahr2020.getId(), null);

            assertThat(ids).isEmpty();
        }

    }

}
