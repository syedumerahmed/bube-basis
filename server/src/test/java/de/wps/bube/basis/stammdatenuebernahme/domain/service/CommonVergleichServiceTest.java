package de.wps.bube.basis.stammdatenuebernahme.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.LeistungVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.ReferenzVector;
import de.wps.bube.basis.stammdatenuebernahme.domain.model.StringVector;

class CommonVergleichServiceTest {
    private CommonVergleichService vergleichService;

    @BeforeEach
    void setUp() {
        this.vergleichService = new CommonVergleichService();
    }

    @Test
    void mapLeistungen() {
        List<Leistung> splLeistungen = new ArrayList<>();
        List<Leistung> spbLeistungen = new ArrayList<>();

        Referenz leistungseinheit = ReferenzBuilder.aLeistungseinheitReferenz("test").build();
        Leistung gleich = new Leistung(10D, leistungseinheit, "test", Leistungsklasse.BETRIEBEN);

        Leistung spl = new Leistung(20D, leistungseinheit, "test", Leistungsklasse.BETRIEBEN);
        Leistung spb = new Leistung(30D, leistungseinheit, "test", Leistungsklasse.BETRIEBEN);

        splLeistungen.add(gleich);
        splLeistungen.add(spl);

        spbLeistungen.add(gleich);
        spbLeistungen.add(spb);

        List<LeistungVector> leistungMaps = this.vergleichService.mapLeistungen(splLeistungen, spbLeistungen);

        assertThat(leistungMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(gleich);
            assertThat(vector.getSpl()).isEqualTo(gleich);
            assertThat(vector.isEqual()).isTrue();
        });
        assertThat(leistungMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(spb);
            assertThat(vector.getSpl()).isEqualTo(null);
            assertThat(vector.isEqual()).isFalse();
        });
        assertThat(leistungMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(null);
            assertThat(vector.getSpl()).isEqualTo(spl);
            assertThat(vector.isEqual()).isFalse();
        });
    }

    @Test
    void mapReferenzListen() {
        List<Referenz> splReferenzen = new ArrayList<>();
        List<Referenz> spbReferenzen = new ArrayList<>();

        Referenz gleich = ReferenzBuilder.aVorschriftReferenz("gleich").id(1L).build();
        Referenz spb = ReferenzBuilder.aVorschriftReferenz("spb").id(2L).build();
        Referenz spl = ReferenzBuilder.aVorschriftReferenz("spl").id(3L).build();

        splReferenzen.add(gleich);
        splReferenzen.add(spl);

        spbReferenzen.add(gleich);
        spbReferenzen.add(spb);

        List<ReferenzVector> referenzMaps = this.vergleichService.mapReferenzListen(splReferenzen, spbReferenzen);

        assertThat(referenzMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(gleich);
            assertThat(vector.getSpl()).isEqualTo(gleich);
            assertThat(vector.isEqual()).isTrue();
        });
        assertThat(referenzMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(spb);
            assertThat(vector.getSpl()).isEqualTo(null);
            assertThat(vector.isEqual()).isFalse();
        });
        assertThat(referenzMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(null);
            assertThat(vector.getSpl()).isEqualTo(spl);
            assertThat(vector.isEqual()).isFalse();
        });
    }

    @Test
    void vergleicheStringLists() {
        List<String> splString = new ArrayList<>();
        List<String> spbString = new ArrayList<>();

        String gleich = "gleich";
        String spb = "spb";
        String spl = "spl";

        splString.add(gleich);
        splString.add(spl);

        spbString.add(gleich);
        spbString.add(spb);

        List<StringVector> stringMaps = this.vergleichService.vergleicheStringLists(splString, spbString);

        assertThat(stringMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(gleich);
            assertThat(vector.getSpl()).isEqualTo(gleich);
            assertThat(vector.isEqual()).isTrue();
        });
        assertThat(stringMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(spb);
            assertThat(vector.getSpl()).isEqualTo(null);
            assertThat(vector.isEqual()).isFalse();
        });
        assertThat(stringMaps).anySatisfy(vector -> {
            assertThat(vector.getSpb()).isEqualTo(null);
            assertThat(vector.getSpl()).isEqualTo(spl);
            assertThat(vector.isEqual()).isFalse();
        });

    }
}
