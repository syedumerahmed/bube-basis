package de.wps.bube.basis.euregistry.xml.bericht;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.xml.bericht.dto.gml.FeatureMember;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;

class EURegGMLWriterProtokollTest extends AbstractEURegTest {


    @Test
    void testProtokoll() {

        Betriebsstaette bst1 = BetriebsstaetteBuilder.aFullBetriebsstaette().id(1L).betriebsstaetteNr("1").name("BST1").localId("bst1").build();
        Betriebsstaette bst2 = BetriebsstaetteBuilder.aFullBetriebsstaette().id(2L).betriebsstaetteNr("2").name("BST2").localId("bst2").build();

        Anlage anl3 = AnlageBuilder.aFullAnlage().id(3L).anlageNr("3").name("Anl3").localId("anl3").build();
        Anlage anl4 = AnlageBuilder.aFullAnlage().id(4L).anlageNr("4").name("Anl4").localId("anl4").build();
        List<String> anlageLocalIds = List.of(anl3.getLocalId(), anl4.getLocalId());

        AtomicLong productionSiteId = new AtomicLong(1000);
        EURegMapper.EURegMappingContext context = new EURegMapper.EURegMappingContext(Land.HAMBURG,
                productionSiteId::incrementAndGet, getReportData(), getInspireParam(), getUbaProperties(),
                anlageLocalIds);

        EURegBetriebsstaette bstBericht5 = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(5L).betriebsstaette(bst1).build();
        EURegBetriebsstaette bstBericht6 = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(6L).betriebsstaette(bst2).build();

        EURegAnlage anlBericht7 = EURegAnlageBuilder.aFullEURegAnl().id(7L).anlage(anl3).build();
        EURegAnlage anlBericht8 = EURegAnlageBuilder.aFullEURegF().id(8L).anlage(anl4).build();

        EURegGMLWriterProtokoll protokoll = new EURegGMLWriterProtokoll();

        protokoll.start(bstBericht5);
        protokoll.log(new FeatureMember(getProductionSiteMapper().toXDto(bstBericht5, context)));
        protokoll.log(new FeatureMember(getProductionFacilityMapper().toXDto(bstBericht5, context)));
        protokoll.addMappingError("First mapping for BST1 failed");
        protokoll.addMappingError("Second mapping for BST1 failed");
        protokoll.start(anlBericht7);
        protokoll.log(new FeatureMember(getProductionInstallationMapper().toXDto(anlBericht7, context)));
        protokoll.start(bstBericht6);
        protokoll.log(new FeatureMember(getProductionSiteMapper().toXDto(bstBericht6, context)));
        protokoll.log(new FeatureMember(getProductionFacilityMapper().toXDto(bstBericht6, context)));
        protokoll.start(anlBericht8);
        protokoll.log(new FeatureMember(getProductionInstallationMapper().toXDto(anlBericht8, context)));
        protokoll.log(new FeatureMember(getProductionInstallationPartMapper().toXDto(anlBericht8, context)));
        protokoll.addValidationError("First validation for Anl4 failed");
        protokoll.addValidationError("Second validation for Anl4 failed");

        String expectedDisplay = """
                Betriebsstätte 1 | BST1 | bst1 | bst1.uba.de
                Anlage 3 | Anl3 | anl3-ext
                
                Betriebsstätte 2 | BST2 | bst2 | bst2.uba.de
                Anlage 4 | Anl4 | anl4-ext | anl4-ext
                
                """;
        String expectedMappingErrors = """
                Betriebsstätte 1 | BST1 | bst1 | bst1.uba.de : Second mapping for BST1 failed
                
                """;
        String expectedValidationErrors = """
                Betriebsstätte 2 | BST2 | bst2 | bst2.uba.de :
                  Anlage 4 | Anl4 | anl4-ext | anl4-ext :
                      First validation for Anl4 failed
                      Second validation for Anl4 failed
                
                """;

        assertThat(protokoll.display(new StringWriter())).hasToString(expectedDisplay);
        assertThat(protokoll.displayMappingErrors(new StringWriter())).hasToString(expectedMappingErrors);
        assertThat(protokoll.displayValidationErrors(new StringWriter())).hasToString(expectedValidationErrors);

    }

}
