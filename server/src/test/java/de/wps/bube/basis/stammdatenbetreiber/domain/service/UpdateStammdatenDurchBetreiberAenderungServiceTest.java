package de.wps.bube.basis.stammdatenbetreiber.domain.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlage;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbAnlagenteil;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetreiber;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbBetriebsstaette;
import de.wps.bube.basis.stammdatenbetreiber.domain.entity.SpbQuelle;

@ExtendWith(MockitoExtension.class)
class UpdateStammdatenDurchBetreiberAenderungServiceTest {

    public static final Betriebsstaette BETRIEBSSTAETTE = BetriebsstaetteBuilder.aFullBetriebsstaette().build();

    private UpdateStammdatenDurchBetreiberAenderungService aenderungService;

    @Mock
    private StammdatenService stammdatenService;

    @BeforeEach
    void setUp() {
        aenderungService = new UpdateStammdatenDurchBetreiberAenderungService(stammdatenService);
    }

    @Test
    void updateAenderungDurchBetreiberSpbBetriebsstaette() {
        SpbBetriebsstaette spbBetriebsstaette = new SpbBetriebsstaette(BETRIEBSSTAETTE);

        this.aenderungService.updateAenderungDurchBetreiber(spbBetriebsstaette);

        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
    }

    @Test
    void updateAenderungDurchBetreiberSpbAnlage_mitAnlage() {
        Anlage anlage = AnlageBuilder.aFullAnlage().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();
        SpbAnlage spbAnlage = new SpbAnlage(anlage);

        this.aenderungService.updateAenderungDurchBetreiber(spbAnlage);

        verify(stammdatenService).aenderungAnlageDurchBetreiber(eq(anlage.getId()));
        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
    }

    @Test
    void updateAenderungDurchBetreiberSpbAnlage_ohneAnlage() {
        Anlage anlage = AnlageBuilder.anAnlage().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();
        SpbAnlage spbAnlage = new SpbAnlage(anlage);

        this.aenderungService.updateAenderungDurchBetreiber(spbAnlage);

        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
        verifyNoMoreInteractions(stammdatenService);
    }

    @Test
    void updateAenderungDurchBetreiberSpbAnlagenteil_mitAnlageUndAnlagenteil() {
        Anlage anlage = AnlageBuilder.aFullAnlage().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();
        Anlagenteil anlagenteil = AnlagenteilBuilder.aFullAnlagenteil().parentAnlageId(anlage.getId()).build();
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(anlagenteil);

        this.aenderungService.updateAenderungDurchBetreiber(spbAnlagenteil, BETRIEBSSTAETTE.getId());

        verify(stammdatenService).aenderungAnlagenteilDurchBetreiber(eq(anlagenteil.getId()));
        verify(stammdatenService).aenderungAnlageDurchBetreiber(eq(anlage.getId()));
        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
    }

    @Test
    void updateAenderungDurchBetreiberSpbAnlagenteil_ohneAnlageUndAnlagenteil() {
        Anlagenteil anlagenteil = AnlagenteilBuilder.anAnlagenteil().build();
        SpbAnlagenteil spbAnlagenteil = new SpbAnlagenteil(anlagenteil);

        this.aenderungService.updateAenderungDurchBetreiber(spbAnlagenteil, BETRIEBSSTAETTE.getId());

        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
        verifyNoMoreInteractions(stammdatenService);
    }

    @Test
    void updateAenderungDurchBetreiberSpbQuelle_mitAnlageUndQuelle() {
        Anlage anlage = AnlageBuilder.aFullAnlage().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();
        Quelle quelle = QuelleBuilder.aFullQuelle()
                                     .parentAnlageId(anlage.getId())
                                     .parentBetriebsstaetteId(BETRIEBSSTAETTE.getId())
                                     .build();

        SpbQuelle spbQuelle = new SpbQuelle(quelle);

        this.aenderungService.updateAenderungDurchBetreiber(spbQuelle);

        verify(stammdatenService).aenderungQuelleDurchBetreiber(eq(quelle.getId()));
        verify(stammdatenService).aenderungAnlageDurchBetreiber(eq(anlage.getId()));
        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
    }

    @Test
    void updateAenderungDurchBetreiberSpbQuelle_ohneAnlageUndQuelle() {
        Quelle quelle = QuelleBuilder.aQuelle().parentBetriebsstaetteId(BETRIEBSSTAETTE.getId()).build();

        SpbQuelle spbQuelle = new SpbQuelle(quelle);

        this.aenderungService.updateAenderungDurchBetreiber(spbQuelle);

        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
        verifyNoMoreInteractions(stammdatenService);
    }

    @Test
    void updateAenderungDurchBetreiberSpbBetreiber() {
        Betreiber betreiber = BetreiberBuilder.aFullBetreiber().build();
        SpbBetreiber spbBetreiber = new SpbBetreiber(betreiber);

        aenderungService.updateAenderungDurchBetreiber(spbBetreiber, BETRIEBSSTAETTE.getId());

        verify(stammdatenService).aenderungBetreiberDurchBetreiber(eq(betreiber.getId()));
        verify(stammdatenService).aenderungBetriebsstaetteDurchBetreiber(eq(BETRIEBSSTAETTE.getId()));
    }
}
