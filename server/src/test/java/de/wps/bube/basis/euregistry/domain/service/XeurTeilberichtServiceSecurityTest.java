package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.base.sftp.SftpService;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtErstellenCommand;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilberichtBuilder;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelService;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = { XeurTeilberichtCrudService.class, XeurTeilberichtService.class })
class XeurTeilberichtServiceSecurityTest extends SecurityTest {

    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private XeurTeilberichtRepository xeurTeilberichtRepository;
    @MockBean
    private EURegistryService euRegistryService;
    @MockBean
    private BerichtPruefungService berichtPruefungService;
    @MockBean
    private RegelPruefungService regelPruefungService;
    @MockBean
    private RegelService regelService;
    @MockBean
    private RegelRepository regelRepository;
    @MockBean
    private KomplexpruefungService komplexpruefungService;
    @MockBean
    private EURegGMLService gmlService;
    @MockBean
    private ParameterService parameterService;
    @MockBean
    private EmailService emailService;
    @MockBean
    private SftpService sftpService;
    @MockBean
    private BenutzerverwaltungService benutzerverwaltungService;

    @Autowired
    private XeurTeilberichtCrudService crudService;
    @Autowired
    private XeurTeilberichtService service;

    @Test
    @WithBubeMockUser(roles = { BEHOERDE_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void erstelleXeurTeilbericht_accessDenied_wrongRechte() {
        List<Long> ids = Collections.singletonList(1L);
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        var command = new XeurTeilberichtErstellenCommand(ids, jahr, NORDRHEIN_WESTFALEN);
        assertThrows(AccessDeniedException.class, () -> service.erstelleXeurTeilbericht(command));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_WRITE }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void erstelleXeurTeilbericht_shouldBeOk_LandWrite() {
        List<Long> ids = Collections.singletonList(1L);
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();

        when(this.euRegistryService.getBetriebsstaetteIdsForBerichtIds(any())).thenReturn(List.of(1L));
        var command = new XeurTeilberichtErstellenCommand(ids, jahr, NORDRHEIN_WESTFALEN);
        when(stammdatenService.canWriteAllBetriebsstaetten(any())).thenReturn(true);
        assertDoesNotThrow(() -> service.erstelleXeurTeilbericht(command));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDE_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void getAllTeilberichte_accessDenied_wrongRechte() {
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        assertThrows(AccessDeniedException.class, () -> crudService.getAllTeilberichte(jahr));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void getAllTeilberichte_shouldBeOk_LandRead() {
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        assertDoesNotThrow(() -> crudService.getAllTeilberichte(jahr));
    }

    @Test
    @WithBubeMockUser(roles = { ADMIN }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void getAllTeilberichte_shouldBeOk_Admin() {
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        assertDoesNotThrow(() -> crudService.getAllTeilberichte(jahr));
    }

    @Test
    @WithBubeMockUser(roles = { GESAMTADMIN }, land = DEUTSCHLAND, behoerden = "BEHOERDE")
    void getAllTeilberichte_shouldBeOk_GesamtAdmin() {
        Referenz jahr = ReferenzBuilder.aJahrReferenz(2020).build();
        assertDoesNotThrow(() -> crudService.getAllTeilberichte(jahr));
    }

    @Test
    @WithBubeMockUser(roles = { BEHOERDE_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void downloadTeilbericht_accessDenied_BehoerdeRead() {
        assertThrows(AccessDeniedException.class, () -> crudService.downloadTeilbericht(null, 1L));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void downloadTeilbericht_shouldBeOk_LandRead() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(NORDRHEIN_WESTFALEN)
                                                                .build();
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);
        assertDoesNotThrow(() -> crudService.downloadTeilbericht(new ByteArrayOutputStream(), xeurTeilbericht.getId()));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void downloadTeilbericht_accessDenied_WrongLand() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(HAMBURG)
                                                                .build();
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);
        assertThrows(AccessDeniedException.class,
                () -> crudService.downloadTeilbericht(new ByteArrayOutputStream(), xeurTeilbericht.getId()));
    }

    @Test
    @WithBubeMockUser(roles = { GESAMTADMIN }, land = DEUTSCHLAND, behoerden = "BEHOERDE")
    void downloadTeilbericht_shouldBeOk_Gesamtadmin() {
        XeurTeilbericht xeurTeilbericht = XeurTeilberichtBuilder.aXeurTeilbericht()
                                                                .id(1L)
                                                                .land(HAMBURG)
                                                                .build();
        when(xeurTeilberichtRepository.getById(xeurTeilbericht.getId())).thenReturn(xeurTeilbericht);
        assertDoesNotThrow(() -> crudService.downloadTeilbericht(new ByteArrayOutputStream(), xeurTeilbericht.getId()));
    }

    @Test
    @WithBubeMockUser(roles = { LAND_READ }, land = NORDRHEIN_WESTFALEN, behoerden = "BEHOERDE")
    void teilberichtFreigeben_accessDenied_LandRead() {
        assertThrows(AccessDeniedException.class, () -> service.teilberichtFreigeben(1L));
    }

}
