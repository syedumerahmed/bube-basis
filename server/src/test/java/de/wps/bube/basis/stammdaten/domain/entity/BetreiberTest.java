package de.wps.bube.basis.stammdaten.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class BetreiberTest {

    @Test
    void testEquals() {
        final var betreiber = new Betreiber(1L, "name");
        betreiber.setId(1L);
        final var betreiber2 = new Betreiber(2L, "name");
        betreiber2.setId(0L);

        assertEquals(betreiber, betreiber);
        assertNotEquals(betreiber, betreiber2);
    }
}
