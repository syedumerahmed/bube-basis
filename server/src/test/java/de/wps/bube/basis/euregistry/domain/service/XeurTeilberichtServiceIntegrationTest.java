package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NORDRHEIN_WESTFALEN;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegAnl;
import static de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder.aFullEURegF;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.INSP_EXTENSION;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.INSP_NS;
import static de.wps.bube.basis.referenzdaten.domain.entity.Parameter.INSP_SITE;
import static de.wps.bube.basis.referenzdaten.domain.entity.ParameterWertBuilder.aParameterWert;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.ADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDE_WRITE;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.GESAMTADMIN;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.LAND_READ;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aFullBetriebsstaette;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResourceURL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.access.AccessDeniedException;
import org.xmlunit.builder.Input;
import org.xmlunit.validation.JAXPValidator;
import org.xmlunit.validation.Languages;

import de.wps.bube.basis.base.email.EmailService;
import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.base.sftp.SftpService;
import de.wps.bube.basis.benutzerverwaltung.domain.service.BenutzerverwaltungService;
import de.wps.bube.basis.euregistry.XeurProperties;
import de.wps.bube.basis.euregistry.domain.XeurTeilberichtErstellenCommand;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.persistence.XeurTeilberichtRepository;
import de.wps.bube.basis.euregistry.xml.bericht.AddressMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.CommonEURegMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.EURegMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.ProductionFacilityMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.ProductionInstallationMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.ProductionInstallationPartMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.ProductionSiteMapperImpl;
import de.wps.bube.basis.euregistry.xml.bericht.ReportDataMapperImpl;
import de.wps.bube.basis.jobs.domain.JobContext;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelService;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.persistence.BehoerdenRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.security.BehoerdeBerechtigungsAccessor;
import de.wps.bube.basis.referenzdaten.security.EigeneBehoerdenBerechtigungsAcessor;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = {
        XeurTeilberichtCrudService.class,
        XeurTeilberichtService.class,
        ReferenzenService.class,
        BehoerdenService.class,
        BehoerdeBerechtigungsAccessor.class,
        EigeneBehoerdenBerechtigungsAcessor.class,
        EURegGMLService.class,
        EURegMapperImpl.class,
        ProductionSiteMapperImpl.class,
        ProductionFacilityMapperImpl.class,
        ProductionInstallationMapperImpl.class,
        ProductionInstallationPartMapperImpl.class,
        ReportDataMapperImpl.class,
        AddressMapperImpl.class,
        CommonEURegMapperImpl.class
})
@EnableJpaRepositories(value = {
        "de.wps.bube.basis.euregistry.persistence",
        "de.wps.bube.basis.stammdaten.persistence",
        "de.wps.bube.basis.komplexpruefung.persistence",
        "de.wps.bube.basis.referenzdaten.persistence"
}, repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({
        "de.wps.bube.basis.euregistry.domain",
        "de.wps.bube.basis.stammdaten.domain",
        "de.wps.bube.basis.komplexpruefung.domain",
        "de.wps.bube.basis.referenzdaten" })
@EnableConfigurationProperties(XeurProperties.class)
@ExtendWith(MockitoExtension.class)
class XeurTeilberichtServiceIntegrationTest extends BubeIntegrationTest {

    @Autowired
    XeurTeilberichtRepository xeurTeilberichtRepository;
    @Autowired
    XeurTeilberichtCrudService crudService;
    @Autowired
    XeurTeilberichtService service;
    @Autowired
    BetriebsstaetteRepository betriebsstaetteRepository;
    @Autowired
    BehoerdenRepository behoerdenRepository;
    @Autowired
    ReferenzRepository referenzRepository;
    @Autowired
    EURegGMLService euRegGMLService;

    @MockBean
    StammdatenService stammdatenService;
    @MockBean
    EURegistryService euRegistryService;
    @MockBean
    BerichtPruefungService berichtPruefungService;
    @MockBean
    RegelPruefungService regelPruefungService;
    @MockBean
    RegelService regelService;
    @MockBean
    RegelRepository regelRepository;
    @MockBean
    KomplexpruefungService komplexpruefungService;
    @MockBean
    ParameterService parameterService;
    @MockBean
    KoordinatenTransformationService koordinatenTransformationService;
    @MockBean
    EmailService emailService;
    @MockBean
    SftpService sftpService;
    @MockBean
    BenutzerverwaltungService benutzerverwaltungService;
    @MockBean
    ReferenzenBerechtigungsContext referenzenBerechtigungsContext;

    private Referenz jahr2020;

    @BeforeEach
    void setUp() {
        jahr2020 = referenzRepository.save(aJahrReferenz(2020).build());

        Referenz betriebsStatus = referenzRepository.save(aBetriebsstatusReferenz("in Betrieb").build());
        Referenz agsHH = referenzRepository.save(aGemeindeReferenz("0200000").build());
        Behoerde behoerdeBst = behoerdenRepository.save(aBehoerde("HHBST").land(HAMBURG).build());
        var betriebsstaetteBuilder = aBetriebsstaette().land(HAMBURG)
                                                       .betriebsstatus(betriebsStatus)
                                                       .akz("AKZ")
                                                       .gemeindekennziffer(agsHH)
                                                       .zustaendigeBehoerde(behoerdeBst);
        betriebsstaetteRepository.save(
                betriebsstaetteBuilder.betriebsstaetteNr("2020_02").berichtsjahr(jahr2020).build());
        when(koordinatenTransformationService.transformToEPSG4258(any())).thenReturn(
                new double[] { 53.55504, 9.99575 });

    }

    @Test
    @WithBubeMockUser(roles = LAND_READ, land = HAMBURG)
    void getAllTeilberichte_Landesuser() {
        // Arrange
        XeurTeilbericht xeurTeilberichtHH = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, HAMBURG));
        XeurTeilbericht xeurTeilberichtNRW = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, NORDRHEIN_WESTFALEN));

        // Act
        List<XeurTeilbericht> allTeilberichte = crudService.getAllTeilberichte(jahr2020);

        // Assert

        assertTrue(allTeilberichte.contains(xeurTeilberichtHH));
        assertFalse(allTeilberichte.contains(xeurTeilberichtNRW));
    }

    @Test
    @WithBubeMockUser(roles = ADMIN, land = HAMBURG)
    void getAllTeilberichte_Landesadmin() {
        // Arrange
        XeurTeilbericht xeurTeilberichtHH = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, HAMBURG));
        XeurTeilbericht xeurTeilberichtNRW = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, NORDRHEIN_WESTFALEN));

        // Act
        List<XeurTeilbericht> allTeilberichte = crudService.getAllTeilberichte(jahr2020);

        // Assert
        assertTrue(allTeilberichte.contains(xeurTeilberichtHH));
        assertFalse(allTeilberichte.contains(xeurTeilberichtNRW));
    }

    @Test
    @WithBubeMockUser(roles = GESAMTADMIN, land = DEUTSCHLAND)
    void getAllTeilberichte_Gesamtadmin() {
        // Arrange
        XeurTeilbericht xeurTeilberichtHH = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, HAMBURG));
        XeurTeilbericht xeurTeilberichtNRW = xeurTeilberichtRepository.save(
                new XeurTeilbericht(jahr2020, NORDRHEIN_WESTFALEN));

        // Act
        List<XeurTeilbericht> allTeilberichte = crudService.getAllTeilberichte(jahr2020);

        // Assert

        assertTrue(allTeilberichte.contains(xeurTeilberichtHH));
        assertTrue(allTeilberichte.contains(xeurTeilberichtNRW));
    }

    @Test
    @WithBubeMockUser(roles = BEHOERDE_WRITE, land = HAMBURG)
    void erstelleXeurTeilbericht_keineBerechtigung() {
        var command = new XeurTeilberichtErstellenCommand(List.of(1L, 2L), jahr2020, HAMBURG);
        doThrow(AccessDeniedException.class)
                .when(euRegistryService)
                .checkDatenberechtigungForBerichtIds(command.berichtsIds, command.land, command.berichtsjahr);

        assertThrows(AccessDeniedException.class, () -> service.erstelleXeurTeilbericht(command));
    }

    @Test
    @WithBubeMockUser(roles = ADMIN, land = HAMBURG)
    void erstellXeurTeilbericht(@Mock Consumer<EURegBetriebsstaette> elementConsumer)
            throws IOException, URISyntaxException {
        // Arrange
        var expectedXML = Input.fromStream(getResource("xml/eureg/xeur-teilbericht.xml"));
        var expectedProtokollPrefix = Pattern.compile(
                "LAND-XEUR-Teilbericht Protokoll  Land: HAMBURG  Zieljahr: 2020  Start: \\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}");
        var expectedProtokollSuffix = Files.readString(
                Path.of(getResourceURL("xml/eureg/xeur-teilbericht-protokoll.txt").toURI()));
        var berichtIds = List.of(1L, 2L);
        JobProtokoll jobProtokoll = new JobProtokoll("test");
        var bericht1 = aEURegBetriebsstaette().id(1L)
                                              .betriebsstaette(aFullBetriebsstaette()
                                                      .berichtsjahr(jahr2020)
                                                      .localId("Local-ID")
                                                      .land(HAMBURG)
                                                      .build())
                                              .build();
        var bericht2 = aEURegBetriebsstaette().id(2L)
                                              .betriebsstaette(aFullBetriebsstaette()
                                                      .berichtsjahr(jahr2020)
                                                      .localId("Local-ID2")
                                                      .land(HAMBURG)
                                                      .build())
                                              .build();
        when(euRegistryService.streamAllByBerichtIdIn(berichtIds))
                .thenAnswer(invocation -> Stream.of(bericht1, bericht2));

        when(stammdatenService.canWriteBetriebsstaette(any())).thenReturn(true);
        when(parameterService.readParameterWert(INSP_NS, Gueltigkeitsjahr.of(jahr2020.getSchluessel()), HAMBURG))
                .thenReturn(aParameterWert().wert("namespace").build());
        when(parameterService.readParameterWert(INSP_SITE, Gueltigkeitsjahr.of(jahr2020.getSchluessel()), HAMBURG))
                .thenReturn(aParameterWert().wert("site").build());
        when(parameterService.findParameterWert(INSP_EXTENSION, Gueltigkeitsjahr.of(jahr2020.getSchluessel()), HAMBURG))
                .thenReturn(Optional.of(aParameterWert().wert("extension").build()));
        when(euRegistryService.findAllAnlagenberichteForBstBericht(bericht1)).thenReturn(
                Stream.of(aFullEURegAnl().build(), aFullEURegF().build()));

        // Act
        service.erstellXeurTeilbericht(berichtIds, jahr2020, HAMBURG, new JobContext<>(jobProtokoll, elementConsumer));

        // Assert
        var teilberichte2020 = crudService.getAllTeilberichte(jahr2020);
        assertThat(teilberichte2020).hasSize(1);

        var teilbericht = teilberichte2020.get(0);
        assertThat(teilbericht.getLocalIds()).containsExactlyInAnyOrder(bericht1.getLocalId(), bericht2.getLocalId());
        assertThat(teilbericht.getJahr()).isEqualTo(jahr2020);
        assertThat(teilbericht.getJahr()).isEqualTo(jahr2020);

        MatcherAssert.assertThat(Input.fromString(teilbericht.getXeurFile()),
                isSimilarTo(expectedXML).ignoreElementContentWhitespace());
        String[] protokoll = teilbericht.getProtokoll().split("\n", 2);
        assertThat(protokoll[0]).matches(expectedProtokollPrefix);
        assertThat(protokoll[1]).isEqualToIgnoringNewLines(expectedProtokollSuffix);
        var schemaValidator = new JAXPValidator(Languages.W3C_XML_SCHEMA_NS_URI);
        schemaValidator.setSchema(euRegGMLService.getSchema());
        schemaValidator.validateSchema();
        schemaValidator.validateInstance(Input.fromString(teilbericht.getXeurFile()).build());

        verify(elementConsumer, times(2)).accept(any());
    }

}
