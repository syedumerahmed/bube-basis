package de.wps.bube.basis.stammdatenbetreiber.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aLeistungseinheitReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.anEpsgReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschriftMitTaetigkeiten;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.entity.Vorschrift;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.stammdaten.domain.entity.Leistung;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;

public final class SpbAnlageBuilder {

    private Long id;
    private String anlageNr;
    private Long parentBetriebsstaetteId;
    private GeoPunkt geoPunkt;
    private List<Vorschrift> vorschriften = new ArrayList<>();
    private List<Leistung> leistungen = new ArrayList<>();
    private String name;
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;
    private Instant ersteErfassung;
    private Instant letzteAenderung;

    private SpbAnlageBuilder() {
    }

    public static SpbAnlageBuilder anSpbAnlage() {
        return anSpbAnlage(null);
    }

    public static SpbAnlageBuilder anSpbAnlage(Long id) {
        return new SpbAnlageBuilder().id(id)
                                     .anlageNr(String.valueOf(id))
                                     .name("Name")
                                     .letzteAenderung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")))
                                     .ersteErfassung(LocalDateTime.of(2020, 2, 20, 14, 0).toInstant(ZoneOffset.of("Z")));
    }

    public static SpbAnlageBuilder aFullAnlage() {
        return anSpbAnlage().id(2L)
                            .anlageNr("nummer")
                            .parentBetriebsstaetteId(23L)
                            .geoPunkt(new GeoPunkt(1, 2, anEpsgReferenz("epsg").build()))
                            .vorschriften(aVorschrift().build(), aVorschriftMitTaetigkeiten().build())
                            .leistungen(List.of(new Leistung(2.5, aLeistungseinheitReferenz("kg/schuss").build(), "bezug",
                                         Leistungsklasse.GENEHMIGT),
                                 new Leistung(2.8, aLeistungseinheitReferenz("m/s").build(), "bezug2",
                                         Leistungsklasse.BETRIEBEN)))
                            .name("Anlage")
                            .betriebsstatus(aBetriebsstatusReferenz("betrieb").build())
                            .betriebsstatusSeit(LocalDate.of(2019, 10, 12))
                            .inbetriebnahme(LocalDate.of(2017, 1, 1));
    }

    public SpbAnlageBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public SpbAnlageBuilder anlageNr(String anlageNr) {
        this.anlageNr = anlageNr;
        return this;
    }

    public SpbAnlageBuilder parentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        return this;
    }

    public SpbAnlageBuilder geoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
        return this;
    }

    public SpbAnlageBuilder vorschriften(Vorschrift... vorschriften) {
        return vorschriften(List.of(vorschriften));
    }

    public SpbAnlageBuilder vorschriften(List<Vorschrift> vorschriften) {
        this.vorschriften = vorschriften;
        return this;
    }

    public SpbAnlageBuilder leistungen(List<Leistung> leistungen) {
        this.leistungen = leistungen;
        return this;
    }

    public SpbAnlageBuilder name(String name) {
        this.name = name;
        return this;
    }

    public SpbAnlageBuilder betriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
        return this;
    }

    public SpbAnlageBuilder betriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
        return this;
    }

    public SpbAnlageBuilder inbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
        return this;
    }

    public SpbAnlageBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public SpbAnlageBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public SpbAnlage build() {
        SpbAnlage anlage = new SpbAnlage();
        anlage.setAnlageNr(anlageNr);
        anlage.setParentBetriebsstaetteId(parentBetriebsstaetteId);
        anlage.setName(name);
        anlage.setId(id);
        anlage.setErsteErfassung(ersteErfassung);
        anlage.setLetzteAenderung(letzteAenderung);
        anlage.setGeoPunkt(geoPunkt);
        anlage.setVorschriften(vorschriften);
        anlage.setLeistungen(leistungen);
        anlage.setBetriebsstatus(betriebsstatus);
        anlage.setBetriebsstatusSeit(betriebsstatusSeit);
        anlage.setInbetriebnahme(inbetriebnahme);
        return anlage;
    }
}
