package de.wps.bube.basis.euregistry.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.web.openapi.model.DownloadKomplexpruefungCommand;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.komplexpruefung.domain.entity.Komplexpruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefung;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelPruefungBuilder;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungenMap;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;

@ExtendWith(MockitoExtension.class)
class BerichtPruefungProtokollServiceTest {

    private static final ZoneOffset ZONE_OFFSET;

    static {
        LocalDateTime summertime = LocalDateTime.of(2021, 7, 2, 8, 24);
        ZoneId zone = ZoneId.of("Europe/Berlin");
        ZONE_OFFSET = zone.getRules().getOffset(summertime);
    }

    @Mock
    private KomplexpruefungService komplexpruefungService;
    @Mock
    private StammdatenService stammdatenService;
    @Mock
    private EURegistryService eURegistryService;

    private BerichtPruefungProtokollService service;

    @BeforeEach
    void setUp() {
        service = new BerichtPruefungProtokollService(eURegistryService, stammdatenService, komplexpruefungService);
    }

    @Test
    void protokolliereBetriebsstaetteMitBericht_ohneFehler() throws IOException {

        var bericht = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        DownloadKomplexpruefungCommand command = new DownloadKomplexpruefungCommand();
        command.setBstId(bericht.getBetriebsstaette().getId());
        Komplexpruefung komplexpruefung = new Komplexpruefung();
        bericht.setKomplexpruefung(komplexpruefung);
        komplexpruefung.setId(1L);
        komplexpruefung.setAusfuehrung(LocalDateTime.of(2021, 7, 2, 8, 24, 55).toInstant(ZONE_OFFSET));

        when(stammdatenService.loadBetriebsstaetteForRead(command.getBstId())).thenReturn(bericht.getBetriebsstaette());
        when(eURegistryService.findBetriebsstaettenBerichtByBstId(command.getBstId())).thenReturn(
                Optional.of(bericht));
        when(komplexpruefungService.readRegelPruefungen(komplexpruefung)).thenReturn(new RegelPruefungenMap());

        var anlage = bericht.getBetriebsstaette().getAnlagen().get(0);
        when(eURegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                Optional.of(EURegAnlageBuilder.anEURegAnlage(1L).build()));

        var anlagenteil = bericht.getBetriebsstaette().getAnlagen().get(0).getAnlagenteile().get(0);
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                Optional.of(EURegAnlageBuilder.anEURegAnlage(2L).build()));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        service.protokolliere(outputStream, command.getBstId());

        String protokoll = """
                Komplexprüfung Start: 2021-07-02T08:24:55 Jahr: 2018
                Das Protokoll enthält 0 Fehler und 0 Warnungen.

                Betriebsstätte: nummer | Halsbrecher Betrieb
                  Betreiber: 1234 | betreiber
                  EUReg Betriebsstätte Bericht:
                  Anlage: 1 | Name
                    EUReg Anlage Bericht:
                    Anlagenteil: 1 | Name
                      EUReg Anlagenteil Bericht:
                    Quelle: 3 | quelle
                  Anlage: 2 | Name
                  Quelle: 1 | quelle
                  Quelle: 2 | quelle
                """;
        assertThat(outputStream.toString()).isEqualToNormalizingNewlines(protokoll);
    }

    @Test
    void protokolliereBetriebsstaetteMitBericht_mitFehler() throws IOException {

        var bericht = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().build();
        DownloadKomplexpruefungCommand command = new DownloadKomplexpruefungCommand();
        command.setBstId(bericht.getBetriebsstaette().getId());
        Komplexpruefung komplexpruefung = new Komplexpruefung();
        bericht.setKomplexpruefung(komplexpruefung);
        komplexpruefung.setId(1L);
        komplexpruefung.addAnzahlFehler(2L);
        komplexpruefung.addAnzahlWarnungen(2L);
        komplexpruefung.setAusfuehrung(LocalDateTime.of(2021, 7, 2, 8, 24, 55).toInstant(ZONE_OFFSET));

        when(stammdatenService.loadBetriebsstaetteForRead(command.getBstId())).thenReturn(bericht.getBetriebsstaette());
        when(eURegistryService.findBetriebsstaettenBerichtByBstId(command.getBstId())).thenReturn(
                Optional.of(bericht));

        var anlage = bericht.getBetriebsstaette().getAnlagen().get(0);
        when(eURegistryService.findAnlagenBerichtByAnlageId(anlage.getId())).thenReturn(
                Optional.of(EURegAnlageBuilder.anEURegAnlage(1L).build()));

        var anlagenteil = bericht.getBetriebsstaette().getAnlagen().get(0).getAnlagenteile().get(0);
        when(eURegistryService.findAnlagenBerichtByAnlagenteilId(anlagenteil.getId())).thenReturn(
                Optional.of(EURegAnlageBuilder.anEURegAnlage(2L).build()));

        Stream<RegelPruefung> stream = Stream.of(
                RegelPruefungBuilder.aRegelPruefungMitFehler()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.EUREG_ANLAGE)
                                    .pruefObjektId(1L)
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitWarnung()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.EUREG_ANLAGE)
                                    .pruefObjektId(1L)
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitWarnung()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.EUREG_F)
                                    .pruefObjektId(2L)
                                    .build(),
                RegelPruefungBuilder.aRegelPruefungMitFehler()
                                    .komplexpruefung(komplexpruefung)
                                    .regelObjekt(RegelObjekt.EUREG_F)
                                    .pruefObjektId(2L)
                                    .build()

        );
        RegelPruefungenMap regelPruefungenMap = new RegelPruefungenMap();
        stream.forEach(regelPruefungenMap::put);
        when(komplexpruefungService.readRegelPruefungen(komplexpruefung)).thenReturn(regelPruefungenMap);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        service.protokolliere(outputStream, command.getBstId());

        String protokoll = """
                Komplexprüfung Start: 2021-07-02T08:24:55 Jahr: 2018
                Das Protokoll enthält 2 Fehler und 2 Warnungen.

                Betriebsstätte: nummer | Halsbrecher Betrieb
                  Betreiber: 1234 | betreiber
                  EUReg Betriebsstätte Bericht:
                  Anlage: 1 | Name
                    EUReg Anlage Bericht:
                    FEHLER 42: Dies ist eine Fehlernachricht
                    WARNUNG 21: Dies ist eine Warnung
                    Anlagenteil: 1 | Name
                      EUReg Anlagenteil Bericht:
                      WARNUNG 21: Dies ist eine Warnung
                      FEHLER 42: Dies ist eine Fehlernachricht
                    Quelle: 3 | quelle
                  Anlage: 2 | Name
                  Quelle: 1 | quelle
                  Quelle: 2 | quelle
                """;
        assertThat(outputStream.toString()).isEqualToNormalizingNewlines(protokoll);
    }

}
