package de.wps.bube.basis.benutzerverwaltung.domain.persistence;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.wps.bube.basis.benutzerverwaltung.domain.entity.BenutzerImportJobStatus;
import de.wps.bube.basis.jobs.domain.JobStatus;
import de.wps.bube.basis.jobs.persistence.JobStatusRepository;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class BenutzerImportJobStatusRepositoryTest {

    private static final String USERNAME = "Test-User";
    private static final String PROTOKOLL = "Test-Protokoll";

    @Autowired
    private JobStatusRepository jobStatusRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void testJobStatusInheritance() {

        // BenutzerImportJobStatus erzeugen
        JobStatus jobStatus = new BenutzerImportJobStatus();
        jobStatus.setUsername(USERNAME);
        jobStatus.setProtokoll(PROTOKOLL);

        // und persistieren
        jobStatusRepository.saveAndFlush(jobStatus);
        entityManager.clear(); // (anderenfalls würden Entitäten nicht aus der DB geladen)

        // JobStatus als BenutzerImportJobStatus neu laden
        Optional<JobStatus> optional = jobStatusRepository.findById(USERNAME);
        assertThat(optional).isPresent();
        jobStatus = optional.get();
        assertThat(jobStatus).isInstanceOf(BenutzerImportJobStatus.class);
        BenutzerImportJobStatus benutzerImportJobStatus = (BenutzerImportJobStatus) jobStatus;
        assertThat(benutzerImportJobStatus.getUsername()).isEqualTo(USERNAME);
        assertThat(benutzerImportJobStatus.getJobName()).isEqualTo(BenutzerImportJobStatus.JOB_NAME.name());
        assertThat(benutzerImportJobStatus.getProtokoll()).isEqualTo(PROTOKOLL);
        assertThat(benutzerImportJobStatus.getCount()).isZero();
        assertThat(benutzerImportJobStatus.isCompleted()).isFalse();
        assertThat(benutzerImportJobStatus.getBenutzerNamen()).isEmpty();

        // BenutzerNamen hinzufügen, Count setzen und speichern
        benutzerImportJobStatus.getBenutzerNamen().add("Benutzer1");
        benutzerImportJobStatus.getBenutzerNamen().add("Benutzer2");
        benutzerImportJobStatus.addToCount(23L);
        jobStatusRepository.saveAndFlush(benutzerImportJobStatus);
        entityManager.clear();

        // Als JobStatus laden => count ist übenommen
        optional = jobStatusRepository.findById(USERNAME);
        assertThat(optional).isPresent();
        jobStatus = optional.get();
        assertThat(jobStatus.getUsername()).isEqualTo(USERNAME);
        assertThat(jobStatus.getJobName()).isEqualTo(BenutzerImportJobStatus.JOB_NAME.name());
        assertThat(jobStatus.getProtokoll()).isEqualTo(PROTOKOLL);
        assertThat(jobStatus.getCount()).isEqualTo(23L);
        assertThat(jobStatus.isCompleted()).isFalse();

        // JobStatus komplettieren und speichern
        jobStatus.setCompleted();
        jobStatusRepository.saveAndFlush(jobStatus);
        entityManager.clear();

        // Noch einmal als BenutzerImportJobStatus laden => Count, Completed und BenutzerNamen sind korrekt gesetzt
        optional = jobStatusRepository.findById(USERNAME);
        assertThat(optional).isPresent();
        jobStatus = optional.get();
        assertThat(jobStatus).isInstanceOf(BenutzerImportJobStatus.class);
        benutzerImportJobStatus = (BenutzerImportJobStatus) jobStatus;
        assertThat(benutzerImportJobStatus.getUsername()).isEqualTo(USERNAME);
        assertThat(benutzerImportJobStatus.getJobName()).isEqualTo(BenutzerImportJobStatus.JOB_NAME.name());
        assertThat(benutzerImportJobStatus.getProtokoll()).isEqualTo(PROTOKOLL);
        assertThat(benutzerImportJobStatus.getCount()).isEqualTo(23L);
        assertThat(benutzerImportJobStatus.isCompleted()).isTrue();
        assertThat(benutzerImportJobStatus.getBenutzerNamen()).containsExactly("Benutzer1", "Benutzer2");

    }

}