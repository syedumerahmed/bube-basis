package de.wps.bube.basis.stammdaten.domain.service;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.*;
import static de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder.aBetreiber;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.function.ThrowingConsumer;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;

import com.querydsl.core.types.Predicate;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.stammdaten.persistence.AnlageRepository;
import de.wps.bube.basis.stammdaten.persistence.AnlagenteilRepository;
import de.wps.bube.basis.stammdaten.persistence.BetreiberRepository;
import de.wps.bube.basis.stammdaten.persistence.BetriebsstaetteRepository;
import de.wps.bube.basis.stammdaten.persistence.QuelleRepository;
import de.wps.bube.basis.stammdaten.security.BetreiberBerechtigungsAccessor;
import de.wps.bube.basis.stammdaten.security.BetriebsstaettenBerechtigungsAccessor;
import de.wps.bube.basis.test.SecurityTest;
import de.wps.bube.basis.test.WithBubeMockUser;

@SpringBootTest(classes = StammdatenService.class)
class StammdatenServiceSecurityTest extends SecurityTest {

    /**
     * Ermöglicht typsichere Erzeugung der Argumente für Testfälle dieser Klasse
     *
     * @param methodName Name der zu testenden Methode
     * @param method     Beschreibung des Service-Aufrufs
     *
     * @return Argumentdefinition für einen Testfall
     */
    private static Arguments createTestArguments(String methodName,
            ThrowingConsumer<StammdatenService> method) {
        return arguments(methodName, method);
    }

    private static Stream<? extends Arguments> writeMethods() {
        return Stream.of(createTestArguments("createBetreiber",
                service -> service.createBetreiberZuBetriebsstaette(aBetreiber().build(), "123")),
                createTestArguments("createBetriebsstaette",
                        service -> service.createBetriebsstaette(aBetriebsstaette().build())),
                createTestArguments("createQuelle",
                        service -> service.createQuelle(aQuelle().parentBetriebsstaetteId(15L).build()
                        )),
                createTestArguments("updateBetreiber",
                        service -> service.updateBetreiber(aBetreiber().id(15L).build())),
                createTestArguments("updateBetriebsstaette",
                        service -> service.updateBetriebsstaette(aBetriebsstaette().id(15L).build())));
    }

    private static Stream<? extends Arguments> readMethods() {
        return Stream.of(createTestArguments("loadBetreiberByJahrLandNummer",
                service -> service.loadBetreiberByJahrLandNummer("03", HAMBURG, "123")),
                createTestArguments("loadBetriebsstaetteForRead", service -> service.loadBetriebsstaetteForRead(15L)),
                createTestArguments("findAllBetreiber",
                        service -> service.loadAllBetreiberByJahrAndLand("04", Land.BAYERN)));
    }

    private static Stream<? extends Arguments> deleteMethods() {
        return Stream.of(createTestArguments("deleteBetreiber", service -> service.deleteBetreiber(5L)),
                createTestArguments("deleteBetriebsstaette", service -> service.deleteBetriebsstaette(5L)));
    }

    @MockBean
    private BetreiberRepository betreiberRepository;
    @MockBean
    private BetriebsstaetteRepository betriebsstaetteRepository;
    @MockBean
    private QuelleRepository quelleRepository;
    @MockBean
    private BetriebsstaettenBerechtigungsAccessor betriebsstaetteBerechtigungsAccessor;
    @MockBean
    private BetreiberBerechtigungsAccessor betreiberBerechtigungsAccessor;
    @MockBean
    private AnlageRepository anlageRepository;
    @MockBean
    private AnlagenteilRepository anlagenteilRepository;
    @MockBean
    private AnlagenUpdater anlagenUpdater;
    @MockBean
    private BetriebsstaettenUpdater betriebsstaettenUpdater;
    @MockBean
    private StammdatenBerichtsjahrwechselMapper berichtsjahrwechselMapper;

    @Autowired
    private StammdatenService service;

    @BeforeEach
    void setUp() {
        when(betriebsstaetteRepository.exists(any(Predicate.class))).thenReturn(true);
        when(betreiberRepository.exists(any(Predicate.class))).thenReturn(true);
        when(betreiberRepository.save(any())).thenReturn(
                aBetreiber(2L).land(Land.BAYERN).jahr(aJahrReferenz(2016).build()).build());
        when(betriebsstaetteRepository.findOne(any(Predicate.class))).thenReturn(
                Optional.of(aBetriebsstaette().id(1L).build()));
        when(betreiberRepository.findOne(any(Predicate.class))).thenReturn(Optional.of(aBetreiber().build()));
        when(betriebsstaetteRepository.exists(any(Predicate.class))).thenReturn(true);
        when(quelleRepository.findById(anyLong())).then(
                i -> Optional.of(aQuelle(i.getArgument(0)).parentBetriebsstaetteId(15L).build()));
    }

    /*
     * Parametrisierter Test, der Parameter-Stream aus den @MethodSource Methoden
     * nimmt und für alle den Testcode ausführt.
     *
     * @DisplayName sorgt für eine Anzeige in der IDE / im Build ohne Parametertypen
     *
     * @ParameterizedTest nutzt nur den Methodennamen als Testnamen innerhalb dieser
     * Gruppe
     *
     * @MethodSource gibt an, welche Methode(n) zur Generierung der Testfälle
     * genutzt werden sollen
     *
     * @WithBubeMockUser ermöglicht, dass der SpringSecurityContext korrekt mit
     * Nutzerrechten befüllt wird
     */
    @DisplayName("withoutAuthorities -> Deny All")
    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods", "deleteMethods" })
    @WithBubeMockUser
    void withoutAuthoritiesDenyAll(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(betriebsstaetteRepository, betreiberRepository, quelleRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "writeMethods", "deleteMethods" })
    @WithBubeMockUser(roles = { BETREIBER, LAND_READ, BEHOERDE_READ })
    void readonlyDenyWriteAndDelete(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(betriebsstaetteRepository, betreiberRepository, quelleRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods" })
    @WithBubeMockUser(roles = BETREIBER)
    void betreiberAllowRead(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods" })
    @WithBubeMockUser(roles = LAND_READ)
    void landAllowRead(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods" })
    @WithBubeMockUser(roles = BEHOERDE_READ)
    void behoerdeAllowRead(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = { LAND_WRITE, BETREIBER, BEHOERDE_WRITE })
    void writeDenyDelete(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatThrownBy(() -> method.accept(service)).isInstanceOf(AccessDeniedException.class);
        verifyNoInteractions(betriebsstaetteRepository, betreiberRepository, quelleRepository);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods" })
    @WithBubeMockUser(roles = LAND_WRITE)
    void landWriteAllowWriteAndRead(String methodName, ThrowingConsumer<StammdatenService> method) {
        when(betriebsstaetteRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "readMethods", "writeMethods" })
    @WithBubeMockUser(roles = BEHOERDE_WRITE)
    void behoerdeWriteAllowWriteAndRead(String methodName, ThrowingConsumer<StammdatenService> method) {
        when(betriebsstaetteRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = LAND_DELETE)
    void landDeleteAllowDelete(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource({ "deleteMethods" })
    @WithBubeMockUser(roles = BEHOERDE_DELETE)
    void behoerdeDeleteAllowDelete(String methodName, ThrowingConsumer<StammdatenService> method) {
        assertThatCode(() -> method.accept(service)).doesNotThrowAnyException();
    }
}
