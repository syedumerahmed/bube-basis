package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.TEILBERICHT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaette;
import de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder;
import de.wps.bube.basis.euregistry.rules.EURegAnlageBeanMapper;
import de.wps.bube.basis.euregistry.rules.EURegBeanMappers;
import de.wps.bube.basis.euregistry.rules.EURegBetriebsstaetteBeanMapper;
import de.wps.bube.basis.euregistry.rules.EURegFBeanMapper;
import de.wps.bube.basis.euregistry.rules.TeilberichtBeanMapper;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelBuilder;
import de.wps.bube.basis.komplexpruefung.domain.service.KomplexpruefungService;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungErgebnis;
import de.wps.bube.basis.komplexpruefung.domain.service.RegelPruefungService;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelGruppe;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.komplexpruefung.persistence.KomplexpruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelPruefungRepository;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.komplexpruefung.rules.BeanMappingContext;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenPruefungService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.rules.AdresseBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.BehoerdeBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.KommunikationsverbindungBeanMapperImpl;
import de.wps.bube.basis.referenzdaten.rules.ReferenzBeanMapperImpl;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.BetriebsstaettePruefungService;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.rules.AnlageBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.AnlagenteilBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.AnsprechpartnerBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.BetreiberBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.BetriebsstaetteBeanMapper;
import de.wps.bube.basis.stammdaten.rules.BetriebsstaetteBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.GeoPunktBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.LeistungBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.QuelleBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.VorschriftBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.ZustaendigeBehoerdeBeanMapperImpl;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;

@SpringBootTest(classes = { BetriebsstaetteBeanMapperImpl.class, ReferenzBeanMapperImpl.class,
        BehoerdeBeanMapperImpl.class, AdresseBeanMapperImpl.class, KommunikationsverbindungBeanMapperImpl.class,
        BetreiberBeanMapperImpl.class, AnsprechpartnerBeanMapperImpl.class, GeoPunktBeanMapperImpl.class,
        ZustaendigeBehoerdeBeanMapperImpl.class, QuelleBeanMapperImpl.class, AnlageBeanMapperImpl.class,
        VorschriftBeanMapperImpl.class, LeistungBeanMapperImpl.class, AnlagenteilBeanMapperImpl.class })
@ExtendWith(MockitoExtension.class)
class BerichtPruefungServiceTest {

    @MockBean
    private RegelRepository regelRepository;
    @MockBean
    private RegelPruefungService regelPruefungService;
    @MockBean
    private KomplexpruefungRepository komplexpruefungRepository;
    @MockBean
    private RegelPruefungRepository regelPruefungRepository;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BerichtPruefungService berichtPruefungService;
    @MockBean
    private BetriebsstaettePruefungService betriebsstaettePruefungService;
    @MockBean
    private KomplexpruefungService komplexpruefungService;
    @MockBean
    private KoordinatenPruefungService koordinatenPruefungService;
    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private EURegBetriebsstaetteBeanMapper euRetBetriebsstaetteBeanMapper;
    @MockBean
    private EURegAnlageBeanMapper euRegAnlageBeanMapper;
    @MockBean
    private EURegFBeanMapper euRegFBeanMapper;
    @MockBean
    private TeilberichtBeanMapper teilberichtBeanMapper;
    @MockBean
    private EURegBeanMappers euRegBeanMappers;

    @MockBean
    private EURegistryService euRegistryService;

    @Autowired
    private BetriebsstaetteBeanMapper betriebsstaetteBeanMapper;

    @BeforeEach
    void setUp() {
        berichtPruefungService = new BerichtPruefungService(euRegistryService, betriebsstaettePruefungService,
                stammdatenService, komplexpruefungService, regelPruefungService, referenzenService,
                euRegBeanMappers);
    }

    @Test
    void komplexpruefungForEuRegBetriebsstaette() {

        // Given
        Land land = Land.HAMBURG;
        var regelBuilder = RegelBuilder.aRegel()
                                       .land(land)
                                       .code("#OBJ.localId != null")
                                       .gruppe(RegelGruppe.GRUPPE_B)
                                       .typ(RegelTyp.FEHLER);
        Regel regel = regelBuilder.build();
        Regel teilberichtRegel = regelBuilder.objekte(TEILBERICHT).build();
        Betriebsstaette betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette(1L).build();
        EURegBetriebsstaette euRegBetriebsstaette
                = EURegBetriebsstaetteBuilder.aEURegBetriebsstaette().id(2L).betriebsstaette(betriebsstaette).build();
        BeanMappingContext context = new BeanMappingContext(betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr());
        BetriebsstaetteBean betriebsstaetteBean = betriebsstaetteBeanMapper.toBean(betriebsstaette, context,
                euRegBeanMappers);
        RegelPruefungErgebnis regelPruefungErgebnis = new RegelPruefungErgebnis();
        regelPruefungErgebnis.add(betriebsstaetteBean, false);

        when(regelRepository.streamZuBeruecksichtigendeByLandAndGruppeAndGueltigkeit(land,
                List.of(RegelGruppe.GRUPPE_B),
                Gueltigkeitsjahr.of(2020))).thenReturn(Stream.of(regel, teilberichtRegel));
        when(regelPruefungService.pruefeRegeln(eq(regel), eq(land),
                eq(betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr()),
                eq(betriebsstaette.getZustaendigeBehoerde().getId()), any())).thenReturn(regelPruefungErgebnis);
        when(komplexpruefungService.save(any())).thenAnswer(invocation -> invocation.getArgument(0));

        // When
        berichtPruefungService.komplexpruefung(euRegBetriebsstaette);

        // Then
        verifyNoMoreInteractions(regelRepository);

        verifyNoMoreInteractions(komplexpruefungRepository);
        verifyNoMoreInteractions(regelPruefungRepository);

        verifyNoMoreInteractions(regelPruefungService);

        verifyNoMoreInteractions(stammdatenService);

    }

}
