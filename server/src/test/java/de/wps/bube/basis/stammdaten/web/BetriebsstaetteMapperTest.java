package de.wps.bube.basis.stammdaten.web;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.web.BehoerdenMapperImpl;
import de.wps.bube.basis.referenzdaten.web.ReferenzMapperImpl;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.service.StammdatenService;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;
import de.wps.bube.basis.stammdaten.domain.vo.suche.BetriebsstaetteListItemView;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { BetriebsstaetteMapperImpl.class, ReferenzMapperImpl.class, BehoerdenMapperImpl.class })
class BetriebsstaetteMapperTest {

    @Autowired
    private BetriebsstaetteMapper mapper;
    @MockBean
    private StammdatenService stammdatenService;
    @MockBean
    private BehoerdenService behoerdenService;
    @MockBean
    private ReferenzenService referenzenService;

    @Test
    void identity() {
        var original = BetriebsstaetteBuilder.aFullBetriebsstaette().build();
        AtomicLong behoerdenId = new AtomicLong();
        var behoerden = Stream.concat(Stream.of(original.getZustaendigeBehoerde()),
                original.getZustaendigeBehoerden().stream().map(
                        ZustaendigeBehoerde::getBehoerde)).collect(Collectors.toList());
        behoerden.forEach(behoerde -> behoerde.setId(behoerdenId.incrementAndGet()));
        var behoerdenMap = behoerden.stream().collect(Collectors.toMap(Behoerde::getId, Function.identity()));

        when(behoerdenService.readBehoerde(anyLong())).thenAnswer(i -> behoerdenMap.get(i.<Long>getArgument(0)));
        when(referenzenService.findReferenz(
                any(Referenzliste.class),
                anyString(),
                any(Gueltigkeitsjahr.class),
                any(Land.class)))
                .thenAnswer(i -> Optional.of(aReferenz(i.getArgument(0), i.getArgument(1)).build()));
        when(stammdatenService.loadBetreiber(original.getBetreiber().getId())).thenReturn(original.getBetreiber());

        var dto = mapper.toDto(original);
        var betriebsstaette = mapper.fromDto(dto);

        // Anlagen und Quellen sind nicht Teil des BetriebsstätteDtos
        assertThat(betriebsstaette).usingRecursiveComparison().ignoringFields("quellen", "anlagen").isEqualTo(original);
    }

    @Test
    void canDeleteListItem_allowed() {
        var bst = new BetriebsstaetteListItemView();
        bst.land = Land.SAARLAND;
        when(stammdatenService.canDeleteBetriebsstaette(bst)).thenReturn(true);

        var dto = mapper.toListDto(bst);

        assertThat(dto.getCanDelete()).isTrue();
    }

    @Test
    void canDeleteListItem_notAllowed() {
        var bst = new BetriebsstaetteListItemView();
        bst.land = Land.SAARLAND;
        when(stammdatenService.canDeleteBetriebsstaette(bst)).thenReturn(false);

        var dto = mapper.toListDto(bst);

        assertThat(dto.getCanDelete()).isFalse();
    }

}
