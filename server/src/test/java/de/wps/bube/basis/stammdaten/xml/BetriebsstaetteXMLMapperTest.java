package de.wps.bube.basis.stammdaten.xml;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVorschriftReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aZustaendigkeitsReferenz;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.base.vo.Land.NIEDERSACHSEN;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.R4BV;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RKOM;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RVORS;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.RZUST;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import de.wps.bube.basis.stammdaten.xml.dto.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.BehoerdenService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzResolverService;
import de.wps.bube.basis.referenzdaten.domain.service.ReferenzenService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { BetriebsstaetteXMLMapperImpl.class, ReferenzResolverService.class })
class BetriebsstaetteXMLMapperTest {

    @MockBean
    private ReferenzenService referenzenService;
    @MockBean
    private BehoerdenService behoerdenService;
    @Autowired
    private BetriebsstaetteXMLMapper mapper;

    @Test
    void toXDto_fullBetriebsstaette() {
        var betriebsstaetteNr = "0123456";
        var betriebsstaette = BetriebsstaetteBuilder.aFullBetriebsstaette()
                                                    .betriebsstaetteNr(betriebsstaetteNr)
                                                    .build();

        var betriebsstaetteXDto = assertDoesNotThrow(() -> mapper.toXDto(betriebsstaette));

        assertThat(betriebsstaetteXDto).hasFieldOrPropertyWithValue("nr", betriebsstaetteNr);
    }

    @Test
    void toXDto_minimalBetriebsstaette() {
        var betriebsstaetteNr = "0123456";
        var betriebsstaette = BetriebsstaetteBuilder.aBetriebsstaette().betriebsstaetteNr(betriebsstaetteNr).build();

        var betriebsstaetteXDto = assertDoesNotThrow(() -> mapper.toXDto(betriebsstaette));

        assertThat(betriebsstaetteXDto).hasFieldOrPropertyWithValue("nr", betriebsstaetteNr);
    }

    @Test
    void fromXDto_shouldCollectErrors() {
        var xdto = new BetriebsstaetteType();
        xdto.setRland("02");
        // Refs, die nicht gefunden werden sollen
        xdto.setRjahr("2000");
        xdto.setRbetz("RBETZ");
        xdto.setRflez("RFLEZ");
        xdto.setRgmd("RGMD");
        xdto.setRnace("RNACE");
        xdto.setVertraulichkeitsgrund("RCONF");

        var vorschriften = new VorschriftenListe();
        vorschriften.getSchluessel().add("V1");
        vorschriften.getSchluessel().add("V2");
        xdto.setVorschriften(vorschriften);

        xdto.setZustaendigeBehoerde("BEH");

        var anlagen = new AnlagenType();
        var anlage = new AnlageType();
        // Ref in der Anlage, die nicht gefunden wird
        anlage.setVertraulichkeitsgrund("VERS");
        anlage.setNr("0100");
        anlage.setName("AN");
        anlagen.getAnlage().add(anlage);
        xdto.setAnlagen(anlagen);

        when(referenzenService.findReferenz(any(), any(), any(), any())).thenReturn(empty());
        when(referenzenService.sucheReferenzUeberAlleJahre(any(), any(), any())).thenReturn(empty());
        when(behoerdenService.findBehoerde(any(), any(), any())).thenReturn(empty());

        var bstWithErrors = assertDoesNotThrow(() -> mapper.fromXDto(xdto));

        // Im Testsetup wurden genau 10 Referenzen/Behörden gefüllt
        assertThat(bstWithErrors.errorsOrEmpty()).hasSize(10);
        bstWithErrors.errorsOrEmpty().forEach(mappingError -> assertThat(mappingError.istWarnung()).isFalse());
    }

    @Test
    void fromXDto_shouldCollectWarnings() {
        var xdto = new BetriebsstaetteType();
        xdto.setRland("02");
        // Refs, die nicht gefunden werden sollen
        xdto.setRjahr("2000");
        xdto.setRbetz("RBETZ");
        xdto.setRflez("RFLEZ");
        xdto.setRgmd("RGMD");
        xdto.setRnace("RNACE");
        xdto.setVertraulichkeitsgrund("RCONF");

        var vorschriften = new VorschriftenListe();
        vorschriften.getSchluessel().add("V1");
        vorschriften.getSchluessel().add("V2");
        xdto.setVorschriften(vorschriften);

        xdto.setZustaendigeBehoerde("BEH");

        var anlagen = new AnlagenType();
        var anlage = new AnlageType();
        // Ref in der Anlage, die nicht gefunden wird
        anlage.setVertraulichkeitsgrund("VERS");
        anlage.setNr("0100");
        anlage.setName("AN");
        anlagen.getAnlage().add(anlage);
        xdto.setAnlagen(anlagen);

        var dummyReferenz = ReferenzBuilder.aJahrReferenz(2000).build();

        when(referenzenService.findReferenz(any(), any(), any(), any())).thenReturn(empty());
        when(referenzenService.sucheReferenzUeberAlleJahre(any(), any(), any())).thenReturn(Optional.of(dummyReferenz));
        when(behoerdenService.findBehoerde("BEH", Gueltigkeitsjahr.of(2000), HAMBURG)).thenReturn(
                Optional.of(aBehoerde("BEH").land(HAMBURG).build()));

        var bstWithErrors = assertDoesNotThrow(() -> mapper.fromXDto(xdto));

        // Im Testsetup wurden genau 9 Referenzen/Behörden gefüllt
        assertThat(bstWithErrors.errorsOrEmpty()).hasSize(9);
        bstWithErrors.errorsOrEmpty().forEach(mappingError -> assertThat(mappingError.istWarnung()).isTrue());

    }

    @Test
    void fromXDto_shouldNotThrow_whenBehoerdeNotFound() {
        var zustBehoerdenXDto = new ZustaendigeBehoerdenType();
        var z1 = new ZustaendigeBehoerdeType();
        var z2 = new ZustaendigeBehoerdeType();
        z1.setBehoerde("notExisting");
        z1.setRzust("ZUST");
        z2.setBehoerde("BEH");
        z2.setRzust("notExisting");
        zustBehoerdenXDto.getZustaendigeBehoerde().add(z1);
        zustBehoerdenXDto.getZustaendigeBehoerde().add(z2);

        var xdto = new BetriebsstaetteType();
        xdto.setRland("03");
        xdto.setRjahr("2020");
        xdto.setZustaendigeBehoerden(zustBehoerdenXDto);

        when(referenzenService.findReferenz(RZUST, "ZUST", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                Optional.of(aZustaendigkeitsReferenz("ZUST").land(NIEDERSACHSEN).build()));
        when(referenzenService.findReferenz(RZUST, "notExisting", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                empty());

        when(behoerdenService.findBehoerde("BEH", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                Optional.of(aBehoerde("BEH").land(NIEDERSACHSEN).build()));
        when(behoerdenService.findBehoerde("notExisting", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                empty());

        assertDoesNotThrow(() -> mapper.fromXDto(xdto));
    }

    @Test
    void fromXDto_shouldNotThrow_whenKommTypNotFound() {
        var kommXDto = new KommunikationsverbindungenType();
        var k1 = new KommunikationsverbindungType();
        var k2 = new KommunikationsverbindungType();
        k1.setRkom("notExisting");
        k1.setValue("moin");
        k2.setRkom("RKOM");
        k2.setValue("moin");
        kommXDto.getKommunikationsverbindung().add(k1);
        kommXDto.getKommunikationsverbindung().add(k2);

        var xdto = new BetriebsstaetteType();
        xdto.setRland("03");
        xdto.setRjahr("2020");
        xdto.setKommunikationsverbindungen(kommXDto);

        when(referenzenService.findReferenz(RKOM, "RKOM", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                Optional.of(aKommunikationsTypReferenz("RKOM").land(NIEDERSACHSEN).build()));
        when(referenzenService.findReferenz(RKOM, "notExisting", Gueltigkeitsjahr.of(2020), NIEDERSACHSEN)).thenReturn(
                empty());

        assertDoesNotThrow(() -> mapper.fromXDto(xdto));
    }

    @Test
    void fromXDto_shouldNotThrow_whenTaetigkeitNotFound() {

        var xdto = new BetriebsstaetteType() {{
            rland = "02";
            rjahr = "2020";
            anlagen = new AnlagenType() {{
                anlage = List.of(new AnlageType() {{
                    nr = "0100";
                    name = "AN";
                    vorschriften = new VorschriftenType() {{
                        vorschrift = List.of(new VorschriftType() {{
                            art = "R4BV";
                            haupttaetigkeit = "notValid";
                            taetigkeiten = new TaetigkeitenListe() {{
                                schluessel = List.of("notValid");
                            }};
                        }});
                    }};
                }});
            }};
        }};

        when(referenzenService.findReferenz(RVORS, "R4BV", Gueltigkeitsjahr.of(2020), HAMBURG)).thenReturn(
                Optional.of(aVorschriftReferenz("R4BV").strg("R4BV").build()));
        when(referenzenService.findReferenz(R4BV, "notValid", Gueltigkeitsjahr.of(2020), HAMBURG)).thenReturn(empty());

        assertDoesNotThrow(() -> mapper.fromXDto(xdto));
    }
}
