package de.wps.bube.basis.sicherheit.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung.FACHMODULE;
import static de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung.LAND;
import static de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung.THEMEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung.VERWALTUNGSGEBIETE;
import static de.wps.bube.basis.sicherheit.domain.vo.Fachmodul.RECHERCHE;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.BODEN;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.LUFT;
import static de.wps.bube.basis.sicherheit.domain.vo.Thema.WASSER;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Thema;

@ExtendWith(MockitoExtension.class)
class KeycloakAuthenticationServiceTest {

    @Mock
    private KeycloakAuthenticationToken token;
    @Mock
    private SimpleKeycloakAccount account;
    @Mock
    private KeycloakPrincipal<KeycloakSecurityContext> principal;
    @Mock
    private RefreshableKeycloakSecurityContext keycloakContext;
    @Mock
    private AccessToken accessToken;

    private KeycloakAuthenticationService keycloakAuthenticationService;

    @BeforeEach
    void setUp() {
        when(token.getDetails()).thenReturn(account);
        when(account.getPrincipal()).thenReturn(principal);
        when(account.getKeycloakSecurityContext()).thenReturn(keycloakContext);
        when(keycloakContext.getToken()).thenReturn(accessToken);

        final SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);
        keycloakAuthenticationService = new KeycloakAuthenticationService();
    }

    @Test
    void getCurrentBenutzer() {
        final Set<String> allRoles = Arrays.stream(Rolle.values())
                                           .map(r -> r.name().toLowerCase())
                                           .collect(Collectors.toSet());
        allRoles.add("nonExistentRole");
        when(principal.getName()).thenReturn("mjoergens");
        when(account.getRoles()).thenReturn(allRoles);
        when(accessToken.getOtherClaims()).thenReturn(
                Map.of(LAND, DEUTSCHLAND.getNr(),
                        THEMEN, Stream.of(BODEN, LUFT, WASSER).map(Thema::getBezeichner).collect(toList()),
                        VERWALTUNGSGEBIETE, List.of("HHLLKKK"),
                        FACHMODULE, List.of(RECHERCHE.getBezeichner())));

        AktiverBenutzer aktiverBenutzer = keycloakAuthenticationService.getCurrentBenutzer();

        assertThat(aktiverBenutzer.getUsername()).isEqualTo("mjoergens");
        assertThat(aktiverBenutzer.hatAlleRollen(Rolle.values())).isTrue();
        assertThat(aktiverBenutzer.getLand()).isEqualTo(DEUTSCHLAND);
        assertThat(aktiverBenutzer.getBerechtigung().themen).contains(BODEN, LUFT, WASSER);
        assertThat(aktiverBenutzer.getBerechtigung().verwaltungsgebiete).contains("HHLLKKK");
        assertThat(aktiverBenutzer.getBerechtigung().fachmodule).contains(RECHERCHE);

    }

}
