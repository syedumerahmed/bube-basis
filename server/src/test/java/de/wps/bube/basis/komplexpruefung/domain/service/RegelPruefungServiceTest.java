package de.wps.bube.basis.komplexpruefung.domain.service;

import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.euregistry.domain.entity.EURegBetriebsstaetteBuilder.aEURegBetriebsstaette;
import static de.wps.bube.basis.komplexpruefung.domain.entity.RegelBuilder.aRegel;
import static de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt.*;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aBetriebsstatusReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder.aBetriebsstaette;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.euregistry.domain.entity.Berichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlage;
import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdaten;
import de.wps.bube.basis.euregistry.domain.entity.FeuerungsanlageBerichtsdatenBuilder;
import de.wps.bube.basis.euregistry.domain.entity.XeurTeilbericht;
import de.wps.bube.basis.euregistry.domain.vo.BerichtsdatenBuilder;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelObjekt;
import de.wps.bube.basis.komplexpruefung.rules.bean.PruefObjekt;
import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.vo.AdresseBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Anlagenteil;
import de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;
import de.wps.bube.basis.stammdaten.domain.entity.BetreiberBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;
import de.wps.bube.basis.stammdaten.rules.bean.AnlageBean;
import de.wps.bube.basis.stammdaten.rules.bean.AnlagenteilBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetreiberBean;
import de.wps.bube.basis.stammdaten.rules.bean.BetriebsstaetteBean;
import de.wps.bube.basis.stammdaten.rules.bean.QuelleBean;

@ExtendWith(MockitoExtension.class)
class RegelPruefungServiceTest extends AbstractRegelPruefungServiceTest {

    Function<RegelObjekt, List<PruefObjekt>> pruefObjektCollector(Betriebsstaette betriebsstaette) {
        return regelObjekt -> berichtPruefungService.getPruefObjekte(betriebsstaette, regelObjekt, new HashMap<>());
    }

    @Test
    void testContextVerbietetSchreibZugriff() {

        Betriebsstaette bst = BetriebsstaetteBuilder.aFullBetriebsstaette().name("Test-BST").build();
        EvaluationContext context = regelPruefungService.setupContext(Gueltigkeitsjahr.of(2020), Land.BREMEN, 1L);
        context.setVariable("OBJ", bst);

        SpelExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("#OBJ.name == \"Test-BST\"");
        assertThat(expression.getValue(context)).isEqualTo(Boolean.TRUE);

        Expression expression2 = parser.parseExpression("#OBJ.name = \"Test-BST2\"");
        assertThrows(SpelEvaluationException.class, () -> expression2.getValue(context));

    }

    @Test
    void testRegelMitSyntaxFehler() {

        Regel regel = aRegel().objekte(List.of(BST)).code("foo bar").build();
        Betriebsstaette bst = aBetriebsstaette(1L).build();

        assertThatExceptionOfType(ParseException.class).isThrownBy(
                () -> regelPruefungService.pruefeRegeln(regel, bst.getLand(), bst.getGueltigkeitsjahrFromBerichtsjahr(),
                        bst.getZustaendigeBehoerde().getId(), pruefObjektCollector(bst)));
    }

    @Test
    void testKriterium02() {

        Regel regel = aRegel()
                .objekte(List.of(BETREIBER))
                .code("#OBJ.adresse != null" +
                        "&& #OBJ.adresse.ort != null " +
                        "&& ((#OBJ.adresse.plz != null && #OBJ.adresse.strasse != null)" +
                        "   || (#OBJ.adresse.postfachPlz != null && #OBJ.adresse.postfach != null))")
                .build();

        Adresse adresse;
        Betreiber betreiber;
        Betriebsstaette betriebsstaette;
        RegelPruefungErgebnis ergebnis;

        // Betreiber ohne Adresse
        betreiber = BetreiberBuilder.aBetreiber(1L).build();
        betriebsstaette = aBetriebsstaette().betreiber(betreiber).build();

        ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(BetreiberBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(1L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

        // Adresse ohne Ort
        adresse = AdresseBuilder.anAdresse().build();
        betreiber = BetreiberBuilder.aBetreiber(2L).adresse(adresse).build();
        betriebsstaette = aBetriebsstaette().betreiber(betreiber).build();

        ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(BetreiberBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(2L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

        // Adresse mit PLZ / Straße
        adresse = AdresseBuilder.anAdresse().strasse("Hans-Henny-Jahnn-Weg 29").plz("22085").ort("Hamburg").build();
        betreiber = BetreiberBuilder.aBetreiber(3L).adresse(adresse).build();
        betriebsstaette = aBetriebsstaette().betreiber(betreiber).build();

        ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getGueltigeObjekte().get(0)).isInstanceOf(BetreiberBean.class);
        assertThat(ergebnis.getGueltigeObjekte().get(0).getId()).isEqualTo(3L);

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

        // Adresse mit Postfach
        adresse = AdresseBuilder.anAdresse().postfach("20 17 44").postfachPlz("20243").ort("Hamburg").build();
        betreiber = BetreiberBuilder.aBetreiber(4L).adresse(adresse).build();
        betriebsstaette = aBetriebsstaette().betreiber(betreiber).build();

        ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getGueltigeObjekte().get(0)).isInstanceOf(BetreiberBean.class);
        assertThat(ergebnis.getGueltigeObjekte().get(0).getId()).isEqualTo(4L);

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();
    }

    @Test
    void testKriterium06FuerBetriebsstaetten() {

        Regel regel = aRegel()
                .objekte(List.of(BST))
                .code("#OBJ.geoPunkt != null")
                .build();

        Betriebsstaette betriebsstaetteOhneGeoPunkt = aBetriebsstaette(1L).build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel,
                betriebsstaetteOhneGeoPunkt.getLand(),
                betriebsstaetteOhneGeoPunkt.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaetteOhneGeoPunkt.getZustaendigeBehoerde().getId(),
                pruefObjektCollector(betriebsstaetteOhneGeoPunkt));

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(BetriebsstaetteBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(1L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

        Betriebsstaette betriebsstaetteMitGeoPunkt = aBetriebsstaette(2L)
                .geoPunkt(new GeoPunkt(1, -1, null))
                .build();
        ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaetteOhneGeoPunkt.getLand(),
                betriebsstaetteOhneGeoPunkt.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaetteOhneGeoPunkt.getZustaendigeBehoerde().getId(),
                pruefObjektCollector(betriebsstaetteMitGeoPunkt));

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getGueltigeObjekte().get(0)).isInstanceOf(BetriebsstaetteBean.class);
        assertThat(ergebnis.getGueltigeObjekte().get(0).getId()).isEqualTo(2L);

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

    }

    @Test
    void testKriterium06FuerAnlagen() {

        Regel regel = aRegel()
                .objekte(List.of(ANLAGE))
                .code("#OBJ.geoPunkt != null")
                .build();
        Anlage anlageOhneGeoPunkt = AnlageBuilder.anAnlage(1L).build();
        Anlage anlageMitGeoPunkt = AnlageBuilder.anAnlage(2L).geoPunkt(new GeoPunkt(1L, -1L, null)).build();
        Betriebsstaette betriebsstaette = aBetriebsstaette()
                .anlagen(anlageOhneGeoPunkt, anlageMitGeoPunkt)
                .build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(1);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        if (gueltigeIds.length > 0) {
            assertThat(gueltigeIds).containsAnyOf(2);
        }

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(AnlageBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(1L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

    }

    @Test
    void testKriterium06FuerAnlagenteile() {

        Regel regel = aRegel()
                .objekte(List.of(ANLAGENTEIL))
                .code("#OBJ.geoPunkt != null")
                .build();
        Anlagenteil anlagenteilOhneGeoPunkt = AnlagenteilBuilder.anAnlagenteil(1L).build();
        Anlagenteil anlagenteilMitGeoPunkt = AnlagenteilBuilder.anAnlagenteil(2L)
                                                               .geoPunkt(new GeoPunkt(1L, -1L, null))
                                                               .build();
        Anlage anlage = AnlageBuilder.anAnlage().anlagenteile(anlagenteilOhneGeoPunkt, anlagenteilMitGeoPunkt).build();
        Betriebsstaette betriebsstaette = aBetriebsstaette().anlagen(anlage).build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(1);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        if (gueltigeIds.length > 0) {
            assertThat(gueltigeIds).containsAnyOf(2);
        }

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(AnlagenteilBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(1L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

    }

    @Test
    void testKriterium06FuerQuellen() {

        Regel regel = aRegel().objekte(List.of(QUELLE)).code("#OBJ.geoPunkt != null").build();
        Quelle quelleOhneGeoPunkt = QuelleBuilder.aQuelle().id(1L).build();
        Quelle quelleMitGeoPunkt = QuelleBuilder.aQuelle().id(2L).geoPunkt(new GeoPunkt(1L, -1L, null)).build();
        Betriebsstaette betriebsstaette = aBetriebsstaette()
                .quellen(quelleOhneGeoPunkt, quelleMitGeoPunkt)
                .build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(1);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        if (gueltigeIds.length > 0) {
            assertThat(gueltigeIds).containsAnyOf(2);
        }

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSize(1);
        assertThat(ergebnis.getUngueltigeObjekte().get(0)).isInstanceOf(QuelleBean.class);
        assertThat(ergebnis.getUngueltigeObjekte().get(0).getId()).isEqualTo(1L);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

    }

    @Test
    void testKriterium06MitUngueltigemCode() {

        Regel regel = aRegel().objekte(List.of(BST)).code("#OBJ.geoPunktt != null").build();
        Betriebsstaette betriebsstaette = aBetriebsstaette(1L).build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).hasSize(1);

        PruefObjekt key = ergebnis.getPruefungsFehler().keySet().iterator().next();
        assertThat(key).isInstanceOf(BetriebsstaetteBean.class);
        assertThat(key.getId()).isEqualTo(1L);

        EvaluationException exception = ergebnis.getPruefungsFehler().get(key);
        assertThat(exception.getMessage()).contains("geoPunktt");

    }

    @Test
    void testKriterium06MitGemischtenObjekten() {

        Regel regel = aRegel()
                .objekte(List.of(BST, ANLAGE, ANLAGENTEIL, QUELLE, BETREIBER))
                .code("#OBJ.geoPunkt != null")
                .build();

        GeoPunkt geoPunkt = new GeoPunkt(1, -1, null);

        Quelle quelle1 = QuelleBuilder.aQuelle(1L).build();
        Quelle quelle2 = QuelleBuilder.aQuelle(2L).geoPunkt(geoPunkt).build();
        Quelle quelle3 = QuelleBuilder.aQuelle(3L).build();
        Quelle quelle4 = QuelleBuilder.aQuelle(4L).geoPunkt(geoPunkt).build();

        Anlagenteil anlagenteil5 = AnlagenteilBuilder.anAnlagenteil(5L).build();
        Anlagenteil anlagenteil6 = AnlagenteilBuilder.anAnlagenteil(6L).geoPunkt(geoPunkt).build();

        Anlage anlage7 = AnlageBuilder.anAnlage(7L).geoPunkt(geoPunkt).build();
        Anlage anlage8 = AnlageBuilder.anAnlage(8L).anlagenteile(anlagenteil5, anlagenteil6).build();
        Anlage anlage9 = AnlageBuilder.anAnlage(9L).quellen(quelle1, quelle2).build();

        Betreiber betreiber = BetreiberBuilder.aBetreiber(10L).build();

        Betriebsstaette betriebsstaette = aBetriebsstaette(11L)
                .geoPunkt(geoPunkt)
                .betreiber(betreiber)
                .anlagen(anlage7, anlage8, anlage9)
                .quellen(quelle3, quelle4)
                .build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(5);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        assertThat(gueltigeIds).containsAnyOf(2, 4, 6, 7, 11);

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSizeLessThanOrEqualTo(5);
        long[] ungueltigeIds = ergebnis.getUngueltigeObjekte()
                                       .stream()
                                       .mapToLong(PruefObjekt::getId)
                                       .sorted()
                                       .toArray();
        assertThat(ungueltigeIds).containsAnyOf(1, 3, 5, 8, 9);

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).hasSize(1);

        PruefObjekt key = ergebnis.getPruefungsFehler().keySet().iterator().next();
        assertThat(key).isInstanceOf(BetreiberBean.class);
        assertThat(key.getId()).isEqualTo(10L);

        EvaluationException exception = ergebnis.getPruefungsFehler().get(key);
        assertThat(exception.getMessage()).contains("geoPunkt");

    }

    @Test
    void testKriterium34() {

        Regel regel = aRegel()
                .objekte(List.of(BST, ANLAGE, ANLAGENTEIL, QUELLE))
                .code("#OBJ.localId != null")
                .build();

        Quelle quelle = QuelleBuilder.aQuelle(1L).build();

        Anlagenteil anlagenteil2 = AnlagenteilBuilder.anAnlagenteil(2L).build();
        Anlagenteil anlagenteil3 = AnlagenteilBuilder.anAnlagenteil(3L).localId("3").build();

        Anlage anlage4 = AnlageBuilder.anAnlage(4L).localId("4").build();
        Anlage anlage5 = AnlageBuilder.anAnlage(5L).anlagenteile(anlagenteil2, anlagenteil3).build();
        Anlage anlage6 = AnlageBuilder.anAnlage(6L).quellen(quelle).build();

        Betriebsstaette betriebsstaette = aBetriebsstaette(7L)
                .localId("7")
                .anlagen(anlage4, anlage5, anlage6)
                .build();
        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(3);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        if (gueltigeIds.length > 0) {
            assertThat(gueltigeIds).containsAnyOf(3, 4, 7);
        }

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSizeLessThanOrEqualTo(3);
        long[] ungueltigeIds = ergebnis.getUngueltigeObjekte()
                                       .stream()
                                       .mapToLong(PruefObjekt::getId)
                                       .sorted()
                                       .toArray();
        if (ungueltigeIds.length > 0) {
            assertThat(ungueltigeIds).containsAnyOf(2, 5, 6);
        }

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).hasSize(1);

        PruefObjekt key = ergebnis.getPruefungsFehler().keySet().iterator().next();
        assertThat(key).isInstanceOf(QuelleBean.class);
        assertThat(key.getId()).isEqualTo(1L);

        EvaluationException exception = ergebnis.getPruefungsFehler().get(key);
        assertThat(exception.getMessage()).contains("localId");

    }

    @Test
    void testKriterium53() {

        Regel regel = aRegel().objekte(List.of(EUREG_ANLAGE)).code("#OBJ.visits > 0").build();

        Anlage anlage1 = AnlageBuilder.anAnlage(1L).build();
        Anlage anlage2 = AnlageBuilder.anAnlage(2L).build();
        Betriebsstaette betriebsstaette = aBetriebsstaette()
                .anlagen(anlage1, anlage2)
                .build();

        EURegAnlage euRegAnlageOhneBesichtigung = EURegAnlageBuilder.anEURegAnlage(1L)
                                                                    .anlage(anlage1)
                                                                    .visits(0)
                                                                    .build();
        EURegAnlage euRegAnlageMitBesichtigung = EURegAnlageBuilder.anEURegAnlage(2L).anlage(anlage2).visits(1).build();

        when(euRegistryService.findAnlagenBerichtByAnlageId(1L)).thenReturn(Optional.of(euRegAnlageOhneBesichtigung));
        when(euRegistryService.findAnlagenBerichtByAnlageId(2L)).thenReturn(Optional.of(euRegAnlageMitBesichtigung));

        RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(regel, betriebsstaette.getLand(),
                betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette)
        );

        assertThat(ergebnis).isNotNull();

        assertThat(ergebnis.getGueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getGueltigeObjekte()).hasSizeLessThanOrEqualTo(1);
        long[] gueltigeIds = ergebnis.getGueltigeObjekte().stream().mapToLong(PruefObjekt::getId).sorted().toArray();
        if (gueltigeIds.length > 0) {
            assertThat(gueltigeIds).containsAnyOf(2);
        }

        assertThat(ergebnis.getUngueltigeObjekte()).isNotNull();
        assertThat(ergebnis.getUngueltigeObjekte()).hasSizeLessThanOrEqualTo(1);
        long[] ungueltigeIds = ergebnis.getUngueltigeObjekte()
                                       .stream()
                                       .mapToLong(PruefObjekt::getId)
                                       .sorted()
                                       .toArray();
        if (ungueltigeIds.length > 0) {
            assertThat(ungueltigeIds).containsAnyOf(1);
        }

        assertThat(ergebnis.getPruefungsFehler()).isNotNull();
        assertThat(ergebnis.getPruefungsFehler()).isEmpty();

    }

    /**
     * Betriebsstätte im vergangenen Berichtsjahr nicht als "dauerhaft stillgelegt/abgebaut" oder
     * "unterliegt nicht der IE-RL" berichtet, muss daher berichtet werden.
     */
    @Nested
    class Regel36 {

        private final Regel regel36 = aRegel()
                .objekte(TEILBERICHT)
                .code("""
                        #OBJ.vorgaenger.berichte
                            .?[!{"03", "04"}.contains(betriebsstaette.betriebsstatus.schluessel) && nachfolger == null]
                            .![betriebsstaette]""")
                .build();
        private final Referenz j2019 = aJahrReferenz(2019).id(2019L).build();
        private final Referenz j2020 = aJahrReferenz(2020).id(2020L).build();
        private final XeurTeilbericht xeurTeilbericht = new XeurTeilbericht(j2020, HAMBURG);

        @BeforeEach
        void setUp() {
            xeurTeilbericht.setXeurFile("<xeur />");
            xeurTeilbericht.setProtokoll("Protokoll");
            when(referenzenService.getJahr("2019")).thenReturn(j2019);
        }

        @Test
        void testRegelErfuellt_keinVorjahresbericht() {
            var result = berichtPruefungService.pruefeTeilberichtRegeln(regel36, xeurTeilbericht);

            assertThat(result).isEmpty();
            verify(xeurTeilberichtCrudService).getAllTeilberichte(j2019, xeurTeilbericht.getLand());
        }

        @Test
        void testRegelErfuellt_imVorjahrAbgebaut() {
            var vorjahresbericht = new XeurTeilbericht(j2019, HAMBURG);
            vorjahresbericht.setXeurFile("<xeur />");
            vorjahresbericht.setProtokoll("Protokoll");
            var euRegBetriebsstaette = aEURegBetriebsstaette()
                    .betriebsstaette(aBetriebsstaette()
                            .betriebsstatus(aBetriebsstatusReferenz("03").build())
                            .localId("localId")
                            .build())
                    .build();
            vorjahresbericht.setLocalIds(List.of(euRegBetriebsstaette.getLocalId()));
            when(xeurTeilberichtCrudService.getAllTeilberichte(j2019, xeurTeilbericht.getLand()))
                    .thenReturn(List.of(vorjahresbericht));
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(j2019.getSchluessel(),
                    euRegBetriebsstaette.getLocalId())).thenReturn(Optional.of(euRegBetriebsstaette));

            var result = berichtPruefungService.pruefeTeilberichtRegeln(regel36, xeurTeilbericht);

            assertThat(result).isEmpty();
        }

        @Test
        void testRegelErfuellt_nichtAbgebautUndVorhanden() {
            var vorjahresbericht = new XeurTeilbericht(j2019, HAMBURG);
            vorjahresbericht.setXeurFile("<xeur />");
            vorjahresbericht.setProtokoll("Protokoll");
            var betriebsstaetteBuilder = aBetriebsstaette()
                    .betriebsstatus(aBetriebsstatusReferenz("01").build())
                    .localId("localId");
            var euRegBst2020 = aEURegBetriebsstaette()
                    .id(2020L)
                    .betriebsstaette(betriebsstaetteBuilder.berichtsjahr(j2020).build())
                    .build();
            var euRegBst2019 = aEURegBetriebsstaette()
                    .id(2019L)
                    .betriebsstaette(betriebsstaetteBuilder.berichtsjahr(j2019).build())
                    .build();
            vorjahresbericht.setLocalIds(List.of(euRegBst2019.getLocalId()));
            when(xeurTeilberichtCrudService.getAllTeilberichte(j2019, xeurTeilbericht.getLand()))
                    .thenReturn(List.of(vorjahresbericht));
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(j2019.getSchluessel(),
                    euRegBst2020.getLocalId())).thenReturn(Optional.of(euRegBst2019));
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(j2020.getSchluessel(),
                    euRegBst2020.getLocalId())).thenReturn(Optional.of(euRegBst2020));

            var result = berichtPruefungService.pruefeTeilberichtRegeln(regel36, xeurTeilbericht);

            assertThat(result).isEmpty();
        }

        @Test
        void testRegelNichtErfuellt() {
            var vorjahresbericht = new XeurTeilbericht(j2019, HAMBURG);
            vorjahresbericht.setXeurFile("<xeur />");
            vorjahresbericht.setProtokoll("Protokoll");
            var betriebsstaetteBuilder = aBetriebsstaette()
                    .betriebsstatus(aBetriebsstatusReferenz("01").build())
                    .localId("localId");
            var euRegBst2019 = aEURegBetriebsstaette().id(2019L)
                                                      .betriebsstaette(
                                                              betriebsstaetteBuilder.berichtsjahr(j2019).build())
                                                      .build();
            vorjahresbericht.setLocalIds(List.of(euRegBst2019.getLocalId()));
            when(xeurTeilberichtCrudService.getAllTeilberichte(j2019, xeurTeilbericht.getLand()))
                    .thenReturn(List.of(vorjahresbericht));
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId(j2019.getSchluessel(),
                    euRegBst2019.getLocalId())).thenReturn(Optional.of(euRegBst2019));
            when(euRegistryService.findBetriebsstaettenBerichtByJahrAndLocalId("2020", "localId"))
                    .thenReturn(Optional.empty());

            var result = berichtPruefungService.pruefeTeilberichtRegeln(regel36, xeurTeilbericht);

            assertThat(result).isNotNull();
        }
    }

    @Nested
    class EURegFTest {

        private Betriebsstaette betriebsstaette;
        private Regel euRegFRegel;
        private Regel euRegAnlageRegel;

        @BeforeEach
        void setUp() {

            Anlage anlage1 = AnlageBuilder.anAnlage(1L).build();
            Anlage anlage2 = AnlageBuilder.anAnlage(2L).build();
            betriebsstaette = aBetriebsstaette()
                    .anlagen(anlage1, anlage2)
                    .build();

            Berichtsdaten euRegAnlageBerichtsdaten = BerichtsdatenBuilder.anEuRegAnlBerichtsdaten().build();
            Berichtsdaten euRegFBerichtsdaten = BerichtsdatenBuilder.anEuRegFBerichtsdaten().build();
            FeuerungsanlageBerichtsdaten feuerungsanlageBerichtsdaten
                    = FeuerungsanlageBerichtsdatenBuilder.aFeuerungsanlageBerichtsdaten()
                                                         .gesamtKapazitaetAbfall(1.0)
                                                         .build();

            EURegAnlage euRegAnlage1 = EURegAnlageBuilder.anEURegAnlage(1L)
                                                         .anlage(anlage1)
                                                         .berichtsdaten(euRegAnlageBerichtsdaten)
                                                         .feuerungsanlage(feuerungsanlageBerichtsdaten)
                                                         .visits(1)
                                                         .build();
            EURegAnlage euRegAnlage2 = EURegAnlageBuilder.anEURegAnlage(2L)
                                                         .anlage(anlage2)
                                                         .berichtsdaten(euRegFBerichtsdaten)
                                                         .feuerungsanlage(feuerungsanlageBerichtsdaten)
                                                         .visits(2)
                                                         .build();

            when(euRegistryService.findAnlagenBerichtByAnlageId(1L)).thenReturn(Optional.of(euRegAnlage1));
            when(euRegistryService.findAnlagenBerichtByAnlageId(2L)).thenReturn(Optional.of(euRegAnlage2));

            euRegFRegel = aRegel().code("#OBJ.feuerungsanlage.gesamtKapazitaetAbfall < 2.0").build();
            euRegAnlageRegel = aRegel().code("#OBJ.visits > 0").objekte(List.of(EUREG_ANLAGE)).build();

        }

        @Test
        void ungueltigeEURegFRegel() {

            euRegFRegel.setObjekte(List.of(EUREG_ANLAGE));

            RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(euRegFRegel,
                    betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                    betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette));

            assertThat(ergebnis).isNotNull();
            assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();
            assertThat(ergebnis.getGueltigeObjekte()).isEmpty();

            assertThat(ergebnis.getPruefungsFehler()).hasSize(2);
            for (EvaluationException fehler : ergebnis.getPruefungsFehler().values()) {
                assertThat(fehler.getMessage()).contains("Property or field 'feuerungsanlage' cannot be found");
            }

        }

        @Test
        void gueltigeEURegFRegel() {

            euRegFRegel.setObjekte(List.of(EUREG_F));

            RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(euRegFRegel,
                    betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                    betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette));

            assertThat(ergebnis).isNotNull();
            assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();

            assertThat(ergebnis.getGueltigeObjekte()).hasSize(1);
            PruefObjekt gueltig = ergebnis.getGueltigeObjekte().get(0);
            assertThat(gueltig.getId()).isEqualTo(2L);

            assertThat(ergebnis.getPruefungsFehler()).isEmpty();

        }

        @Test
        void euRegAnlageRegel() {

            RegelPruefungErgebnis ergebnis = regelPruefungService.pruefeRegeln(euRegAnlageRegel,
                    betriebsstaette.getLand(), betriebsstaette.getGueltigkeitsjahrFromBerichtsjahr(),
                    betriebsstaette.getZustaendigeBehoerde().getId(), pruefObjektCollector(betriebsstaette));

            assertThat(ergebnis).isNotNull();
            assertThat(ergebnis.getUngueltigeObjekte()).isEmpty();
            assertThat(ergebnis.getPruefungsFehler()).isEmpty();

            assertThat(ergebnis.getGueltigeObjekte()).hasSize(2);

        }

    }

}
