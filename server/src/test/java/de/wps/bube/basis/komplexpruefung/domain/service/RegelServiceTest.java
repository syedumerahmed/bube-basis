package de.wps.bube.basis.komplexpruefung.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.komplexpruefung.domain.RegelException;
import de.wps.bube.basis.komplexpruefung.domain.entity.Regel;
import de.wps.bube.basis.komplexpruefung.domain.entity.RegelBuilder;
import de.wps.bube.basis.komplexpruefung.domain.vo.RegelTyp;
import de.wps.bube.basis.komplexpruefung.persistence.RegelRepository;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;

@ExtendWith(MockitoExtension.class)
class RegelServiceTest {

    enum RegelServiceAction implements BiFunction<RegelService, Regel, Regel> {

        CREATE {
            @Override
            public Regel apply(RegelService regelService, Regel regel) {
                return regelService.createRegel(regel);
            }
        },
        UPDATE {
            @Override
            public Regel apply(RegelService regelService, Regel regel) {
                return regelService.updateRegel(regel);
            }
        },
        DELETE {
            @Override
            public Regel apply(RegelService regelService, Regel regel) {
                regelService.deleteRegel(regel.getNummer());
                return null;
            }
        }

    }

    @Mock
    private RegelRepository repository;
    @Mock
    private AktiverBenutzer aktiverBenutzer;

    private RegelService regelService;

    @BeforeEach
    void setUp() {
        //        when(benutzer.getUsername()).thenReturn("testuser");
        regelService = new RegelService(repository, aktiverBenutzer);
    }

    @Test
    void readAllRegeln() {

        // Given
        Regel regel1 = RegelBuilder.aRegel().nummer(1L).build();
        Regel regel2 = RegelBuilder.aRegel().nummer(2L).build();
        when(repository.findAll()).thenReturn(List.of(regel1, regel2));

        // When
        List<Regel> regeln = regelService.readAllRegeln();

        // Then
        assertThat(regeln).containsExactly(regel1, regel2);
        verifyNoMoreInteractions(repository);
        verifyNoInteractions(aktiverBenutzer);

    }

    @Test
    void loadRegelByNummer_illegalArgument() {

        assertThrows(IllegalArgumentException.class, () -> regelService.loadRegelByNummer(null));

        verifyNoInteractions(repository);
        verifyNoInteractions(aktiverBenutzer);
    }

    @Test
    void loadRegelByNummer_invalidArgument() {

        // When
        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class,
                () -> regelService.loadRegelByNummer(1L));

        // Then
        assertThat(exception.getMessage()).isEqualTo(
                "Regel mit Nummer 1 wurde nicht gefunden oder keine Berechtigung zum Öffnen des Objektes");

        verify(repository).findByNummer(1L);
        verifyNoMoreInteractions(repository);
        verifyNoInteractions(aktiverBenutzer);

    }

    @Test
    void loadRegelByNummer_validArgument() {

        // Given
        Regel regel = RegelBuilder.aRegel().nummer(1L).build();
        when(repository.findByNummer(1L)).thenReturn(Optional.of(regel));

        // When
        Regel regel1 = regelService.loadRegelByNummer(1L);

        // Then
        assertThat(regel1).isSameAs(regel);

        verifyNoMoreInteractions(repository);
        verifyNoInteractions(aktiverBenutzer);
    }

    @ParameterizedTest
    @EnumSource(value = RegelServiceAction.class, names = {"CREATE", "UPDATE"})
    void actionOhneDatenberechtigung(RegelServiceAction action) {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(false);

        // When
        RegelException exception = assertThrows(RegelException.class, () -> action.apply(regelService, regel));

        // Then
        assertThat(exception.getMessage()).isEqualTo("Keine Datenberechtigung für Schreiben der Regel");

        verifyNoInteractions(repository);
    }

    @Test
    void deleteOhneDatenberechtigung() {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(false);
        when(repository.findByNummer(anyLong())).thenReturn(Optional.of(regel));

        // When
        RegelException exception = assertThrows(RegelException.class,
                () -> RegelServiceAction.DELETE.apply(regelService, regel));

        // Then
        assertThat(exception.getMessage()).isEqualTo("Keine Datenberechtigung für Löschen der Regel");

        verifyNoMoreInteractions(repository);
    }

    @ParameterizedTest
    @EnumSource(value = RegelServiceAction.class, names = { "CREATE", "UPDATE" })
    void actionOhneGueltigkeit(RegelServiceAction action) {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(true);
        doThrow(new RegelException("Gültig bis liegt zeitlich vor Gültig von")).when(regel).pruefeGueltigkeit();

        // When
        RegelException exception = assertThrows(RegelException.class, () -> action.apply(regelService, regel));

        // Then
        assertThat(exception.getMessage()).isEqualTo("Gültig bis liegt zeitlich vor Gültig von");

        verifyNoInteractions(repository);
    }

    @ParameterizedTest
    @EnumSource(value = RegelServiceAction.class, names = { "CREATE", "UPDATE" })
    void actionMitFalschenObjekten(RegelServiceAction action) {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(true);
        doThrow(new RegelException("Ein oder mehrere falsche Objekte für Gruppe A")).when(regel)
                                                                                    .pruefeObjekteInGruppe();

        // When
        RegelException exception = assertThrows(RegelException.class, () -> action.apply(regelService, regel));

        // Then
        assertThat(exception.getMessage()).isEqualTo("Ein oder mehrere falsche Objekte für Gruppe A");

        verifyNoInteractions(repository);
    }

    @ParameterizedTest
    @EnumSource(value = RegelServiceAction.class, names = { "CREATE", "UPDATE" })
    void actionMitFalschemLand(RegelServiceAction action) {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(true);
        doThrow(new RegelException("Typ bundeseinheitlich kann nur bei Regeln mit Land=Deutschland gesetzt werden"))
                .when(regel).pruefeTypBundeseinheitlich();

        // When
        RegelException exception = assertThrows(RegelException.class, () -> action.apply(regelService, regel));

        // Then
        assertThat(exception.getMessage()).isEqualTo(
                "Typ bundeseinheitlich kann nur bei Regeln mit Land=Deutschland gesetzt werden");

        verifyNoInteractions(repository);
    }

    @ParameterizedTest
    @EnumSource(value = RegelServiceAction.class, names = { "CREATE", "UPDATE" })
    void actionOhneFehler(RegelServiceAction action) {

        // Given
        Regel regel = mock(Regel.class);
        Regel regel2 = RegelBuilder.aRegel().build();
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(true);
        when(repository.save(regel)).thenReturn(regel2);

        // When
        regel = action.apply(regelService, regel);

        // Then
        assertThat(regel).isSameAs(regel2);
        verifyNoMoreInteractions(repository);
    }

    @Test
    void deleteMitFalscherNummer() {

        // Given
        Regel regel = mock(Regel.class);
        when(repository.findByNummer(41L)).thenReturn(Optional.empty());

        // When
        BubeEntityNotFoundException exception = assertThrows(BubeEntityNotFoundException.class,
                () -> regelService.deleteRegel(41L));

        // Then
        assertThat(exception.getMessage()).startsWith("Regel mit Nummer 41 wurde nicht gefunden");
        verifyNoMoreInteractions(repository);

    }

    @Test
    void deleteOhneFehler() {

        // Given
        Regel regel = mock(Regel.class);
        when(regel.hatDatenberechtigung(anyBoolean(), any(), anyBoolean())).thenReturn(true);
        when(repository.findByNummer(42L)).thenReturn(Optional.of(regel));

        // When
        regelService.deleteRegel(42L);

        // Then
        verify(repository).delete(regel);
        verifyNoMoreInteractions(repository);

    }

    @Test
    void typAendernMitUngueltigerRegel() {

        assertThrows(BubeEntityNotFoundException.class, () -> regelService.typAendern(List.of(1L), RegelTyp.WARNUNG));

        verify(repository).findByNummer(1L);
        verifyNoMoreInteractions(repository);
        verifyNoInteractions(aktiverBenutzer);

    }

    @Test
    void typAendernMitUngueltigemLand() {

        // Given
        Regel regel = RegelBuilder.aRegel().nummer(2L).land(Land.BERLIN).typ(RegelTyp.FEHLER).build();
        when(repository.findByNummer(2L)).thenReturn(Optional.of(regel));

        // When
        RegelException exception = assertThrows(RegelException.class,
                () -> regelService.typAendern(List.of(2L), RegelTyp.WARNUNG));

        // Then
        assertThat(exception.getMessage()).isEqualTo(
                "Der Typ kann nur bei Regeln mit Land=Deutschland geändert werden");

        verify(repository).findByNummer(2L);
        verifyNoMoreInteractions(repository);
        verify(aktiverBenutzer).getLand();
        verifyNoMoreInteractions(aktiverBenutzer);
    }

    @Test
    void typAendernMitGueltigemLand() {

        // Given
        Regel regel = RegelBuilder.aRegel().nummer(3L).land(Land.DEUTSCHLAND).typ(RegelTyp.FEHLER).build();
        when(repository.findByNummer(3L)).thenReturn(Optional.of(regel));
        when(aktiverBenutzer.getLand()).thenReturn(Land.BRANDENBURG);

        // When
        regelService.typAendern(List.of(3L), RegelTyp.WARNUNG);

        // Then
        verify(repository).save(argThat(r -> {
            assertThat(r.getNummer()).isEqualTo(3L);
            assertThat(r.getTyp()).isSameAs(RegelTyp.FEHLER);
            assertThat(r.getTyp(Land.HAMBURG)).isSameAs(RegelTyp.FEHLER);
            assertThat(r.getTyp(Land.BRANDENBURG)).isSameAs(RegelTyp.WARNUNG);
            return true;
        }));

        verify(repository).findByNummer(3L);
        verify(repository).save(any(Regel.class));
        verifyNoMoreInteractions(repository);
        verifyNoMoreInteractions(aktiverBenutzer);
    }

}
