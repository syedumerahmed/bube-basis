package de.wps.bube.basis.arch;

import static com.tngtech.archunit.base.DescribedPredicate.alwaysTrue;
import static com.tngtech.archunit.base.DescribedPredicate.not;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.assignableTo;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.belongToAnyOf;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.equivalentTo;
import static com.tngtech.archunit.core.domain.properties.CanBeAnnotated.Predicates.annotatedWith;
import static com.tngtech.archunit.core.domain.properties.CanBeAnnotated.Predicates.metaAnnotatedWith;
import static com.tngtech.archunit.lang.conditions.ArchPredicates.are;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Expression;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;

import de.wps.bube.basis.base.persistence.Column;
import de.wps.bube.basis.base.web.WebappProperties;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.xml.XMLService;
import de.wps.bube.basis.benutzerverwaltung.domain.entity.Benutzer;
import de.wps.bube.basis.referenzdaten.domain.entity.Behoerde;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.sicherheit.domain.security.BerechtigungsInMemoryAccessor;
import de.wps.bube.basis.stammdaten.domain.entity.Betreiber;

@AnalyzeClasses(packages = "de.wps.bube.basis", importOptions = ImportOption.DoNotIncludeTests.class)
class TechnicalLayerTest {

    @ArchTest
    public static final ArchRule webControllers = classes()
            .that()
            .areAnnotatedWith(RestController.class)
            .should()
            .resideInAPackage("..web..")
            .andShould()
            .haveSimpleNameEndingWith("Controller");

    @ArchTest
    public static final ArchRule services = classes()
            // TODO
            .that().resideOutsideOfPackage("de.wps.bube.basis.base..")
            .and()
            .areAnnotatedWith(Service.class)
            .should()
            .resideInAPackage("..domain.service..")
            .andShould()
            .haveSimpleNameEndingWith("Service").orShould().haveSimpleNameEndingWith("Job");

    // TODO
    @ArchTest
    @ArchIgnore
    public static final ArchRule servicesSecured = methods()
            .that()
            .arePublic()
            .and()
            // TODO
            .areDeclaredInClassesThat(are(not(equivalentTo(XMLService.class))).and(annotatedWith(Service.class)))
            .should()
            .beMetaAnnotatedWith(Secured.class);

    @ArchTest
    public static final ArchRule webLayer = bubeArchitecture()
            .ignoreDependency(alwaysTrue(), belongToAnyOf(ListStateDto.class))
            .whereLayer("Web").mayNotBeAccessedByAnyLayer();

    @ArchTest
    public static final ArchRule configurationLayer = bubeArchitecture()
            .ignoreDependency(alwaysTrue(), belongToAnyOf(WebappProperties.class))

            .whereLayer("Configuration").mayNotBeAccessedByAnyLayer();

    @ArchTest
    public static final ArchRule xmlLayer = bubeArchitecture()
            .whereLayer("XML").mayOnlyBeAccessedByLayers("Service", "Web");

    @ArchTest
    public static final ArchRule rulesLayer = bubeArchitecture()
            .whereLayer("Rules").mayOnlyBeAccessedByLayers("Service", "ValueObject");

    @ArchTest
    public static final ArchRule serviceLayer = bubeArchitecture()
            .whereLayer("Service").mayOnlyBeAccessedByLayers("Configuration", "XML", "Web", "Rules");

    @ArchTest
    public static final ArchRule securityLayer = bubeArchitecture()
            // TODO:
            .ignoreDependency(Benutzer.class, BerechtigungsInMemoryAccessor.class)
            .ignoreDependency(Betreiber.class, BerechtigungsInMemoryAccessor.class)

            .whereLayer("Security").mayOnlyBeAccessedByLayers("Configuration", "Service", "Web");

    @ArchTest
    public static final ArchRule persistenceLayer = bubeArchitecture()
            .ignoreDependency(alwaysTrue(), belongToAnyOf(Column.class))

            .whereLayer("Persistence").mayOnlyBeAccessedByLayers("Configuration", "Service", "Domain");

    @ArchTest
    public static final ArchRule entityLayer = bubeArchitecture()
            // Ignoriere generierte QueryDSL Klassen
            .ignoreDependency(alwaysTrue(), assignableTo(Expression.class))
            // TODO:
            .ignoreDependency(alwaysTrue(), belongToAnyOf(Referenz.class, Behoerde.class))

            .whereLayer("Entity")
            .mayOnlyBeAccessedByLayers("Configuration", "XML", "Web", "Service", "Domain", "Security", "Persistence",
                    "Rules");

    private static Architectures.LayeredArchitecture bubeArchitecture() {
        return layeredArchitecture()
                .layer("Configuration").definedBy(metaAnnotatedWith(Configuration.class)
                        .or(metaAnnotatedWith(ControllerAdvice.class)))
                .layer("Web").definedBy("..web..")
                .layer("XML").definedBy("..xml..")
                .layer("Domain").definedBy("..domain", "..domain.event", "..domain.model")
                .layer("Service").definedBy("..domain.service..")
                .layer("Security").definedBy("..security..")
                .layer("Persistence").definedBy("..persistence..")
                .layer("Entity").definedBy("..domain.entity..")
                .layer("ValueObject").definedBy("..domain.vo..")
                .layer("Rules").definedBy("..rules..")
                ;
    }
}
