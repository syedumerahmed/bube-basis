package de.wps.bube.basis.euregistry.domain.service;

import static de.wps.bube.basis.euregistry.domain.entity.QEURegBetriebsstaette.eURegBetriebsstaette;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.Pageable.unpaged;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.Expressions;

import de.wps.bube.basis.euregistry.persistence.EURegBetriebsstaetteRepository;
import de.wps.bube.basis.euregistry.security.EuRegBstBerechtigungsAccessor;
import de.wps.bube.basis.sicherheit.domain.security.StammdatenBerechtigungsContext;
import de.wps.bube.basis.stammdaten.domain.service.SuchattributePredicateFactory;
import de.wps.bube.basis.stammdaten.domain.vo.suche.Suchattribute;

@ExtendWith(MockitoExtension.class)
class EURegistrySucheServiceTest {

    @Mock
    StammdatenBerechtigungsContext stammdatenBerechtigungsContext;
    @Mock
    EURegBetriebsstaetteRepository repository;
    @Mock
    EuRegBstBerechtigungsAccessor accessor;
    @Mock
    SuchattributePredicateFactory suchattributePredicatefactory;

    EURegistrySucheService service;
    Suchattribute suchattribute;
    BooleanBuilder suchPredicate, datenberechtigungen;

    @BeforeEach
    void setUp() {
        service = new EURegistrySucheService(stammdatenBerechtigungsContext, repository, accessor, suchattributePredicatefactory);
        datenberechtigungen = new BooleanBuilder(Expressions.asString("datenberechtigungen").eq("datenberechtigungen"));
        when(stammdatenBerechtigungsContext.createPredicateRead(accessor)).thenReturn(datenberechtigungen);
        suchattribute = new Suchattribute();
        suchPredicate = new BooleanBuilder(Expressions.asString("suche").eq("suche"));
        when(suchattributePredicatefactory.createPredicate(same(suchattribute),
                same(eURegBetriebsstaette.betriebsstaette))).thenReturn(suchPredicate);
    }

    @Test
    void shouldApplyDatenberechtigungenAndSuchparameter() {
        var expectedPredicate = new BooleanBuilder(datenberechtigungen).and(
                eURegBetriebsstaette.betriebsstaette.berichtsjahr.id.eq(2020L)).and(suchPredicate);

        service.readAllEuRegBetriebsstaette(2020L, suchattribute, unpaged());

        verify(repository).findAllAsListItems(expectedPredicate, unpaged());
    }

    @Test
    void shouldApplyDatenberechtigungenAndSuchparameterForIds() {
        var expectedPredicate = new BooleanBuilder(datenberechtigungen).and(
                eURegBetriebsstaette.betriebsstaette.berichtsjahr.id.eq(2020L)).and(suchPredicate);

        service.readAllEuRegBetriebsstaetteIds(2020L, suchattribute);

        verify(repository).findAllIds(expectedPredicate);
    }
}
