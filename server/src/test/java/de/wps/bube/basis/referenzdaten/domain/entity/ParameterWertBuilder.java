package de.wps.bube.basis.referenzdaten.domain.entity;

import java.time.Instant;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;

public final class ParameterWertBuilder {
    private Long id;
    private Land land;
    private String pschluessel;
    private String wert;
    private Gueltigkeitsjahr gueltigVon;
    private Gueltigkeitsjahr gueltigBis;
    private Instant letzteAenderung;
    private Behoerde behoerde;

    private ParameterWertBuilder() {
    }

    public static ParameterWertBuilder aParameterWert() {
        return new ParameterWertBuilder().land(Land.DEUTSCHLAND)
                                         .pschluessel("url_pdiscl")
                                         .wert("url")
                                         .gueltigVon(Gueltigkeitsjahr.of(2020))
                                         .gueltigBis(Gueltigkeitsjahr.of(2030))
                                         .letzteAenderung(Instant.now());
    }

    public ParameterWertBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public ParameterWertBuilder land(Land land) {
        this.land = land;
        return this;
    }

    public ParameterWertBuilder pschluessel(String pschluessel) {
        this.pschluessel = pschluessel;
        return this;
    }

    public ParameterWertBuilder wert(String wert) {
        this.wert = wert;
        return this;
    }

    public ParameterWertBuilder gueltigVon(Gueltigkeitsjahr gueltigVon) {
        this.gueltigVon = gueltigVon;
        return this;
    }

    public ParameterWertBuilder gueltigVon(int jahr) {
        return gueltigVon(Gueltigkeitsjahr.of(jahr));
    }

    public ParameterWertBuilder gueltigBis(Gueltigkeitsjahr gueltigBis) {
        this.gueltigBis = gueltigBis;
        return this;
    }

    public ParameterWertBuilder gueltigBis(int jahr) {
        return gueltigBis(Gueltigkeitsjahr.of(jahr));
    }

    public ParameterWertBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public ParameterWertBuilder behoerde(Behoerde behoerde) {
        this.behoerde = behoerde;
        return this;
    }

    public ParameterWert build() {
        return new ParameterWert(id, land, pschluessel, wert, gueltigVon, gueltigBis, behoerde, letzteAenderung);
    }
}
