package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class QuelleTest {

    @Test
    void hasSameKey_parentBetriebsstaette() {
        var q1 = aQuelle().parentBetriebsstaetteId(11L).quelleNr("nummer").bemerkung("hi").build();
        var q2 = aQuelle().parentBetriebsstaetteId(11L).quelleNr("nummer").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isTrue();
    }

    @Test
    void hasSameKey_parentAnlage() {
        var q1 = aQuelle().parentAnlageId(22L).quelleNr("nummer").bemerkung("hi").build();
        var q2 = aQuelle().parentAnlageId(22L).quelleNr("nummer").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isTrue();
    }

    @Test
    void hasSameKey_parentBetriebsstaette_undAnlage_shouldOnlyCheckAnlage() {
        var q1 = aQuelle().parentBetriebsstaetteId(11L).parentAnlageId(22L).quelleNr("nummer").bemerkung("hi").build();
        var q2 = aQuelle().parentBetriebsstaetteId(33L)
                          .parentAnlageId(22L)
                          .quelleNr("nummer")
                          .bemerkung("andere")
                          .build();

        assertThat(q1.hasSameKey(q2)).isTrue();
    }

    @Test
    void hasSameKey_otherBetriebsstaette() {
        var q1 = aQuelle().parentBetriebsstaetteId(11L).quelleNr("nummer").bemerkung("hi").build();
        var q2 = aQuelle().parentBetriebsstaetteId(33L).quelleNr("nummer").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isFalse();
    }

    @Test
    void hasSameKey_betriebsstaette_otherNummer() {
        var q1 = aQuelle().parentBetriebsstaetteId(11L).quelleNr("nummer1").bemerkung("hi").build();
        var q2 = aQuelle().parentBetriebsstaetteId(11L).quelleNr("nummer2").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isFalse();
    }

    @Test
    void hasSameKey_otherAnlage() {
        var q1 = aQuelle().parentAnlageId(22L).quelleNr("nummer").bemerkung("hi").build();
        var q2 = aQuelle().parentAnlageId(33L).quelleNr("nummer").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isFalse();
    }

    @Test
    void hasSameKey_anlage_otherNummer() {
        var q1 = aQuelle().parentAnlageId(22L).quelleNr("nummer1").bemerkung("hi").build();
        var q2 = aQuelle().parentAnlageId(22L).quelleNr("nummer2").bemerkung("andere").build();

        assertThat(q1.hasSameKey(q2)).isFalse();
    }
}
