package de.wps.bube.basis.benutzerverwaltung.domain.entity;

import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BEHOERDENBENUTZER;
import static de.wps.bube.basis.sicherheit.domain.vo.Rolle.BETREIBER;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.sicherheit.domain.vo.Datenberechtigung;
import de.wps.bube.basis.sicherheit.domain.vo.Fachmodul;
import de.wps.bube.basis.sicherheit.domain.vo.Rolle;
import de.wps.bube.basis.sicherheit.domain.vo.Thema;

public final class BenutzerBuilder {

    private String id;
    private String benutzername;
    private String vorname;
    private String nachname;
    private String email;
    private String telefonnummer;

    private boolean aktiv;
    private boolean needsTempPassword;

    private final Datenberechtigung.DatenberechtigungBuilder berechtigung;
    private final Set<Rolle> rollen = EnumSet.noneOf(Rolle.class);

    private BenutzerBuilder(Datenberechtigung.DatenberechtigungBuilder berechtigung, Set<Rolle> rollen) {
        this.berechtigung = berechtigung;
        this.rollen.addAll(rollen);
    }

    public static BenutzerBuilder aBehoerdenbenutzer(String benutzername, Land land) {
        var berechtigung = Datenberechtigung.DatenberechtigungBuilder.land(land);
        return new BenutzerBuilder(berechtigung, Set.of(BEHOERDENBENUTZER)).benutzername(benutzername)
                                                                           .id(UUID.randomUUID().toString());
    }

    public static BenutzerBuilder aBetreiberbenutzer(String benutzername, Land land) {
        var berechtigung = Datenberechtigung.DatenberechtigungBuilder.land(land);
        return new BenutzerBuilder(berechtigung, Set.of(BETREIBER)).benutzername(benutzername)
                                                                   .id(UUID.randomUUID().toString())
                                                                   .betriebsstaetten("Betreiber_BST");
    }

    public BenutzerBuilder id(String id) {
        this.id = id;
        return this;
    }

    public BenutzerBuilder benutzername(String benutzername) {
        this.benutzername = benutzername;
        return this;
    }

    public BenutzerBuilder vorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public BenutzerBuilder nachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public BenutzerBuilder email(String email) {
        this.email = email;
        return this;
    }

    public BenutzerBuilder telefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
        return this;
    }

    public BenutzerBuilder aktiv(boolean aktiv) {
        this.aktiv = aktiv;
        return this;
    }

    public BenutzerBuilder needsTempPassword(boolean needsTempPassword) {
        this.needsTempPassword = needsTempPassword;
        return this;
    }

    public BenutzerBuilder behoerden(String... behoerden) {
        this.berechtigung.behoerden(behoerden);
        return this;
    }

    public BenutzerBuilder akz(String... akz) {
        this.berechtigung.akz(akz);
        return this;
    }

    public BenutzerBuilder verwaltungsgebiete(String... gebiete) {
        this.berechtigung.verwaltungsgebiete(gebiete);
        return this;
    }

    public BenutzerBuilder betriebsstaetten(String... betriebsstaetten) {
        this.berechtigung.betriebsstaetten(betriebsstaetten);
        return this;
    }

    public BenutzerBuilder themen(Thema... themen) {
        this.berechtigung.themen(themen);
        return this;
    }

    public BenutzerBuilder fachmodule(Fachmodul... fachmodule) {
        this.berechtigung.fachmodule(fachmodule);
        return this;
    }

    public BenutzerBuilder addRollen(Set<Rolle> rollen) {
        this.rollen.addAll(rollen);
        return this;
    }

    public BenutzerBuilder addRollen(Rolle... rollen) {
        return addRollen(Set.of(rollen));
    }

    public Benutzer build() {
        var b = new Benutzer(benutzername);
        b.setId(id);
        b.setAktiv(aktiv);
        b.setEmail(email);
        b.setNachname(nachname);
        b.setTelefonnummer(telefonnummer);
        b.setVorname(vorname);
        b.setDatenberechtigung(berechtigung.build());
        b.setZugewieseneRollen(List.copyOf(rollen));
        b.setNeedsTempPassword(needsTempPassword);
        return b;
    }
}
