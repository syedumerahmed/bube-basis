package de.wps.bube.basis.euregistry.xml.bericht;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.wps.bube.basis.euregistry.domain.entity.EURegAnlageBuilder;
import de.wps.bube.basis.euregistry.domain.vo.InspireParam;
import de.wps.bube.basis.euregistry.xml.bericht.dto.SchemaSet;
import de.wps.bube.basis.euregistry.xml.bericht.dto.eureg.ProductionSite;
import de.wps.bube.basis.koordinaten.domain.service.KoordinatenTransformationService;
import de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.ZustaendigeBehoerde;

@SpringBootTest(classes = { ProductionInstallationMapperImpl.class, ProductionInstallationPartMapperImpl.class,
        CommonEURegMapperImpl.class })
class ProductionInstallationMapperTest {

    @MockBean
    private KoordinatenTransformationService koordinatenTransformationService;

    @Autowired
    private ProductionInstallationMapper mapper;

    @BeforeEach
    void setUp() {
        when(koordinatenTransformationService.transformToEPSG4258(any())).thenReturn(new double[] {0.0, 0.0});
    }

    @Test
    void toXDto() {
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().build();

        ZustaendigeBehoerde genemigungsbehoerde = new ZustaendigeBehoerde(BehoerdeBuilder.aFullBehoerde("B1").build(),
                ReferenzBuilder.aZustaendigkeitsReferenz(ZustaendigeBehoerde.GENEHMIGUNG_SCHLUESSEL).build());
        euRegAnlage.getAnlage().getZustaendigeBehoerden().add(genemigungsbehoerde);

        ZustaendigeBehoerde ueberwachungssbehoerde = new ZustaendigeBehoerde(BehoerdeBuilder.aBehoerde("B2").build(),
                ReferenzBuilder.aZustaendigkeitsReferenz(ZustaendigeBehoerde.UEBERWACHUNG_SCHLUESSEL).build());
        euRegAnlage.getAnlage().getZustaendigeBehoerden().add(ueberwachungssbehoerde);

        var inspireParam = new InspireParam("insp_ns", "insp_site", "insp_extension");
        var productionSite = new ProductionSite();
        productionSite.setGmlId("siteGmlId");
        var context = mock(ProductionInstallationMapperContext.class);
        when(context.getProductionSite()).thenReturn(productionSite);
        when(context.getInspireParam()).thenReturn(inspireParam);
        var xdto = mapper.toXDto(euRegAnlage, context);

        assertThat(xdto.getProductionInstallationPart()).isNull();

        assertThat(xdto.getInspireID().getIdentifier().getLocalId()).isEqualTo(
                euRegAnlage.getLocalId() + inspireParam.getExtension());
        assertThat(xdto.getInspireID().getIdentifier().getNamespace()).isEqualTo(inspireParam.getNamespace());

        assertThat(xdto.getThematicID().getIdentifier()).isEqualTo(euRegAnlage.getBerichtsdaten().getThematicId());
        assertThat(xdto.getThematicID().getIdentifierScheme()).isEqualTo(
                euRegAnlage.getBerichtsdaten().getSchema());

        assertThat(xdto.getGeometry().getPos()).isEqualTo("0.0 0.0");
        assertThat(xdto.getGeometry().getSrsDimension()).isEqualTo(2);

        assertThat(xdto.getInstallationName().getNameOfFeature()).isEqualTo(euRegAnlage.getName());

        assertThat(xdto.getBaselineReportIndicator().getCode()).isEqualTo("NOTREQUIRED");
        assertThat(xdto.getBaselineReportIndicator().getSchema()).isEqualTo(SchemaSet.BASELINE_REPORT_INDICATOR_CODES);

        assertThat(xdto.getBatDerogation().size()).isEqualTo(1);
        assertThat(xdto.getBatDerogation().get(0).getDerogationDurationEndDate()).isNull();
        assertThat(xdto.getBatDerogation().get(0).getDerogationDurationStartDate()).isEqualTo("2021-08-12");
        assertThat(xdto.getBatDerogation().get(0).getBatael().getCode()).isEqualTo("bvtAusnahmeEU");
        assertThat(xdto.getBatDerogation().get(0).getBatael().getSchema()).isEqualTo(SchemaSet.BATAEL_CODES);
        assertThat(xdto.getBatDerogation().get(0).getPublicReasonURL()).isEqualTo("https://public.url");
        assertThat(xdto.getBatDerogation().get(0).getBatDerogationIndicator()).isTrue();

        assertThat(xdto.getIEDAnnexIActivity().getMainActivity()).isEqualTo("EUhaupt");
        assertThat(xdto.getIEDAnnexIActivity().getOtherActivity()).isEqualTo(List.of("EUneben"));

        assertThat(xdto.getPermitDetails().size()).isEqualTo(1);
        assertThat(xdto.getPermitDetails().get(0).getPermitGranted()).isEqualTo(
                euRegAnlage.getGenehmigung().isErteilt());
        assertThat(xdto.getPermitDetails().get(0).getPermitReconsidered()).isEqualTo(
                euRegAnlage.getGenehmigung().isNeuBetrachtet());
        assertThat(xdto.getPermitDetails().get(0).getPermitUpdated()).isEqualTo(
                euRegAnlage.getGenehmigung().isGeaendert());
        assertThat(xdto.getPermitDetails().get(0).getDateOfGranting()).isEqualTo("2019-08-10");
        assertThat(xdto.getPermitDetails().get(0).getDateOfLastUpdate()).isEqualTo(null);
        assertThat(xdto.getPermitDetails().get(0).getPermitURL()).isEqualTo("https://genehmigung.url");
        assertThat(xdto.getPermitDetails().get(0).getEnforcementAction()).isEqualTo("durchsetzungsmassnahmen");

        assertThat(xdto.getOtherRelevantChapters()).isEqualTo(List.of("Kapitel IE-RLEU"));

        assertThat(xdto.getSiteVisits().getSiteVisitNumber()).isEqualTo(euRegAnlage.getVisits());
        assertThat(xdto.getSiteVisits().getSiteVisitURL()).isEqualTo(euRegAnlage.getVisitUrl());

        assertThat(xdto.getDateOfStartOfOperation()).isEqualTo("2017-01-01");

        assertThat(xdto.getStatus().getStatusType().getCode()).isEqualTo("EUbetrieb");
        assertThat(xdto.getStatus().getStatusType().getSchema()).isEqualTo(SchemaSet.CONDITION_OF_FACILITY_VALUE_CODES);
        assertThat(xdto.getStatus().getValidFrom().toString()).isEqualTo("2019-10-12");
        assertThat(xdto.getStatus().getDescription()).isNull();
        assertThat(xdto.getStatus().getValidTo()).isNull();

        assertThat(xdto.getRemarks()).isEqualTo(euRegAnlage.getBerichtsdaten().getBemerkung());

        assertThat(xdto.getCompetentAuthorityPermits().size()).isEqualTo(1);
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getOrganisationName()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getBezeichnung());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getIndividualName()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getBezeichnung2());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getElectronicMailAddress()).isEqualTo("B1@gmail.de");
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getTelephoneNo()).isEqualTo("040 76234151");
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getFaxNo()).isEqualTo(null);
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getAddress().getStreetName()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getAdresse().getStrasse());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getAddress().getBuildingNumber()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getAdresse().getHausNr());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getAddress().getCity()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getAdresse().getOrt());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getAddress().getPostalCode()).isEqualTo(
                genemigungsbehoerde.getBehoerde().getAdresse().getPlz());
        assertThat(xdto.getCompetentAuthorityPermits().get(0).getAddress().getConfidentialityReason()).isEqualTo(null);

        assertThat(xdto.getCompetentAuthorityInspections().size()).isEqualTo(1);

        assertThat(xdto.geteSPIRSIdentifier()).isEqualTo(List.of(euRegAnlage.geteSpirsNr()));
        assertThat(xdto.getETSIdentifier()).isEqualTo(List.of(euRegAnlage.getTehgNr()));

        assertThat(xdto.getStricterPermitConditions().size()).isEqualTo(1);
        assertThat(xdto.getStricterPermitConditions().get(0).getArticle18()).isEqualTo(false);
        assertThat(xdto.getStricterPermitConditions().get(0).getArticle14_4()).isEqualTo(true);
        assertThat(xdto.getStricterPermitConditions().get(0).getBatael()).isEqualTo("RBATEEU");
        assertThat(xdto.getStricterPermitConditions().get(0).getStricterPermitConditionsIndicator()).isEqualTo(true);

        assertThat(xdto.getPublicEmissionMonitoring()).isEqualTo(euRegAnlage.getMonitor());
        assertThat(xdto.getPublicEmissionMonitoringURL()).isEqualTo(euRegAnlage.getMonitorUrl());

        assertThat(xdto.getBATConclusion()).isEqualTo(List.of("anwendbareBvtEU"));

        assertThat(xdto.getInstallationType().getCode()).isEqualTo("IED");
        assertThat(xdto.getInstallationType().getSchema()).isEqualTo(SchemaSet.INSTALLATION_TYPE_CODE);
    }

    @Test
    void toXDto_no_insp_extension() {
        var euRegAnlage = EURegAnlageBuilder.aFullEURegAnl().build();

        var inspireParam = new InspireParam("insp_ns", "insp_site", null);
        var productionSite = new ProductionSite();
        productionSite.setGmlId("siteGmlId");
        var context = mock(ProductionInstallationMapperContext.class);
        when(context.getProductionSite()).thenReturn(productionSite);
        when(context.getInspireParam()).thenReturn(inspireParam);
        var xdto = mapper.toXDto(euRegAnlage, context);

        assertThat(xdto.getInspireID().getIdentifier().getLocalId()).isEqualTo(
                euRegAnlage.getLocalId2());
        assertThat(xdto.getInspireID().getIdentifier().getNamespace()).isEqualTo(inspireParam.getNamespace());

    }
}
