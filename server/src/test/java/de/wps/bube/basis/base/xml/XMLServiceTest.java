package de.wps.bube.basis.base.xml;

import static de.wps.bube.basis.test.BubeIntegrationTest.Helpers.getResource;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.wps.bube.basis.stammdaten.xml.dto.BetriebsstaettenXDto;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.base.vo.Validierungshinweis;

class XMLServiceTest {

    private static final String INVALID_SMALL_XML = "xml/invalid_stammdaten_small_import_test.xml";

    private final XMLService xmlService = new XMLService();

    @Test
    void testDefaultLocale_isdeDE() {
        assertEquals("de_DE", Locale.getDefault().toString());
    }

    @Test
    void testGermanValidierungshinweis_default_Locale() throws FileNotFoundException {

        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_SMALL_XML), BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);
        assertFalse(hinweise.isEmpty());

        assertEquals(
                "XML-Import Fehler Zeile 2: cvc-elt.1.a: Deklaration des Elements \"xxx\" kann nicht gefunden werden.",
                hinweise.get(0).hinweis);
        assertTrue(hinweise.get(0).isError());
    }

    @Test
    void testGermanValidierungshinweis_US_Locale() throws FileNotFoundException {

        Locale.setDefault(Locale.US);

        List<Validierungshinweis> hinweise = new ArrayList<>();
        xmlService.validate(getResource(INVALID_SMALL_XML), BetriebsstaettenXDto.XSD_SCHEMA_URL, hinweise);
        assertFalse(hinweise.isEmpty());

        assertEquals(
                "XML-Import Fehler Zeile 2: cvc-elt.1.a: Deklaration des Elements \"xxx\" kann nicht gefunden werden.",
                hinweise.get(0).hinweis);
        assertTrue(hinweise.get(0).isError());
    }

}
