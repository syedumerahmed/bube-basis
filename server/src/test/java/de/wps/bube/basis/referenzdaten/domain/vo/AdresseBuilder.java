package de.wps.bube.basis.referenzdaten.domain.vo;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aVertraulichkeitsReferenz;

import de.wps.bube.basis.referenzdaten.domain.entity.Adresse;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;

public final class AdresseBuilder {
    private String strasse;
    private String hausNr;
    private String plz;
    private String ort;
    private String ortsteil;
    private String postfachPlz;
    private String postfach;
    private Referenz landIsoCode;
    private Referenz vertraulichkeitsgrund;
    private String notiz;

    private AdresseBuilder() {
    }

    public static AdresseBuilder anAdresse() {
        return new AdresseBuilder().ort("ort").plz("plz");
    }

    public static AdresseBuilder aFullAdresse() {
        return new AdresseBuilder().hausNr("2")
                                   .landIsoCode(aReferenz(Referenzliste.RSTAAT, "STAAT").build())
                                   .vertraulichkeit(aVertraulichkeitsReferenz("VERTRAULICH").build())
                                   .notiz("notiz")
                                   .ort("ort")
                                   .ortsteil("ortsteil")
                                   .plz("plz")
                                   .postfach("postfach")
                                   .postfachPlz("postfachPLZ")
                                   .strasse("strasse");
    }

    public AdresseBuilder strasse(String strasse) {
        this.strasse = strasse;
        return this;
    }

    public AdresseBuilder hausNr(String hausNr) {
        this.hausNr = hausNr;
        return this;
    }

    public AdresseBuilder plz(String plz) {
        this.plz = plz;
        return this;
    }

    public AdresseBuilder ort(String ort) {
        this.ort = ort;
        return this;
    }

    public AdresseBuilder ortsteil(String ortsteil) {
        this.ortsteil = ortsteil;
        return this;
    }

    public AdresseBuilder postfachPlz(String postfachPlz) {
        this.postfachPlz = postfachPlz;
        return this;
    }

    public AdresseBuilder postfach(String postfach) {
        this.postfach = postfach;
        return this;
    }

    public AdresseBuilder landIsoCode(Referenz landIsoCode) {
        this.landIsoCode = landIsoCode;
        return this;
    }

    public AdresseBuilder vertraulichkeit(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        return this;
    }

    public AdresseBuilder notiz(String notiz) {
        this.notiz = notiz;
        return this;
    }

    public Adresse build() {
        Adresse adresse = new Adresse(strasse, hausNr, ort, plz);
        adresse.setOrtsteil(ortsteil);
        adresse.setPostfachPlz(postfachPlz);
        adresse.setPostfach(postfach);
        adresse.setLandIsoCode(landIsoCode);
        adresse.setVertraulichkeitsgrund(vertraulichkeitsgrund);
        adresse.setNotiz(notiz);
        return adresse;
    }
}
