package de.wps.bube.basis.stammdaten.domain.vo.suche;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.StringFilter.ESCAPE_CHAR;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.ANLAGE_NUMMER;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.querydsl.core.types.ExpressionUtils;

class AnlageOrAnlagenteilFilterTest {

    @Test
    void shouldCreateForEmpty() {
        var filter = ANLAGE_NUMMER.createPredicate(new Suchattribute(), betriebsstaette);

        assertThat(filter).isNull();
    }

    @Test
    void shouldCreateForAnlageOnly() {
        var filter = ANLAGE_NUMMER.createPredicate(new Suchattribute() {{
            anlage.searchAnlagenteile = false;
            anlage.nummer = "ANL123";
        }}, betriebsstaette);

        var expected = betriebsstaette.anlagen.any().anlageNr.likeIgnoreCase("ANL123", ESCAPE_CHAR);
        assertThat(filter).isEqualTo(expected);
    }

    @Test
    void shouldSearchAnlageAndAnlagenteil() {
        var filter = ANLAGE_NUMMER.createPredicate(new Suchattribute() {{
            anlage.nummer = "ANL123";
            anlage.searchAnlagenteile = true;
        }}, betriebsstaette);

        var expected = ExpressionUtils.or(betriebsstaette.anlagen.any().anlageNr.likeIgnoreCase("ANL123", ESCAPE_CHAR),
                betriebsstaette.anlagen.any().anlagenteile.any().anlagenteilNr.likeIgnoreCase("ANL123", ESCAPE_CHAR));
        assertThat(filter).isEqualTo(expected);
    }
}
