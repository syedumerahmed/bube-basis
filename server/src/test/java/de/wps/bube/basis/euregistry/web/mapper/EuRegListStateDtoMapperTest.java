package de.wps.bube.basis.euregistry.web.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.wps.bube.basis.base.web.PageMapperImpl;
import de.wps.bube.basis.base.web.dto.ListStateDto;
import de.wps.bube.basis.base.web.openapi.model.EuRegBetriebsstaettenListStateDto;
import de.wps.bube.basis.base.web.openapi.model.FilterStateDtoEuRegBetriebsstaetteColumn;
import de.wps.bube.basis.base.web.openapi.model.FilterStateDtoEuRegBetriebsstaetteColumnPropertyEnum;
import de.wps.bube.basis.base.web.openapi.model.PageStateDto;
import de.wps.bube.basis.base.web.openapi.model.SortStateDtoEuRegBetriebsstaetteColumn;
import de.wps.bube.basis.base.web.openapi.model.SortStateDtoEuRegBetriebsstaetteColumnByEnum;
import de.wps.bube.basis.euregistry.domain.vo.EuRegBetriebsstaetteColumn;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(classes = { EuRegListStateMapperImpl.class, PageMapperImpl.class })
class EuRegListStateDtoMapperTest {

    @Autowired
    private EuRegListStateMapper mapper;

    @Test
    void fromEuRegBetriebsstaettenListStateDto() {
        EuRegBetriebsstaettenListStateDto state = new EuRegBetriebsstaettenListStateDto();

        FilterStateDtoEuRegBetriebsstaetteColumn column = new FilterStateDtoEuRegBetriebsstaetteColumn();
        column.setValue("test");
        column.setProperty(FilterStateDtoEuRegBetriebsstaetteColumnPropertyEnum.NAME);
        state.setFilters(Collections.singletonList(column));

        PageStateDto page = new PageStateDto();
        page.setCurrent(5);
        page.setSize(10);
        state.setPage(page);

        SortStateDtoEuRegBetriebsstaetteColumn sort = new SortStateDtoEuRegBetriebsstaetteColumn();
        sort.setBy(SortStateDtoEuRegBetriebsstaetteColumnByEnum.NAME);
        sort.setReverse(false);
        state.setSort(sort);

        ListStateDto<EuRegBetriebsstaetteColumn> listState = mapper.fromEuRegBetriebsstaettenListStateDto(state);

        assertThat(listState.page.current).isEqualTo(page.getCurrent());
        assertThat(listState.page.size).isEqualTo(page.getSize());

        assertThat(listState.filters.get(0).value).isEqualTo(column.getValue());
        assertThat(listState.filters.get(0).property.toString()).isEqualTo(column.getProperty().getValue());
    }
}
