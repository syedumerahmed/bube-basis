package de.wps.bube.basis.referenzdaten.persistence;

import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aJahrReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.*;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.test.BubeJpaTest;

@BubeJpaTest
class ReferenzRepositoryTest {

    @Autowired
    private ReferenzRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAllInBatch();
    }

    @Test
    void testFindByReferenzliste() {
        var referenzBuilder = aReferenz(RBETZ, "any");
        var gesamtliste = repository.saveAll(
                List.of(referenzBuilder.genNewReferenznummer().sortier("01").build(),
                        referenzBuilder.genNewReferenznummer().sortier("04").build(),
                        referenzBuilder.genNewReferenznummer().sortier("02").build(),
                        referenzBuilder.genNewReferenznummer().sortier("03").build()));

        var liste = repository.findByReferenzlisteOrderBySortierAsc(RBETZ);

        assertThat(liste).containsExactly(gesamtliste.get(0), gesamtliste.get(2), gesamtliste.get(3),
                gesamtliste.get(1));

    }

    @Test
    void testFindByReferenzlisteAndLand() {
        var referenzBuilder = aReferenz(RBETZ, "any");
        var gesamtliste = repository.saveAll(
                List.of(referenzBuilder.genNewReferenznummer().sortier("01").land(Land.DEUTSCHLAND).build(),
                        referenzBuilder.genNewReferenznummer().sortier("04").land(Land.DEUTSCHLAND).build(),
                        referenzBuilder.genNewReferenznummer().sortier("02").land(Land.HAMBURG).build(),
                        referenzBuilder.genNewReferenznummer().sortier("03").land(Land.SCHLESWIG_HOLSTEIN).build()));

        var liste = repository.findByReferenzlisteAndLandInOrderBySortierAsc(RBETZ,
                List.of(Land.DEUTSCHLAND, Land.HAMBURG));

        assertThat(liste).containsExactly(gesamtliste.get(0), gesamtliste.get(2), gesamtliste.get(1));

        liste = repository.findByReferenzlisteAndLandInOrderBySortierAsc(RBETZ,
                List.of(Land.DEUTSCHLAND, Land.SCHLESWIG_HOLSTEIN));

        assertThat(liste).containsExactly(gesamtliste.get(0), gesamtliste.get(3), gesamtliste.get(1));

        liste = repository.findByReferenzlisteAndLandInOrderBySortierAsc(RBETZ, List.of(Land.MECKLENBURG_VORPOMMERN));

        assertThat(liste).isEmpty();

    }

    @Test
    void testFindByAll_found() {

        var refliste = RIET;
        var land = Land.DEUTSCHLAND;

        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());

        var referenzOptional = repository.findByKey(refliste, land, "AAA", Gueltigkeitsjahr.of(2005));

        assertThat(referenzOptional).isPresent();
    }

    @Test
    void testFindByAll_notFoundLand() {

        var refliste = RIET;
        var land = Land.DEUTSCHLAND;

        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());

        var referenzOptional = repository.findByKey(refliste, Land.HAMBURG, "AAA", Gueltigkeitsjahr.of(2005));

        assertThat(referenzOptional).isEmpty();
    }

    @Test
    void testFindByAll_notFoundSchluessel() {

        var refliste = RIET;
        var land = Land.DEUTSCHLAND;

        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());

        var referenzOptional = repository.findByKey(refliste, land, "BBB", Gueltigkeitsjahr.of(2005));

        assertThat(referenzOptional).isEmpty();
    }

    @Test
    void testFindByAll_BadData() {

        var refliste = RIET;
        var land = Land.DEUTSCHLAND;

        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());
        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());

        assertThrows(IncorrectResultSizeDataAccessException.class,
                () -> repository.findByKey(refliste, land, "AAA", Gueltigkeitsjahr.of(2005)));
    }

    @Test
    void testFindByAll_notFoundJahr() {

        var refliste = RIET;
        var land = Land.DEUTSCHLAND;

        repository.save(aReferenz(refliste, "AAA").land(land).gueltigVon(2005).gueltigBis(2010).build());

        var referenzOptional = repository.findByKey(refliste, land, "AAA", Gueltigkeitsjahr.of(2015));

        assertThat(referenzOptional).isEmpty();
    }

    @Test
    void testFindAll() {
        var refliste = RGMD;
        var referenzBuilder = aReferenz(refliste, "AAA").gueltigVon(2005).gueltigBis(2020);
        var referenz00 = repository.save(referenzBuilder.land(Land.DEUTSCHLAND).genNewReferenznummer().build());
        var referenz02 = repository.save(referenzBuilder.land(Land.HAMBURG).genNewReferenznummer().build());
        var referenzFalscherZeitraum = repository.save(referenzBuilder.gueltigBis(2010).genNewReferenznummer().build());
        var referenzFalscheListe = repository.save(
                referenzBuilder.gueltigBis(2020).referenzliste(RKOM).genNewReferenznummer().build());

        var referenzen = repository.findAll(refliste, Gueltigkeitsjahr.of(2015)).collect(toList());

        assertThat(referenzen).contains(referenz00, referenz02);
        assertThat(referenzen).doesNotContain(referenzFalscheListe, referenzFalscherZeitraum);
    }

    @Test
    void testFindFirstByReferenzlisteOrderBySortierDesc() {
        for (int jahr = 2012; jahr <= 2020; jahr++) {
            repository.save(aJahrReferenz(jahr).build());
        }
        var neustesJahr = repository.save(aJahrReferenz(2021).build());

        var actualJahr = repository.findFirstByReferenzlisteOrderBySortierDesc(RJAHR);

        assertThat(actualJahr).hasValue(neustesJahr);
    }

    @Test
    void testFindFirstByReferenzlisteOrderBySortierDesc_Liste_nichtVorhanden() {
        for (int jahr = 2012; jahr <= 2020; jahr++) {
            repository.save(aJahrReferenz(jahr).build());
        }

        var actualJahr = repository.findFirstByReferenzlisteOrderBySortierDesc(RBETZ);

        assertThat(actualJahr).isEmpty();
    }

    @Test
    void testZeitraum_gueltig_vor_other() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2011);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_gueltig_nach_other() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2010)
                                                         .gueltigBis(2012)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, RKOM.name(), Land.HESSEN.getNr(), "01", 2013, 2020);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_ungueltig_gueltigBis_betweenOther() {
        repository.saveAndFlush(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                                 .schluessel("01")
                                                                 .gueltigVon(2012)
                                                                 .gueltigBis(2020)
                                                                 .build());

        var duplicate = repository.functionalKeyDuplicate(987654L, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2013);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_beides_betweenOther() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, RKOM.name(), Land.HESSEN.getNr(), "01", 2013, 2015);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_gueltigVon_betweenOther() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, RKOM.name(), Land.HESSEN.getNr(), "01", 2015, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_ungueltig_umschliessend() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(100L, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_gueltig_selbeID() {
        var id = repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                                  .schluessel("01")
                                                                  .gueltigVon(2012)
                                                                  .gueltigBis(2020)
                                                                  .build()).getId();

        var duplicate = repository.functionalKeyDuplicate(id, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2015);

        assertThat(duplicate).isFalse();
    }

    @Test
    void testZeitraum_ungueltig_ohneID() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(null, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2077);

        assertThat(duplicate).isTrue();
    }

    @Test
    void testZeitraum_gueltig_ohneID() {
        repository.save(aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                         .schluessel("01")
                                                         .gueltigVon(2012)
                                                         .gueltigBis(2020)
                                                         .build());

        var duplicate = repository.functionalKeyDuplicate(null, RKOM.name(), Land.HESSEN.getNr(), "01", 2010, 2011);

        assertThat(duplicate).isFalse();
    }

    @Test
    void test_create() {

        var ref = aKommunikationsTypReferenz("tel").land(Land.HESSEN)
                                                   .schluessel("01")
                                                   .gueltigVon(2012)
                                                   .gueltigBis(2020)
                                                   .build();
        assertThat(ref.getId()).isNull();
        assertThat(ref.getReferenznummer()).isNotNull();

        var refSaved = repository.saveAndFlush(ref);

        assertThat(refSaved.getId()).isNotNull();
        assertThat(refSaved.getReferenznummer()).isNotNull();
    }

    @Test
    void test_findByReferenznummer() {
        var ref = aKommunikationsTypReferenz("tel").build();

        var refSaved = repository.saveAndFlush(ref);

        assertThat(repository.findByReferenznummer(refSaved.getReferenznummer())).isPresent();
    }

    @Test
    void findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc() {

        var referenzBuilder = aReferenz(RBETZ, "SCHLUESSEL").land(Land.DEUTSCHLAND);
        var gesamtliste = repository.saveAll(
                List.of(referenzBuilder.genNewReferenznummer().sortier("04").gueltigBis(2020).build(),
                        referenzBuilder.genNewReferenznummer().sortier("03").gueltigBis(2019).build(),
                        referenzBuilder.genNewReferenznummer().sortier("02").gueltigBis(2018).build(),
                        referenzBuilder.genNewReferenznummer().sortier("01").gueltigBis(2017).build()));

        var optionalReferenz = repository.findFirstByReferenzlisteAndLandAndSchluesselOrderByGueltigBisDesc(RBETZ,
                Land.DEUTSCHLAND, "SCHLUESSEL");

        assertTrue(optionalReferenz.isPresent());
        assertThat(optionalReferenz.get().getSortier()).isEqualTo("04");
    }
}
