package de.wps.bube.basis.stammdaten.domain.vo.suche;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.SuchattributKonfiguration.ZUSTAENDIGE_BEHOERDE;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

class ReferenzFilterTest {

    @Test
    void shouldMapNull() {
        final var referenzFilter = ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute(), betriebsstaette);

        assertThat(referenzFilter).isNull();
    }

    @Test
    void shouldMapValue() {
        final var referenzFilter = ZUSTAENDIGE_BEHOERDE.createPredicate(new Suchattribute() {{
            betriebsstaette.behoerdeIn = List.of(1002L, 1003L);
        }}, betriebsstaette);

        assertThat(referenzFilter).isEqualTo(betriebsstaette.zustaendigeBehoerde.id.in(1002L, 1003L));
    }
}
