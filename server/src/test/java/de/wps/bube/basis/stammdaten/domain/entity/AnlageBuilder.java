package de.wps.bube.basis.stammdaten.domain.entity;

import static de.wps.bube.basis.referenzdaten.domain.entity.BehoerdeBuilder.aBehoerde;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.*;
import static de.wps.bube.basis.stammdaten.domain.entity.AnlagenteilBuilder.anAnlagenteil;
import static de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder.aQuelle;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.a13BImSchVVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.a17BImSchVVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschrift;
import static de.wps.bube.basis.stammdaten.domain.entity.VorschriftBuilder.aVorschriftMitTaetigkeiten;
import static de.wps.bube.basis.stammdaten.domain.vo.VorschriftSchluessel.V_IE_RL;
import static java.util.Collections.emptyList;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.stammdaten.domain.vo.Leistungsklasse;

public final class AnlageBuilder {
    private Long id;
    private String referenzAnlagenkataster;
    private String anlageNr;
    private String localId;
    // Eine Anlage hat eine Eltern Betriebsstätte
    private Long parentBetriebsstaetteId;
    // hat GeoPunkt 0..n
    private GeoPunkt geoPunkt;
    // hat Vorschriften 0..n
    private List<Vorschrift> vorschriften = new ArrayList<>();
    // hat Leistungen 0..n
    private List<Leistung> leistungen = new ArrayList<>();
    private List<ZustaendigeBehoerde> zustaendigeBehoerden = new ArrayList<>();
    private String name;
    private Referenz vertraulichkeitsgrund;
    private Referenz betriebsstatus;
    private LocalDate betriebsstatusSeit;
    private LocalDate inbetriebnahme;
    private String bemerkung;
    private Instant ersteErfassung;
    private Instant letzteAenderung;
    private List<Anlagenteil> anlagenteile = new ArrayList<>();
    private List<Quelle> quellen = new ArrayList<>();
    private boolean uebernommen;
    private Instant letzteAenderungBetreiber;

    private AnlageBuilder() {
    }

    public static AnlageBuilder anAnlage() {
        return anAnlage(null);
    }

    public static AnlageBuilder anAnlage(Long id) {
        return new AnlageBuilder().id(id)
                                  .anlageNr(String.valueOf(id))
                                  .name("Name")
                                  .letzteAenderung(Instant.parse("2020-02-20T14:00:00Z"))
                                  .ersteErfassung(Instant.parse("2019-02-20T14:00:00Z"));
    }

    public static AnlageBuilder aFullAnlage() {
        return anAnlage(2L)
                         .referenzAnlagenkataster("kataster")
                         .anlagenteile(anAnlagenteil(1L).build(), anAnlagenteil(2L).build())
                         .quellen(aQuelle(1L).build(), aQuelle(2L).build())
                         .anlageNr("nummer")
                         .parentBetriebsstaetteId(23L)
                         .geoPunkt(new GeoPunkt(1, 2, anEpsgReferenz("epsg").build()))
                         .vorschriften(aVorschriftMitTaetigkeiten().build(), aVorschriftMitTaetigkeiten().build())
                         .leistungen(List.of(new Leistung(2.5, aLeistungseinheitReferenz("kg/schuss").build(), "bezug",
                                         Leistungsklasse.GENEHMIGT),
                                 new Leistung(2.8, aLeistungseinheitReferenz("m/s").build(), "bezug2",
                                         Leistungsklasse.BETRIEBEN)))
                         .name("Anlage")
                         .vertraulichkeitsgrund(aVertraulichkeitsReferenz("vertraulich").build())
                         .betriebsstatus(aBetriebsstatusReferenz("betrieb").build())
                         .betriebsstatusSeit(LocalDate.of(2019, 10, 12))
                         .inbetriebnahme(LocalDate.of(2017, 1, 1))
                         .bemerkung("bemerkung")
                         .localId("localId")
                         .zustaendigeBehoerden(new ZustaendigeBehoerde(aBehoerde("behoerde2").build(),
                                         aZustaendigkeitsReferenz("zustaendig").build()),
                                 new ZustaendigeBehoerde(aBehoerde("behoerde3").build(),
                                         aZustaendigkeitsReferenz("zustaendig2").build()));
    }

    public AnlageBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public AnlageBuilder referenzAnlagenkataster(String referenzAnlagenkataster) {
        this.referenzAnlagenkataster = referenzAnlagenkataster;
        return this;
    }

    public AnlageBuilder anlageNr(String anlageNr) {
        this.anlageNr = anlageNr;
        return this;
    }

    public AnlageBuilder localId(String localId) {
        this.localId = localId;
        return this;
    }

    public AnlageBuilder parentBetriebsstaetteId(Long parentBetriebsstaetteId) {
        this.parentBetriebsstaetteId = parentBetriebsstaetteId;
        return this;
    }

    public AnlageBuilder geoPunkt(GeoPunkt geoPunkt) {
        this.geoPunkt = geoPunkt;
        return this;
    }

    public AnlageBuilder vorschriften(Vorschrift... vorschriften) {
        return vorschriften(List.of(vorschriften));
    }

    public AnlageBuilder vorschriften(List<Vorschrift> vorschriften) {
        this.vorschriften = new ArrayList<Vorschrift>(vorschriften);
        return this;
    }

    public AnlageBuilder addVorschrift(Vorschrift vorschrift) {
        this.vorschriften.add(vorschrift);
        return this;
    }

    public AnlageBuilder leistungen(List<Leistung> leistungen) {
        this.leistungen = leistungen;
        return this;
    }

    public AnlageBuilder name(String name) {
        this.name = name;
        return this;
    }

    public AnlageBuilder vertraulichkeitsgrund(Referenz vertraulichkeitsgrund) {
        this.vertraulichkeitsgrund = vertraulichkeitsgrund;
        return this;
    }

    public AnlageBuilder betriebsstatus(Referenz betriebsstatus) {
        this.betriebsstatus = betriebsstatus;
        return this;
    }

    public AnlageBuilder betriebsstatusSeit(LocalDate betriebsstatusSeit) {
        this.betriebsstatusSeit = betriebsstatusSeit;
        return this;
    }

    public AnlageBuilder inbetriebnahme(LocalDate inbetriebnahme) {
        this.inbetriebnahme = inbetriebnahme;
        return this;
    }

    public AnlageBuilder bemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
        return this;
    }

    public AnlageBuilder ersteErfassung(Instant ersteErfassung) {
        this.ersteErfassung = ersteErfassung;
        return this;
    }

    public AnlageBuilder letzteAenderung(Instant letzteAenderung) {
        this.letzteAenderung = letzteAenderung;
        return this;
    }

    public AnlageBuilder anlagenteile(List<Anlagenteil> anlagenteile) {
        this.anlagenteile = anlagenteile;
        return this;
    }

    public AnlageBuilder anlagenteile(Anlagenteil... anlagenteile) {
        return this.anlagenteile(List.of(anlagenteile));
    }

    public AnlageBuilder addAnlagenteil(Anlagenteil anlagenteil) {
        this.anlagenteile.add(anlagenteil);
        return this;
    }

    public AnlageBuilder addAnlagenteil(AnlagenteilBuilder anlagenteilBuilder) {
        return addAnlagenteil(anlagenteilBuilder.build());
    }

    public AnlageBuilder quellen(List<Quelle> quellen) {
        this.quellen = quellen;
        return this;
    }

    public AnlageBuilder quellen(Quelle... quellen) {
        return this.quellen(List.of(quellen));
    }

    public AnlageBuilder zustaendigeBehoerden(ZustaendigeBehoerde... zustaendigeBehoerden) {
        return zustaendigeBehoerden(List.of(zustaendigeBehoerden));
    }

    public AnlageBuilder zustaendigeBehoerden(List<ZustaendigeBehoerde> zustaendigeBehoerden) {
        this.zustaendigeBehoerden = new ArrayList<>(zustaendigeBehoerden);
        return this;
    }

    public AnlageBuilder uebernommen(boolean uebernommen) {
        this.uebernommen = uebernommen;
        return this;
    }

    public AnlageBuilder letzteAenderungBetreiber(Instant letzteAenderungBetreiber) {
        this.letzteAenderungBetreiber = letzteAenderungBetreiber;
        return this;
    }

    public AnlageBuilder addZustaendigeBehoerde(ZustaendigeBehoerde zustaendigeBehoerde) {
        this.zustaendigeBehoerden.add(zustaendigeBehoerde);
        return this;
    }

    /**
     * Macht eine Anlage für Berichtsart EuRegAnl berichtspflichtig oder nicht
     *
     * @param pflichtig Wenn {@code true} werden die Vorschriften mit einer berichtspflichten Vorschrift überschrieben
     *                  Wenn {@code false} werden alle Vorschriften und alle Anlagenteile gelöscht
     *
     * @return
     */
    public AnlageBuilder euRegAnl(boolean pflichtig) {
        if (pflichtig) {
            vorschriften(aVorschrift().art(aVorschriftReferenz(V_IE_RL.getSchluessel()).build())
                                      .build());
        } else {
            vorschriften(emptyList());
            anlagenteile(emptyList());
        }
        return this;
    }

    public AnlageBuilder add13BimschvVorschrift() {
        return addVorschrift(a13BImSchVVorschrift().build());
    }

    public AnlageBuilder add17BimschvVorschrift() {
        return addVorschrift(a17BImSchVVorschrift().build());
    }

    public Anlage build() {
        Anlage anlage = new Anlage(anlageNr, parentBetriebsstaetteId, name);
        anlage.setId(id);
        anlage.setErsteErfassung(ersteErfassung);
        anlage.setLetzteAenderung(letzteAenderung);
        anlage.setReferenzAnlagenkataster(referenzAnlagenkataster);
        anlage.setGeoPunkt(geoPunkt);
        anlage.setVorschriften(vorschriften);
        anlage.setLeistungen(leistungen);
        anlage.setLocalId(localId);
        anlage.setVertraulichkeitsgrund(vertraulichkeitsgrund);
        anlage.setBetriebsstatus(betriebsstatus);
        anlage.setBetriebsstatusSeit(betriebsstatusSeit);
        anlage.setInbetriebnahme(inbetriebnahme);
        anlage.setBemerkung(bemerkung);
        anlagenteile.forEach(an -> an.setParentAnlageId(id));
        anlage.setAnlagenteile(anlagenteile);
        anlage.setQuellen(quellen);
        anlage.setZustaendigeBehoerden(zustaendigeBehoerden);
        anlage.setBetreiberdatenUebernommen(uebernommen);
        anlage.setLetzteAenderungBetreiber(letzteAenderungBetreiber);
        return anlage;
    }
}
