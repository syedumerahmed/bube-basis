package de.wps.bube.basis.koordinaten.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.wps.bube.basis.base.persistence.QuerydslPredicateProjectionRepositoryFactoryBean;
import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.jobs.domain.BenutzerJobRegistry;
import de.wps.bube.basis.jobs.domain.JobProtokoll;
import de.wps.bube.basis.koordinaten.domain.KoordinatenException;
import de.wps.bube.basis.koordinaten.persistence.GemeindekoordinatenRepository;
import de.wps.bube.basis.referenzdaten.domain.entity.GeoPunkt;
import de.wps.bube.basis.referenzdaten.domain.entity.Parameter;
import de.wps.bube.basis.referenzdaten.domain.entity.ParameterWert;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.service.ParameterService;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.test.BubeIntegrationTest;

@SpringBootTest(classes = { KoordinatenPruefungService.class, VG250ImportJob.class, ReferenzRepository.class })
@EnableJpaRepositories(value = { "de.wps.bube.basis.koordinaten.persistence",
        "de.wps.bube.basis.referenzdaten.persistence" },
        repositoryFactoryBeanClass = QuerydslPredicateProjectionRepositoryFactoryBean.class)
@EntityScan({ "de.wps.bube.basis.koordinaten.domain", "de.wps.bube.basis.referenzdaten.domain" })
class KoordinatenPruefungServiceIntegrationTest extends BubeIntegrationTest {

    public static final Referenz EPSG_CODE = ReferenzBuilder.anEpsgReferenz("25832").build();

    public static final GeoPunkt SYLT_3 = new GeoPunkt(6087443L, 457325L, EPSG_CODE);
    public static final String AGS_SYLT_3 = "01054149";
    public static final GeoPunkt SYLT_4 = new GeoPunkt(6083756L, 456833L, EPSG_CODE);
    public static final GeoPunkt INVALID = new GeoPunkt(6083756L, 456833L,
            ReferenzBuilder.anEpsgReferenz("9999999").build());
    public static final String AGS_SYLT_4 = "01054168";
    public static final ParameterWert GEO_BUFF = new ParameterWert(1L, Land.DEUTSCHLAND, "geo_buff", "100",
            Gueltigkeitsjahr.of(2007), Gueltigkeitsjahr.of(2099), null, Instant.now());
    public Referenz R_2020 = ReferenzBuilder.aJahrReferenz(2020).build();

    private static final Gueltigkeitsjahr J_2020 = Gueltigkeitsjahr.of(2020);

    @Autowired
    private KoordinatenPruefungService koordinatenPruefungService;
    @Autowired
    private VG250ImportJob vG250Import;
    @Autowired
    private GemeindekoordinatenRepository gemeindekoordinatenRepository;
    @Autowired
    private ReferenzRepository referenzRepository;
    @MockBean
    private ParameterService parameterService;
    @MockBean
    private BenutzerJobRegistry jobRegistry;

    @BeforeEach
    public void setUp() throws IOException {
        R_2020 = referenzRepository.save(R_2020);
        File file = new File("src/test/resources/koordinaten/test.shp");
        vG250Import.importShapeFile(file.toURI().toURL(), new JobProtokoll(), R_2020);
        gemeindekoordinatenRepository.flush();

        when(parameterService.findParameterWert(Parameter.GEO_BUFFER, J_2020, Land.DEUTSCHLAND)).thenReturn(Optional.of(
                GEO_BUFF));
    }

    @Test
    public void isGeoPunktInAgsGeometrieTrue() {
        boolean geoPunktInAgsGeometrie = koordinatenPruefungService.isGeoPunktInAgsGeometrie(AGS_SYLT_4, SYLT_4, J_2020,
                Land.DEUTSCHLAND, R_2020);

        assertTrue(geoPunktInAgsGeometrie);
    }

    @Test
    public void isGeoPunktInAgsGeometrieTrueForUnknownYear() {
        Referenz R_2019 = ReferenzBuilder.aJahrReferenz(2019).build();
        boolean geoPunktInAgsGeometrie = koordinatenPruefungService.isGeoPunktInAgsGeometrie(AGS_SYLT_4, SYLT_4, J_2020,
                Land.DEUTSCHLAND, R_2019);

        assertTrue(geoPunktInAgsGeometrie);
    }

    @Test
    public void isGeoPunktInAgsGeometrieFalse() {
        boolean geoPunktInAgsGeometrie = koordinatenPruefungService.isGeoPunktInAgsGeometrie(AGS_SYLT_4, SYLT_3, J_2020,
                Land.DEUTSCHLAND, R_2020);

        assertFalse(geoPunktInAgsGeometrie);
    }

    @Test
    public void isGeoPunktInAgsGeometrie_AGSUnbekannt() {
        assertThrows(KoordinatenException.class,
                () -> koordinatenPruefungService.isGeoPunktInAgsGeometrie("123", SYLT_3, J_2020, Land.DEUTSCHLAND,
                        R_2020));
    }

    @Test
    public void isGeoPunktInAgsGeometrie_KeinParameter() {
        when(parameterService.findParameterWert(Parameter.GEO_BUFFER, J_2020, Land.NIEDERSACHSEN)).thenReturn(
                Optional.empty());
        assertThrows(KoordinatenException.class,
                () -> koordinatenPruefungService.isGeoPunktInAgsGeometrie(AGS_SYLT_4, SYLT_3, J_2020,
                        Land.NIEDERSACHSEN, R_2020));
    }

    @Test
    // Wenn der EPSG-Code nicht valide ist, wird eine Exception geworfen
    public void isGeoPunktInAgsGeometrie_InvalidEpsg() {
        var koordinatenException = assertThrows(KoordinatenException.class,
                () -> koordinatenPruefungService.isGeoPunktInAgsGeometrie(AGS_SYLT_4, INVALID, J_2020, Land.DEUTSCHLAND,
                        R_2020));
        assertThat(koordinatenException).hasMessageContaining(INVALID.getEpsgCode().getSchluessel());
    }
}
