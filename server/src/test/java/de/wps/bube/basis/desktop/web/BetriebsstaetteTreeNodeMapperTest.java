package de.wps.bube.basis.desktop.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import de.wps.bube.basis.base.vo.Land;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeDto;
import de.wps.bube.basis.base.web.openapi.model.TreeNodeTypEnum;
import de.wps.bube.basis.stammdaten.domain.entity.Anlage;
import de.wps.bube.basis.stammdaten.domain.entity.AnlageBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Betriebsstaette;
import de.wps.bube.basis.stammdaten.domain.entity.BetriebsstaetteBuilder;
import de.wps.bube.basis.stammdaten.domain.entity.Quelle;
import de.wps.bube.basis.stammdaten.domain.entity.QuelleBuilder;

class BetriebsstaetteTreeNodeMapperTest {

    @Test
    void mapBetriebsstaette() {

        // Setup
        Anlage a = AnlageBuilder.anAnlage().name("anlage").anlageNr("200").build();
        Quelle q = QuelleBuilder.aQuelle().name("quelle").quelleNr("300").build();
        Betriebsstaette b = BetriebsstaetteBuilder.aBetriebsstaette()
                                                  .name("name")
                                                  .betriebsstaetteNr("100")
                                                  .land(Land.HAMBURG)
                                                  .quellen(q)
                                                  .anlagen(a)
                                                  .build();


        // Act
        TreeNodeDto betriebsstaetteDto = BetriebsstaetteTreeNodeMapper.mapBetriebsstaette(b);

        // Assert
        assertEquals("name", betriebsstaetteDto.getName());
        assertEquals(TreeNodeTypEnum.BETRIEBSSTAETTE, betriebsstaetteDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100", betriebsstaetteDto.getLink());
        assertEquals("100", betriebsstaetteDto.getNummer());
        assertEquals(2, betriebsstaetteDto.getChildren().size());

        // Act
        TreeNodeDto anlageDto = betriebsstaetteDto.getChildren().get(0);

        // Assert
        assertEquals("anlage", anlageDto.getName());
        assertEquals(TreeNodeTypEnum.ANLAGE, anlageDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/anlage/nummer/200",
                anlageDto.getLink());
        assertEquals("200", anlageDto.getNummer());
        assertNull(anlageDto.getChildren());

        // Act
        TreeNodeDto quelleDto = betriebsstaetteDto.getChildren().get(1);

        // Assert
        assertEquals("quelle", quelleDto.getName());
        assertEquals(TreeNodeTypEnum.QUELLE, quelleDto.getTyp());
        assertEquals("betriebsstaette/land/02/nummer/100/quelle/nummer/300",
                quelleDto.getLink());
        assertEquals("300", quelleDto.getNummer());
        assertNull(quelleDto.getChildren());

    }
}
