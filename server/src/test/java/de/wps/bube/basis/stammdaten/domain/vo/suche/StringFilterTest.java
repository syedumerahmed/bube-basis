package de.wps.bube.basis.stammdaten.domain.vo.suche;

import static de.wps.bube.basis.stammdaten.domain.entity.QBetriebsstaette.betriebsstaette;
import static de.wps.bube.basis.stammdaten.domain.vo.suche.StringFilter.ESCAPE_CHAR;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class StringFilterTest {

    @Test
    void shouldReplaceWildcards() {
        final var suchparameter = new Suchattribute() {{
            betriebsstaette.name = "*search*";
        }};

        final var filter = SuchattributKonfiguration.NAME.createPredicate(suchparameter, betriebsstaette);

        assertThat(filter).isEqualTo(betriebsstaette.name.likeIgnoreCase("%search%", ESCAPE_CHAR));
    }

    @Test
    void shouldNotReplaceWildcardInBetween() {
        final var suchparameter = new Suchattribute() {{
            betriebsstaette.name = "before*after";
        }};

        final var filter = SuchattributKonfiguration.NAME.createPredicate(suchparameter, betriebsstaette);

        assertThat(filter).isEqualTo(betriebsstaette.name.likeIgnoreCase("before*after", ESCAPE_CHAR));
    }

    @Test
    void shouldEscapeLikeWildcard() {
        final var suchparameter = new Suchattribute() {{
            betriebsstaette.name = "underscore_percent%";
        }};

        final var filter = SuchattributKonfiguration.NAME.createPredicate(suchparameter, betriebsstaette);

        assertThat(filter).isEqualTo(betriebsstaette.name.likeIgnoreCase(
                                                                 "underscore" + ESCAPE_CHAR + "_percent" + ESCAPE_CHAR + "%",
                                                                 ESCAPE_CHAR));
    }
}
