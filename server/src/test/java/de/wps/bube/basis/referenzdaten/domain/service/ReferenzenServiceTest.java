package de.wps.bube.basis.referenzdaten.domain.service;

import static de.wps.bube.basis.base.vo.Land.DEUTSCHLAND;
import static de.wps.bube.basis.base.vo.Land.HAMBURG;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aFlusseinzugsgebiet;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aGemeindeReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aKommunikationsTypReferenz;
import static de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder.aReferenz;
import static de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.wps.bube.basis.base.persistence.BubeEntityNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenException;
import de.wps.bube.basis.referenzdaten.domain.ReferenzdatenNotFoundException;
import de.wps.bube.basis.referenzdaten.domain.entity.Referenz;
import de.wps.bube.basis.referenzdaten.domain.entity.ReferenzBuilder;
import de.wps.bube.basis.referenzdaten.domain.vo.Gueltigkeitsjahr;
import de.wps.bube.basis.referenzdaten.domain.vo.Referenzliste;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzRepository;
import de.wps.bube.basis.referenzdaten.persistence.ReferenzlisteMetadataRepository;
import de.wps.bube.basis.sicherheit.domain.entity.AktiverBenutzer;
import de.wps.bube.basis.sicherheit.domain.security.ReferenzenBerechtigungsContext;

@ExtendWith(MockitoExtension.class)
class ReferenzenServiceTest {

    @Mock
    private ReferenzRepository repository;

    @Mock
    private ReferenzlisteMetadataRepository referenzlisteRepository;

    @Mock
    private AktiverBenutzer bearbeiter;

    @Mock
    private ReferenzenBerechtigungsContext referenzenBerechtigungsContext;

    private ReferenzenService service;

    @BeforeEach
    void setUp() {
        service = new ReferenzenService(repository, referenzlisteRepository, bearbeiter, referenzenBerechtigungsContext);
    }

    @Test
    void readReferenzliste_Bund_nichtGefunden() {
        when(repository.findForLand(RKOM, DEUTSCHLAND, Gueltigkeitsjahr.of(2020))).thenReturn(Stream.empty());

        var result = service.readReferenzliste(RKOM, Gueltigkeitsjahr.of(2020), HAMBURG);

        assertThat(result).isEmpty();
    }

    @Test
    void readReferenzliste_Bund_Gefunden() {
        Referenz r = aKommunikationsTypReferenz("SCHLUESSEL").land(DEUTSCHLAND).build();
        when(repository.findForLand(RKOM, DEUTSCHLAND, Gueltigkeitsjahr.of(2020))).thenReturn(Stream.of(r));

        var result = service.readReferenzliste(RKOM, Gueltigkeitsjahr.of(2020), HAMBURG);

        assertThat(result).containsExactly(r);
    }

    @Test
    void readReferenzliste_Land_asGesamtadmin() {
        Referenz r = aGemeindeReferenz("SCHLUESSEL").land(HAMBURG).build();
        when(bearbeiter.isGesamtadmin()).thenReturn(true);
        when(repository.findAll(RGMD, Gueltigkeitsjahr.of(2020))).thenReturn(Stream.of(r));

        var result = service.readReferenzliste(RGMD, Gueltigkeitsjahr.of(2020), DEUTSCHLAND);

        assertThat(result).containsExactly(r);
    }

    @Test
    void readReferenzliste_LandAndBundExist() {
        var refliste = RGMD;
        var jahr = Gueltigkeitsjahr.of(2008);
        var refBuilder = aReferenz(refliste, "").gueltigVon(jahr).gueltigBis(jahr);

        // Müssen unterschiedliche IDs haben, damit containsExactly funktioniert (prüft equals())
        var hamburgSchluessel01 = refBuilder.id(1L).schluessel("01").land(HAMBURG).sortier("03").build();
        var deutschlandSchluessel01 = refBuilder.id(2L).schluessel("01").land(DEUTSCHLAND).sortier("03").build();
        var hamburgSchluessel02 = refBuilder.id(3L).schluessel("02").land(HAMBURG).sortier("01").build();
        var deutschlandSchluessel03 = refBuilder.id(4L).schluessel("03").land(DEUTSCHLAND).sortier("02").build();

        when(repository.findForLand(refliste, DEUTSCHLAND, jahr)).thenReturn(
                Stream.of(deutschlandSchluessel01, deutschlandSchluessel03));
        when(repository.findForLand(refliste, HAMBURG, jahr)).thenReturn(
                Stream.of(hamburgSchluessel02, hamburgSchluessel01));
        when(referenzenBerechtigungsContext.checkLandZugriff(HAMBURG)).thenReturn(true);

        var result = service.readReferenzliste(refliste, jahr, HAMBURG);

        assertThat(result).containsExactly(hamburgSchluessel02, deutschlandSchluessel03, hamburgSchluessel01);
    }

    @Test
    void readReferenz_Bund_nichtGefunden() {

        when(repository.findByKey(RKOM, DEUTSCHLAND, "SCHLUESSEL", Gueltigkeitsjahr.of(2020))).thenReturn(
                Optional.empty());

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class,
                () -> service.readReferenz(RKOM, "SCHLUESSEL", Gueltigkeitsjahr.of(2020), HAMBURG));

        assertEquals(RKOM, exception.referenzliste);
        assertEquals("SCHLUESSEL", exception.schluessel);
        assertEquals(Gueltigkeitsjahr.of(2020), exception.jahr);
        assertEquals(HAMBURG, exception.land);
    }

    @Test
    void readReferenz_Land_nichtGefunden() {

        when(repository.findByKey(Referenzliste.REPSG, HAMBURG, "SCHLUESSEL", Gueltigkeitsjahr.of(2020))).thenReturn(
                Optional.empty());

        when(referenzenBerechtigungsContext.checkLandZugriff(HAMBURG)).thenReturn(true);

        ReferenzdatenNotFoundException exception = assertThrows(ReferenzdatenNotFoundException.class,
                () -> service.readReferenz(REPSG, "SCHLUESSEL", Gueltigkeitsjahr.of(2020), HAMBURG));
        assertEquals(REPSG, exception.referenzliste);
        assertEquals("SCHLUESSEL", exception.schluessel);
        assertEquals(Gueltigkeitsjahr.of(2020), exception.jahr);
        assertEquals(HAMBURG, exception.land);
    }

    @Test
    void readReferenz() {

        Referenz ref = ReferenzBuilder.aReferenz(REPSG, "SCHLUESSEL").land(HAMBURG).id(1L).build();

        when(repository.findByKey(Referenzliste.REPSG, HAMBURG, "SCHLUESSEL", Gueltigkeitsjahr.of(2020))).thenReturn(
                Optional.of(ref));
        when(referenzenBerechtigungsContext.checkLandZugriff(HAMBURG)).thenReturn(true);

        Referenz readRef = service.readReferenz(REPSG, "SCHLUESSEL", Gueltigkeitsjahr.of(2020), HAMBURG);

        assertEquals(ref, readRef);
    }

    @Test
    void readReferenzlisteForEdit() {
        when(bearbeiter.isGesamtadmin()).thenReturn(false);
        when(bearbeiter.getLand()).thenReturn(HAMBURG);

        service.readReferenzlisteForEdit(RKOM, Function.identity());

        verify(repository).findByReferenzlisteAndLandInOrderBySortierAsc(RKOM, List.of(DEUTSCHLAND, HAMBURG));
    }

    @Test
    void readReferenzlisteForEdit_admin() {
        when(bearbeiter.isGesamtadmin()).thenReturn(true);

        service.readReferenzlisteForEdit(RKOM, Function.identity());

        verify(repository).findByReferenzlisteOrderBySortierAsc(RKOM);
    }

    @Test
    void readReferenz_bundesliste() {
        var jahr2020 = Gueltigkeitsjahr.of(2020);
        when(repository.findByKey(RFLEZ, DEUTSCHLAND, "SCHL", jahr2020)).thenReturn(
                Optional.of(aFlusseinzugsgebiet("SCHL").build()));

        service.readReferenz(RFLEZ, "SCHL", jahr2020, DEUTSCHLAND);

        verifyNoMoreInteractions(repository);
    }

    @Test
    void readReferenz_landesliste_landEintrag() {
        var jahr2020 = Gueltigkeitsjahr.of(2020);
        when(repository.findByKey(REINH, HAMBURG, "SCHL", jahr2020)).thenReturn(
                Optional.of(aReferenz(REINH, "SCHL").build()));
        when(referenzenBerechtigungsContext.checkLandZugriff(HAMBURG)).thenReturn(true);

        service.readReferenz(REINH, "SCHL", jahr2020, HAMBURG);

        verifyNoMoreInteractions(repository);
    }

    @Test
    void readReferenz_landesliste_bundesEintrag() {
        var jahr2020 = Gueltigkeitsjahr.of(2020);
        when(repository.findByKey(REINH, HAMBURG, "SCHL", Gueltigkeitsjahr.of(2020))).thenReturn(Optional.empty());
        when(repository.findByKey(REINH, DEUTSCHLAND, "SCHL", Gueltigkeitsjahr.of(2020))).thenReturn(
                Optional.of(aReferenz(REINH, "SCHL").land(DEUTSCHLAND).build()));
        when(referenzenBerechtigungsContext.checkLandZugriff(HAMBURG)).thenReturn(true);

        service.readReferenz(REINH, "SCHL", jahr2020, HAMBURG);

        verifyNoMoreInteractions(repository);
    }

    @Test
    void createReferenz() {
        var ref = aKommunikationsTypReferenz("A").referenznummer(null).build();
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(true);

        service.createReferenz(ref);

        verify(repository).save(ref);
    }

    @Test
    void createReferenz_keineBerechtigung() {
        var ref = aKommunikationsTypReferenz("A").referenznummer(null).build();
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(false);

        assertThrows(ReferenzdatenException.class, () ->service.createReferenz(ref));

    }

    @Test
    void updateReferenz_valid() {
        var ref = aKommunikationsTypReferenz("A").id(1L).referenznummer("1").build();
        when(repository.findById(1L)).thenReturn(Optional.of(ref));
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(true);
        service.updateReferenz(ref);

        verify(repository).save(ref);
    }

    @Test
    void updateReferenz_without_referenznummer() {
        var ref = aKommunikationsTypReferenz("A").id(1L).referenznummer(null).build();
        assertThrows(IllegalArgumentException.class, () -> service.updateReferenz(ref));
    }

    @Test
    void updateReferenz_unbekannteId() {
        var ref = aKommunikationsTypReferenz("A").id(1L).referenznummer("1").build();
        when(repository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(BubeEntityNotFoundException.class, () -> service.updateReferenz(ref));
    }

    @Test
    void updateReferenz_conflict() {
        var ref = aKommunikationsTypReferenz("A").id(1L)
                                                 .referenznummer("1")
                                                 .land(DEUTSCHLAND)
                                                 .gueltigVon(2019)
                                                 .gueltigBis(2020)
                                                 .build();
        when(repository.findById(1L)).thenReturn(Optional.of(ref));
        when(repository.functionalKeyDuplicate(ref.getId(), RKOM.name(), DEUTSCHLAND.getNr(), "A", 2019,
                2020)).thenReturn(true);
        when(referenzenBerechtigungsContext.checkBerechtigungWrite(any())).thenReturn(true);

        assertThrows(ReferenzdatenException.class, () -> service.updateReferenz(ref));
    }

    @Test
    void deleteReferenz() {
        var ref = aKommunikationsTypReferenz("A").id(1L).build();
        when(repository.findById(ref.getId())).thenReturn(Optional.of(ref));
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);

        service.deleteReferenz(ref.getId());

        verify(repository).deleteById(ref.getId());
        verify(referenzenBerechtigungsContext).checkBerechtigungDelete(any());
    }

    @Test
    void deleteMultipleReferenzen() {
        var ref1 = aKommunikationsTypReferenz("A").id(1L).build();
        var ref2 = aKommunikationsTypReferenz("B").id(2L).build();
        var ref3 = aKommunikationsTypReferenz("C").id(3L).build();

        when(repository.findById(1L)).thenReturn(Optional.of(ref1));
        when(repository.findById(2L)).thenReturn(Optional.of(ref2));
        when(repository.findById(3L)).thenReturn(Optional.of(ref3));
        when(referenzenBerechtigungsContext.checkBerechtigungDelete(any())).thenReturn(true);

        service.deleteMultipleReferenzen(List.of(1L, 2L, 3L));

        verify(repository).deleteById(1L);
        verify(repository).deleteById(2L);
        verify(repository).deleteById(3L);

    }
}
