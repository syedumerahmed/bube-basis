package de.wps.bube.basis.jobs.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.wps.bube.basis.stammdaten.domain.service.StammdatenJob;

class JobProtokollTest {

    private JobProtokoll jobProtokoll;

    @BeforeEach
    void setUp() {
        jobProtokoll = new JobProtokoll(StammdatenJob.PROTOKOLL_OPENING);
    }

    @Test
    void jobProtokollShouldStartWithDefaultOpening() {
        assertThat(jobProtokoll.getProtokoll()).isEqualTo(StammdatenJob.PROTOKOLL_OPENING);
    }

    @Test
    void jobProtokollShouldAddEintragAndNewLineToProtokoll() {
        jobProtokoll.addEintrag("A");
        jobProtokoll.addEintrag("B");
        assertThat(jobProtokoll.getProtokoll()).isEqualTo(
                StammdatenJob.PROTOKOLL_OPENING + System.lineSeparator() + "A" + System.lineSeparator() + "B");
    }

    @Test
    void jobProtokollShouldAddFehlerAndNewLineToProtokoll() {
        jobProtokoll.addFehler("Fehlertext 1", "exception 1");
        jobProtokoll.addFehler("Fehlertext 2", "exception 2");
        assertThat(jobProtokoll.getProtokoll()).isEqualTo(
                StammdatenJob.PROTOKOLL_OPENING +
                        System.lineSeparator() +
                        "Fehlertext 1" +
                        System.lineSeparator() +
                        "Technische Ursache: exception 1" +
                        System.lineSeparator() +
                        "Fehlertext 2" +
                        System.lineSeparator() +
                        "Technische Ursache: exception 2");
    }
}
