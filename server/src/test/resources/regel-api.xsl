<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml" xmlns:xs="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="*/@name"/>
                </title>
                <meta name="author" content="WPS - Workplace Solutions GmbH"/>
                <meta name="publisher" content="WPS - Workplace Solutions GmbH"/>
                <meta name="copyright" content="WPS - Workplace Solutions GmbH"/>
                <meta name="description" content="Dokumentation der BUBE-Regel-API"/>
                <meta name="keywords" content="Regeln, API, Dokumentation"/>
                <meta name="page-topic" content="Umwelt"/>
                <meta name="robots" content="noindex, nofollow"/>
                <meta name="generator" content="RegelApiDocumentationGenerator"/>
                <meta http-equiv="content-language" content="de"/>
                <xsl:comment>This file is automatically generated. DO NOT EDIT MANUALLY.</xsl:comment>
                <link rel="stylesheet" type="text/css" href="../regel-api.css"/>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="enum-type">

        <table>
            <thead>
                <tr>
                    <th>
                        <xsl:attribute name="colspan">
                            <xsl:value-of select="count(enum-constant[1]/@*)"/>
                        </xsl:attribute>
                        <xsl:text>Aufzählungstyp '</xsl:text>
                        <xsl:value-of select="@name"/>
                        <xsl:text>'</xsl:text>
                    </th>
                </tr>
                <tr>
                    <xsl:for-each select="enum-constant[1]/@*">
                        <th>
                            <xsl:value-of select="name()"/>
                        </th>
                    </xsl:for-each>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates/>
            </tbody>
        </table>

    </xsl:template>

    <xsl:template match="enum-constant">
        <tr>
            <xsl:for-each select="@*">
                <td>
                    <xsl:value-of select="."/>
                </td>
            </xsl:for-each>
        </tr>
    </xsl:template>

    <xsl:template match="bean-type">
        <xsl:variable name="includeDescription" select="bean-field/@description"/>
        <table>
            <thead>
                <tr>
                    <th class="top" colspan="2">
                        <xsl:attribute name="colspan">
                            <xsl:choose>
                                <xsl:when test="$includeDescription">3</xsl:when>
                                <xsl:otherwise>2</xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:text>Objekttyp '</xsl:text>
                        <xsl:value-of select="@name"/>
                        <xsl:text>'</xsl:text>
                    </th>
                </tr>
                <tr>
                    <th>Feld</th>
                    <th>Typ</th>
                    <xsl:if test="$includeDescription">
                        <th>Beschreibung</th>
                    </xsl:if>
                </tr>
            </thead>
            <tbody>
                <xsl:for-each select="bean-field[not(@params)]">
                    <xsl:sort select="@name"/>
                    <xsl:apply-templates select=".">
                        <xsl:with-param name="includeDescription" select="$includeDescription"/>
                    </xsl:apply-templates>
                </xsl:for-each>
                <xsl:if test="bean-field[@params]">
                    <tr>
                        <th>Methode</th>
                        <th>Rückgabe-Typ</th>
                        <xsl:if test="$includeDescription">
                            <th>Beschreibung</th>
                        </xsl:if>
                    </tr>
                    <xsl:for-each select="bean-field[@params]">
                        <xsl:sort select="@name"/>
                        <xsl:apply-templates select=".">
                            <xsl:with-param name="includeDescription" select="$includeDescription"/>
                        </xsl:apply-templates>
                    </xsl:for-each>
                </xsl:if>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="bean-field">
        <xsl:param name="includeDescription"/>
        <tr>
            <td>
                <xsl:value-of select="@name"/>
                <xsl:if test="@params">
                    <xsl:text>(</xsl:text>
                    <xsl:value-of select="@params"/>
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="@bean">
                        <xsl:attribute name="title">
                            <xsl:text>Objekttyp </xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@list">(Liste)</xsl:if>
                        </xsl:attribute>
                        <xsl:text>O: </xsl:text>
                    </xsl:when>
                    <xsl:when test="@enum">
                        <xsl:attribute name="title">
                            <xsl:text>Aufzählungstyp </xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@list">(Liste)</xsl:if>
                        </xsl:attribute>
                        <xsl:text>A: </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="title">
                            <xsl:text>Basis-Typ </xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@list">(Liste)</xsl:if>
                        </xsl:attribute>
                        <xsl:text>B: </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="@list">[</xsl:if>
                <a>
                    <xsl:attribute name="href">
                        <xsl:apply-templates select="." mode="href"/>
                    </xsl:attribute>
                    <xsl:value-of select="@type"/>
                </a>
                <xsl:if test="@ref-list">
                    <xsl:text>&lt;</xsl:text>
                    <xsl:value-of select="@ref-list"/>
                    <xsl:text>&gt;</xsl:text>
                </xsl:if>
                <xsl:if test="@list">]</xsl:if>
            </td>
            <xsl:if test="$includeDescription">
                <td>
                    <xsl:value-of select="@description"/>
                </td>
            </xsl:if>
        </tr>
    </xsl:template>

    <xsl:template match="bean-field" mode="href">
        <xsl:choose>
            <xsl:when test="@bean = 'true' or @enum = 'true'">
                <xsl:value-of select="@type"/>
                <xs:text>.html</xs:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>../base-types.html#</xsl:text>
                <xsl:value-of select="@type"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="types">
        <h1>BUBE-Regel-API</h1>
        <a style="display:block; padding-bottom: 10px;" href="../overview.html" target="content">Übersicht</a>
        <a style="display:block; padding-left: 10px;" href="../overview.html#literale" target="content">Literale</a>
        <a style="display:block; padding-left: 10px;" href="../overview.html#operatoren" target="content">Operatoren</a>
        <a style="display:block; padding-left: 10px;" href="../overview.html#ausdruecke" target="content">Objekt-Ausdrücke</a>
        <h2>Basis-Typen</h2>
        <xsl:apply-templates select="base-type" mode="types"/>
        <h2>Aufzählungstypen</h2>
        <xsl:for-each select="enum-type">
            <xsl:sort select="@name"/>
            <xsl:apply-templates select="." mode="types"/>
        </xsl:for-each>
        <h2>Objekttypen</h2>
        <xsl:for-each select="bean-type">
            <xsl:sort select="@name"/>
            <xsl:apply-templates select="." mode="types"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="base-type" mode="types">
        <div>
            <a href="../base-types.html#{@name}" target="content">
                <xsl:value-of select="@name"/>
            </a>
        </div>
    </xsl:template>

    <xsl:template match="enum-type|bean-type" mode="types">
        <div>
            <a href="{@name}.html" target="content">
                <xsl:value-of select="@name"/>
            </a>
        </div>
    </xsl:template>

</xsl:stylesheet>