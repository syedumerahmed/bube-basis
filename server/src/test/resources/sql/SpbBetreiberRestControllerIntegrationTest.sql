INSERT INTO behoerde (id, land, schluessel, bezeichnung, gueltig_von, gueltig_bis, ort, plz, letzte_aenderung)
VALUES (1, '02', 'HH', 'Hamburger Umweltbehörde', 2009, 2099, 'Hamburg', '20001', current_timestamp);

-- BST ohne SpbBetriebsstaette und ohne Betreiber
INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (101, 'Hamburg', '20101', '101', '02', 'bst101', 'BST 101',
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

-- BST mit SpbBetriebsstaette aber ohne Betreiber
INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (102, 'Hamburg', '20102', '102', '02', 'bst102', 'BST 102',
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

INSERT INTO spb_betriebsstaette(id, betriebsstaette_id, direkteinleiter, indirekteinleiter, erste_erfassung,
                                letzte_aenderung)
VALUES (202, 102, false, false, current_timestamp, current_timestamp);

-- BST ohne SpbBetriebsstaette, mit Betreiber, aber ohne SpbBetreiber
INSERT INTO betreiber(id, land, name, berichtsjahr_id, ort, plz, betreiberdaten_uebernommen, erste_erfassung,
                      letzte_aenderung)
VALUES (13, '02', 'B 13', 10134, 'Hamburg', '20013', false, current_timestamp, current_timestamp);

INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name, betreiber_id,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (103, 'Hamburg', '20103', '103', '02', 'bst103', 'BST 103', 13,
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

-- BST mit SpbBetriebsstaette und mit Betreiber, aber ohne SpbBetreiber
INSERT INTO betreiber(id, land, name, berichtsjahr_id, ort, plz, betreiberdaten_uebernommen, erste_erfassung,
                      letzte_aenderung)
VALUES (14, '02', 'B 14', 10134, 'Hamburg', '20014', false, current_timestamp, current_timestamp);

INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name, betreiber_id,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (104, 'Hamburg', '20104', '104', '02', 'bst104', 'BST 104', 14,
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

INSERT INTO spb_betriebsstaette(id, betriebsstaette_id, direkteinleiter, indirekteinleiter, erste_erfassung,
                                letzte_aenderung)
VALUES (204, 104, false, false, current_timestamp, current_timestamp);

-- BST ohne SpbBetriebsstaette, aber mit Betreiber und SpbBetreiber
INSERT INTO betreiber(id, land, name, berichtsjahr_id, ort, plz, betreiberdaten_uebernommen, erste_erfassung,
                      letzte_aenderung)
VALUES (15, '02', 'B 15', 10134, 'Hamburg', '20015', false, current_timestamp, current_timestamp);

INSERT INTO spb_betreiber(id, betreiber_id, name, ort, plz, erste_erfassung, letzte_aenderung)
VALUES (25, 15, 'B 25', 'Hamburg', '20025', current_timestamp, current_timestamp);

INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name, betreiber_id,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (105, 'Hamburg', '20105', '105', '02', 'bst105', 'BST 105', 15,
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

-- BST mit SpbBetriebsstaette, mit Betreiber und mit SpbBetreiber
INSERT INTO betreiber(id, land, name, berichtsjahr_id, ort, plz, betreiberdaten_uebernommen, erste_erfassung,
                      letzte_aenderung)
VALUES (16, '02', 'B 16', 10134, 'Hamburg', '20016', false, current_timestamp, current_timestamp);

INSERT INTO spb_betreiber(id, betreiber_id, name, ort, plz, erste_erfassung, letzte_aenderung)
VALUES (26, 16, 'B 26', 'Hamburg', '20026', current_timestamp, current_timestamp);

INSERT INTO betriebsstaette (id, ort, plz, betriebsstaette_nr, land, local_id, name, betreiber_id,
                             berichtsjahr_id, betriebsstatus_id, gemeindekennziffer_id, zustaendige_behoerde_id,
                             betreiberdaten_uebernommen, direkteinleiter, indirekteinleiter, erste_erfassung,
                             letzte_aenderung)
VALUES (106, 'Hamburg', '20106', '106', '02', 'bst106', 'BST 106', 16,
        10134, 10011, 10108, 1, false, false, false, current_timestamp, current_timestamp);

INSERT INTO spb_betriebsstaette(id, betriebsstaette_id, direkteinleiter, indirekteinleiter, spb_betreiber_id,
                                erste_erfassung, letzte_aenderung)
VALUES (206, 106, false, false, 26, current_timestamp, current_timestamp);
