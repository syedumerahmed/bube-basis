# BUBE-Online - Basismodul - Server

Der Server ist eine Spring-Boot Anwendung auf Basis von Java 17.

## Bauen und Starten des Servers

0. Benötigte Programme:
    * `git`
    * `mvn`
    * JDK 17
    * Java-IDE
1. Repository auschecken entweder:
    1. Via SSH: `git clone git@gitlab.wps.de:bube/bube-basis.git` oder
    1. Via HTTPS: `git clone https://gitlab.wps.de/bube/bube-basis.git`
2. Nach Auschecken des Repositories kann das Projekt im [`server`](.)-Verzeichnis mit Maven gebaut werden: `mvn compile`
3. Für die Ausführung wird eine Postgres-Datenbank benötigt. 
4. Die Anwendung lässt sich dann für die lokale Entwicklung mittels `mvn spring-boot:run` oder wahlweise über die IDE
   starten.

## API generieren

Die API wird über ein OpenApi-Spezifikation definiert. Nach jeder Änderung an der Spezifikation muss diese per `mvn clean compile` neu generiert werden.

## Unit Tests

Wir verwenden JUnit 5 für unsere Tests. Die Tests liegen in einer parallelen Packagestruktur zu dem Produktivcode
in [`src/test`](src/test). Ausgeführt werden sie über `mvn test` bzw. die IDE.

## Codestyle

Der Code wird mit dem BUBE-Formatter formatiert, der entsprechend in der IDE eingestellt werden muss:

* IntelliJ IDEA: `File | Settings | Editor | Code Style | Java`
    * Bei Scheme auf das Zahnrad klicken, dort dann `Import Scheme | IntelliJ IDEA code style XML`
    * Die Datei [`bube-basis/BUBE-Formatter-intellij.xml`](../BUBE-Formatter-intellij.xml) auswählen und anwenden
* Eclipse: `Java > Code Style > Formatter > Import...`
    * Die Datei [`bube-basis/BUBE-Formatter-eclipse.xml`](../BUBE-Formatter-eclipse.xml) auswählen und anwenden
    
# Infrastruktur

Der Bube Server fährt Docker Container auf `dev.bube.aws.wps.de` für den Webserver, den Webclient (inkl. Nginx) und die
Datenbank hoch. Die Zertifikatsverwaltung funktioniert hier automatisch über das Nginx Image mit einem integrierten
Certbot. Nach dem Start des Images ist der Client erstmal unsicher über Port 80 erreichbar. Mit dem integrierten
Skript `init-certbot.sh` initialisiert man Certbot, wodurch dieser ein Zertifikat erstellt und den Nginx optimal für
HTTPS konfiguriert. Dies passiert aktuell automatisch über die Gitlab Pipeline. Der Pfad für die Zertifikate wird als
Volume gemounted, wodurch diese persistiert werden. Liegen dort bereits gültige Zertifikate, lädt Certbot keine neuen (
würde sie aber falls notwendig erneuern).

## Verbindung mit IntelliJ Ultimate / DBeaver

Die Datenbanken der Dev- und Testumgebung können auch per Client mit SSH-Tunneling geöffnet werden.

1. Database-Reiter -> Add DataSource > PostgreSQL
0. sinnvollen namen je nach umgebung vergeben
0. Reiter SSH konfigurieren
    * Einstellungen wie beim Verbinden auf die Instanzen per SSH (Zugangsdaten in KeePass)
0. Reiter General
    * Auth User/Passwort: s. KeePass
    * Host-IP ist die IP des Containers auf der EC2-Instanz


# AWS Umgebung
## Datenbank-Backup
Auf dem Wirt liegt das Skript `backup_db.sh`, mit der im Ordner ~/backups bei Bedarf ein Datenbankabzug der Postgres DB erzeugt wird.
Das Backup der DB wird aktuell nicht automatisch ausgeführt und die Backups sollten manuell gesichert werden.
Das Backup Skript wird von der Pipeline auf den Server kopiert.

## Logs Anzeigen

Auf dem Server kann man sich über dem Befehl `docker-compose logs webserver` die letzten Logs anzeigen lassen. Mit dem Befehl `docker-compose logs -f webserver` bekommt man eine Live-Ansicht der Logs.

