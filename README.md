# BUBE-Online - Basismodul

Die Funktionalität von BUBE-Online wird in mehreren Self-Contained-Systems zur Verfügung gestellt, die in jeweils
eigenen Repositories liegen. (
s. [Architekturdokumentation](https://wps-bube.atlassian.net/wiki/spaces/BB/pages/125468673/Architekturdokumentation))

Dieses Basismodul stellt die grundlegende Verwaltung von Stamm-, Referenz- und Benutzerdaten zur Verfügung.

# Lizenzen

(Meine Haupt-Quelle: https://medium.com/@vovabilonenko/licenses-of-npm-dependencies-bacaa00c8c65)
BUBE wird unter der AGPL Lizenz veröffentlicht und hat dementsprechend auch Anforderungen an die Lizenzen von
verwendeten Bibliotheken. Die Liste erlaubter Lizenzen ist im Lastenheft zu finden. Das Thema Lizenzen ist relativ
kompliziert. In der Regel greifen OpenSource Lizenzen (darauf bezieht sich dieser Absatz!) erst, wenn sie "verteilt"
werden. Ausgenommen von Network-Protected Lizenzen (z.B. AGPL) bedeutet dies: Ein expliziter Download der Bibliotheken.
Das Nutzen über einen Webserver (Also eine Angular App) zählt dabei nicht. Das bedeutet, dass in jedem Fall auch nur die
Produktiv-Bibliotheken relevant sind. Am besten sollten Lizenzfragen trotzdem in Einzelfall geklärt werden. Die
verwendeten Bibliotheks-Lizenzen vom Client und Server lassen sich extrahieren:

## Client

Der Angular Client benutzt den license-checker, um die Lizenzen zu überprüfen. Mit `yarn check-license` werden die verwendeten Bibliotheken und deren Lizenzen mit einer Whitelist abgeglichen.

## Server

Wir nutzen das license-maven-plugin (s. [POM](./server/pom.xml)). Mit dessen aktueller Konfiguration lassen sich die
verwendeten Produktivlizenzen extrahieren. Diese liegen nach der Ausführung in der `licenses.xml` unter `/target/generated-resources/`
