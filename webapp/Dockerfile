### define builder image
FROM node:lts-alpine as builder

RUN apk add openjdk11

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN CYPRESS_INSTALL_BINARY=0 yarn --frozen-lockfile && yarn check-license && rm -rf `yarn cache dir`
COPY . .

RUN NODE_OPTIONS="--max-old-space-size=4096" yarn build:ci

### end of builder image

### define actual image
FROM nginx:alpine
# Certbot hinzufügen
RUN apk add certbot certbot-nginx

COPY nginx/init-certbot.sh .

COPY nginx/default.conf.template /etc/nginx/templates/default.conf.template
COPY --from=builder /usr/src/app/dist/webapp /usr/share/nginx/html

CMD ["sh", "-c", "/docker-entrypoint.sh nginx && sh ./init-certbot.sh && nginx -s stop && sleep 2 && nginx -g 'daemon off;'"]
### end of actual image
