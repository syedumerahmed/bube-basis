# Webapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Installation

Nach Checkout des Repository muss ein das Verzeichnis `webapp` gewechselt werden.

Um die benötigten Abhängigkeiten zu installieren, müssen diese über den Paketmanager heruntergeladen werden:
Als Paketmanager verwenden wir [`yarn`](https://yarnpkg.com/) im Gegensatz zu `npm`. `yarn` kann über `npm` installiert werden und anschließend die Abhängigkeiten installieren:

```shell script
npm install --global yarn
yarn install
```

## API generieren

Die API wird über ein OpenApi-Spezifikation definiert. Um sie zu generieren muss, vor dem ersten Start und nach jeder Änderung an der Spezifikation, auf Windows `yarn run generate:api-win` und auf Linux `yarn run generate:api-linux` ausgeführt werden.

## Lokale Entwicklung

Mit `yarn start` wird ein lokaler DevServer gestartet, der unter `http://localhost:4200/` erreichbar ist. Die App wird automatisch neu geladen, wenn Source-Dateien verändert werden (Hot-Reloading).

Mit `yarn start:dev` kann man sich mit dem Frontend gegen unsere Entwicklungsumgebung `dev.bube.aws.wps.de` verbinden. In diesem Fall werden alle API-Requests an das Backend dorthin umgeleitet.

## Testing

S. [Architekturdokumentation](https://wps-bube.atlassian.net/wiki/spaces/BB/pages/270467073/Continuous+Integration+Continuous+Deployment#Client)

### Unit-Tests

Mit `ng test` können die Unit-Tests mit [Jest](https://jestjs.io/) gestartet werden.

`ng test --watch` führt automatisch die Tests aus, die Abhängigkeiten zu verändertem Code seit dem letzten Commit haben. Beim Editieren von Quellcode werden die Tests automatisch ausgeführt.

#### Fehlersuche

Kommt es bei der Initialisierung der Komponenten in den Tests (`fixture.detectChanges()`) zu nicht nachvollziehbaren Fehlern, kann es sein, dass Fehler in den `.subscribe()`-Methoden von RxJs gefangen und im Test verschluckt werden.
In diesem Fall kann folgender Befehl temporär für die Fehlersuche genutzt werden:

```typescript
import { config } from "rxjs";
config.useDeprecatedSynchronousErrorHandling = true;
```

#### ClarityV5 Probleme mit Jest:

In Version V5 von Clarity werden experimentelle Features von Javascript verwendet, die in den Unit-Tests mit Jest nicht korrekt kompiliert werden können. Dies betrifft das Clarity-Icons Modul in `@cds/core/icon`.

Um dies zu beheben, konfigurieren wir `jest` so um, dass während der Tests alle Modulzugriffe auf` @cds/core/icon` zu einem von uns erstellten TestModul umgeleitet werden. Dies können wir machen, da in den Unit-Tests die Icons nicht benötigt werden.

`package.json`:

```json5
{
  // ...
  jest: {
    // ...
    moduleNameMapper: {
      // ...
      "^@cds/core/icon": "<rootDir>/src/testModules/cds-icon-test.module.ts",
    },
  },
}
```

TestModul:

```typescript
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [],
  imports: [CommonModule],
})
export class CdsIconTestModule {}

export class ClarityIcons {
  public static addIcons(any: any[]) {}
}
```

### End-to-End Tests

Server muss gestartet sein. (s. [Lokale Entwicklung](#lokale-entwicklung))

Mit `yarn cy:open` kann die interaktive Oberfläche von [Cypress](https://www.cypress.io/) gestartet werden.

## Module

S. [Architekturdokumentation](https://wps-bube.atlassian.net/wiki/spaces/BB/pages/243105808/Fachliche+Schichtung+vertikal#Client)

### Neues Modul erstellen

Das Erstellen eines neuen (Sub-)Moduls ist mittels Angular-CLI sehr einfach möglich:

```shell script
ng generate module $MODUL_NAME --module $PARENT_MODULE
```

Soll das Modul Routing selbst verwalten (aber ohne Lazy-Loading) muss außerdem der Parameter `--routing` angegeben werden.

Soll ein Modul Lazy-geloadet werden, lässt sich die Grundstruktur auch über Angular-CLI erstellen:

```shell script
ng generate module $MODUL_NAME --route $ROUTE --module $PARENT_MODULE
```

S. [Angular Dokumentation](https://angular.io/guide/lazy-loading-ngmodules#lazy-loading-feature-modules)
