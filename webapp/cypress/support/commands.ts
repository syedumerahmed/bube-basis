// ***********************************************
// This example namespace declaration will help
// with Intellisense and code completion in your
// IDE or Text Editor.
// ***********************************************
// declare namespace Cypress {
//   interface Chainable<Subject = any> {
//     customCommand(param: any): typeof customCommand;
//   }
// }
//
// function customCommand(param: any): void {
//   console.warn(param);
// }
//
// NOTE: You can use it like so:
// Cypress.Commands.add('customCommand', customCommand);
//
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-keycloak-commands';
import 'cypress-file-upload';
import { BehoerdeListItemDto, BetriebsstaetteDto } from '@api/stammdaten';
import { BehoerdeDto } from '@api/referenzdaten';
import Chainable = Cypress.Chainable;
import RequestOptions = Cypress.RequestOptions;
import Response = Cypress.Response;

declare global {
  namespace Cypress {
    interface Chainable {
      authenticatedRequest: typeof authenticatedRequest;
      deleteBetriebsstaetteByJahrLandNummer: typeof deleteBetriebsstaetteByJahrLandNummer;
      getOrCreateBehoerde: typeof getOrCreateBehoerde;
    }
  }
}

function authenticatedRequest<T = void>(
  options: Partial<RequestOptions>
): Chainable<Response<T>> {
  if (!this.tokens) {
    throw Error(`Not logged in! Use cy.kcLogin(username).as('tokens') first!`);
  }
  return cy.request<T>({
    ...options,
    auth: { bearer: this.tokens.access_token },
  });
}

function deleteBetriebsstaetteByJahrLandNummer(
  jahr: string,
  land: string,
  nummer: string
): Chainable<Response<BetriebsstaetteDto>> {
  return cy
    .authenticatedRequest<BetriebsstaetteDto>({
      url: `/api/betriebsstaetten/readbyjahrlandnummer?jahrSchluessel=${jahr}&landSchluessel=${land}&nummer=${nummer}`,
      failOnStatusCode: false,
    })
    .then((response) => {
      if (response.isOkStatusCode) {
        // Existierende Betriebsstätte löschen
        cy.authenticatedRequest({
          method: 'DELETE',
          url: `/api/betriebsstaetten/${response.body.id}`,
        });
      }
    });
}

function getOrCreateBehoerde(
  behoerde: BehoerdeDto
): Chainable<BehoerdeListItemDto> {
  // Behörde abfragen
  return cy
    .authenticatedRequest<Array<BehoerdeListItemDto>>({
      url: `/api/referenzen/behoerden/jahr/${behoerde.gueltigVon}?landNr=${behoerde.land}`,
      method: 'GET',
    })
    .then((response) => {
      if (response.body.length === 0) {
        // falls keine Behörden existieren, wird eine angelegt
        return cy
          .authenticatedRequest<BehoerdeListItemDto>({
            url: `/api/referenzen/behoerden`,
            method: 'POST',
            body: behoerde,
          })
          .its('body');
      } else {
        return cy.wrap(response.body[0]);
      }
    });
}

Cypress.Commands.add('authenticatedRequest', authenticatedRequest);
Cypress.Commands.add(
  'deleteBetriebsstaetteByJahrLandNummer',
  deleteBetriebsstaetteByJahrLandNummer
);
Cypress.Commands.add('getOrCreateBehoerde', getOrCreateBehoerde);
