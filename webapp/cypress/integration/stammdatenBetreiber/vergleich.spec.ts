import { aBehoerde } from '../../fixtures/referenzen/behoerde';
import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';
import { ReferenzDto } from '@api/stammdaten';
import {
  AdresseDto,
  AnlageDto,
  AnlagenteilDto,
  BetriebsstaetteDto,
  QuelleDto,
  VorschriftDto,
} from '@api/stammdaten-uebernahme';
import {
  SpbAnlageDto,
  SpbAnlagenteilDto,
  SpbBetriebsstaetteDto,
  SpbQuelleDto,
} from '@api/stammdatenbetreiber';

describe('Stammdaten Betreiber Vergleich', () => {
  const J2020: ReferenzDto = {
    schluessel: '2020',
    referenzliste: 'RJAHR',
    land: '00',
    id: 10134,
    ktext: '2020',
    ltext: '2020 laaaang',
    sortier: '14',
    gueltigVon: 2007,
    gueltigBis: 2099,
  };
  const betriebsstatus: ReferenzDto = {
    id: 10011,
    gueltigBis: 2099,
    gueltigVon: 2007,
    ktext: 'in Betrieb',
    land: '00',
    letzteAenderung: '2020-02-09T00:00:00Z',
    ltext: 'in Betrieb',
    referenzliste: 'RBETZ',
    schluessel: '01',
    sortier: '01',
  };

  const vorschriften: Array<VorschriftDto> = [
    {
      art: {
        id: 10163,
        referenznummer: '10163',
        land: '00',
        referenzliste: 'RVORS',
        schluessel: '13BV',
        sortier: '03',
        ktext: '13. BImSchV',
        ltext: null,
        gueltigVon: 2007,
        gueltigBis: 2099,
        letzteAenderung: '2021-08-31T10:18:46.325439Z',
        eu: null,
        strg: null,
      },
      haupttaetigkeit: null,
      taetigkeiten: [],
    },
    {
      art: {
        id: 10164,
        referenznummer: '10164',
        land: '00',
        referenzliste: 'RVORS',
        schluessel: '17BV',
        sortier: '04',
        ktext: '17. BImSchV',
        ltext: null,
        gueltigVon: 2007,
        gueltigBis: 2099,
        letzteAenderung: '2021-08-31T10:18:50.123885Z',
        eu: null,
        strg: null,
      },
      haupttaetigkeit: null,
      taetigkeiten: [],
    },
    {
      art: {
        id: 10166,
        referenznummer: '10166',
        land: '00',
        referenzliste: 'RVORS',
        schluessel: 'IE-RL',
        sortier: '07',
        ktext: 'IE-RL',
        ltext: null,
        gueltigVon: 2007,
        gueltigBis: 2099,
        letzteAenderung: '2021-08-31T10:18:58.987866Z',
        eu: null,
        strg: 'RIET',
      },
      haupttaetigkeit: {
        id: 13052,
        referenznummer: '13052',
        land: '00',
        referenzliste: 'RIET',
        schluessel: '1.3',
        sortier: '03',
        ktext: 'Kokereien',
        ltext: 'Erzeugung von Koks',
        gueltigVon: 2007,
        gueltigBis: 2099,
        letzteAenderung: '2021-09-03T07:32:11.870524Z',
        eu: null,
        strg: null,
      },
      taetigkeiten: [
        {
          id: 13052,
          referenznummer: '13052',
          land: '00',
          referenzliste: 'RIET',
          schluessel: '1.3',
          sortier: '03',
          ktext: 'Kokereien',
          ltext: 'Erzeugung von Koks',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2021-09-03T07:32:11.870524Z',
          eu: null,
          strg: null,
        },
      ],
    },
  ];
  const Adresse: AdresseDto = {
    ort: 'Hamburg',
    plz: 'plz',
  };

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');
    // Behörde für Hamburg abfragen
    cy.getOrCreateBehoerde({
      ...aBehoerde(),
      land: '02',
      gueltigVon: 2020,
      gueltigBis: 2020,
    }).as('behoerde');
  });

  it('should be able to change BetriebsstaetteDaten', function () {
    // SETUP
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', '18');
    // Zwei Betriebsstätten mit Kindobjekten per API erstellen
    const betriebsstaetteUebernahme18: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr: '18',
      name: 'BetriebsstätteUebernahme18',
      zustaendigeBehoerde: this.behoerde,
    };
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaetteUebernahme18,
    })
      .its('body')
      .as('betriebsstaetteUebernahme18');

    // Anlagen(teile) und Quellen hinzufügen
    cy.get('@betriebsstaetteUebernahme18')
      .its('id')
      .then((parentBetriebsstaetteId) => {
        const anlageDto: AnlageDto = {
          anlageNr: '0118',
          name: 'BST18ANL0118',
          parentBetriebsstaetteId,
        };
        const quelleDto: QuelleDto = {
          quelleNr: '0118',
          name: 'BST18QLE0118',
          parentBetriebsstaetteId,
        };
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/quellen',
          body: quelleDto,
        });
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/anlagen',
          body: anlageDto,
        })
          .its('body')
          .as('anlage');

        cy.get('@anlage')
          .its('id')
          .then((parentAnlageId) => {
            const quelleDto: QuelleDto = {
              quelleNr: '0118',
              name: 'BST18ANL0118QLE0118',
              parentAnlageId,
            };
            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/quellen',
              body: quelleDto,
            });
            const anlagenteilDto: AnlagenteilDto = {
              anlagenteilNr: '0118',
              name: 'BST18ANL0118ANT0118',
              parentAnlageId,
              parentBetriebsstaetteId,
            };

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/anlagenteil',
              body: anlagenteilDto,
            });
          });
      });

    // TEST
    cy.kcLogout();
    cy.kcLogin('betreiber2_test_user').as('tokens');

    cy.visit('/stammdatenbetreiber/jahr/2020');

    // Betriebsstätte bearbeiten
    cy.get('[data-cy=betriebsstaetteRow]')
      .contains('[data-cy=betriebsstaetteRow]', 'BetriebsstätteUebernahme18')
      .find('[data-cy=oeffneBetriebsstaette]')
      .click();
    cy.get('[data-cy=inbetriebnahme]').clear().type('12.05.2020');
    cy.get('[data-cy=betriebsstatus] > select').select('1: Object');
    cy.get('[data-cy=ost]').clear().type('1');
    cy.get('[data-cy=nord]').clear().type('3');
    cy.get('[data-cy=epsg] > select').select('1: Object');
    cy.get('[data-cy=abfall] input').clear().type('22{enter}');
    cy.get('[data-cy=abfall] input').type('20{enter}');
    cy.get('[data-cy=tabAdresse]').click();
    cy.get('[data-cy=strasse-input]').clear().type('15');
    cy.get('#kommunikation').click();
    cy.get('[data-cy=add-kommunikation-btn]').click();
    cy.get('[data-cy=typ-select] > select').select('2: Object');
    cy.get('[data-cy=verbindung-input]').clear().type('0123');
    cy.get('[data-cy=uebernehmen-btn]').click();
    cy.get('#ansprechpartner').click();
    cy.get('.pt-2 > .btn').click();
    cy.get('[data-cy=vorname]').clear().type('TestName');
    cy.get('[data-cy=nachname]').clear().type('TestNachname');
    cy.get('.clr-combobox-trigger > cds-icon').click();
    cy.get('[data-cy=funktion-options] > clr-option:nth-child(1)').click();
    cy.get('.modal-body > .btn').click();
    cy.get('app-referenz-select.ng-pristine > select').select('2: Object');
    cy.get('[data-cy=kommunikation]').clear().type('156');
    cy.get('.modal-footer > .btn-primary').click();
    cy.intercept('POST', '/api/spbbetriebsstaetten').as('saveBetriebsstaette');
    cy.get('[data-cy=saveBetriebsstaette]').click();
    cy.wait('@saveBetriebsstaette')
      .its('response.statusCode')
      .should('eq', 200);

    // Anlage bearbeiten
    cy.get('[data-cy=openAnlage0118]').click();
    cy.wait(1000);
    cy.get('[data-cy=inbetriebnahme] button').click();
    cy.get('clr-calendar').contains('2').click();
    cy.get(
      'app-referenz-select[formControlName=betriebsstatus] > select'
    ).select('1: Object');
    cy.get('[data-cy=anlageName]').clear().type('BST18ANL011818');
    cy.get('[data-cy=leistungen]').click();
    cy.get('[data-cy=neu]').click();
    cy.get('[data-cy=leistungswert]').clear().type('1');
    cy.get('.clr-combobox-trigger').click();
    cy.get('[data-cy=einheit-options] > clr-option:nth-child(2)').click();
    cy.get('select[name=klasse]').select('1: INSTALLIERT');
    cy.get('[data-cy=Bezug]').clear().type('test');
    cy.get('[data-cy=save]').click();
    cy.intercept('POST', '/api/spbanlagen').as('saveAnlage');
    cy.get('[data-cy=saveAnlage]').click();
    cy.wait('@saveAnlage').its('response.statusCode').should('eq', 200);

    // Anlagenteil bearbeiten
    cy.get('[data-cy=openAnlagenteil0118] > cds-icon').click();
    cy.get(
      '.clr-form > :nth-child(3) > .clr-control-container > .clr-input-wrapper'
    ).click();
    cy.get('[data-cy=anlageName]').clear().type('BST18ANL0118ANT01189');
    cy.get('[data-cy=anlageNr]').clear().type('01183');
    cy.intercept('POST', '/api/spbanlagenteil').as('saveAnlagenteil');
    cy.get('[data-cy=saveAnlagenteil]').click();
    cy.wait('@saveAnlagenteil').its('response.statusCode').should('eq', 200);

    // Quelle bearbeiten
    cy.get('[data-cy=anlagenteilNavBack]').click();
    cy.get('[data-cy="0118"] > cds-icon').click();
    cy.get('[data-cy=flaeche]').clear().type('5');
    cy.intercept('POST', '/api/spbquellen').as('saveQuelle');
    cy.get('[data-cy=saveQuelle]').click();
    cy.wait('@saveQuelle').its('response.statusCode').should('eq', 200);

    // Neue SpbAnlage erstellen
    cy.get('[data-cy=quelleNavBack]').click();
    cy.get('[data-cy=anlageNavBack]').click();
    cy.get('[data-cy=createAnlage]').click();
    cy.get('[data-cy=anlageAllgemein] [data-cy=anlageNr]').clear().type('556');
    cy.get('[data-cy=anlageName]').clear().type('TestAnl');
    cy.get('[data-cy=saveAnlage]').click();
    cy.wait('@saveAnlage').its('response.statusCode').should('eq', 200);

    // Neues SpbAnlagenteil erstellen
    cy.get('[data-cy=app-loading]').should('not.exist');
    cy.get('[data-cy=createAnlagenteil]').click();
    cy.get('[data-cy=anlagenteilAllgemein] [data-cy=anlageNr]')
      .clear()
      .type('557');
    cy.get('[data-cy=anlageName]').clear().type('TestAnlTeil');
    cy.get('[data-cy=saveAnlagenteil]').click();
    cy.wait('@saveAnlagenteil').its('response.statusCode').should('eq', 200);

    // Neue SpbQuelle erstellen
    cy.get('[data-cy=app-loading]').should('not.exist');
    cy.get('[data-cy=anlagenteilNavBack]').click();
    cy.get('[data-cy=createQuelle]').click();
    cy.get('[data-cy=quelleNr]').clear().type('89');
    cy.get('[data-cy=quellenName]').clear().type('TestQuelle');
    cy.get('[data-cy=saveQuelle]').click();
    cy.wait('@saveQuelle').its('response.statusCode').should('eq', 200);
  });

  it('should show Vergleich of Full BetriebsstaetteBaum', function () {
    // SETUP
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', '18');
    // Zwei Betriebsstätten mit Kindobjekten per API erstellen
    const betriebsstaetteUebernahme18: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr: '18',
      name: 'BetriebsstätteUebernahme18',
      zustaendigeBehoerde: this.behoerde,
    };

    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaetteUebernahme18,
    })
      .its('body')
      .as('betriebsstaetteUebernahme18');

    // Anlagen(teile) und Quellen hinzufügen
    cy.get('@betriebsstaetteUebernahme18')
      .its('id')
      .then((parentBetriebsstaetteId) => {
        const spbBetriebsstaette18: SpbBetriebsstaetteDto = {
          adresse: Adresse,
          berichtsjahr: J2020,
          betriebsstatus: betriebsstatus,
          land: '02',
          zustaendigeBehoerde: '10',
          betriebsstaetteNr: '18',
          name: 'verändert18',
          betriebsstaetteId: parentBetriebsstaetteId,
          ansprechpartner: [],
        };

        const anlageDto: AnlageDto = {
          anlageNr: '0118',
          name: 'BST18ANL0118',
          parentBetriebsstaetteId,
        };
        const quelleDto: QuelleDto = {
          quelleNr: '0118',
          name: 'BST18QLE0118',
          parentBetriebsstaetteId,
        };
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/quellen',
          body: quelleDto,
        });
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/anlagen',
          body: anlageDto,
        })
          .its('body')
          .as('anlage');

        cy.kcLogout();
        cy.kcLogin('betreiber2_test_user').as('tokens');

        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/spbbetriebsstaetten',
          body: spbBetriebsstaette18,
        });

        cy.kcLogout();
        cy.kcLogin('admin_hh_test_user').as('tokens');

        cy.get('@anlage')
          .its('id')
          .then((parentAnlageId) => {
            const quelleDto: QuelleDto = {
              quelleNr: '0118',
              name: 'BST18ANL0118QLE0118',
              parentAnlageId,
            };
            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/quellen',
              body: quelleDto,
            })
              .its('body')
              .as('quelle');
            const anlagenteilDto: AnlagenteilDto = {
              anlagenteilNr: '0118',
              name: 'BST18ANL0118ANT0118',
              parentAnlageId,
              parentBetriebsstaetteId,
            };
            const spbAnlage: SpbAnlageDto = {
              anlageNr: '0118',
              anlageId: parentAnlageId,
              parentBetriebsstaetteId,
              name: 'newName',
            };

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/anlagenteil',
              body: anlagenteilDto,
            })
              .its('body')
              .as('anlagenteil');

            cy.kcLogout();
            cy.kcLogin('betreiber2_test_user').as('tokens');

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/spbanlagen',
              body: spbAnlage,
            });
            cy.get('@quelle')
              .its('id')
              .then((quelleId) => {
                const spbQuelle: SpbQuelleDto = {
                  quelleNr: '0118',
                  quelleId,
                  name: 'verändert',
                };
                cy.authenticatedRequest({
                  method: 'POST',
                  url: '/api/spbquellen',
                  body: spbQuelle,
                });
              });

            cy.get('@anlagenteil')
              .its('id')
              .then((anlagenteilId) => {
                const spbAnlagenteil: SpbAnlagenteilDto = {
                  anlagenteilId,
                  parentAnlageId,
                  anlageNr: '0118',
                  name: 'verändert',
                };

                cy.authenticatedRequest({
                  method: 'POST',
                  url: '/api/spbanlagenteil',
                  body: spbAnlagenteil,
                });
              });
          });
      });

    // TEST

    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    cy.visit('/stammdaten/jahr/2020');
    // Übernahme Betriebsstaette

    /* ==== Generated with Cypress Studio ==== */

    cy.get('[data-cy=betriebsstaetteRow]')
      .contains('[data-cy=betriebsstaetteRow]', 'BetriebsstätteUebernahme18')
      .find('[data-cy=oeffneBetriebsstaette]')
      .click();
    cy.get('[data-cy=aufgabenUebernahme]').click();
    cy.get('[title="Stammdaten Betreiber übernehmen"]').click();
    cy.get('[data-cy=allgemeinAccordion').click();
    cy.get('.clr-control-label').click();
    cy.get('[data-cy=adresseAccordion]').click();
    cy.get(
      '[label="PLZ"] > .clr-row > .clr-col-1 > .clr-checkbox-wrapper > .clr-control-label'
    ).click();
    cy.get('[data-cy=uebernehmen').click();
    cy.intercept('POST', '/api/betriebsstaetten/uebernehmen/uebernahme').as(
      'uebernehmeBst'
    );
    cy.get('[data-cy=uebernehmenDialogButton').click();
    cy.wait('@uebernehmeBst').its('response.statusCode').should('eq', 200);

    cy.get('[data-cy=name]').should('have.value', 'verändert18');
    cy.get('[data-cy=tabAdresse]').click();
    cy.get('[data-cy=plz]').should('have.value', 'plz');
    cy.get('[data-cy=ort-input]').should('have.value', 'Hamburg');
    cy.get('[data-cy=tabAllgemein]').click();

    // Übernahme Anlage

    cy.get('[data-cy=openAnlage0118] > cds-icon').click();
    cy.wait(2000);
    cy.contains('button', 'Übernehmen').click();
    cy.wait(300);
    cy.get('[data-cy=allgemeinAccordion]').click();
    cy.get('.clr-control-label').click();
    cy.get('[data-cy=uebernehmen').click();
    cy.intercept('POST', '/api/anlagen/uebernehmen/uebernahme').as(
      'uebernehmeAnlage'
    );
    cy.get('[data-cy=uebernehmenDialogButton').click();
    cy.wait('@uebernehmeAnlage').its('response.statusCode').should('eq', 200);
    cy.get('[data-cy=anlageName]').should('have.value', 'newName');

    /* ==== End Cypress Studio ==== */

    cy.get('[aria-label="Öffnen"]').click();
    cy.wait(3000);

    cy.contains('button', 'Übernehmen').click();
    cy.wait(300);
    cy.get('.clr-control-label').click();
    cy.get('[data-cy=uebernehmen').click();
    cy.intercept('POST', '/api/quellen/uebernehmen/uebernahme').as(
      'uebernehmeQuelle'
    );
    cy.get('[data-cy=uebernehmenDialogButton').click();
    cy.wait('@uebernehmeQuelle').its('response.statusCode').should('eq', 200);
    cy.get('[data-cy=quellenName]').should('have.value', 'verändert');
    cy.get('[data-cy=quelleNavBack]').click();

    // Übernahme Anlagenteil
    cy.visit(
      'stammdaten/jahr/2020/betriebsstaette/land/02/nummer/18/anlage/nummer/0118/anlagenteil/nummer/0118'
    );
    cy.wait(3000);
    cy.contains('button', 'Übernehmen').click();
    cy.wait(3000);
    cy.get('[data-cy=allgemeinAccordion]').click();
    cy.get('.clr-control-label').click();
    cy.get('#clr-form-control-13').check();
    cy.get('[data-cy=uebernehmen').click();
    cy.intercept('POST', '/api/anlagenteil/uebernehmen/uebernahme').as(
      'uebernehmeAnlagenteil'
    );
    cy.get('[data-cy=uebernehmenDialogButton').click();
    cy.wait('@uebernehmeAnlagenteil')
      .its('response.statusCode')
      .should('eq', 200);
    cy.get('[data-cy=anlageName]').should('have.value', 'verändert');
  });

  it('should delete Full Betriebsstaette', function () {
    // SETUP
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', '19');
    // Zwei Betriebsstätten mit Kindobjekten per API erstellen
    const betriebsstaetteUebernahme19: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr: '19',
      name: 'BetriebsstätteUebernahme19',
      zustaendigeBehoerde: this.behoerde,
    };

    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaetteUebernahme19,
    })
      .its('body')
      .as('betriebsstaetteUebernahme19');

    // Anlagen(teile) und Quellen hinzufügen
    cy.get('@betriebsstaetteUebernahme19')
      .its('id')
      .then((parentBetriebsstaetteId) => {
        const spbBetriebsstaette19: SpbBetriebsstaetteDto = {
          adresse: Adresse,
          berichtsjahr: J2020,
          betriebsstatus: betriebsstatus,
          land: '02',
          zustaendigeBehoerde: '10',
          betriebsstaetteNr: '19',
          name: 'verändert19',
          betriebsstaetteId: parentBetriebsstaetteId,
          ansprechpartner: [],
        };

        const anlageDto: AnlageDto = {
          anlageNr: '0119',
          name: 'BST19ANL0119',
          parentBetriebsstaetteId,
          vorschriften,
        };
        const quelleDto: QuelleDto = {
          quelleNr: '0119',
          name: 'BST19QLE0119',
          parentBetriebsstaetteId,
        };
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/quellen',
          body: quelleDto,
        });
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/anlagen',
          body: anlageDto,
        })
          .its('body')
          .as('anlage');

        cy.kcLogout();
        cy.kcLogin('betreiber2_test_user').as('tokens');

        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/spbbetriebsstaetten',
          body: spbBetriebsstaette19,
        });

        cy.kcLogout();
        cy.kcLogin('admin_hh_test_user').as('tokens');

        cy.get('@anlage')
          .its('id')
          .then((parentAnlageId) => {
            const quelleDto: QuelleDto = {
              quelleNr: '0119',
              name: 'BST19ANL0119QLE0119',
              parentAnlageId,
            };
            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/quellen',
              body: quelleDto,
            })
              .its('body')
              .as('quelle');
            const anlagenteilDto: AnlagenteilDto = {
              anlagenteilNr: '0119',
              name: 'BST19ANL0119ANT0119',
              parentAnlageId,
              parentBetriebsstaetteId,
            };
            const spbAnlage: SpbAnlageDto = {
              anlageNr: '0119',
              anlageId: parentAnlageId,
              parentBetriebsstaetteId,
              name: 'newName',
            };

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/anlagenteil',
              body: anlagenteilDto,
            })
              .its('body')
              .as('anlagenteil');

            cy.kcLogout();
            cy.kcLogin('betreiber2_test_user').as('tokens');

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/spbanlagen',
              body: spbAnlage,
            });
            cy.get('@quelle')
              .its('id')
              .then((quelleId) => {
                const spbQuelle: SpbQuelleDto = {
                  quelleNr: '0119',
                  quelleId,
                  name: 'verändert',
                };
                cy.authenticatedRequest({
                  method: 'POST',
                  url: '/api/spbquellen',
                  body: spbQuelle,
                });
              });

            cy.get('@anlagenteil')
              .its('id')
              .then((anlagenteilId) => {
                const spbAnlagenteil: SpbAnlagenteilDto = {
                  anlagenteilId,
                  parentAnlageId,
                  anlageNr: '0119',
                  name: 'verändert',
                };

                cy.authenticatedRequest({
                  method: 'POST',
                  url: '/api/spbanlagenteil',
                  body: spbAnlagenteil,
                });
              });
          });
      });

    // TEST

    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    cy.visit('/stammdaten/jahr/2020');
    // Prüfe EURegistry
    /* ==== Generated with Cypress Studio ==== */
    cy.get('[data-cy=euRegistryModule]').click();
    cy.get('[data-cy="19"]').click();

    // Lösche BST
    cy.get('.header-nav > [routerlink="/stammdaten"]').click();
    cy.get('[data-cy=betriebsstaetteRow]')
      .contains('[data-cy=betriebsstaetteRow]', 'BetriebsstätteUebernahme19')
      .find('input[type=checkbox]')
      .click({ force: true });

    cy.get('[data-cy=loeschen]').click();
    cy.intercept('POST', '/api/betriebsstaetten/loeschen').as('loescheBst');
    cy.get('[data-cy=loeschenDialogButton]').click();
    cy.wait('@loescheBst').its('response.statusCode').should('eq', 200);
    /* ==== End Cypress Studio ==== */
  });
});
