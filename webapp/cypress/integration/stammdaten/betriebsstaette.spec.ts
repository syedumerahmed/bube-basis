import { aBehoerde } from '../../fixtures/referenzen/behoerde';

describe('betriebsstätten', () => {
  const betriebsstaetteNr = 'Nummer-123';

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    // Betriebsstätte ggf. löschen, falls Nummer vergeben
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', betriebsstaetteNr);
    cy.getOrCreateBehoerde(aBehoerde());
  });

  it('should be able to create new betriebsstätte', () => {
    cy.visit('/stammdaten');
    cy.get('[data-cy=createBetriebsstaette]').click();
    cy.get('[data-cy=betriebsstaetteNr]').type(betriebsstaetteNr);
    cy.get('[data-cy=name]').type('Betriebsstätte');
    cy.get('[data-cy=betriebsstatus] select')
      .children()
      .eq(1)
      .then((e) =>
        cy.get('[data-cy=betriebsstatus] select').select(e.val().toString())
      );
    cy.get('[data-cy=gemeindekennziffer] input').type('Hamburg{enter}');

    // wechsel zu Adresse Tab, um weitere Pflichtfelder zu füllen
    cy.get('[data-cy=tabAdresse]').click();
    cy.get('[data-cy=plz]').type('12345');

    // save
    cy.get('[data-cy=saveBetriebsstaette]').click();
    cy.url().should('contain', `/nummer/${betriebsstaetteNr}`);

    // Anlage hinzufügen
    cy.get('[data-cy=createAnlage]').click();
    cy.get('[data-cy=anlageNr]').type('1');
    cy.get('[data-cy=anlageName]').type('Anlage');
    cy.get('[data-cy=saveAnlage]').click();

    cy.url().should('contain', `/nummer/${betriebsstaetteNr}/anlage/nummer/1`);
    cy.get('[data-cy=anlageNavBack]').click();
    cy.get('[data-cy=anlageRow]').should('have.length', 1);

    // Quelle und Anlagenteil hinzufügen
    cy.get('[data-cy=openAnlage1]').click();
    cy.get('[data-cy=anlageCreateQuelle]').click();
    cy.get('[data-cy=quelleNr]').type('1');
    cy.get('[data-cy=quellenName]').type('Quelle zur Anlage');
    cy.get('[data-cy=saveQuelle]').click();

    cy.url().should(
      'contain',
      `/nummer/${betriebsstaetteNr}/anlage/nummer/1/quelle/nummer/1`
    );
    // Gespeicherte Quelle verändern und noch einmal speichern
    cy.get('[data-cy=quellenName]')
      .clear()
      .type('Quelle zur Anlage aber verändert');

    cy.intercept('PUT', '/api/quellen').as('updateQuelle');
    cy.get('[data-cy=saveQuelle]').click();
    cy.wait('@updateQuelle').its('response.statusCode').should('eq', 200);

    cy.get('[data-cy=quellenName]').should(
      'have.value',
      'Quelle zur Anlage aber verändert'
    );
    cy.get('[data-cy=alertContainer] .alert-item').should('have.length', 0);

    cy.get('[data-cy=quelleNavBack]').click();
    cy.get('[data-cy=anlageQuelleRow]').should('have.length', 1);

    cy.get('[data-cy=createAnlagenteil]').click();

    // Cypress versucht bei anlageNr und anlageName noch auf die "alten" Elemente zu gehen, die
    // vorher in der Anlage-Component offen waren.
    // Der workaround funktioniert so erstmal ...
    cy.wait(5000);
    cy.get('[data-cy=anlageNr]').type('1');
    cy.get('[data-cy=anlageName]').type('Anlagenteil');
    cy.get('[data-cy=saveAnlagenteil]').click();

    cy.url().should(
      'contain',
      `/nummer/${betriebsstaetteNr}/anlage/nummer/1/anlagenteil/nummer/1`
    );

    cy.get('[data-cy=anlagenteilNavBack]').click();
    cy.get('[data-cy=anlagenteilRow]').should('have.length', 1);
  });
});
