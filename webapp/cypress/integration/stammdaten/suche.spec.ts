import { BetriebsstaetteDto, ReferenzDto } from '@api/stammdaten';
import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';
import { aBehoerde } from '../../fixtures/referenzen/behoerde';

describe('Stammdaten - Suche', () => {
  const betriebsstaetteNr = 'Nummer-123';

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    // Betriebsstätte ggf. löschen, falls Nummer vergeben
    cy.deleteBetriebsstaetteByJahrLandNummer(
      '2020',
      '02',
      betriebsstaetteNr
    ).then(() => {
      cy.authenticatedRequest({
        url: `/api/referenzen/behoerden`,
        method: 'POST',
        body: aBehoerde(),
        failOnStatusCode: false,
      });
    });
  });

  it('should search', () => {
    const betriebsstatus: ReferenzDto = {
      id: 10011,
      gueltigBis: 2099,
      gueltigVon: 2007,
      ktext: 'in Betrieb',
      land: '00',
      letzteAenderung: '2020-02-09T00:00:00Z',
      ltext: 'in Betrieb',
      referenzliste: 'RBETZ',
      schluessel: '01',
      sortier: '01',
    };

    cy.getOrCreateBehoerde(aBehoerde()).then((zustaendigeBehoerde) => {
      const betriebsstaette: BetriebsstaetteDto = {
        ...aBetriebsstaette(),
        betriebsstaetteNr,
        name: 'Betriebsstätte von Cypress',
        land: '02',
        betriebsstatus,
        zustaendigeBehoerde,
      };
      cy.authenticatedRequest({
        method: 'POST',
        url: '/api/betriebsstaetten',
        body: betriebsstaette,
        failOnStatusCode: false,
      });
    });

    cy.visit('/stammdaten');

    cy.get('[data-cy=searchToggle]').click();
    cy.get('[data-cy=searchBstNr]').type(
      betriebsstaetteNr.substring(betriebsstaetteNr.length - 3)
    );
    cy.intercept('POST', '/api/betriebsstaetten/list?*').as('search');
    cy.get('[data-cy=searchSubmit]').click();
    cy.wait('@search');

    cy.get('[data-cy=betriebsstaetteRow]').should('have.length', 0);

    cy.get('[data-cy=searchBstNr]').clear().type(betriebsstaetteNr);
    cy.get('[data-cy=searchSubmit]').click();
    cy.get('[data-cy=searchToggle]').should('contain.text', '1 aktiv');
    cy.wait('@search');

    cy.get('[data-cy=betriebsstaetteRow]').should('have.length', 1);

    cy.get(
      '[data-cy=searchInputBetriebsstatus] > .clr-combobox-wrapper > .clr-combobox-trigger'
    ).click();
    cy.get('[data-cy=betriebsstatusOption]').contains('außer Betrieb').click();
    cy.get('[data-cy=searchSubmit]').click();
    cy.get('[data-cy=searchToggle]').should('contain.text', '2 aktiv');
    cy.wait('@search');
    cy.get('[data-cy=betriebsstaetteRow]').should('have.length', 0);

    cy.get('[data-cy=searchInputBetriebsstatus]').find('input').type('Betr');
    cy.get('[data-cy=betriebsstatusOption]').contains('in Betrieb').click();
    cy.get('[data-cy=searchSubmit]').click();
    cy.wait('@search');

    cy.get('[data-cy=betriebsstaetteRow]').should('have.length', 1);
  });
});
