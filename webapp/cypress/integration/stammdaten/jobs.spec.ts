import { BetriebsstaetteDto } from '@api/stammdaten';
import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';
import { aBehoerde } from '../../fixtures/referenzen/behoerde';

describe('Stammdaten - Jobs', () => {
  const betriebsstaetteNr = 'Nummer-123';

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    // Betriebsstätte ggf. löschen, falls Nummer vergeben
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', betriebsstaetteNr);
    cy.getOrCreateBehoerde(aBehoerde()).as('behoerde');
  });

  it('should uebernehmen in anderes jahr', function () {
    // BST im Zieljahr löschen falls vorhanden
    cy.deleteBetriebsstaetteByJahrLandNummer('2019', '02', betriebsstaetteNr);

    const betriebsstaette: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr,
      name: 'Betriebsstätte zum Übernehmen',
      land: '02',
      zustaendigeBehoerde: this.behoerde,
    };

    cy.visit('/stammdaten');
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaette,
      failOnStatusCode: false,
    });

    // BST suchen und auswählen
    cy.get('[data-cy=searchToggle]').click();
    cy.get('[data-cy=searchBstNr]').clear().type(betriebsstaetteNr);
    cy.get('[data-cy=searchSubmit]').click();
    cy.get('[data-cy=alleMarkieren]').click();

    cy.get('[data-cy=aufgaben]').click();
    cy.get('[data-cy=inJahrKopieren]').click();
    cy.get('[data-cy=zieljahr]').select('2019');
    cy.get('[data-cy=uebernehmen]').click();

    cy.get('[data-cy=berichtsjahrBtn]').click();
    cy.get('[data-cy=berichtsjahrDropdown]').contains('2019').click();

    cy.get('[data-cy=betriebsstaetteRow]').should('have.length', 1);
  });
});
