import { aBehoerde } from '../../fixtures/referenzen/behoerde';
import {
  AnlageDto,
  AnlagenteilDto,
  BetriebsstaetteDto,
  QuelleDto,
} from '@api/stammdaten';
import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';

describe('Stammdaten - Navi-Baum', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');
    // Stammdaten Desktop ggf. leeren
    cy.authenticatedRequest({
      url: '/api/desktop/STAMMDATEN/2020',
      method: 'DELETE',
    });
    // Behörde für Hamburg abfragen
    cy.getOrCreateBehoerde({
      ...aBehoerde(),
      land: '02',
      gueltigVon: 2020,
      gueltigBis: 2020,
    }).as('behoerde');
  });

  it('should add betriebsstätten to navi-baum', function () {
    // SETUP
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', '01');
    cy.deleteBetriebsstaetteByJahrLandNummer('2020', '02', '02');
    // Zwei Betriebsstätten mit Kindobjekten per API erstellen
    const betriebsstaette01: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr: '01',
      name: 'Betriebsstätte01',
      zustaendigeBehoerde: this.behoerde,
    };
    const betriebsstaette02: BetriebsstaetteDto = {
      ...aBetriebsstaette(),
      betriebsstaetteNr: '02',
      name: 'Betriebsstätte02',
      zustaendigeBehoerde: this.behoerde,
    };
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaette01,
    })
      .its('body')
      .as('betriebsstaette01');
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/betriebsstaetten',
      body: betriebsstaette02,
    })
      .its('body')
      .as('betriebsstaette02');

    // Anlagen(teile) und Quellen hinzufügen
    cy.get('@betriebsstaette01')
      .its('id')
      .then((parentBetriebsstaetteId) => {
        const anlageDto: AnlageDto = {
          anlageNr: '0100',
          name: 'BST01ANL0100',
          parentBetriebsstaetteId,
        };
        const quelleDto: QuelleDto = {
          quelleNr: '0100',
          name: 'BST01QLE0100',
          parentBetriebsstaetteId,
        };
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/quellen',
          body: quelleDto,
        });
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/anlagen',
          body: anlageDto,
        })
          .its('body')
          .as('anlage');

        cy.get('@anlage')
          .its('id')
          .then((parentAnlageId) => {
            const quelleDto: QuelleDto = {
              quelleNr: '0100',
              name: 'BST01ANL0100QLE0100',
              parentAnlageId,
            };
            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/quellen',
              body: quelleDto,
            });
            const anlagenteilDto: AnlagenteilDto = {
              anlagenteilNr: '0100',
              name: 'BST01ANL0100ANT0100',
              parentAnlageId,
              parentBetriebsstaetteId,
            };

            cy.authenticatedRequest({
              method: 'POST',
              url: '/api/anlagenteil',
              body: anlagenteilDto,
            });
          });
      });

    // TEST
    cy.visit('/stammdaten/jahr/2020');

    // Betriebsstätten in Liste selektieren
    [betriebsstaette01, betriebsstaette02].forEach((betriebsstaette) => {
      cy.get('[data-cy=betriebsstaetteRow]')
        .contains('[data-cy=betriebsstaetteRow]', betriebsstaette.name)
        .find('input[type=checkbox]')
        // Das Datagrid macht komischen CSS Kram mit den Zeilen und Cypress
        // glaubt, dass die Zeilen nicht sichtbar wären
        .click({ force: true });
    });

    cy.get('[data-cy=addToNaviBaumBtn]').click();
    cy.get('[data-cy=naviBaumStamm]')
      .and('contain.text', betriebsstaette01.name)
      .and('contain.text', betriebsstaette02.name);

    cy.get('[data-cy=naviBaumStamm] [data-cy=expandAllBtn]').click();
    cy.get('[data-cy=naviBaumStamm] [data-cy=desktopNode]')
      .as('treeNodes')
      .should('have.length', 6);
    cy.get('@treeNodes').eq(0).should('contain.text', betriebsstaette01.name);
    cy.get('@treeNodes').eq(1).should('contain.text', 'BST01ANL0100');
    cy.get('@treeNodes').eq(2).should('contain.text', 'BST01ANL0100ANT0100');
    cy.get('@treeNodes').eq(3).should('contain.text', 'BST01ANL0100QLE0100');
    cy.get('@treeNodes').eq(4).should('contain.text', 'BST01QLE0100');
    cy.get('@treeNodes').eq(5).should('contain.text', betriebsstaette02.name);
  });
});
