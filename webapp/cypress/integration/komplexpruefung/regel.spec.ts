describe('regeln', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');
  });

  it('should be able to create new regel', () => {
    cy.visit('/komplexpruefung/regeln');
    cy.get('[data-cy=createRegel]').click();

    cy.url().should('contain', `/neu`);

    cy.get('[data-cy=beschreibung]').type('Beschreibung');
    cy.get('[data-cy=code]').type('Code');
    cy.get('[data-cy=fehlertext]').type('Fehlertext');
    cy.get('[data-cy=objekte]').within(() => {
      cy.get('input').type('BST{enter}');
    });

    // save
    cy.get('[data-cy=saveRegel]').click();
    cy.url().should('not.contain', `/neu`);
  });
});
