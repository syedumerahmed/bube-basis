describe('Load Application', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('gesadmin_test_user');
  });

  it('should load initial state', () => {
    cy.visit('/');
    // default redirect
    cy.url({ timeout: 10_000 }).should('contain', '/stammdaten/jahr/');
    cy.get('[data-cy=toolbar]').should('be.visible');
    cy.get('[data-cy=benutzerDropdown]').contains('gesadmin_test_user');
  });
});
