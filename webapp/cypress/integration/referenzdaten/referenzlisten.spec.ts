import { ReferenzDto } from '@api/stammdaten';

describe('referenzlisten', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');
  });

  it('should be able to call referenzlisten page', () => {
    cy.visit('/referenzdaten/referenzlisten');
    cy.get('[data-cy=referenzlisten-row]').should('have.length', 24);
  });

  it('should filter landeseinheitlich', () => {
    cy.visit('/referenzdaten/referenzlisten');
    cy.get('[data-cy=bund-land-col] .datagrid-filter-toggle').click({
      force: true,
    });
    cy.get('[data-cy=bund-land-filter]').contains('bundeseinheitlich').click();
    cy.get('[data-cy=referenzlisten-row]').should('have.length', 4);
    cy.get('[data-cy=referenzlisten-row]').each((e) =>
      cy.wrap(e).should('contain.text', 'länderspezifisch')
    );
  });

  it('should create landesreferenz', () => {
    const referenzliste = 'RFKN';
    const jahr = 2020;
    const land = '02';
    const schluessel = '1234';
    // Try to delete Referenz if it exists
    cy.authenticatedRequest<ReferenzDto>({
      url: `/api/referenzen/${referenzliste}/jahr/${jahr}/land/${land}/schluessel/${schluessel}`,
      failOnStatusCode: false,
    }).then((response) => {
      if (response.isOkStatusCode) {
        cy.authenticatedRequest({
          method: 'DELETE',
          url: `/api/referenzen/${response.body.id}`,
        });
      }
    });
    cy.visit('/referenzdaten/referenzlisten');
    cy.get('[data-cy=referenzlisten-row]')
      .contains('[data-cy=referenzlisten-row]', 'RFKN')
      .find('button')
      .click();
    cy.get('[data-cy=referenz-hinzufuegen-btn]').click();
    cy.get('[data-cy=referenz-edit-modal]').should('exist').and('be.visible');
    cy.get('[data-cy=schluessel-input]').type(schluessel);
    cy.get('[data-cy=ktext-input]').type('neue Funktion');
    cy.get('[data-cy=sortier-input]').type('1234');
    cy.get('[data-cy=gueltig-von-input]').clear().type(String(jahr));
    cy.get('[data-cy=gueltig-bis-input]').clear().type(String(jahr));
    cy.get('[data-cy=referenz-save-btn]').click();
    cy.get('[data-cy=referenz-edit-modal]').should('not.exist');
    cy.get('[data-cy=referenz-row]')
      .should('contain.text', '1234')
      .and('contain.text', 'neue Funktion');
  });
});
