import { BehoerdeListItemDto } from '@api/stammdaten';

describe('behoerden', () => {
  let jahr: number;
  let land: string;
  let schluessel: string;

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    jahr = 2020;
    land = '02';
    schluessel = 'cypress';
  });

  it('should be able to import behoerde', () => {
    deleteBehoerdenIfExist(jahr, land, [schluessel]);
    // Behörde Datei ggf. löschen
    cy.authenticatedRequest({
      method: 'DELETE',
      url: `/api/referenzen/behoerden/import/delete`,
      failOnStatusCode: false,
    });
    cy.visit('/referenzdaten/behoerden');
    cy.get('[data-cy=aufgabenDropdown]').click();
    cy.get('[data-cy=importModalOpen]').click();

    // File Upload
    cy.get('[data-cy="fileUploadInput"]').attachFile('referenzen/behoerde.xml');

    //Import Starten
    cy.intercept('/api/referenzen/behoerden/import').as('importBehoerde');
    cy.get('[data-cy=importStartenBtn]').click();
    cy.wait('@importBehoerde').its('response.statusCode').should('eq', 200);

    //Prüfen, ob der Job ohne Fehler gelaufen ist.
    cy.get('[data-cy=alertContainer]')
      .get('.alert-success')
      .should('be.visible');
  });

  it('should create and copy behoerde', () => {
    const schluesselKopie = schluessel + '2';
    deleteBehoerdenIfExist(jahr, land, [schluessel, schluesselKopie]);

    cy.visit('/referenzdaten/behoerden');
    cy.get('[data-cy=create-behoerde-btn]').click();

    // Allgemein
    const kreis = '314';
    cy.get('[data-cy=kreis-input]').type(kreis);
    cy.get('[data-cy=schluessel-input]').type(schluessel);
    const bezeichnung = 'Bezeichnung';
    cy.get('[data-cy=bezeichnung-input]').type(bezeichnung);
    const bezeichnung2 = 'Bezeichnung2';
    cy.get('[data-cy=bezeichnung2-input]').type(bezeichnung2);
    cy.get('[data-cy=zustaendigkeiten-combobox] input').type(
      '{downArrow}{enter}{downArrow}{enter}{esc}'
    );
    cy.get('[data-cy=gueltig-von-input]').clear().type(String(jahr));
    cy.get('[data-cy=gueltig-bis-input]').clear().type(String(jahr));

    // Adresse
    cy.get('[data-cy=adresse-tab]').click();
    const strasse = 'Strasse';
    cy.get('[data-cy=strasse-input]').type(strasse);
    const hausNr = 'Hausnummer';
    cy.get('[data-cy=haus-nr-input]').type(hausNr);
    const plz = 'PLZ';
    cy.get('[data-cy=plz]').type(plz);
    const ort = 'Ort';
    cy.get('[data-cy=ort-input]').type(ort);
    const ortsteil = 'Ortsteil';
    cy.get('[data-cy=ortsteil-input]').type(ortsteil);
    cy.get('[data-cy=land-combobox]').click().type('De{enter}{esc}');
    const vertraulichkeitsgrund = 'kein Schutzgrund';
    cy.get('[data-cy=vertraulichkeit-select] select').select(
      vertraulichkeitsgrund
    );
    const postfach = 'Postfach';
    cy.get('[data-cy=postfach-input]').type(postfach);
    const postfachPlz = 'PostfachPlz';
    cy.get('[data-cy=postfach-plz-input]').type(postfachPlz);
    const adressNotiz = 'Notiz';
    cy.get('[data-cy=adresse-notiz-input]').type(adressNotiz);

    // Kommunikation
    cy.get('[data-cy=kommunikation-tab]').click();
    cy.get('[data-cy=add-kommunikation-btn]').click();
    const kommunikationstyp = 'Fax';
    cy.get('[data-cy=typ-select] select').select(kommunikationstyp);
    const verbindung = '+49123456789';
    cy.get('[data-cy=verbindung-input]').type(verbindung);
    cy.get('[data-cy=uebernehmen-btn]').click();

    cy.intercept('POST', '/api/referenzen/behoerden').as('createBehoerde');
    cy.get('[data-cy=save-btn]').click();
    cy.wait('@createBehoerde').its('response.statusCode').should('eq', 200);
    cy.get('[data-cy=back-to-liste-btn]').click();

    // Behörde kopieren
    cy.get('[data-cy=behoerde-row]')
      .contains('[data-cy=behoerde-row]', schluessel)
      .find('[data-cy=copy-btn]')
      .click();

    // Kopie überprüfen
    // Allgemein
    cy.get('[data-cy=kreis-input]').should('have.value', kreis);
    cy.get('[data-cy=schluessel-input]').should('be.empty');
    cy.get('[data-cy=bezeichnung-input]')
      .should('contain.value', bezeichnung)
      .and('contain.value', 'Kopie');
    cy.get('[data-cy=bezeichnung2-input]').should('have.value', bezeichnung2);
    cy.get(
      '[data-cy=zustaendigkeiten-combobox] .clr-combobox-pill-content'
    ).should('have.length', 2);
    cy.get('[data-cy=gueltig-von-input]').should('have.value', String(jahr));
    cy.get('[data-cy=gueltig-bis-input]').should('have.value', String(jahr));

    // Adresse
    cy.get('[data-cy=adresse-tab]').click();
    cy.get('[data-cy=strasse-input]').should('have.value', strasse);
    cy.get('[data-cy=haus-nr-input]').should('have.value', hausNr);
    cy.get('[data-cy=plz]').should('have.value', plz);
    cy.get('[data-cy=ort-input]').should('have.value', ort);
    cy.get('[data-cy=ortsteil-input]').should('have.value', ortsteil);
    cy.get('[data-cy=land-combobox] input').should('have.value', 'Deutschland');
    cy.get('[data-cy=vertraulichkeit-select] select').should(
      'contain.text',
      vertraulichkeitsgrund
    );
    cy.get('[data-cy=postfach-input]').should('have.value', postfach);
    cy.get('[data-cy=postfach-plz-input]').should('have.value', postfachPlz);
    cy.get('[data-cy=adresse-notiz-input]').should('have.value', adressNotiz);

    // Kommunikation
    cy.get('[data-cy=kommunikation-tab]').click();
    cy.get('[data-cy=kommunikation-row]')
      .should('contain.text', kommunikationstyp)
      .and('contain.text', verbindung);

    cy.get('[data-cy=allgemein-tab]').click();
    cy.get('[data-cy=schluessel-input]').type(schluesselKopie);

    // Kopie speichern
    cy.get('[data-cy=save-btn]').click();
    cy.wait('@createBehoerde').its('response.statusCode').should('eq', 200);
    cy.get('[data-cy=back-to-liste-btn]').click();

    // Original + Kopie in Liste
    cy.get('[data-cy=schluessel-col] .datagrid-filter-toggle').click();
    cy.get('.datagrid-filter input').type(schluessel + '{esc}');
    cy.get('[data-cy=behoerde-row]').should('have.length', 2);
  });

  function deleteBehoerdenIfExist(
    jahr: number,
    land: string,
    schluesselIn: string[]
  ) {
    cy.authenticatedRequest<Array<BehoerdeListItemDto>>({
      url: `/api/referenzen/behoerden/jahr/${jahr}?landNr=${land}`,
    }).then((response) => {
      const behoerdenIds = response.body
        .filter((b) => schluesselIn.includes(b.schluessel))
        .map((b) => b.id);
      if (behoerdenIds.length) {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/referenzen/behoerden/loeschen',
          body: behoerdenIds,
        });
      }
    });
  }
});
