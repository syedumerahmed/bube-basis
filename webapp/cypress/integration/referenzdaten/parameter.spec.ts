describe('parameter', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user');
  });
  it('should be able to call parameter page', () => {
    cy.visit('/referenzdaten/parameter');
    cy.get('[data-cy=parameter-row]').should('have.length.at.least', 18);
  });
});
