import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';
import { aAnlageMit17BV } from '../../fixtures/stammdaten/anlage';
import { aBehoerde } from '../../fixtures/referenzen/behoerde';
import { AnlageDto, BetriebsstaetteDto } from '@api/stammdaten';

describe('euregistry', () => {
  const betriebsstaette: BetriebsstaetteDto = {
    ...aBetriebsstaette(),
    betriebsstaetteNr: 'cypressEUReg',
    name: 'CypressEUReg',
    localId: 'CY_BST_EUREG',
  };

  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user').as('tokens');

    cy.getOrCreateBehoerde(aBehoerde())
      .then((zustaendigeBehoerde) =>
        cy.authenticatedRequest<BetriebsstaetteDto>({
          method: 'POST',
          url: '/api/betriebsstaetten',
          body: { ...betriebsstaette, zustaendigeBehoerde },
          failOnStatusCode: false,
        })
      )
      .then((response) => {
        if (response.isOkStatusCode) {
          const anlage: AnlageDto = aAnlageMit17BV(response.body.id);
          // Anlage zur Betriebsstätte anlegen
          cy.authenticatedRequest({
            method: 'POST',
            url: '/api/anlagen',
            body: anlage,
            failOnStatusCode: false,
          });
        }
      });
  });

  it('should use berichtsjahr from stammdaten', () => {
    cy.visit('/');
    cy.get('[data-cy=berichtsjahrBtn]').click();
    cy.get('[data-cy=berichtsjahrDropdown]').contains('2017').click();
    cy.get('[data-cy=euRegistryModule]').click();
    cy.url().should('contain', '/eureg/berichtsdaten/jahr/2017');
    cy.get('[data-cy=berichtsjahrBtn]').should('contain.text', '2017');
  });

  it('should save betriebsstaettenbericht', () => {
    cy.visit('/eureg/jahr/2020');
    cy.get('[data-cy=cypressEUReg]').click();
    cy.url().should('contain', `betriebsstaette/land/02/nummer/cypressEUReg`);

    cy.get('[data-cy=akz]').clear();
    cy.get('[data-cy=akz]').type('AKZ');

    cy.intercept('PUT', '/api/eureg').as('updateBetriebsstaettenBericht');
    cy.get('[data-cy=saveBericht]').click();
    cy.wait('@updateBetriebsstaettenBericht')
      .its('response.statusCode')
      .should('eq', 200);
  });

  it('should be able to search', () => {
    cy.visit('/eureg/jahr/2020');
    cy.get('[data-cy=searchToggle]').click();
    const partialBst = betriebsstaette.betriebsstaetteNr.substring(
      betriebsstaette.betriebsstaetteNr.length - 3
    );
    cy.get('[data-cy=searchBstNr]').type(partialBst);
    cy.intercept('POST', '/api/eureg/list?*').as('search');
    cy.get('[data-cy=searchSubmit]').click();
    cy.wait('@search');

    cy.get('[data-cy=bstBerichtRow]').should('have.length', 0);

    cy.get('[data-cy=searchBstNr]')
      .clear()
      .type('*' + partialBst);
    cy.get('[data-cy=searchSubmit]').click();
    cy.wait('@search');

    cy.get('[data-cy=bstBerichtRow]')
      .should('have.length', 1)
      .and('contain.text', betriebsstaette.name);

    cy.get('.pagination-description').should(
      'contain.text',
      '1 - 1 von 1 Berichten'
    );
  });

  it('should show EuRegBST in NaviBaum', () => {
    cy.visit('/eureg/jahr/2020');

    // Betriebsstätten in Liste selektieren
    cy.get('[data-cy=bstBerichtRow]')
      .contains('[data-cy=bstBerichtRow]', betriebsstaette.name)
      .find('input[type=checkbox]')
      // Das Datagrid macht komischen CSS Kram mit den Zeilen und Cypress
      // glaubt, dass die Zeilen nicht sichtbar wären
      .click({ force: true });

    cy.get('[data-cy=inDesktopSpeichernButton]').click();

    cy.get('[data-cy=naviBaumEureg]').should(
      'contain.text',
      betriebsstaette.name
    );

    cy.intercept('DELETE', '/api/desktop/EUREG/item/*').as('deleteDesktopItem');

    cy.get('[data-cy=naviBaumEureg]')
      .contains('[data-cy=desktopNode]', `EuRegBST: ${betriebsstaette.name}`)
      .find('[data-cy=deleteDesktopItemBtn]')
      .click();

    cy.wait('@deleteDesktopItem').its('response.statusCode').should('eq', 200);

    cy.get('[data-cy=naviBaumEureg]').should(
      'not.contain.text',
      betriebsstaette.name
    );
  });

  it('should do export', () => {
    cy.intercept('POST', '/api/eureg/export').as('export');

    cy.visit('/eureg/jahr/2020');

    cy.get('[data-cy=alleMarkierenButton]').click();
    cy.get('[data-cy=aufgaben]').click();
    cy.get('[data-cy=xmlExportBtn]').click();

    cy.get('app-export-berichtsdaten-modal').should('be.visible');
    cy.contains('Exportieren').click();

    cy.wait('@export').its('response.statusCode').should('eq', 200);
    cy.get('[data-cy=alertContainer]')
      .get('.alert-success')
      .should('be.visible');

    // TODO verify the file is saved
    // const downloadsFolder = Cypress.config('downloadsFolder');
  });

  it('should do import', () => {
    // Import Datei ggf. löschen
    cy.authenticatedRequest({
      method: 'DELETE',
      url: `/api/euregbetriebsstaetten/import/delete`,
      failOnStatusCode: false,
    });
    cy.intercept('/api/euregbetriebsstaetten/import/2020').as('import');

    cy.visit('/eureg/jahr/2020');

    cy.get('[data-cy=aufgaben]').click();
    cy.get('[data-cy=xmlImportBtn]').click();

    // File Upload
    cy.get('[data-cy="fileUploadInput"]').attachFile('eureg/upload.xml');

    //Import Starten
    cy.get('[data-cy=importStartenBtn]').click();
    cy.wait('@import').its('response.statusCode').should('eq', 200);

    //Prüfen, ob der Job ohne Fehler gelaufen ist.
    cy.get('[data-cy=alertContainer]')
      .get('.alert-success')
      .should('be.visible');
  });
});
