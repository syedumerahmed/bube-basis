import RequestOptions = Cypress.RequestOptions;

describe('Security', () => {
  beforeEach(() => {
    cy.kcLogout();
  });

  it('should authorize api requests with keycloak token', () => {
    // no Auth Header
    const request: Partial<RequestOptions> = {
      url: '/api/benutzer?benutzername=gesadmin_test_user',
      failOnStatusCode: false,
    };
    cy.request(request).its('status').should('eq', 401);

    cy.kcLogin('gesadmin_test_user').then((tokens) => {
      request.auth = { bearer: tokens.access_token };
      cy.request(request).its('isOkStatusCode').should('eq', true);
    });
  });
});
