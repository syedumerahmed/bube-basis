import { aBetriebsstaette } from '../../fixtures/stammdaten/betriebsstaette';
import { aBehoerde } from '../../fixtures/referenzen/behoerde';

describe('Benutzerverwaltung', () => {
  beforeEach(() => {
    cy.kcLogout();
    cy.kcLogin('admin_hh_test_user')
      .as('tokens')
      .then(() => {
        // Benutzer ggf. löschen, falls Name vergeben
        cy.authenticatedRequest({
          method: 'DELETE',
          url: `/api/benutzer?benutzername=cypress`,
          failOnStatusCode: false,
        });
      });
  });

  it('should be able to create new behoerdenbenutzer with email', () => {
    cy.intercept('POST', '/api/benutzer/listbehoerde').as('listbehoerde');
    cy.visit('/benutzerverwaltung/behoerden');
    cy.wait('@listbehoerde');
    cy.get('[data-cy=behoerdenbenutzerRow]').should('have.length.at.least', 1);
    cy.get('[data-cy=createBehoerdenbenutzerBtn]').click();
    cy.get('.card-header')
      .as('title')
      .should('contain.text', 'Behördenbenutzer hinzufügen');
    cy.get('[data-cy=benutzername]').type('cypress');
    cy.get('[data-cy=email]').type('cypress@bube.aws.wps.de');
    cy.get('[data-cy=saveUserBtn]').click();

    cy.get('@title').should('contain.text', 'Behördenbenutzer cypress');
    cy.url().should('contain', '/benutzer/cypress');
  });

  it('should be able to create new betreiberbenutzer without email', function () {
    cy.intercept('POST', '/api/benutzer/listbetreiber').as('listbetreiber');

    cy.visit('/benutzerverwaltung/betreiber');

    // Es muss eine Behörde für das Land und den Schlüssel in der BST geben
    cy.getOrCreateBehoerde(aBehoerde())
      .then((behoerde) => {
        // Betriebsstaette mit Nummer 'cypress' muss existieren
        return cy.authenticatedRequest({
          method: 'POST',
          url: '/api/betriebsstaetten',
          body: {
            ...aBetriebsstaette(),
            betriebsstaetteNr: 'cypress',
            zustaendigeBehoerde: behoerde,
          },
          failOnStatusCode: false,
        });
      })
      .then(() => {
        cy.wait('@listbetreiber');
        cy.get('[data-cy=createBetreiberbenutzerBtn]').click();
        cy.get('.card-header')
          .as('title')
          .should('contain.text', 'Betreiberbenutzer hinzufügen');
        cy.get('[data-cy=benutzername]').type('cypress');
        cy.get('[data-cy=betriebsstaetten]').type('cypress{enter}');
        cy.get('[data-cy=saveUserBtn]').click();

        cy.get('[data-cy=passwordModal]')
          .as('modal')
          .should('be.visible')
          .get('[data-cy=tempPassword]')
          .should((domElement) => {
            expect(domElement.text().trim()).to.have.length(12);
          });
        cy.get('[data-cy=tempPasswordCheckbox]').click();
        cy.get('[data-cy=tempPasswordAck]').click();

        cy.url().should('contain', '/benutzer/cypress');
        cy.get('@title').should('contain.text', 'Betreiberbenutzer cypress');
      });
  });

  it('should have access to own profile ignoring role', () => {
    cy.kcLogout();
    cy.kcLogin('land_loeschen').as('tokens');
    cy.visit('/');
    cy.get('[data-cy=benutzerDropdown]').click();
    cy.get('[data-cy=openProfileBtn]').click();
    cy.get('[data-cy=benutzername]').should('have.value', 'land_loeschen');
  });
});
