import { AnlageDto } from '@api/stammdaten';

export function aAnlageMit17BV(parentBetriebsstaetteId: number): AnlageDto {
  return {
    anlageNr: 'cypressEUReg',
    name: '0100',
    localId: 'CY_ANLAGE_EUREG',
    parentBetriebsstaetteId: parentBetriebsstaetteId,
    vorschriften: [
      {
        art: {
          id: 10164,
          land: '00',
          referenzliste: 'RVORS', // Hier kann nicht die Enum-Konstante verwendet werden. Dann macht Cypress Probleme
          schluessel: '17BV',
          sortier: '04',
          ktext: '17. BImSchV',
          gueltigVon: 2007,
          gueltigBis: 2099,
        },
      },
    ],
  };
}
