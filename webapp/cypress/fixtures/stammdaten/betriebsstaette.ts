import { BetriebsstaetteDto } from '@api/stammdaten';

export function aBetriebsstaette(): BetriebsstaetteDto {
  return {
    betriebsstaetteNr: 'cypress',
    name: 'Cypress',
    land: '02',
    adresse: {
      plz: '22085',
      ort: 'Hamburg',
    },
    berichtsjahr: {
      schluessel: '2020',
      referenzliste: 'RJAHR',
      land: '00',
      id: 10134,
      ktext: '2020',
      ltext: '2020 laaaang',
      sortier: '14',
      gueltigVon: 2007,
      gueltigBis: 2099,
    },
    betriebsstatus: {
      id: 10011,
      gueltigBis: 2099,
      gueltigVon: 2007,
      ktext: 'in Betrieb',
      land: '00',
      letzteAenderung: '2020-02-09T00:00:00Z',
      ltext: 'in Betrieb',
      referenzliste: 'RBETZ',
      schluessel: '01',
      sortier: '01',
    },
    gemeindekennziffer: {
      schluessel: '2020',
      referenzliste: 'RGMD',
      land: '00',
      id: 10108,
      ktext: '2020',
      ltext: '2020 laaaang',
      sortier: '14',
      gueltigVon: 2007,
      gueltigBis: 2099,
    },
    zustaendigeBehoerde: null,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };
}
