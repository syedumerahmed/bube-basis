import { BehoerdeDto } from '@api/referenzdaten';

export function aBehoerde(): BehoerdeDto {
  return {
    land: '02',
    bezeichnung: 'bezeichnung',
    gueltigBis: 2099,
    gueltigVon: 2007,
    schluessel: '10',
    adresse: { ort: 'ort', plz: '12345' },
  };
}
