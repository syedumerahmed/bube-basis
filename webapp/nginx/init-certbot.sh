#!/bin/bash
# Initialisiert Certbot in diesem Container
if [ "$DOMAIN_NAME" != "localhost" ]; then
    certbot --nginx --agree-tos --email bube-dev@wps.de -d "${DOMAIN_NAME}" -n
fi
