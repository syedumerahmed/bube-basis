<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>BUBE-Regel-API</title>
  <meta name="author" content="WPS - Workplace Solutions GmbH">
  <meta name="publisher" content="WPS - Workplace Solutions GmbH">
  <meta name="copyright" content="WPS - Workplace Solutions GmbH">
  <meta name="description" content="Dokumentation der BUBE-Regel-API">
  <meta name="keywords" content="Regeln, API, Dokumentation">
  <meta name="page-topic" content="Umwelt">
  <meta name="robots" content="noindex, nofollow">
  <meta http-equiv="content-language" content="de">
  <link rel="stylesheet" type="text/css" href="regel-api.css">
</head>
<body>

<h1>BUBE-Regel-API</h1>

<p>
  Bei der BUBE-Komplexprüfung werden zahlreiche <em>Regeln</em> auf ausgewählte Unterobjekte der betreffenden
  Betriebsstätten angewendet; Verstöße gegen diese Regeln werden protokolliert.
</p>
<p>
  Die Syntax der Regeln basiert auf der
  <a href="https://docs.spring.io/spring-framework/docs/5.3.8/reference/html/core.html#expressions">Spring Expression
  Language (SpEL)</a>, mit der sich Ausdrücke formulieren lassen, die - je nach Belegung der enthaltenen Variablen -
  entweder zu "wahr" oder "falsch" ausgewertet werden können.
</p>
<p class="hinweis">
  Grundsätzlich sind Regeln so zu formulieren, dass der Wert "wahr" dem Zustand "gültig" entspricht und der Wert
  "falsch" einem Regelverstoß.
</p>
<p>
  Gültige SpEL-Ausdrücke sind nach gewissen Regeln aus <em>Literalen</em>, <em>Operatoren</em> und
  <em>Objekt-Ausdrücken</em> aufgebaut.
</p>

<h2 id="literale">Literale</h2>
<p>
  Literale sind Konstanten, die in einem einzigen, atomaren Ausdruck spezifiziert werden. Dabei werden
  <em>String-Literale</em> stets in doppelten Anführungszeichen notiert, numerische Literale als Zahlenwerte ohne
  Anführungszeichen.
</p>
<p class="hinweis">
  Bei Fließkomma-Literalen (also solchen vom Typ <a href="base-types.html#Double">Double</a>) ist als
  Dezimaltrennzeichen der Punkt zu verwenden (also nicht wie im Deutschen üblich das Dezimalkomma).
</p>
<p>
  Neben Strings und numerischen Literalen existieren die <em>booleschen Literale</em> <code>true</code> (wahr) und
  <code>false</code> (falsch).
</p>
<p>
  Ein spezielles Literal ist der Ausdruck <code>null</code>, der ein nicht vorhandenes Objekt oder ein nicht gesetztes
  Attribut eines Objekts bezeichnet.
</p>

<h2 id="operatoren">Operatoren</h2>
<p>
  SpEL unterstützt zahlreiche <a
  href="https://docs.spring.io/spring-framework/docs/5.3.8/reference/html/core.html#expressions-operators">Operatoren</a>,
  die im Wesentlichen aus den Kategorien <em>Vergleichs-</em>, <em>logische</em> und <em>mathematische Operatoren</em>
  bestehen.
</p>
<h3>Vergleichsoperatoren</h3>
<p>
  <a href="https://docs.spring.io/spring-framework/docs/5.3.8/reference/html/core.html#expressions-operators-relational">Vergleichsoperatoren</a>
  überprüfen die verknüpften Terme auf Gleichheit, Ungleichheit oder Größe.
</p>
<p class="hinweis">
  Werden Terme auf Gleichheit getestet, ist der Gleichheitsoperator <code>==</code> zu verwenden, d.h. mit doppeltem
  Gleichheitszeichen. Der Ungleichheitsoperator <code>!=</code> besteht aus einem Ausrufezeichen und einem einfachen
  Gleichheitszeichen.
</p>
<h3>Logische Operatoren</h3>
<p>
  Durch die logischen Operatoren <code>&&</code> ("und"), <code>||</code> ("oder") und <code>!</code> ("nicht") werden
  Ausdrücke mit Booleschen Werten verknüpft. Alternativ können die Schlüsselwörter <code>and</code>, <code>or</code>
  bzw. <code>not</code> verwendet werden.
</p>
<p>
  Der <em>ternäre Operator</em>, der drei Terme mit den Zeichen <code>?</code> und <code>:</code> verknüpft, drückt
  eine "wenn-dann-ansonsten"-Beziehung aus: Der erste Term muss ein boolescher Ausdruck sein; wird dieser zu
  <code>true</code> ausgewertet, ist der Wert des gesamten Ausdrucks derjenige des zweiten Teilausdrucks, anderenfalls
  ist es der des dritten Teilausdrucks.
</p>
<p class="beispiel">
  <code>#OBJ.betriebsstatus.schluessel == "01" ? #OBJ.betriebsstatusSeit : #OBJ.inbetriebnahme</code> (ist der
  Betriebsstatus "in Betrieb", wird das Datum "betriebsstatusSeit" zurückgegeben, ansonsten das Datum "inbetriebnahme".)
</p>
<h3>Mathematische Operatoren</h3>
<p>
  SpEL unterstützt die mathematischen Grundrechenarten mit den Operatoren <code>+</code>, <code>-</code>,
  <code>*</code> und <code>/</code>, außerdem den <em>Modulo-Operator</em> <code>%</code> (für Ganzzahlarithmetik)
  und den <em>Potenzierungsoperator</em> <code>^</code>.
</p>

<h2 id="ausdruecke">Objekt-Ausdrücke</h2>
<p>
  Die Typen sämtlicher Objekte sind entweder <em>Basistypen</em> (z.B. <a href="base-types.html#String">String</a> oder
  <a href="base-types.html#Long">Long</a>) oder <em>zusammengesetzte Objekte</em>, die aus mehreren <em>Attributen</em>
  (<em>"Unterobjekten"</em>) bestehen und/oder über <em>Methoden</em> (Funktionsaufrufe) verfügen. Zur Referenzierung
  eines Unterobjekts stehen mehrere <em>Navigationsoperatoren</em> zur Verfügung.
</p>

<h3>Reguläre Navigation</h3>
<p>
  Ein Unterobjekt eines Objekts wird durch <code>.</code> (einen Punkt), gefolgt vom Namen des Attributs referenziert.
  Durch mehrfache Anwendung des Punkt-Operators sind auch verschachtelte Navigationen möglich.
</p>
<p class="beispiel">
  <code>#OBJ.geoPunkt.ost</code> referenziert den ost-Wert des GeoPunkts eines Objekts.
</p>
<p>
  Methodenaufrufe werden ebenfalls durch den Punkt referenziert und durch eine Liste der übergebenen Parameter in
  runden Klammern gekennzeichnet.
</p>
<p class="beispiel">
  <code>#OBJ.geoPunkt.isInAgs("02000000")</code> ruft die Methode <code>isInAgs</code> der Klasse <code>GeoPunkt</code>
  mit dem String-Parameter-Wert <code>"02000000"</code> auf.
</p>
<h3>"Sichere Navigation"</h3>
<p>
  In den obigen Beispielen tritt ein Fehler auf, falls die verwendete Variable <code>#OBJ</code> ein Objekt referenziert,
  für welches der GeoPunkt nicht gesetzt (also <code>null</code>) ist. In diesem Fall führt z.B. die Bestimmung des Wertes
  des Ausdrucks<code>#OBJ.geoPunkt.ost</code> zu einem Fehler. Aus diesem Grund kann - und sollte, speziell bei
  geschachtelter Navigation - statt des Punkt-Operators der Operator <code>?.</code> für die "sichere Navigation"
  genutzt werden. Dieser wertet im Falle eines nicht gesetzten "Zwischenobjekts" den Gesamtausdruck zu einem
  <em>Default-Wert</em> aus, nämlich zu
</p>
<ul>
  <li><code>false</code> für boolesche Ausdrücke,</li>
  <li><code>0</code> für numerische Ausdrücke und</li>
  <li><code>null</code> für alle anderen Ausdrücke.</li>
</ul>
<p class="beispiel">
  <code>#OBJ.geoPunkt?.ost</code> ergibt den ost-Wert des GeoPunkts eines Objekts bzw. <code>0</code>, falls dieser
  GeoPunkt nicht gesetzt ist.
</p>
<h3>Listen</h3>
<p>
  Mehrere Objekte der Komplexprüfung enthalten Unterobjekte, die <em>Listen</em> von Objekten gemeinsamen Typs
  darstellen, z.B. ist <code>anlage.vorschriften</code> eine Liste von Objekten vom Typ <code>Vorschrift</code>.
</p>
<p>
  Um auf einzelne Elemente der Liste zuzugreifen, wird der <em>Klammeroperator</em> verwendet, bei dem in eckigen
  Klammern der Index des gesuchten Elements aus der Liste angegeben wird.
</p>
<p class="hinweis">
  Die Indizierung von Listen startet grundsätzlich bei <code>0</code> (nicht bei <code>1</code>).
</p>
<p class="beispiel">
  <code>#OBJ.vorschriften[1]</code> referenziert die zweite Vorschrift eines Objekts.
</p>
<h4>Filterung von Listen</h4>
<p>
  Listen lassen sich mit Hilfe des <em>Filterungsoperators</em> <code>.?[...]</code> auf eine Teilliste von Elementen
  reduzieren, die einer bestimmten Bedingung genügt; die zu testende Bedingung wird dabei jeweils in den eckigen
  Klammern angegeben.
</p>
<p class="beispiel">
  <code>#OBJ.anlagen.?[betriebsstatus.ktext == "in Betrieb"]</code> gibt die Liste aller Anlagen zurück, die den
  Betriebsstatus "in Betrieb" haben.
</p>
<h4>Projektion von Listen</h4>
<p>
  Listen können auf andere Listen abgebildet werden, indem jedes Element anhand einer Vorschrift einzeln auf ein neues
  Objekt abgebildet wird; die Liste dieser neuen Objekte wird <em>Projektion</em> der Ausgangsliste genannt.
</p>
<p>
  Hierfür steht der <em>Projektionsoperator</em> <code>.![...]</code> zur Verfügung; ähnlich wie beim Filterungsoperator
  enthalten die eckigen Klammern dabei die Abbildungsvorschrift.
</p>
<p class="beispiel">
  <code>#OBJ.anlagen.![betriebsstatus]</code> gibt die Liste der Betriebsstatus aller Anlagen zurück.
</p>
<p class="beispiel">
  Die Liste aller Anlagen mit dem Betriebsstatus "in Betrieb" aus dem Beispiel zur Listenfilterung kann somit alternativ
  auch durch<br/>
  <code>#OBJ.anlagen.![betriebsstatus].?[ktext == "in Betrieb"]</code> ausgedrückt werden.
</p>
<h4>Listen-Methoden</h4>
<p>
  Listen unterstützen eine Reihe nützlicher <em>Methoden</em> (Funktionen), die in der
  <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/List.html#method.summary">Dokumentation
    des Java-Interfaces <code>List</code></a> detailliert beschrieben sind. Die wichtigsten davon sind
</p>
<ul>
  <li><code>List.isEmpty()</code> prüft, ob die Liste leer ist oder nicht,</li>
  <li><code>List.size()</code> gibt die Größe der Liste zurück,</li>
  <li><code>List.contains(...)</code> prüft, ob die Liste ein bestimmtes Element enthält.</li>
</ul>
<p>
  Insbesondere die ersten beiden genannten Methoden lassen sich einsetzen, um zu prüfen, ob eine Bedingung für alle
  Unterobjekte einer Liste gilt (was sich auf anderem Weg leider nicht ausdrücken lässt), indem die Negation der
  Bedingung zur Filterung verwendet wird und anschließend sichergestellt wird, dass die Ergebnisliste leer ist.
</p>
<p class="beispiel">
  Um zu prüfen, dass alle Anlagen einer Betriebsstätte den Status "außer Betrieb" oder "dauerhaft stillgelegt" haben,
  kann die Bedingung<br/>
  <code>#OBJ.anlagen.![betriebsstatus].?[schluessel != "02" && schluessel != "03"].isEmpty()</code> verwendet werden.
</p>
<p>
  In einer Regel verwendete Listen lassen sich fest definieren durch Angabe der Listenelemente in geschweiften Klammern.
  Dies ist insbesondere im Zusammenhang mit der <code>contains()</code>-Methode hilfreich.
</p>
<p class="beispiel">
  Dieselbe Bedingung wie im vorigen Beispiel lässt sich mit dem Code<br/>
  <code>#OBJ.anlagen.![betriebsstatus].?[ ! &#123;"02", "03"}.contains(schluessel)].isEmpty()</code> formulieren.
</p>
<h3>Variablen</h3>
<p>
  <em>Variablen</em> sind Ausdrücke, die in Kontext einer Regelüberprüfung mit unterschiedlichen Objekten belegt
  werden. Sie beginnen stets mit einem <em>#</em> (Hash-Zeichen), gefolgt vom Variablennamen. Derzeit werden die
  folgenden Variablen verwendet:
</p>
<dl>
  <dt><code>#OBJ</code></dt>
  <dd>Das durch die Regel geprüfte Objekt, also &ndash; je nach den in der Regel spezifizierten Objekttypen &ndash; eine
    <a href="generated/Betriebsstätte.html">Betriebsstätte</a>, eine <a href="generated/Anlage.html">Anlage</a>, ein
    <a href="generated/Anlagenteil.html">Anlagenteil</a>, ein <a href="generated/Betreiber.html">Betreiber</a>, eine
    <a href="generated/EURegBetriebsstätte.html">EURegBetriebsstätte</a> oder eine
    <a href="generated/EURegAnlage.html">EURegAnlage</a>.
  </dd>
  <dt><code>#JAHR</code></dt>
  <dd>Das Berichtsjahr der <a href="generated/Betriebsstätte.html">Betriebsstätte</a>.</dd>
  <dt><code>#LAND</code></dt>
  <dd>Das <a href="generated/Land.html">Bundesland</a> der Betriebsstätte.</dd>
  <dt><code>#this</code></dt>
  <dd>
    Kann im Kontext einer Listenfilterung (s.o.) verwendet werden, um das Objekt, an dem die in eckigen Klammern
    formulierte Bedingung getestet wird, zu spezifizieren.
    <p class="beispiel">
      Die Anlagen-Liste aus den Beispielen zur Filterung und Projektion kann alternativ auch durch<br/>
      <code>#OBJ.anlagen.![betriebsstatus.ktext].?[#this == "in Betrieb"]</code> ausgedrückt werden.
    </p>
  </dd>
</dl>
<h3>Funktionen</h3>
<p>
  Die Komplexprüfung stellt einige Hilfsfunktionen bereit, um die Formulierung bestimmter Bedingungen zu erleichtern.
</p>
<p>
  Die mathematische Funktion <code>#ABS</code> berechnet den Absolut-Betrag eines numerischen Wertes.
</p>
<p class="beispiel">
  <code>#ABS(#OBJ.geoPunkt.ost - #OBJ.vorgaenger.geoPunkt.ost) <= 100</code>
</p>
<p>
  Daneben existiert die Funktion <code>#DATE</code>, die zur Spezifikation von
  <a href="base-types.html#Date">Datumswerten</a> dient.
</p>
<p class="beispiel">
  <code>#OBJ.inbetriebnahme < #DATE("01.07.2019")</code>
</p>
<p>
  Die Funktion <code>#NUMBER</code> wandelt einen String in eine Zahl um.
</p>
<p>
  Außerdem existiert die Funktion <code>#PARA</code>, die einen Parameterwert (als String) aus der Anwendung
  ausliest; das dabei verwendete Bundesland ist das der jeweils geprüften Betriebsstätte.
</p>
<p class="beispiel">
  <code>#OBJ.bauhoehe < #NUMBER(#PARA("que_hoehe"))</code>
</p>
<h3>Aufzählungstypen</h3>
<p>
  <em>Aufzählungstypen</em> sind Typen, von denen nur eine feste, begrenzte Zahl von Instanzen existiert. Die
  in der Regelprüfung verwendeten Aufzählungstypen haben alle ein Attribut namens <code>name</code>; einige von ihnen
  (z.B. <a href="generated/Leistungsklasse.html">Leistungsklasse</a>) haben auch noch weitere Attribute.<br>
</p>
<p class="hinweis">
  Alle Attribute von Aufzählungstypen sind selbst vom Typ <a href="base-types.html#String">String</a>.
</p>

</body>
</html>
