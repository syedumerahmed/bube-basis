import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconShapeTuple } from '@cds/core/icon/interfaces/icon.interfaces';

/**
 * Dieses Modul wurde angelegt, da ClarityV5 Icons nicht mit Jest zusammen funktionieren. (Das Clarity-Icons
 * Modul kann nicht korrekt in der Test-Configuration geparsed werden)
 * Da die Icons für die Tests nicht wichtig sind, biegen wir in der Package.json in der Jest-Config das Clarity-Icons Modul
 * auf dieses Modul um. (ModulesNameMapper: "^@cds/core/icon": "<rootDir>/src/testModules/cds-icon-test.module.ts")
 */

@NgModule({
  declarations: [],
  imports: [CommonModule],
})
export class CdsIconTestModule {}

export class ClarityIcons {
  public static addIcons(...shapes: IconShapeTuple[]) {}
}
