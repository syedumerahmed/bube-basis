import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { tap } from 'rxjs/operators';
import { BehoerdeListItemDto } from '@api/stammdaten';
import { ReferenzenRestControllerService } from '@api/referenzdaten';

@Injectable({ providedIn: 'root' })
export class BehoerdenListForEditResolver
  implements Resolve<Array<BehoerdeListItemDto>>
{
  constructor(
    private referenzenController: ReferenzenRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(): Observable<Array<BehoerdeListItemDto>> {
    return this.referenzenController.readAllBehoerden().pipe(
      tap((list: Array<BehoerdeListItemDto>) => {
        if (!list.length) {
          this.alertService.setWarning(
            'Keine Referenzwerte für Behörden gefunden.'
          );
        }
      }),
      this.alertService.catchError('Fehler beim Lesen der Behördenliste!')
    );
  }
}
