import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  ReferenzenRestControllerService,
  ReferenzlisteMetadataDto,
} from '@api/referenzdaten';

@Injectable()
export class ReferenzlisteMetadataListResolver
  implements Resolve<Array<ReferenzlisteMetadataDto>>
{
  constructor(
    private service: ReferenzenRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(): Observable<Array<ReferenzlisteMetadataDto>> {
    return this.service
      .readAllReferenzlistenMetadata()
      .pipe(
        this.alertService.catchError(
          'Fehler beim Lesen der Referenzlisten-Daten'
        )
      );
  }
}
