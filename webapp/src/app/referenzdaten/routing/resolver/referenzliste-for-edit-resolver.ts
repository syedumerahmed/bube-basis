import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { ReferenzDto } from '@api/stammdaten';

@Injectable({ providedIn: 'root' })
export class ReferenzlisteForEditResolver
  implements Resolve<Array<ReferenzDto>>
{
  constructor(
    private referenzenRestControllerService: ReferenzenRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Array<ReferenzDto>> {
    return this.referenzenRestControllerService
      .readReferenzlisteForEdit(route.params.name)
      .pipe(
        this.alertService.catchError(
          'Fehler beim Lesen der Referenzliste ' + route.params.name
        )
      );
  }
}
