import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  ReferenzenRestControllerService,
  ReferenzlisteMetadataDto,
} from '@api/referenzdaten';

@Injectable()
export class ReferenzlisteMetadataResolver
  implements Resolve<ReferenzlisteMetadataDto>
{
  constructor(
    private service: ReferenzenRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ReferenzlisteMetadataDto> {
    return this.service
      .readReferenzlisteMetadata(route.params.name)
      .pipe(
        this.alertService.catchError(
          'Fehler beim Lesen der Referenzlisten-Daten für ' + route.params.name
        )
      );
  }
}
