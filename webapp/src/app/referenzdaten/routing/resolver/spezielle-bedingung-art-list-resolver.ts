import { Injectable } from '@angular/core';
import { ReferenzlisteEnum } from '@api/stammdaten';
import { AlertService } from '@/base/service/alert.service';
import { AbstractReferenzResolver } from '@/referenzdaten/routing/resolver/abstract-referenz-resolver';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';

@Injectable({ providedIn: 'root' })
export class SpezielleBedingungArtListResolver extends AbstractReferenzResolver {
  constructor(
    referenzenService: ReferenzdatenService,
    alertService: AlertService
  ) {
    super(ReferenzlisteEnum.RSPECC, referenzenService, alertService);
  }
}
