import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { catchError, map } from 'rxjs/operators';
import {
  BehoerdeDto,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Land } from '@/base/model/Land';

@Injectable()
export class BehoerdenResolver implements Resolve<BehoerdeDto> {
  constructor(
    private referenzenRestControllerService: ReferenzenRestControllerService,
    private alertService: AlertService,
    private benutzerProfil: BenutzerProfil,
    private router: Router
  ) {}

  private static copyBehoerde(behoerde: BehoerdeDto): BehoerdeDto {
    return {
      ...behoerde,
      id: null,
      bezeichnung: 'Kopie - ' + behoerde.bezeichnung,
      schluessel: '',
      letzteAenderung: null,
    };
  }

  private createBehoerde(): BehoerdeDto {
    return {
      adresse: null,
      bezeichnung: '',
      bezeichnung2: '',
      gueltigBis: 2099,
      gueltigVon: 2007,
      id: null,
      kommunikationsverbindungen: [],
      kreis: '',
      land:
        this.benutzerProfil.land !== Land.DEUTSCHLAND
          ? this.benutzerProfil.landNr
          : null,
      schluessel: '',
      zustaendigkeiten: null,
    };
  }

  resolve(route: ActivatedRouteSnapshot): Observable<BehoerdeDto> {
    const id = route.params.id;
    if (id === 'neu') {
      return of(this.createBehoerde());
    }
    if (id === 'kopieren') {
      const behoerdeId =
        this.router.getCurrentNavigation().extras.state?.behoerdeId;
      return behoerdeId
        ? this.referenzenRestControllerService
            .readBehoerde(behoerdeId)
            .pipe(
              map(BehoerdenResolver.copyBehoerde),
              catchError(this.alertService.setAlert.bind(this))
            )
        : EMPTY;
    }
    return this.referenzenRestControllerService
      .readBehoerde(id)
      .pipe(this.alertService.catchError());
  }
}
