import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ReferenzDto } from '@api/stammdaten';
import { AlertService } from '@/base/service/alert.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';

@Injectable({ providedIn: 'root' })
export class BerichtsjahrListResolver implements Resolve<Array<ReferenzDto>> {
  constructor(
    private service: BerichtsjahrService,
    private alertService: AlertService
  ) {}

  resolve(): Observable<Array<ReferenzDto>> {
    return this.service.alleBerichtsJahre$.pipe(
      take(1),
      this.alertService.catchError()
    );
  }
}
