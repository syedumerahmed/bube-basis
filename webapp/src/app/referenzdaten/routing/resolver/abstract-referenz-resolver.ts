import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
export class AbstractReferenzResolver implements Resolve<Array<ReferenzDto>> {
  constructor(
    private readonly referenzliste: ReferenzlisteEnum,
    private readonly referenzenService: ReferenzdatenService,
    private readonly alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Array<ReferenzDto>> {
    return this.referenzenService
      .readReferenzliste(
        this.referenzliste,
        route.params.berichtsjahr,
        route.params.land
      )
      .pipe(
        tap((list: Array<ReferenzDto>) => {
          if (!list.length) {
            this.alertService.setWarning(
              `Keine Referenzwerte für ${this.referenzliste} gefunden.`
            );
          }
        }),
        this.alertService.catchError()
      );
  }
}
