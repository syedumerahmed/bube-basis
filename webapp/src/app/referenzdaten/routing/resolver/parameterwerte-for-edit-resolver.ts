import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { ParameterWertDto } from '@api/referenzdaten';

@Injectable({ providedIn: 'root' })
export class ParameterWerteForEditResolver
  implements Resolve<Array<ParameterWertDto>>
{
  constructor(
    private parameterService: ParameterService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Array<ParameterWertDto>> {
    return this.parameterService
      .readParameterwerteForEdit(route.params.schluessel)
      .pipe(
        this.alertService.catchError(
          'Fehler beim Lesen der Parameterwerte ' + route.params.schluessel
        )
      );
  }
}
