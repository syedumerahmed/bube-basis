import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { ParameterDto } from '@api/referenzdaten';

@Injectable()
export class ParameterlisteResolver implements Resolve<Array<ParameterDto>> {
  constructor(
    private service: ParameterService,
    private alertService: AlertService
  ) {}

  resolve(): Observable<Array<ParameterDto>> {
    return this.service
      .readParameterliste()
      .pipe(
        this.alertService.catchError('Fehler beim Lesen der Parameterliste')
      );
  }
}
