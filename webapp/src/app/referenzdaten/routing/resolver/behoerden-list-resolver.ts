import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { tap } from 'rxjs/operators';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';

@Injectable({ providedIn: 'root' })
export class BehoerdenListResolver implements Resolve<Array<BehoerdeListItem>> {
  constructor(
    private referenzdatenService: ReferenzdatenService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Array<BehoerdeListItem>> {
    const jahr = route.params.berichtsjahr || new Date().getFullYear();
    const land = route.params.land;
    return this.referenzdatenService.readBehoerden(jahr, land).pipe(
      tap((list: Array<BehoerdeListItem>) => {
        if (!list.length) {
          this.alertService.setWarning(
            'Keine Referenzwerte für Behörden gefunden.'
          );
        }
      }),
      this.alertService.catchError('Fehler beim Lesen der Behördenliste!')
    );
  }
}
