import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReferenzDto } from '@api/stammdaten';
import { AlertService } from '@/base/service/alert.service';
import { switchMap, tap } from 'rxjs/operators';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';

@Injectable({ providedIn: 'root' })
export class KommunikationsTypenListResolver
  implements Resolve<Array<ReferenzDto>>
{
  constructor(
    private berichtsjahrService: BerichtsjahrService,
    private referenzdatenService: ReferenzdatenService,
    private alertService: AlertService,
    private benutzerProfil: BenutzerProfil
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Array<ReferenzDto>> {
    const land = route.params.land || this.benutzerProfil.landNr;
    return this.berichtsjahrService.selectDefaultBerichtsjahr().pipe(
      switchMap((dto: ReferenzDto) =>
        this.referenzdatenService.readReferenzliste(
          'RKOM',
          Number(dto.schluessel),
          land
        )
      ),
      tap((list: Array<ReferenzDto>) => {
        if (!list.length) {
          this.alertService.setWarning(
            'Keine Referenzwerte für RKOM gefunden.'
          );
        }
      }),
      this.alertService.catchError('Fehler beim Lesen der Referenzliste RKOM!')
    );
  }
}
