import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { ParameterDto } from '@api/referenzdaten';

@Injectable()
export class ParameterResolver implements Resolve<ParameterDto> {
  constructor(
    private service: ParameterService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ParameterDto> {
    return this.service
      .readParameter(route.params.schluessel)
      .pipe(
        this.alertService.catchError(
          'Fehler beim Lesen eines Parameters ' + route.params.schluessel
        )
      );
  }
}
