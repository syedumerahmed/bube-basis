import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReferenzlisteForEditResolver } from '@/referenzdaten/routing/resolver/referenzliste-for-edit-resolver';
import { ReferenzdatenComponent } from '@/referenzdaten/component/referenzdaten/referenzdaten/referenzdaten.component';
import { ReferenzlisteMetadataListResolver } from '@/referenzdaten/routing/resolver/referenzliste-metadata-list-resolver';
import { ParameterComponent } from '@/referenzdaten/component/parameter/parameter.component';
import { ReferenzlisteMetadataResolver } from '@/referenzdaten/routing/resolver/referenzliste-metadata-resolver';
import { ParameterlisteResolver } from '@/referenzdaten/routing/resolver/parameter-list-resolver';
import { ParameterWerteComponent } from '@/referenzdaten/component/parameter-werte/parameter-werte.component';
import { ParameterWerteForEditResolver } from '@/referenzdaten/routing/resolver/parameterwerte-for-edit-resolver';
import { ParameterResolver } from '@/referenzdaten/routing/resolver/parameter-resolver';
import { ReferenzlistenListeComponent } from '@/referenzdaten/component/referenzdaten/referenzlisten-liste/referenzlisten-liste.component';
import { ReferenzlisteComponent } from '@/referenzdaten/component/referenzdaten/referenzliste/referenzliste.component';
import { BehoerdenListeComponent } from '@/referenzdaten/component/behoerden/behoerden-liste/behoerden-liste.component';
import { BehoerdenDetailsComponent } from '@/referenzdaten/component/behoerden/behoerden-details/behoerden-details.component';
import { BehoerdenResolver } from '@/referenzdaten/routing/resolver/behoerden-resolver';
import { KommunikationsTypenListResolver } from '@/referenzdaten/routing/resolver/kommunikations-typen-list-resolver';
import { LandisocodeListResolver } from '@/referenzdaten/routing/resolver/landisocode-list-resolver';
import { VertraulichkeitsgrundListResolver } from '@/referenzdaten/routing/resolver/vertraulichkeitsgrund-list-resolver';
import { BehoerdenListForEditResolver } from '@/referenzdaten/routing/resolver/behoerden-list-for-edit-resolver';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ReferenzdatenComponent,
    children: [
      {
        path: 'referenzlisten',
        component: ReferenzlistenListeComponent,
        resolve: { referenzlistenListe: ReferenzlisteMetadataListResolver },
      },
      {
        path: 'referenzlisten/liste/:name',
        component: ReferenzlisteComponent,
        resolve: {
          referenzliste: ReferenzlisteForEditResolver,
          referenzlisteMetadata: ReferenzlisteMetadataResolver,
        },
      },
      {
        path: 'behoerden',
        component: BehoerdenListeComponent,
        resolve: { behoerdenListe: BehoerdenListForEditResolver },
      },
      {
        path: 'behoerden/:id',
        component: BehoerdenDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          behoerde: BehoerdenResolver,
          kommunikationsTypenListe: KommunikationsTypenListResolver,
          landIsoCodeListe: LandisocodeListResolver,
          vertraulichkeitsgrundListe: VertraulichkeitsgrundListResolver,
        },
      },
      {
        path: 'parameter',
        component: ParameterComponent,
        resolve: { parameterliste: ParameterlisteResolver },
      },
      {
        path: 'parameter/werte/:schluessel',
        component: ParameterWerteComponent,
        resolve: {
          parameterWerte: ParameterWerteForEditResolver,
          parameter: ParameterResolver,
        },
      },
      {
        path: '',
        redirectTo: 'referenzlisten',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ReferenzlisteMetadataListResolver,
    ReferenzlisteForEditResolver,
    ReferenzlisteMetadataResolver,
    ParameterlisteResolver,
    ParameterWerteForEditResolver,
    ParameterResolver,
    BehoerdenResolver,
  ],
})
export class ReferenzdatenRoutingModule {}
