import { BehoerdeListItemDto } from '@api/stammdaten';

export class BehoerdeListItem implements BehoerdeListItemDto {
  readonly id: number;
  readonly land: string;
  readonly schluessel: string;
  readonly bezeichnung: string;
  readonly gueltigVon: number;
  readonly gueltigBis: number;
  readonly ktext: string;

  constructor(dto: BehoerdeListItemDto) {
    Object.assign(this, dto);
    this.ktext = `${this.schluessel} - ${this.bezeichnung}`;
  }
}
