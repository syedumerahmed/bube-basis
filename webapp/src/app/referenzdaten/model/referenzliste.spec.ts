import { Referenzliste } from '@/referenzdaten/model/referenzliste';

describe('Referenzliste', () => {
  describe('empty list', () => {
    let referenzliste: Referenzliste<{ id: number; someProp?: string }>;
    beforeEach(() => {
      referenzliste = new Referenzliste([]);
    });

    it('should create', () => {
      expect(referenzliste).toBeTruthy();
    });

    it('should return empty value', () => {
      expect(referenzliste.values).toEqual([]);
    });

    it('should map no value', () => {
      expect(referenzliste.map([])).toEqual([]);
    });

    it('should skip all values', () => {
      const list = [{ id: 1 }, { id: 2, someProp: 'someProp' }];
      expect(referenzliste.map(list)).toEqual(list);
    });

    it('should not find value', () => {
      const value = { id: 2, someProp: 'someProp' };
      expect(referenzliste.find(value)).toBe(value);
    });
  });

  describe('two entries', () => {
    let values: { id: number; value: string }[];
    let referenzliste: Referenzliste<{ id: number; value?: string }>;
    beforeEach(() => {
      values = [
        { id: 1, value: 'current1' },
        { id: 2, value: 'current2' },
      ];
      referenzliste = new Referenzliste(values);
    });

    it('should create', () => {
      expect(referenzliste).toBeTruthy();
    });

    it('should return values', () => {
      expect(referenzliste.values).toEqual(values);
    });

    it('should map no value', () => {
      expect(referenzliste.map([])).toEqual([]);
    });

    it('should find value', () => {
      const value = { id: 2, someProp: 'someProp' };
      expect(referenzliste.find(value)).toBe(values[1]);
    });

    it('should not find value', () => {
      const value = { id: 3, someProp: 'someProp' };
      expect(referenzliste.find(value)).toBe(value);
    });

    it('should map values by id', () => {
      const list = [{ id: 1 }, { id: 2, someProp: 'someProp' }, { id: 3 }];
      const actual = referenzliste.map(list);
      expect(actual).toHaveLength(3);
      expect(actual[0]).toBe(values[0]);
      expect(actual[1]).toBe(values[1]);
      expect(actual[2]).toBe(list[2]);
    });
  });
});
