export class Referenzliste<R extends { id: number }> {
  private readonly referenzen: Map<number, R>;

  constructor(referenzenList: Array<R>) {
    if (!Array.isArray(referenzenList)) {
      throw Error('Ungültiger Parameter: ' + referenzenList);
    }
    this.referenzen = new Map(referenzenList.map((r) => [r.id, r]));
  }

  find(referenz: R): R {
    return this.referenzen.get(referenz?.id) ?? referenz;
  }

  map(liste: Array<R>): Array<R> {
    return liste.map(this.find.bind(this));
  }

  get values(): Array<R> {
    return [...this.referenzen.values()];
  }

  get length(): number {
    return this.referenzen.size;
  }
}
