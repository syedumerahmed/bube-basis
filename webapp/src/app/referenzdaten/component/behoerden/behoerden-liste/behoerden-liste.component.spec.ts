import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BehoerdenListeComponent } from './behoerden-liste.component';
import { ClarityModule } from '@clr/angular';
import { MockComponents, MockModule, MockPipe, MockService } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import {
  BehoerdeDto,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('BehoerdenListeComponent', () => {
  let component: BehoerdenListeComponent;
  let fixture: ComponentFixture<BehoerdenListeComponent>;

  const liste: Array<BehoerdeDto> = [
    { id: 1, schluessel: '01', land: '01' },
    { id: 2, schluessel: '02', land: '02' },
    { id: 3, schluessel: '03', land: '03' },
    { id: 4, schluessel: '04', land: '04' },
  ] as Array<BehoerdeDto>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        { provide: AlertService, useValue: MockService(AlertService) },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ReferenzenRestControllerService,
          useValue: MockService(ReferenzenRestControllerService),
        },
        {
          provide: FilenameDateFormatPipe,
          useValue: MockPipe(FilenameDateFormatPipe),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: { behoerdenListe: liste } },
          },
        },
      ],
      declarations: [
        BehoerdenListeComponent,
        MockComponents(ImportModalComponent),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehoerdenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('delete behoerde', () => {
    let behoerdeToDelete;
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        spyOn(referenzenService, 'deleteBehoerde').mockReturnValue(of(null));
        component.openLoeschenDialog(1);
        behoerdeToDelete = liste.find((b) => b.id === 1);
        component.deleteBehoerde();
      }
    ));

    it('should call service', inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        expect(referenzenService.deleteBehoerde).toHaveBeenCalledWith(1);
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove behoerde from list', () => {
      expect(component.behoerdenListe).not.toContainEqual(behoerdeToDelete);
    });
  });

  describe('delete behoerde unsuccessful', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        spyOn(referenzenService, 'deleteBehoerde').mockReturnValue(
          throwError({ error: 'error' })
        );
        component.openLoeschenDialog(1);
        component.deleteBehoerde();
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should not remove anything from list', () => {
      expect(component.behoerdenListe).toEqual(liste);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalledWith('error');
      }
    ));
  });

  describe('delete multiple behoerden', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        spyOn(referenzenService, 'deleteMultipleBehoerden').mockReturnValue(
          of(null)
        );
        component.selected = component.behoerdenListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteBehoerden();
      }
    ));

    it('should call service', inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        expect(referenzenService.deleteMultipleBehoerden).toHaveBeenCalledWith([
          2, 3,
        ]);
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove behoerden from list', () => {
      expect(component.behoerdenListe).not.toContainEqual(liste[1]);
      expect(component.behoerdenListe).not.toContainEqual(liste[2]);
    });
  });

  describe('delete behoerden unsuccessful', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        spyOn(referenzenService, 'deleteMultipleBehoerden').mockReturnValue(
          throwError({ error: 'error' })
        );
        component.selected = component.behoerdenListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteBehoerden();
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should not remove anything from list', () => {
      expect(component.behoerdenListe).toEqual(liste);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalledWith('error');
      }
    ));
  });
});
