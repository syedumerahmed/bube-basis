import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { AlertService } from '@/base/service/alert.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import * as fileSaver from 'file-saver';
import { Event, EventBusService } from '@/base/service/event-bus.service';
import { Subscription } from 'rxjs';
import { BehoerdenImportAdapterService } from '@/base/service/behoerden-import-adapter.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import { switchMap } from 'rxjs/operators';
import {
  ExportiereBehoerdenCommand,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { BehoerdeListItemDto } from '@api/stammdaten';
import {
  angleIcon,
  checkIcon,
  ClarityIcons,
  copyIcon,
  eyeIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  viewListIcon,
} from '@cds/core/icon';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';

const KOPIEREN_FLAG = 'kopieren';

ClarityIcons.addIcons(
  viewListIcon,
  checkIcon,
  timesIcon,
  trashIcon,
  angleIcon,
  plusIcon,
  eyeIcon,
  copyIcon
);
@Component({
  selector: 'app-behoerden-liste',
  templateUrl: './behoerden-liste.component.html',
  styleUrls: ['./behoerden-liste.component.scss'],
})
export class BehoerdenListeComponent implements OnInit, OnDestroy {
  behoerdenListe: Array<BehoerdeListItemDto>;
  loeschenDialog: boolean;
  public exportDialogOpen = false;
  public importModalOpen = false;
  behoerdenId: number;
  selected: Array<BehoerdeListItemDto>;
  loescht = false;

  private eventbusSub: Subscription;

  readonly SortOrder = ClrDatagridSortOrder;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly referenzenService: ReferenzenRestControllerService,
    private readonly referenzenCacheService: ReferenzdatenService,
    private readonly alertService: AlertService,
    private readonly dateFormat: FilenameDateFormatPipe,
    private readonly eventbus: EventBusService,
    readonly behoerdenImportAdapter: BehoerdenImportAdapterService
  ) {}

  ngOnInit(): void {
    this.behoerdenListe = this.route.snapshot.data.behoerdenListe;
    this.selected = this.canDelete ? [] : null;

    this.eventbusSub = this.eventbus
      .on(Event.BehoerdenImportFertig)
      .pipe(switchMap(() => this.referenzenService.readAllBehoerden()))
      .subscribe(
        (liste: BehoerdeListItemDto[]) => {
          this.behoerdenListe = liste;
          this.markierungAufheben();
        },
        (error) => this.alertService.setAlert(error)
      );
  }

  ngOnDestroy(): void {
    this.eventbusSub.unsubscribe();
  }

  get canDelete(): boolean {
    return this.benutzerProfil.canDeleteReferenzLand;
  }

  get canWrite(): boolean {
    return this.benutzerProfil.canWriteReferenzLand;
  }

  get canExport(): boolean {
    return this.benutzerProfil.canReadReferenzdaten;
  }

  get canImport(): boolean {
    return this.benutzerProfil.canWriteReferenzLand;
  }

  hatMarkierung(): boolean {
    return this.selected?.length === 0;
  }

  alleMarkieren(): void {
    this.selected = this.behoerdenListe;
  }

  markierungAufheben(): void {
    this.selected = [];
  }

  kopieren(behoerdeId: number): void {
    this.router.navigate([KOPIEREN_FLAG], {
      relativeTo: this.route,
      state: { behoerdeId },
    });
  }

  deleteBehoerde(): void {
    this.loescht = true;
    this.referenzenService.deleteBehoerde(this.behoerdenId).subscribe(
      () => {
        const index = this.behoerdenListe.findIndex(
          (b) => b.id === this.behoerdenId
        );
        if (index !== -1) {
          this.behoerdenListe.splice(index, 1);
        }
        this.referenzenCacheService.invalidateBehoerdenCache();
        this.closeLoeschenDialog();
      },
      (err) => {
        this.alertService.setAlert(err.error);
        this.closeLoeschenDialog();
      }
    );
  }

  deleteBehoerden(): void {
    this.loescht = true;
    this.referenzenService
      .deleteMultipleBehoerden(this.selected.map((b) => b.id))
      .subscribe(
        () => {
          this.behoerdenListe = this.behoerdenListe.filter(
            (b) => !this.selected.includes(b)
          );
          this.selected = [];
          this.referenzenCacheService.invalidateBehoerdenCache();
          this.closeLoeschenDialog();
        },
        (err) => {
          this.alertService.setAlert(err.error);
          this.closeLoeschenDialog();
        }
      );
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.behoerdenId = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(behoerdenId: number): void {
    this.behoerdenId = behoerdenId;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }

  openExportDialog(): void {
    this.exportDialogOpen = true;
  }

  closeExportDialog(): void {
    this.exportDialogOpen = false;
  }

  startExport(): void {
    this.exportDialogOpen = false;
    const idList: Array<number> = this.selected.map((behoerde) => behoerde.id);
    const command: ExportiereBehoerdenCommand = { behoerdenList: idList };

    this.referenzenService.exportBehoerden(command).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(new Date());
        fileSaver.saveAs(blob, 'behoerden_' + datum + '.xml');
        this.alertService.setSuccess(
          this.selected.length +
            ' Behörde' +
            (this.selected.length > 1 ? 'n' : '') +
            ' wurde' +
            (this.selected.length > 1 ? 'n' : '') +
            ' exportiert.'
        );
        this.markierungAufheben();
      },
      () => {
        this.alertService.setAlert(
          'Es ist ein Fehler beim Exportieren der XML-Datei aufgetreten'
        );
      }
    );
  }
}
