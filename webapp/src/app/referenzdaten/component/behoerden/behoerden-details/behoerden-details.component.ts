import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import {
  AdresseComponent,
  AdresseFormData,
} from '@/basedomain/component/adresse/adresse.component';
import { FormArray, FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  BehoerdeAllgemeinComponent,
  BehoerdeAllgemeinFormData,
} from '@/referenzdaten/component/behoerden/behoerde-allgemein/behoerde-allgemein.component';
import {
  KommunikationComponent,
  KommunikationsverbindungFormData,
} from '@/basedomain/component/kommunikation/kommunikation.component';
import { LandProperties, lookupLand } from '@/base/model/Land';
import { Observable } from 'rxjs';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  BehoerdeDto,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';

export interface BehoerdenFormData {
  allgemein: BehoerdeAllgemeinFormData;
  adresse: AdresseFormData;
  kommunikation: Array<KommunikationsverbindungFormData>;
}

ClarityIcons.addIcons(timesIcon, floppyIcon, errorStandardIcon);

@Component({
  selector: 'app-behoerden-details',
  templateUrl: './behoerden-details.component.html',
  styleUrls: ['./behoerden-details.component.scss'],
})
export class BehoerdenDetailsComponent implements OnInit, UnsavedChanges {
  @ViewChild(BehoerdeAllgemeinComponent)
  allgemeinComponent: BehoerdeAllgemeinComponent;

  @ViewChild(AdresseComponent)
  adresseComponent: AdresseComponent;

  behoerde: BehoerdeDto;
  form: FormGroup<BehoerdenFormData>;

  saveAttempt = false;

  isoCodes: Referenzliste<Required<ReferenzDto>>;
  kommunikationsTypen: Referenzliste<Required<ReferenzDto>>;
  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;

  alert: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly alertService: AlertService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly referenzenService: ReferenzenRestControllerService,
    private readonly referenzenCacheService: ReferenzdatenService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly fb: FormBuilder
  ) {
    this.route.data.subscribe((routeData) => {
      this.isoCodes = new Referenzliste(routeData.landIsoCodeListe);
      this.kommunikationsTypen = new Referenzliste(
        routeData.kommunikationsTypenListe
      );
      this.vertraulichkeitsgruende = new Referenzliste(
        routeData.vertraulichkeitsgrundListe
      );
    });
  }

  ngOnInit(): void {
    this.behoerde = this.route.snapshot.data.behoerde;
    if (!this.behoerde) {
      throw Error('behoerde === ' + this.behoerde);
    }
    if (!this.form) {
      this.form = this.createForm(this.toForm(this.behoerde));
    } else {
      this.form.reset(this.toForm(this.behoerde));
    }

    if (!this.canWrite) {
      this.form.disable();
    }
  }

  get canWrite(): boolean {
    return this.benutzerProfil.canWriteReferenzLand;
  }

  get isNeu(): boolean {
    return this.behoerde.id == null;
  }

  get displayLand(): string {
    return LandProperties[lookupLand(this.behoerde.land)].display;
  }

  get displayName(): string {
    return this.behoerde.bezeichnung;
  }

  get allgemeinForm(): FormGroup<BehoerdeAllgemeinFormData> {
    return this.form.controls.allgemein as FormGroup<BehoerdeAllgemeinFormData>;
  }

  get adresseForm(): FormGroup<AdresseFormData> {
    return this.form.controls.adresse as FormGroup<AdresseFormData>;
  }

  get kommunikationForm(): FormArray<KommunikationsverbindungFormData> {
    return this.form.controls
      .kommunikation as FormArray<KommunikationsverbindungFormData>;
  }

  save(): void {
    this.saveAttempt = true;
    if (this.form.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.adresseComponent?.markAllAsTouched();
      return;
    }
    const formData = this.form.value;
    const behoerdeDto: BehoerdeDto = {
      ...this.behoerde,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      zustaendigkeiten: formData.allgemein.zustaendigkeiten,
      land: LandProperties[formData.allgemein.land].nr,
      adresse: FormUtil.emptyStringsToNull(formData.adresse),
      kommunikationsverbindungen: formData.kommunikation,
    };

    let response: Observable<BehoerdeDto>;
    if (this.isNeu) {
      response = this.referenzenService.createBehoerde(behoerdeDto);
    } else {
      response = this.referenzenService.updateBehoerde(behoerdeDto);
    }

    response.subscribe(
      (behoerde) => {
        this.behoerde = behoerde;
        this.reset();
        this.alert = null;
        this.router.navigate(['../', behoerde.id], { relativeTo: this.route });
        this.referenzenCacheService.invalidateBehoerdenCache();
        this.alertService.clearAlertMessages();
      },
      (err) => (this.alert = AlertService.getAlertText(err))
    );
  }

  reset(): void {
    if (this.form) {
      this.saveAttempt = false;
      const originalBehoerde = this.toForm(this.behoerde);
      this.form.reset(originalBehoerde);
      this.form.setControl(
        'allgemein',
        BehoerdeAllgemeinComponent.createForm(
          this.fb,
          originalBehoerde.allgemein
        )
      );
      this.form.setControl(
        'adresse',
        AdresseComponent.createForm(this.fb, originalBehoerde.adresse)
      );
      this.form.setControl(
        'kommunikation',
        KommunikationComponent.createForm(
          this.fb,
          originalBehoerde.kommunikation,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
    }
  }

  private createForm(
    initialBehoerde: BehoerdenFormData
  ): FormGroup<BehoerdenFormData> {
    return this.fb.group<BehoerdenFormData>({
      allgemein: BehoerdeAllgemeinComponent.createForm(
        this.fb,
        initialBehoerde.allgemein
      ),
      adresse: AdresseComponent.createForm(this.fb, initialBehoerde.adresse),
      kommunikation: KommunikationComponent.createForm(
        this.fb,
        initialBehoerde.kommunikation,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private toForm(behoerdeDto: BehoerdeDto): BehoerdenFormData {
    return {
      allgemein: BehoerdeAllgemeinComponent.toForm(behoerdeDto),
      adresse: AdresseComponent.toForm(
        behoerdeDto.adresse,
        this.isoCodes,
        this.vertraulichkeitsgruende
      ),
      kommunikation: KommunikationComponent.toForm(
        behoerdeDto.kommunikationsverbindungen,
        this.kommunikationsTypen
      ),
    };
  }

  navigateBack(): void {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  canDeactivate(): boolean {
    return this.form.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
