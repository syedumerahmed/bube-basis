import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BehoerdenDetailsComponent } from './behoerden-details.component';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockProviders,
} from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { BehoerdeAllgemeinComponent } from '@/referenzdaten/component/behoerden/behoerde-allgemein/behoerde-allgemein.component';
import { AdresseComponent } from '@/basedomain/component/adresse/adresse.component';
import { KommunikationComponent } from '@/basedomain/component/kommunikation/kommunikation.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { BehaviorSubject, Subject } from 'rxjs';
import { Land, LandProperties } from '@/base/model/Land';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BehoerdeListItemDto, ReferenzDto } from '@api/stammdaten';
import {
  BehoerdeDto,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import spyOn = jest.spyOn;

describe('BehoerdenDetailsComponent', () => {
  let component: BehoerdenDetailsComponent;
  let fixture: ComponentFixture<BehoerdenDetailsComponent>;

  const behoerde: BehoerdeListItemDto = {
    id: null,
    bezeichnung: '',
    gueltigBis: 0,
    gueltigVon: 0,
    land: '01',
    schluessel: '',
  };

  const landIsoCodeListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' } as ReferenzDto,
  ];
  const vertraulichkeitsgruende: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' } as ReferenzDto,
  ];
  const kommunikationsTypenListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' } as ReferenzDto,
  ];

  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    const routeData$ = new BehaviorSubject({
      behoerde,
      landIsoCodeListe,
      vertraulichkeitsgrundListe: vertraulichkeitsgruende,
      kommunikationsTypenListe,
    });
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        RouterTestingModule,
      ],
      declarations: [
        BehoerdenDetailsComponent,
        BehoerdeAllgemeinComponent,
        MockComponents(
          AdresseComponent,
          KommunikationComponent,
          TooltipComponent
        ),
      ],
      providers: [
        MockProviders(ReferenzenRestControllerService),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: { data: routeData$, snapshot: { data: routeData$.value } },
        },
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehoerdenDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should have initial state', () => {
    const form = component.form;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [BenutzerProfil],
    (benutzerProfil: BenutzerProfil) => {
      spyOn(benutzerProfil, 'canWriteReferenzLand', 'get').mockReturnValue(
        false
      );
      spyOn(benutzerProfil, 'isGesamtadmin', 'get').mockReturnValue(false);
      component.ngOnInit();
      expect(component.form.enabled).toBe(false);
    }
  ));

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({ allgemein: { kreis: '321' } });
    });

    it('should be invalid', () => {
      expect(component.form.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          BehoerdeAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [ReferenzenRestControllerService],
        (referenzenService: ReferenzenRestControllerService) => {
          expect(referenzenService.createBehoerde).not.toHaveBeenCalled();
          expect(referenzenService.updateBehoerde).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.form.patchValue({
        allgemein: {
          land: Land.BREMEN,
          schluessel: '01',
          bezeichnung: 'bezeichnung',
          gueltigVon: 2007,
          gueltigBis: 2010,
        },
        adresse: {
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
        },
      });
      component.form.markAsDirty();
      component.form.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.form.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedBehoerdenDto: BehoerdeDto = {
        land: LandProperties[Land.BREMEN].nr,
        id: null,
        schluessel: '01',
        bezeichnung: 'bezeichnung',
        gueltigVon: 2007,
        gueltigBis: 2010,
        zustaendigkeiten: [],
        bezeichnung2: null,
        kreis: null,
        kommunikationsverbindungen: [],
        adresse: {
          notiz: null,
          postfachPlz: null,
          postfach: null,
          landIsoCode: null,
          vertraulichkeitsgrund: null,
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
          hausNr: null,
          strasse: null,
        },
      };
      let behoerdeObservable: Subject<BehoerdeDto>;
      beforeEach(inject(
        [ReferenzenRestControllerService],
        (referenzenService: ReferenzenRestControllerService) => {
          behoerdeObservable = new Subject<BehoerdeDto>();
          spyOn(referenzenService, 'createBehoerde').mockReturnValue(
            behoerdeObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [ReferenzenRestControllerService],
        (referenzenService: ReferenzenRestControllerService) => {
          expect(referenzenService.createBehoerde).toHaveBeenCalledWith(
            expectedBehoerdenDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedBehoerdenDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigate').mockResolvedValue(true);
          behoerdeObservable.next(responseDto);
          behoerdeObservable.complete();
        }));

        it('should update behoerde', () => {
          expect(component.behoerde).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.form.touched).toBe(false);
          expect(component.form.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigate).toHaveBeenCalled();
        }));
      });
    });
  });
});
