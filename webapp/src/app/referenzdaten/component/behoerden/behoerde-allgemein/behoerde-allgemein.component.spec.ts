import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BehoerdeAllgemeinComponent } from './behoerde-allgemein.component';
import { MockComponent, MockModule } from 'ng-mocks';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { FormBuilder } from '@/base/forms';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BehoerdeDto } from '@api/referenzdaten';

const behoerde: BehoerdeDto = {
  adresse: null,
  bezeichnung: '',
  bezeichnung2: '',
  gueltigBis: 0,
  gueltigVon: 0,
  id: 0,
  kommunikationsverbindungen: [],
  kreis: '',
  land: '01',
  letzteAenderung: '',
  schluessel: '',
  zustaendigkeiten: null,
};

describe('BehoerdeAllgemeinComponent', () => {
  let component: BehoerdeAllgemeinComponent;
  let fixture: ComponentFixture<BehoerdeAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ReactiveFormsModule), MockModule(ClarityModule)],
      declarations: [
        MockComponent(TooltipComponent),
        BehoerdeAllgemeinComponent,
      ],
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(BehoerdeAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = BehoerdeAllgemeinComponent.createForm(
      formBuilder,
      BehoerdeAllgemeinComponent.toForm(behoerde)
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
