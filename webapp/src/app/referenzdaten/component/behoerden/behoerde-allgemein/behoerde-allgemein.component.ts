import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { BehoerdeDto, BehoerdeZustaendigkeitenEnum } from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Land, LandProperties, lookupLand } from '@/base/model/Land';

export interface BehoerdeAllgemeinFormData {
  land: string;
  schluessel: string;
  kreis: string;
  bezeichnung: string;
  bezeichnung2: string;
  zustaendigkeiten: Array<BehoerdeZustaendigkeitenEnum>;
  gueltigVon: number;
  gueltigBis: number;
}

@Component({
  selector: 'app-behoerde-allgemein',
  templateUrl: './behoerde-allgemein.component.html',
  styleUrls: ['./behoerde-allgemein.component.scss'],
})
export class BehoerdeAllgemeinComponent
  implements AfterViewInit, AfterViewChecked
{
  constructor(private readonly benutzerProfil: BenutzerProfil) {}

  get canSeeLand(): boolean {
    return this.isNeu && this.benutzerProfil.isGesamtadmin;
  }

  @Input()
  form: FormGroup<BehoerdeAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  isNeu: boolean;

  readonly zustaendigkeitenListe: Array<BehoerdeZustaendigkeitenEnum> =
    Object.values(BehoerdeZustaendigkeitenEnum);
  readonly laender = new Map(
    Object.entries(LandProperties).filter(
      (land) => Object.values(land)[0] !== Land.DEUTSCHLAND
    )
  );

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  zustaendigkeiten: Record<BehoerdeZustaendigkeitenEnum, string> = {
    PRTR: 'PRTR',
    UEBERWACHUNG: 'Überwachung',
    LAND: 'Land',
    KREIS: 'Kreis',
    BIMSCHV11: '11. BImSchV',
    BIMSCHV42: '42. BImSchV',
    GENEHMIGUNG: 'Genehmigung',
  };

  static toForm(dto: BehoerdeDto): BehoerdeAllgemeinFormData {
    return {
      land: dto.land,
      kreis: dto.kreis,
      schluessel: dto.schluessel,
      gueltigBis: dto.gueltigBis,
      gueltigVon: dto.gueltigVon,
      bezeichnung2: dto.bezeichnung2,
      bezeichnung: dto.bezeichnung,
      zustaendigkeiten: dto.zustaendigkeiten ? [...dto.zustaendigkeiten] : [],
    };
  }

  static createForm(
    fb: FormBuilder,
    allgemein: BehoerdeAllgemeinFormData
  ): FormGroup<BehoerdeAllgemeinFormData> {
    return fb.group({
      land: [
        allgemein.land ? lookupLand(allgemein.land) : null,
        Validators.required,
      ],
      schluessel: [
        allgemein.schluessel,
        [Validators.required, Validators.maxLength(10)],
      ],
      kreis: [allgemein.kreis, [Validators.maxLength(3)]],
      bezeichnung: [
        allgemein.bezeichnung,
        [Validators.required, Validators.maxLength(500)],
      ],
      bezeichnung2: [allgemein.bezeichnung2, [Validators.maxLength(500)]],
      zustaendigkeiten: [allgemein.zustaendigkeiten],
      gueltigVon: [
        allgemein.gueltigVon,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
      gueltigBis: [
        allgemein.gueltigBis,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm?.markAsTouched();
  }

  displayZustaendigkeit(zust: BehoerdeZustaendigkeitenEnum): string {
    return this.zustaendigkeiten[zust];
  }
}
