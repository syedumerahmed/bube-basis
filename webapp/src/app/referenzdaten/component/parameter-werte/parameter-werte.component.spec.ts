import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ParameterWerteComponent } from './parameter-werte.component';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MockComponent,
  MockModule,
  MockProviders,
  MockService,
} from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { AlertService } from '@/base/service/alert.service';
import {
  ParameterDto,
  ParameterRestControllerService,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { of } from 'rxjs';
import spyOn = jest.spyOn;

describe('ParameterWerteComponent', () => {
  let component: ParameterWerteComponent;
  let fixture: ComponentFixture<ParameterWerteComponent>;

  const para: ParameterDto = {
    schluessel: 'schluessel',
    text: 'text',
    typ: 'INTEGER',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ParameterWerteComponent, MockComponent(TooltipComponent)],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ParameterRestControllerService,
          useValue: MockService(ParameterRestControllerService),
        },
        {
          provide: ReferenzenRestControllerService,
          useValue: MockService(ReferenzenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: { parameterWerte: [], parameter: para } },
          },
        },
        MockProviders(BerichtsjahrService, AlertService),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
      ],
    }).compileComponents();
  });

  beforeEach(inject([ReferenzenRestControllerService], (referenzenService) => {
    spyOn(referenzenService, 'readAllBehoerden').mockReturnValue(of());
    fixture = TestBed.createComponent(ParameterWerteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
