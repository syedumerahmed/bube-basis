import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { AlertService } from '@/base/service/alert.service';
import { ValidatorFn, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ClrForm } from '@clr/angular';
import {
  BehoerdeListItemDto,
  ParameterDto,
  ParameterRestControllerService,
  ParameterTypEnum,
  ParameterWertDto,
  ReferenzenRestControllerService,
} from '@api/referenzdaten';
import { ReferenzDto } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties, lookupLand } from '@/base/model/Land';
import {
  ClarityIcons,
  copyIcon,
  exclamationCircleIcon,
  eyeIcon,
  plusIcon,
  trashIcon,
  viewListIcon,
} from '@cds/core/icon';
import { ParameterService } from '@/referenzdaten/service/parameter.service';

export interface ParameterWertFormData {
  land: string;
  wert: string;
  behoerde?: BehoerdeListItemDto;
  gueltigVon: number;
  gueltigBis: number;
}

const kommunikationsverbindungValidator: ValidatorFn[] = [
  Validators.required,
  Validators.maxLength(500),
];

ClarityIcons.addIcons(
  trashIcon,
  exclamationCircleIcon,
  copyIcon,
  eyeIcon,
  plusIcon,
  viewListIcon
);

@Component({
  selector: 'app-parameter-werte',
  templateUrl: './parameter-werte.component.html',
  styleUrls: ['./parameter-werte.component.scss'],
})
export class ParameterWerteComponent implements OnInit {
  readonly LandProperties = LandProperties;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly parameterRestService: ParameterRestControllerService,
    private readonly parameterChachingService: ParameterService,
    private readonly fb: FormBuilder,
    private readonly alertService: AlertService,
    private readonly referenzenService: ReferenzenRestControllerService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  public alleBehoerden: Array<BehoerdeListItemDto>;
  public filteredBehoerden: BehaviorSubject<Array<BehoerdeListItemDto>>;

  landSubscription: Subscription;

  public loeschenDialog: boolean;
  public bearbeitenDialog: boolean;
  public parameter: ParameterDto;
  public parameterWerte: Array<ParameterWertDto>;
  public parameterWert: ParameterWertDto;
  public parameterWertForm: FormGroup<ParameterWertFormData>;
  public alert: string;

  static createForm(
    fb: FormBuilder,
    para: ParameterWertFormData,
    typ: string
  ): FormGroup<ParameterWertFormData> {
    return fb.group<ParameterWertFormData>({
      land: [lookupLand(para.land), [Validators.required]],
      wert: [
        para.wert,
        [
          ...kommunikationsverbindungValidator,
          ParameterWerteComponent.specificValidator(typ),
        ],
      ],
      behoerde: [para.behoerde],
      gueltigVon: [
        para.gueltigVon,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
      gueltigBis: [
        para.gueltigBis,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
    });
  }

  private static specificValidator(typ: string): ValidatorFn {
    switch (typ) {
      case ParameterTypEnum.INTEGER:
        return Validators.pattern('(0|-?[1-9][0-9]*)');
      case ParameterTypEnum.STRING:
        return Validators.nullValidator;
      default:
        return Validators.nullValidator;
    }
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }

  ngOnInit(): void {
    this.parameter = this.route.snapshot.data.parameter;
    this.parameterWerte = this.route.snapshot.data.parameterWerte;
    this.referenzenService.readAllBehoerden().subscribe((value) => {
      this.alleBehoerden = value;
      this.filteredBehoerden = new BehaviorSubject(
        value.filter((v) => v.land === this.benutzerProfil.landNr)
      );
    });
  }

  get isNeu(): boolean {
    return this.parameterWert?.id == null;
  }

  deleteParameterWert(): void {
    this.parameterRestService
      .deleteParameterWert(this.parameterWert.id)
      .subscribe(
        () => {
          const index = this.parameterWerte.findIndex(
            (b) => b.id === this.parameterWert.id
          );
          if (index !== -1) {
            this.parameterWerte.splice(index, 1);
          }
          this.parameterChachingService.updateParameterWert(
            this.parameterWert.pschluessel
          );
          this.closeLoeschenDialog();
        },
        () => {
          this.alertService.setAlert(
            'Parameterwert kann nicht gelöscht werden'
          );
          this.closeLoeschenDialog();
        }
      );
  }

  canDelete(parameterWert: ParameterWertDto): boolean {
    if (parameterWert.land === LandProperties.DEUTSCHLAND.nr) {
      return this.benutzerProfil.canDeleteReferenzBund;
    } else {
      return (
        this.benutzerProfil.canDeleteReferenzLand &&
        (parameterWert.land === this.benutzerProfil.landNr ||
          this.benutzerProfil.isGesamtadmin)
      );
    }
  }

  canWrite(parameterWert: ParameterWertDto): boolean {
    if (parameterWert.land === LandProperties.DEUTSCHLAND.nr) {
      return this.benutzerProfil.canWriteReferenzBund;
    } else {
      return (
        this.benutzerProfil.canWriteReferenzLand &&
        (parameterWert.land === this.benutzerProfil.landNr ||
          this.benutzerProfil.isGesamtadmin)
      );
    }
  }

  canHinzufuegen(): boolean {
    return this.benutzerProfil.canWriteReferenzLand;
  }

  closeLoeschenDialog(): void {
    this.parameterWert = null;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(parameterWert: ParameterWertDto): void {
    this.parameterWert = parameterWert;
    this.loeschenDialog = true;
  }

  closeBearbeitenDialog(): void {
    this.parameterWert = null;
    this.parameterWertForm = null;
    this.bearbeitenDialog = false;
    this.alert = null;
    this.landSubscription.unsubscribe();
  }

  parameterWertHinzufuegen(): void {
    this.openBearbeitenDialog({
      id: null,
      wert: null,
      pschluessel: this.parameter.schluessel,
      gueltigBis: 9999,
      gueltigVon: 2007,
      land: this.benutzerProfil.landNr,
      behoerde: null,
    });
  }

  openBearbeitenDialogKopie(parameterWert: ParameterWertDto): void {
    this.openBearbeitenDialog({
      id: null,
      wert: 'Kopie von ' + parameterWert.wert,
      pschluessel: parameterWert.pschluessel,
      gueltigBis: parameterWert.gueltigBis,
      gueltigVon: parameterWert.gueltigVon,
      land: this.benutzerProfil.landNr,
      behoerde: null,
    });
  }

  filterBehoerden(land: string): void {
    const filtered: BehoerdeListItemDto[] = this.alleBehoerden.filter(
      (b) => lookupLand(b.land) === land
    );

    // Entfernee ausgewählte Behörde wennn sie nicht mehr dem Filter entspricht
    if (
      this.parameterWertForm.controls.behoerde.value &&
      !filtered.find(
        (item) => this.parameterWertForm.controls.behoerde.value.id === item.id
      )
    ) {
      this.parameterWertForm.controls.behoerde.setValue(null);
    }

    this.filteredBehoerden.next(filtered);
  }

  openBearbeitenDialog(parameterWert: ParameterWertDto): void {
    this.parameterWert = parameterWert;
    this.parameterWertForm = ParameterWerteComponent.createForm(
      this.fb,
      this.parameterWert,
      this.parameter.typ
    );

    this.filterBehoerden(this.parameterWertForm.controls.land.value);
    this.landSubscription =
      this.parameterWertForm.controls.land.valueChanges.subscribe((land) => {
        this.filterBehoerden(land);
      });

    this.bearbeitenDialog = true;
  }

  save(): void {
    if (this.parameterWertForm.invalid) {
      this.clrForm.markAsTouched();
      return;
    }
    const formData = this.parameterWertForm.value;
    const parameterWert: ParameterWertDto = {
      ...this.parameterWert,
      ...FormUtil.emptyStringsToNull(formData),
      land: LandProperties[this.parameterWertForm.value.land].nr,
    };

    let response: Observable<ParameterWertDto>;
    if (this.isNeu) {
      response = this.parameterRestService.createParameterWert(parameterWert);
    } else {
      response = this.parameterRestService.updateParameterWert(parameterWert);
    }

    this.parameterChachingService.updateParameterWert(
      parameterWert.pschluessel
    );

    response.subscribe(
      (ref) => {
        if (this.isNeu) {
          this.parameterWerte.push(ref);
        } else {
          const index = this.parameterWerte.indexOf(this.parameterWert);
          this.parameterWerte[index] = ref;
        }
        this.alert = null;
        this.closeBearbeitenDialog();
        this.alertService.clearAlertMessages();
      },
      (err) => {
        this.alert = AlertService.getAlertText(err);
      }
    );
  }

  hasLandSelect(): boolean {
    return this.isNeu && this.benutzerProfil.isGesamtadmin;
  }

  getHelperText(schluessel: string): string {
    if (schluessel == 'que_zuord') {
      return 'Für die Zuordnung der Quellen bitte "BST" oder "Anlage" eintragen.';
    }
    return '';
  }

  navigateBack(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
}
