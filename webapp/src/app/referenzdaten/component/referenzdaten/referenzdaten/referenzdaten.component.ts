import { Component } from '@angular/core';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Component({
  selector: 'app-referenzdaten',
  templateUrl: './referenzdaten.component.html',
  styleUrls: ['./referenzdaten.component.scss'],
})
export class ReferenzdatenComponent {
  constructor(private readonly benutzer: BenutzerProfil) {}

  get canSeeBehoerden(): boolean {
    return this.benutzer.canSeeReferenzLand;
  }
}
