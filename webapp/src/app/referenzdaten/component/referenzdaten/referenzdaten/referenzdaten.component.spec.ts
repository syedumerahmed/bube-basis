import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenzdatenComponent } from './referenzdaten.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';

describe('ReferenzdatenComponent', () => {
  let component: ReferenzdatenComponent;
  let fixture: ComponentFixture<ReferenzdatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReferenzdatenComponent],
      imports: [RouterTestingModule],
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenzdatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
