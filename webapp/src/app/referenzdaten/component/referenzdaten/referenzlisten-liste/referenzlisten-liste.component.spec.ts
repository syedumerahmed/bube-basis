import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenzlistenListeComponent } from './referenzlisten-liste.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockModule, MockPipe, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { HttpClient } from '@angular/common/http';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { BundLandFilterComponent } from '@/referenzdaten/component/referenzdaten/bund-land-filter/bund-land-filter.component';
import { FormsModule } from '@angular/forms';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';

describe('RefernzlistenListeComponent', () => {
  let component: ReferenzlistenListeComponent;
  let fixture: ComponentFixture<ReferenzlistenListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ReferenzlistenListeComponent,
        MockComponents(ImportModalComponent),
        BundLandFilterComponent,
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(FormsModule),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        { provide: HttpClient, useValue: MockService(HttpClient) },
        {
          provide: FilenameDateFormatPipe,
          useValue: MockPipe(FilenameDateFormatPipe),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenzlistenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
