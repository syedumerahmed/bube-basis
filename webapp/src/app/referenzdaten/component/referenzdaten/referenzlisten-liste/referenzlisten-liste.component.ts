import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as fileSaver from 'file-saver';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Event, EventBusService } from '@/base/service/event-bus.service';
import { Subscription } from 'rxjs';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { ReferenzdatenImportAdapterService } from '@/base/service/referenzdaten-import-adapter.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  ExportiereReferenzdatenCommand,
  ReferenzenRestControllerService,
  ReferenzlisteMetadataDto,
} from '@api/referenzdaten';
import {
  angleIcon,
  checkIcon,
  ClarityIcons,
  eyeIcon,
  timesIcon,
  viewListIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(viewListIcon, timesIcon, checkIcon, angleIcon, eyeIcon);
@Component({
  selector: 'app-refernzlisten-liste',
  templateUrl: './referenzlisten-liste.component.html',
  styleUrls: ['./referenzlisten-liste.component.scss'],
})
export class ReferenzlistenListeComponent implements OnInit, OnDestroy {
  public referenzlistenListe: Array<ReferenzlisteMetadataDto>;
  public selected: Array<ReferenzlisteMetadataDto> = [];
  public exportDialogOpen = false;
  public referenzdatenImportModalOpen = false;

  readonly SortOrder = ClrDatagridSortOrder;

  private eventbusSub: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly alertService: AlertService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly eventBus: EventBusService,
    private readonly dateFormat: FilenameDateFormatPipe,
    readonly referenzdatenImportAdapter: ReferenzdatenImportAdapterService,
    private readonly referenzenService: ReferenzenRestControllerService
  ) {}

  ngOnInit(): void {
    this.referenzlistenListe = this.route.snapshot.data.referenzlistenListe;

    this.eventbusSub = this.eventBus
      .on(Event.ReferenzdatenImportFertig)
      .subscribe(() => this.markierungAufheben());
  }

  ngOnDestroy(): void {
    this.eventbusSub.unsubscribe();
  }

  getTextBundeseinheitlich(bundeseinheitlich: boolean): string {
    return bundeseinheitlich ? 'bundeseinheitlich' : 'länderspezifisch';
  }

  hatMarkierung(): boolean {
    return this.selected.length === 0;
  }

  alleMarkieren(): void {
    this.selected = this.referenzlistenListe;
  }

  markierungAufheben(): void {
    this.selected = [];
  }

  openExportDialog(): void {
    this.exportDialogOpen = true;
  }

  closeExportDialog(): void {
    this.exportDialogOpen = false;
  }

  startExport(): void {
    this.exportDialogOpen = false;
    const referenzlisteList: Array<string> = [];
    this.selected.forEach((refliste) => referenzlisteList.push(refliste.name));

    const command: ExportiereReferenzdatenCommand = { referenzlisteList };

    this.referenzenService.exportReferenzen(command).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(new Date());
        fileSaver.saveAs(blob, 'referenzdaten_' + datum + '.xml');
        this.alertService.setSuccess(
          this.selected.length +
            ' Referenzliste' +
            (this.selected.length > 1 ? 'n' : '') +
            ' wurde' +
            (this.selected.length > 1 ? 'n' : '') +
            ' exportiert.'
        );
        this.markierungAufheben();
      },
      () => {
        this.alertService.setAlert(
          'Es ist ein Fehler beim Exportieren der XML-Datei aufgetreten'
        );
      }
    );
  }

  get canExport(): boolean {
    return this.benutzerProfil.canReadReferenzdaten;
  }

  get canImport(): boolean {
    return (
      this.benutzerProfil.canWriteReferenzLand ||
      this.benutzerProfil.canWriteReferenzBund
    );
  }
}
