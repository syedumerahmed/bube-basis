import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClrDatagridFilterInterface } from '@clr/angular';
import { ReplaySubject } from 'rxjs';
import { ReferenzlisteMetadataDto } from '@api/referenzdaten';

@Component({
  selector: 'app-bund-land-filter',
  templateUrl: './bund-land-filter.component.html',
  styleUrls: ['./bund-land-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BundLandFilterComponent
  implements ClrDatagridFilterInterface<ReferenzlisteMetadataDto>
{
  bundChecked = true;
  landChecked = true;

  changes = new ReplaySubject(1);

  accepts(liste: ReferenzlisteMetadataDto): boolean {
    return (
      (liste.bundeseinheitlich && this.bundChecked) ||
      (!liste.bundeseinheitlich && this.landChecked)
    );
  }

  isActive(): boolean {
    return !(this.bundChecked && this.landChecked);
  }

  selectedOptionsChanged(): void {
    this.changes.next();
  }
}
