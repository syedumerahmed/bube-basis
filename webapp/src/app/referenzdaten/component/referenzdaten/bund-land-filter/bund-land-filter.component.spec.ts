import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BundLandFilterComponent } from './bund-land-filter.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('BundLandFilterComponent', () => {
  let component: BundLandFilterComponent;
  let fixture: ComponentFixture<BundLandFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BundLandFilterComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BundLandFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
