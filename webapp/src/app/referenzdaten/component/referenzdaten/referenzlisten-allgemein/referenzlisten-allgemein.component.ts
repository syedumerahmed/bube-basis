import { Component } from '@angular/core';

@Component({
  selector: 'app-referenzlisten-allgemein',
  templateUrl: './referenzlisten-allgemein.component.html',
  styleUrls: ['./referenzlisten-allgemein.component.scss'],
})
export class ReferenzlistenAllgemeinComponent {}
