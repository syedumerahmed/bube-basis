import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenzlistenAllgemeinComponent } from './referenzlisten-allgemein.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ReferenzlistenAllgemeinComponent', () => {
  let component: ReferenzlistenAllgemeinComponent;
  let fixture: ComponentFixture<ReferenzlistenAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ReferenzlistenAllgemeinComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenzlistenAllgemeinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
