import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  FormControl,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { ClrDatagridSortOrder, ClrForm } from '@clr/angular';
import {
  ReferenzenRestControllerService,
  ReferenzlisteMetadataDto,
} from '@api/referenzdaten';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { KommunikationsTypenSteuerung } from '@/basedomain/component/kommunikation/KommunikationsTypenSteuerung';
import { Land, LandProperties, lookupLand } from '@/base/model/Land';
import { Rolle } from '@/base/security/model/Rolle';
import {
  checkIcon,
  ClarityIcons,
  copyIcon,
  eyeIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  viewListIcon,
} from '@cds/core/icon';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';

export interface ReferenzFormData {
  land: string;
  schluessel: string;
  sortier: string;
  ktext: string;
  ltext?: string;
  gueltigVon: number;
  gueltigBis: number;
  eu?: string;
  strg?: string;
}

ClarityIcons.addIcons(
  trashIcon,
  viewListIcon,
  checkIcon,
  timesIcon,
  plusIcon,
  eyeIcon,
  copyIcon
);

@Component({
  selector: 'app-referenzliste',
  templateUrl: './referenzliste.component.html',
  styleUrls: ['./referenzliste.component.scss'],
})
export class ReferenzlisteComponent implements OnInit {
  readonly LandProperties = LandProperties;
  readonly SortOrder = ClrDatagridSortOrder;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private benutzerProfil: BenutzerProfil,
    private referenzenService: ReferenzenRestControllerService,
    private readonly referenzenCacheService: ReferenzdatenService,
    private fb: FormBuilder,
    private alertService: AlertService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  public loeschenDialog: boolean;
  public loeschenMultiDialog: boolean;
  public bearbeitenDialog: boolean;
  public referenzliste: Array<ReferenzDto>;
  public selected: Array<ReferenzDto> = [];
  public referenz: ReferenzDto;
  public referenzlisteMetadata: ReferenzlisteMetadataDto;
  public referenzForm: FormGroup<ReferenzFormData>;
  public alert: string;

  static conditionalValidator(
    param: () => boolean,
    validator: ValidatorFn,
    fehler: ValidationErrors
  ) {
    return (formControl: FormControl): ValidationErrors => {
      if (!formControl.parent) {
        return null;
      }
      if (param() && validator(formControl)) {
        return fehler;
      }
      return null;
    };
  }

  static createForm(
    fb: FormBuilder,
    ref: ReferenzFormData,
    refListName: string
  ): FormGroup<ReferenzFormData> {
    return fb.group<ReferenzFormData>({
      land: [lookupLand(ref.land), Validators.required],
      schluessel: [
        ref.schluessel,
        [
          Validators.required,
          Validators.maxLength(50),
          ReferenzlisteComponent.conditionalValidator(
            () => refListName === 'RJAHR',
            Validators.pattern('(0|-?[1-9][0-9]*)'),
            { jahr: 'jahr' }
          ),
        ],
      ],
      sortier: [ref.sortier, [Validators.required, Validators.maxLength(50)]],
      ktext: [ref.ktext, [Validators.required, Validators.maxLength(500)]],
      ltext: [ref.ltext, Validators.maxLength(1000)],
      gueltigVon: [
        ref.gueltigVon,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
      gueltigBis: [
        ref.gueltigBis,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
      eu: [ref.eu, Validators.maxLength(500)],
      strg: [ref.strg, Validators.maxLength(20)],
    });
  }

  ngOnInit(): void {
    this.referenzliste = this.route.snapshot.data.referenzliste;
    this.referenzlisteMetadata = this.route.snapshot.data.referenzlisteMetadata;
  }

  alleMarkieren(): void {
    this.selected = this.referenzliste;
  }

  markierungAufheben(): void {
    this.selected = [];
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  get isNeu(): boolean {
    return this.referenz?.id == null;
  }

  deleteReferenz(): void {
    this.referenzenService.deleteReferenz(this.referenz.id).subscribe(
      () => {
        const index = this.referenzliste.findIndex(
          (b) => b.id === this.referenz.id
        );
        if (index !== -1) {
          this.referenzliste.splice(index, 1);
        }
        this.referenzenCacheService.invalidateReferenzListeCache();
        this.closeLoeschenDialog();
      },
      (error) => {
        this.alertService.setAlert(error);
        this.closeLoeschenDialog();
      }
    );
  }

  canDeleteReferenz(referenz: ReferenzDto): boolean {
    if (referenz.land === LandProperties.DEUTSCHLAND.nr) {
      return this.benutzerProfil.canDeleteReferenzBund;
    } else {
      return (
        this.benutzerProfil.canDeleteReferenzLand &&
        (referenz.land === this.benutzerProfil.landNr ||
          this.benutzerProfil.isGesamtadmin)
      );
    }
  }

  get canDeleteSelection(): boolean {
    if (this.nothingSelected) {
      return false;
    }
    return this.selected.every((referenz) => this.canDeleteReferenz(referenz));
  }

  canWrite(referenz: ReferenzDto): boolean {
    if (referenz.land === LandProperties.DEUTSCHLAND.nr) {
      return this.benutzerProfil.canWriteReferenzBund;
    } else {
      return (
        this.benutzerProfil.canWriteReferenzLand &&
        (referenz.land === this.benutzerProfil.landNr ||
          this.benutzerProfil.isGesamtadmin)
      );
    }
  }

  get canHinzufuegen(): boolean {
    return (
      this.benutzerProfil.canWriteReferenzBund ||
      (this.benutzerProfil.canWriteReferenzLand &&
        !this.referenzlisteMetadata.bundeseinheitlich)
    );
  }

  get canDelete(): boolean {
    if (this.referenzlisteMetadata.bundeseinheitlich) {
      return this.benutzerProfil.canDeleteReferenzBund;
    } else {
      return this.benutzerProfil.canDeleteReferenzLand;
    }
  }

  closeLoeschenDialog(): void {
    this.referenz = null;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(referenz: ReferenzDto): void {
    this.referenz = referenz;
    this.loeschenDialog = true;
  }

  closeBearbeitenDialog(): void {
    this.referenz = null;
    this.referenzForm = null;
    this.bearbeitenDialog = false;
    this.alert = null;
  }

  referenzHinzufuegen(): void {
    this.openBearbeitenDialog({
      id: null,
      ktext: null,
      schluessel: null,
      sortier: null,
      gueltigBis: 9999,
      gueltigVon: 2007,
      land: this.getLandNr(),
      referenzliste: this.referenzlisteMetadata.name,
    });
  }

  openBearbeitenDialogKopie(referenz: ReferenzDto): void {
    this.openBearbeitenDialog({
      id: null,
      ktext: 'Kopie von ' + referenz.ktext,
      ltext: 'Kopie von ' + referenz.ltext,
      schluessel: null,
      sortier: referenz.sortier,
      gueltigBis: referenz.gueltigBis,
      gueltigVon: referenz.gueltigVon,
      land: referenz.land,
      referenzliste: this.referenzlisteMetadata.name,
      eu: 'Kopie von ' + (referenz.eu || ''),
      strg: referenz.strg,
    });
  }

  getLandNr(): string {
    // Wenn bundeseinheitlich dann kann nur Deutschland
    if (this.referenzlisteMetadata.bundeseinheitlich) {
      return LandProperties.DEUTSCHLAND.nr;
    }
    // Wenn länderspezifisch aber ich habe nicht das Land Recht
    // dann kann es nur Deutschland sein
    if (!this.benutzerProfil.canWriteReferenzLand) {
      return LandProperties.DEUTSCHLAND.nr;
    }
    return this.benutzerProfil.landNr;
  }

  openBearbeitenDialog(referenz: ReferenzDto): void {
    this.referenz = referenz;
    this.referenzForm = ReferenzlisteComponent.createForm(
      this.fb,
      this.referenz,
      this.referenz.referenzliste
    );
    this.bearbeitenDialog = true;
  }

  save(): void {
    if (this.referenzForm.invalid) {
      this.clrForm.markAsTouched();
      return;
    }
    const formData = this.referenzForm.value;
    const referenz: ReferenzDto = {
      ...this.referenz,
      ...FormUtil.emptyStringsToNull(formData),
      land: LandProperties[this.referenzForm.value.land].nr,
    };

    let response: Observable<ReferenzDto>;
    if (this.isNeu) {
      response = this.referenzenService.createReferenz(referenz);
    } else {
      response = this.referenzenService.updateReferenz(referenz);
    }

    response.subscribe(
      (ref) => {
        if (this.isNeu) {
          this.referenzliste.push(ref);
        } else {
          const index = this.referenzliste.indexOf(this.referenz);
          this.referenzliste[index] = ref;
        }
        this.alert = null;
        this.referenzenCacheService.invalidateBehoerdenCache();
        this.closeBearbeitenDialog();
        this.alertService.clearAlertMessages();
      },
      (err) => {
        this.alert = AlertService.getAlertText(err);
      }
    );
  }

  get steuerungsHinweis(): string {
    switch (this.referenzlisteMetadata.name) {
      case ReferenzlisteEnum.RKOM:
        return `Validierungstyp der Kommunikationsverbindung. Auswahl zwischen ${Object.values(
          KommunikationsTypenSteuerung
        ).join(', ')}.`;
      case ReferenzlisteEnum.RVORS:
        return `Referenzliste der möglichen Tätigkeiten. Beispiel: ${ReferenzlisteEnum.R4BV}`;
      default:
        return 'Steuert die Anzeige in Formularen oder andere Aktionen';
    }
  }

  hasLandSelect(): boolean {
    return (
      this.isNeu &&
      !this.referenzlisteMetadata.bundeseinheitlich &&
      this.benutzerProfil.hasRolle(Rolle.REFERENZLISTEN_BUND_WRITE)
    );
  }

  get selectableLaender():
    | Map<string, { nr: string; display: string }>
    | Record<string, { nr: string; display: string }> {
    if (this.benutzerProfil.isGesamtadmin) {
      return LandProperties;
    }
    const laender: Array<string> = [];
    if (this.benutzerProfil.canWriteReferenzLand) {
      laender.push(this.benutzerProfil.land);
    }
    if (this.benutzerProfil.canWriteReferenzBund) {
      laender.push(Land.DEUTSCHLAND);
    }
    return new Map(
      Object.entries(LandProperties).filter((land) => laender.includes(land[0]))
    );
  }

  navigateBackToReferenzlisten(): void {
    this.router.navigate(['../..'], { relativeTo: this.route });
  }

  deleteMultipleReferenzen(): void {
    this.referenzenService
      .deleteMultipleReferenzen(this.selected.map((b) => b.id))
      .subscribe(
        () => {
          this.referenzliste = this.referenzliste.filter(
            (b) => !this.selected.includes(b)
          );
          this.selected = [];
          this.loeschenMultiDialog = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loeschenMultiDialog = false;
        }
      );
  }
}
