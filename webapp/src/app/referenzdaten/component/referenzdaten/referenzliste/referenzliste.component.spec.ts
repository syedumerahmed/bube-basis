import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ReferenzlisteComponent } from './referenzliste.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent, MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@/base/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import {
  ReferenzenRestControllerService,
  ReferenzlisteMetadataDto,
} from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('ReferenzlisteComponent', () => {
  let component: ReferenzlisteComponent;
  let fixture: ComponentFixture<ReferenzlisteComponent>;

  const referenzen: Array<ReferenzDto> = [
    {
      id: 1,
      gueltigBis: 9999,
      gueltigVon: 1900,
      ktext: 'ktext',
      land: '00',
      referenzliste: ReferenzlisteEnum.R4BV,
      schluessel: 'schluessel1',
      sortier: '1000',
      letzteAenderung: null,
    },
    {
      id: 2,
      gueltigBis: 9999,
      gueltigVon: 1900,
      ktext: 'ktext2',
      land: '02',
      referenzliste: ReferenzlisteEnum.R4BV,
      schluessel: 'schluessel2',
      sortier: '2000',
      letzteAenderung: null,
    },
  ] as Array<ReferenzDto>;

  const referenz: ReferenzDto = referenzen[0];

  const liste: ReferenzlisteMetadataDto = {
    name: ReferenzlisteEnum.R4BV,
    ktext: '',
    ltext: '',
    bundeseinheitlich: true,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MockComponent(TooltipComponent), ReferenzlisteComponent],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        { provide: AlertService, useValue: MockService(AlertService) },
        {
          provide: ReferenzenRestControllerService,
          useValue: MockService(ReferenzenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: { referenzliste: referenzen, referenzlisteMetadata: liste },
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(ReferenzlisteComponent);
    component = fixture.componentInstance;
    component.referenzForm = ReferenzlisteComponent.createForm(
      formBuilder,
      referenz,
      referenz.referenzliste
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.referenzForm.pristine).toBe(true);
  });

  describe('canDeleteSelection', () => {
    it('should be false if nothing is selected', () => {
      component.selected = [];
      expect(component.canDeleteSelection).toBe(false);
    });
    describe('with selection', () => {
      beforeEach(() => {
        component.selected = referenzen;
      });

      it('should be false if canDelete returns false', () => {
        spyOn(component, 'canDeleteReferenz').mockReturnValue(false);
        expect(component.canDeleteSelection).toBe(false);
      });

      it('should be true if canDelete returns true', () => {
        spyOn(component, 'canDeleteReferenz').mockReturnValue(true);
        expect(component.canDeleteSelection).toBe(true);
      });
    });
  });

  describe('canDeleteReferenz', () => {
    describe('with Bundesreferenz', () => {
      const bundesReferenz = referenzen[0];
      it('should be false if canDeleteReferenzBund is false', inject(
        [BenutzerProfil],
        (benutzerProfil: BenutzerProfil) => {
          jest
            .spyOn(benutzerProfil, 'canDeleteReferenzBund', 'get')
            .mockReturnValue(false);
          expect(component.canDeleteReferenz(bundesReferenz)).toBe(false);
        }
      ));

      it('should be true if canDeleteReferenzBund is true', inject(
        [BenutzerProfil],
        (benutzerProfil: BenutzerProfil) => {
          jest
            .spyOn(benutzerProfil, 'canDeleteReferenzBund', 'get')
            .mockReturnValue(true);
          expect(component.canDeleteReferenz(bundesReferenz)).toBe(true);
        }
      ));
    });

    describe('with Landesreferenz', () => {
      const landesReferenz = referenzen[1];
      it('should be false if user has no delete rights', inject(
        [BenutzerProfil],
        (benutzerProfil: BenutzerProfil) => {
          jest
            .spyOn(benutzerProfil, 'canDeleteReferenzLand', 'get')
            .mockReturnValue(false);
          expect(component.canDeleteReferenz(landesReferenz)).toBe(false);
        }
      ));

      describe('user has delete rights', () => {
        beforeEach(inject(
          [BenutzerProfil],
          (benutzerProfil: BenutzerProfil) => {
            jest
              .spyOn(benutzerProfil, 'canDeleteReferenzLand', 'get')
              .mockReturnValue(true);
          }
        ));

        it('should be true', inject([BenutzerProfil], () => {
          expect(component.canDeleteReferenz(landesReferenz)).toBe(true);
        }));

        it('should be false when benutzer land not referenzland', inject(
          [BenutzerProfil],
          (benutzerProfil: BenutzerProfil) => {
            jest
              .spyOn(benutzerProfil, 'isGesamtadmin', 'get')
              .mockReturnValue(false);
            jest.spyOn(benutzerProfil, 'landNr', 'get').mockReturnValue('00');

            expect(component.canDeleteReferenz(landesReferenz)).toBe(false);
          }
        ));

        it('should be true when benutzer is gesamtadmin', inject(
          [BenutzerProfil],
          (benutzerProfil: BenutzerProfil) => {
            jest
              .spyOn(benutzerProfil, 'isGesamtadmin', 'get')
              .mockReturnValue(true);
            jest.spyOn(benutzerProfil, 'landNr', 'get').mockReturnValue('00');

            expect(component.canDeleteReferenz(landesReferenz)).toBe(true);
          }
        ));
      });
    });
  });

  describe('delete referenz', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        spyOn(referenzenService, 'deleteReferenz').mockReturnValue(of(null));
        component.openLoeschenDialog(referenz);
        component.deleteReferenz();
      }
    ));

    it('should call service', inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        expect(referenzenService.deleteReferenz).toHaveBeenCalledWith(1);
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove behoerde from list', () => {
      expect(component.referenzliste).not.toContainEqual(referenz);
    });
  });

  describe('delete referenz unsuccessful', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService) => {
        spyOn(referenzenService, 'deleteReferenz').mockReturnValue(
          throwError({ error: 'error' })
        );
        component.openLoeschenDialog(referenz);
        component.deleteReferenz();
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should not remove anything from list', () => {
      expect(component.referenzliste).toEqual(referenzen);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalledTimes(1);
      }
    ));
  });

  describe('delete multiple referenzen', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        spyOn(referenzenService, 'deleteMultipleReferenzen').mockReturnValue(
          of(null)
        );
        component.selected = component.referenzliste.slice(1, 2);
        component.loeschenMultiDialog = true;
        component.deleteMultipleReferenzen();
      }
    ));

    it('should call service', inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        expect(referenzenService.deleteMultipleReferenzen).toHaveBeenCalledWith(
          [2]
        );
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenMultiDialog).toBe(false);
    });

    it('should remove behoerden from list', () => {
      expect(component.referenzliste).not.toContainEqual(referenzen[1]);
    });
  });

  describe('delete referenzen unsuccessful', () => {
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenService: ReferenzenRestControllerService) => {
        spyOn(referenzenService, 'deleteMultipleReferenzen').mockReturnValue(
          throwError({ error: 'error' })
        );
        component.selected = component.referenzliste.slice(1, 2);
        component.loeschenMultiDialog = true;
        component.deleteMultipleReferenzen();
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenMultiDialog).toBe(false);
    });

    it('should not remove anything from list', () => {
      expect(component.referenzliste).toEqual(referenzen);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalled();
      }
    ));
  });
});
