import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClrDatagridSortOrder } from '@clr/angular';
import { ParameterDto } from '@api/referenzdaten';
import { ClarityIcons, eyeIcon, viewListIcon } from '@cds/core/icon';

ClarityIcons.addIcons(viewListIcon, eyeIcon);
@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.scss'],
})
export class ParameterComponent implements OnInit {
  public parameterliste: Array<ParameterDto>;
  readonly SortOrder = ClrDatagridSortOrder;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.parameterliste = this.route.snapshot.data.parameterliste;
  }

  isFuerBehoerde(fuerBehoerden: boolean): string {
    return fuerBehoerden ? 'Ja' : 'Nein';
  }
}
