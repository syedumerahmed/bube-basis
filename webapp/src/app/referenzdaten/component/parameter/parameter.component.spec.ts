import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterComponent } from './parameter.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent, MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { DatagridBooleanFilterComponent } from '@/base/component/datagrid-boolean-filter/datagrid-boolean-filter.component';

describe('ParameterComponent', () => {
  let component: ParameterComponent;
  let fixture: ComponentFixture<ParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ParameterComponent,
        MockComponent(DatagridBooleanFilterComponent),
      ],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
