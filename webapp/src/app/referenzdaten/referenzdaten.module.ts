import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BaseModule } from '@/base/base.module';
import { ReferenzdatenRoutingModule } from '@/referenzdaten/routing/referenzdaten-routing.module';
import { ReferenzlistenListeComponent } from './component/referenzdaten/referenzlisten-liste/referenzlisten-liste.component';
import { ReferenzdatenComponent } from './component/referenzdaten/referenzdaten/referenzdaten.component';
import { ParameterComponent } from './component/parameter/parameter.component';
import { ReferenzlisteComponent } from './component/referenzdaten/referenzliste/referenzliste.component';
import { ReferenzlistenAllgemeinComponent } from './component/referenzdaten/referenzlisten-allgemein/referenzlisten-allgemein.component';
import { ParameterWerteComponent } from './component/parameter-werte/parameter-werte.component';
import { BehoerdenListeComponent } from './component/behoerden/behoerden-liste/behoerden-liste.component';
import { BehoerdenDetailsComponent } from './component/behoerden/behoerden-details/behoerden-details.component';
import { BehoerdeAllgemeinComponent } from './component/behoerden/behoerde-allgemein/behoerde-allgemein.component';
import { BundLandFilterComponent } from './component/referenzdaten/bund-land-filter/bund-land-filter.component';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { BasedomainModule } from '@/basedomain/basedomain.module';

@NgModule({
  declarations: [
    ReferenzlistenListeComponent,
    ReferenzdatenComponent,
    ParameterComponent,
    ReferenzlisteComponent,
    ReferenzlistenAllgemeinComponent,
    ParameterWerteComponent,
    BundLandFilterComponent,
    BehoerdenListeComponent,
    BehoerdenDetailsComponent,
    BehoerdeAllgemeinComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    BaseModule,
    BasedomainModule,
    ReferenzdatenRoutingModule,
  ],
  providers: [FilenameDateFormatPipe],
})
export class ReferenzdatenModule {}
