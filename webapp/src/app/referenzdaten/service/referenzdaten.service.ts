import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount, take } from 'rxjs/operators';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import { ReferenzenRestControllerService } from '@api/referenzdaten';

@Injectable({
  providedIn: 'root',
})
export class ReferenzdatenService {
  private readonly referenzenCache = new Map<
    string,
    Observable<Array<ReferenzDto>>
  >();
  private readonly behoerdenCache = new Map<
    string,
    Observable<Array<BehoerdeListItem>>
  >();

  constructor(private readonly controller: ReferenzenRestControllerService) {}

  readReferenzliste(
    name: ReferenzlisteEnum,
    jahr: number,
    land: string
  ): Observable<Array<ReferenzDto>> {
    const key = `${name}-${jahr}-${land}`;
    if (!this.referenzenCache.has(key)) {
      const replaySubject = this.controller
        .readReferenzliste(name, jahr, land)
        .pipe(publishReplay(1), refCount(), take(1));
      this.referenzenCache.set(key, replaySubject);
    }
    return this.referenzenCache.get(key);
  }

  readBehoerden(
    jahr: number,
    land?: string
  ): Observable<Array<BehoerdeListItem>> {
    const key = `${jahr}-${land}`;
    if (!this.behoerdenCache.has(key)) {
      const replaySubject = this.controller.readBehoerden(jahr, land).pipe(
        map((list) => list.map((b) => new BehoerdeListItem(b))),
        publishReplay(1),
        refCount(),
        take(1)
      );
      this.behoerdenCache.set(key, replaySubject);
    }
    return this.behoerdenCache.get(key);
  }

  invalidateReferenzListeCache(): void {
    this.referenzenCache.clear();
  }

  invalidateBehoerdenCache(): void {
    this.behoerdenCache.clear();
  }
}
