import { fakeAsync, inject, TestBed } from '@angular/core/testing';
import { ReferenzdatenService } from './referenzdaten.service';
import { MockProvider } from 'ng-mocks';
import { Subject } from 'rxjs';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import spyOn = jest.spyOn;
import Mocked = jest.Mocked;

describe('ReferenzdatenService', () => {
  let service: ReferenzdatenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockProvider(ReferenzenRestControllerService)],
    });
    service = TestBed.inject(ReferenzdatenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Referenzen on first call', () => {
    let response$: Subject<any>;
    let subscriber: jest.Mock;

    beforeEach(fakeAsync(
      inject(
        [ReferenzenRestControllerService],
        (controller: ReferenzenRestControllerService) => {
          response$ = new Subject<any>();
          spyOn(controller, 'readReferenzliste').mockReturnValue(
            response$.asObservable()
          );

          subscriber = jest.fn();
          service
            .readReferenzliste(ReferenzlisteEnum.RVORS, 1999, 'DE')
            .subscribe(subscriber);
        }
      )
    ));

    it('should call controller', inject(
      [ReferenzenRestControllerService],
      (controller: ReferenzenRestControllerService) => {
        expect(controller.readReferenzliste).toHaveBeenCalledTimes(1);
        expect(controller.readReferenzliste).toHaveBeenCalledWith(
          ReferenzlisteEnum.RVORS,
          1999,
          'DE'
        );
      }
    ));

    describe('on response', () => {
      let response: Array<ReferenzDto>;
      beforeEach(fakeAsync(() => {
        response = [
          {
            id: 852,
            land: 'DE',
            referenzliste: ReferenzlisteEnum.RVORS,
            schluessel: '05',
            sortier: '05',
            ktext: 'ktext',
            ltext: 'ltext',
            gueltigVon: 1980,
            gueltigBis: 2011,
            letzteAenderung: '1999-08-12',
            eu: 'eu',
            strg: 'strg',
          },
          {
            id: 853,
            land: 'DE',
            referenzliste: ReferenzlisteEnum.RVORS,
            schluessel: '06',
            sortier: '06',
            ktext: 'ktext',
            ltext: 'ltext',
            gueltigVon: 1980,
            gueltigBis: 2011,
            letzteAenderung: '1999-08-12',
            eu: 'eu',
            strg: 'strg',
          },
        ];
        response$.next(response);
      }));

      it('should emit', () => {
        expect(subscriber).toHaveBeenCalledWith(response);
      });

      it('should cache result for second subscriber', inject(
        [ReferenzenRestControllerService],
        (controller: Mocked<ReferenzenRestControllerService>) => {
          const anotherSubscriber = jest.fn();
          service
            .readReferenzliste(ReferenzlisteEnum.RVORS, 1999, 'DE')
            .subscribe(anotherSubscriber);

          expect(anotherSubscriber).toHaveBeenCalledWith(response);
          expect(controller.readReferenzliste).toHaveBeenCalledTimes(1);
        }
      ));

      it('should not cache different args', inject(
        [ReferenzenRestControllerService],
        (controller: Mocked<ReferenzenRestControllerService>) => {
          const anotherSubscriber = jest.fn();
          service
            .readReferenzliste(ReferenzlisteEnum.RVORS, 2000, 'DE')
            .subscribe(anotherSubscriber);

          expect(anotherSubscriber).not.toHaveBeenCalled();
          expect(controller.readReferenzliste).toHaveBeenNthCalledWith(
            2,
            ReferenzlisteEnum.RVORS,
            2000,
            'DE'
          );
          response$.next(response);
          expect(anotherSubscriber).toHaveBeenCalledWith(response);
        }
      ));
    });
  });
});
