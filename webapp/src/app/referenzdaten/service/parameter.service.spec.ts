import { fakeAsync, inject, TestBed } from '@angular/core/testing';

import { ParameterService } from './parameter.service';
import { MockProviders } from 'ng-mocks';
import { Subject } from 'rxjs';
import {
  ParameterDto,
  ParameterRestControllerService,
} from '@api/referenzdaten';
import spyOn = jest.spyOn;
import Mocked = jest.Mocked;

describe('ParameterService', () => {
  let service: ParameterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockProviders(ParameterRestControllerService)],
    });
    service = TestBed.inject(ParameterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Parameter on first call', () => {
    let response$: Subject<any>;
    let subscriber: jest.Mock;

    beforeEach(fakeAsync(
      inject(
        [ParameterRestControllerService],
        (controller: ParameterRestControllerService) => {
          response$ = new Subject<any>();
          spyOn(controller, 'readParameterliste').mockReturnValue(
            response$.asObservable()
          );

          subscriber = jest.fn();
          service.readParameterliste().subscribe(subscriber);
        }
      )
    ));

    it('should call controller', inject(
      [ParameterRestControllerService],
      (controller: ParameterRestControllerService) => {
        expect(controller.readParameterliste).toHaveBeenCalledTimes(1);
      }
    ));

    describe('on response', () => {
      let response: Array<ParameterDto>;
      beforeEach(fakeAsync(() => {
        response = [
          {
            typ: 'STRING',
            schluessel: 'schluessel-1',
            text: 'text 1',
          },
          {
            typ: 'INTEGER',
            schluessel: 'schluessel-2',
            text: '1',
          },
        ];
        response$.next(response);
      }));

      it('should emit', () => {
        expect(subscriber).toHaveBeenCalledWith(response);
      });

      it('should cache result for second subscriber', inject(
        [ParameterRestControllerService],
        (controller: Mocked<ParameterRestControllerService>) => {
          const anotherSubscriber = jest.fn();
          service.readParameterliste().subscribe(anotherSubscriber);

          expect(anotherSubscriber).toHaveBeenCalledWith(response);
          expect(controller.readParameterliste).toHaveBeenCalledTimes(1);
        }
      ));
    });
  });
});
