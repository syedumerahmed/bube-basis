import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { publishReplay, refCount, take } from 'rxjs/operators';
import {
  ParameterDto,
  ParameterRestControllerService,
  ParameterWertDto,
} from '@api/referenzdaten';

@Injectable({
  providedIn: 'root',
})
export class ParameterService {
  private parameterListeCache: Observable<Array<ParameterDto>>;
  private parameterCache = new Map<string, Observable<ParameterDto>>();

  private parameterForEditCache = new Map<
    string,
    Observable<Array<ParameterWertDto>>
  >();
  private parameterWertOptionalCache = new Map<
    string,
    Observable<ParameterWertDto>
  >();
  private parameterWertBehoerdeOptionalCache = new Map<
    string,
    Observable<ParameterWertDto>
  >();
  private parameterWertCache = new Map<string, Observable<ParameterWertDto>>();

  constructor(private controller: ParameterRestControllerService) {}

  readParameterliste(): Observable<Array<ParameterDto>> {
    if (!this.parameterListeCache) {
      this.parameterListeCache = this.controller
        .readParameterliste()
        .pipe(publishReplay(1), refCount(), take(1));
    }
    return this.parameterListeCache;
  }

  readParameter(schluessel: string): Observable<ParameterDto> {
    if (!this.parameterCache.has(schluessel)) {
      const replaySubject = this.controller
        .readParameter(schluessel)
        .pipe(publishReplay(1), refCount(), take(1));
      this.parameterCache.set(schluessel, replaySubject);
    }
    return this.parameterCache.get(schluessel);
  }

  readParameterwerteForEdit(
    schluessel: string
  ): Observable<Array<ParameterWertDto>> {
    if (!this.parameterCache.has(schluessel)) {
      const replaySubject = this.controller
        .readParameterwerteForEdit(schluessel)
        .pipe(publishReplay(1), refCount(), take(1));
      this.parameterForEditCache.set(schluessel, replaySubject);
    }
    return this.parameterForEditCache.get(schluessel);
  }

  readParameterWertOptional(
    schluessel: string,
    jahr: string,
    land: string
  ): Observable<ParameterWertDto> {
    const key = `${schluessel}-${jahr}-${land}`;
    if (!this.parameterWertOptionalCache.has(key)) {
      const replaySubject = this.controller
        .readParameterWertOptional(schluessel, jahr, land)
        .pipe(publishReplay(1), refCount(), take(1));
      this.parameterWertOptionalCache.set(key, replaySubject);
    }
    return this.parameterWertOptionalCache.get(key);
  }

  readParameterWertBehoerdeOptional(
    schluessel: string,
    jahr: string,
    land: string,
    behoerdeId: number
  ): Observable<ParameterWertDto> {
    const key = `${schluessel}-${jahr}-${land}-${behoerdeId}`;
    if (!this.parameterWertBehoerdeOptionalCache.has(key)) {
      const replaySubject = this.controller
        .readParameterWertBehoerdeOptional(schluessel, jahr, land, behoerdeId)
        .pipe(publishReplay(1), refCount(), take(1));
      this.parameterWertOptionalCache.set(key, replaySubject);
    }
    return this.parameterWertOptionalCache.get(key);
  }

  readParameterWert(
    schluessel: string,
    jahr: string,
    land: string
  ): Observable<ParameterWertDto> {
    const key = `${schluessel}-${jahr}-${land}`;
    if (!this.parameterWertCache.has(key)) {
      const replaySubject = this.controller
        .readParameterWert(schluessel, jahr, land)
        .pipe(publishReplay(1), refCount(), take(1));
      this.parameterWertCache.set(key, replaySubject);
    }
    return this.parameterWertCache.get(key);
  }

  updateParameterWert(schluessel: string): void {
    const replaySubject = this.controller
      .readParameterwerteForEdit(schluessel)
      .pipe(publishReplay(1), refCount(), take(1));
    this.parameterForEditCache.set(schluessel, replaySubject);
  }
}
