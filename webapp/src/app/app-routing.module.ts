import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutingNotFoundComponent } from '@/base/component/routing-not-found/routing-not-found.component';
import { EmailGuard } from '@/benutzerverwaltung/routing/guard/email.guard';
import { UpdateProfileComponent } from '@/app/component/update-profile/update-profile.component';
import { UpdateProfileGuard } from '@/benutzerverwaltung/routing/guard/update-profile.guard';
import { EuregistryGuard } from '@/euregistry/routing/guard/euregistry.guard';
import { StammdatenGuard } from '@/stammdaten/routing/guard/stammdaten.guard';
import { BenutzerverwaltungGuard } from '@/benutzerverwaltung/routing/guard/benutzerverwaltung.guard';
import { ReferenzdatenGuard } from '@/referenzdaten/routing/guard/referenzdaten.guard';
import { KomplexpruefungGuard } from '@/komplexpruefung/routing/guard/komplexpruefung.guard';
import { StammdatenbetreiberGuard } from '@/stammdatenbetreiber/routing/guard/stammdatenbetreiber.guard';
import { RedirectGuard } from '@/stammdaten/routing/guard/redirect.guard';

const routes: Routes = [
  {
    path: 'update-profile',
    canActivate: [UpdateProfileGuard],
    component: UpdateProfileComponent,
  },
  {
    path: '',
    canActivate: [EmailGuard, RedirectGuard],
    children: [
      {
        path: 'stammdaten',
        loadChildren: () =>
          import('./stammdaten/stammdaten.module').then(
            (m) => m.StammdatenModule
          ),
        canActivate: [StammdatenGuard],
      },
      {
        path: 'stammdatenbetreiber',
        loadChildren: () =>
          import('./stammdatenbetreiber/stammdatenbetreiber.module').then(
            (m) => m.StammdatenbetreiberModule
          ),
        canActivate: [StammdatenbetreiberGuard],
      },
      {
        path: 'benutzerverwaltung',
        loadChildren: () =>
          import('./benutzerverwaltung/benutzerverwaltung.module').then(
            (m) => m.BenutzerverwaltungModule
          ),
        canActivate: [BenutzerverwaltungGuard],
      },
      {
        path: 'referenzdaten',
        loadChildren: () =>
          import('./referenzdaten/referenzdaten.module').then(
            (m) => m.ReferenzdatenModule
          ),
        canActivate: [ReferenzdatenGuard],
      },
      {
        path: 'eureg',
        loadChildren: () =>
          import('./euregistry/euregistry.module').then(
            (m) => m.EuregistryModule
          ),
        canActivate: [EuregistryGuard],
      },
      {
        path: 'komplexpruefung',
        loadChildren: () =>
          import('./komplexpruefung/komplexpruefung.module').then(
            (m) => m.KomplexpruefungModule
          ),
        canActivate: [KomplexpruefungGuard],
      },
      { path: '**', component: RoutingNotFoundComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      paramsInheritanceStrategy: 'always',
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
