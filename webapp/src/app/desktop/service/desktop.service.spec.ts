import { TestBed } from '@angular/core/testing';

import { DesktopService } from './desktop.service';
import { AlertService } from '@/base/service/alert.service';
import { MockService } from 'ng-mocks';

describe('DesktopService', () => {
  let service: DesktopService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AlertService, useValue: MockService(AlertService) },
        { provide: DesktopService, useValue: MockService(DesktopService) },
      ],
    });
    service = TestBed.inject(DesktopService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
