import { Injectable } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { Subject } from 'rxjs';
import {
  DesktopRestControllerService,
  SpeichereItemsAufDesktop,
} from '@api/desktop';

@Injectable({
  providedIn: 'root',
})
export class DesktopService {
  private treeNodesChanged = new Subject<void>();

  treeNodesChanged$ = this.treeNodesChanged.asObservable();

  constructor(
    private readonly alertService: AlertService,
    private readonly desktopRestService: DesktopRestControllerService
  ) {}

  public inDesktopSpeichern(
    desktopTyp: 'STAMMDATEN' | 'EUREG' | 'SPB_STAMMDATEN',
    ids: Array<number>,
    jahrSchluessel: string
  ): void {
    const command: SpeichereItemsAufDesktop = {
      ids,
      jahrSchluessel,
    };

    this.desktopRestService.saveDesktopItems(desktopTyp, command).subscribe(
      () => {
        this.treeNodesChanged.next();
      },
      (err) => {
        this.alertService.setAlert(err);
      }
    );
  }
}
