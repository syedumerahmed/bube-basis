import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehoerdeFilterComponent } from './behoerde-filter.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { FormsModule } from '@angular/forms';
import spyOn = jest.spyOn;
import { BenutzerDto, DatenberechtigungDto } from '@api/benutzer';

describe('BehoerdeFilterComponent', () => {
  let component: BehoerdeFilterComponent;
  let fixture: ComponentFixture<BehoerdeFilterComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
      declarations: [BehoerdeFilterComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehoerdeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      spyOn(component.changes, 'next').mockImplementation();
      component.ngOnInit();
    });

    it('should notify about changes', () => {
      expect(component.changes.next).toHaveBeenCalledTimes(1);
    });
  });

  describe('accept', () => {
    const benutzer: BenutzerDto = {
      datenberechtigung: {
        land: '02',
        behoerden: ['01', '07'],
      } as Partial<DatenberechtigungDto> as DatenberechtigungDto,
    } as Partial<BenutzerDto> as BenutzerDto;

    it('should be true if benutzer matches any selected item', () => {
      component.selectedItems = [
        { schluessel: '01', land: '02' },
        { schluessel: '01', land: '01' },
        { schluessel: '02', land: '02' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];

      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if benutzer matches all selected item', () => {
      component.selectedItems = [
        { schluessel: '01', land: '02' },
        { schluessel: '07', land: '02' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];

      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if no item is selected', () => {
      component.selectedItems = [];
      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if no item is selected and no datenberechtigung is set', () => {
      component.selectedItems = [];
      expect(component.accepts({} as Partial<BenutzerDto> as BenutzerDto)).toBe(
        true
      );
    });

    it('should be true if selected items are undefined', () => {
      component.selectedItems = undefined;
      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be false if schluessel not matched', () => {
      component.selectedItems = [
        { schluessel: '02', land: '02' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];

      expect(component.accepts(benutzer)).toBe(false);
    });

    it('should be false if land not matched', () => {
      component.selectedItems = [
        { schluessel: '07', land: '03' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];

      expect(component.accepts(benutzer)).toBe(false);
    });

    it('should be false if no selected item is matched', () => {
      component.selectedItems = [
        { schluessel: '08' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];

      expect(component.accepts({} as Partial<BenutzerDto> as BenutzerDto)).toBe(
        false
      );
    });
  });

  describe('selectedOptionsChanged', () => {
    beforeEach(() => {
      spyOn(component.changes, 'next').mockImplementation();
      component.selectedOptionsChanged();
    });

    it('should notify about changes', () => {
      expect(component.changes.next).toHaveBeenCalledTimes(1);
    });
  });

  describe('active', () => {
    it('should be true if there are selected items', () => {
      component.selectedItems = [
        { schluessel: '08' },
      ] as Partial<BehoerdeListItem>[] as BehoerdeListItem[];
      expect(component.isActive()).toBe(true);
    });

    it('should be false if selectedItems are empty', () => {
      component.selectedItems = [];
      expect(component.isActive()).toBe(false);
    });
  });
});
