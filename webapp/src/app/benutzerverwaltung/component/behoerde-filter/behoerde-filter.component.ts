import { Component, Input, OnInit } from '@angular/core';
import { ClrDatagridFilterInterface } from '@clr/angular';
import { ReplaySubject } from 'rxjs';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { BenutzerDto } from '@api/benutzer';

@Component({
  selector: 'app-behoerde-filter',
  templateUrl: './behoerde-filter.component.html',
  styleUrls: ['./behoerde-filter.component.scss'],
})
export class BehoerdeFilterComponent
  implements OnInit, ClrDatagridFilterInterface<BenutzerDto>
{
  @Input() options: Array<BehoerdeListItem>;
  selectedItems: Array<BehoerdeListItem> = [];
  changes = new ReplaySubject(1);

  ngOnInit(): void {
    this.changes.next();
  }

  accepts(benutzer: BenutzerDto): boolean {
    if (!this.selectedItems || this.selectedItems.length === 0) {
      return true;
    }
    return this.selectedItems.some((b) => {
      return (
        benutzer.datenberechtigung?.behoerden?.includes(b.schluessel) &&
        benutzer.datenberechtigung?.land === b.land
      );
    });
  }

  isActive(): boolean {
    return this.selectedItems && this.selectedItems.length > 0;
  }

  selectedOptionsChanged(): void {
    this.changes.next();
  }
}
