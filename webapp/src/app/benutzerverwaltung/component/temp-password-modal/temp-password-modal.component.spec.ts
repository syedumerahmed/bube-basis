import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempPasswordModalComponent } from './temp-password-modal.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('TempPasswordModalComponent', () => {
  let component: TempPasswordModalComponent;
  let fixture: ComponentFixture<TempPasswordModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TempPasswordModalComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TempPasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
