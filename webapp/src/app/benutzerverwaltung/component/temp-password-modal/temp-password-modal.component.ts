import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  ClarityIcons,
  copyIcon,
  successStandardIcon,
  warningStandardIcon,
} from '@cds/core/icon';
import { BehaviorSubject, from } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

ClarityIcons.addIcons(copyIcon, successStandardIcon, warningStandardIcon);

@Component({
  selector: 'app-temp-password-modal',
  templateUrl: './temp-password-modal.component.html',
  styleUrls: ['./temp-password-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TempPasswordModalComponent {
  @Output()
  openChange = new EventEmitter<boolean>();

  @Output()
  closed = new EventEmitter<void>();

  @Input()
  set open(value: boolean) {
    this._open = value;
    this.openChange.next(value);
  }

  get open(): boolean {
    return this._open;
  }

  @Input() tempPassword: string;
  showKopiertAlert = new BehaviorSubject(false);
  tempPwAccepted = false;

  private _open: boolean;

  copyToClipboard(): void {
    from(navigator.clipboard.writeText(this.tempPassword))
      .pipe(
        tap(() => this.showKopiertAlert.next(true)),
        delay(1_000),
        tap(() => this.showKopiertAlert.next(false))
      )
      .subscribe();
  }

  close(): void {
    this.closed.next();
    this.open = false;
  }
}
