import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetriebsstaetteFilterComponent } from './betriebsstaette-filter.component';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { EMPTY, of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import spyOn = jest.spyOn;
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';
import { BenutzerDto, DatenberechtigungDto } from '@api/benutzer';

describe('BetriebsstaetteFilterComponent', () => {
  let component: BetriebsstaetteFilterComponent;
  let fixture: ComponentFixture<BetriebsstaetteFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
      declarations: [BetriebsstaetteFilterComponent],
      providers: [
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
      ],
    }).compileComponents();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(BetriebsstaetteFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      spyOn(component.changes, 'next');
      component.ngOnInit();
    });

    it('should notify about changes', () => {
      expect(component.changes.next).toHaveBeenCalledTimes(1);
    });
  });

  describe('accept', () => {
    const benutzer: BenutzerDto = {
      datenberechtigung: {
        betriebsstaetten: ['01', '07'],
      } as Partial<DatenberechtigungDto> as DatenberechtigungDto,
    } as Partial<BenutzerDto> as BenutzerDto;

    it('should be true if benutzer matches any selected item', () => {
      component.selectedItems = [
        { betriebsstaetteNr: '01' },
        { betriebsstaetteNr: '02' },
      ] as Partial<BetriebsstaetteDto>[] as BetriebsstaetteDto[];

      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if benutzer matches all selected item', () => {
      component.selectedItems = [
        { betriebsstaetteNr: '01' },
        { betriebsstaetteNr: '02' },
      ] as Partial<BetriebsstaetteDto>[] as BetriebsstaetteDto[];

      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if no item is selected', () => {
      component.selectedItems = [];
      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be true if no item is selected and no datenberechtigung is set', () => {
      component.selectedItems = [];
      expect(component.accepts({} as Partial<BenutzerDto> as BenutzerDto)).toBe(
        true
      );
    });

    it('should be true if selected items are undefined', () => {
      component.selectedItems = undefined;
      expect(component.accepts(benutzer)).toBe(true);
    });

    it('should be false if single selected item is matched', () => {
      component.selectedItems = [
        { betriebsstaetteNr: '02' },
      ] as Partial<BetriebsstaetteDto>[] as BetriebsstaetteDto[];

      expect(component.accepts(benutzer)).toBe(false);
    });

    it('should be false if none of selected item is matched', () => {
      component.selectedItems = [
        { betriebsstaetteNr: '01' },
        { betriebsstaetteNr: '02' },
      ] as Partial<BetriebsstaetteDto>[] as BetriebsstaetteDto[];

      expect(component.accepts({} as Partial<BenutzerDto> as BenutzerDto)).toBe(
        false
      );
    });
  });

  describe('selectedOptionsChanged', () => {
    beforeEach(() => {
      spyOn(component.changes, 'next');
      component.selectedOptionsChanged();
    });

    it('should notify about changes', () => {
      expect(component.changes.next).toHaveBeenCalledTimes(1);
    });
  });

  describe('active', () => {
    it('should be true if there are selected items', () => {
      component.selectedItems = [
        { betriebsstaetteNr: '01' },
        { betriebsstaetteNr: '02' },
      ] as Partial<BetriebsstaetteDto>[] as BetriebsstaetteDto[];
      expect(component.isActive()).toBe(true);
    });

    it('should be false if selectedItems are empty', () => {
      component.selectedItems = [];
      expect(component.isActive()).toBe(false);
    });
  });

  describe('fetchOptions', () => {
    it('should fetch betriebsstaetten with suchstring', inject(
      [BetriebsstaettenRestControllerService],
      (service: BetriebsstaettenRestControllerService) => {
        spyOn(service, 'findBetriebsstaetteByNummerNameOrt').mockReturnValue(
          EMPTY
        );
        component.fetchOptions('A');
        expect(service.findBetriebsstaetteByNummerNameOrt).toHaveBeenCalledWith(
          'A'
        );
      }
    ));

    it('should fetch betriebsstaetten with undefined suchstring', inject(
      [BetriebsstaettenRestControllerService],
      (service: BetriebsstaettenRestControllerService) => {
        spyOn(service, 'findBetriebsstaetteByNummerNameOrt').mockReturnValue(
          EMPTY
        );
        component.fetchOptions();
        expect(service.findBetriebsstaetteByNummerNameOrt).toHaveBeenCalledWith(
          ''
        );
      }
    ));

    const betriebsstaettenResultMock: BetriebsstaetteDto[] = [];
    describe('successful fetch', () => {
      beforeEach(inject(
        [BetriebsstaettenRestControllerService],
        (service: BetriebsstaettenRestControllerService) => {
          spyOn(service, 'findBetriebsstaetteByNummerNameOrt').mockReturnValue(
            of(betriebsstaettenResultMock) as any
          );
        }
      ));

      it('should add result to asyncOptions', () => {
        spyOn(component.asyncOptions$, 'next');
        component.fetchOptions();
        expect(component.asyncOptions$.next).toHaveBeenCalledWith(
          betriebsstaettenResultMock
        );
      });
    });
  });
});
