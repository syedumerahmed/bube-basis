import { Component, OnInit } from '@angular/core';
import { ClrDatagridFilterInterface } from '@clr/angular';
import { interval, ReplaySubject } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { BenutzerDto } from '@api/benutzer';
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';
import { buildingIcon, ClarityIcons } from '@cds/core/icon';

const INPUT_DEBOUNCE_INTERVAL = 1000;

ClarityIcons.addIcons(buildingIcon);

@Component({
  selector: 'app-betrieb-filter',
  templateUrl: './betriebsstaette-filter.component.html',
  styleUrls: ['./betriebsstaette-filter.component.scss'],
})
export class BetriebsstaetteFilterComponent
  implements OnInit, ClrDatagridFilterInterface<BenutzerDto>
{
  selectedItems: BetriebsstaetteDto[] = [];
  changes = new ReplaySubject(1);
  asyncOptions$ = new ReplaySubject<BetriebsstaetteDto[]>(1);
  inputChanged$ = new ReplaySubject<string>(1);
  loading = false;

  constructor(
    private readonly betriebsstaettenRestControllerService: BetriebsstaettenRestControllerService
  ) {}

  ngOnInit(): void {
    this.inputChanged$
      .pipe(debounce(() => interval(INPUT_DEBOUNCE_INTERVAL)))
      .subscribe((input) => this.fetchOptions(input));
    this.changes.next();
  }

  accepts(benutzer: BenutzerDto): boolean {
    if (!this.selectedItems || this.selectedItems.length === 0) {
      return true;
    }
    return this.selectedItems
      .map((b) => b.betriebsstaetteNr)
      .some((r) => benutzer.datenberechtigung?.betriebsstaetten?.includes(r));
  }

  isActive(): boolean {
    return this.selectedItems && this.selectedItems.length > 0;
  }

  selectedOptionsChanged(): void {
    this.changes.next();
  }

  fetchOptions($event?: string): void {
    $event = $event ? $event : '';
    this.loading = true;
    this.asyncOptions$.next([]);
    this.betriebsstaettenRestControllerService
      .findBetriebsstaetteByNummerNameOrt($event)
      .subscribe(
        (b) => {
          this.loading = false;
          this.asyncOptions$.next(b);
        },
        () => {
          this.loading = false;
        }
      );
  }

  displayText(betriebsstaette: BetriebsstaetteDto): string {
    return (
      betriebsstaette.betriebsstaetteNr +
      ' ' +
      betriebsstaette.name +
      ', ' +
      betriebsstaette.adresse?.ort
    );
  }
}
