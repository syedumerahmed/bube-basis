import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  BenutzerRollenComponent,
  RollenFormData,
} from './benutzer-rollen.component';
import { Rolle } from '@/base/security/model/Rolle';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { BaseModule } from '@/base/base.module';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { FormBuilder, FormGroup } from '@/base/forms';

describe('BenutzerRollenComponent', () => {
  let component: BenutzerRollenComponent;
  let fixture: ComponentFixture<BenutzerRollenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BenutzerRollenComponent],
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
      imports: [
        MockModule(FormsModule),
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(BaseModule),
      ],
    }).compileComponents();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(BenutzerRollenComponent);
    component = fixture.componentInstance;
    component.form = BenutzerRollenComponent.createRollenForm(
      new FormBuilder(),
      GESAMTADMIN_DUMMY(),
      BenutzerRollenComponent.mapRollenToForm([])
    );
    component.benutzerRollen = [];
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  type RollenTestParams = {
    form: keyof RollenFormData;
    selectableRoles: () => { rolle: Rolle }[];
    rollen: { read: Rolle; write: Rolle; delete: Rolle };
  };
  describe.each([
    {
      form: 'land',
      selectableRoles: () => component.landOptions,
      rollen: {
        read: Rolle.LAND_READ,
        write: Rolle.LAND_WRITE,
        delete: Rolle.LAND_DELETE,
      },
    },
    {
      form: 'behoerde',
      selectableRoles: () => component.behoerdeOptions,
      rollen: {
        read: Rolle.BEHOERDE_READ,
        write: Rolle.BEHOERDE_WRITE,
        delete: Rolle.BEHOERDE_DELETE,
      },
    },
    {
      form: 'referenzenBund',
      selectableRoles: () => component.referenzenBundOptions,
      rollen: {
        read: Rolle.REFERENZLISTEN_BUND_READ,
        write: Rolle.REFERENZLISTEN_BUND_WRITE,
        delete: Rolle.REFERENZLISTEN_BUND_DELETE,
      },
    },
    {
      form: 'referenzenLand',
      selectableRoles: () => component.referenzenLandOptions,
      rollen: {
        read: Rolle.REFERENZLISTEN_LAND_READ,
        write: Rolle.REFERENZLISTEN_LAND_WRITE,
        delete: Rolle.REFERENZLISTEN_LAND_DELETE,
      },
    },
  ] as RollenTestParams[])(
    'Rollenzuweisungen $form',
    ({ form, rollen, selectableRoles }) => {
      describe('Bearbeiter mit Löschberechtigung', () => {
        let bearbeiterLoeschen: BenutzerProfil;
        beforeEach(() => {
          bearbeiterLoeschen = new BenutzerProfil(
            'deleteUser',
            null,
            null,
            null,
            [
              Rolle.BEHOERDENBENUTZER,
              Rolle.BENUTZERVERWALTUNG_WRITE,
              rollen.read,
              rollen.write,
              rollen.delete,
            ],
            { land: '00' }
          );
          TestBed.overrideProvider(BenutzerProfil, {
            useValue: bearbeiterLoeschen,
          });
          fixture = TestBed.createComponent(BenutzerRollenComponent);
          component = fixture.componentInstance;
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterLoeschen,
            BenutzerRollenComponent.mapRollenToForm([])
          );
          component.benutzerRollen = [];
          fixture.detectChanges();
          component.ngOnChanges();
        });

        it('should have all assignable options', () => {
          expect(selectableRoles().map((o) => o.rolle)).toEqual([
            null,
            rollen.read,
            rollen.write,
            rollen.delete,
          ]);
        });

        describe.each([
          { rolle: rollen.delete, shouldEdit: true },
          { rolle: rollen.write, shouldEdit: true },
          { rolle: rollen.read, shouldEdit: true },
        ])('user with rolle $rolle', ({ rolle, shouldEdit }) => {
          beforeEach(() => {
            component.benutzerRollen = [rolle];
            component.form = BenutzerRollenComponent.createRollenForm(
              new FormBuilder(),
              bearbeiterLoeschen,
              BenutzerRollenComponent.mapRollenToForm([rolle])
            );
            fixture.detectChanges();
            component.ngOnChanges();
          });

          it('should have value in form', () => {
            expect(component.form.controls[form].value).toBe(rolle);
          });

          it(`control should be ${shouldEdit ? 'enabled' : 'disabled'}`, () => {
            expect(component.form.controls[form].enabled).toBe(shouldEdit);
          });
        });
      });

      describe('Bearbeiter mit Schreibberechtigung', () => {
        let bearbeiterSchreiben: BenutzerProfil;
        beforeEach(() => {
          bearbeiterSchreiben = new BenutzerProfil(
            'writeUser',
            null,
            null,
            null,
            [
              Rolle.BEHOERDENBENUTZER,
              Rolle.BENUTZERVERWALTUNG_WRITE,
              rollen.read,
              rollen.write,
            ],
            { land: '00' }
          );
          TestBed.overrideProvider(BenutzerProfil, {
            useValue: bearbeiterSchreiben,
          });
          fixture = TestBed.createComponent(BenutzerRollenComponent);
          component = fixture.componentInstance;
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterSchreiben,
            BenutzerRollenComponent.mapRollenToForm([])
          );
          component.benutzerRollen = [];
          fixture.detectChanges();
          component.ngOnChanges();
        });

        it('should have read and write options', () => {
          expect(selectableRoles().map((o) => o.rolle)).toEqual([
            null,
            rollen.read,
            rollen.write,
          ]);
        });

        describe.each([
          { rolle: rollen.delete, shouldEdit: false },
          { rolle: rollen.write, shouldEdit: true },
          { rolle: rollen.read, shouldEdit: true },
        ])('user with rolle $rolle', ({ rolle, shouldEdit }) => {
          beforeEach(() => {
            component.benutzerRollen = [rolle];
            component.form = BenutzerRollenComponent.createRollenForm(
              new FormBuilder(),
              bearbeiterSchreiben,
              BenutzerRollenComponent.mapRollenToForm([rolle])
            );
            fixture.detectChanges();
            component.ngOnChanges();
          });

          it('should have value in form', () => {
            expect(component.form.controls[form].value).toBe(rolle);
          });

          it(`control should be ${shouldEdit ? 'enabled' : 'disabled'}`, () => {
            expect(component.form.controls[form].enabled).toBe(shouldEdit);
          });
        });
      });

      describe('Bearbeiter mit Leseberechtigung', () => {
        const bearbeiterLesen = new BenutzerProfil(
          'readUser',
          null,
          null,
          null,
          [
            Rolle.BEHOERDENBENUTZER,
            Rolle.BENUTZERVERWALTUNG_WRITE,
            rollen.read,
          ],
          { land: '00' }
        );
        beforeEach(() => {
          TestBed.overrideProvider(BenutzerProfil, {
            useValue: bearbeiterLesen,
          });
          fixture = TestBed.createComponent(BenutzerRollenComponent);
          component = fixture.componentInstance;
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterLesen,
            BenutzerRollenComponent.mapRollenToForm([])
          );
          component.benutzerRollen = [];
          fixture.detectChanges();
          component.ngOnChanges();
        });

        it('should have read option', () => {
          expect(selectableRoles().map((o) => o.rolle)).toEqual([
            null,
            rollen.read,
          ]);
        });

        describe.each([
          { rolle: rollen.delete, shouldEdit: false },
          { rolle: rollen.write, shouldEdit: false },
          { rolle: rollen.read, shouldEdit: true },
        ])('user with rolle $rolle', ({ rolle, shouldEdit }) => {
          beforeEach(() => {
            component.benutzerRollen = [rolle];
            component.form = BenutzerRollenComponent.createRollenForm(
              new FormBuilder(),
              bearbeiterLesen,
              BenutzerRollenComponent.mapRollenToForm([rolle])
            );
            fixture.detectChanges();
            component.ngOnChanges();
          });

          it('should have value in form', () => {
            expect(component.form.controls[form].value).toBe(rolle);
          });

          it(`control should be ${shouldEdit ? 'enabled' : 'disabled'}`, () => {
            expect(component.form.controls[form].enabled).toBe(shouldEdit);
          });
        });
      });
    }
  );

  describe('Rollenzuweisungen benutzerverwaltung', () => {
    describe('Bearbeiter mit Löschberechtigung', () => {
      const bearbeiterLoeschen = new BenutzerProfil(
        'deleteUser',
        null,
        null,
        null,
        [
          Rolle.BEHOERDENBENUTZER,
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.BENUTZERVERWALTUNG_WRITE,
          Rolle.BENUTZERVERWALTUNG_DELETE,
        ],
        { land: '00' }
      );
      beforeEach(() => {
        TestBed.overrideProvider(BenutzerProfil, {
          useValue: bearbeiterLoeschen,
        });
        fixture = TestBed.createComponent(BenutzerRollenComponent);
        component = fixture.componentInstance;
        component.form = BenutzerRollenComponent.createRollenForm(
          new FormBuilder(),
          bearbeiterLoeschen,
          BenutzerRollenComponent.mapRollenToForm([])
        );
        component.benutzerRollen = [];
        fixture.detectChanges();
        component.ngOnChanges();
      });

      it('should have all assignable options', () => {
        expect(component.benutzerverwaltungOptions.map((o) => o.rolle)).toEqual(
          [
            null,
            Rolle.BENUTZERVERWALTUNG_READ,
            Rolle.BENUTZERVERWALTUNG_WRITE,
            Rolle.BENUTZERVERWALTUNG_DELETE,
          ]
        );
      });

      describe.each([
        { rolle: Rolle.BENUTZERVERWALTUNG_READ, shouldEdit: true },
        { rolle: Rolle.BENUTZERVERWALTUNG_WRITE, shouldEdit: true },
        { rolle: Rolle.BENUTZERVERWALTUNG_DELETE, shouldEdit: true },
      ])('user with rolle $rolle', ({ rolle, shouldEdit }) => {
        beforeEach(() => {
          component.benutzerRollen = [rolle];
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterLoeschen,
            BenutzerRollenComponent.mapRollenToForm([rolle])
          );
          fixture.detectChanges();
          component.ngOnChanges();
        });

        it('should have value in form', () => {
          expect(component.form.controls.benutzerverwaltung.value).toBe(rolle);
        });

        it(`control should be ${shouldEdit ? 'enabled' : 'disabled'}`, () => {
          expect(component.form.controls.benutzerverwaltung.enabled).toBe(
            shouldEdit
          );
        });
      });
    });

    describe('Bearbeiter mit Schreibberechtigung', () => {
      const bearbeiterLesen = new BenutzerProfil(
        'writeUser',
        null,
        null,
        null,
        [
          Rolle.BEHOERDENBENUTZER,
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.BENUTZERVERWALTUNG_WRITE,
        ],
        { land: '00' }
      );
      beforeEach(() => {
        TestBed.overrideProvider(BenutzerProfil, {
          useValue: bearbeiterLesen,
        });
        fixture = TestBed.createComponent(BenutzerRollenComponent);
        component = fixture.componentInstance;
        component.form = BenutzerRollenComponent.createRollenForm(
          new FormBuilder(),
          bearbeiterLesen,
          BenutzerRollenComponent.mapRollenToForm([])
        );
        component.benutzerRollen = [];
        fixture.detectChanges();
        component.ngOnChanges();
      });

      it('should have read and write options', () => {
        expect(component.benutzerverwaltungOptions.map((o) => o.rolle)).toEqual(
          [null, Rolle.BENUTZERVERWALTUNG_READ, Rolle.BENUTZERVERWALTUNG_WRITE]
        );
      });

      describe.each([
        { rolle: Rolle.BENUTZERVERWALTUNG_READ, shouldEdit: true },
        { rolle: Rolle.BENUTZERVERWALTUNG_WRITE, shouldEdit: true },
        { rolle: Rolle.BENUTZERVERWALTUNG_DELETE, shouldEdit: false },
      ])('user with rolle $rolle', ({ rolle, shouldEdit }) => {
        beforeEach(() => {
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterLesen,
            BenutzerRollenComponent.mapRollenToForm([rolle])
          );
        });

        it('should have value in form', () => {
          expect(component.form.controls.benutzerverwaltung.value).toBe(rolle);
        });

        it(`control should be ${shouldEdit ? 'enabled' : 'disabled'}`, () => {
          expect(component.form.controls.benutzerverwaltung.enabled).toBe(
            shouldEdit
          );
        });
      });
    });

    describe('Bearbeiter mit Leseberechtigung', () => {
      const bearbeiterLesen = new BenutzerProfil(
        'readUser',
        null,
        null,
        null,
        [Rolle.BEHOERDENBENUTZER, Rolle.BENUTZERVERWALTUNG_READ],
        { land: '00' }
      );
      beforeEach(() => {
        TestBed.overrideProvider(BenutzerProfil, {
          useValue: bearbeiterLesen,
        });
        fixture = TestBed.createComponent(BenutzerRollenComponent);
        component = fixture.componentInstance;
        component.form = BenutzerRollenComponent.createRollenForm(
          new FormBuilder(),
          bearbeiterLesen,
          BenutzerRollenComponent.mapRollenToForm([])
        );
        component.benutzerRollen = [];
        fixture.detectChanges();
        component.ngOnChanges();
      });

      describe.each([
        Rolle.BENUTZERVERWALTUNG_READ,
        Rolle.BENUTZERVERWALTUNG_WRITE,
        Rolle.BENUTZERVERWALTUNG_DELETE,
      ])('user with rolle %s', (rolle) => {
        beforeEach(() => {
          component.benutzerRollen = [rolle];
          component.form = BenutzerRollenComponent.createRollenForm(
            new FormBuilder(),
            bearbeiterLesen,
            BenutzerRollenComponent.mapRollenToForm([rolle])
          );
        });

        it('should have value in form', () => {
          expect(component.form.controls.benutzerverwaltung.value).toBe(rolle);
        });
      });
    });
  });
});

describe('Rollenzuweisungen form updates', () => {
  let form: FormGroup<RollenFormData>;
  beforeEach(() => {
    form = BenutzerRollenComponent.createRollenForm(
      new FormBuilder(),
      GESAMTADMIN_DUMMY(),
      BenutzerRollenComponent.mapRollenToForm([])
    );
  });

  it.each([
    'land',
    'behoerde',
    'referenzenBund',
    'referenzenLand',
    'benutzerverwaltung',
    'admin',
    'gesamtadmin',
  ])('should have all %s selections enabled', (control) => {
    expect(form.controls[control].enabled).toBe(true);
  });

  describe('add admin role', () => {
    beforeEach(() => {
      form.controls.admin.setValue(true);
    });

    it('form should be enabled', () => {
      expect(form.enabled).toBe(true);
    });

    it.each`
      control                 | enabled
      ${'land'}               | ${false}
      ${'behoerde'}           | ${false}
      ${'referenzenBund'}     | ${true}
      ${'referenzenLand'}     | ${false}
      ${'benutzerverwaltung'} | ${false}
      ${'admin'}              | ${true}
      ${'gesamtadmin'}        | ${true}
    `(
      '$control should have enabled=$enabled when admin',
      ({ control, enabled }) => {
        expect(form.controls[control].enabled).toBe(enabled);
      }
    );

    describe('add gesamtadmin role', () => {
      beforeEach(() => {
        form.controls.gesamtadmin.setValue(true);
      });

      it('form should be enabled', () => {
        expect(form.enabled).toBe(true);
      });

      it.each`
        control                 | enabled
        ${'land'}               | ${false}
        ${'behoerde'}           | ${false}
        ${'referenzenBund'}     | ${false}
        ${'referenzenLand'}     | ${false}
        ${'benutzerverwaltung'} | ${false}
        ${'admin'}              | ${false}
        ${'gesamtadmin'}        | ${true}
      `(
        '$control should have enabled=$enabled when gesamtadmin',
        ({ control, enabled }) => {
          expect(form.controls[control].enabled).toBe(enabled);
        }
      );
    });
  });
});
