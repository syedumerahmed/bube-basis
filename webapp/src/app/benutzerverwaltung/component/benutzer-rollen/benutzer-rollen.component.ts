import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
} from '@angular/core';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Rolle } from '@/base/security/model/Rolle';
import { BenutzerZugewieseneRollenEnum } from '@api/benutzer';
import { ValidationErrors } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';

type RollenType = keyof typeof Rolle;
type LandRollen = Extract<
  RollenType,
  'LAND_READ' | 'LAND_WRITE' | 'LAND_DELETE'
>;
type BehoerdeRollen = Extract<
  RollenType,
  'BEHOERDE_READ' | 'BEHOERDE_WRITE' | 'BEHOERDE_DELETE'
>;
type ReferenzenBundRollen = Extract<
  RollenType,
  | 'REFERENZLISTEN_BUND_READ'
  | 'REFERENZLISTEN_BUND_WRITE'
  | 'REFERENZLISTEN_BUND_DELETE'
>;
type ReferenzenLandRollen = Extract<
  RollenType,
  | 'REFERENZLISTEN_LAND_READ'
  | 'REFERENZLISTEN_LAND_WRITE'
  | 'REFERENZLISTEN_LAND_DELETE'
>;
type BenutzerverwaltungRollen = Extract<
  RollenType,
  | 'BENUTZERVERWALTUNG_READ'
  | 'BENUTZERVERWALTUNG_WRITE'
  | 'BENUTZERVERWALTUNG_DELETE'
>;
type RolleOption<T extends RollenType> = { rolle: T; displayText: string };

const LAND_OPTIONS: Array<RolleOption<LandRollen>> = [
  { rolle: null, displayText: 'Bitte wählen' },
  { rolle: Rolle.LAND_READ, displayText: 'lesen' },
  { rolle: Rolle.LAND_WRITE, displayText: 'schreiben' },
  { rolle: Rolle.LAND_DELETE, displayText: 'löschen' },
];

const BEHOERDE_OPTIONS: Array<RolleOption<BehoerdeRollen>> = [
  { rolle: null, displayText: 'Bitte wählen' },
  { rolle: Rolle.BEHOERDE_READ, displayText: 'lesen' },
  { rolle: Rolle.BEHOERDE_WRITE, displayText: 'schreiben' },
  { rolle: Rolle.BEHOERDE_DELETE, displayText: 'löschen' },
];

const REFERENZEN_BUND_OPTIONS: Array<RolleOption<ReferenzenBundRollen>> = [
  { rolle: null, displayText: 'Bitte wählen' },
  { rolle: Rolle.REFERENZLISTEN_BUND_READ, displayText: 'lesen' },
  { rolle: Rolle.REFERENZLISTEN_BUND_WRITE, displayText: 'schreiben' },
  { rolle: Rolle.REFERENZLISTEN_BUND_DELETE, displayText: 'löschen' },
];

const REFERENZEN_LAND_OPTIONS: Array<RolleOption<ReferenzenLandRollen>> = [
  { rolle: null, displayText: 'Bitte wählen' },
  { rolle: Rolle.REFERENZLISTEN_LAND_READ, displayText: 'lesen' },
  { rolle: Rolle.REFERENZLISTEN_LAND_WRITE, displayText: 'schreiben' },
  { rolle: Rolle.REFERENZLISTEN_LAND_DELETE, displayText: 'löschen' },
];

const BENUTZERVERWALTUNG_OPTIONS: Array<RolleOption<BenutzerverwaltungRollen>> =
  [
    { rolle: null, displayText: 'Bitte wählen' },
    { rolle: Rolle.BENUTZERVERWALTUNG_READ, displayText: 'lesen' },
    { rolle: Rolle.BENUTZERVERWALTUNG_WRITE, displayText: 'schreiben' },
    { rolle: Rolle.BENUTZERVERWALTUNG_DELETE, displayText: 'löschen' },
  ];

export interface RollenFormData {
  land: LandRollen;
  behoerde: BehoerdeRollen;
  referenzenBund: ReferenzenBundRollen;
  referenzenLand: ReferenzenLandRollen;
  benutzerverwaltung: BenutzerverwaltungRollen;
  admin: boolean;
  gesamtadmin: boolean;
}

@Component({
  selector: 'app-benutzer-rollen',
  templateUrl: './benutzer-rollen.component.html',
  styleUrls: ['./benutzer-rollen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BenutzerRollenComponent implements OnChanges {
  readonly Rolle = Rolle;

  @Input()
  form: FormGroup<RollenFormData>;
  @Input()
  benutzerRollen: Array<BenutzerZugewieseneRollenEnum>;

  landOptions: Array<RolleOption<LandRollen>>;
  behoerdeOptions: Array<RolleOption<BehoerdeRollen>>;
  referenzenLandOptions: Array<RolleOption<ReferenzenLandRollen>>;
  referenzenBundOptions: Array<RolleOption<ReferenzenBundRollen>>;
  benutzerverwaltungOptions: Array<RolleOption<BenutzerverwaltungRollen>>;

  static mapRollenToForm(
    rollen: Array<BenutzerZugewieseneRollenEnum>
  ): RollenFormData {
    return {
      land: this.findRolle(
        rollen,
        LAND_OPTIONS.map((o) => o.rolle)
      ),
      behoerde: this.findRolle(
        rollen,
        BEHOERDE_OPTIONS.map((o) => o.rolle)
      ),
      referenzenBund: this.findRolle(
        rollen,
        REFERENZEN_BUND_OPTIONS.map((o) => o.rolle)
      ),
      referenzenLand: this.findRolle(
        rollen,
        REFERENZEN_LAND_OPTIONS.map((o) => o.rolle)
      ),
      benutzerverwaltung: this.findRolle(
        rollen,
        BENUTZERVERWALTUNG_OPTIONS.map((o) => o.rolle)
      ),
      admin: rollen.includes(Rolle.ADMIN),
      gesamtadmin: rollen.includes(Rolle.GESAMTADMIN),
    };
  }

  private static findRolle<T extends RollenType>(
    zugewieseneRollen: Array<RollenType>,
    rollen: Array<T>
  ): T {
    return rollen.find((r) => zugewieseneRollen.includes(r)) || null;
  }

  // Mindestens eine Rolle muss ausgewählt werden
  private static rollenValidator(
    group: FormGroup<RollenFormData>
  ): ValidationErrors {
    const value = group.value;
    if (
      value.gesamtadmin ||
      value.admin ||
      value.land ||
      value.behoerde ||
      value.referenzenBund ||
      value.referenzenLand ||
      value.benutzerverwaltung
    ) {
      return null;
    }
    return { atLeastOneRole: true };
  }

  static createRollenForm(
    fb: FormBuilder,
    bearbeiter: BenutzerProfil,
    rollen: RollenFormData
  ): FormGroup<RollenFormData> {
    const formGroup = fb.group(
      {
        land: {
          value: rollen.land,
          disabled:
            rollen.gesamtadmin ||
            rollen.admin ||
            !BenutzerRollenComponent.canAssignRolle(bearbeiter, rollen.land),
        },
        behoerde: {
          value: rollen.behoerde,
          disabled:
            rollen.gesamtadmin ||
            rollen.admin ||
            !BenutzerRollenComponent.canAssignRolle(
              bearbeiter,
              rollen.behoerde
            ),
        },
        referenzenBund: {
          value: rollen.referenzenBund,
          disabled:
            rollen.gesamtadmin ||
            !BenutzerRollenComponent.canAssignRolle(
              bearbeiter,
              rollen.referenzenBund
            ),
        },
        referenzenLand: {
          value: rollen.referenzenLand,
          disabled:
            rollen.gesamtadmin ||
            rollen.admin ||
            !BenutzerRollenComponent.canAssignRolle(
              bearbeiter,
              rollen.referenzenLand
            ),
        },
        benutzerverwaltung: {
          value: rollen.benutzerverwaltung,
          disabled:
            rollen.gesamtadmin ||
            rollen.admin ||
            !BenutzerRollenComponent.canAssignRolle(
              bearbeiter,
              rollen.benutzerverwaltung
            ),
        },
        admin: {
          value: rollen.admin,
          disabled: rollen.gesamtadmin,
        },
        gesamtadmin: rollen.gesamtadmin,
      },
      {
        validators: BenutzerRollenComponent.rollenValidator,
      }
    );

    const rollenControls = formGroup.controls;
    rollenControls.admin.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((admin) => {
        rollenControls.admin.markAsTouched();
        [
          rollenControls.land,
          rollenControls.behoerde,
          rollenControls.benutzerverwaltung,
          rollenControls.referenzenLand,
        ].forEach((control) => (admin ? control.disable() : control.enable()));
      });
    rollenControls.gesamtadmin.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((gesamtadmin) => {
        rollenControls.gesamtadmin.markAsTouched();
        if (!rollenControls.admin.value) {
          [
            rollenControls.land,
            rollenControls.behoerde,
            rollenControls.benutzerverwaltung,
            rollenControls.referenzenLand,
          ].forEach((control) =>
            gesamtadmin ? control.disable() : control.enable()
          );
        }
        [rollenControls.admin, rollenControls.referenzenBund].forEach(
          (control) => (gesamtadmin ? control.disable() : control.enable())
        );
      });
    return formGroup;
  }

  private static canAssignRolle(bearbeiter: BenutzerProfil, rolle: RollenType) {
    return !rolle || bearbeiter.hasRolle(Rolle[rolle]);
  }

  constructor(
    readonly bearbeiter: BenutzerProfil,
    private readonly fb: FormBuilder
  ) {}

  ngOnChanges(): void {
    this.landOptions = this.filterOptions(LAND_OPTIONS);
    this.behoerdeOptions = this.filterOptions(BEHOERDE_OPTIONS);
    this.referenzenLandOptions = this.filterOptions(REFERENZEN_LAND_OPTIONS);
    this.referenzenBundOptions = this.filterOptions(REFERENZEN_BUND_OPTIONS);
    this.benutzerverwaltungOptions = this.filterOptions(
      BENUTZERVERWALTUNG_OPTIONS
    );
  }

  private filterOptions<T extends RollenType>(
    availableOptions: Array<RolleOption<T>>
  ): Array<RolleOption<T>> {
    return availableOptions.filter(
      ({ rolle }) =>
        BenutzerRollenComponent.canAssignRolle(this.bearbeiter, rolle) ||
        // Sonderfall zur Anzeige, wenn Benutzer in einem Bereich höheres Recht
        // hat, als der Bearbeiter selbst
        this.benutzerRollen.includes(rolle)
    );
  }
}
