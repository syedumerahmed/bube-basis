import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenutzerAktivFilterComponent } from './benutzer-aktiv-filter.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('BenutzerAktivFilterComponent', () => {
  let component: BenutzerAktivFilterComponent;
  let fixture: ComponentFixture<BenutzerAktivFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BenutzerAktivFilterComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenutzerAktivFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
