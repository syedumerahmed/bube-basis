import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClrDatagridFilterInterface } from '@clr/angular';
import { ReplaySubject } from 'rxjs';
import { BenutzerDto } from '@api/benutzer';

@Component({
  selector: 'app-benutzer-aktiv-filter',
  templateUrl: './benutzer-aktiv-filter.component.html',
  styleUrls: ['./benutzer-aktiv-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BenutzerAktivFilterComponent
  implements ClrDatagridFilterInterface<BenutzerDto>
{
  aktivChecked = true;
  inaktivChecked = true;

  changes = new ReplaySubject(1);

  accepts(benutzer: BenutzerDto): boolean {
    return (
      (benutzer.aktiv && this.aktivChecked) ||
      (!benutzer.aktiv && this.inaktivChecked)
    );
  }

  isActive(): boolean {
    return !(this.aktivChecked && this.inaktivChecked);
  }

  selectedOptionsChanged(): void {
    this.changes.next();
  }
}
