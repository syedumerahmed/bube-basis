import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { AlertService } from '@/base/service/alert.service';
import { Subscription } from 'rxjs';
import { saveAs } from 'file-saver';
import { Event, EventBusService } from '@/base/service/event-bus.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { BenutzerImportAdapterService } from '@/base/service/benutzer-import-adapter.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';
import {
  angleIcon,
  checkIcon,
  ClarityIcons,
  eyeIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  usersIcon,
} from '@cds/core/icon';
import { TemporaryPasswordsComponent } from '@/benutzerverwaltung/component/temporary-passwords/temporary-passwords.component';
import { filter, map, switchMap, tap } from 'rxjs/operators';

ClarityIcons.addIcons(
  usersIcon,
  checkIcon,
  timesIcon,
  trashIcon,
  angleIcon,
  plusIcon,
  eyeIcon
);

@Component({
  selector: 'app-betreiberbenutzer',
  templateUrl: './betreiberbenutzer.component.html',
  styleUrls: ['./betreiberbenutzer.component.scss'],
})
export class BetreiberbenutzerComponent implements OnInit, OnDestroy {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly benutzerRestControllerService: BenutzerRestControllerService,
    private readonly alertService: AlertService,
    private readonly eventbus: EventBusService,
    private readonly dateFormat: FilenameDateFormatPipe,
    readonly benutzerImportAdapter: BenutzerImportAdapterService
  ) {}
  @ViewChild(TemporaryPasswordsComponent, { static: true })
  temporaryPasswordsComponent: TemporaryPasswordsComponent;

  loading = false;

  importDialogOpen = false;

  exportDialogOpen = false;
  showTemporaryPasswords = false;
  benutzerMitEinmalpassworten: Array<BenutzerDto>;

  loeschenDialog: boolean;
  benutzerListe: Array<BenutzerDto>;
  benutzer: BenutzerDto;
  selected: Array<BenutzerDto> = [];
  loescht = false;

  readonly SortOrder = ClrDatagridSortOrder;

  private eventbusSub: Subscription;

  get canDelete(): boolean {
    return this.benutzerProfil.canDeleteBenutzer;
  }

  get canDeleteSelection(): boolean {
    if (this.nothingSelected) {
      return false;
    }
    return this.selected.every((benutzer) => this.canDeleteBenutzer(benutzer));
  }

  canDeleteBenutzer(benutzer: BenutzerDto): boolean {
    return (
      this.canDelete &&
      benutzer.canDelete &&
      benutzer.benutzername !== this.benutzerProfil.username
    );
  }

  get canWrite(): boolean {
    return this.benutzerProfil.canWriteBenutzer;
  }

  get isBehoerdenbenutzer(): boolean {
    return this.benutzerProfil.isBehoerdenbenutzer;
  }

  ngOnInit(): void {
    this.benutzerListe = this.route.snapshot.data.benutzer;
    this.route.data
      .pipe(
        map((d) => d.benutzerOhnePasswort),
        filter<Array<string>>(Boolean)
      )
      .subscribe((benutzerNamen) => {
        this.createTempPasswords(this.findBenutzerByNames(benutzerNamen));
      });

    this.eventbusSub = this.eventbus
      .on(Event.BetreiberBenutzerImportFertig)
      .pipe(
        tap(() => (this.loading = true)),
        switchMap(() =>
          this.benutzerRestControllerService.listAllBetreiberBenutzer()
        ),
        tap(() => (this.loading = false))
      )
      .subscribe(
        (benutzerListe) => (this.benutzerListe = benutzerListe),
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  private findBenutzerByNames(namen: string[]): BenutzerDto[] {
    return this.benutzerListe.filter((benutzer) =>
      namen.includes(benutzer.benutzername)
    );
  }

  ngOnDestroy(): void {
    this.eventbusSub.unsubscribe();
  }

  getAktiv(benutzer: BenutzerDto): string {
    return benutzer.aktiv ? 'aktiv' : 'inaktiv';
  }

  getBetriebsstaetten(benutzer: BenutzerDto): string {
    return benutzer.datenberechtigung.betriebsstaetten.join(', ').toString();
  }

  getBehoerdenNummern(benutzer: BenutzerDto): string {
    return benutzer.behoerdenSchluessel.join(', ').toString();
  }

  deleteBenutzer(): void {
    this.loeschen(this.benutzer.benutzername);
    this.closeLoeschenDialog();
  }

  deleteMultipleBenutzer(): void {
    this.loescht = true;
    this.selected
      .map((b) => b.benutzername)
      .forEach((name) => this.loeschen(name));
    this.closeLoeschenDialog();
  }

  private loeschen(name: string): void {
    this.benutzerRestControllerService.deleteBenutzer(name).subscribe(
      () => {
        const index = this.benutzerListe.findIndex(
          (b) => b.benutzername === name
        );
        if (index !== -1) {
          this.benutzerListe.splice(index, 1);
        }
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(benutzer: BenutzerDto): void {
    this.benutzer = benutzer;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.benutzer = null;
    this.loeschenDialog = true;
  }

  alleMarkieren(): void {
    this.selected = this.benutzerListe;
  }

  markierungAufheben(): void {
    this.selected = [];
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  openImportDialog(): void {
    this.importDialogOpen = true;
  }

  openExportDialog(): void {
    this.exportDialogOpen = true;
  }

  closeExportDialog(): void {
    this.exportDialogOpen = false;
  }

  startExport(): void {
    this.exportDialogOpen = false;
    const benutzerIds: Array<string> = this.selected.map(
      (benutzer) => benutzer.id
    );

    this.benutzerRestControllerService
      .exportBenutzer({ benutzerIds })
      .subscribe(
        (blob) => {
          const datum = this.dateFormat.transform(new Date());
          saveAs(blob, 'benutzer_' + datum + '.xml');
          this.alertService.setSuccess(
            `${this.selected.length} Benutzer ${
              this.selected.length > 1 ? 'wurden' : 'wurde'
            } exportiert.`
          );
          this.markierungAufheben();
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Fehler beim Exportieren der XML-Datei aufgetreten'
          );
        }
      );
  }

  get canCreateTempPasswordsForAllSelected(): boolean {
    return (
      this.selected.length !== 0 &&
      this.selected.every((benutzerDto) => benutzerDto.needsTempPassword)
    );
  }

  createTempPasswordsForAllSelected(): void {
    this.createTempPasswords(this.selected);
  }

  private createTempPasswords(benutzer: Array<BenutzerDto>): void {
    if (!benutzer?.length) {
      return;
    }
    this.showTemporaryPasswords = true;
    this.benutzerRestControllerService
      .createTempPasswords(benutzer.map((benutzer) => benutzer.id))
      .subscribe(
        (benutzer) => {
          this.benutzerMitEinmalpassworten = benutzer;
          this.markierungAufheben();
        },
        (error) => {
          this.showTemporaryPasswords = false;
          this.alertService.setAlert(error);
        }
      );
  }
}
