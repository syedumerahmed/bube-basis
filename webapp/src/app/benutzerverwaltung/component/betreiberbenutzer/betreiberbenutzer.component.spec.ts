import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetreiberbenutzerComponent } from './betreiberbenutzer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  of,
  Subject,
  throwError,
} from 'rxjs';
import { BetriebsstaetteFilterComponent } from '@/benutzerverwaltung/component/betriebsstaette-filter/betriebsstaette-filter.component';
import { BenutzerAktivFilterComponent } from '@/benutzerverwaltung/component/benutzer-aktiv-filter/benutzer-aktiv-filter.component';
import { FormsModule } from '@angular/forms';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { AlertService } from '@/base/service/alert.service';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties } from '@/base/model/Land';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { Rolle } from '@/base/security/model/Rolle';
import { EventBusService } from '@/base/service/event-bus.service';
import { BenutzerImportAdapterService } from '@/base/service/benutzer-import-adapter.service';
import { catchError } from 'rxjs/operators';
import { TemporaryPasswordsComponent } from '../temporary-passwords/temporary-passwords.component';
import { JobStatusDto } from '@api';
import spyOn = jest.spyOn;

describe('BetreiberbenutzerComponent', () => {
  let component: BetreiberbenutzerComponent;
  let fixture: ComponentFixture<BetreiberbenutzerComponent>;
  let benutzerListe: BenutzerDto[];
  let routeData$: BehaviorSubject<Data>;

  beforeEach(async () => {
    benutzerListe = ['mjoergens', 'tkrauss', 'rgarbers', 'fschaedler'].map(
      (benutzername, i) => ({
        id: benutzername + '_ID',
        benutzername,
        datenberechtigung: { land: LandProperties.DEUTSCHLAND.nr },
        zugewieseneRollen: [Rolle.BETREIBER],
        needsTempPassword: i % 2 === 0,
      })
    );
    routeData$ = new BehaviorSubject({ benutzer: benutzerListe });
    await TestBed.configureTestingModule({
      declarations: [
        BetreiberbenutzerComponent,
        MockComponents(
          ImportModalComponent,
          TemporaryPasswordsComponent,
          BetriebsstaetteFilterComponent,
          BenutzerAktivFilterComponent
        ),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(FormsModule),
      ],
      providers: [
        MockProviders(
          BenutzerRestControllerService,
          BenutzerImportAdapterService,
          EventBusService,
          FilenameDateFormatPipe,
          AlertService
        ),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: routeData$.value },
            data: routeData$.asObservable(),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BetreiberbenutzerComponent);
    component = fixture.componentInstance;
    const eventBusService = TestBed.inject(EventBusService);
    spyOn(eventBusService, 'on').mockReturnValue(new Subject());
    const alertService = TestBed.inject(AlertService);
    spyOn(alertService, 'catchError').mockReturnValue(catchError(() => EMPTY));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create tempPasswords with state present', inject(
    [BenutzerRestControllerService],
    (benutzerService: BenutzerRestControllerService) => {
      routeData$.next({
        ...routeData$.value,
        benutzerOhnePasswort: benutzerListe.map((b) => b.benutzername),
      });
      spyOn(benutzerService, 'createTempPasswords').mockReturnValue(EMPTY);

      expect(benutzerService.createTempPasswords).toHaveBeenCalledWith(
        benutzerListe.map((b) => b.id)
      );
    }
  ));

  it('should delete', inject(
    [BenutzerRestControllerService],
    (benutzerService: BenutzerRestControllerService) => {
      spyOn(benutzerService, 'deleteBenutzer').mockReturnValue(of(null));
      const toDelete = benutzerListe[2];

      component.openLoeschenDialog(toDelete);
      component.deleteBenutzer();

      expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith(
        toDelete.benutzername
      );
      expect(component.benutzerListe).toEqual([
        benutzerListe[0],
        benutzerListe[1],
        benutzerListe[3],
      ]);
    }
  ));

  describe('delete multiple benutzer', () => {
    beforeEach(inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        spyOn(benutzerService, 'deleteBenutzer').mockReturnValue(of(null));
        component.selected = component.benutzerListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteMultipleBenutzer();
      }
    ));

    it('should call service', inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('tkrauss');
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('rgarbers');
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove benutzer from list', () => {
      expect(component.benutzerListe).not.toContainEqual(benutzerListe[1]);
      expect(component.benutzerListe).not.toContainEqual(benutzerListe[2]);
    });
  });

  describe('delete multiple benutzer unsuccessful', () => {
    beforeEach(inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        spyOn(benutzerService, 'deleteBenutzer').mockImplementation((name) => {
          return name === 'tkrauss' ? of(null) : throwError('error');
        });
        component.selected = component.benutzerListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteMultipleBenutzer();
      }
    ));

    it('should call service', inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('tkrauss');
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('rgarbers');
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove all successful from list', () => {
      expect(component.benutzerListe.map((b) => b.benutzername)).toEqual([
        'mjoergens',
        'rgarbers',
        'fschaedler',
      ]);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalledWith('error');
      }
    ));
  });

  describe('needsTempPassword', () => {
    it('with nothing selected should be false', () => {
      expect(component.selected).toHaveLength(0);
      expect(component.canCreateTempPasswordsForAllSelected).toBe(false);
    });

    it('select all with tempPassword needed', () => {
      component.selected = [benutzerListe[0], benutzerListe[2]];

      expect(component.canCreateTempPasswordsForAllSelected).toBe(true);
    });

    it('select all with no tempPassword needed', () => {
      component.selected = [benutzerListe[1], benutzerListe[3]];

      expect(component.canCreateTempPasswordsForAllSelected).toBe(false);
    });

    it('select all mixed', () => {
      component.selected = [...benutzerListe];

      expect(component.canCreateTempPasswordsForAllSelected).toBe(false);
    });
  });

  describe('createTempPasswords', () => {
    let response: Subject<Array<BenutzerDto>>;
    beforeEach(inject(
      [BenutzerRestControllerService],
      (benutzerService: jest.Mocked<BenutzerRestControllerService>) => {
        response = new Subject();
        benutzerService.createTempPasswords.mockReturnValue(
          response.asObservable() as Observable<any>
        );
        component.selected = [benutzerListe[0], benutzerListe[2]];
        component.createTempPasswordsForAllSelected();
      }
    ));

    it('should send correct request', inject(
      [BenutzerRestControllerService],
      (benutzerService: jest.Mocked<BenutzerRestControllerService>) => {
        expect(benutzerService.createTempPasswords).toHaveBeenCalledWith([
          benutzerListe[0].id,
          benutzerListe[2].id,
        ]);
      }
    ));

    it('should open dialog in loading state', () => {
      expect(component.showTemporaryPasswords).toBe(true);
      expect(component.benutzerMitEinmalpassworten).toBeFalsy();
    });

    describe('on success', () => {
      let value: Array<BenutzerDto>;
      beforeEach(() => {
        value = component.selected.map((benutzer) => ({
          ...benutzer,
          tempPassword: benutzer + '_PASS',
        }));
        response.next(value);
      });

      it('should deselect', () => {
        expect(component.selected).toHaveLength(0);
      });

      it('should update input', () => {
        expect(component.benutzerMitEinmalpassworten).toBe(value);
      });
    });

    describe('on error', () => {
      let error: any;
      beforeEach(() => {
        error = { message: 'ERROR' };
        response.error(error);
      });

      it('should close dialog', () => {
        expect(component.showTemporaryPasswords).toBe(false);
      });

      it('should show alert', inject(
        [AlertService],
        (alertService: jest.Mocked<AlertService>) => {
          expect(alertService.setAlert).toHaveBeenCalledWith(error);
        }
      ));
    });
  });

  describe('after BenutzerImport', () => {
    let importDone$: Subject<JobStatusDto>;
    beforeEach(inject([EventBusService], (eventBus: EventBusService) => {
      importDone$ = new Subject();
      spyOn(eventBus, 'on').mockReturnValue(importDone$.asObservable());
      component.ngOnInit();
    }));

    it('should reload benutzerListe', inject(
      [BenutzerRestControllerService],
      (benutzerService: jest.Mocked<BenutzerRestControllerService>) => {
        benutzerService.listAllBetreiberBenutzer
          .mockClear()
          .mockImplementation(() => EMPTY);
        expect(component.loading).toBe(false);

        importDone$.next({});

        expect(component.loading).toBe(true);
        expect(benutzerService.listAllBetreiberBenutzer).toHaveBeenCalled();
      }
    ));

    describe('when done reloading', () => {
      let benutzerListe$: Subject<Array<BenutzerDto>>;
      beforeEach(inject(
        [BenutzerRestControllerService],
        (benutzerService: jest.Mocked<BenutzerRestControllerService>) => {
          benutzerListe$ = new Subject();
          benutzerService.listAllBetreiberBenutzer.mockReturnValue(
            benutzerListe$.asObservable() as Observable<any>
          );
        }
      ));

      it('should not be loading anymore', () => {
        importDone$.next({});
        benutzerListe$.next(benutzerListe);

        expect(component.loading).toBe(false);
      });
    });
  });
});
