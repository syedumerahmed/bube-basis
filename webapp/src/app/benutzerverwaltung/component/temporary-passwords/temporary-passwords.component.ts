import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { BenutzerDto } from '@api/benutzer';
import { from } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { ClarityIcons, downloadIcon } from '@cds/core/icon';

ClarityIcons.addIcons(downloadIcon);

@Component({
  selector: 'app-temporary-passwords',
  templateUrl: './temporary-passwords.component.html',
  styleUrls: ['./temporary-passwords.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemporaryPasswordsComponent implements OnChanges {
  private _open = false;
  @Output() openChange = new EventEmitter<boolean>();
  get open(): boolean {
    return this._open;
  }
  @Input() set open(value: boolean) {
    this._open = value;
    this.openChange.next(value);
    if (!value) {
      this.closed.next();
    }
  }
  @Output() closed = new EventEmitter<void>();
  @Input() benutzer: Array<BenutzerDto>;

  private _hideKopiertAlert = true;
  get hideKopiertAlert(): boolean {
    return this._hideKopiertAlert;
  }
  set hideKopiertAlert(value: boolean) {
    this._hideKopiertAlert = value;
    this.changeDetectorRef.markForCheck();
  }

  acknowledged = false;

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  ngOnChanges(): void {
    this.acknowledged = false;
  }

  copyToClipboard(text: string): void {
    from(navigator.clipboard.writeText(text))
      .pipe(
        tap(() => (this.hideKopiertAlert = false)),
        delay(5_000),
        tap(() => (this.hideKopiertAlert = true))
      )
      .subscribe();
  }

  downloadCsv(): void {
    const quote = (s: string): string => `"${s.replace('"', '""')}"`;
    const header = ['Benutzerkennung', 'Name', 'Temporäres Passwort'];
    const content = this.benutzer.map((benutzer) => [
      benutzer.benutzername,
      `${benutzer.vorname ?? ''} ${benutzer.nachname ?? ''}`.trim(),
      benutzer.tempPassword,
    ]);
    const lines = [header.map(quote), ...content.map((b) => b.map(quote))];
    const blob = new Blob([lines.join('\r\n')], {
      type: 'text/csv;charset=utf-8',
    });
    saveAs(blob, 'einmalpassworte.csv');
  }
}
