import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { MockModule } from 'ng-mocks';

import { TemporaryPasswordsComponent } from './temporary-passwords.component';
import { BenutzerDto } from '@api/benutzer';
import { saveAs } from 'file-saver';

jest.mock('file-saver');

describe('TemporaryPasswordsComponent', () => {
  let component: TemporaryPasswordsComponent;
  let fixture: ComponentFixture<TemporaryPasswordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TemporaryPasswordsComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemporaryPasswordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create csv', () => {
    jest
      // @ts-ignore
      .spyOn(global, 'Blob')
      .mockImplementationOnce(
        (blobParts, options) => ({ blobParts, options } as any)
      );
    component.benutzer = [
      {
        benutzername: 'mjoergens',
        vorname: 'Malte',
        nachname: 'Jörgens',
        tempPassword: 'mjoergensPASS',
      },
      {
        benutzername: 'jvoss',
        tempPassword: 'escape"',
      },
    ] as Array<BenutzerDto>;

    component.downloadCsv();

    expect(saveAs).toHaveBeenCalledTimes(1);
    const blob = saveAs.mock.calls[0][0];
    const csv = blob.blobParts[0];
    expect(csv).toMatchInlineSnapshot(`
      "\\"Benutzerkennung\\",\\"Name\\",\\"Temporäres Passwort\\"
      \\"mjoergens\\",\\"Malte Jörgens\\",\\"mjoergensPASS\\"
      \\"jvoss\\",\\"\\",\\"escape\\"\\"\\""
    `);
  });
});
