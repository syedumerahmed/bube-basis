import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BenutzerComponent } from './benutzer.component';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockProviders,
} from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputListComponent } from '@/base/component/input-list/input-list.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import {
  BenutzerDto,
  BenutzerRestControllerService,
  DatenberechtigungFachmoduleEnum,
  DatenberechtigungThemenEnum,
} from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import {
  GESAMTADMIN_DUMMY,
  LAND_TEST_USER,
} from '@/base/security/model/benutzer-profil.mock';
import { LandProperties } from '@/base/model/Land';
import { Rolle } from '@/base/security/model/Rolle';
import { TempPasswordModalComponent } from '../temp-password-modal/temp-password-modal.component';
import { BenutzerRollenComponent } from '../benutzer-rollen/benutzer-rollen.component';
import { BenutzerDatenberechtigungenComponent } from '@/benutzerverwaltung/component/benutzer-datenberechtigungen/benutzer-datenberechtigungen.component';
import Mocked = jest.Mocked;

describe('BenutzerComponent', () => {
  let component: BenutzerComponent;
  let fixture: ComponentFixture<BenutzerComponent>;
  let routeData$: BehaviorSubject<{ benutzer: BenutzerDto }>;

  beforeEach(async () => {
    routeData$ = new BehaviorSubject({
      benutzer: {
        zugewieseneRollen: [],
        benutzername: null,
        canWrite: true,
        datenberechtigung: { land: LandProperties.HAMBURG.nr },
      },
    });

    await TestBed.configureTestingModule({
      declarations: [
        BenutzerComponent,
        BenutzerDatenberechtigungenComponent,
        MockComponents(
          InputListComponent,
          TooltipComponent,
          TempPasswordModalComponent,
          BenutzerRollenComponent
        ),
      ],
      providers: [
        MockProviders(BenutzerRestControllerService, AlertService),
        MockProvider(ActivatedRoute, {
          data: routeData$.asObservable(),
        }),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
      imports: [
        MockModule(RouterTestingModule),
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
      ],
    }).compileComponents();
  });

  describe('without Benutzerverwaltung Recht', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, { useFactory: LAND_TEST_USER });
      fixture = TestBed.createComponent(BenutzerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('edit own user info', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            ...bearbeiter,
            benutzername: bearbeiter.username,
            zugewieseneRollen: [Rolle.LAND_WRITE],
            datenberechtigung: bearbeiter.datenberechtigung,
            canWrite: true,
          },
        });
      }));

      it('should be self', () => {
        expect(component.isSelf).toBe(true);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should not be able to write', () => {
        expect(component.canWrite).toBe(false);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be disabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(false);
      });

      it('rollen should be disabled', () => {
        expect(component.form.controls.rollen.enabled).toBe(false);
      });

      it('should show own rollen', () => {
        expect(component.form.controls.rollen.value).toMatchInlineSnapshot(`
          Object {
            "admin": false,
            "behoerde": null,
            "benutzerverwaltung": null,
            "gesamtadmin": false,
            "land": "LAND_WRITE",
            "referenzenBund": null,
            "referenzenLand": null,
          }
        `);
      });
    });
  });

  describe('with Benutzerverwaltung (lesen) Recht', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, {
        useFactory: () =>
          new BenutzerProfil(
            'benutzerverwaltung_read',
            'benutzerverwaltung',
            'read',
            'read@benutzerverwaltung',
            [Rolle.BENUTZERVERWALTUNG_READ, Rolle.BEHOERDENBENUTZER],
            { land: '01', behoerden: ['01'] }
          ),
      });
      fixture = TestBed.createComponent(BenutzerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('edit own user info', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            ...bearbeiter,
            benutzername: bearbeiter.username,
            zugewieseneRollen: bearbeiter.rollen,
            datenberechtigung: bearbeiter.datenberechtigung,
            canWrite: true,
          },
        });
      }));

      it('should be self', () => {
        expect(component.isSelf).toBe(true);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should not be able to write', () => {
        expect(component.canWrite).toBe(false);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be disabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(false);
      });

      it('rollen should be disabled', () => {
        expect(component.form.controls.rollen.enabled).toBe(false);
      });

      it('should show own rollen', () => {
        expect(component.form.controls.rollen.value).toMatchInlineSnapshot(`
          Object {
            "admin": false,
            "behoerde": null,
            "benutzerverwaltung": "BENUTZERVERWALTUNG_READ",
            "gesamtadmin": false,
            "land": null,
            "referenzenBund": null,
            "referenzenLand": null,
          }
        `);
      });
    });

    describe('read behoerdenbenutzer of own behoerde', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: 'some_other_behoerden_user',
            zugewieseneRollen: [
              Rolle.BEHOERDE_DELETE,
              Rolle.LAND_READ,
              Rolle.BENUTZERVERWALTUNG_WRITE,
            ],
            datenberechtigung: {
              land: bearbeiter.landNr,
              behoerden: [...bearbeiter.datenberechtigung.behoerden],
              akz: ['AKZ'],
              verwaltungsgebiete: ['02100', '02200'],
              betriebsstaetten: ['20', '30', '40'],
              fachmodule: [
                DatenberechtigungFachmoduleEnum.PROTOTYP,
                DatenberechtigungFachmoduleEnum.PRTR_LCP,
              ],
              themen: [DatenberechtigungThemenEnum.WASSER],
            },
            canWrite: true,
          },
        });
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should not be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(false);
      });

      it('should not be able to write', () => {
        expect(component.canWrite).toBe(false);
      });

      it('form should be disabled', () => {
        expect(component.form.enabled).toBe(false);
      });

      it('should be a behoerdenbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(true);
        expect(component.isBetreiber).toBe(false);
      });

      it('should show rollen', () => {
        expect(component.form.controls.rollen.value).toMatchInlineSnapshot(`
          Object {
            "admin": false,
            "behoerde": "BEHOERDE_DELETE",
            "benutzerverwaltung": "BENUTZERVERWALTUNG_WRITE",
            "gesamtadmin": false,
            "land": "LAND_READ",
            "referenzenBund": null,
            "referenzenLand": null,
          }
        `);
      });

      it('should show datenberechtigung', () => {
        expect(component.form.controls.datenberechtigung.value)
          .toMatchInlineSnapshot(`
          Object {
            "akz": Array [
              "AKZ",
            ],
            "behoerden": Array [
              Object {
                "ktext": "01",
                "schluessel": "01",
              },
            ],
            "betriebsstaetten": Array [
              "20",
              "30",
              "40",
            ],
            "fachmodule": Array [
              "PROTOTYP",
              "PRTR_LCP",
            ],
            "land": "SCHLESWIG_HOLSTEIN",
            "themen": Array [
              "WASSER",
            ],
            "verwaltungsgebiete": Array [
              "02100",
              "02200",
            ],
          }
        `);
      });
    });

    describe('read betreiberbenutzer', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: 'some_betreiber_user',
            zugewieseneRollen: [Rolle.BETREIBER],
            datenberechtigung: {
              land: bearbeiter.landNr,
              betriebsstaetten: ['20', '30', '40'],
              fachmodule: [
                DatenberechtigungFachmoduleEnum.PROTOTYP,
                DatenberechtigungFachmoduleEnum.PRTR_LCP,
              ],
              themen: [DatenberechtigungThemenEnum.WASSER],
            },
            canWrite: true,
          },
        });
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should not be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(false);
      });

      it('should not be able to write', () => {
        expect(component.canWrite).toBe(false);
      });

      it('form should be disabled', () => {
        expect(component.form.enabled).toBe(false);
      });

      it('should be a betreiberbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(false);
        expect(component.isBetreiber).toBe(true);
      });

      it('should show datenberechtigung', () => {
        expect(component.form.controls.datenberechtigung.value)
          .toMatchInlineSnapshot(`
          Object {
            "akz": Array [],
            "behoerden": Array [],
            "betriebsstaetten": Array [
              "20",
              "30",
              "40",
            ],
            "fachmodule": Array [
              "PROTOTYP",
              "PRTR_LCP",
            ],
            "land": "SCHLESWIG_HOLSTEIN",
            "themen": Array [
              "WASSER",
            ],
            "verwaltungsgebiete": Array [],
          }
        `);
      });
    });
  });

  describe('with Benutzerverwaltung (schreiben) Recht', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, {
        useFactory: () =>
          new BenutzerProfil(
            'benutzerverwaltung_read',
            'benutzerverwaltung',
            'read',
            'read@benutzerverwaltung',
            [
              Rolle.BENUTZERVERWALTUNG_READ,
              Rolle.BENUTZERVERWALTUNG_WRITE,
              Rolle.BEHOERDENBENUTZER,
            ],
            { land: '01', behoerden: ['01'] }
          ),
      });
      fixture = TestBed.createComponent(BenutzerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('edit own user info', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            ...bearbeiter,
            benutzername: bearbeiter.username,
            zugewieseneRollen: [Rolle.BENUTZERVERWALTUNG_WRITE],
            datenberechtigung: bearbeiter.datenberechtigung,
            canWrite: true,
          },
        });
      }));

      it('should be self', () => {
        expect(component.isSelf).toBe(true);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should be able to write', () => {
        expect(component.canWrite).toBe(true);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be enabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(true);
      });

      it('rollen should be enabled', () => {
        expect(component.form.controls.rollen.enabled).toBe(true);
      });

      it('should show own rollen', () => {
        expect(component.form.controls.rollen.value).toMatchInlineSnapshot(`
          Object {
            "admin": false,
            "behoerde": null,
            "benutzerverwaltung": "BENUTZERVERWALTUNG_WRITE",
            "gesamtadmin": false,
            "land": null,
            "referenzenBund": null,
            "referenzenLand": null,
          }
        `);
      });
    });

    describe('create new Behoerdenbenutzer', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: null,
            zugewieseneRollen: [],
            datenberechtigung: { ...bearbeiter.datenberechtigung },
            canWrite: true,
          },
        });
      }));

      it('should be neu', () => {
        expect(component.isNeu).toBe(true);
      });

      it('should be invalid', () => {
        expect(component.form.invalid).toBe(true);
      });

      describe('fill pflichtfelder', () => {
        beforeEach(() => {
          component.form.controls.benutzername.setValue('newUser');
          component.rollenForm.controls.behoerde.setValue(Rolle.BEHOERDE_READ);
        });

        it('should be valid', () => {
          expect(component.form.valid).toBe(true);
        });

        describe('save', () => {
          let response$: Subject<BenutzerDto>;
          beforeEach(inject(
            [BenutzerRestControllerService],
            (restController: Mocked<BenutzerRestControllerService>) => {
              response$ = new Subject();
              restController.createBenutzer.mockImplementation(
                () => response$.asObservable() as any
              );
              component.save();
            }
          ));

          it('should post user', inject(
            [BenutzerRestControllerService],
            (restController: Mocked<BenutzerRestControllerService>) => {
              expect(restController.createBenutzer).toHaveBeenCalledTimes(1);
              const savedUser = restController.createBenutzer.mock.calls[0][0];
              expect(savedUser).toMatchSnapshot();
            }
          ));

          describe('on success with tempPassword', () => {
            const id = 'd8c870a6-5bad-42aa-9b6a-38b139b3f891';
            const tempPassword = '123456';
            beforeEach(inject(
              [BenutzerRestControllerService],
              (restController: Mocked<BenutzerRestControllerService>) => {
                response$.next({
                  ...restController.createBenutzer.mock.calls[0][0],
                  id,
                  tempPassword,
                });
              }
            ));

            it('should show password modal', () => {
              expect(component.passwordModalOpen).toBe(true);
              expect(component.tempPassword).toBe(tempPassword);
              expect(component.benutzername).toBe('newUser');
            });
          });

          describe('on success without tempPassword', () => {
            const id = 'd8c870a6-5bad-42aa-9b6a-38b139b3f891';
            beforeEach(inject(
              [BenutzerRestControllerService],
              (restController: Mocked<BenutzerRestControllerService>) => {
                response$.next({
                  ...restController.createBenutzer.mock.calls[0][0],
                  id,
                });
              }
            ));

            it('should navigate to new URI', inject(
              [Router],
              (router: Mocked<Router>) => {
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate.mock.calls[0][0]).toEqual([
                  '../benutzer',
                  'newUser',
                ]);
              }
            ));
          });

          describe('on error', () => {
            const error = { error: 'Something went wrong' };

            beforeEach(() => {
              response$.error(error);
            });

            it('should alert', inject(
              [AlertService],
              (alertService: AlertService) => {
                expect(alertService.setAlert).toHaveBeenCalledWith(error);
              }
            ));
          });
        });
      });
    });

    describe('edit behoerdenbenutzer of own behoerde', () => {
      let benutzer: BenutzerDto;

      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        benutzer = {
          aktiv: true,
          email: 'unit@test.de',
          id: '00bc0ecd-054e-40d6-a4c3-9362c2c03359',
          nachname: 'Test',
          telefonnummer: '+49-(0)40-229499-0',
          vorname: 'Unit',
          benutzername: 'unittest',
          zugewieseneRollen: [
            Rolle.BEHOERDE_DELETE,
            Rolle.LAND_READ,
            Rolle.BENUTZERVERWALTUNG_WRITE,
          ],
          datenberechtigung: {
            land: bearbeiter.landNr,
            behoerden: [...bearbeiter.datenberechtigung.behoerden],
            akz: ['AKZ'],
            verwaltungsgebiete: ['02100', '02200'],
            betriebsstaetten: ['20', '30', '40'],
            fachmodule: [
              DatenberechtigungFachmoduleEnum.PROTOTYP,
              DatenberechtigungFachmoduleEnum.PRTR_LCP,
            ],
            themen: [DatenberechtigungThemenEnum.WASSER],
          },
          canWrite: true,
        };
        routeData$.next({ benutzer });
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should be able to write', () => {
        expect(component.canWrite).toBe(true);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be enabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(true);
      });

      it('rollen should be enabled', () => {
        expect(component.form.controls.rollen.enabled).toBe(true);
      });

      it('should be a behoerdenbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(true);
        expect(component.isBetreiber).toBe(false);
      });

      it('should map to form', () => {
        expect(component.form.value).toMatchSnapshot();
      });

      it('should be valid and pristine', () => {
        expect(component.form.valid).toBe(true);
        expect(component.form.pristine).toBe(true);
      });

      describe('edit user', () => {
        beforeEach(() => {
          component.form.patchValue({
            vorname: 'Neuer Vorname',
            rollen: { benutzerverwaltung: Rolle.BENUTZERVERWALTUNG_READ },
            datenberechtigung: {
              betriebsstaetten: [
                ...benutzer.datenberechtigung.betriebsstaetten,
                'zusaetzBST',
              ],
            },
          });
          component.form.markAsTouched();
          component.form.markAsDirty();
        });

        describe('reset', () => {
          beforeEach(() => {
            component.reset();
          });

          it('should be valid and pristine', () => {
            expect(component.form.valid).toBe(true);
            expect(component.form.pristine).toBe(true);
          });

          it('should have original benutzer', () => {
            expect(component.form.value).toMatchSnapshot();
          });
        });

        describe('save', () => {
          let response$: Subject<BenutzerDto>;
          beforeEach(inject(
            [BenutzerRestControllerService],
            (restController: Mocked<BenutzerRestControllerService>) => {
              response$ = new Subject();
              restController.updateBenutzer.mockImplementation(
                () => response$.asObservable() as any
              );
              component.save();
            }
          ));

          it('should post user', inject(
            [BenutzerRestControllerService],
            (restController: Mocked<BenutzerRestControllerService>) => {
              expect(restController.updateBenutzer).toHaveBeenCalledTimes(1);
              const savedUser = restController.updateBenutzer.mock.calls[0][0];
              expect(savedUser).toMatchSnapshot();
            }
          ));

          describe('on success', () => {
            let response: BenutzerDto;
            beforeEach(inject(
              [BenutzerRestControllerService],
              (restController: Mocked<BenutzerRestControllerService>) => {
                response = restController.updateBenutzer.mock.calls[0][0];
                response$.next(response);
              }
            ));

            it('should update user', () => {
              expect(component.benutzer).toBe(response);
            });

            it('should be valid and pristine', () => {
              expect(component.form.valid).toBe(true);
              expect(component.form.pristine).toBe(true);
            });
          });

          describe('on error', () => {
            const error = { error: 'Something went wrong' };

            beforeEach(() => {
              response$.error(error);
            });

            it('should alert', inject(
              [AlertService],
              (alertService: AlertService) => {
                expect(alertService.setAlert).toHaveBeenCalledWith(error);
              }
            ));
          });
        });
      });

      it('should show datenberechtigung', () => {
        expect(component.form.controls.datenberechtigung.value)
          .toMatchInlineSnapshot(`
          Object {
            "akz": Array [
              "AKZ",
            ],
            "behoerden": Array [
              Object {
                "ktext": "01",
                "schluessel": "01",
              },
            ],
            "betriebsstaetten": Array [
              "20",
              "30",
              "40",
            ],
            "fachmodule": Array [
              "PROTOTYP",
              "PRTR_LCP",
            ],
            "themen": Array [
              "WASSER",
            ],
            "verwaltungsgebiete": Array [
              "02100",
              "02200",
            ],
          }
        `);
      });
    });

    describe('read behoerdenbenutzer of other behoerde', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: 'some_other_behoerden_user',
            zugewieseneRollen: [
              Rolle.BEHOERDE_DELETE,
              Rolle.LAND_READ,
              Rolle.BENUTZERVERWALTUNG_WRITE,
            ],
            datenberechtigung: {
              land: bearbeiter.landNr,
              behoerden: ['03'],
              akz: ['AKZ'],
              verwaltungsgebiete: ['02100', '02200'],
              betriebsstaetten: ['20', '30', '40'],
              fachmodule: [
                DatenberechtigungFachmoduleEnum.PROTOTYP,
                DatenberechtigungFachmoduleEnum.PRTR_LCP,
              ],
              themen: [DatenberechtigungThemenEnum.WASSER],
            },
            canWrite: false,
          },
        });
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should not be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(false);
      });

      it('should not be able to write', () => {
        expect(component.canWrite).toBe(false);
      });

      it('form should be disabled', () => {
        expect(component.form.enabled).toBe(false);
      });

      it('should be a behoerdenbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(true);
        expect(component.isBetreiber).toBe(false);
      });

      it('should show rollen', () => {
        expect(component.form.controls.rollen.value).toMatchInlineSnapshot(`
          Object {
            "admin": false,
            "behoerde": "BEHOERDE_DELETE",
            "benutzerverwaltung": "BENUTZERVERWALTUNG_WRITE",
            "gesamtadmin": false,
            "land": "LAND_READ",
            "referenzenBund": null,
            "referenzenLand": null,
          }
        `);
      });

      it('should show datenberechtigung', () => {
        expect(component.form.controls.datenberechtigung.value)
          .toMatchInlineSnapshot(`
          Object {
            "akz": Array [
              "AKZ",
            ],
            "behoerden": Array [
              Object {
                "ktext": "03",
                "schluessel": "03",
              },
            ],
            "betriebsstaetten": Array [
              "20",
              "30",
              "40",
            ],
            "fachmodule": Array [
              "PROTOTYP",
              "PRTR_LCP",
            ],
            "land": "SCHLESWIG_HOLSTEIN",
            "themen": Array [
              "WASSER",
            ],
            "verwaltungsgebiete": Array [
              "02100",
              "02200",
            ],
          }
        `);
      });
    });

    describe('edit betreiberbenutzer', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: 'some_betreiber_user',
            zugewieseneRollen: [Rolle.BETREIBER],
            datenberechtigung: {
              land: bearbeiter.landNr,
              betriebsstaetten: ['20', '30', '40'],
              fachmodule: [
                DatenberechtigungFachmoduleEnum.PROTOTYP,
                DatenberechtigungFachmoduleEnum.PRTR_LCP,
              ],
              themen: [DatenberechtigungThemenEnum.WASSER],
            },
            canWrite: true,
          },
        });
        fixture.detectChanges();
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should be able to write', () => {
        expect(component.canWrite).toBe(true);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be enabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(true);
      });

      it('should have at least one betriebsstätte', () => {
        const betriebsstaettenControl = component.form.get(
          'datenberechtigung.betriebsstaetten'
        );
        betriebsstaettenControl.setValue([]);

        expect(betriebsstaettenControl.invalid).toBe(true);
        expect(betriebsstaettenControl.errors).toEqual({ required: true });
      });

      it('should be a betreiberbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(false);
        expect(component.isBetreiber).toBe(true);
      });

      it('should show datenberechtigung', () => {
        expect(component.form.controls.datenberechtigung.value)
          .toMatchInlineSnapshot(`
          Object {
            "betriebsstaetten": Array [
              "20",
              "30",
              "40",
            ],
            "fachmodule": Array [
              "PROTOTYP",
              "PRTR_LCP",
            ],
            "themen": Array [
              "WASSER",
            ],
          }
        `);
      });
    });
  });

  describe('as Betreiber', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, {
        useFactory: () =>
          new BenutzerProfil(
            'benutzerverwaltung_read',
            'benutzerverwaltung',
            'read',
            'read@benutzerverwaltung',
            [
              Rolle.BENUTZERVERWALTUNG_READ,
              Rolle.BENUTZERVERWALTUNG_WRITE,
              Rolle.BENUTZERVERWALTUNG_DELETE,
              Rolle.BETREIBER,
            ],
            { land: '01', betriebsstaetten: ['01'] }
          ),
      });
      fixture = TestBed.createComponent(BenutzerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('edit own user info', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            ...bearbeiter,
            benutzername: bearbeiter.username,
            zugewieseneRollen: bearbeiter.rollen,
            datenberechtigung: bearbeiter.datenberechtigung,
            canWrite: true,
          },
        });
      }));

      it('should be self', () => {
        expect(component.isSelf).toBe(true);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should be able to write', () => {
        expect(component.canWrite).toBe(true);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be disabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(false);
      });

      it('rollen should be disabled', () => {
        expect(component.form.controls.rollen.enabled).toBe(false);
      });
    });

    describe('edit stellvertreter', () => {
      beforeEach(inject([BenutzerProfil], (bearbeiter: BenutzerProfil) => {
        routeData$.next({
          benutzer: {
            benutzername: 'some_betreiber_user',
            zugewieseneRollen: [Rolle.BETREIBER],
            datenberechtigung: {
              land: bearbeiter.landNr,
              betriebsstaetten: ['20', '30', '40'],
              fachmodule: [
                DatenberechtigungFachmoduleEnum.PROTOTYP,
                DatenberechtigungFachmoduleEnum.PRTR_LCP,
              ],
              themen: [DatenberechtigungThemenEnum.WASSER],
            },
            canWrite: true,
          },
        });
      }));

      it('should not be self', () => {
        expect(component.isSelf).toBe(false);
      });

      it('should be able to edit user info', () => {
        expect(component.canEditUserInfo).toBe(true);
      });

      it('should be able to write', () => {
        expect(component.canWrite).toBe(true);
      });

      it('form should be enabled', () => {
        expect(component.form.enabled).toBe(true);
      });

      it('datenberechtigungen should be disabled', () => {
        expect(component.form.controls.datenberechtigung.enabled).toBe(false);
      });

      it('should be a betreiberbenutzer', () => {
        expect(component.isBehoerdenbenutzer).toBe(false);
        expect(component.isBetreiber).toBe(true);
      });
    });
  });
});
