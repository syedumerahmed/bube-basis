import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@/base/forms';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Observable } from 'rxjs';
import { Land, LandProperties } from '@/base/model/Land';
import { AlertService } from '@/base/service/alert.service';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { Rolle } from '@/base/security/model/Rolle';
import {
  BenutzerDto,
  BenutzerRestControllerService,
  BenutzerZugewieseneRollenEnum,
} from '@api/benutzer';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ReferenzDto } from '@api/stammdaten';
import {
  BenutzerRollenComponent,
  RollenFormData,
} from '../benutzer-rollen/benutzer-rollen.component';
import {
  BenutzerDatenberechtigungenComponent,
  DatenberechtigungenFormData,
} from '../benutzer-datenberechtigungen/benutzer-datenberechtigungen.component';

import {
  ClarityIcons,
  copyIcon,
  floppyIcon,
  timesIcon,
  userIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

ClarityIcons.addIcons(userIcon, timesIcon, floppyIcon, copyIcon);

export interface BenutzerForm {
  benutzername: string;
  vorname: string;
  nachname: string;
  email: string;
  telefonnummer: string;
  aktiv: boolean;
  rollen: RollenFormData;
  datenberechtigung: DatenberechtigungenFormData;
}

@Component({
  selector: 'app-benutzer',
  templateUrl: './benutzer.component.html',
  styleUrls: ['./benutzer.component.scss'],
})
export class BenutzerComponent implements OnInit, UnsavedChanges {
  @ViewChild(ClrForm)
  private clrForm: ClrForm;

  @ViewChild(BenutzerDatenberechtigungenComponent, { static: true })
  private datenberechtigungenComponent: BenutzerDatenberechtigungenComponent;

  benutzer: BenutzerDto;
  form: FormGroup<BenutzerForm>;
  resetDialog: 'PASSWORD' | '2FA';
  passwordModalOpen = false;
  tempPassword: string;
  benutzername: string;
  behoerdenListe: Referenzliste<Required<ReferenzDto>>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    readonly bearbeiter: BenutzerProfil,
    private readonly benutzerService: BenutzerRestControllerService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.benutzer = data.benutzer;
      if (!this.benutzer) {
        throw Error('benutzer === ' + this.benutzer);
      }
      this.behoerdenListe = new Referenzliste(data.behoerden || []);
      this.form = this.createForm(this.toForm(this.benutzer));
    });
  }

  get displayName(): string {
    const { vorname, nachname, benutzername } = this.benutzer;
    return vorname && nachname ? `${vorname} ${nachname}` : benutzername;
  }

  // Rolle + Datenberechtigung

  get canWrite(): boolean {
    return (
      this.benutzer.canWrite &&
      this.bearbeiter.hasRolle(Rolle.BENUTZERVERWALTUNG_WRITE)
    );
  }

  get canEditUserInfo(): boolean {
    return this.isSelf || this.canWrite;
  }

  get canEditLand(): boolean {
    return this.bearbeiter.land === Land.DEUTSCHLAND;
  }

  get canEditDatenberechtigung(): boolean {
    return this.bearbeiter.isBehoerdenbenutzer;
  }

  get isNeu(): boolean {
    return this.benutzer.id == null;
  }

  get isBetreiber(): boolean {
    return this.benutzer.zugewieseneRollen.includes(Rolle.BETREIBER);
  }

  get isBehoerdenbenutzer(): boolean {
    return !this.isBetreiber;
  }

  get isSelf(): boolean {
    return this.benutzer.benutzername === this.bearbeiter.username;
  }

  get benutzerTyp(): string {
    return this.isBetreiber ? 'Betreiberbenutzer' : 'Behördenbenutzer';
  }

  get canResetCredentials(): boolean {
    return !this.isNeu && this.canEditUserInfo;
  }

  get rollenForm(): FormGroup<RollenFormData> {
    return this.form.controls.rollen as FormGroup<RollenFormData>;
  }

  save(): void {
    if (this.form.invalid) {
      this.clrForm.markAsTouched();
      return;
    }
    const benutzerDto: BenutzerDto = this.toDto(this.form.getRawValue());

    let createOrUpdate: Observable<BenutzerDto>;
    if (this.isNeu) {
      createOrUpdate = this.benutzerService.createBenutzer(benutzerDto);
    } else {
      createOrUpdate = this.benutzerService.updateBenutzer(benutzerDto);
    }
    createOrUpdate.subscribe(
      (response) => {
        if (this.isNeu) {
          this.reset();
          if (response.tempPassword) {
            this.openPasswordModal(
              response.benutzername,
              response.tempPassword
            );
            return;
          }
          this.router.navigate(['../benutzer', response.benutzername], {
            relativeTo: this.route,
          });
        } else {
          this.benutzer = response;
          this.reset();
        }
        this.alertService.clearAlertMessages();
      },
      (error) => this.alertService.setAlert(error || error.message)
    );
  }

  reset(): void {
    this.form.reset(this.toForm(this.benutzer));
    if (this.isBetreiber) {
      this.form.controls.rollen.disable({ emitEvent: false });
    }
    this.form.markAsPristine();
  }

  resetCredential(resetDialog: 'PASSWORD' | '2FA'): void {
    let response: Observable<void>;
    switch (resetDialog) {
      case 'PASSWORD':
        response = this.benutzerService.resetPassword(
          this.benutzer.benutzername
        );
        break;
      case '2FA':
        response = this.benutzerService.resetTwoFactor(
          this.benutzer.benutzername
        );
        break;
    }
    response.subscribe(
      () => (this.resetDialog = null),
      (error) => {
        this.alertService.setAlert(error);
        this.resetDialog = null;
      }
    );
  }

  private toForm(benutzer: BenutzerDto): BenutzerForm {
    return {
      benutzername: benutzer.benutzername,
      vorname: benutzer.vorname,
      nachname: benutzer.nachname,
      email: benutzer.email,
      telefonnummer: benutzer.telefonnummer,
      aktiv: benutzer.aktiv,
      rollen: BenutzerRollenComponent.mapRollenToForm(
        benutzer.zugewieseneRollen
      ),
      datenberechtigung: BenutzerDatenberechtigungenComponent.toForm(
        benutzer.datenberechtigung,
        this.findBehordenFromSchluessel(benutzer.datenberechtigung.behoerden)
      ),
    };
  }

  private createForm(benutzer: BenutzerForm): FormGroup<BenutzerForm> {
    const form = this.fb.group<BenutzerForm>({
      benutzername: [
        benutzer.benutzername,
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern('[A-Za-z0-9-_./]*'),
        ],
      ],
      vorname: [benutzer.vorname, Validators.maxLength(80)],
      nachname: [benutzer.nachname, Validators.maxLength(80)],
      email: [benutzer.email, [Validators.maxLength(80), BubeValidators.email]],
      telefonnummer: [benutzer.telefonnummer, Validators.maxLength(80)],
      aktiv: benutzer.aktiv,
      rollen: BenutzerRollenComponent.createRollenForm(
        this.fb,
        this.bearbeiter,
        benutzer.rollen
      ),
      datenberechtigung:
        BenutzerDatenberechtigungenComponent.createDatenberechtigungenForm(
          this.fb,
          benutzer.datenberechtigung,
          this.isBetreiber,
          this.bearbeiter
        ),
    });

    if (this.isBetreiber) {
      form.controls.rollen.disable({ emitEvent: false });
    }
    if (!this.canWrite) {
      if (this.canEditUserInfo) {
        form.controls.rollen.disable({ emitEvent: false });
        form.controls.datenberechtigung.disable({ emitEvent: false });
      } else {
        form.disable({ emitEvent: false });
      }
    }
    return form;
  }

  private toDto(formValue: BenutzerForm): BenutzerDto {
    const { vorname, email, telefonnummer, rollen, aktiv, nachname } =
      formValue;
    return {
      id: this.benutzer.id,
      benutzername: this.benutzer.benutzername ?? formValue.benutzername,
      datenberechtigung: !this.canEditDatenberechtigung
        ? this.benutzer.datenberechtigung
        : {
            ...formValue.datenberechtigung,
            land: this.canEditLand
              ? LandProperties[formValue.datenberechtigung.land].nr
              : this.benutzer.datenberechtigung.land,
            behoerden:
              formValue.datenberechtigung.behoerden?.map(
                (behoerde) => behoerde.schluessel
              ) ?? [],
          },
      zugewieseneRollen: this.mapRollenToDto(rollen),
      aktiv,
      email,
      nachname,
      telefonnummer,
      vorname,
    };
  }

  private mapRollenToDto(
    rollen: RollenFormData
  ): BenutzerZugewieseneRollenEnum[] {
    if (this.isBetreiber) {
      return [BenutzerZugewieseneRollenEnum.BETREIBER];
    }

    const zugewieseneRollen: BenutzerZugewieseneRollenEnum[] = [
      Rolle.BEHOERDENBENUTZER,
      rollen.land,
      rollen.behoerde,
      rollen.referenzenBund,
      rollen.referenzenLand,
      rollen.benutzerverwaltung,
    ].filter(Boolean);
    if (rollen.admin) {
      zugewieseneRollen.push(BenutzerZugewieseneRollenEnum.ADMIN);
    }
    if (rollen.gesamtadmin) {
      zugewieseneRollen.push(BenutzerZugewieseneRollenEnum.GESAMTADMIN);
    }
    return zugewieseneRollen;
  }

  navigateBack(): void {
    const to = this.isBetreiber ? 'betreiber' : 'behoerden';
    this.router.navigate([to], { relativeTo: this.route.parent });
  }

  private openPasswordModal(benutzername: string, tempPassword: string): void {
    this.tempPassword = tempPassword;
    this.benutzername = benutzername;
    this.passwordModalOpen = true;
  }

  onPasswordModalClosed(): void {
    this.tempPassword = null;
    this.router.navigate(['../benutzer', this.benutzername], {
      relativeTo: this.route,
    });
    this.benutzername = null;
  }

  private findBehordenFromSchluessel(
    behoerdenSchluessel: Array<string>
  ): Required<ReferenzDto>[] {
    if (!behoerdenSchluessel) {
      return [];
    }
    const behoerden = this.behoerdenListe.values;
    return behoerdenSchluessel.map(
      (schluessel) =>
        behoerden.find((a) => schluessel === a.schluessel) ??
        // Dummy Objekt zur Anzeige nicht gefundener Behoerden
        ({ ktext: schluessel, schluessel } as Required<ReferenzDto>)
    );
  }

  canDeactivate(): boolean {
    return this.form.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
