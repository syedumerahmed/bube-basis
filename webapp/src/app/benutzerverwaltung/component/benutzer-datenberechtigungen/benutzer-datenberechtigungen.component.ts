import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@/base/forms';
import { ReferenzDto } from '@api/stammdaten';
import {
  DatenberechtigungDto,
  DatenberechtigungFachmoduleEnum,
  DatenberechtigungThemenEnum,
} from '@api/benutzer';
import { Land, LandProperties, lookupLand } from '@/base/model/Land';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { ValidatorFn, Validators } from '@angular/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';

export interface DatenberechtigungenFormData {
  land: string;
  behoerden: Array<ReferenzDto>;
  akz: Array<string>;
  verwaltungsgebiete: Array<string>;
  betriebsstaetten: Array<string>;
  fachmodule: Array<DatenberechtigungFachmoduleEnum>;
  themen: Array<DatenberechtigungThemenEnum>;
}

@Component({
  selector: 'app-benutzer-datenberechtigungen',
  templateUrl: './benutzer-datenberechtigungen.component.html',
  styleUrls: ['./benutzer-datenberechtigungen.component.scss'],
})
export class BenutzerDatenberechtigungenComponent {
  readonly LandProperties = LandProperties;
  readonly fachmoduleDisplayText: Record<
    DatenberechtigungFachmoduleEnum,
    string
  > = {
    E_ERKLAERUNG: 'E-Erklärung',
    PROTOTYP: 'Prototyp',
    PRTR_LCP: 'PRTR+LCP',
    RECHERCHE: 'Recherche',
  };
  readonly themenDisplayText: Record<DatenberechtigungThemenEnum, string> = {
    ABFALL: 'Abfall',
    BODEN: 'Boden',
    LCP: 'LCP',
    LUFT: 'Luft',
    WASSER: 'Wasser',
  };

  readonly fachmodule: Array<DatenberechtigungFachmoduleEnum> = Object.values(
    DatenberechtigungFachmoduleEnum
  ).filter((fachmodul) => {
    const fachmodule = this.bearbeiter.datenberechtigung.fachmodule;
    return !fachmodule?.length || fachmodule.includes(fachmodul);
  });

  readonly themen: Array<DatenberechtigungThemenEnum> = Object.values(
    DatenberechtigungThemenEnum
  ).filter((thema) => {
    const themen = this.bearbeiter.datenberechtigung.themen;
    return !themen?.length || themen.includes(thema);
  });

  @Input()
  form: FormGroup<DatenberechtigungenFormData>;
  @Input()
  behoerdenListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  isBetreiber: boolean;

  static subsetOfValidator(superset: string[]): ValidatorFn {
    return (control: FormControl<string[]>) => {
      if (!superset || superset.length === 0) {
        return null;
      }
      if (control.value.length === 0) {
        return { subsetOf: { oneOf: superset } };
      }
      const noAccess = control.value.filter((e) => !superset.includes(e));
      if (noAccess.length !== 0) {
        return { subsetOf: { noAccess } };
      }
      return null;
    };
  }

  static behoerdenValidator(bearbeiterBehoerden: string[]): ValidatorFn {
    return (control: FormControl<Array<ReferenzDto>>) => {
      if (!bearbeiterBehoerden || bearbeiterBehoerden.length === 0) {
        return null;
      }
      if (!control.value?.length) {
        return { subsetOf: { oneOf: bearbeiterBehoerden } };
      }
      const noAccess = control.value
        .map((b) => b.schluessel)
        .filter((e) => !bearbeiterBehoerden.includes(e));
      if (noAccess.length !== 0) {
        return { subsetOf: { noAccess } };
      }
      return null;
    };
  }

  static verwaltungsgebieteValidator(
    verwaltungsgebiete: string[]
  ): ValidatorFn {
    return (control: FormControl<string[]>) => {
      if (!verwaltungsgebiete || verwaltungsgebiete.length === 0) {
        return null;
      }
      if (control.value.length === 0) {
        return { verwaltungsgebieteIn: { oneOf: verwaltungsgebiete } };
      }
      const noAccess = control.value.filter(
        (e) => !verwaltungsgebiete.some((v) => e.startsWith(v))
      );
      if (noAccess.length !== 0) {
        return { verwaltungsgebieteIn: { noAccess } };
      }
      return null;
    };
  }

  static toForm(
    datenberechtigung: DatenberechtigungDto,
    behoerden: Required<ReferenzDto>[]
  ): DatenberechtigungenFormData {
    return {
      land: lookupLand(datenberechtigung.land),
      behoerden: behoerden,
      akz: datenberechtigung.akz?.slice() || [],
      verwaltungsgebiete: datenberechtigung.verwaltungsgebiete?.slice() || [],
      betriebsstaetten: datenberechtigung.betriebsstaetten?.slice() || [],
      fachmodule: datenberechtigung.fachmodule?.slice() || [],
      themen: datenberechtigung.themen?.slice() || [],
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly bearbeiter: BenutzerProfil
  ) {}

  static createDatenberechtigungenForm(
    fb: FormBuilder,
    datenberechtigung: DatenberechtigungenFormData,
    isBetreiber: boolean,
    bearbeiter: BenutzerProfil
  ): FormGroup<DatenberechtigungenFormData> {
    const form = fb.group({
      land: {
        value: datenberechtigung.land,
        disabled: bearbeiter.land !== Land.DEUTSCHLAND,
      },
      behoerden: [
        {
          value: datenberechtigung.behoerden,
          disabled: isBetreiber,
        },
        BenutzerDatenberechtigungenComponent.behoerdenValidator(
          bearbeiter.datenberechtigung.behoerden
        ),
      ],
      akz: [
        {
          value: datenberechtigung.akz,
          disabled: isBetreiber,
        },
        BenutzerDatenberechtigungenComponent.subsetOfValidator(
          bearbeiter.datenberechtigung.akz
        ),
      ],
      verwaltungsgebiete: [
        {
          value: datenberechtigung.verwaltungsgebiete,
          disabled: isBetreiber,
        },
        BenutzerDatenberechtigungenComponent.verwaltungsgebieteValidator(
          bearbeiter.datenberechtigung.verwaltungsgebiete
        ),
      ],
      betriebsstaetten: [
        {
          value: datenberechtigung.betriebsstaetten,
          disabled: !bearbeiter.isBehoerdenbenutzer,
        },
        [
          isBetreiber ? Validators.required : Validators.nullValidator,
          BenutzerDatenberechtigungenComponent.subsetOfValidator(
            bearbeiter.datenberechtigung.betriebsstaetten
          ),
        ],
      ],
      fachmodule: [
        {
          value: datenberechtigung.fachmodule,
          disabled: !bearbeiter.isBehoerdenbenutzer,
        },
        bearbeiter.datenberechtigung.fachmodule.length
          ? Validators.required
          : Validators.nullValidator,
      ],
      themen: [
        {
          value: datenberechtigung.themen,
          disabled:
            !bearbeiter.isBehoerdenbenutzer ||
            !BenutzerDatenberechtigungenComponent.includesPRTR(
              datenberechtigung.fachmodule
            ),
        },
        bearbeiter.datenberechtigung.themen.length
          ? Validators.required
          : Validators.nullValidator,
      ],
    });
    form.controls.fachmodule.valueChanges.subscribe((fachmodulValues) => {
      if (BenutzerDatenberechtigungenComponent.includesPRTR(fachmodulValues)) {
        form.controls.themen.enable();
      } else {
        form.controls.themen.patchValue([]);
        form.controls.themen.disable();
      }
    });
    return form;
  }

  private static includesPRTR(
    fachmodulValues: Array<DatenberechtigungFachmoduleEnum>
  ): boolean {
    return fachmodulValues?.includes('PRTR_LCP');
  }
}
