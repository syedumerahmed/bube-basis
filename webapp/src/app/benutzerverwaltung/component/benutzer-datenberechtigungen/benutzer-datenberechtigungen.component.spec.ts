import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenutzerDatenberechtigungenComponent } from './benutzer-datenberechtigungen.component';
import { BaseModule } from '@/base/base.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { MockModule } from 'ng-mocks';
import { FormBuilder, FormControl } from '@/base/forms';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { ReferenzDto } from '@api/referenzdaten';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';

describe('BenutzerDatenberechtigungenComponent', () => {
  let component: BenutzerDatenberechtigungenComponent;
  let fixture: ComponentFixture<BenutzerDatenberechtigungenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BenutzerDatenberechtigungenComponent],
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
      imports: [
        MockModule(FormsModule),
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(BaseModule),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenutzerDatenberechtigungenComponent);
    component = fixture.componentInstance;
    component.form =
      BenutzerDatenberechtigungenComponent.createDatenberechtigungenForm(
        new FormBuilder(),
        BenutzerDatenberechtigungenComponent.toForm({ land: '00' }, []),
        false,
        GESAMTADMIN_DUMMY()
      );
    component.behoerdenListe = new Referenzliste<Required<ReferenzDto>>([]);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Validators', () => {
  describe('subsetOfValidator', () => {
    const validator = BenutzerDatenberechtigungenComponent.subsetOfValidator;
    describe('empty set', () => {
      let control;
      beforeEach(() => {
        control = new FormControl([]);
      });
      it('should be valid with unrestricted bearbeiter', () => {
        expect(validator(null)(control)).toBeNull();
        expect(validator([])(control)).toBeNull();
      });
      it('should be invalid with restricted bearbeiter', () => {
        expect(validator(['BHD1'])(control)).toEqual({
          subsetOf: { oneOf: ['BHD1'] },
        });
      });
    });
    describe('subset', () => {
      let control;
      beforeEach(() => {
        control = new FormControl(['BHD1', 'BHD2']);
      });
      it('should be valid with unrestricted bearbeiter', () => {
        expect(validator(null)(control)).toBeNull();
        expect(validator([])(control)).toBeNull();
      });
      it('should be invalid when no subset', () => {
        expect(validator(['BHD1'])(control)).toEqual({
          subsetOf: { noAccess: ['BHD2'] },
        });
      });
      it('should be valid when equal', () => {
        expect(validator(['BHD2', 'BHD1'])(control)).toBeNull();
      });
      it('should be valid when subset', () => {
        expect(validator(['BHD2', 'BHD1', 'BHD3'])(control)).toBeNull();
      });
    });
  });
  describe('verwaltungsgebietValidator', () => {
    const validator =
      BenutzerDatenberechtigungenComponent.verwaltungsgebieteValidator;

    describe('empty set', () => {
      let control;
      beforeEach(() => {
        control = new FormControl([]);
      });
      it('should be valid with unrestricted bearbeiter', () => {
        expect(validator(null)(control)).toBeNull();
        expect(validator([])(control)).toBeNull();
      });
      it('should be invalid with restricted bearbeiter', () => {
        expect(validator(['LLBKK'])(control)).toEqual({
          verwaltungsgebieteIn: { oneOf: ['LLBKK'] },
        });
      });
    });
    describe('subset', () => {
      let control;
      beforeEach(() => {
        control = new FormControl(['LLBKK', 'HHBKK']);
      });
      it('should be valid with unrestricted bearbeiter', () => {
        expect(validator(null)(control)).toBeNull();
        expect(validator([])(control)).toBeNull();
      });
      it('should be invalid when no subset', () => {
        expect(validator(['LLBKK'])(control)).toEqual({
          verwaltungsgebieteIn: { noAccess: ['HHBKK'] },
        });
      });
      it('should be valid when equal', () => {
        expect(validator(['LLBKK', 'HHBKK'])(control)).toBeNull();
      });
      it('should be valid with prefix', () => {
        expect(validator(['LLB', 'HH'])(control)).toBeNull();
      });
      it('should be valid when subset', () => {
        expect(validator(['LLBKK', 'HHBKK', 'BEBKK'])(control)).toBeNull();
      });
    });
  });
});
