import { Component } from '@angular/core';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Component({
  selector: 'app-benutzerverwaltung',
  templateUrl: './benutzerverwaltung.component.html',
  styleUrls: ['./benutzerverwaltung.component.scss'],
})
export class BenutzerverwaltungComponent {
  constructor(private readonly benutzer: BenutzerProfil) {}

  get canSeeBenutzerverwaltungBehoerdenbenutzer(): boolean {
    return (
      this.benutzer.canSeeBenutzerverwaltungModule &&
      this.benutzer.isBehoerdenbenutzer
    );
  }
}
