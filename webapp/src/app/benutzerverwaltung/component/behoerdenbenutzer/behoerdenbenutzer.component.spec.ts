import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BehoerdenbenutzerComponent } from './behoerdenbenutzer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { BehoerdeFilterComponent } from '@/benutzerverwaltung/component/behoerde-filter/behoerde-filter.component';
import { BenutzerAktivFilterComponent } from '@/benutzerverwaltung/component/benutzer-aktiv-filter/benutzer-aktiv-filter.component';
import { FormsModule } from '@angular/forms';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties } from '@/base/model/Land';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { Rolle } from '@/base/security/model/Rolle';
import spyOn = jest.spyOn;

describe('BehoerdenbenutzerComponent', () => {
  let component: BehoerdenbenutzerComponent;
  let fixture: ComponentFixture<BehoerdenbenutzerComponent>;
  let benutzerListe: BenutzerDto[];

  beforeEach(async () => {
    benutzerListe = ['mjoergens', 'tkrauss', 'rgarbers', 'fschaedler'].map(
      (benutzername) => ({
        id: benutzername,
        benutzername,
        datenberechtigung: { land: LandProperties.DEUTSCHLAND.nr },
        zugewieseneRollen: [Rolle.GESAMTADMIN],
      })
    );
    await TestBed.configureTestingModule({
      declarations: [
        BehoerdenbenutzerComponent,
        BehoerdeFilterComponent,
        BenutzerAktivFilterComponent,
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(FormsModule),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: BenutzerRestControllerService,
          useValue: MockService(BenutzerRestControllerService),
        },
        { provide: AlertService, useValue: MockService(AlertService) },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: { benutzer: benutzerListe } },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BehoerdenbenutzerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should delete', inject(
    [BenutzerRestControllerService],
    (benutzerService: BenutzerRestControllerService) => {
      spyOn(benutzerService, 'deleteBenutzer').mockReturnValue(of(null));
      const toDelete = benutzerListe[2];

      component.openLoeschenDialog(toDelete);
      component.deleteBenutzer();

      expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith(
        toDelete.benutzername
      );
      expect(component.benutzerListe).toHaveLength(3);
    }
  ));

  describe('delete multiple benutzer', () => {
    beforeEach(inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        spyOn(benutzerService, 'deleteBenutzer').mockReturnValue(of(null));
        component.selected = component.benutzerListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteMultipleBenutzer();
      }
    ));

    it('should call service', inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('tkrauss');
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('rgarbers');
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove behoerden from list', () => {
      expect(component.benutzerListe).not.toContainEqual(benutzerListe[1]);
      expect(component.benutzerListe).not.toContainEqual(benutzerListe[2]);
    });
  });

  describe('delete multiple benutzer unsuccessful', () => {
    beforeEach(inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        spyOn(benutzerService, 'deleteBenutzer').mockImplementation((name) => {
          return name === 'tkrauss' ? of(null) : throwError('error');
        });
        component.selected = component.benutzerListe.slice(1, 3);
        component.openMarkierteLoeschenDialog();
        component.deleteMultipleBenutzer();
      }
    ));

    it('should call service', inject(
      [BenutzerRestControllerService],
      (benutzerService: BenutzerRestControllerService) => {
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('tkrauss');
        expect(benutzerService.deleteBenutzer).toHaveBeenCalledWith('rgarbers');
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialog).toBe(false);
    });

    it('should remove all successful from list', () => {
      expect(component.benutzerListe.map((b) => b.benutzername)).toEqual([
        'mjoergens',
        'rgarbers',
        'fschaedler',
      ]);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalledWith('error');
      }
    ));
  });
});
