import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { AlertService } from '@/base/service/alert.service';
import { RolleProperties } from '@/base/security/model/Rolle';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { ClrDatagridSortOrder } from '@clr/angular';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';
import {
  checkIcon,
  ClarityIcons,
  eyeIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  usersIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(
  checkIcon,
  usersIcon,
  timesIcon,
  trashIcon,
  plusIcon,
  eyeIcon
);

@Component({
  selector: 'app-behoerdenbenutzer',
  templateUrl: './behoerdenbenutzer.component.html',
  styleUrls: ['./behoerdenbenutzer.component.scss'],
})
export class BehoerdenbenutzerComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private benutzerProfil: BenutzerProfil,
    private benutzerRestControllerService: BenutzerRestControllerService,
    private alertService: AlertService
  ) {}

  readonly SortOrder = ClrDatagridSortOrder;

  get canWrite(): boolean {
    return this.benutzerProfil.canWriteBenutzer;
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  behoerden: Array<BehoerdeListItem> = [];
  loeschenDialog: boolean;
  benutzerListe: Array<BenutzerDto>;
  benutzer: BenutzerDto;
  selected: Array<BenutzerDto> = [];
  loescht = false;

  ngOnInit(): void {
    this.behoerden = this.route.snapshot.data.behoerden;
    this.benutzerListe = this.route.snapshot.data.benutzer;

    this.benutzerListe.forEach((benutzer) =>
      this.setRollenNameToDisplay(benutzer)
    );
  }

  deleteBenutzer(): void {
    this.loeschen(this.benutzer.benutzername);
    this.closeLoeschenDialog();
  }

  deleteMultipleBenutzer(): void {
    this.loescht = true;
    this.selected
      .map((b) => b.benutzername)
      .forEach((name) => this.loeschen(name));
    this.closeLoeschenDialog();
  }

  private loeschen(name: string): void {
    this.benutzerRestControllerService.deleteBenutzer(name).subscribe(
      () => {
        const index = this.benutzerListe.findIndex(
          (b) => b.benutzername === name
        );
        if (index !== -1) {
          this.benutzerListe.splice(index, 1);
        }
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  get canDelete(): boolean {
    return this.benutzerProfil.canDeleteBenutzer;
  }

  get canDeleteSelection(): boolean {
    if (this.nothingSelected) {
      return false;
    }
    return this.selected.every((benutzer) => this.canDeleteBenutzer(benutzer));
  }

  canDeleteBenutzer(benutzer: BenutzerDto): boolean {
    return (
      benutzer.canDelete &&
      benutzer.benutzername !== this.benutzerProfil.username
    );
  }

  getAktiv(benutzer: BenutzerDto): string {
    return benutzer.aktiv ? 'aktiv' : 'inaktiv';
  }

  // Zur Anzeige der Rollennamen. Damit Filter und Sortierung auch funktionieren werden diese in das Propertie zuück geschrieben
  setRollenNameToDisplay(benutzer: BenutzerDto): void {
    const displayStrings = [];

    benutzer.zugewieseneRollen.forEach((rolle) => {
      displayStrings.push(RolleProperties[rolle].display);
    });

    benutzer.zugewieseneRollen = displayStrings;
  }

  getRollen(benutzer: BenutzerDto): string {
    return benutzer.zugewieseneRollen.join(', ').toString();
  }

  getBehoerden(benutzer: BenutzerDto): string {
    return benutzer.datenberechtigung.behoerden.join(', ').toString();
  }

  getAKZ(benutzer: BenutzerDto): string {
    return benutzer.datenberechtigung.akz.join(', ').toString();
  }

  getVerwaltungsgebiete(benutzer: BenutzerDto): string {
    return benutzer.datenberechtigung.verwaltungsgebiete.join(', ').toString();
  }

  getBetriebsstaetten(benutzer: BenutzerDto): string {
    return benutzer.datenberechtigung.betriebsstaetten.join(', ').toString();
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(benutzer: BenutzerDto): void {
    this.benutzer = benutzer;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.benutzer = null;
    this.loeschenDialog = true;
  }

  alleMarkieren(): void {
    this.selected = this.benutzerListe;
  }

  markierungAufheben(): void {
    this.selected = [];
  }
}
