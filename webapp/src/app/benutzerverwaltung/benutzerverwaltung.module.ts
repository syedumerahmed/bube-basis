import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenutzerverwaltungRoutingModule } from './routing/benutzerverwaltung-routing.module';
import { BenutzerverwaltungComponent } from './component/benutzerverwaltung/benutzerverwaltung.component';
import { BehoerdenbenutzerComponent } from './component/behoerdenbenutzer/behoerdenbenutzer.component';
import { BetreiberbenutzerComponent } from './component/betreiberbenutzer/betreiberbenutzer.component';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BenutzerComponent } from './component/benutzer/benutzer.component';
import { BaseModule } from '@/base/base.module';
import { BehoerdeFilterComponent } from './component/behoerde-filter/behoerde-filter.component';
import { BetriebsstaetteFilterComponent } from './component/betriebsstaette-filter/betriebsstaette-filter.component';
import { BenutzerAktivFilterComponent } from './component/benutzer-aktiv-filter/benutzer-aktiv-filter.component';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { TempPasswordModalComponent } from './component/temp-password-modal/temp-password-modal.component';
import { BenutzerRollenComponent } from './component/benutzer-rollen/benutzer-rollen.component';
import { BenutzerDatenberechtigungenComponent } from './component/benutzer-datenberechtigungen/benutzer-datenberechtigungen.component';
import { TemporaryPasswordsComponent } from './component/temporary-passwords/temporary-passwords.component';

@NgModule({
  declarations: [
    BenutzerverwaltungComponent,
    BehoerdenbenutzerComponent,
    BetreiberbenutzerComponent,
    BenutzerComponent,
    BehoerdeFilterComponent,
    BetriebsstaetteFilterComponent,
    BenutzerAktivFilterComponent,
    TempPasswordModalComponent,
    BenutzerRollenComponent,
    BenutzerDatenberechtigungenComponent,
    TemporaryPasswordsComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BaseModule,
    BenutzerverwaltungRoutingModule,
  ],
  providers: [FilenameDateFormatPipe],
})
export class BenutzerverwaltungModule {}
