import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable({ providedIn: 'root' })
export class BenutzerverwaltungGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly location: Location,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly alertService: AlertService
  ) {}

  private static getOwnProfileUrl(benutzer: BenutzerProfil) {
    return `/benutzerverwaltung/${
      benutzer.isBehoerdenbenutzer ? 'behoerden' : 'betreiber'
    }/benutzer/${benutzer.username}`;
  }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    const isAuthorized =
      this.benutzerProfil.canSeeBenutzerverwaltungModule ||
      state.url ===
        BenutzerverwaltungGuard.getOwnProfileUrl(this.benutzerProfil);
    if (!isAuthorized) {
      this.alertService.setAlert('Das gesuchte Element wurde nicht gefunden');
      await this.router
        .navigate(['/404'])
        .then(() => this.location.replaceState(state.url));
    }
    return isAuthorized;
  }
}
