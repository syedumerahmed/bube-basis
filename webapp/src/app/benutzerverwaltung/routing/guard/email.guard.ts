import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable({
  providedIn: 'root',
})
export class EmailGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil
  ) {}

  canActivate(): boolean | UrlTree {
    if (!this.benutzerProfil.email) {
      return this.router.createUrlTree(['update-profile']);
    }
    return true;
  }
}
