import { Router, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BenutzerverwaltungGuard } from './benutzerverwaltung.guard';
import { inject, TestBed } from '@angular/core/testing';
import { MockProviders } from 'ng-mocks';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe(BenutzerverwaltungGuard.name, () => {
  let guard: BenutzerverwaltungGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BenutzerverwaltungGuard,
        MockProviders(Router, Location, AlertService),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
    });
    guard = TestBed.inject(BenutzerverwaltungGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should allow', inject(
    [BenutzerProfil],
    async (benutzerProfil: BenutzerProfil) => {
      spyOn(
        benutzerProfil,
        'canSeeBenutzerverwaltungModule',
        'get'
      ).mockReturnValue(true);
      const isAuthorized = await guard.canActivate(null, {
        url: '/betriebsstaetten',
      } as RouterStateSnapshot);
      expect(isAuthorized).toBe(true);
    }
  ));

  describe('no access to Benutzerverwaltung', () => {
    beforeEach(inject([BenutzerProfil], (benutzerProfil: BenutzerProfil) => {
      spyOn(
        benutzerProfil,
        'canSeeBenutzerverwaltungModule',
        'get'
      ).mockReturnValue(false);
    }));

    it('should have access to own profile as behoerde', inject(
      [BenutzerProfil],
      async (benutzerProfil: BenutzerProfil) => {
        spyOn(benutzerProfil, 'isBehoerdenbenutzer', 'get').mockReturnValue(
          true
        );
        const isAuthorized = await guard.canActivate(null, {
          url: `/benutzerverwaltung/behoerden/benutzer/${benutzerProfil.username}`,
        } as RouterStateSnapshot);
        expect(isAuthorized).toBe(true);
      }
    ));

    it('should have access to own profile as betreiber', inject(
      [BenutzerProfil],
      async (benutzerProfil: BenutzerProfil) => {
        spyOn(benutzerProfil, 'isBehoerdenbenutzer', 'get').mockReturnValue(
          false
        );
        const isAuthorized = await guard.canActivate(null, {
          url: `/benutzerverwaltung/betreiber/benutzer/${benutzerProfil.username}`,
        } as RouterStateSnapshot);
        expect(isAuthorized).toBe(true);
      }
    ));

    it('should deny and redirect', inject(
      [Router, Location, AlertService],
      async (router: Router, location: Location, alertService) => {
        spyOn(router, 'navigate').mockResolvedValue(true);
        const url = '/benutzerverwaltung/behoerden/benutzer/username';
        const isAuthorized = await guard.canActivate(null, {
          url,
        } as RouterStateSnapshot);

        expect(isAuthorized).toBe(false);
        expect(router.navigate).toHaveBeenCalledWith(['/404']);
        expect(location.replaceState).toHaveBeenCalledWith(url);
        expect(alertService.setAlert).toHaveBeenCalled();
      }
    ));
  });
});
