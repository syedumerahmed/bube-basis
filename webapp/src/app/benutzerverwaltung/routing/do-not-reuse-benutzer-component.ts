import {
  ActivatedRouteSnapshot,
  BaseRouteReuseStrategy,
} from '@angular/router';
import { BenutzerComponent } from '@/benutzerverwaltung/component/benutzer/benutzer.component';

/**
 * Ich wünschte, ich hätte diese Klasse nicht schreiben müssen.
 *
 * Bei der BenutzerComponent geht irgendwie die DOM-Synchronisierung mit der
 * Form kaputt, wenn man von einem Benutzer direkt auf einen anderen navigiert,
 * für den andere Berechtigungen gelten (z.B. vorher anderen Benutzer im
 * Lesen-Modus angesehen und dann das eigene Benutzerprofil öffnen).
 *
 * Die FormGroup in der BenutzerComponent wird korrekt enabled/disabled, aber
 * das wirkt sich nicht die DOM-Eigenschaften aus (sowohl required-Attribut
 * als auch Validatoren, die DOM-Elemente hängen dann ziemlich tot rum)
 *
 * Dieses explizite nicht-reusen der Komponente führt dazu, dass sie einmal
 * ganz abgeräumt und vollständig neu initialisiert wird, dann klappt auch
 * erstmal alles.
 *
 * Und leider muss diese Sonderlocke auch noch in das Root-App-Modul
 * eingebunden werden...
 */
export class DoNotReuseBenutzerComponent extends BaseRouteReuseStrategy {
  shouldReuseRoute(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot
  ): boolean {
    return (
      super.shouldReuseRoute(future, curr) &&
      future.component !== BenutzerComponent
    );
  }
}
