import { inject, TestBed } from '@angular/core/testing';

import { NeuerBehoerdenbenutzerResolver } from './neuer-behoerdenbenutzer-resolver';
import { MockService } from 'ng-mocks';
import { Observable, of } from 'rxjs';
import {
  BenutzerDto,
  BenutzerRestControllerService,
  BenutzerZugewieseneRollenEnum,
} from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties } from '@/base/model/Land';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('NeuerBehördenbenutzerResolver', () => {
  let resolver: NeuerBehoerdenbenutzerResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NeuerBehoerdenbenutzerResolver,
        {
          provide: BenutzerRestControllerService,
          useValue: MockService(BenutzerRestControllerService),
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
    });
    resolver = TestBed.inject(NeuerBehoerdenbenutzerResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should use datenberechtigungen of creator', inject(
    [BenutzerRestControllerService],
    async (benutzerService: BenutzerRestControllerService) => {
      const ownBenutzer: BenutzerDto = {
        benutzername: 'me',
        datenberechtigung: {
          land: LandProperties.THUERINGEN.nr,
          behoerden: ['BHD1', 'BHD2'],
          betriebsstaetten: ['BST1', 'BST2'],
        },
        zugewieseneRollen: [
          BenutzerZugewieseneRollenEnum.BENUTZERVERWALTUNG_WRITE,
          BenutzerZugewieseneRollenEnum.LAND_READ,
        ],
      };
      spyOn(benutzerService, 'loadBenutzer').mockReturnValue(
        of(ownBenutzer) as Observable<any>
      );

      const benutzerDto = await resolver.resolve().toPromise();

      expect(benutzerDto).toBeDefined();
      expect(benutzerDto.datenberechtigung).toEqual(
        ownBenutzer.datenberechtigung
      );
      expect(benutzerDto.zugewieseneRollen).toEqual(
        ownBenutzer.zugewieseneRollen
      );
      expect(benutzerDto.benutzername).toBeFalsy();
      expect(benutzerDto.id).toBeFalsy();
    }
  ));
});
