import { inject, TestBed } from '@angular/core/testing';

import { NeuerBetreiberbenutzerResolver } from './neuer-betreiberbenutzer-resolver';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { LandProperties } from '@/base/model/Land';
import {
  BenutzerZugewieseneRollenEnum,
  DatenberechtigungFachmoduleEnum,
  DatenberechtigungThemenEnum,
} from '@api/benutzer';

describe('NeuerBetreiberbenutzerResolver', () => {
  let resolver: NeuerBetreiberbenutzerResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NeuerBetreiberbenutzerResolver,
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
    });
    resolver = TestBed.inject(NeuerBetreiberbenutzerResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should use datenberechtigungen of creator is behoerdenbenutzer', inject(
    [BenutzerProfil],
    (ersteller: BenutzerProfil) => {
      const land = LandProperties.SCHLESWIG_HOLSTEIN;
      const datenberechtigung = {
        land: land.nr,
        behoerden: [],
        akz: [],
        verwaltungsgebiete: [],
        betriebsstaetten: ['BST1'],
        fachmodule: [
          DatenberechtigungFachmoduleEnum.PRTR_LCP,
          DatenberechtigungFachmoduleEnum.E_ERKLAERUNG,
        ],
        themen: [DatenberechtigungThemenEnum.WASSER],
      };
      jest.spyOn(ersteller, 'landNr', 'get').mockReturnValue(land.nr);
      jest
        .spyOn(ersteller, 'datenberechtigung', 'get')
        .mockReturnValue(datenberechtigung);
      jest.spyOn(ersteller, 'isBehoerdenbenutzer', 'get').mockReturnValue(true);

      const benutzerDto = resolver.resolve();

      expect(benutzerDto).toBeDefined();
      expect(benutzerDto.datenberechtigung).toEqual({
        land: datenberechtigung.land,
        betriebsstaetten: null,
        fachmodule: null,
        themen: null,
      });
      expect(benutzerDto.zugewieseneRollen).toEqual([
        BenutzerZugewieseneRollenEnum.BETREIBER,
      ]);
      expect(benutzerDto.benutzername).toBeFalsy();
      expect(benutzerDto.id).toBeFalsy();
    }
  ));

  it('should use datenberechtigungen of creator is betreiber', inject(
    [BenutzerProfil],
    (ersteller: BenutzerProfil) => {
      const land = LandProperties.SCHLESWIG_HOLSTEIN;
      const datenberechtigung = {
        land: land.nr,
        behoerden: [],
        akz: [],
        verwaltungsgebiete: [],
        betriebsstaetten: ['BST1'],
        fachmodule: [
          DatenberechtigungFachmoduleEnum.PRTR_LCP,
          DatenberechtigungFachmoduleEnum.E_ERKLAERUNG,
        ],
        themen: [DatenberechtigungThemenEnum.WASSER],
      };
      jest.spyOn(ersteller, 'landNr', 'get').mockReturnValue(land.nr);
      jest
        .spyOn(ersteller, 'datenberechtigung', 'get')
        .mockReturnValue(datenberechtigung);
      jest
        .spyOn(ersteller, 'isBehoerdenbenutzer', 'get')
        .mockReturnValue(false);

      const benutzerDto = resolver.resolve();

      expect(benutzerDto).toBeDefined();
      expect(benutzerDto.datenberechtigung).toEqual({
        land: datenberechtigung.land,
        betriebsstaetten: ['BST1'],
        fachmodule: [
          DatenberechtigungFachmoduleEnum.PRTR_LCP,
          DatenberechtigungFachmoduleEnum.E_ERKLAERUNG,
        ],
        themen: [DatenberechtigungThemenEnum.WASSER],
      });
      expect(benutzerDto.zugewieseneRollen).toEqual([
        BenutzerZugewieseneRollenEnum.BETREIBER,
      ]);
      expect(benutzerDto.benutzername).toBeFalsy();
      expect(benutzerDto.id).toBeFalsy();
    }
  ));
});
