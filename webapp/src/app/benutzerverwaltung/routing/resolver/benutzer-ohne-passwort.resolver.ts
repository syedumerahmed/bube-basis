import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BenutzerOhnePasswortResolver implements Resolve<Array<string>> {
  constructor(private readonly router: Router) {}

  resolve(): Observable<Array<string>> {
    return of(this.router.getCurrentNavigation()?.extras.state?.benutzerNamen);
  }
}
