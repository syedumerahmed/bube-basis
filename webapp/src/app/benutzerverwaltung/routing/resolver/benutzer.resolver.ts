import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';

@Injectable()
export class BenutzerResolver implements Resolve<BenutzerDto> {
  constructor(
    private readonly service: BenutzerRestControllerService,
    private readonly alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<BenutzerDto> {
    return this.service
      .loadBenutzer(route.params.benutzername)
      .pipe(this.alertService.catchError());
  }
}
