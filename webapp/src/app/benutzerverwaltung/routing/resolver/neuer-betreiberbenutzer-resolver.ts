import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { BenutzerDto, BenutzerZugewieseneRollenEnum } from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable()
export class NeuerBetreiberbenutzerResolver implements Resolve<BenutzerDto> {
  constructor(private readonly bearbeiter: BenutzerProfil) {}

  resolve(): BenutzerDto {
    return {
      benutzername: null,
      aktiv: true,
      canWrite: true,
      datenberechtigung: {
        land: this.bearbeiter.datenberechtigung.land,
        betriebsstaetten: this.bearbeiter.isBehoerdenbenutzer
          ? null
          : this.bearbeiter.datenberechtigung.betriebsstaetten,
        fachmodule: this.bearbeiter.isBehoerdenbenutzer
          ? null
          : this.bearbeiter.datenberechtigung.fachmodule,
        themen: this.bearbeiter.isBehoerdenbenutzer
          ? null
          : this.bearbeiter.datenberechtigung.themen,
      },
      zugewieseneRollen: [BenutzerZugewieseneRollenEnum.BETREIBER],
    };
  }
}
