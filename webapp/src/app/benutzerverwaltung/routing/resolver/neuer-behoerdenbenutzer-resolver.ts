import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable()
export class NeuerBehoerdenbenutzerResolver implements Resolve<BenutzerDto> {
  constructor(
    private readonly alertService: AlertService,
    private readonly benutzerService: BenutzerRestControllerService,
    private readonly bearbeiter: BenutzerProfil
  ) {}

  resolve(): Observable<BenutzerDto> {
    return this.benutzerService.loadBenutzer(this.bearbeiter.username).pipe(
      this.alertService.catchError(),
      map((bearbeiter) => ({
        benutzername: null,
        aktiv: true,
        canWrite: true,
        datenberechtigung: bearbeiter.datenberechtigung,
        zugewieseneRollen: bearbeiter.zugewieseneRollen,
      }))
    );
  }
}
