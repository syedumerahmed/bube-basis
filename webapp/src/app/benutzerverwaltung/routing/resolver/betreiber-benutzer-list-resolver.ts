import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';

@Injectable()
export class BetreiberBenutzerListResolver
  implements Resolve<Array<BenutzerDto>>
{
  constructor(
    private readonly service: BenutzerRestControllerService,
    private readonly alertService: AlertService
  ) {}

  resolve(): Observable<Array<BenutzerDto>> {
    return this.service
      .listAllBetreiberBenutzer()
      .pipe(this.alertService.catchError());
  }
}
