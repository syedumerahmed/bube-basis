import { inject, TestBed } from '@angular/core/testing';

import { BenutzerResolver } from './benutzer.resolver';
import { AlertService } from '@/base/service/alert.service';
import { MockProviders } from 'ng-mocks';
import { Observable, of, throwError } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Rolle } from '@/base/security/model/Rolle';
import { Land } from '@/base/model/Land';
import { catchError } from 'rxjs/operators';
import spyOn = jest.spyOn;
import anything = jasmine.anything;
import { BenutzerDto, BenutzerRestControllerService } from '@api/benutzer';

describe('BenutzerResolver', () => {
  let resolver: BenutzerResolver;

  let benutzer: BenutzerDto;
  let route: ActivatedRouteSnapshot;
  let setAlert: jest.Mock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BenutzerResolver,
        MockProviders(AlertService, BenutzerRestControllerService),
      ],
    });
    resolver = TestBed.inject(BenutzerResolver);
    setAlert = jest.fn();
    spyOn(TestBed.inject(AlertService), 'catchError').mockReturnValue(
      catchError(setAlert)
    );

    benutzer = {
      benutzername: 'unittest',
      datenberechtigung: { land: Land.DEUTSCHLAND },
      zugewieseneRollen: [Rolle.GESAMTADMIN],
    };
    route = new ActivatedRouteSnapshot();
    route.params = { benutzername: benutzer.benutzername };
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should load benutzer', inject(
    [BenutzerRestControllerService],
    async (benutzerService: BenutzerRestControllerService) => {
      spyOn(benutzerService, 'loadBenutzer').mockReturnValue(
        of(benutzer) as Observable<any>
      );

      const benutzerDto = await resolver.resolve(route).toPromise();

      expect(benutzerService.loadBenutzer).toHaveBeenCalledWith(
        benutzer.benutzername
      );
      expect(benutzerDto).toBe(benutzer);
    }
  ));

  it('should alert when user not found', inject(
    [BenutzerRestControllerService],
    (benutzerService: BenutzerRestControllerService) => {
      const error = { error: 'Benutzer nicht gefunden' };
      spyOn(benutzerService, 'loadBenutzer').mockReturnValue(throwError(error));
      const subscriber = jest.fn();

      const subscription = resolver.resolve(route).subscribe(subscriber);

      expect(setAlert).toHaveBeenCalledWith(error, anything());
      expect(subscriber).not.toHaveBeenCalled();
      expect(subscription.closed).toBe(true);
    }
  ));
});
