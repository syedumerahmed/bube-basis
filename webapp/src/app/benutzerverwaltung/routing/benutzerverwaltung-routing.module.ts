import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BenutzerverwaltungComponent } from '../component/benutzerverwaltung/benutzerverwaltung.component';
import { BehoerdenbenutzerComponent } from '../component/behoerdenbenutzer/behoerdenbenutzer.component';
import { BetreiberbenutzerComponent } from '../component/betreiberbenutzer/betreiberbenutzer.component';
import { BenutzerComponent } from '../component/benutzer/benutzer.component';
import { BetreiberBenutzerListResolver } from './resolver/betreiber-benutzer-list-resolver';
import { BenutzerResolver } from './resolver/benutzer.resolver';
import { NeuerBetreiberbenutzerResolver } from './resolver/neuer-betreiberbenutzer-resolver';
import { NeuerBehoerdenbenutzerResolver } from './resolver/neuer-behoerdenbenutzer-resolver';
import { BehoerdeBenutzerListResolver } from './resolver/behoerde-benutzer-list-resolver';
import { BehoerdenbenutzerGuard } from './guard/behoerdenbenutzer.guard';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';
import { BenutzerOhnePasswortResolver } from './resolver/benutzer-ohne-passwort.resolver';

const routes: Routes = [
  {
    path: '',
    component: BenutzerverwaltungComponent,
    children: [
      {
        path: 'behoerden',
        component: BehoerdenbenutzerComponent,
        canActivate: [BehoerdenbenutzerGuard],
        resolve: {
          benutzer: BehoerdeBenutzerListResolver,
          behoerden: BehoerdenListResolver,
        },
      },
      {
        path: 'behoerden/neu',
        component: BenutzerComponent,
        canActivate: [BehoerdenbenutzerGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          benutzer: NeuerBehoerdenbenutzerResolver,
          behoerden: BehoerdenListResolver,
        },
      },
      {
        path: 'behoerden/benutzer/:benutzername',
        component: BenutzerComponent,
        canActivate: [BehoerdenbenutzerGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          benutzer: BenutzerResolver,
          behoerden: BehoerdenListResolver,
        },
      },

      {
        path: 'betreiber',
        component: BetreiberbenutzerComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          benutzer: BetreiberBenutzerListResolver,
          benutzerOhnePasswort: BenutzerOhnePasswortResolver,
        },
      },
      {
        path: 'betreiber/neu',
        component: BenutzerComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: { benutzer: NeuerBetreiberbenutzerResolver },
      },
      {
        path: 'betreiber/benutzer/:benutzername',
        component: BenutzerComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: { benutzer: BenutzerResolver },
      },
      {
        path: '',
        redirectTo: 'betreiber',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    BehoerdenbenutzerGuard,
    BetreiberBenutzerListResolver,
    BehoerdeBenutzerListResolver,
    NeuerBehoerdenbenutzerResolver,
    NeuerBetreiberbenutzerResolver,
    BenutzerResolver,
  ],
})
export class BenutzerverwaltungRoutingModule {}
