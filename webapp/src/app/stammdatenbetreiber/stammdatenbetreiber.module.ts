import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { StammdatenComponent } from './component/stammdaten/stammdaten.component';

import { StammdatenbetreiberRoutingModule } from './routing/stammdatenbetreiber-routing.module';
import { SpbBetriebsstaetteModule } from './betriebsstaette/spb-betriebsstaette.module';
import { BaseModule } from '@/base/base.module';
import { StammdatenModule } from '@/stammdaten/stammdaten.module';
import { BasedomainModule } from '@/basedomain/basedomain.module';

@NgModule({
  declarations: [StammdatenComponent],
  imports: [
    CommonModule,
    ClarityModule,
    BaseModule,
    BasedomainModule,
    StammdatenbetreiberRoutingModule,
    SpbBetriebsstaetteModule,
    StammdatenModule,
  ],
  exports: [],
})
export class StammdatenbetreiberModule {}
