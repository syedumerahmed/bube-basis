import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetriebsstatusListResolver } from '@/referenzdaten/routing/resolver/betriebsstatus-list-resolver';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { VorschriftenListResolver } from '@/referenzdaten/routing/resolver/vorschriften-list-resolver';
import { VertraulichkeitsgrundListResolver } from '@/referenzdaten/routing/resolver/vertraulichkeitsgrund-list-resolver';
import { ZustaendigkeitListResolver } from '@/referenzdaten/routing/resolver/zustaendigkeit-list-resolver';
import { EpsgListResolver } from '@/referenzdaten/routing/resolver/epsg-list-resolver';
import { LeistungseinheitenListResolver } from '@/referenzdaten/routing/resolver/leistungseinheiten-list-resolver';
import { Nummer4BImSchVListResolver } from '@/referenzdaten/routing/resolver/nummer4bimschv-list-resolver';
import { PrtrTaetigkeitenListResolver } from '@/referenzdaten/routing/resolver/prtr-taetigkeiten-list-resolver';
import { IedTaetigkeitenListResolver } from '@/referenzdaten/routing/resolver/ied-taetigkeiten-list-resolver';
import { SpbAnlagenResolver } from '@/stammdatenbetreiber/anlage/routing/resolver/spb-anlagen-resolver';
import { SpbAnlageNeuResolver } from '@/stammdatenbetreiber/anlage/routing/resolver/spb-anlage-neu-resolver';
import { SpbAnlageMainComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-main/spb-anlage-main.component';
import { SpbAnlageComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage/spb-anlage.component';
import { SpbAnlagenteilDetailsComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlagenteil-details/spb-anlagenteil-details.component';
import { SpbAnlagenteilNeuResolver } from '@/stammdatenbetreiber/anlage/routing/resolver/spb-anlagenteil-neu-resolver';
import { SpbAnlagenteilResolver } from '@/stammdatenbetreiber/anlage/routing/resolver/spb-anlagenteil-resolver';
import { SpbQuellenModule } from '@/stammdatenbetreiber/quellen/spb-quellen-module';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const anlageReferenzenResolvers = {
  betriebsstatusListe: BetriebsstatusListResolver,
  vorschriftenListe: VorschriftenListResolver,
  nummer4BImSchVTaetigkeitenListe: Nummer4BImSchVListResolver,
  prtrTaetigkeitenListe: PrtrTaetigkeitenListResolver,
  iedTaetigkeitenListe: IedTaetigkeitenListResolver,
  vertraulichkeitsgrundListe: VertraulichkeitsgrundListResolver,
  behoerdenListe: BehoerdenListResolver,
  zustaendigkeitenListe: ZustaendigkeitListResolver,
  einheitenListe: LeistungseinheitenListResolver,
  epsgCodeListe: EpsgListResolver,
};

const routes: Routes = [
  {
    path: 'neu',
    component: SpbAnlageComponent,
    resolve: {
      anlage: SpbAnlageNeuResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: SpbAnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
    ],
  },
  {
    path: 'nummer/:anlageNummer',
    component: SpbAnlageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      anlage: SpbAnlagenResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: SpbAnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
      {
        path: 'quelle',
        loadChildren: () => SpbQuellenModule,
      },
      {
        path: 'anlagenteil/neu',
        component: SpbAnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: SpbAnlagenteilNeuResolver,
        },
      },
      {
        path: 'anlagenteil/nummer/:anlagenteilNummer',
        component: SpbAnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: SpbAnlagenteilResolver,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    SpbAnlagenResolver,
    SpbAnlageNeuResolver,
    SpbAnlagenteilResolver,
    SpbAnlagenteilNeuResolver,
  ],
})
export class SpbAnlageRoutingModule {}
