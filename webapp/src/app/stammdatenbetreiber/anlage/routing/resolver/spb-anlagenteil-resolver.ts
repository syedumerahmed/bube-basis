import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlagenteilDto,
  SpbAnlagenteilRestControllerService,
} from '@api/stammdatenbetreiber';
@Injectable()
export class SpbAnlagenteilResolver implements Resolve<SpbAnlagenteilDto> {
  constructor(
    private service: SpbAnlagenteilRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbAnlagenteilDto> {
    const anlageId = route.parent.data.anlage.anlageId;
    const betriebsstaetteId =
      route.parent.parent.parent.data.betriebsstaette.betriebsstaetteId;
    const nummer = route.params.anlagenteilNummer;

    if (anlageId != null) {
      return this.service
        .readSpbAnlagenteil(betriebsstaetteId, anlageId, nummer)
        .pipe(this.alertService.catchError());
    } else {
      const spbAnlageId = route.parent.data.anlage.id;
      return this.service
        .readSpbAnlagenteilAnSpbAnlage(betriebsstaetteId, spbAnlageId, nummer)
        .pipe(this.alertService.catchError());
    }
  }
}
