import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { SpbAnlagenteilDto } from '@api/stammdatenbetreiber';

@Injectable()
export class SpbAnlagenteilNeuResolver implements Resolve<SpbAnlagenteilDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<SpbAnlagenteilDto> {
    const anlageId = route.parent.data.anlage.anlageId;
    const spbAnlageId = route.parent.data.anlage.id;

    if (anlageId != null) {
      return of({
        parentAnlageId: anlageId,
        parentSpbAnlageId: null,
        name: null,
        anlageNr: null,
      });
    } else {
      return of({
        parentAnlageId: null,
        parentSpbAnlageId: spbAnlageId,
        name: null,
        anlageNr: null,
      });
    }
  }
}
