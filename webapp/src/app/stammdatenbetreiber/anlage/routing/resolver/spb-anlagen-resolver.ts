import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlageDto,
  SpbAnlagenRestControllerService,
} from '@api/stammdatenbetreiber';

@Injectable()
export class SpbAnlagenResolver implements Resolve<SpbAnlageDto> {
  constructor(
    private service: SpbAnlagenRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbAnlageDto> {
    const betriebsstaetteId =
      route.parent.parent.data.betriebsstaette.betriebsstaetteId;
    const nummer = route.params.anlageNummer;

    return this.service
      .readSpbAnlage(betriebsstaetteId, nummer)
      .pipe(this.alertService.catchError());
  }
}
