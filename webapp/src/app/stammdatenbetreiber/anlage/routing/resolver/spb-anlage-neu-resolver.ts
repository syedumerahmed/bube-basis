import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { SpbAnlageDto } from '@api/stammdatenbetreiber';

@Injectable()
export class SpbAnlageNeuResolver implements Resolve<SpbAnlageDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<SpbAnlageDto> {
    return of({
      parentBetriebsstaetteId:
        route.parent.parent.data.betriebsstaette.betriebsstaetteId,
      anlageId: null,
      name: null,
      anlageNr: null,
    });
  }
}
