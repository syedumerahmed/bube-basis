import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute } from '@angular/router';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { SpbAnlageDetailsComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-details/spb-anlage-details.component';

@Component({
  selector: 'app-spb-anlage-main',
  templateUrl: './spb-anlage-main.component.html',
  styleUrls: ['./spb-anlage-main.component.scss'],
})
export class SpbAnlageMainComponent implements OnInit, UnsavedChanges {
  @ViewChild(SpbAnlageDetailsComponent, { static: true })
  spbAnlageDetailsComponent: SpbAnlageDetailsComponent;

  quellenDisplay: boolean;

  constructor(
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly quellenZuordungService: QuellenZuordungService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.quellenZuordungService
        .quellenSindAnAnlagen(
          data.betriebsstaette.berichtsjahr.schluessel,
          data.betriebsstaette.land
        )
        .subscribe(
          (ret) => {
            this.quellenDisplay = ret;
          },
          (error) => this.alertService.setAlert(error)
        );
    });
  }

  canDeactivate(): boolean {
    return this.spbAnlageDetailsComponent.canDeactivate();
  }

  showSaveWarning(): void {
    this.spbAnlageDetailsComponent.showSaveWarning();
  }
}
