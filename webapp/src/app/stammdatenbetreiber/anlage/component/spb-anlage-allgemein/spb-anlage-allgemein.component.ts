import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { SpbAnlageDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import {
  GeoPunktComponent,
  GeoPunktFormData,
} from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { BubeValidators } from '@/base/forms/bube-validators';

export interface SpbAnlageAllgemeinFormData {
  anlageNr: string;
  name: string;
  inbetriebnahme: string;
  betriebsstatus: ReferenzDto;
  betriebsstatusSeit: string;
  geoPunkt: GeoPunktFormData;
}

@Component({
  selector: 'app-spb-anlage-allgemein',
  templateUrl: './spb-anlage-allgemein.component.html',
  styleUrls: ['./spb-anlage-allgemein.component.scss'],
  providers: [DatePipe],
})
export class SpbAnlageAllgemeinComponent
  implements OnInit, AfterViewInit, AfterViewChecked
{
  constructor(private readonly datePipe: DatePipe) {}

  @Input()
  form: FormGroup<SpbAnlageAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;
  @ViewChild(GeoPunktComponent)
  geoPunktComponent: GeoPunktComponent;

  @Input()
  betriebsstatusListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  today: string;

  static toForm(anlageDto: SpbAnlageDto): SpbAnlageAllgemeinFormData {
    const {
      anlageNr,
      inbetriebnahme,
      betriebsstatusSeit,
      name,
      betriebsstatus,
      geoPunkt,
    } = anlageDto;
    return {
      name,
      anlageNr,
      betriebsstatus,
      inbetriebnahme: FormUtil.fromIsoDate(inbetriebnahme),
      betriebsstatusSeit: FormUtil.fromIsoDate(betriebsstatusSeit),
      geoPunkt: GeoPunktComponent.toForm(geoPunkt),
    };
  }

  static createForm(
    fb: FormBuilder,
    anlage: SpbAnlageAllgemeinFormData,
    selectedBerichtsjahr: ReferenzDto
  ): FormGroup<SpbAnlageAllgemeinFormData> {
    return fb.group<SpbAnlageAllgemeinFormData>({
      name: [anlage.name, [Validators.required, Validators.maxLength(120)]],
      anlageNr: [
        anlage.anlageNr,
        [Validators.required, Validators.maxLength(20)],
      ],
      inbetriebnahme: [anlage.inbetriebnahme, BubeValidators.date],
      betriebsstatus: [anlage.betriebsstatus],
      betriebsstatusSeit: [anlage.betriebsstatusSeit, BubeValidators.date],
      geoPunkt: GeoPunktComponent.createForm(
        fb,
        anlage.geoPunkt,
        selectedBerichtsjahr
      ),
    });
  }

  ngOnInit(): void {
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm?.markAsTouched();
  }

  get geoPunktForm(): FormGroup<GeoPunktFormData> {
    return this.form.get('geoPunkt') as FormGroup<GeoPunktFormData>;
  }
}
