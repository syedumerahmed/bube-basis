import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder } from '@/base/forms';
import { MockComponents, MockModule } from 'ng-mocks';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { SpbAnlageAllgemeinComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-allgemein/spb-anlage-allgemein.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { SpbAnlageDto } from '@api/stammdatenbetreiber';
import { ReferenzDto, VorschriftDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';

const anlage: SpbAnlageDto = {
  anlageNr: 'ANLA1',
  name: 'Anlage 1',
  parentBetriebsstaetteId: 20,
  id: 21,
  betriebsstatus: { schluessel: '01' } as ReferenzDto,
  betriebsstatusSeit: null,
  inbetriebnahme: null,
  vorschriften: [
    { art: { schluessel: 'V1' } },
    { art: { schluessel: 'V2' } },
  ] as Array<VorschriftDto>,
  geoPunkt: {
    nord: 1,
    ost: 2,
    epsgCode: { schluessel: '01' } as ReferenzDto,
  },
};

describe('SpbAnlageAllgemeinComponent', () => {
  let component: SpbAnlageAllgemeinComponent;
  let fixture: ComponentFixture<SpbAnlageAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SpbAnlageAllgemeinComponent,
        MockComponents(
          ReferenzSelectComponent,
          TooltipComponent,
          GeoPunktComponent
        ),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(SpbAnlageAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = SpbAnlageAllgemeinComponent.createForm(
      formBuilder,
      SpbAnlageAllgemeinComponent.toForm(anlage),
      {} as ReferenzDto
    );
    component.betriebsstatusListe = new Referenzliste<Required<ReferenzDto>>(
      anlage.vorschriften.map((v) => v.art as Required<ReferenzDto>)
    );
    component.epsgCodeListe = new Referenzliste<Required<ReferenzDto>>([]);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      SpbAnlageAllgemeinComponent.toForm(anlage)
    );
  });
});
