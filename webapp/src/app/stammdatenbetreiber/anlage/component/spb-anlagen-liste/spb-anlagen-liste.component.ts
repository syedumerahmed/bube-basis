import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbAnlageListItemDto,
  SpbAnlagenRestControllerService,
  SpbBetriebsstaetteDto,
} from '@api/stammdatenbetreiber';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  undoIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(factoryIcon, plusIcon, eyeIcon, undoIcon);
@Component({
  selector: 'app-spb-anlagen-liste',
  templateUrl: './spb-anlagen-liste.component.html',
  styleUrls: ['./spb-anlagen-liste.component.scss'],
})
export class SpbAnlagenListeComponent implements OnInit {
  loeschenDialog: boolean;
  spbAnlageForDelete: SpbAnlageListItemDto;
  spbAnlagen: Array<SpbAnlageListItemDto>;
  spbBetriebsstaette: SpbBetriebsstaetteDto;

  readonly SortOrder = ClrDatagridSortOrder;

  constructor(
    private route: ActivatedRoute,
    private spbAnlagenService: SpbAnlagenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.spbBetriebsstaette = data.betriebsstaette;
      this.refresh();
    });
  }

  refresh(): void {
    this.spbAnlagenService
      .listAllSpbAnlagen(this.spbBetriebsstaette.betriebsstaetteId)
      .subscribe(
        (spbAnlagen) => (this.spbAnlagen = spbAnlagen),
        (error) => this.alertService.setAlert(error)
      );
  }

  deleteAnlage(): void {
    this.spbAnlagenService
      .deleteSpbAnlage(this.spbAnlageForDelete.spbAnlageId)
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  closeLoeschenDialog(): void {
    this.loeschenDialog = false;
    this.spbAnlageForDelete = null;
  }

  openLoeschenDialog(spbAnlage: SpbAnlageListItemDto): void {
    this.spbAnlageForDelete = spbAnlage;
    this.loeschenDialog = true;
  }

  getSpbStatus(spbAnlage: SpbAnlageListItemDto): string {
    if (spbAnlage.spbAnlageId == null) {
      return 'unverändert';
    }
    if (spbAnlage.anlageVorhanden) {
      return 'verändert';
    } else {
      return 'neu';
    }
  }
}
