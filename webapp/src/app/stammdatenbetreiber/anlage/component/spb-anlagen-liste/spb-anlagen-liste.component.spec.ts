import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import { SpbAnlagenListeComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlagen-liste/spb-anlagen-liste.component';
import {
  SpbAnlagenRestControllerService,
  SpbBetriebsstaetteDto,
} from '@api/stammdatenbetreiber';

describe('SpbAnlagenListeComponent', () => {
  const betriebsstaette: SpbBetriebsstaetteDto = {
    adresse: undefined,
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
    betriebsstaetteId: 2,
  };

  let dataSubject: Subject<Data>;
  let component: SpbAnlagenListeComponent;
  let fixture: ComponentFixture<SpbAnlagenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [SpbAnlagenListeComponent],
      providers: [
        {
          provide: SpbAnlagenRestControllerService,
          useValue: MockService(SpbAnlagenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(SpbAnlagenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject(
    [SpbAnlagenRestControllerService],
    (anlagenService: SpbAnlagenRestControllerService) => {
      expect(component).toBeTruthy();
      expect(anlagenService.listAllSpbAnlagen).toHaveBeenCalled();
    }
  ));
});
