import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MockComponents,
  MockModule,
  MockPipe,
  MockProvider,
  MockProviders,
} from 'ng-mocks';
import { VorschriftenComponent } from '@/basedomain/component/vorschriften/vorschriften.component';
import { LeistungenComponent } from '@/basedomain/component/leistungen/leistungen.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { SpbAnlageAllgemeinComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-allgemein/spb-anlage-allgemein.component';
import { SpbAnlageDetailsComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-details/spb-anlage-details.component';
import { CompleteUriEncoderPipe } from '@/base/texts/complete.uri.encoder.pipe';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import {
  SpbAnlageDto,
  SpbAnlagenRestControllerService,
} from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import spyOn = jest.spyOn;

describe('SpbAnlageDetailsComponent', () => {
  let component: SpbAnlageDetailsComponent;
  let fixture: ComponentFixture<SpbAnlageDetailsComponent>;
  const betriebsstatusListe: Array<ReferenzDto> = [
    { schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vorschriftenListe: Array<ReferenzDto> = [
    { schluessel: '02' },
    { schluessel: '03' },
  ] as Array<ReferenzDto>;
  const nummer4BImSchVTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'R4BV01' },
    { schluessel: 'R4BV02' },
  ] as Array<ReferenzDto>;
  const iedTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RIET01' },
    { schluessel: 'RIET02' },
  ] as Array<ReferenzDto>;
  const prtrTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RPRTR01' },
    { schluessel: 'RPRTR02' },
  ] as Array<ReferenzDto>;
  const einheitenListe = [
    { schluessel: '1', ktext: 'm' },
    { schluessel: '2', ktext: 'km' },
  ] as ReferenzDto[];

  const anlage: SpbAnlageDto = {
    anlageNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    vorschriften: [],
  };
  const routeData = new BehaviorSubject({ anlage });
  const selectedBerichtsjahr = {
    id: 2018,
  } as ReferenzDto;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SpbAnlageDetailsComponent,
        SpbAnlageAllgemeinComponent,
        ReferenzSelectComponent,
        MockComponents(
          VorschriftenComponent,
          LeistungenComponent,
          TooltipComponent,
          GeoPunktComponent
        ),
        MockPipe(EllipsizePipe),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
      providers: [
        MockProviders(SpbAnlagenRestControllerService, CompleteUriEncoderPipe),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              snapshot: {
                data: {
                  vorschriftenListe,
                  nummer4BImSchVTaetigkeitenListe,
                  iedTaetigkeitenListe,
                  prtrTaetigkeitenListe,
                  betriebsstatusListe,
                  einheitenListe,
                  epsgCodeListe: [],
                },
              },
            },
            snapshot: {
              params: { anlageNummer: 'neu' },
              data: { schreibRecht: true },
            },
            data: routeData.asObservable(),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SpbAnlageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should deactivate berichtsjahrwechsel', inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();

      component.ngOnDestroy();

      expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.anlageForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
      component.anlageForm.patchValue({
        allgemein: { name: 'Meine neue Anlage' },
      });
    });

    it('should be invalid', () => {
      expect(component.anlageForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          SpbAnlageAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [SpbAnlagenRestControllerService],
        (anlagenService: SpbAnlagenRestControllerService) => {
          expect(anlagenService.createSpbAnlage).not.toHaveBeenCalled();
          expect(anlagenService.updateSpbAnlage).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.anlageForm.patchValue({
        allgemein: {
          name: 'Meine neue Anlage',
          anlageNr: 'ANL1234',
          betriebsstatus: betriebsstatusListe[0],
        },
        vorschriften: vorschriftenListe.map((r) => ({
          art: r,
          haupttaetigkeit: null,
          taetigkeiten: null,
        })),
      });
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.anlageForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedAnlageDto: SpbAnlageDto = {
        anlageNr: 'ANL1234',
        betriebsstatus: betriebsstatusListe[0],
        betriebsstatusSeit: null,
        inbetriebnahme: null,
        name: 'Meine neue Anlage',
        parentBetriebsstaetteId: 0,
        vorschriften: [],
        leistungen: [],
        geoPunkt: null,
      };
      let anlageObservable: Subject<SpbAnlageDto>;
      beforeEach(inject(
        [SpbAnlagenRestControllerService],
        (anlagenService: SpbAnlagenRestControllerService) => {
          anlageObservable = new Subject<SpbAnlageDto>();
          spyOn(anlagenService, 'createSpbAnlage').mockReturnValue(
            anlageObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [SpbAnlagenRestControllerService],
        (anlagenService: SpbAnlagenRestControllerService) => {
          expect(anlagenService.createSpbAnlage).toHaveBeenCalledWith(
            expectedAnlageDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedAnlageDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          anlageObservable.next(responseDto);
          anlageObservable.complete();
        }));

        it('should update anlage', () => {
          expect(component.anlage).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.anlageForm.touched).toBe(false);
          expect(component.anlageForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });
});
