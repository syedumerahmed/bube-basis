import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  SpbAnlageAllgemeinComponent,
  SpbAnlageAllgemeinFormData,
} from '../spb-anlage-allgemein/spb-anlage-allgemein.component';
import { DatePipe } from '@angular/common';
import { FormUtil } from '@/base/forms/form-util';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  LeistungenComponent,
  LeistungenFormData,
} from '@/basedomain/component/leistungen/leistungen.component';
import {
  VorschriftenComponent,
  VorschriftFormData,
} from '@/basedomain/component/vorschriften/vorschriften.component';
import { CompleteUriEncoderPipe } from '@/base/texts/complete.uri.encoder.pipe';
import {
  SpbAnlageDto,
  SpbAnlagenRestControllerService,
} from '@api/stammdatenbetreiber';
import { GeoPunktDto, ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  factoryIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface SpbAnlageFormData {
  allgemein: SpbAnlageAllgemeinFormData;
  vorschriften: Array<VorschriftFormData>;
  leistungen: Array<LeistungenFormData>;
}

ClarityIcons.addIcons(factoryIcon, timesIcon, floppyIcon, errorStandardIcon);

@Component({
  selector: 'app-spb-anlagen-details',
  templateUrl: './spb-anlage-details.component.html',
  styleUrls: ['./spb-anlage-details.component.scss'],
  providers: [DatePipe],
})
export class SpbAnlageDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(SpbAnlageAllgemeinComponent)
  allgemeinComponent: SpbAnlageAllgemeinComponent;

  anlage: SpbAnlageDto;
  anlageForm: FormGroup<SpbAnlageFormData>;

  saveAttempt = false;
  schreibsperreBetreiber = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  einheitenListe: Referenzliste<Required<ReferenzDto>>;
  nummer4BImSchVTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  iedTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  prtrTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;

  private mapToForm(anlageDto: SpbAnlageDto): SpbAnlageFormData {
    return {
      allgemein: SpbAnlageAllgemeinComponent.toForm(anlageDto),
      vorschriften: VorschriftenComponent.vorschriftenToForm(
        anlageDto.vorschriften,
        this.nummer4BImSchVTaetigkeitenListe,
        this.prtrTaetigkeitenListe,
        this.iedTaetigkeitenListe
      ),
      leistungen: LeistungenComponent.toForm(anlageDto.leistungen),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly anlagenService: SpbAnlagenRestControllerService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly uriEncoder: CompleteUriEncoderPipe
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.anlage = data.anlage;
      this.schreibsperreBetreiber = data.betriebsstaette.schreibsperreBetreiber;
      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.vorschriften = new Referenzliste(data.vorschriftenListe);
      this.einheitenListe = new Referenzliste(data.einheitenListe);
      this.nummer4BImSchVTaetigkeitenListe = new Referenzliste(
        data.nummer4BImSchVTaetigkeitenListe
      );
      this.iedTaetigkeitenListe = new Referenzliste(data.iedTaetigkeitenListe);
      this.prtrTaetigkeitenListe = new Referenzliste(
        data.prtrTaetigkeitenListe
      );

      this.reset();
    });
    this.anlageForm = this.createAnlageForm(this.mapToForm(this.anlage));
    this.checkSchreibsperreBetreiber();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  checkSchreibsperreBetreiber(): void {
    if (this.schreibsperreBetreiber) {
      this.anlageForm.disable();
    } else {
      this.anlageForm.enable();
    }
  }

  formToAnlage(): SpbAnlageDto {
    const formData = this.anlageForm.value;
    return {
      ...this.anlage,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      leistungen: formData.leistungen.map((data) => ({
        ...data,
        leistungseinheit: data.einheit,
        leistungsklasse: data.klasse,
      })),
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
    };
  }

  save(): void {
    this.saveAttempt = true;
    if (this.anlageForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }

    const anlageDto: SpbAnlageDto = this.formToAnlage();

    if (this.isNeu) {
      this.createAnlage(anlageDto);
    } else {
      this.updateAnlage(anlageDto);
    }
  }

  updateAnlage(anlageDto: SpbAnlageDto): void {
    this.anlagenService.updateSpbAnlage(anlageDto).subscribe(
      (anlage) => {
        this.anlage = anlage;
        this.reset();
        this.updateUrlAfterChange(anlage.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  createAnlage(anlageDto: SpbAnlageDto): void {
    this.anlagenService.createSpbAnlage(anlageDto).subscribe(
      (anlage) => {
        this.anlage = anlage;
        this.reset();
        this.updateUrlAfterCreate(anlage.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  getGeoPunkt(geoPunkt: SpbAnlageAllgemeinFormData['geoPunkt']): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.anlageForm) {
      this.saveAttempt = false;
      const originalAnlage = this.mapToForm(this.anlage);
      this.anlageForm.reset(originalAnlage);
      this.anlageForm.setControl(
        'leistungen',
        LeistungenComponent.createForm(
          this.fb,
          originalAnlage.leistungen,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'vorschriften',
        VorschriftenComponent.vorschriftenCreateForm(
          this.fb,
          originalAnlage.vorschriften,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.checkSchreibsperreBetreiber();
    }
  }

  navigateBackToBetriebsstaette(): void {
    this.router.navigate(['.'], {
      relativeTo: this.route.parent.parent.parent,
    });
  }

  get isNeu(): boolean {
    return this.anlage.id == null;
  }

  get allgemeinForm(): FormGroup<SpbAnlageAllgemeinFormData> {
    return this.anlageForm.get(
      'allgemein'
    ) as FormGroup<SpbAnlageAllgemeinFormData>;
  }

  get vorschriftenForm(): FormArray<VorschriftFormData> {
    const formArray: FormArray<VorschriftFormData> = this.anlageForm.get(
      'vorschriften'
    ) as FormArray<VorschriftFormData>;
    // Die Betreiber sollen die Vorschriften nurnoch lesen können. Dies wird über das disable der Form umgesetzt
    formArray.disable();
    return formArray;
  }

  get leistungenForm(): FormArray<LeistungenFormData> {
    return this.anlageForm.get('leistungen') as FormArray<LeistungenFormData>;
  }

  private createAnlageForm(
    initialAnlage: SpbAnlageFormData
  ): FormGroup<SpbAnlageFormData> {
    return this.fb.group<SpbAnlageFormData>({
      allgemein: SpbAnlageAllgemeinComponent.createForm(
        this.fb,
        initialAnlage.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      vorschriften: VorschriftenComponent.vorschriftenCreateForm(
        this.fb,
        initialAnlage.vorschriften,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      leistungen: LeistungenComponent.createForm(
        this.fb,
        initialAnlage.leistungen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, `/anlage/nummer/${nummer}`);
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder die Original-Nr. oder "neu" in der URL und muss ersetzt werden
    this.updateUrl(nummer, new RegExp('/anlage/.+$'));
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      this.uriEncoder.transform(nummer),
      '/anlage/nummer/' +
        this.uriEncoder.transform(this.route.snapshot.params.anlageNummer)
    );
  }

  canDeactivate(): boolean {
    return this.anlageForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
