import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbAnlageDto,
  SpbAnlagenteilListItemDto,
  SpbAnlagenteilRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  undoIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(undoIcon, factoryIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-spb-anlagenteile-liste',
  templateUrl: './spb-anlagenteile-liste.component.html',
  styleUrls: ['./spb-anlagenteile-liste.component.scss'],
})
export class SpbAnlagenteileListeComponent implements OnInit {
  loeschenDialog: boolean;
  spbAnlagenteilForDelete: SpbAnlagenteilListItemDto;
  spbAnlagenteile: Array<SpbAnlagenteilListItemDto>;
  spbAnlage: SpbAnlageDto;

  readonly SortOrder = ClrDatagridSortOrder;

  schreibsperreBetreiber = false;

  constructor(
    private route: ActivatedRoute,
    private spbAnlagenteilService: SpbAnlagenteilRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.spbAnlage = data.anlage;
      this.schreibsperreBetreiber = data.betriebsstaette.schreibsperreBetreiber;
      this.refresh();
    });
  }

  refresh(): void {
    if (this.spbAnlage.anlageId != null) {
      this.spbAnlagenteilService
        .listAllSpbAnlagenteile(
          Number(this.spbAnlage.anlageId),
          this.spbAnlage.parentBetriebsstaetteId
        )
        .subscribe(
          (spbAnlagenteile) => (this.spbAnlagenteile = spbAnlagenteile),
          (error) => this.alertService.setAlert(error)
        );
    } else if (this.spbAnlage.id != null) {
      this.spbAnlagenteilService
        .listAllSpbAnlagenteileAnSpbAnlage(
          this.spbAnlage.id,
          this.spbAnlage.parentBetriebsstaetteId
        )
        .subscribe(
          (spbAnlagenteile) => (this.spbAnlagenteile = spbAnlagenteile),
          (error) => this.alertService.setAlert(error)
        );
    }
  }

  deleteAnlagenteil(): void {
    this.spbAnlagenteilService
      .deleteSpbAnlagenteil(
        this.spbAnlage.parentBetriebsstaetteId,
        this.spbAnlagenteilForDelete.spbAnlagenteilId
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  closeLoeschenDialog(): void {
    this.loeschenDialog = false;
    this.spbAnlagenteilForDelete = null;
  }

  openLoeschenDialog(spbAnlage: SpbAnlagenteilListItemDto): void {
    this.spbAnlagenteilForDelete = spbAnlage;
    this.loeschenDialog = true;
  }

  getSpbStatus(spbAnlagenteil: SpbAnlagenteilListItemDto): string {
    if (spbAnlagenteil.spbAnlagenteilId == null) {
      return 'unverändert';
    }
    if (spbAnlagenteil.anlagenteilVorhanden) {
      return 'verändert';
    } else {
      return 'neu';
    }
  }
}
