import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GeoPunktDto, ReferenzDto } from '@api/stammdaten';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  SpbAnlageAllgemeinComponent,
  SpbAnlageAllgemeinFormData,
} from '../spb-anlage-allgemein/spb-anlage-allgemein.component';
import { DatePipe } from '@angular/common';
import { FormUtil } from '@/base/forms/form-util';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  LeistungenComponent,
  LeistungenFormData,
} from '@/basedomain/component/leistungen/leistungen.component';
import {
  VorschriftenComponent,
  VorschriftFormData,
} from '@/basedomain/component/vorschriften/vorschriften.component';
import { CompleteUriEncoderPipe } from '@/base/texts/complete.uri.encoder.pipe';
import {
  SpbAnlageDto,
  SpbAnlagenteilDto,
  SpbAnlagenteilRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  ClarityIcons,
  errorStandardIcon,
  factoryIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface SpbAnlageFormData {
  allgemein: SpbAnlageAllgemeinFormData;
  vorschriften: Array<VorschriftFormData>;
  leistungen: Array<LeistungenFormData>;
}

ClarityIcons.addIcons(factoryIcon, timesIcon, floppyIcon, errorStandardIcon);

@Component({
  selector: 'app-spb-anlagenteil-details',
  templateUrl: './spb-anlagenteil-details.component.html',
  styleUrls: ['./spb-anlagenteil-details.component.scss'],
  providers: [DatePipe],
})
export class SpbAnlagenteilDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(SpbAnlageAllgemeinComponent)
  allgemeinComponent: SpbAnlageAllgemeinComponent;

  anlagenteil: SpbAnlagenteilDto;
  anlageForm: FormGroup<SpbAnlageFormData>;

  saveAttempt = false;
  schreibsperreBetreiber = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  einheitenListe: Referenzliste<Required<ReferenzDto>>;
  nummer4BImSchVTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  iedTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  prtrTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;

  private mapToForm(anlagenteilDto: SpbAnlagenteilDto): SpbAnlageFormData {
    const anlageDto: SpbAnlageDto = {
      anlageId: 0,
      parentBetriebsstaetteId: 0,
      ...anlagenteilDto,
    };

    return {
      allgemein: SpbAnlageAllgemeinComponent.toForm(anlageDto),
      vorschriften: VorschriftenComponent.vorschriftenToForm(
        anlagenteilDto.vorschriften,
        this.nummer4BImSchVTaetigkeitenListe,
        this.prtrTaetigkeitenListe,
        this.iedTaetigkeitenListe
      ),
      leistungen: LeistungenComponent.toForm(anlagenteilDto.leistungen),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly spbAnlagenteilService: SpbAnlagenteilRestControllerService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly uriEncoder: CompleteUriEncoderPipe
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.anlagenteil = data.anlagenteil;
      this.schreibsperreBetreiber = data.betriebsstaette.schreibsperreBetreiber;
      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.vorschriften = new Referenzliste(data.vorschriftenListe);
      this.einheitenListe = new Referenzliste(data.einheitenListe);
      this.nummer4BImSchVTaetigkeitenListe = new Referenzliste(
        data.nummer4BImSchVTaetigkeitenListe
      );
      this.iedTaetigkeitenListe = new Referenzliste(data.iedTaetigkeitenListe);
      this.prtrTaetigkeitenListe = new Referenzliste(
        data.prtrTaetigkeitenListe
      );

      this.reset();
    });
    this.anlageForm = this.createAnlageForm(this.mapToForm(this.anlagenteil));
    this.checkSchreibsperreBetreiber();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  checkSchreibsperreBetreiber(): void {
    if (this.schreibsperreBetreiber) {
      this.anlageForm.disable();
    } else {
      this.anlageForm.enable();
    }
  }

  formToAnlage(): SpbAnlagenteilDto {
    const formData = this.anlageForm.value;
    return {
      ...this.anlagenteil,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      leistungen: formData.leistungen.map((data) => ({
        ...data,
        leistungseinheit: data.einheit,
        leistungsklasse: data.klasse,
      })),
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
    };
  }

  save(): void {
    this.saveAttempt = true;
    if (this.anlageForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }

    const anlagenteilDto: SpbAnlagenteilDto = this.formToAnlage();

    if (this.isNeu) {
      this.createAnlage(anlagenteilDto);
    } else {
      this.updateAnlage(anlagenteilDto);
    }
  }

  updateAnlage(anlagenteilDto: SpbAnlagenteilDto): void {
    this.spbAnlagenteilService.updateSpbAnlagenteil(anlagenteilDto).subscribe(
      (anlagenteil) => {
        this.anlagenteil = anlagenteil;
        this.reset();
        this.updateUrlAfterChange(anlagenteil.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  createAnlage(anlagenteilDto: SpbAnlagenteilDto): void {
    this.spbAnlagenteilService.createSpbAnlagenteil(anlagenteilDto).subscribe(
      (anlagenteil) => {
        this.anlagenteil = anlagenteil;
        this.reset();
        this.updateUrlAfterCreate(anlagenteil.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  getGeoPunkt(geoPunkt: SpbAnlageAllgemeinFormData['geoPunkt']): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.anlageForm) {
      this.saveAttempt = false;
      const originalAnlage = this.mapToForm(this.anlagenteil);
      this.anlageForm.reset(originalAnlage);
      this.anlageForm.setControl(
        'leistungen',
        LeistungenComponent.createForm(
          this.fb,
          originalAnlage.leistungen,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'vorschriften',
        VorschriftenComponent.vorschriftenCreateForm(
          this.fb,
          originalAnlage.vorschriften,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.checkSchreibsperreBetreiber();
    }
  }

  navigateBackToBetriebsstaette(): void {
    this.router.navigate(['.'], {
      relativeTo: this.route.parent.parent.parent,
    });
  }

  get isNeu(): boolean {
    return this.anlagenteil.id == null;
  }

  get allgemeinForm(): FormGroup<SpbAnlageAllgemeinFormData> {
    return this.anlageForm.get(
      'allgemein'
    ) as FormGroup<SpbAnlageAllgemeinFormData>;
  }

  get vorschriftenForm(): FormArray<VorschriftFormData> {
    const formArray: FormArray<VorschriftFormData> = this.anlageForm.get(
      'vorschriften'
    ) as FormArray<VorschriftFormData>;
    // Die Betreiber sollen die Vorschriften nurnoch lesen können. Dies wird über das disable der Form umgesetzt
    formArray.disable();
    return formArray;
  }

  get leistungenForm(): FormArray<LeistungenFormData> {
    return this.anlageForm.get('leistungen') as FormArray<LeistungenFormData>;
  }

  private createAnlageForm(
    initialAnlage: SpbAnlageFormData
  ): FormGroup<SpbAnlageFormData> {
    return this.fb.group<SpbAnlageFormData>({
      allgemein: SpbAnlageAllgemeinComponent.createForm(
        this.fb,
        initialAnlage.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      vorschriften: VorschriftenComponent.vorschriftenCreateForm(
        this.fb,
        initialAnlage.vorschriften,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      leistungen: LeistungenComponent.createForm(
        this.fb,
        initialAnlage.leistungen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, `/anlagenteil/nummer/${nummer}`);
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder die Original-Nr. oder "neu" in der URL und muss ersetzt werden
    this.updateUrl(nummer, new RegExp('/anlagenteil/.+$'));
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      this.uriEncoder.transform(nummer),
      '/anlagenteil/nummer/' +
        this.uriEncoder.transform(this.route.snapshot.params.anlagenteilNummer)
    );
  }

  navigateBack(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  canDeactivate(): boolean {
    return this.anlageForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
