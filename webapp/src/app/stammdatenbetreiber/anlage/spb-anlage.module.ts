import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { SpbAnlageRoutingModule } from '@/stammdatenbetreiber/anlage/routing/spb-anlage-routing.module';
import { SpbAnlageMainComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-main/spb-anlage-main.component';
import { SpbAnlageAllgemeinComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-allgemein/spb-anlage-allgemein.component';
import { SpbAnlageDetailsComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage-details/spb-anlage-details.component';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { SpbAnlageComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlage/spb-anlage.component';
import { SpbAnlagenListeComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlagen-liste/spb-anlagen-liste.component';
import { SpbAnlagenteilDetailsComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlagenteil-details/spb-anlagenteil-details.component';
import { SpbAnlagenteileListeComponent } from '@/stammdatenbetreiber/anlage/component/spb-anlagenteile-liste/spb-anlagenteile-liste.component';
import { BaseModule } from '@/base/base.module';
import { SpbQuellenModule } from '@/stammdatenbetreiber/quellen/spb-quellen-module';

@NgModule({
  declarations: [
    SpbAnlageDetailsComponent,
    SpbAnlageMainComponent,
    SpbAnlageAllgemeinComponent,
    SpbAnlageComponent,
    SpbAnlagenListeComponent,
    SpbAnlagenteilDetailsComponent,
    SpbAnlagenteileListeComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    ReactiveFormsModule,
    BasedomainModule,
    SpbAnlageRoutingModule,
    BaseModule,
    SpbQuellenModule,
  ],
  exports: [SpbAnlagenListeComponent],
})
export class SpbAnlageModule {}
