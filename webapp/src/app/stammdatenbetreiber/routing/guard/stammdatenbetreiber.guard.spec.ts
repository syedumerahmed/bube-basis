import { TestBed } from '@angular/core/testing';

import { StammdatenbetreiberGuard } from './stammdatenbetreiber.guard';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';

describe('StammdatenbetreiberGuard', () => {
  let guard: StammdatenbetreiberGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
    });
    guard = TestBed.inject(StammdatenbetreiberGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
