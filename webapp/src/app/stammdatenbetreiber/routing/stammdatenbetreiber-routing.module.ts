import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BerichtsjahrGuard } from '@/base/routing/guard/berichtsjahr.guard';
import { BerichtsjahrListResolver } from '@/referenzdaten/routing/resolver/berichtsjahr-list-resolver';
import { SpbBetriebsstaetteModule } from '../betriebsstaette/spb-betriebsstaette.module';
import { StammdatenComponent } from '../component/stammdaten/stammdaten.component';

const routes: Routes = [
  {
    path: 'jahr/:berichtsjahr',
    component: StammdatenComponent,
    canActivate: [BerichtsjahrGuard],
    resolve: {
      berichtsjahre: BerichtsjahrListResolver,
    },
    loadChildren: () => SpbBetriebsstaetteModule,
  },
  {
    path: '',
    redirectTo: 'jahr/',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class StammdatenbetreiberRoutingModule {}
