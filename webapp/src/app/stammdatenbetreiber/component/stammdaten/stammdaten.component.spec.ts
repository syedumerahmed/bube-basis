import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StammdatenComponent } from './stammdaten.component';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { StammdatenModule } from '@/stammdaten/stammdaten.module';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';

describe('StammdatenComponent', () => {
  let component: StammdatenComponent;
  let fixture: ComponentFixture<StammdatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StammdatenComponent],
      imports: [
        MockModule(BaseModule),
        MockModule(BasedomainModule),
        MockModule(StammdatenModule),
        RouterTestingModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StammdatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
