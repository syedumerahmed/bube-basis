import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import {
  SpbQuelleDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import { catchError } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';

@Injectable()
export class SpbQuelleResolver implements Resolve<SpbQuelleDto> {
  constructor(
    private service: SpbQuellenRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<SpbQuelleDto> {
    const betriebsstaetteId =
      route.parent.parent.data.betriebsstaette.betriebsstaetteId;
    const spbAnlageId = route.parent.data.anlage?.id;
    const anlageId = route.parent.data.anlage?.anlageId;
    const nummer = route.params.quelleNummer;

    if (anlageId) {
      return this.service
        .readSpbQuelleByAnlage(betriebsstaetteId, anlageId, nummer)
        .pipe(
          catchError((err) => {
            if (err.status === 404) {
              this.router.navigateByUrl(
                state.url.replace('/quelle/' + nummer, '')
              );
            }
            this.alertService.setAlert(err);
            return EMPTY;
          })
        );
    }
    if (spbAnlageId) {
      return this.service
        .readSpbQuelleBySpbAnlage(betriebsstaetteId, spbAnlageId, nummer)
        .pipe(
          catchError((err) => {
            if (err.status === 404) {
              this.router.navigateByUrl(
                state.url.replace('/quelle/' + nummer, '')
              );
            }
            this.alertService.setAlert(err);
            return EMPTY;
          })
        );
    }
    return this.service
      .readSpbQuelleByBetriebsstaette(betriebsstaetteId, nummer)
      .pipe(
        catchError((err) => {
          if (err.status === 404) {
            this.router.navigateByUrl(
              state.url.replace('/quelle/' + nummer, '')
            );
          }
          this.alertService.setAlert(err);
          return EMPTY;
        })
      );
  }
}
