import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { SpbQuelleDto } from '@api/stammdatenbetreiber';

@Injectable()
export class SpbQuelleNeuResolver implements Resolve<SpbQuelleDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<SpbQuelleDto> {
    return of({
      parentBetriebsstaetteId:
        route.parent.parent.data.betriebsstaette.betriebsstaetteId,
      parentAnlageId: route.parent.data.anlage?.anlageId,
      parentSpbAnlageId: route.parent.data.anlage?.id,
      quelleId: null,
      name: null,
      quelleNr: null,
    });
  }
}
