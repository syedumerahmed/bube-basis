import { QuellenArtListResolver } from '@/referenzdaten/routing/resolver/quellen-art-list-resolver';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SpbQuelleResolver } from '@/stammdatenbetreiber/quellen/routing/resolver/spb-quelle-resolver';
import { SpbQuelleNeuResolver } from '@/stammdatenbetreiber/quellen/routing/resolver/spb-quelle-neu-resolver';
import { SpbQuelleDetailsComponent } from '@/stammdatenbetreiber/quellen/component/spb-quelle-details/spb-quelle-details.component';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const quellenReferenzResolver = {
  quellenArtListe: QuellenArtListResolver,
};

const routes: Routes = [
  {
    path: 'neu',
    component: SpbQuelleDetailsComponent,
    canDeactivate: [UnsavedChangesGuard],
    resolve: {
      quelle: SpbQuelleNeuResolver,
      ...quellenReferenzResolver,
    },
  },
  {
    path: 'nummer/:quelleNummer',
    component: SpbQuelleDetailsComponent,
    canDeactivate: [UnsavedChangesGuard],
    runGuardsAndResolvers: 'always',
    resolve: {
      quelle: SpbQuelleResolver,
      ...quellenReferenzResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [SpbQuelleResolver, SpbQuelleNeuResolver],
})
export class SpbQuellenRoutingModule {}
