import { Component, OnInit } from '@angular/core';
import { ClrDatagridSortOrder } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlageDto,
  SpbBetriebsstaetteDto,
  SpbQuelleListItemDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  undoIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(undoIcon, factoryIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-spb-anlage-quellen-liste',
  templateUrl: './spb-anlage-quellen-liste.component.html',
  styleUrls: ['./spb-anlage-quellen-liste.component.scss'],
})
export class SpbAnlageQuellenListeComponent implements OnInit {
  loeschenDialog: boolean;
  spbQuellen: Array<SpbQuelleListItemDto>;
  spbBetriebsstaette: SpbBetriebsstaetteDto;
  spbAnlage: SpbAnlageDto;
  spbQuelleForDelete: SpbQuelleListItemDto;

  readonly SortOrder = ClrDatagridSortOrder;

  isNew(spbQuelle: SpbQuelleListItemDto): boolean {
    return spbQuelle.spbQuelleId == null;
  }

  constructor(
    private route: ActivatedRoute,
    private spbQuellenRestControllerService: SpbQuellenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.spbBetriebsstaette = data.betriebsstaette;
      this.spbAnlage = data.anlage;
      this.refresh();
    });
  }

  refresh(): void {
    this.spbQuellenRestControllerService
      .listAllSpbQuellenForAnlage({
        anlageId: this.spbAnlage.anlageId,
        spbAnlageId: this.spbAnlage.id,
        betriebsstaetteId: this.spbBetriebsstaette.betriebsstaetteId,
      })
      .subscribe(
        (spbQuellen) => (this.spbQuellen = spbQuellen),
        (error) => this.alertService.setAlert(error)
      );
  }

  deleteQuelle(): void {
    this.spbQuellenRestControllerService
      .deleteSpbQuelle(
        this.spbBetriebsstaette.betriebsstaetteId,
        this.spbQuelleForDelete.spbQuelleId
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  closeLoeschenDialog(): void {
    this.spbQuelleForDelete = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(spbQuelle: SpbQuelleListItemDto): void {
    this.spbQuelleForDelete = spbQuelle;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }

  getSpbStatus(spbQuelle: SpbQuelleListItemDto): string {
    if (spbQuelle.spbQuelleId == null) {
      return 'unverändert';
    }
    if (spbQuelle.quelleVorhanden) {
      return 'verändert';
    } else {
      return 'neu';
    }
  }
}
