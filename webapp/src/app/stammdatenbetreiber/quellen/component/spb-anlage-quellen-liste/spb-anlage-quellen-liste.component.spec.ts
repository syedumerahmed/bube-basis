import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbAnlageQuellenListeComponent } from './spb-anlage-quellen-liste.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockProvider } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlageDto,
  SpbBetriebsstaetteDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';

describe('SpbAnlageQuellenListeComponent', () => {
  const betriebsstaette: SpbBetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
  };
  const anlage: SpbAnlageDto = {
    anlageNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    id: 1,
  };

  let dataSubject: Subject<Data>;
  let component: SpbAnlageQuellenListeComponent;
  let fixture: ComponentFixture<SpbAnlageQuellenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({
      anlage: anlage,
      betriebsstaette: betriebsstaette,
    });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        MockProvider(SpbQuellenRestControllerService),
        MockProvider(AlertService),
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: {
              data: { anlage: anlage, betriebsstaette: betriebsstaette },
            },
          },
        },
      ],
      declarations: [SpbAnlageQuellenListeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SpbAnlageQuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save Id from betriebsstaette', () => {
    expect(component.spbBetriebsstaette.id).toBe(betriebsstaette.id);
  });

  it('should save Id from anlage', () => {
    expect(component.spbAnlage.id).toBe(anlage.id);
  });
});
