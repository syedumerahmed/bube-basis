import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SpbQuelleAllgemeinComponent } from './spb-quelle-allgemein.component';
import { FormBuilder } from '@/base/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { MockComponents, MockModule } from 'ng-mocks';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { SpbQuelleDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';

const quelle: SpbQuelleDto = {
  parentBetriebsstaetteId: 1,
  bauhoehe: 1,
  breite: 2,
  durchmesser: 3,
  flaeche: 4,
  laenge: 5,
  name: 'Quelle',
  quelleNr: 'Nr-1',
  quellenArt: { schluessel: '01' } as ReferenzDto,
  geoPunkt: {
    epsgCode: null,
    nord: null,
    ost: null,
  },
};

describe('SpbQuelleAllgemeinComponent', () => {
  const quellenArtListe: Array<ReferenzDto> = [];
  let component: SpbQuelleAllgemeinComponent;
  let fixture: ComponentFixture<SpbQuelleAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SpbQuelleAllgemeinComponent,
        MockComponents(ReferenzSelectComponent, GeoPunktComponent),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { data: { quellenArtListe } } },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(SpbQuelleAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = SpbQuelleAllgemeinComponent.createForm(
      formBuilder,
      SpbQuelleAllgemeinComponent.toForm(quelle),
      {} as ReferenzDto
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      SpbQuelleAllgemeinComponent.toForm(quelle)
    );
  });
});
