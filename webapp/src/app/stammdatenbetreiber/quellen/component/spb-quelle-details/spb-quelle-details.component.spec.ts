import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MockComponents, MockModule, MockProvider } from 'ng-mocks';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { AlertService } from '@/base/service/alert.service';
import { SpbQuelleDetailsComponent } from '@/stammdatenbetreiber/quellen/component/spb-quelle-details/spb-quelle-details.component';
import { SpbQuelleAllgemeinComponent } from '@/stammdatenbetreiber/quellen/component/spb-quelle-allgemein/spb-quelle-allgemein.component';
import {
  SpbQuelleDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import spyOn = jest.spyOn;

describe('SpbQuelleDetailsComponent', () => {
  const selectedBerichtsjahr = {
    id: 2020,
  } as ReferenzDto;
  const epsgCodeListe: Array<ReferenzDto> = [
    { schluessel: '01' },
  ] as Array<ReferenzDto>;

  const quelle: SpbQuelleDto = {
    parentBetriebsstaetteId: 1,
    name: 'quelle',
    quelleNr: '2',
  };

  const betriebsstaette = { id: 1 };
  const routeData = new BehaviorSubject({
    epsgCodeListe,
    quelle,
    betriebsstaette,
  });
  let component: SpbQuelleDetailsComponent;
  let fixture: ComponentFixture<SpbQuelleDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
      ],
      declarations: [
        SpbQuelleDetailsComponent,
        SpbQuelleAllgemeinComponent,
        MockComponents(ReferenzSelectComponent, GeoPunktComponent),
      ],
      providers: [
        MockProvider(SpbQuellenRestControllerService),
        MockProvider(AlertService),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData.asObservable(),
            snapshot: {
              params: { quelleNummer: '2' },
              data: { epsgCodeListe, schreibRecht: true },
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbQuelleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable berichtsjahr', inject(
    [BerichtsjahrService],
    (berichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.quelleForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.quelleForm.markAsDirty();
      component.quelleForm.markAsTouched();
      component.quelleForm.patchValue({ allgemein: { laenge: -1 } });
    });

    it('should be invalid', () => {
      expect(component.quelleForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          SpbQuelleAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [SpbQuellenRestControllerService],
        (quellenService: SpbQuellenRestControllerService) => {
          expect(quellenService.createSpbQuelle).not.toHaveBeenCalled();
          expect(quellenService.updateSpbQuelle).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.quelleForm.patchValue({
        allgemein: {
          name: 'Meine neue Quelle',
        },
      });
      component.quelleForm.markAsDirty();
      component.quelleForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.quelleForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedQuelleDto: SpbQuelleDto = {
        parentBetriebsstaetteId: 1,
        bauhoehe: null,
        durchmesser: null,
        breite: null,
        flaeche: null,
        laenge: null,
        quelleNr: '2',
        name: 'Meine neue Quelle',
        geoPunkt: null,
      };

      let quelleObservable: Subject<SpbQuelleDto>;
      beforeEach(inject(
        [SpbQuellenRestControllerService],
        (quellenService: SpbQuellenRestControllerService) => {
          quelleObservable = new Subject<SpbQuelleDto>();
          spyOn(quellenService, 'createSpbQuelle').mockReturnValue(
            quelleObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [SpbQuellenRestControllerService],
        (quellenService: SpbQuellenRestControllerService) => {
          expect(quellenService.createSpbQuelle).toHaveBeenCalledWith(
            expectedQuelleDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedQuelleDto,
          parentBetriebsstaetteId: 1,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          quelleObservable.next(responseDto);
          quelleObservable.complete();
        }));

        it('should update quelle and set parentBetriebsstaetteId', () => {
          expect(component.quelle).toStrictEqual({
            ...responseDto,
            parentBetriebsstaetteId: 1,
          });
        });

        it('should reset form', () => {
          expect(component.quelleForm.touched).toBe(false);
          expect(component.quelleForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });

  describe('on destroy', () => {
    beforeEach(() => {
      component.ngOnDestroy();
    });
    it('should reset berichtsjahr default', inject(
      [BerichtsjahrService],
      (berichtsjahrService) => {
        expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
      }
    ));
  });
});
