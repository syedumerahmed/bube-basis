import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  SpbQuelleAllgemeinComponent,
  SpbQuelleAllgemeinFormData,
} from '@/stammdatenbetreiber/quellen/component/spb-quelle-allgemein/spb-quelle-allgemein.component';
import {
  SpbQuelleDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import { GeoPunktDto, ReferenzDto } from '@api/stammdaten';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ClarityIcons, cloudIcon, floppyIcon, timesIcon } from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface SpbQuelleFormData {
  allgemein: SpbQuelleAllgemeinFormData;
}

ClarityIcons.addIcons(cloudIcon, timesIcon, floppyIcon);

@Component({
  selector: 'app-spb-quelle-details',
  templateUrl: './spb-quelle-details.component.html',
  styleUrls: ['./spb-quelle-details.component.scss'],
})
export class SpbQuelleDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(SpbQuelleAllgemeinComponent)
  allgemeinComponent: SpbQuelleAllgemeinComponent;

  quelle: SpbQuelleDto;
  quelleForm: FormGroup<SpbQuelleFormData>;

  saveAttempt = false;
  schreibsperreBetreiber = false;

  private parentBetriebsstaetteId;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  private static mapToForm(quelleDto: SpbQuelleDto): SpbQuelleFormData {
    return {
      allgemein: SpbQuelleAllgemeinComponent.toForm(quelleDto),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly quellenService: SpbQuellenRestControllerService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.setQuelle(data.quelle);
      this.schreibsperreBetreiber = data.betriebsstaette.schreibsperreBetreiber;
      this.reset();
    });
    this.quelleForm = this.createQuelleForm(
      SpbQuelleDetailsComponent.mapToForm(this.quelle)
    );
    this.checkSchreibsperreBetreiber();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  checkSchreibsperreBetreiber(): void {
    if (this.schreibsperreBetreiber) {
      this.quelleForm.disable();
    } else {
      this.quelleForm.enable();
    }
  }

  save(): void {
    this.saveAttempt = true;
    if (this.quelleForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }

    const formData = this.quelleForm.value;
    const quelleDto: SpbQuelleDto = {
      ...this.quelle,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
    };

    if (this.isNeu) {
      this.createQuelle(quelleDto);
    } else {
      this.updateQuelle(quelleDto);
    }
  }

  createQuelle(quelleDto: SpbQuelleDto): void {
    this.quellenService.createSpbQuelle(quelleDto).subscribe(
      (quelle) => {
        this.setQuelle(quelle);
        this.reset();
        this.updateUrlAfterCreate(quelle.quelleNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  updateQuelle(quelleDto: SpbQuelleDto): void {
    this.quellenService.updateSpbQuelle(quelleDto).subscribe(
      (quelle) => {
        this.setQuelle(quelle);
        this.reset();
        this.updateUrlAfterChange(quelle.quelleNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  getGeoPunkt(geoPunkt: SpbQuelleAllgemeinFormData['geoPunkt']): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.quelleForm) {
      this.saveAttempt = false;
      const originalQuelle = SpbQuelleDetailsComponent.mapToForm(this.quelle);
      this.quelleForm.reset(originalQuelle);
      this.checkSchreibsperreBetreiber();
    }
  }

  navigateBack(): void {
    this.router.navigate(['..'], { relativeTo: this.route.parent });
  }

  get isNeu(): boolean {
    return this.quelle.id == null;
  }

  get allgemeinForm(): FormGroup<SpbQuelleAllgemeinFormData> {
    return this.quelleForm.get(
      'allgemein'
    ) as FormGroup<SpbQuelleAllgemeinFormData>;
  }

  private setQuelle(quelle: SpbQuelleDto) {
    this.quelle = quelle;
    if (!this.quelle.parentBetriebsstaetteId) {
      this.quelle.parentBetriebsstaetteId = this.parentBetriebsstaetteId;
    }
  }

  private createQuelleForm(
    initialQuelle: SpbQuelleFormData
  ): FormGroup<SpbQuelleFormData> {
    return this.fb.group<SpbQuelleFormData>({
      allgemein: SpbQuelleAllgemeinComponent.createForm(
        this.fb,
        initialQuelle.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, '/quelle/nummer/' + encodeURI(nummer));
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder neu oder uebernehmen in der URL und muss ersetzt werden
    this.updateUrl(nummer, new RegExp('/quelle/(neu|uebernehmen)+'));
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      nummer,
      '/quelle/nummer/' + this.route.snapshot.params.quelleNummer
    );
  }

  isChildOfBetriebsstaette(): boolean {
    return !this.quelle.parentAnlageId && !this.quelle.parentSpbAnlageId;
  }

  canDeactivate(): boolean {
    return this.quelleForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
