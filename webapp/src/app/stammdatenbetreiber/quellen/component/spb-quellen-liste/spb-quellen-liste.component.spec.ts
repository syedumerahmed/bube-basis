import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockProvider } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import { SpbQuellenListeComponent } from '@/stammdatenbetreiber/quellen/component/spb-quellen-liste/spb-quellen-liste.component';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbBetriebsstaetteDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';

describe('SpbQuellenListeComponent', () => {
  const betriebsstaette: SpbBetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
  };

  let dataSubject: Subject<Data>;

  let component: SpbQuellenListeComponent;
  let fixture: ComponentFixture<SpbQuellenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        MockProvider(SpbQuellenRestControllerService),
        MockProvider(AlertService),
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
      declarations: [SpbQuellenListeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SpbQuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save Id from betriebsstaette', () => {
    expect(component.spbBetriebsstaette.id).toBe(betriebsstaette.id);
  });
});
