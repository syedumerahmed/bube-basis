import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClrDatagridSortOrder } from '@clr/angular';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbBetriebsstaetteDto,
  SpbQuelleListItemDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  undoIcon,
} from '@cds/core/icon';
ClarityIcons.addIcons(undoIcon, eyeIcon, plusIcon, factoryIcon);

@Component({
  selector: 'app-spb-quellen-liste',
  templateUrl: './spb-quellen-liste.component.html',
  styleUrls: ['./spb-quellen-liste.component.scss'],
})
export class SpbQuellenListeComponent implements OnInit {
  loeschenDialog: boolean;
  spbQuellen: Array<SpbQuelleListItemDto>;
  spbBetriebsstaette: SpbBetriebsstaetteDto;
  spbQuelleForDelete: SpbQuelleListItemDto;

  readonly SortOrder = ClrDatagridSortOrder;

  constructor(
    private route: ActivatedRoute,
    private spbQuellenRestControllerService: SpbQuellenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.spbBetriebsstaette = data.betriebsstaette;
      this.refresh();
    });
  }

  refresh(): void {
    this.spbQuellenRestControllerService
      .listAllSpbQuellenForBetriebsstaette(
        this.spbBetriebsstaette.betriebsstaetteId
      )
      .subscribe(
        (spbQuellen) => (this.spbQuellen = spbQuellen),
        (error) => this.alertService.setAlert(error)
      );
  }

  deleteQuelle(): void {
    this.spbQuellenRestControllerService
      .deleteSpbQuelle(
        this.spbBetriebsstaette.betriebsstaetteId,
        this.spbQuelleForDelete.spbQuelleId
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  closeLoeschenDialog(): void {
    this.spbQuelleForDelete = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(spbQuelle: SpbQuelleListItemDto): void {
    this.spbQuelleForDelete = spbQuelle;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }

  getSpbStatus(spbQuelle: SpbQuelleListItemDto): string {
    if (spbQuelle.spbQuelleId == null) {
      return 'unverändert';
    }
    if (spbQuelle.quelleVorhanden) {
      return 'verändert';
    } else {
      return 'neu';
    }
  }

  isNew(spbQuelle: SpbQuelleListItemDto): boolean {
    return spbQuelle.spbQuelleId == null;
  }
}
