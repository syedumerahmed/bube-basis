import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { SpbAnlageQuellenListeComponent } from '@/stammdatenbetreiber/quellen/component/spb-anlage-quellen-liste/spb-anlage-quellen-liste.component';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';
import { SpbQuelleAllgemeinComponent } from '@/stammdatenbetreiber/quellen/component/spb-quelle-allgemein/spb-quelle-allgemein.component';
import { SpbQuelleDetailsComponent } from '@/stammdatenbetreiber/quellen/component/spb-quelle-details/spb-quelle-details.component';
import { SpbQuellenListeComponent } from '@/stammdatenbetreiber/quellen/component/spb-quellen-liste/spb-quellen-liste.component';
import { SpbQuellenRoutingModule } from '@/stammdatenbetreiber/quellen/routing/spb-quellen-routing.module';

@NgModule({
  declarations: [
    SpbAnlageQuellenListeComponent,
    SpbQuelleAllgemeinComponent,
    SpbQuelleDetailsComponent,
    SpbQuellenListeComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    ReactiveFormsModule,
    BasedomainModule,
    SpbQuellenRoutingModule,
    BaseModule,
  ],
  exports: [SpbAnlageQuellenListeComponent, SpbQuellenListeComponent],
})
export class SpbQuellenModule {}
