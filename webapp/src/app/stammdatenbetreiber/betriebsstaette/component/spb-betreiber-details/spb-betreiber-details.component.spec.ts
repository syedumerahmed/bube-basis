import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbBetreiberDetailsComponent } from './spb-betreiber-details.component';
import { MockComponents, MockModule, MockPipe, MockProvider } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { SpbBetreiberAllgemeinComponent } from '@/stammdatenbetreiber/betriebsstaette/component/spb-betreiber-allgemein/spb-betreiber-allgemein.component';
import { SpbAdresseComponent } from '@/stammdatenbetreiber/betriebsstaette/component/spb-adresse/spb-adresse.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import {
  SpbBetreiberDto,
  SpbBetreiberRestControllerService,
} from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';

describe('SpbBetreiberComponent', () => {
  let component: SpbBetreiberDetailsComponent;
  let fixture: ComponentFixture<SpbBetreiberDetailsComponent>;

  beforeEach(async () => {
    const routeData$ = new BehaviorSubject({
      betreiber: { name: 'Der Betreiber' } as SpbBetreiberDto,
      betriebsstaette: { id: 4711 },
      landIsoCodeListe: [] as ReferenzDto[],
    });
    await TestBed.configureTestingModule({
      declarations: [
        SpbBetreiberDetailsComponent,
        MockComponents(SpbBetreiberAllgemeinComponent, SpbAdresseComponent),
        MockPipe(EllipsizePipe),
      ],
      imports: [MockModule(ClarityModule), RouterTestingModule],
      providers: [
        MockProvider(SpbBetreiberRestControllerService),
        MockProvider(ActivatedRoute, {
          data: routeData$.asObservable(),
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbBetreiberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
