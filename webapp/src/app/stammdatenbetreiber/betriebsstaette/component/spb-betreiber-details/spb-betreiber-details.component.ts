import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  SpbAdresseComponent,
  SpbAdresseFormData,
} from '@/stammdatenbetreiber/betriebsstaette/component/spb-adresse/spb-adresse.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  SpbBetreiberAllgemeinComponent,
  SpbBetreiberAllgemeinFormData,
} from '@/stammdatenbetreiber/betriebsstaette/component/spb-betreiber-allgemein/spb-betreiber-allgemein.component';
import { AdresseFormData } from '@/basedomain/component/adresse/adresse.component';
import { BetreiberAllgemeinFormData } from '@/stammdaten/betriebsstaette/component/betreiber-allgemein/betreiber-allgemein.component';
import { tap } from 'rxjs/operators';
import {
  SpbBetreiberDto,
  SpbBetreiberRestControllerService,
  SpbBetriebsstaetteDto,
} from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import {
  buildingIcon,
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  timesIcon,
  trashIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface SpbBetreiberFormData {
  adresse?: SpbAdresseFormData;
  allgemein: SpbBetreiberAllgemeinFormData;
}

ClarityIcons.addIcons(
  trashIcon,
  buildingIcon,
  timesIcon,
  floppyIcon,
  errorStandardIcon
);

@Component({
  selector: 'app-spb-betreiber-details',
  templateUrl: './spb-betreiber-details.component.html',
  styleUrls: ['./spb-betreiber-details.component.scss'],
})
export class SpbBetreiberDetailsComponent implements OnInit, UnsavedChanges {
  @ViewChild(SpbBetreiberAllgemeinComponent)
  allgemeinComponent: SpbBetreiberAllgemeinComponent;

  @ViewChild(SpbAdresseComponent)
  adresseComponent: SpbAdresseComponent;

  betreiberForm: FormGroup<SpbBetreiberFormData>;

  betreiber: SpbBetreiberDto;
  betriebsstaetteId: number;
  isoCodes: Referenzliste<Required<ReferenzDto>>;

  saveAttempt = false;

  get hasBetreiber(): boolean {
    return this.betreiber?.betreiberId != null;
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly spbBetreiberService: SpbBetreiberRestControllerService,
    private readonly alertService: AlertService,
    private readonly router: Router,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betreiber = data.betreiber;
      this.betriebsstaetteId = data.betriebsstaette.betriebsstaetteId;
      this.isoCodes = new Referenzliste(data.landIsoCodeListe);
      this.betreiberForm = this.createForm(this.mapToFrom(this.betreiber));
      this.reset();
      this.checkSchreibsperreBetreiber(data.betriebsstaette);
    });
  }

  checkSchreibsperreBetreiber(betriebsstaette: SpbBetriebsstaetteDto): void {
    if (betriebsstaette.schreibsperreBetreiber) {
      this.betreiberForm.disable();
    } else {
      this.betreiberForm.enable();
    }
  }

  private mapToFrom(betreiberDto: SpbBetreiberDto): SpbBetreiberFormData {
    return {
      allgemein: SpbBetreiberAllgemeinComponent.toForm(betreiberDto),
      adresse: SpbAdresseComponent.toForm(betreiberDto?.adresse, this.isoCodes),
    };
  }

  private createForm(
    betreiber: SpbBetreiberFormData
  ): FormGroup<SpbBetreiberFormData> {
    return this.fb.group<SpbBetreiberFormData>({
      allgemein: SpbBetreiberAllgemeinComponent.createForm(
        this.fb,
        betreiber.allgemein
      ),
      adresse: SpbAdresseComponent.createForm(this.fb, betreiber.adresse),
    });
  }

  formToBetreiber(): SpbBetreiberDto {
    const formData = this.betreiberForm.getRawValue();
    return {
      ...this.betreiber,
      name: formData.allgemein.name,
      adresse: FormUtil.emptyStringsToNull(formData.adresse),
    };
  }

  reset(): void {
    if (this.betreiberForm) {
      this.saveAttempt = false;
      const originalBetreiber = this.mapToFrom(this.betreiber);
      this.betreiberForm.reset(originalBetreiber);
      this.betreiberForm.setControl(
        'adresse',
        SpbAdresseComponent.createForm(this.fb, originalBetreiber.adresse)
      );
    }
  }

  get isNeu(): boolean {
    return this.betreiber?.id == null;
  }

  save(): void {
    this.saveAttempt = true;
    if (this.betreiberForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.adresseComponent?.markAllAsTouched();
      return;
    }

    const betreiberDto: SpbBetreiberDto = this.formToBetreiber();
    betreiberDto.betriebsstaetteId = this.betriebsstaetteId;
    if (this.isNeu) {
      this.createBetreiber(betreiberDto);
    } else {
      this.updateBetreiber(betreiberDto);
    }
    this.betreiberForm.markAsPristine();
  }

  updateBetreiber(betreiberDto: SpbBetreiberDto): void {
    this.spbBetreiberService.updateSpbBetreiber(betreiberDto).subscribe(
      (betreiber) => {
        this.betreiber = betreiber;
        this.reset();
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  createBetreiber(betreiberDto: SpbBetreiberDto): void {
    this.spbBetreiberService
      .createSpbBetreiber(betreiberDto)
      .pipe(
        tap(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe(
        (betreiber) => {
          this.betreiber = betreiber;
          this.reset();
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  get allgemeinForm(): FormGroup<BetreiberAllgemeinFormData> {
    return this.betreiberForm.controls
      .allgemein as FormGroup<BetreiberAllgemeinFormData>;
  }

  get adresseForm(): FormGroup<AdresseFormData> {
    return this.betreiberForm.controls.adresse as FormGroup<AdresseFormData>;
  }

  get canDelete(): boolean {
    return this.betreiber?.id != null;
  }

  delete(): void {
    this.spbBetreiberService
      .deleteSpbBetreiber(this.betreiber.id)
      .pipe(
        tap(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe(
        (data) => {
          this.betreiber = data;
          this.reset();
        },
        (err) => {
          this.alertService.setAlert(err);
        }
      );
  }

  canDeactivate(): boolean {
    return this.betreiberForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
