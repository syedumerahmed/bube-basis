import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { SpbAdresseDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';

export interface SpbAdresseFormData {
  strasse: string;
  hausNr: string;
  plz: string;
  ort: string;
  ortsteil: string;
  landIsoCode: ReferenzDto;
  postfach: string;
  postfachPlz: string;
}

@Component({
  selector: 'app-spb-adresse',
  templateUrl: './spb-adresse.component.html',
  styleUrls: ['./spb-adresse.component.scss'],
})
export class SpbAdresseComponent implements AfterViewInit {
  @Input()
  form: FormGroup<SpbAdresseFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  isoCodes: Referenzliste<Required<ReferenzDto>>;

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  static toForm(
    adresseDto: SpbAdresseDto,
    landIsoCodes: Referenzliste<Required<ReferenzDto>>
  ): SpbAdresseFormData {
    return {
      strasse: adresseDto?.strasse,
      hausNr: adresseDto?.hausNr,
      plz: adresseDto?.plz,
      ort: adresseDto?.ort,
      ortsteil: adresseDto?.ortsteil,
      landIsoCode: landIsoCodes.find(
        adresseDto?.landIsoCode as Required<ReferenzDto>
      ),
      postfach: adresseDto?.postfach,
      postfachPlz: adresseDto?.postfachPlz,
    };
  }

  static createForm(
    fb: FormBuilder,
    adresse: SpbAdresseFormData
  ): FormGroup<SpbAdresseFormData> {
    return fb.group({
      strasse: [adresse.strasse, [Validators.maxLength(64)]],
      hausNr: [adresse.hausNr, [Validators.maxLength(14)]],
      plz: [adresse.plz, [Validators.required, Validators.maxLength(11)]],
      ort: [adresse.ort, [Validators.required, Validators.maxLength(64)]],
      ortsteil: [adresse.ortsteil, [Validators.maxLength(64)]],
      postfachPlz: [adresse.postfachPlz, [Validators.maxLength(24)]],
      postfach: [adresse.postfach, [Validators.maxLength(12)]],
      landIsoCode: adresse.landIsoCode,
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }
}
