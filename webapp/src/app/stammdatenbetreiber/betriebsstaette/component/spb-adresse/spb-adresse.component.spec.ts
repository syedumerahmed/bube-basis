import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SpbAdresseComponent } from '@/stammdatenbetreiber/betriebsstaette/component/spb-adresse/spb-adresse.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { FormBuilder } from '@/base/forms';
import { MockComponents, MockModule } from 'ng-mocks';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { SpbAdresseDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';

describe('SpbAdresseComponent', () => {
  const adresse: SpbAdresseDto = {
    hausNr: '1',
    landIsoCode: { schluessel: 'de' } as ReferenzDto,
    ort: 'Ort',
    plz: '12345',
  };

  let component: SpbAdresseComponent;
  let fixture: ComponentFixture<SpbAdresseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SpbAdresseComponent,
        MockComponents(ReferenzSelectComponent, TooltipComponent),
      ],
      imports: [MockModule(ReactiveFormsModule), MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(SpbAdresseComponent);
    component = fixture.componentInstance;
    component.isoCodes = new Referenzliste([]);
    component.form = SpbAdresseComponent.createForm(
      formBuilder,
      SpbAdresseComponent.toForm(adresse, component.isoCodes)
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
