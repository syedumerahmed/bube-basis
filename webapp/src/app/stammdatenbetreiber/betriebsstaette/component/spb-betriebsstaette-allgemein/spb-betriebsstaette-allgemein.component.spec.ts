import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SpbBetriebsstaetteAllgemeinComponent } from './spb-betriebsstaette-allgemein.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { MockModule } from 'ng-mocks';
import { FormBuilder } from '@/base/forms';
import { of } from 'rxjs';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';
import { SpbBetriebsstaetteDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';

describe('BetriebsstaetteAllgemeinComponent', () => {
  const betriebsstaette: SpbBetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    berichtsjahr: { schluessel: '2020' } as ReferenzDto,
    land: '00',
    zustaendigeBehoerde: '01',
    name: 'Meine neue Betriebsstaette',
    betriebsstaetteNr: 'BST1234',
    betriebsstatus: { schluessel: '01' } as ReferenzDto,
    akz: 'akz',
    einleiterNummern: ['nr1', 'nr2'],
    abfallerzeugerNummern: ['abfall', 'erzeuger', 'nr'],
    naceCode: { schluessel: '01' } as ReferenzDto,
    inbetriebnahme: '2020-10-10',
    flusseinzugsgebiet: { schluessel: '01' } as ReferenzDto,
    indirekteinleiter: true,
    direkteinleiter: false,
    betriebsstatusSeit: '2010-12-12',
  };

  const betriebsstatusListe: Array<Required<ReferenzDto>> = [];
  const gemeindekennzifferListe: Array<Required<ReferenzDto>> = [];
  const flusseinzugsgebietListe: Array<Required<ReferenzDto>> = [];
  const naceCodeListe: Array<ReferenzDto> = [];
  const epsgCodeListe: Array<ReferenzDto> = [];

  let component: SpbBetriebsstaetteAllgemeinComponent;
  let fixture: ComponentFixture<SpbBetriebsstaetteAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SpbBetriebsstaetteAllgemeinComponent],
      imports: [
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(BasedomainModule),
        MockModule(BaseModule),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              data: of({
                betriebsstatusListe,
                gemeindekennzifferListe,
                flusseinzugsgebietListe,
                naceCodeListe,
                epsgCodeListe,
              }),
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(SpbBetriebsstaetteAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = SpbBetriebsstaetteAllgemeinComponent.createForm(
      formBuilder,
      SpbBetriebsstaetteAllgemeinComponent.toForm(
        betriebsstaette,
        new Referenzliste<Required<ReferenzDto>>(flusseinzugsgebietListe)
      ),
      {} as ReferenzDto
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
