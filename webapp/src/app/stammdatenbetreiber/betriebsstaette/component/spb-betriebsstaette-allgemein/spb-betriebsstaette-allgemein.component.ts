import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { SpbBetriebsstaetteDto } from '@api/stammdatenbetreiber';
import { ReferenzDto } from '@api/stammdaten';
import {
  GeoPunktComponent,
  GeoPunktFormData,
} from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { BubeValidators } from '@/base/forms/bube-validators';

export interface SpbBetriebsstaetteAllgemeinFormData {
  name: string;
  betriebsstaetteNr: string;
  akz: string;
  inbetriebnahme: string;
  zustaendigeBehoerde: string;
  betriebsstatus: ReferenzDto;
  betriebsstatusSeit: string;
  naceCode: ReferenzDto;
  flusseinzugsgebiet: ReferenzDto;
  geoPunkt: GeoPunktFormData;
  abfallerzeugerNummern: Array<string>;
  einleiterNummern: Array<string>;
  direkteinleiter: boolean;
  indirekteinleiter: boolean;
}

@Component({
  selector: 'app-betriebsstaette-allgemein',
  templateUrl: './spb-betriebsstaette-allgemein.component.html',
  styleUrls: ['./spb-betriebsstaette-allgemein.component.scss'],
  providers: [DatePipe],
})
export class SpbBetriebsstaetteAllgemeinComponent
  implements OnInit, AfterViewInit, AfterViewChecked
{
  constructor(
    private readonly datePipe: DatePipe,
    private readonly route: ActivatedRoute
  ) {}

  @Input()
  form: FormGroup<SpbBetriebsstaetteAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;
  @ViewChild(GeoPunktComponent)
  geoPunktComponent: GeoPunktComponent;

  betriebsstatusListe: Referenzliste<Required<ReferenzDto>>;
  flusseinzugsgebietListe: Referenzliste<Required<ReferenzDto>>;
  naceCodeListe: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  today: string;

  static toForm(
    betriebsstaetteDto: SpbBetriebsstaetteDto,
    flusseinzugsgebiete: Referenzliste<Required<ReferenzDto>>
  ): SpbBetriebsstaetteAllgemeinFormData {
    return {
      name: betriebsstaetteDto.name,
      betriebsstatus: betriebsstaetteDto.betriebsstatus,
      zustaendigeBehoerde: betriebsstaetteDto.zustaendigeBehoerde,
      geoPunkt: GeoPunktComponent.toForm(betriebsstaetteDto.geoPunkt),
      abfallerzeugerNummern: [...betriebsstaetteDto.abfallerzeugerNummern],
      einleiterNummern: [...betriebsstaetteDto.einleiterNummern],
      akz: betriebsstaetteDto.akz,
      betriebsstaetteNr: betriebsstaetteDto.betriebsstaetteNr,
      betriebsstatusSeit: FormUtil.fromIsoDate(
        betriebsstaetteDto.betriebsstatusSeit
      ),
      direkteinleiter: betriebsstaetteDto.direkteinleiter,
      indirekteinleiter: betriebsstaetteDto.indirekteinleiter,
      flusseinzugsgebiet: flusseinzugsgebiete.find(
        betriebsstaetteDto.flusseinzugsgebiet as Required<ReferenzDto>
      ),
      inbetriebnahme: FormUtil.fromIsoDate(betriebsstaetteDto.inbetriebnahme),
      naceCode: betriebsstaetteDto.naceCode,
    };
  }

  static createForm(
    fb: FormBuilder,
    betriebsstaette: SpbBetriebsstaetteAllgemeinFormData,
    selectedYear: ReferenzDto
  ): FormGroup<SpbBetriebsstaetteAllgemeinFormData> {
    return fb.group<SpbBetriebsstaetteAllgemeinFormData>({
      akz: [betriebsstaette.akz, [Validators.maxLength(14)]],
      name: [
        betriebsstaette.name,
        [Validators.required, Validators.maxLength(250)],
      ],
      betriebsstaetteNr: [
        betriebsstaette.betriebsstaetteNr,
        [Validators.required, Validators.maxLength(20)],
      ],
      inbetriebnahme: [betriebsstaette.inbetriebnahme, BubeValidators.date],
      zustaendigeBehoerde: [
        betriebsstaette.zustaendigeBehoerde,
        [Validators.required],
      ],
      betriebsstatus: [betriebsstaette.betriebsstatus, [Validators.required]],
      direkteinleiter: [betriebsstaette.direkteinleiter],
      indirekteinleiter: [betriebsstaette.indirekteinleiter],
      betriebsstatusSeit: [
        betriebsstaette.betriebsstatusSeit,
        BubeValidators.date,
      ],
      geoPunkt: GeoPunktComponent.createForm(
        fb,
        betriebsstaette.geoPunkt,
        selectedYear
      ),
      flusseinzugsgebiet: [betriebsstaette.flusseinzugsgebiet],
      naceCode: [betriebsstaette.naceCode],
      einleiterNummern: [betriebsstaette.einleiterNummern],
      abfallerzeugerNummern: [betriebsstaette.abfallerzeugerNummern],
    });
  }

  ngOnInit(): void {
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.route.parent.data.subscribe((data) => {
      this.betriebsstatusListe = new Referenzliste(data.betriebsstatusListe);
      this.flusseinzugsgebietListe = new Referenzliste(
        data.flusseinzugsgebietListe
      );
      this.naceCodeListe = new Referenzliste(data.naceCodeListe);
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm?.markAsTouched();
  }

  get geoPunktForm(): FormGroup<GeoPunktFormData> {
    return this.form.get('geoPunkt') as FormGroup<GeoPunktFormData>;
  }
}
