import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { AlertService } from '@/base/service/alert.service';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { SpbBetriebsstaetteDetailsComponent } from '@/stammdatenbetreiber/betriebsstaette/component/spb-betriebsstaette-details/spb-betriebsstaette-details.component';
import { SpbBetreiberDetailsComponent } from '@/stammdatenbetreiber/betriebsstaette/component/spb-betreiber-details/spb-betreiber-details.component';
import { SpbBetriebsstaettenRestControllerService } from '@api/stammdatenbetreiber';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';

@Component({
  selector: 'app-betriebsstaette-main',
  templateUrl: './spb-betriebsstaette-main.component.html',
  styleUrls: ['./spb-betriebsstaette-main.component.scss'],
})
export class SpbBetriebsstaetteMainComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(SpbBetriebsstaetteDetailsComponent, { static: true })
  spbBetriebsstaetteDetailsComponent: SpbBetriebsstaetteDetailsComponent;

  @ViewChild(SpbBetreiberDetailsComponent, { static: true })
  spbBetreiberDetailsComponent: SpbBetreiberDetailsComponent;

  quellenDisplay: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly quellenZuordnungService: QuellenZuordungService,
    private readonly spbBetriebsstaettenService: SpbBetriebsstaettenRestControllerService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.quellenZuordnungService
        .quellenSindAnBetriebsstaetten(
          data.betriebsstaette.berichtsjahr.schluessel,
          data.betriebsstaette.land
        )
        .subscribe(
          (ret) => {
            this.quellenDisplay = ret;
          },
          (error) => this.alertService.setAlert(error)
        );
      this.spbBetriebsstaettenService
        .getAllYearsForBetriebsstaette(data.betriebsstaette.betriebsstaetteId)
        .subscribe((jahre) => {
          this.berichtsjahrService.setFilter((jahr: ReferenzDto) => {
            return jahre.map((j) => j.schluessel).includes(jahr.schluessel);
          });
        });
    });
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.resetFilter();
  }

  navigateBackToUebersicht(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent.parent });
  }

  canDeactivate(): boolean {
    return (
      this.spbBetriebsstaetteDetailsComponent.canDeactivate() &&
      this.spbBetreiberDetailsComponent.canDeactivate()
    );
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
