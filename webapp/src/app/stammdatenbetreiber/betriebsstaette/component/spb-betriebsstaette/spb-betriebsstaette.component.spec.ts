import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpbBetriebsstaetteComponent } from './spb-betriebsstaette.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('BetriebsstaetteComponent', () => {
  let component: SpbBetriebsstaetteComponent;
  let fixture: ComponentFixture<SpbBetriebsstaetteComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [SpbBetriebsstaetteComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbBetriebsstaetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
