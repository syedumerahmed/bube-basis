import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, combineLatest, of, Subject } from 'rxjs';
import { ClrDatagridStateInterface } from '@clr/angular';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { AlertService } from '@/base/service/alert.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { DesktopService } from '@/desktop/service/desktop.service';
import {
  ReferenzDto,
  SortStateDtoBetriebsstaetteColumnByEnum,
} from '@api/stammdaten';
import {
  PageDtoSpbBetriebsstaetteListItemDto,
  SpbBetriebsstaettenRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  buildingIcon,
  checkIcon,
  ClarityIcons,
  eyeIcon,
  timesIcon,
  treeViewIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(
  buildingIcon,
  checkIcon,
  timesIcon,
  treeViewIcon,
  eyeIcon
);
@Component({
  selector: 'app-betriebsstaetten-liste',
  templateUrl: './spb-betriebsstaetten-liste.component.html',
  styleUrls: ['./spb-betriebsstaetten-liste.component.scss'],
})
export class SpbBetriebsstaettenListeComponent implements OnInit, OnDestroy {
  betriebsstaettenPage: PageDtoSpbBetriebsstaetteListItemDto;
  loading = true;
  currentPage: number;

  private datagridState$: Subject<ClrDatagridStateInterface> = new Subject();

  selectedIds: Array<number> = [];
  currentBerichtsjahr$: BehaviorSubject<ReferenzDto> = new BehaviorSubject(
    null
  );

  private readonly destroyed$ = new Subject();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly betriebsstaettenRestControllerService: SpbBetriebsstaettenRestControllerService,
    private readonly alertService: AlertService,
    private readonly desktopService: DesktopService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.selectedBerichtsjahr$
      .pipe(takeUntil(this.destroyed$), tap(this.currentBerichtsjahr$))
      .subscribe(() => {
        this.currentPage = 1;
        this.markierungAufheben();
      });
    combineLatest([this.currentBerichtsjahr$, this.datagridState$])
      .pipe(takeUntil(this.destroyed$))
      .subscribe(([berichtsjahr, dgState]) => {
        this.refresh(berichtsjahr, dgState);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  onDatagridStateChanged(state: ClrDatagridStateInterface): void {
    this.datagridState$.next(state);
  }

  refresh(
    berichtsjahr: ReferenzDto,
    { page, sort: gridSort }: ClrDatagridStateInterface
  ): void {
    this.loading = true;
    this.betriebsstaettenRestControllerService
      .readAllSpbBetriebsstaette(berichtsjahr.id, {
        page: {
          size: page?.size,
          current: page ? page.current - 1 : 0,
        },
        sort: gridSort
          ? {
              by: gridSort.by as SortStateDtoBetriebsstaetteColumnByEnum,
              reverse: gridSort.reverse,
            }
          : null,
      })
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.alertService.setAlert(err.message);
          return of<PageDtoSpbBetriebsstaetteListItemDto>(null);
        }),
        tap(() => {
          this.loading = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe((response) => (this.betriebsstaettenPage = response));
  }

  alleMarkieren(): void {
    const berichtsjahrId: number = this.currentBerichtsjahr$.value.id;
    this.betriebsstaettenRestControllerService
      .readAllSpbBetriebsstaetteIds(berichtsjahrId)
      .pipe(tap(() => this.changeDetectorRef.markForCheck()))
      .subscribe(
        (idList) => (this.selectedIds = idList),
        (err: HttpErrorResponse) => this.alertService.setAlert(err.message)
      );
  }

  markierungAufheben(): void {
    this.selectedIds = [];
  }

  get nothingSelected(): boolean {
    return this.selectedIds.length === 0;
  }

  inDesktopSpeichern(): void {
    this.desktopService.inDesktopSpeichern(
      'SPB_STAMMDATEN',
      this.selectedIds,
      this.currentBerichtsjahr$.value.schluessel
    );
    this.markierungAufheben();
  }
}
