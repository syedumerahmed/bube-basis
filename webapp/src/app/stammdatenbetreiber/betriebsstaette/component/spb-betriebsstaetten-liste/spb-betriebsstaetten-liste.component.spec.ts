import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SpbBetriebsstaettenListeComponent } from './spb-betriebsstaetten-liste.component';
import { MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule, ClrDatagridStateInterface } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { BehaviorSubject, EMPTY, Observable, Subject } from 'rxjs';
import {
  PageDtoBetriebsstaetteListItemDto,
  ReferenzDto,
  SortStateDtoBetriebsstaetteColumnByEnum,
} from '@api/stammdaten';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { DesktopService } from '@/desktop/service/desktop.service';
import { AlertService } from '@/base/service/alert.service';
import {
  PageDtoSpbBetriebsstaetteListItemDto,
  SpbBetriebsstaettenRestControllerService,
} from '@api/stammdatenbetreiber';
import spyOn = jest.spyOn;
import anything = jasmine.anything;

describe('BetriebsstaettenListeComponent', () => {
  let component: SpbBetriebsstaettenListeComponent;
  let fixture: ComponentFixture<SpbBetriebsstaettenListeComponent>;
  let selectedBerichtsjahr$: BehaviorSubject<ReferenzDto>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SpbBetriebsstaettenListeComponent],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProviders(
          BerichtsjahrService,
          SpbBetriebsstaettenRestControllerService,
          DesktopService,
          AlertService
        ),
      ],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      selectedBerichtsjahr$ = new BehaviorSubject({ id: 2020 } as ReferenzDto);
      spyOn(
        berichtsjahrService,
        'selectedBerichtsjahr$',
        'get'
      ).mockReturnValue(selectedBerichtsjahr$.asObservable());
      fixture = TestBed.createComponent(SpbBetriebsstaettenListeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }
  ));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('refresh on datagrid change', () => {
    const pageResponse: PageDtoBetriebsstaetteListItemDto = {
      totalElements: 0,
      content: [],
    };
    let state: ClrDatagridStateInterface;
    let response$: Subject<PageDtoSpbBetriebsstaetteListItemDto>;

    beforeEach(inject(
      [SpbBetriebsstaettenRestControllerService],
      (betriebsstaettenService: SpbBetriebsstaettenRestControllerService) => {
        response$ = new Subject();
        spyOn(
          betriebsstaettenService,
          'readAllSpbBetriebsstaette'
        ).mockReturnValue(response$.asObservable() as Observable<any>);
        state = {
          page: {
            from: 1,
            to: 3,
            size: 5,
            current: 7,
          },
          sort: {
            by: 'ORT',
            reverse: true,
          },
        };
        component.onDatagridStateChanged(state);
      }
    ));

    describe('before result', () => {
      it('should set loading to be true', () => {
        expect(component.loading).toBe(true);
      });

      it('should invoke readAllBetriebssaette with mapped state to gridstatedto and berichtsjahr', inject(
        [SpbBetriebsstaettenRestControllerService],
        (betriebsstaettenService: SpbBetriebsstaettenRestControllerService) => {
          expect(
            betriebsstaettenService.readAllSpbBetriebsstaette
          ).toHaveBeenLastCalledWith(2020, {
            page: { size: 5, current: 6 },
            sort: {
              by: SortStateDtoBetriebsstaetteColumnByEnum.ORT,
              reverse: true,
            },
          });
        }
      ));
    });

    describe('successful', () => {
      beforeEach(() => {
        response$.next(pageResponse);
      });

      it('should set loading to be false', () => {
        expect(component.loading).toBe(false);
      });

      it('should set new page to betriebsstaettenpage', () => {
        expect(component.betriebsstaettenPage).toBe(pageResponse);
      });
    });

    describe('unsuccessful', () => {
      beforeEach(() => {
        response$.error('');
      });

      it('should set loading to be false', () => {
        expect(component.loading).toBe(false);
      });

      it('should set alert message', inject(
        [AlertService],
        (alertService: AlertService) => {
          expect(alertService.setAlert).toHaveBeenCalled();
        }
      ));
    });
  });

  describe('on berichtsjahrwechsel', () => {
    let state: ClrDatagridStateInterface;

    beforeEach(() => {
      state = { page: { current: 3 } };
      component.currentPage = 3;
      selectedBerichtsjahr$.next({ id: 2019 } as ReferenzDto);
      component.onDatagridStateChanged(state);
    });

    it('should set loading to be true', () => {
      expect(component.loading).toBe(true);
    });

    it('should reset to page 1', () => {
      expect(component.currentPage).toBe(1);
    });

    it('should clear selection', () => {
      expect(component.selectedIds).toHaveLength(0);
    });

    it('should invoke readAllBetriebssaette with mapped state to gridstatedto and berichtsjahr', inject(
      [SpbBetriebsstaettenRestControllerService],
      (betriebsstaettenService: SpbBetriebsstaettenRestControllerService) => {
        expect(
          betriebsstaettenService.readAllSpbBetriebsstaette
        ).toHaveBeenLastCalledWith(2019, anything());
      }
    ));
  });

  it('markierung aufheben should empty selectedIds', () => {
    component.markierungAufheben();
    expect(component.selectedIds).toHaveLength(0);
  });

  it('alle markieren should call readAllBetriebsstaetteIds', inject(
    [SpbBetriebsstaettenRestControllerService],
    (betriebsstaettenService: SpbBetriebsstaettenRestControllerService) => {
      spyOn(
        betriebsstaettenService,
        'readAllSpbBetriebsstaetteIds'
      ).mockReturnValue(EMPTY);

      component.alleMarkieren();

      expect(
        betriebsstaettenService.readAllSpbBetriebsstaetteIds
      ).toHaveBeenLastCalledWith(2020);
    }
  ));
});
