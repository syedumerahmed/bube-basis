import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { SpbBetreiberDto } from '@api/stammdatenbetreiber';

export interface SpbBetreiberAllgemeinFormData {
  name: string;
}

@Component({
  selector: 'app-spb-betreiber-allgemein',
  templateUrl: './spb-betreiber-allgemein.component.html',
  styleUrls: ['./spb-betreiber-allgemein.component.scss'],
})
export class SpbBetreiberAllgemeinComponent implements AfterViewInit {
  @Input()
  form: FormGroup<SpbBetreiberAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  static toForm(betreiberDto: SpbBetreiberDto): SpbBetreiberAllgemeinFormData {
    return {
      name: betreiberDto?.name,
    };
  }

  static createForm(
    fb: FormBuilder,
    betreiber: SpbBetreiberAllgemeinFormData
  ): FormGroup<SpbBetreiberAllgemeinFormData> {
    return fb.group<SpbBetreiberAllgemeinFormData>({
      name: betreiber.name,
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }
}
