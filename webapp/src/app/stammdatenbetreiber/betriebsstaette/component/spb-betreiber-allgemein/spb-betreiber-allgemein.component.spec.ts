import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbBetreiberAllgemeinComponent } from './spb-betreiber-allgemein.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder } from '@/base/forms';

describe('SpbBetreiberAllgemeinComponent', () => {
  let component: SpbBetreiberAllgemeinComponent;
  let fixture: ComponentFixture<SpbBetreiberAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SpbBetreiberAllgemeinComponent],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbBetreiberAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = SpbBetreiberAllgemeinComponent.createForm(
      new FormBuilder(),
      { name: 'Betreiber' }
    );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
