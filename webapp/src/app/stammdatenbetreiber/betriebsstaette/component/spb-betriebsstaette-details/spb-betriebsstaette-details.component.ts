import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { FormUtil } from '@/base/forms/form-util';
import {
  AnsprechpartnerComponent,
  AnsprechpartnerFormData,
} from '@/stammdaten/betriebsstaette/component/ansprechpartner/ansprechpartner.component';
import {
  SpbBetriebsstaetteAllgemeinComponent,
  SpbBetriebsstaetteAllgemeinFormData,
} from '../spb-betriebsstaette-allgemein/spb-betriebsstaette-allgemein.component';
import {
  SpbAdresseComponent,
  SpbAdresseFormData,
} from '../spb-adresse/spb-adresse.component';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  KommunikationComponent,
  KommunikationsverbindungFormData,
} from '@/basedomain/component/kommunikation/kommunikation.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { LandProperties, lookupLand } from '@/base/model/Land';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import {
  SpbBetriebsstaetteDto,
  SpbBetriebsstaettenRestControllerService,
} from '@api/stammdatenbetreiber';
import {
  buildingIcon,
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface SpbBetriebsstaetteFormData {
  allgemein: SpbBetriebsstaetteAllgemeinFormData;
  adresse: SpbAdresseFormData;
  kommunikation: Array<KommunikationsverbindungFormData>;
  ansprechpartner: Array<AnsprechpartnerFormData>;
}

ClarityIcons.addIcons(buildingIcon, timesIcon, floppyIcon, errorStandardIcon);

@Component({
  selector: 'app-spb-betriebsstaette-details',
  templateUrl: './spb-betriebsstaette-details.component.html',
  styleUrls: ['./spb-betriebsstaette-details.component.scss'],
})
export class SpbBetriebsstaetteDetailsComponent
  implements OnInit, UnsavedChanges
{
  @ViewChild(SpbBetriebsstaetteAllgemeinComponent)
  allgemeinComponent: SpbBetriebsstaetteAllgemeinComponent;

  @ViewChild(SpbAdresseComponent)
  adresseComponent: SpbAdresseComponent;

  betriebsstaette: SpbBetriebsstaetteDto;
  betriebsstaetteForm: FormGroup<SpbBetriebsstaetteFormData>;

  saveAttempt = false;
  openSaveWarning = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  flusseinzugsgebiete: Referenzliste<Required<ReferenzDto>>;
  naceCodes: Referenzliste<Required<ReferenzDto>>;
  isoCodes: Referenzliste<Required<ReferenzDto>>;
  kommunikationsTypen: Referenzliste<Required<ReferenzDto>>;
  funktionen: Referenzliste<Required<ReferenzDto>>;
  epsgCodes: Referenzliste<Required<ReferenzDto>>;

  private mapToForm(
    betriebsstaetteDto: SpbBetriebsstaetteDto
  ): SpbBetriebsstaetteFormData {
    return {
      allgemein: SpbBetriebsstaetteAllgemeinComponent.toForm(
        betriebsstaetteDto,
        this.flusseinzugsgebiete
      ),
      adresse: SpbAdresseComponent.toForm(
        betriebsstaetteDto.adresse,
        this.isoCodes
      ),
      kommunikation: KommunikationComponent.toForm(
        betriebsstaetteDto.kommunikationsverbindungen,
        this.kommunikationsTypen
      ),
      ansprechpartner: AnsprechpartnerComponent.toForm(
        betriebsstaetteDto.ansprechpartner,
        this.kommunikationsTypen
      ),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly betriebsstaettenService: SpbBetriebsstaettenRestControllerService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaette = data.betriebsstaette;
      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.naceCodes = new Referenzliste(data.naceCodeListe);
      this.flusseinzugsgebiete = new Referenzliste(
        data.flusseinzugsgebietListe
      );
      this.isoCodes = new Referenzliste(data.landIsoCodeListe);
      this.kommunikationsTypen = new Referenzliste(
        data.kommunikationsTypenListe
      );
      this.epsgCodes = new Referenzliste(data.epsgCodeListe);
      this.funktionen = new Referenzliste(data.funktionenListe);
      this.reset();
    });
    this.betriebsstaetteForm = this.createBetriebsstaetteForm(
      this.mapToForm(this.betriebsstaette)
    );
    this.checkSchreibsperreBetreiber(this.betriebsstaette);
  }

  formToBetriebsstaette(): SpbBetriebsstaetteDto {
    // RawValue gibt auch die disabled Values mit (in diesem Fall für "adresse.ort" benötigt)
    const formData = this.betriebsstaetteForm.getRawValue();
    return {
      ...this.betriebsstaette,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      flusseinzugsgebiet: formData.allgemein.flusseinzugsgebiet,
      naceCode: formData.allgemein.naceCode || null,
      abfallerzeugerNummern: formData.allgemein.abfallerzeugerNummern,
      einleiterNummern: formData.allgemein.einleiterNummern,
      adresse: FormUtil.emptyStringsToNull(formData.adresse),
      geoPunkt: formData.allgemein.geoPunkt.epsgCode
        ? formData.allgemein.geoPunkt
        : null,
      zustaendigeBehoerde: formData.allgemein.zustaendigeBehoerde,
      kommunikationsverbindungen: formData.kommunikation,
      ansprechpartner: formData.ansprechpartner,
    };
  }

  save(): void {
    this.saveAttempt = true;
    this.openSaveWarning = false;
    if (this.betriebsstaetteForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.adresseComponent?.markAllAsTouched();
      return;
    }

    const betriebsstaetteDto: SpbBetriebsstaetteDto =
      this.formToBetriebsstaette();

    if (this.isNeu) {
      this.createBetriebsstaette(betriebsstaetteDto);
    } else {
      this.updateBetriebsstaette(betriebsstaetteDto);
    }
  }

  updateBetriebsstaette(betriebsstaetteDto: SpbBetriebsstaetteDto): void {
    this.betriebsstaettenService
      .updateSpbBetriebsstaette(betriebsstaetteDto)
      .subscribe(
        (betriebsstaette) => {
          this.betriebsstaette = betriebsstaette;
          this.reset();
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  createBetriebsstaette(betriebsstaetteDto: SpbBetriebsstaetteDto): void {
    this.betriebsstaettenService
      .createSpbBetriebsstaette(betriebsstaetteDto)
      .subscribe(
        (betriebsstaette) => {
          this.betriebsstaette = betriebsstaette;
          this.reset();
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  saveAbbrechen(): void {
    this.openSaveWarning = false;
  }

  reset(): void {
    if (this.betriebsstaetteForm) {
      this.saveAttempt = false;
      const originalBetriebsstaette = this.mapToForm(this.betriebsstaette);
      this.betriebsstaetteForm.reset(originalBetriebsstaette);
      this.betriebsstaetteForm.setControl(
        'adresse',
        SpbAdresseComponent.createForm(this.fb, originalBetriebsstaette.adresse)
      );
      this.betriebsstaetteForm.setControl(
        'kommunikation',
        KommunikationComponent.createForm(
          this.fb,
          originalBetriebsstaette.kommunikation,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.betriebsstaetteForm.setControl(
        'ansprechpartner',
        AnsprechpartnerComponent.createForm(
          this.fb,
          originalBetriebsstaette.ansprechpartner,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );

      this.allgemeinForm.get('einleiterNummern').markAsPristine();
      this.allgemeinForm.get('abfallerzeugerNummern').markAsPristine();

      this.checkSchreibsperreBetreiber(this.betriebsstaette);
    }
  }

  checkSchreibsperreBetreiber(betriebsstaette: SpbBetriebsstaetteDto): void {
    if (betriebsstaette.schreibsperreBetreiber) {
      this.betriebsstaetteForm.disable();
    } else {
      this.betriebsstaetteForm.enable();
    }
  }

  get isNeu(): boolean {
    return this.betriebsstaette.id == null;
  }

  get allgemeinForm(): FormGroup<SpbBetriebsstaetteAllgemeinFormData> {
    return this.betriebsstaetteForm.get(
      'allgemein'
    ) as FormGroup<SpbBetriebsstaetteAllgemeinFormData>;
  }

  get adresseForm(): FormGroup<SpbAdresseFormData> {
    return this.betriebsstaetteForm.get(
      'adresse'
    ) as FormGroup<SpbAdresseFormData>;
  }

  get kommunikationForm(): FormArray<KommunikationsverbindungFormData> {
    return this.betriebsstaetteForm.get(
      'kommunikation'
    ) as FormArray<KommunikationsverbindungFormData>;
  }

  get ansprechpartnerForm(): FormArray<AnsprechpartnerFormData> {
    return this.betriebsstaetteForm.get(
      'ansprechpartner'
    ) as FormArray<AnsprechpartnerFormData>;
  }

  private createBetriebsstaetteForm(
    initialBetriebsstaette: SpbBetriebsstaetteFormData
  ): FormGroup<SpbBetriebsstaetteFormData> {
    return this.fb.group<SpbBetriebsstaetteFormData>({
      allgemein: SpbBetriebsstaetteAllgemeinComponent.createForm(
        this.fb,
        initialBetriebsstaette.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      adresse: SpbAdresseComponent.createForm(
        this.fb,
        initialBetriebsstaette.adresse
      ),
      kommunikation: KommunikationComponent.createForm(
        this.fb,
        initialBetriebsstaette.kommunikation,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      ansprechpartner: AnsprechpartnerComponent.createForm(
        this.fb,
        initialBetriebsstaette.ansprechpartner,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  get displayLand(): string {
    return LandProperties[lookupLand(this.betriebsstaette.land)].display;
  }

  canDeactivate(): boolean {
    return this.betriebsstaetteForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
