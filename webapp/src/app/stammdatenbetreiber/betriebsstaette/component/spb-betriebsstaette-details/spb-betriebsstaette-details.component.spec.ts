import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockService,
} from 'ng-mocks';
import { of } from 'rxjs';
import { SpbBetriebsstaetteDetailsComponent } from './spb-betriebsstaette-details.component';
import { SpbBetriebsstaetteAllgemeinComponent } from '../spb-betriebsstaette-allgemein/spb-betriebsstaette-allgemein.component';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';
import { HttpClientModule } from '@angular/common/http';
import { SpbAdresseComponent } from '../spb-adresse/spb-adresse.component';
import { AnsprechpartnerComponent } from '@/stammdaten/betriebsstaette/component/ansprechpartner/ansprechpartner.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  BetriebsstaettenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { SpbBetriebsstaetteDto } from '@api/stammdatenbetreiber';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';

describe('BetriebsstaetteDetailsComponent', () => {
  let component: SpbBetriebsstaetteDetailsComponent;
  let fixture: ComponentFixture<SpbBetriebsstaetteDetailsComponent>;

  const betriebsstatusListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vorschriftenListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const flusseinzugsgebietListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const naceCodeListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const zustaendigkeitenListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const gemeindekennzifferListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01', ktext: 'Hamburg' },
    { id: 2, schluessel: '02', ktext: 'Lüneburg' },
    { id: 3, schluessel: '03', ktext: 'Celle' },
  ] as Array<ReferenzDto>;

  const betriebsstaette: SpbBetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    name: '',
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '01',
    zustaendigeBehoerde: '11',
    abfallerzeugerNummern: [],
    einleiterNummern: [],
  };

  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(BasedomainModule),
        MockModule(BaseModule),
        RouterTestingModule,
        HttpClientModule,
      ],
      declarations: [
        SpbBetriebsstaetteDetailsComponent,
        SpbBetriebsstaetteAllgemeinComponent,
        MockComponents(SpbAdresseComponent, AnsprechpartnerComponent),
      ],
      providers: [
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: { nummer: 'nummer' },
              data: { schreibRecht: true },
            },
            data: of({
              betriebsstaette,
              betriebsstatusListe,
              gemeindekennzifferListe,
              vorschriftenListe,
              flusseinzugsgebietListe,
              naceCodeListe,
              zustaendigkeitenListe,
              landIsoCodeListe: [],
              kommunikationsTypenListe: [],
              epsgCodeListe: [],
              funktionenListe: [],
            }),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SpbBetriebsstaetteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.betriebsstaetteForm.patchValue({
        allgemein: {
          name: 'Meine neue Betriebsstaette',
          betriebsstaetteNr: 'BST1234',
          betriebsstatus: betriebsstatusListe[0],
          zustaendigeBehoerde: '01',
          akz: 'akz',
          einleiterNummern: ['nr1', 'nr2'],
          abfallerzeugerNummern: ['abfall', 'erzeuger', 'nr'],
          naceCode: naceCodeListe[0],
          inbetriebnahme: '12.12.2009',
          flusseinzugsgebiet: flusseinzugsgebietListe[0],
          indirekteinleiter: true,
          direkteinleiter: false,
          betriebsstatusSeit: '12.12.2009',
        },
        adresse: {
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
        },
      });
      component.betriebsstaetteForm.markAsDirty();
      component.betriebsstaetteForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.betriebsstaetteForm.valid).toBe(true);
    });
  });
});
