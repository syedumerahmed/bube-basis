import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import {
  SpbBetreiberDto,
  SpbBetreiberRestControllerService,
} from '@api/stammdatenbetreiber';

@Injectable()
export class SpbBetreiberResolver implements Resolve<SpbBetreiberDto> {
  constructor(private service: SpbBetreiberRestControllerService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbBetreiberDto> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const nummer = route.params.nummer;

    return this.service.readSpbBetreiberByJahrLandNummer(
      berichtsjahr,
      landSchluessel,
      nummer
    );
  }
}
