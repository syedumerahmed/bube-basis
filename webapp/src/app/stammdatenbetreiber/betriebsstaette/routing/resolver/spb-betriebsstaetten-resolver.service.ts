import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import {
  SpbBetriebsstaetteDto,
  SpbBetriebsstaettenRestControllerService,
} from '@api/stammdatenbetreiber';

@Injectable()
export class SpbBetriebsstaettenResolver
  implements Resolve<SpbBetriebsstaetteDto>
{
  constructor(
    private readonly service: SpbBetriebsstaettenRestControllerService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<SpbBetriebsstaetteDto> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const nummer = route.params.nummer;

    const betriebsstaette = this.service.readSpbBetriebsstaetteByJahrLandNummer(
      berichtsjahr,
      landSchluessel,
      nummer
    );

    return betriebsstaette.pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router
            .navigate(['stammdaten', 'jahr', berichtsjahr, '404'])
            .then(() => this.location.replaceState(state.url));
        }
        this.alertService.setAlert(err);
        return EMPTY;
      })
    );
  }
}
