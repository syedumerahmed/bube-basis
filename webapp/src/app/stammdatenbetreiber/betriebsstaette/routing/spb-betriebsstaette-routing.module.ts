import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpbBetriebsstaettenListeComponent } from 'src/app/stammdatenbetreiber/betriebsstaette/component/spb-betriebsstaetten-liste/spb-betriebsstaetten-liste.component';
import { SpbBetriebsstaetteComponent } from 'src/app/stammdatenbetreiber/betriebsstaette/component/spb-betriebsstaette/spb-betriebsstaette.component';
import { SpbBetriebsstaettenResolver } from 'src/app/stammdatenbetreiber/betriebsstaette/routing/resolver/spb-betriebsstaetten-resolver.service';
import { BetriebsstatusListResolver } from '@/referenzdaten/routing/resolver/betriebsstatus-list-resolver';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { FlusseinzugsgebietListResolver } from '@/referenzdaten/routing/resolver/flusseinzugsgebiet-list-resolver';
import { NaceCodeListResolver } from '@/referenzdaten/routing/resolver/nace-code-list-resolver';
import { KommunikationsTypenListResolver } from '@/referenzdaten/routing/resolver/kommunikations-typen-list-resolver';
import { LandisocodeListResolver } from '@/referenzdaten/routing/resolver/landisocode-list-resolver';
import { EpsgListResolver } from '@/referenzdaten/routing/resolver/epsg-list-resolver';
import { FunktionListResolver } from '@/referenzdaten/routing/resolver/funktion-list-resolver';
import { SpbBetriebsstaetteMainComponent } from 'src/app/stammdatenbetreiber/betriebsstaette/component/spb-betriebsstaette-main/spb-betriebsstaette-main.component';
import { SpbAnlageModule } from '@/stammdatenbetreiber/anlage/spb-anlage.module';
import { SpbBetreiberResolver } from '@/stammdatenbetreiber/betriebsstaette/routing/resolver/spb-betreiber-resolver';
import { SpbQuellenModule } from '@/stammdatenbetreiber/quellen/spb-quellen-module';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const betriebsstaetteReferenzenResolvers = {
  betriebsstatusListe: BetriebsstatusListResolver,
  behoerdenListe: BehoerdenListResolver,
  flusseinzugsgebietListe: FlusseinzugsgebietListResolver,
  naceCodeListe: NaceCodeListResolver,
  kommunikationsTypenListe: KommunikationsTypenListResolver,
  landIsoCodeListe: LandisocodeListResolver,
  epsgCodeListe: EpsgListResolver,
  funktionenListe: FunktionListResolver,
};

const routes: Routes = [
  {
    path: '',
    component: SpbBetriebsstaettenListeComponent,
  },
  {
    path: 'betriebsstaette/land/:land/nummer/:nummer',
    component: SpbBetriebsstaetteComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      betriebsstaette: SpbBetriebsstaettenResolver,
      betreiber: SpbBetreiberResolver,
      ...betriebsstaetteReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: SpbBetriebsstaetteMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
      {
        path: 'anlage',
        loadChildren: () => SpbAnlageModule,
      },
      {
        path: 'quelle',
        loadChildren: () => SpbQuellenModule,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [SpbBetriebsstaettenResolver, SpbBetreiberResolver],
})
export class SpbBetriebsstaetteRoutingModule {}
