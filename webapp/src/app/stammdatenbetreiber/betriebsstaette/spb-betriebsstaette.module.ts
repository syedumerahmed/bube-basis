import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';

import { SpbBetriebsstaetteRoutingModule } from 'src/app/stammdatenbetreiber/betriebsstaette/routing/spb-betriebsstaette-routing.module';
import { SpbBetriebsstaettenListeComponent } from './component/spb-betriebsstaetten-liste/spb-betriebsstaetten-liste.component';
import { BaseModule } from '@/base/base.module';
import { SpbBetriebsstaetteComponent } from './component/spb-betriebsstaette/spb-betriebsstaette.component';
import { SpbBetriebsstaetteMainComponent } from './component/spb-betriebsstaette-main/spb-betriebsstaette-main.component';
import { SpbBetriebsstaetteDetailsComponent } from './component/spb-betriebsstaette-details/spb-betriebsstaette-details.component';
import { SpbBetriebsstaetteAllgemeinComponent } from './component/spb-betriebsstaette-allgemein/spb-betriebsstaette-allgemein.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { SpbAdresseComponent } from './component/spb-adresse/spb-adresse.component';
import { BetriebsstaetteModule } from '@/stammdaten/betriebsstaette/betriebsstaette.module';
import { SpbAnlageModule } from '@/stammdatenbetreiber/anlage/spb-anlage.module';
import { SpbBetreiberDetailsComponent } from './component/spb-betreiber-details/spb-betreiber-details.component';
import { SpbBetreiberAllgemeinComponent } from './component/spb-betreiber-allgemein/spb-betreiber-allgemein.component';
import { SpbQuellenModule } from '@/stammdatenbetreiber/quellen/spb-quellen-module';

@NgModule({
  declarations: [
    SpbBetriebsstaettenListeComponent,
    SpbBetriebsstaetteComponent,
    SpbBetriebsstaetteMainComponent,
    SpbBetriebsstaetteDetailsComponent,
    SpbBetriebsstaetteAllgemeinComponent,
    SpbAdresseComponent,
    SpbBetreiberDetailsComponent,
    SpbBetreiberAllgemeinComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,

    BasedomainModule,

    SpbBetriebsstaetteRoutingModule,

    BaseModule,
    BetriebsstaetteModule,
    SpbAnlageModule,
    SpbQuellenModule,
  ],
})
export class SpbBetriebsstaetteModule {}
