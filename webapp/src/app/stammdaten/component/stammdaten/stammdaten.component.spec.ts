import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StammdatenComponent } from '@/stammdaten/component/stammdaten/stammdaten.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule } from 'ng-mocks';
import { BaseModule } from '@/base/base.module';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { StammdatenModule } from '@/stammdaten/stammdaten.module';

describe('StammdatenComponent', () => {
  let component: StammdatenComponent;
  let fixture: ComponentFixture<StammdatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StammdatenComponent],
      imports: [
        RouterTestingModule,
        MockModule(BaseModule),
        MockModule(BasedomainModule),
        MockModule(StammdatenModule),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StammdatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
