import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { DesktopService } from '@/desktop/service/desktop.service';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  DesktopRestControllerService,
  TreeNodeDto,
  TreeNodeTagsEnum,
  TreeNodeTypEnum,
} from '@api/desktop';
import {
  buildingIcon,
  ClarityIcons,
  cloudIcon,
  factoryIcon,
  importIcon,
  minusCircleIcon,
  plusCircleIcon,
  refreshIcon,
  timesCircleIcon,
  timesIcon,
} from '@cds/core/icon';
import { Router } from '@angular/router';

ClarityIcons.addIcons(
  refreshIcon,
  plusCircleIcon,
  minusCircleIcon,
  timesCircleIcon,
  timesIcon,
  buildingIcon,
  factoryIcon,
  cloudIcon,
  importIcon
);

@Component({
  selector: 'app-navi-baum',
  templateUrl: './navi-baum.component.html',
  styleUrls: ['./navi-baum.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NaviBaumComponent implements OnInit, OnDestroy {
  private static readonly icons: Record<TreeNodeTypEnum, string> = {
    BETRIEBSSTAETTE: 'building',
    ANLAGE: 'factory',
    QUELLE: 'cloud',

    EUREG_BST: 'building',
    EUREG_ANL: 'factory',
    EUREG_F: 'factory',
  };

  private static readonly typeNames: Partial<Record<TreeNodeTypEnum, string>> =
    {
      EUREG_BST: 'EuRegBST',
      EUREG_ANL: 'EuRegAnl',
      EUREG_F: 'EuRegF',
    };

  loading$ = new BehaviorSubject(true);

  root: TreeNodeDto[] = [];
  private readonly destroyed$ = new Subject();
  @Input()
  typ: 'STAMMDATEN' | 'EUREG' | 'SPB_STAMMDATEN';

  getChildren = (node: TreeNodeDto): TreeNodeDto[] => node.children;

  constructor(
    private readonly router: Router,
    private readonly desktopRestService: DesktopRestControllerService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly desktopService: DesktopService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.berichtsjahrService.selectedBerichtsjahr$,
      this.desktopService.treeNodesChanged$.pipe(startWith(true)),
    ])
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.readDesktop());
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  setDesktop(newRoot: TreeNodeDto[]): void {
    this.root = newRoot;
    this.loading$.next(false);
  }

  readDesktop(): void {
    this.loading$.next(true);
    this.desktopRestService
      .readDesktop(
        this.typ,
        this.berichtsjahrService.selectedBerichtsjahr.schluessel
      )
      .subscribe(
        (root) => this.setDesktop(root),
        (err) => this.alertService.setAlert(err)
      );
  }

  deleteDesktop(): void {
    this.desktopRestService
      .deleteDesktop(
        this.typ,
        this.berichtsjahrService.selectedBerichtsjahr.schluessel
      )
      .subscribe(
        () => this.setDesktop([]),
        (err) => this.alertService.setAlert(err.rror)
      );
  }

  deleteDesktopItem(betriebssteatteId: number): void {
    this.loading$.next(true);
    this.desktopRestService
      .deleteDesktopItem(this.typ, betriebssteatteId)
      .subscribe(
        () => this.readDesktop(),
        (err) => this.alertService.setAlert(err)
      );
  }

  alleEinklappen(): void {
    this.root.forEach((node) => {
      node.aufgeklappt = false;
      node.children?.forEach((child) => (child.aufgeklappt = false));
    });
  }

  alleAufklappen(): void {
    this.root.forEach((node) => {
      node.aufgeklappt = true;
      node.children?.forEach((child) => (child.aufgeklappt = true));
    });
  }

  isBetriebsstaette(node: TreeNodeDto): boolean {
    return [
      TreeNodeTypEnum.BETRIEBSSTAETTE,
      TreeNodeTypEnum.EUREG_BST,
    ].includes(node.typ);
  }

  getIcon(node: TreeNodeDto): string {
    return NaviBaumComponent.icons[node.typ] ?? 'unknown-status';
  }

  getNodeName(node: TreeNodeDto): string {
    const typeName = NaviBaumComponent.typeNames[node.typ];
    return typeName ? `${typeName}: ${node.name}` : node.name;
  }

  hasNoTag(node: TreeNodeDto): boolean {
    return node.tags == undefined || node.tags.length == 0;
  }

  hasTagAenderungDurchBetreiber(node: TreeNodeDto): boolean {
    return node.tags?.includes(TreeNodeTagsEnum.AenderungDurchBetreiber);
  }

  hasTagNeuDurchBetreiber(node: TreeNodeDto): boolean {
    return node.tags?.includes(TreeNodeTagsEnum.NeuDurchBetreiber);
  }
}
