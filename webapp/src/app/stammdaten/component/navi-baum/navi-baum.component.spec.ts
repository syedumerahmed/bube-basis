import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NaviBaumComponent } from './navi-baum.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { BehaviorSubject, of } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import { DesktopRestControllerService } from '@api/desktop';

describe('NaviBaumComponent', () => {
  let component: NaviBaumComponent;
  let fixture: ComponentFixture<NaviBaumComponent>;
  let selectedBerichtsjahr$: BehaviorSubject<ReferenzDto>;

  const currentBerichtsjahr: ReferenzDto = {
    id: 2020,
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '',
  };

  beforeEach(async () => {
    selectedBerichtsjahr$ = new BehaviorSubject(currentBerichtsjahr);
    await TestBed.configureTestingModule({
      declarations: [NaviBaumComponent],
      providers: [
        {
          provide: DesktopRestControllerService,
          useValue: MockService(DesktopRestControllerService),
        },
        {
          provide: BerichtsjahrService,
          useValue: MockService(BerichtsjahrService),
        },
      ],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'berichtsjahrEnabled$', 'get')
      .mockReturnValue(of(true));
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr$', 'get')
      .mockReturnValue(selectedBerichtsjahr$);

    fixture = TestBed.createComponent(NaviBaumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be empty', () => {
    expect(component.root).toHaveLength(0);
  });
});
