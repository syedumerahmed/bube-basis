import { Component, OnDestroy, OnInit } from '@angular/core';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { buildingIcon, ClarityIcons } from '@cds/core/icon';

ClarityIcons.addIcons(buildingIcon);
@Component({
  selector: 'app-entity-not-found',
  templateUrl: './entity-not-found.component.html',
  styleUrls: ['./entity-not-found.component.scss'],
})
export class EntityNotFoundComponent implements OnInit, OnDestroy {
  constructor(private readonly berichtsjahrService: BerichtsjahrService) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }
}
