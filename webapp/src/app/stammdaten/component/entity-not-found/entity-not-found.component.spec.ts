import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityNotFoundComponent } from './entity-not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { MockModule, MockService } from 'ng-mocks';

describe('EntityNotFoundComponent', () => {
  let component: EntityNotFoundComponent;
  let fixture: ComponentFixture<EntityNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [EntityNotFoundComponent],
      providers: [
        {
          provide: BerichtsjahrService,
          useValue: MockService(BerichtsjahrService),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EntityNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
