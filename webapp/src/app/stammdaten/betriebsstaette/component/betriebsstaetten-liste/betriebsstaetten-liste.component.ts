import { Component, OnDestroy, OnInit } from '@angular/core';
import { ClrDatagridSortOrder, ClrDatagridStateInterface } from '@clr/angular';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobStatusService } from '@/base/service/job-status.service';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { startWith, takeUntil, tap } from 'rxjs/operators';
import { DesktopService } from '@/desktop/service/desktop.service';
import { Event, EventBusService } from '@/base/service/event-bus.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import * as fileSaver from 'file-saver';
import {
  AdminRestControllerService,
  BetriebsstaetteListItemDto,
  BetriebsstaettenRestControllerService,
  DownloadKomplexpruefungCommand,
  KomplexpruefungDurchfuehrenCommand,
  PageDtoBetriebsstaetteListItemDto,
  ReferenzDto,
  SortStateDtoBetriebsstaetteColumnByEnum,
  Suchattribute,
  UebernimmBetriebsstaettenInJahr,
} from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { StammdatenImportAdapterService } from '@/base/service/stammdaten-import-adapter.service';
import { LandProperties } from '@/base/model/Land';
import { Rolle } from '@/base/security/model/Rolle';
import {
  angleIcon,
  buildingIcon,
  checkIcon,
  ClarityIcons,
  eyeIcon,
  listIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  treeViewIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(
  trashIcon,
  buildingIcon,
  checkIcon,
  timesIcon,
  treeViewIcon,
  angleIcon,
  plusIcon,
  eyeIcon,
  listIcon
);
@Component({
  selector: 'app-betriebsstaetten-liste',
  templateUrl: './betriebsstaetten-liste.component.html',
  styleUrls: ['./betriebsstaetten-liste.component.scss'],
})
export class BetriebsstaettenListeComponent implements OnInit, OnDestroy {
  readonly betriebsstaettenColumns = SortStateDtoBetriebsstaetteColumnByEnum;
  readonly SortOrder = ClrDatagridSortOrder;
  betriebsstaettenPage: PageDtoBetriebsstaetteListItemDto;
  currentPage: number;
  loading = true;

  private datagridState$: Subject<ClrDatagridStateInterface> = new Subject();
  private updateBetriebsstaettenListe$: BehaviorSubject<void> =
    new BehaviorSubject(null);
  private searchattributesChanged$: BehaviorSubject<Suchattribute> =
    new BehaviorSubject(null);
  loeschenDialogOpen: boolean;
  alleAnsprechpersonenloeschenDialogOpen: boolean;
  public uebernehmenDialogOpen: boolean;
  stammdatenImportModalOpen = false;
  stammdatenExportModalOpen = false;
  isExporting = false;
  berichtsjahrListe: Array<ReferenzDto>;

  betriebsstaetteId: number;
  selectedIds: Array<number> = [];
  currentBerichtsjahr$: BehaviorSubject<ReferenzDto> = new BehaviorSubject(
    null
  );
  loescht = false;

  private readonly destroyed$ = new Subject();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly betriebsstaettenRestControllerService: BetriebsstaettenRestControllerService,
    private readonly alertService: AlertService,
    private readonly adminService: AdminRestControllerService,
    private readonly jobStatusService: JobStatusService,
    private readonly desktopService: DesktopService,
    private readonly eventbus: EventBusService,
    private readonly berichtsjahrService: BerichtsjahrService,
    readonly stammdatenImportAdapter: StammdatenImportAdapterService,
    private readonly dateFormat: FilenameDateFormatPipe
  ) {}

  ngOnInit(): void {
    this.berichtsjahrListe = this.route.snapshot.parent.data.berichtsjahre;
    this.berichtsjahrService.resetFilter();
    this.berichtsjahrService.selectedBerichtsjahr$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(this.currentBerichtsjahr$);

    this.currentBerichtsjahr$.subscribe(() => {
      this.markierungAufheben();
      this.currentPage = 1;
    });

    combineLatest([
      this.currentBerichtsjahr$,
      this.datagridState$,
      this.searchattributesChanged$.pipe(tap(() => this.markierungAufheben())),
      this.updateBetriebsstaettenListe$,
      this.eventbus
        .on(Event.BetriebsstaettenImportFertig, Event.KomplexpruefungFertig)
        .pipe(startWith(true)), // Wir benötigen zum Start einen dummy Wert (hier true)
    ])
      .pipe(takeUntil(this.destroyed$))
      .subscribe(([berichtsjahr, dgState, searchParams]) => {
        this.refresh(berichtsjahr, dgState, searchParams);
      });

    this.eventbus
      .on(
        Event.BetriebsstaettenImportFertig,
        Event.BetriebsstaettenExportFertig,
        Event.KomplexpruefungFertig
      )
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.markierungAufheben());
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  onDatagridStateChanged(state: ClrDatagridStateInterface): void {
    this.datagridState$.next(state);
  }

  refresh(
    berichtsjahr: ReferenzDto,
    { page, sort: gridSort }: ClrDatagridStateInterface,
    suche: Suchattribute
  ): void {
    this.loading = true;
    this.berichtsjahrService.resetFilter();
    this.betriebsstaettenRestControllerService
      .readAllBetriebsstaette(berichtsjahr.id, {
        suche,
        page: {
          size: page?.size,
          current: page ? page.current - 1 : 0,
        },
        sort: gridSort
          ? {
              by: gridSort.by as SortStateDtoBetriebsstaetteColumnByEnum,
              reverse: gridSort.reverse,
            }
          : null,
      })
      .subscribe(
        (response) => {
          this.betriebsstaettenPage = response;
          this.loading = false;
        },
        (err: HttpErrorResponse) => {
          this.alertService.setAlert(err.message);
          this.loading = false;
        }
      );
  }

  public deleteBetriebsstaette(): void {
    this.betriebsstaettenRestControllerService
      .deleteBetriebsstaette(this.betriebsstaetteId)
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          const deletedIndex = this.selectedIds.indexOf(this.betriebsstaetteId);
          if (deletedIndex !== -1) {
            this.selectedIds.splice(deletedIndex, 1);
          }
          this.updateBetriebsstaettenListe$.next();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  deleteBetriebsstaetten(): void {
    this.loescht = true;
    this.betriebsstaettenRestControllerService
      .deleteMultipleBetriebsstaetten(this.selectedIds)
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.selectedIds = [];
          this.updateBetriebsstaettenListe$.next();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  canDelete(betriebsstaette: BetriebsstaetteListItemDto = null): boolean {
    return (
      this.benutzerProfil.canDeleteStammdaten &&
      (!betriebsstaette || betriebsstaette.canDelete)
    );
  }

  get canCreate(): boolean {
    return (
      this.benutzerProfil.canWriteStammdaten &&
      !this.benutzerProfil.isGesamtadmin
    );
  }

  get isGesamtadmin(): boolean {
    return this.benutzerProfil.isGesamtadmin;
  }

  get isLandesadmin(): boolean {
    return this.benutzerProfil.isLandesadmin;
  }

  get benutzerLand(): string {
    return this.benutzerProfil.landNr;
  }

  get zustaendigeLaender(): string {
    if (this.benutzerProfil.isGesamtadmin) {
      return 'alle Länder';
    }
    return LandProperties[this.benutzerProfil.land].display;
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.betriebsstaetteId = null;
    this.loeschenDialogOpen = false;
  }

  openLoeschenDialog(betriebsstaetteId: number): void {
    this.betriebsstaetteId = betriebsstaetteId;
    this.loeschenDialogOpen = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialogOpen = true;
  }

  closeAlleAnsprechpersonenLoeschenDialog(): void {
    this.alleAnsprechpersonenloeschenDialogOpen = false;
  }

  openAlleAnsprechpersonenLoeschenDialog(): void {
    this.alleAnsprechpersonenloeschenDialogOpen = true;
  }

  deleteAlleAnsprechpersonen(): void {
    this.adminService
      .deleteAllAnsprechpartner(this.currentBerichtsjahr$.value.schluessel)
      .subscribe(
        () => {
          this.closeAlleAnsprechpersonenLoeschenDialog();
          this.jobStatusService.jobStarted();
          this.markierungAufheben();
        },
        () => {
          this.closeAlleAnsprechpersonenLoeschenDialog();
          this.alertService.setAlert(
            'Fehler beim Löschen der Ansprechpersonen'
          );
        }
      );
  }

  alleMarkieren(): void {
    const berichtsjahrId: number = this.currentBerichtsjahr$.value.id;

    this.betriebsstaettenRestControllerService
      .readAllBetriebsstaetteIds(
        berichtsjahrId,
        this.searchattributesChanged$.value
      )
      .subscribe(
        (idList) => (this.selectedIds = idList),
        (err: HttpErrorResponse) => this.alertService.setAlert(err.message)
      );
  }

  markierungAufheben(): void {
    this.selectedIds = [];
  }

  get canUseSearch(): boolean {
    return this.benutzerProfil.isBehoerdenbenutzer;
  }

  get nothingSelected(): boolean {
    return this.selectedIds.length === 0;
  }

  searchChanged(data: Suchattribute): void {
    this.searchattributesChanged$.next(data);
  }

  get canUebernehmen(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_WRITE);
  }

  get canExport(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_READ);
  }

  get canImport(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_DELETE);
  }

  inDesktopSpeichern(): void {
    this.desktopService.inDesktopSpeichern(
      'STAMMDATEN',
      this.selectedIds,
      this.currentBerichtsjahr$.value.schluessel
    );
    this.markierungAufheben();
  }

  inJahrUebernehmen(zieljahr: ReferenzDto): void {
    const uebernimmCommand: UebernimmBetriebsstaettenInJahr = {
      betriebsstaettenIds: this.selectedIds,
      zieljahr,
    };
    this.betriebsstaettenRestControllerService
      .betriebsstaettenUebernehmen(uebernimmCommand)
      .subscribe(
        () => {
          this.closeUebernehmenDialog();
          this.jobStatusService.jobStarted();
          this.markierungAufheben();
        },
        (err) => {
          this.closeUebernehmenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  openUebernehmenDialog(): void {
    this.uebernehmenDialogOpen = true;
  }

  closeUebernehmenDialog(): void {
    this.uebernehmenDialogOpen = false;
  }

  komplexpruefungAusfuehren(): void {
    const command: KomplexpruefungDurchfuehrenCommand = {
      ids: this.selectedIds,
      regelGruppe: 'GRUPPE_A',
    };
    this.betriebsstaettenRestControllerService
      .runKomplexpruefung(command)
      .subscribe(
        () => {
          this.jobStatusService.jobStarted();
          this.markierungAufheben();
        },
        (err) => {
          this.alertService.setAlert(err);
        }
      );
  }

  komplexpruefungDownload(bstId: number, bstNummer: string): void {
    const command: DownloadKomplexpruefungCommand = {
      bstId: bstId,
    };

    this.betriebsstaettenRestControllerService
      .komplexpruefungDownload(command)
      .subscribe(
        (blob) => {
          const datum = this.dateFormat.transform(new Date());
          fileSaver.saveAs(blob, 'Prüfung_' + bstNummer + '_' + datum + '.txt');
        },
        () => {
          this.alertService.setAlert('Fehler beim Download des Protokolls');
        }
      );
  }

  getPruefungsfehler(bst: BetriebsstaetteListItemDto): string {
    if (bst.pruefungVorhanden) {
      return bst.pruefungsfehler ? 'Ja' : 'Nein';
    }
    return '-';
  }

  navigate(betriebsstaette: BetriebsstaetteListItemDto): void {
    this.router.navigate(
      [
        'stammdaten',
        'jahr',
        this.berichtsjahrService.selectedBerichtsjahr.schluessel,
        'betriebsstaette',
        'land',
        betriebsstaette.land,
        'nummer',
        betriebsstaette.nummer,
      ],
      {
        queryParams: {
          localId: betriebsstaette.localId,
        },
      }
    );
  }
}
