import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetriebsstaettenListeComponent } from './betriebsstaetten-liste.component';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule, ClrDatagridStateInterface } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute } from '@angular/router';
import { SucheComponent } from '@/stammdaten/betriebsstaette/component/suche/suche.component';
import { DesktopService } from '@/desktop/service/desktop.service';
import { ReactiveFormsModule } from '@angular/forms';
import { StammdatenExportModalComponent } from '@/stammdaten/betriebsstaette/component/stammdaten-export-modal/stammdaten-export-modal.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import { DatenInAnderesJahrUebernehmenModalComponent } from '@/basedomain/component/daten-in-anderes-jahr-uebenrnehmen-modal/daten-in-anderes-jahr-uebernehmen-modal.component';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import {
  AdminRestControllerService,
  BetriebsstaettenRestControllerService,
  PageDtoBetriebsstaetteListItemDto,
  ReferenzDto,
  SortStateDtoBetriebsstaetteColumnByEnum,
  Suchattribute,
} from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('BetriebsstaettenListeComponent', () => {
  let component: BetriebsstaettenListeComponent;
  let fixture: ComponentFixture<BetriebsstaettenListeComponent>;
  let selectedBerichtsjahr$: BehaviorSubject<ReferenzDto>;
  let allBetriebsstaetten$: Subject<PageDtoBetriebsstaetteListItemDto>;

  beforeEach(async () => {
    selectedBerichtsjahr$ = new BehaviorSubject({ id: 2020 } as ReferenzDto);
    await TestBed.configureTestingModule({
      declarations: [
        BetriebsstaettenListeComponent,
        MockComponents(SucheComponent),
        MockComponents(StammdatenExportModalComponent),
        MockComponents(ImportModalComponent),
        MockComponents(DatenInAnderesJahrUebernehmenModalComponent),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProviders(
          BerichtsjahrService,
          BetriebsstaettenRestControllerService,
          AlertService,
          AdminRestControllerService,
          DesktopService,
          FilenameDateFormatPipe
        ),
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: { berichtsjahre: [{ id: 2020 }, { id: 2019 }] },
              parent: {
                data: { berichtsjahre: [{ id: 2020 }, { id: 2019 }] },
              },
            },
          },
        },
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
      ],
    }).compileComponents();
    const berichtsjahrMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrMock, 'selectedBerichtsjahr$', 'get')
      .mockReturnValue(selectedBerichtsjahr$);
  });

  beforeEach(inject(
    [BetriebsstaettenRestControllerService],
    (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
      allBetriebsstaetten$ = new Subject();
      spyOn(betriebsstaettenService, 'readAllBetriebsstaette').mockReturnValue(
        allBetriebsstaetten$.asObservable() as Observable<any>
      );
      spyOn(
        betriebsstaettenService,
        'readAllBetriebsstaetteIds'
      ).mockReturnValue(of([]) as Observable<any>);
      fixture = TestBed.createComponent(BetriebsstaettenListeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.onDatagridStateChanged({});
    }
  ));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('stammdaten in jahr übernehmen successful', () => {
    let sBetriebsstaettenService: BetriebsstaettenRestControllerService;

    beforeEach(inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService) => {
        sBetriebsstaettenService = betriebsstaettenService;
        spyOn(
          sBetriebsstaettenService,
          'betriebsstaettenUebernehmen'
        ).mockReturnValue(of(null));

        component.selectedIds = [1, 2, 3];
        component.openUebernehmenDialog();
        component.inJahrUebernehmen({ id: 2019 } as ReferenzDto);
      }
    ));

    it('should send correct command', () => {
      expect(
        sBetriebsstaettenService.betriebsstaettenUebernehmen
      ).toHaveBeenCalledWith({
        betriebsstaettenIds: [1, 2, 3],
        zieljahr: { id: 2019 },
      });
    });

    it('should close uebernehmen dialog', () => {
      expect(component.uebernehmenDialogOpen).toBe(false);
    });
  });

  describe('stammdaten in jahr uebernehmen unsuccessful', () => {
    beforeEach(inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService) => {
        spyOn(
          betriebsstaettenService,
          'betriebsstaettenUebernehmen'
        ).mockReturnValue(throwError(''));

        component.selectedIds = [1, 2, 3];
        component.openUebernehmenDialog();
        component.inJahrUebernehmen({ id: 2019 } as ReferenzDto);
      }
    ));

    it('should close uebernehmen dialog', () => {
      expect(component.uebernehmenDialogOpen).toBe(false);
    });

    it('should set alert', inject([AlertService], (alertService) => {
      expect(alertService.setAlert).toHaveBeenCalled();
    }));
  });

  describe('deleteBetriebsstaette successful', () => {
    beforeEach(inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService) => {
        spyOn(betriebsstaettenService, 'deleteBetriebsstaette').mockReturnValue(
          of(null)
        );
        spyOn(component, 'refresh').mockImplementation(() => {});
        component.openLoeschenDialog(1);
        component.deleteBetriebsstaette();
      }
    ));

    it('delete should use betriebsstaetteId field', inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService) => {
        expect(
          betriebsstaettenService.deleteBetriebsstaette
        ).toHaveBeenCalledWith(1);
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialogOpen).toBe(false);
    });

    it('should invoke refresh', () => {
      expect(component.refresh).toHaveBeenCalled();
    });
  });

  describe('deleteBeriebsstaette unsuccessful', () => {
    beforeEach(inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService) => {
        spyOn(betriebsstaettenService, 'deleteBetriebsstaette').mockReturnValue(
          throwError('')
        );
        spyOn(component, 'refresh').mockImplementation(() => {});
        component.openLoeschenDialog(1);
        component.deleteBetriebsstaette();
      }
    ));

    it('should close löschendialog', () => {
      expect(component.loeschenDialogOpen).toBe(false);
    });

    it('should set alert', inject(
      [AlertService],
      (alertService: AlertService) => {
        expect(alertService.setAlert).toHaveBeenCalled();
      }
    ));
  });

  it('closeLoeschenDialog should set loeschenDialog to false', () => {
    component.loeschenDialogOpen = false;
    component.closeLoeschenDialog();
    expect(component.loeschenDialogOpen).toBe(false);
  });

  it('openLoeschenDialog should set loeschenDialog to true', () => {
    component.loeschenDialogOpen = true;
    component.openLoeschenDialog(1);
    expect(component.loeschenDialogOpen).toBe(true);
  });

  describe('refresh on datagrid change', () => {
    const pageResponse: PageDtoBetriebsstaetteListItemDto = {
      totalElements: 0,
      content: [],
    };
    let state: ClrDatagridStateInterface;

    beforeEach(() => {
      state = {
        page: {
          from: 1,
          to: 3,
          size: 5,
          current: 7,
        },
        sort: {
          by: 'ORT',
          reverse: true,
        },
      };
      component.onDatagridStateChanged(state);
    });

    describe('before result', () => {
      it('should set loading to be true', () => {
        expect(component.loading).toBe(true);
      });

      it('should invoke readAllBetriebssaette with mapped state to gridstatedto and berichtsjahr', inject(
        [BetriebsstaettenRestControllerService],
        (betriebsstaettenService) => {
          expect(
            betriebsstaettenService.readAllBetriebsstaette
          ).toHaveBeenLastCalledWith(2020, {
            suche: null,
            page: { size: 5, current: 6 },
            sort: {
              by: SortStateDtoBetriebsstaetteColumnByEnum.ORT,
              reverse: true,
            },
          });
        }
      ));
    });

    describe('successful', () => {
      beforeEach(() => {
        allBetriebsstaetten$.next(pageResponse);
      });

      it('should set loading to be false', () => {
        expect(component.loading).toBe(false);
      });

      it('should set new page to betriebsstaettenpage', () => {
        expect(component.betriebsstaettenPage).toBe(pageResponse);
      });
    });

    describe('unsuccessful', () => {
      beforeEach(() => {
        allBetriebsstaetten$.error('');
      });

      it('should set loading to be false', () => {
        expect(component.loading).toBe(false);
      });

      it('should set alert message', inject(
        [AlertService],
        (alertService: AlertService) => {
          expect(alertService.setAlert).toHaveBeenCalled();
        }
      ));
    });

    describe('refresh on search change', () => {
      let suchattribute: Suchattribute;
      beforeEach(() => {
        suchattribute = {
          betriebsstaette: {
            name: 'betriebsstättenname*',
            statusIn: [12, 13],
          },
          anlage: {},
          betreiber: {},
        };
        component.searchChanged(suchattribute);
      });

      it('should invoke readAllBetriebsstaette with mapped state to gridstatedto and berichtsjahr', inject(
        [BetriebsstaettenRestControllerService],
        (betriebsstaettenService) => {
          expect(
            betriebsstaettenService.readAllBetriebsstaette
          ).toHaveBeenLastCalledWith(2020, {
            suche: suchattribute,
            page: { size: 5, current: 6 },
            sort: {
              by: SortStateDtoBetriebsstaetteColumnByEnum.ORT,
              reverse: true,
            },
          });
        }
      ));
    });
  });

  describe('on berichtsjahrwechsel', () => {
    beforeEach(() => {
      component.currentPage = 3;
      selectedBerichtsjahr$.next({ id: 2019 } as ReferenzDto);
    });

    it('should set loading to be true', () => {
      expect(component.loading).toBe(true);
    });

    it('should reset to page 1', () => {
      expect(component.currentPage).toBe(1);
    });

    it('should clear selection', () => {
      expect(component.selectedIds).toHaveLength(0);
    });

    it('should invoke readAllBetriebssaette with mapped state to gridstatedto and berichtsjahr', inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
        expect(
          betriebsstaettenService.readAllBetriebsstaette
        ).toHaveBeenLastCalledWith(2019, {
          suche: null,
          sort: null,
          page: { current: 0, size: undefined },
        });
      }
    ));
  });

  it('markierung aufheben should empty selectedIds', () => {
    component.markierungAufheben();
    expect(component.selectedIds).toHaveLength(0);
  });

  it('alle markieren should call readAllBetriebsstaetteIds', inject(
    [BetriebsstaettenRestControllerService],
    (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
      component.alleMarkieren();
      expect(
        betriebsstaettenService.readAllBetriebsstaetteIds
      ).toHaveBeenLastCalledWith(2020, null);
    }
  ));
});
