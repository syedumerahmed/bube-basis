import { Component } from '@angular/core';
import { ClarityIcons, fileGroupIcon, fileIcon } from '@cds/core/icon';

ClarityIcons.addIcons(fileGroupIcon, fileIcon);
@Component({
  selector: 'app-pruefberichte',
  templateUrl: './pruefberichte.component.html',
  styleUrls: ['./pruefberichte.component.scss'],
})
export class PruefberichteComponent {}
