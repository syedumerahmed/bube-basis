import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruefberichteComponent } from './pruefberichte.component';
import { ClarityModule } from '@clr/angular';
import { MockModule } from 'ng-mocks';

describe('PruefberichteComponent', () => {
  let component: PruefberichteComponent;
  let fixture: ComponentFixture<PruefberichteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule)],
      declarations: [PruefberichteComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PruefberichteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
