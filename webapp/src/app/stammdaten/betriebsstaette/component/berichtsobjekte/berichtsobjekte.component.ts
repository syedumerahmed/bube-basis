import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClarityIcons, fileGroupIcon } from '@cds/core/icon';

export interface BerichtsobjektData {
  art: string;
  status: string;
  url: string;
}

ClarityIcons.addIcons(fileGroupIcon);
@Component({
  selector: 'app-berichtsobjekte',
  templateUrl: './berichtsobjekte.component.html',
  styleUrls: ['./berichtsobjekte.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BerichtsobjekteComponent implements OnInit {
  berichtsobjekte: Array<BerichtsobjektData> = [];

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.berichtsobjekte = (data.betriebsstaette.berichtsobjekte ?? []).map(
        (berichtsobjekt) => ({
          art: berichtsobjekt.berichtsart.schluessel,
          status: berichtsobjekt.bearbeitungsstatus.ktext,
          url: berichtsobjekt.url,
        })
      );
    });
  }
}
