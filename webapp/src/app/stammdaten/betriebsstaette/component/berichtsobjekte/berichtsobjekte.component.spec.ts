import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BerichtsobjekteComponent } from './berichtsobjekte.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Data } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

describe('BerichtsobjekteComponent', () => {
  let component: BerichtsobjekteComponent;
  let fixture: ComponentFixture<BerichtsobjekteComponent>;

  const dataSubject: Subject<Data> = new BehaviorSubject({});

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), RouterTestingModule],
      declarations: [BerichtsobjekteComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BerichtsobjekteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have no berichtsobjekte', () => {
    expect(component.berichtsobjekte).toHaveLength(0);
  });

  describe('betriebsstaette without berichtsobjekte', () => {
    beforeEach(() => {
      dataSubject.next({
        betriebsstaette: {},
      });
    });

    it('should have no berichtsobjekte', () => {
      expect(component.berichtsobjekte).toHaveLength(0);
    });
  });

  describe('betriebsstaette with empty berichtsobjekt array', () => {
    beforeEach(() => {
      dataSubject.next({
        betriebsstaette: {
          berichtsobjekte: [],
        },
      });
    });

    it('should have no berichtsobjekte', () => {
      expect(component.berichtsobjekte).toHaveLength(0);
    });
  });

  describe('betriebsstaette with non-empty berichtsobjekt array', () => {
    beforeEach(() => {
      dataSubject.next({
        betriebsstaette: {
          berichtsobjekte: [
            {
              berichtsart: {
                schluessel: 'EURegBST',
              },
              bearbeitungsstatus: {
                ktext: 'unbearbeitet',
              },
              url: 'bericht123',
            },
          ],
        },
      });
    });

    it('should have one berichtsobjekt', () => {
      expect(component.berichtsobjekte).toHaveLength(1);
      expect(component.berichtsobjekte[0].art).toBe('EURegBST');
      expect(component.berichtsobjekte[0].status).toBe('unbearbeitet');
      expect(component.berichtsobjekte[0].url).toBe('bericht123');
    });
  });
});
