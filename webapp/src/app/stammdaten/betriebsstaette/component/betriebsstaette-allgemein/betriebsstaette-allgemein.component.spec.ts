import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetriebsstaetteAllgemeinComponent } from './betriebsstaette-allgemein.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { MockModule } from 'ng-mocks';
import { FormBuilder } from '@/base/forms';
import { of } from 'rxjs';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import {
  BehoerdeListItemDto,
  BetriebsstaetteDto,
  ReferenzDto,
} from '@api/stammdaten';

describe('BetriebsstaetteAllgemeinComponent', () => {
  const betriebsstaette: BetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    berichtsjahr: { schluessel: '2020' } as ReferenzDto,
    land: '00',
    zustaendigeBehoerde: { schluessel: '01' } as BehoerdeListItemDto,
    name: 'Meine neue Betriebsstaette',
    betriebsstaetteNr: 'BST1234',
    betriebsstatus: { schluessel: '01' } as ReferenzDto,
    vertraulichkeitsgrund: { schluessel: '01' } as ReferenzDto,
    vorschriften: [
      { schluessel: '01' },
      { schluessel: '02' },
    ] as Array<ReferenzDto>,
    akz: 'akz',
    einleiterNummern: ['nr1', 'nr2'],
    abfallerzeugerNummern: ['abfall', 'erzeuger', 'nr'],
    naceCode: { schluessel: '01' } as ReferenzDto,
    inbetriebnahme: '2020-10-10',
    flusseinzugsgebiet: { schluessel: '01' } as ReferenzDto,
    indirekteinleiter: true,
    direkteinleiter: false,
    betriebsstatusSeit: '2010-12-12',
    bemerkung: 'bemerkung',
    gemeindekennziffer: { schluessel: '01' } as ReferenzDto,
    localId: 'local-id',
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  const betriebsstatusListe: Array<Required<ReferenzDto>> = [];
  const vorschriftenListe: Array<Required<ReferenzDto>> =
    betriebsstaette.vorschriften as Array<Required<ReferenzDto>>;
  const vertraulichkeitsgrundListe: Array<Required<ReferenzDto>> = [];
  const gemeindekennzifferListe: Array<Required<ReferenzDto>> = [];
  const flusseinzugsgebietListe: Array<Required<ReferenzDto>> = [];
  const naceCodeListe: Array<ReferenzDto> = [];
  const behoerdenListe: Array<BehoerdeListItemDto> = [];
  const epsgCodeListe: Array<ReferenzDto> = [];

  let component: BetriebsstaetteAllgemeinComponent;
  let fixture: ComponentFixture<BetriebsstaetteAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BetriebsstaetteAllgemeinComponent],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(BasedomainModule),
        MockModule(BaseModule),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              data: of({
                betriebsstatusListe,
                vorschriftenListe,
                vertraulichkeitsgrundListe,
                gemeindekennzifferListe,
                flusseinzugsgebietListe,
                naceCodeListe,
                behoerdenListe,
                epsgCodeListe,
              }),
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(BetriebsstaetteAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = BetriebsstaetteAllgemeinComponent.createForm(
      formBuilder,
      BetriebsstaetteAllgemeinComponent.toForm(
        betriebsstaette,
        new Referenzliste<BehoerdeListItem>(
          behoerdenListe as Array<BehoerdeListItem>
        ),
        new Referenzliste<Required<ReferenzDto>>(vertraulichkeitsgrundListe),
        new Referenzliste<Required<ReferenzDto>>(vorschriftenListe),
        new Referenzliste<Required<ReferenzDto>>(flusseinzugsgebietListe)
      ),
      {
        gueltigBis: 0,
        gueltigVon: 0,
        ktext: '',
        land: '',
        referenzliste: undefined,
        sortier: '',
        schluessel: '2020',
      }
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
