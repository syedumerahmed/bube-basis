import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormUtil,
} from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { ValidationErrors, Validators } from '@angular/forms';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BetriebsstaetteDto, ReferenzDto } from '@api/stammdaten';
import {
  GeoPunktComponent,
  GeoPunktFormData,
} from '@/basedomain/component/geo-punkt/geo-punkt.component';

export interface BetriebsstaetteAllgemeinFormData {
  name: string;
  betriebsstaetteNr: string;
  localId: string;
  akz: string;
  inbetriebnahme: string;
  zustaendigeBehoerde: BehoerdeListItem;
  betriebsstatus: ReferenzDto;
  betriebsstatusSeit: string;
  naceCode: ReferenzDto;
  flusseinzugsgebiet: ReferenzDto;
  gemeindekennziffer: ReferenzDto;
  geoPunkt: GeoPunktFormData;
  vorschriften: Array<ReferenzDto>;
  abfallerzeugerNummern: Array<string>;
  einleiterNummern: Array<string>;
  direkteinleiter: boolean;
  indirekteinleiter: boolean;
  vertraulichkeitsgrund: ReferenzDto;
  bemerkung: string;
}

@Component({
  selector: 'app-betriebsstaette-allgemein',
  templateUrl: './betriebsstaette-allgemein.component.html',
  styleUrls: ['./betriebsstaette-allgemein.component.scss'],
  providers: [DatePipe],
})
export class BetriebsstaetteAllgemeinComponent
  implements OnInit, AfterViewInit, AfterViewChecked
{
  constructor(
    private readonly datePipe: DatePipe,
    private readonly route: ActivatedRoute
  ) {}

  @Input()
  form: FormGroup<BetriebsstaetteAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;
  @ViewChild(GeoPunktComponent)
  geoPunktComponent: GeoPunktComponent;

  betriebsstatusListe: Referenzliste<Required<ReferenzDto>>;
  gemeindekennzifferListe: Referenzliste<Required<ReferenzDto>>;
  vorschriftenListe: Referenzliste<Required<ReferenzDto>>;
  vertraulichkeitsgrundListe: Referenzliste<Required<ReferenzDto>>;
  flusseinzugsgebietListe: Referenzliste<Required<ReferenzDto>>;
  naceCodeListe: Referenzliste<Required<ReferenzDto>>;
  behoerdenListe: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  today: string;

  static stringArrayElementlengthValidator(length: number) {
    return (control: AbstractControl<Array<string>>): ValidationErrors => {
      if (!control.value) {
        return null;
      }
      if (!control.value.every((e) => e.length < length)) {
        return { length: length };
      }
      return null;
    };
  }

  static toForm(
    betriebsstaetteDto: BetriebsstaetteDto,
    behoerden: Referenzliste<BehoerdeListItem>,
    vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>,
    vorschriften: Referenzliste<Required<ReferenzDto>>,
    flusseinzugsgebiete: Referenzliste<Required<ReferenzDto>>
  ): BetriebsstaetteAllgemeinFormData {
    return {
      name: betriebsstaetteDto.name,
      localId: betriebsstaetteDto.localId,
      betriebsstatus: betriebsstaetteDto.betriebsstatus,
      zustaendigeBehoerde: betriebsstaetteDto.zustaendigeBehoerde
        ? behoerden.find(
            new BehoerdeListItem(betriebsstaetteDto.zustaendigeBehoerde)
          )
        : null,
      gemeindekennziffer: betriebsstaetteDto.gemeindekennziffer,
      geoPunkt: GeoPunktComponent.toForm(betriebsstaetteDto.geoPunkt),
      vorschriften: vorschriften.map(
        (betriebsstaetteDto.vorschriften ?? []) as Array<Required<ReferenzDto>>
      ),
      vertraulichkeitsgrund: vertraulichkeitsgruende.find(
        betriebsstaetteDto.vertraulichkeitsgrund as Required<ReferenzDto>
      ),
      abfallerzeugerNummern: [...betriebsstaetteDto.abfallerzeugerNummern],
      einleiterNummern: [...betriebsstaetteDto.einleiterNummern],
      akz: betriebsstaetteDto.akz,
      bemerkung: betriebsstaetteDto.bemerkung,
      betriebsstaetteNr: betriebsstaetteDto.betriebsstaetteNr,
      betriebsstatusSeit: FormUtil.fromIsoDate(
        betriebsstaetteDto.betriebsstatusSeit
      ),
      direkteinleiter: betriebsstaetteDto.direkteinleiter,
      indirekteinleiter: betriebsstaetteDto.indirekteinleiter,
      flusseinzugsgebiet: flusseinzugsgebiete.find(
        betriebsstaetteDto.flusseinzugsgebiet as Required<ReferenzDto>
      ),
      inbetriebnahme: FormUtil.fromIsoDate(betriebsstaetteDto.inbetriebnahme),
      naceCode: betriebsstaetteDto.naceCode,
    };
  }

  static createForm(
    fb: FormBuilder,
    betriebsstaette: BetriebsstaetteAllgemeinFormData,
    selectedYear: ReferenzDto
  ): FormGroup<BetriebsstaetteAllgemeinFormData> {
    return fb.group<BetriebsstaetteAllgemeinFormData>({
      akz: [betriebsstaette.akz, [Validators.maxLength(14)]],
      name: [
        betriebsstaette.name,
        [Validators.required, Validators.maxLength(250)],
      ],
      betriebsstaetteNr: [
        betriebsstaette.betriebsstaetteNr,
        [Validators.required, Validators.maxLength(20)],
      ],
      localId: [betriebsstaette.localId, [Validators.maxLength(40)]],
      inbetriebnahme: [betriebsstaette.inbetriebnahme, BubeValidators.date],
      zustaendigeBehoerde: [
        betriebsstaette.zustaendigeBehoerde,
        [
          Validators.required,
          BubeValidators.isReferenceValid(Number(selectedYear.schluessel)),
        ],
      ],
      betriebsstatus: [
        betriebsstaette.betriebsstatus,
        [
          Validators.required,
          BubeValidators.isReferenceValid(Number(selectedYear.schluessel)),
        ],
      ],
      direkteinleiter: [betriebsstaette.direkteinleiter],
      indirekteinleiter: [betriebsstaette.indirekteinleiter],
      bemerkung: [betriebsstaette.bemerkung, [Validators.maxLength(1000)]],
      betriebsstatusSeit: [
        betriebsstaette.betriebsstatusSeit,
        BubeValidators.date,
      ],
      gemeindekennziffer: [
        betriebsstaette.gemeindekennziffer,
        [
          Validators.required,
          BubeValidators.isReferenceValid(Number(selectedYear.schluessel)),
        ],
      ],
      geoPunkt: GeoPunktComponent.createForm(
        fb,
        betriebsstaette.geoPunkt,
        selectedYear
      ),
      vorschriften: [
        betriebsstaette.vorschriften,
        [BubeValidators.areReferencesValid(Number(selectedYear.schluessel))],
      ],
      vertraulichkeitsgrund: [
        betriebsstaette.vertraulichkeitsgrund,
        [BubeValidators.isReferenceValid(Number(selectedYear.schluessel))],
      ],
      flusseinzugsgebiet: [
        betriebsstaette.flusseinzugsgebiet,
        [BubeValidators.isReferenceValid(Number(selectedYear.schluessel))],
      ],
      naceCode: [
        betriebsstaette.naceCode,
        [BubeValidators.isReferenceValid(Number(selectedYear.schluessel))],
      ],
      einleiterNummern: [
        betriebsstaette.einleiterNummern,
        [this.stringArrayElementlengthValidator(255)],
      ],
      abfallerzeugerNummern: [
        betriebsstaette.abfallerzeugerNummern,
        [this.stringArrayElementlengthValidator(255)],
      ],
    });
  }

  ngOnInit(): void {
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.route.parent.data.subscribe((data) => {
      this.betriebsstatusListe = new Referenzliste(data.betriebsstatusListe);
      this.gemeindekennzifferListe = new Referenzliste(
        data.gemeindekennzifferListe
      );
      this.vorschriftenListe = new Referenzliste(data.vorschriftenListe);
      this.vertraulichkeitsgrundListe = new Referenzliste(
        data.vertraulichkeitsgrundListe
      );
      this.flusseinzugsgebietListe = new Referenzliste(
        data.flusseinzugsgebietListe
      );
      this.naceCodeListe = new Referenzliste(data.naceCodeListe);
      this.behoerdenListe = new Referenzliste(data.behoerdenListe);
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    if (this.form.invalid) {
      this.clrForm.markAsTouched();
    }
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }

  get geoPunktForm(): FormGroup<GeoPunktFormData> {
    return this.form.get('geoPunkt') as FormGroup<GeoPunktFormData>;
  }
}
