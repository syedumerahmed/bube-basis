import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import {
  AnsprechpartnerComponent,
  AnsprechpartnerFormData,
} from './ansprechpartner.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ReferenzDto } from '@api/stammdaten';
import { FormBuilder, FormGroup } from '@/base/forms';
import { MockModule, MockProviders } from 'ng-mocks';
import { KommunikationsTypenSteuerung } from '@/basedomain/component/kommunikation/KommunikationsTypenSteuerung';
import { KommunikationsverbindungFormData } from '@/basedomain/component/kommunikation/kommunikation.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';

describe('AnsprechpartnerComponent', () => {
  let component: AnsprechpartnerComponent;
  let fixture: ComponentFixture<AnsprechpartnerComponent>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const kommunikationsTypen = new Referenzliste([
    { id: 2, schluessel: '02', ktext: 'tel' },
    { id: 3, schluessel: '03', ktext: 'mobil' },
    {
      id: 4,
      schluessel: 'url',
      ktext: 'url',
      strg: KommunikationsTypenSteuerung.URL,
    },
  ] as Array<Required<ReferenzDto>>);

  const funktionen = new Referenzliste<Required<ReferenzDto>>([
    { id: 2, schluessel: '02' },
    { id: 3, schluessel: '03' },
  ] as Array<Required<ReferenzDto>>);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(BasedomainModule),
      ],
      declarations: [AnsprechpartnerComponent],
      providers: MockProviders(BerichtsjahrService),
    }).compileComponents();
    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnsprechpartnerComponent);
      component = fixture.componentInstance;
      component.formArray = AnsprechpartnerComponent.createForm(
        fb,
        AnsprechpartnerComponent.toForm([], kommunikationsTypen),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      component.kommunikationsTypen = kommunikationsTypen;
      component.funktionen = funktionen;
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnsprechpartnerComponent);
      component = fixture.componentInstance;
      component.formArray = AnsprechpartnerComponent.createForm(
        fb,
        AnsprechpartnerComponent.toForm(
          [
            {
              vorname: 'Harri',
              nachname: 'Hirsch',
              funktion: funktionen.values[0],
              kommunikationsverbindungen: [
                { typ: kommunikationsTypen.values[0], verbindung: '1234' },
              ],
            },
            {
              vorname: 'Maxi',
              nachname: 'Musti',
              funktion: funktionen.values[1],
              kommunikationsverbindungen: [
                { typ: kommunikationsTypen.values[1], verbindung: '4321' },
              ],
            },
          ],
          kommunikationsTypen
        ),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      component.kommunikationsTypen = kommunikationsTypen;
      component.funktionen = funktionen;
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.ansprechpartnerForm).toBeTruthy();
        expect(component.ansprechpartnerForm.value).toStrictEqual({
          vorname: null,
          nachname: null,
          funktion: null,
          bemerkung: null,
          kommunikationsverbindungen: [],
        });
        expect(component.ansprechpartnerForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let formValue: AnsprechpartnerFormData;
        beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
          formValue = {
            vorname: 'Test',
            bemerkung: 'Bemerkung',
            nachname: 'Name',
            funktion: funktionen.values[0],
            kommunikationsverbindungen: [
              { typ: kommunikationsTypen.values[0], verbindung: '1111' },
            ],
          };
          component.ansprechpartnerForm.setControl(
            'kommunikationsverbindungen',
            fb.array<KommunikationsverbindungFormData>([
              fb.group({
                typ: kommunikationsTypen.values[1],
                verbindung: ['1111'],
              }),
            ])
          );
          component.ansprechpartnerForm.setValue(formValue);
        }));
        it('should be valid', () => {
          expect(component.ansprechpartnerForm.valid).toBe(true);
        });
        it('should validate kommunikationsverbindung with strg', () => {
          const kommunikationsverbindungControl =
            component.kommunikationsverbindungen.at(
              0
            ) as FormGroup<KommunikationsverbindungFormData>;
          kommunikationsverbindungControl.setValue({
            typ: kommunikationsTypen.values[2],
            verbindung: '123456789',
          });
          component.kommunikationsTypChanged(kommunikationsverbindungControl);
          expect(kommunikationsverbindungControl.valid).toBe(false);
          expect(
            kommunikationsverbindungControl.getError('url', 'verbindung')
          ).toHaveProperty('invalid_scheme');
          expect(component.ansprechpartnerForm.valid).toBe(false);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(formValue);
          });

          it('should null ansprechpartnerForm', () => {
            expect(component.ansprechpartnerForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(formValue);
          });

          it('should null ansprechpartnerForm', () => {
            expect(component.ansprechpartnerForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.ansprechpartnerForm).toBeTruthy();
        expect(component.ansprechpartnerForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.ansprechpartnerForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: AnsprechpartnerFormData;
        let newValue: AnsprechpartnerFormData;
        beforeEach(() => {
          originalValue = component.ansprechpartnerForm.value;
          newValue = {
            vorname: 'Vorname',
            bemerkung: 'Bemerkung',
            nachname: 'Nachname',
            funktion: funktionen.values[0],
            kommunikationsverbindungen: [
              { typ: kommunikationsTypen.values[0], verbindung: '1111' },
            ],
          };
          component.ansprechpartnerForm.setValue(newValue);
          component.ansprechpartnerForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(newValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.ansprechpartnerForm.patchValue(newValue);
            component.ansprechpartnerForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null ansprechpartnerForm', () => {
            expect(component.ansprechpartnerForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(newValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();

            expect(component.formArray.dirty).toBe(true);
          });

          it('should null ansprechpartnerForm', () => {
            expect(component.ansprechpartnerForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: AnsprechpartnerFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
      });
    });
  });
});
