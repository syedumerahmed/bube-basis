import { AfterViewChecked, Component, Input, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  KommunikationsverbindungFormData,
  kommunikationsverbindungValidator,
} from '@/basedomain/component/kommunikation/kommunikation.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { AnsprechpartnerDto, ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface AnsprechpartnerFormData {
  vorname: string;
  nachname: string;
  funktion: ReferenzDto;
  bemerkung: string;
  kommunikationsverbindungen: KommunikationsverbindungFormData[];
}

ClarityIcons.addIcons(trashIcon, errorStandardIcon, noteIcon, plusIcon);

@Component({
  selector: 'app-ansprechpartner',
  templateUrl: './ansprechpartner.component.html',
  styleUrls: ['./ansprechpartner.component.scss'],
})
export class AnsprechpartnerComponent implements AfterViewChecked {
  constructor(
    private fb: FormBuilder,
    private berichtsjahrService: BerichtsjahrService
  ) {}

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  get kommunikationsverbindungen(): FormArray<KommunikationsverbindungFormData> {
    return this.ansprechpartnerForm.controls
      .kommunikationsverbindungen as FormArray<KommunikationsverbindungFormData>;
  }

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  ansprechpartnerForm: FormGroup<AnsprechpartnerFormData>;

  @Input()
  formArray: FormArray<AnsprechpartnerFormData>;

  @Input()
  kommunikationsTypen: Referenzliste<Required<ReferenzDto>>;
  @Input()
  funktionen: Referenzliste<Required<ReferenzDto>>;
  @Input()
  hideBemerkung = false;

  editModalOpen = false;

  editExistingIndex: number = null;

  static toForm(
    ansprechpartner: Array<AnsprechpartnerDto>,
    kommunikationsTypen: Referenzliste<Required<ReferenzDto>>
  ): Array<AnsprechpartnerFormData> {
    return (ansprechpartner ?? []).map((ansprechperson) => ({
      vorname: ansprechperson.vorname,
      nachname: ansprechperson.nachname,
      funktion: ansprechperson.funktion,
      bemerkung: ansprechperson.bemerkung,
      kommunikationsverbindungen: ansprechperson.kommunikationsverbindungen.map(
        (komm) => ({
          verbindung: komm.verbindung,
          typ: kommunikationsTypen.find(komm.typ as Required<ReferenzDto>),
        })
      ),
    }));
  }

  static createForm(
    fb: FormBuilder,
    ansprechpartner: Array<AnsprechpartnerFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<AnsprechpartnerFormData> {
    return fb.array<AnsprechpartnerFormData>(
      ansprechpartner.map((ansprechperson) => {
        const formGroup = fb.group({
          vorname: [ansprechperson.vorname, [Validators.maxLength(120)]],
          nachname: [
            ansprechperson.nachname,
            [Validators.required, Validators.maxLength(80)],
          ],
          funktion: [
            ansprechperson.funktion,
            [
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
          bemerkung: [ansprechperson.bemerkung, [Validators.maxLength(1000)]],
          kommunikationsverbindungen: fb.array([]),
        });
        formGroup.setControl(
          'kommunikationsverbindungen',
          this.kommunikationCreateForm(
            fb,
            ansprechperson.kommunikationsverbindungen,
            selectedBerichtsJahr
          )
        );
        return formGroup;
      })
    );
  }

  private static kommunikationCreateForm(
    fb: FormBuilder,
    kommunikationsverbindungen: Array<KommunikationsverbindungFormData>,
    selectedBerichtsjahr: ReferenzDto
  ): FormArray<KommunikationsverbindungFormData> {
    return fb.array<KommunikationsverbindungFormData>(
      kommunikationsverbindungen.map((komm) => {
        return fb.group({
          verbindung: [
            komm.verbindung,
            kommunikationsverbindungValidator(komm.typ.strg),
          ],
          typ: [
            komm.typ,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsjahr.schluessel)
              ),
            ],
          ],
        });
      })
    );
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    const value = this.formArray.at(index).value;
    editableForm.setControl(
      'kommunikationsverbindungen',
      AnsprechpartnerComponent.kommunikationCreateForm(
        this.fb,
        value.kommunikationsverbindungen,
        this.berichtsjahrService.selectedBerichtsjahr
      )
    );
    editableForm.setValue(value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.ansprechpartnerForm = null;
  }

  uebernehmen(): void {
    if (this.ansprechpartnerForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(
          this.editExistingIndex
        ) as FormGroup<AnsprechpartnerFormData>;
        const value = this.ansprechpartnerForm.value;
        editedControl.setControl(
          'kommunikationsverbindungen',
          AnsprechpartnerComponent.kommunikationCreateForm(
            this.fb,
            value.kommunikationsverbindungen,
            this.berichtsjahrService.selectedBerichtsjahr
          )
        );
        editedControl.setValue(value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.ansprechpartnerForm)) {
        this.formArray.push(this.ansprechpartnerForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.ansprechpartnerForm = null;
    }
  }

  kommunikationsTypChanged(
    kommunikationGroup: FormGroup<KommunikationsverbindungFormData>
  ): void {
    const verbindung = kommunikationGroup.controls.verbindung;
    const steuerung = kommunikationGroup.value.typ.strg;
    verbindung.setValidators(kommunikationsverbindungValidator(steuerung));
    verbindung.updateValueAndValidity();
  }

  loescheKommunikation(index: number): void {
    this.kommunikationsverbindungen.removeAt(index);
    this.ansprechpartnerForm.markAsDirty();
  }

  neueKommunikation(): void {
    this.kommunikationsverbindungen.push(this.createEmptyKommunikationForm());
  }

  private createEmptyForm(): FormGroup<AnsprechpartnerFormData> {
    return this.fb.group<AnsprechpartnerFormData>({
      vorname: [null, [Validators.maxLength(120)]],
      nachname: [null, [Validators.required, Validators.maxLength(80)]],
      funktion: [
        null,
        [
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      bemerkung: [null, [Validators.maxLength(1000)]],
      kommunikationsverbindungen:
        this.fb.array<KommunikationsverbindungFormData>([]),
    });
  }

  private createEmptyKommunikationForm(): FormGroup<KommunikationsverbindungFormData> {
    return this.fb.group({
      typ: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      verbindung: [null, kommunikationsverbindungValidator(null)],
    });
  }

  private openEdit(formGroup: FormGroup<AnsprechpartnerFormData>): void {
    this.ansprechpartnerForm = formGroup;
    this.editModalOpen = true;
  }
}
