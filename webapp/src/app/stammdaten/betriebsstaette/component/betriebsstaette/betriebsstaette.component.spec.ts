import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BetriebsstaetteComponent } from './betriebsstaette.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('BetriebsstaetteComponent', () => {
  let component: BetriebsstaetteComponent;
  let fixture: ComponentFixture<BetriebsstaetteComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [BetriebsstaetteComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(BetriebsstaetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
