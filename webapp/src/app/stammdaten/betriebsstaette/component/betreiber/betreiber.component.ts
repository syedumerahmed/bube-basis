import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { prop, sortBy } from 'ramda';
import {
  BetreiberListItemDto,
  BetreiberRestControllerService,
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';
import {
  ClarityIcons,
  eyeIcon,
  floppyIcon,
  plusIcon,
  timesIcon,
  trashIcon,
  userIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

ClarityIcons.addIcons(
  trashIcon,
  userIcon,
  eyeIcon,
  plusIcon,
  floppyIcon,
  timesIcon
);

@Component({
  selector: 'app-betreiber',
  templateUrl: './betreiber.component.html',
  styleUrls: ['./betreiber.component.scss'],
})
export class BetreiberComponent implements OnInit, UnsavedChanges {
  betreiber: BetreiberListItemDto = null;
  selectedBetreiber: BetreiberListItemDto = null;
  betreiberListe: Array<BetreiberListItemDto>;
  betriebsstaette: BetriebsstaetteDto;
  loeschenDialog: boolean;

  @Output()
  betreiberAuswahl: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private readonly benutzerProfil: BenutzerProfil,
    private readonly betriebsstaettenService: BetriebsstaettenRestControllerService,
    private readonly betreiberService: BetreiberRestControllerService,
    private readonly route: ActivatedRoute,
    private readonly alertService: AlertService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaette = data.betriebsstaette;
      this.betreiberListe = sortBy(prop('name'), data.betreiberListe);

      const betreiberId = this.betriebsstaette.betreiberId;

      if (betreiberId != null) {
        this.betreiber = this.betreiberListe.find(
          (betreiber) => betreiber.id === betreiberId
        );
        this.selectedBetreiber = this.betreiber;
      }
    });
  }

  save(): void {
    this.betriebsstaette.betreiberId = this.selectedBetreiber?.id;
    let response: Observable<BetriebsstaetteDto>;
    if (this.betriebsstaette.id == null) {
      response = this.betriebsstaettenService.createBetriebsstaette(
        this.betriebsstaette
      );
    } else {
      response = this.betriebsstaettenService.updateBetriebsstaette(
        this.betriebsstaette
      );
    }

    response.subscribe(
      (betriebsstaette) => {
        this.betriebsstaette = betriebsstaette;
        this.betreiber = this.selectedBetreiber;
      },
      (err) => {
        this.alertService.setAlert(err);
        this.betriebsstaette.betreiberId = this.betreiber?.id;
      }
    );
  }

  deleteBetreiber(): void {
    this.betreiberService.deleteBetreiber(this.betreiber.id).subscribe(
      () => {
        const index = this.betreiberListe.indexOf(this.betreiber);
        this.betreiberListe.splice(index, 1);
        this.betreiber = null;
        this.selectedBetreiber = null;
      },
      (err) => this.alertService.setAlert(err)
    );

    this.loeschenDialog = false;
  }

  verwerfen(): void {
    this.selectedBetreiber = this.betreiber;
  }

  speichern(): void {
    this.save();
  }

  neueBetriebsstaette(): boolean {
    return !this.betriebsstaette.id;
  }

  keinBetreiber(): boolean {
    return !this.betreiber;
  }

  get selectionDirty(): boolean {
    return this.selectedBetreiber !== this.betreiber;
  }

  get canDelete(): boolean {
    // Der Betreiber kann nur gelöscht werden, wenn auch das Schreibrecht auf die BST vorhanden ist
    return this.canWrite && this.benutzerProfil.canDeleteStammdaten;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  createBetreiber(): void {
    this.router.navigate(['betreiber', 'neu'], { relativeTo: this.route });
  }

  navigateToBetreiber(betreiberNummer: string): void {
    this.router.navigate(['betreiber', 'nummer', betreiberNummer], {
      relativeTo: this.route,
    });
  }

  canDeactivate(): boolean {
    return !this.selectionDirty;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
