import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetreiberComponent } from './betreiber.component';
import { ActivatedRoute, Data } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;
import {
  BetreiberListItemDto,
  BetreiberRestControllerService,
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';

describe('BetreiberComponent', () => {
  let component: BetreiberComponent;
  let fixture: ComponentFixture<BetreiberComponent>;
  const createNoBetreiberBetriebsstaette: () => BetriebsstaetteDto = () => ({
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    gemeindekennziffer: undefined,
    zustaendigeBehoerde: undefined,
    id: 1,
    name: '',
    betreiberId: null,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  });

  const dataSubject: Subject<Data> = new BehaviorSubject({
    betriebsstaette: createNoBetreiberBetriebsstaette(),
    betreiberListe: [],
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        RouterTestingModule,
        MockModule(FormsModule),
      ],
      declarations: [BetreiberComponent],
      providers: [
        { provide: AlertService, useValue: MockService(AlertService) },
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
        {
          provide: BetreiberRestControllerService,
          useValue: MockService(BetreiberRestControllerService),
        },
        {
          provide: BerichtsjahrService,
          useValue: MockService(BerichtsjahrService),
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { schreibRecht: true } },
          },
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(BetreiberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('empty', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have no betreiber', () => {
      expect(component.keinBetreiber()).toBe(true);
    });

    it('should not have selectionDirty state', () => {
      expect(component.selectionDirty).toBeFalsy();
    });
  });

  describe('pre-filled', () => {
    const createBetreiberBetriebsstaette = (): BetriebsstaetteDto => ({
      adresse: { plz: '22085', ort: 'Hamburg' },
      betriebsstatus: undefined,
      berichtsjahr: undefined,
      betriebsstaetteNr: '',
      land: '',
      gemeindekennziffer: undefined,
      zustaendigeBehoerde: undefined,
      id: 1,
      name: '',
      betreiberId: 1,
      pruefungsfehler: false,
      pruefungVorhanden: false,
    });

    const createBetreiber: () => BetreiberListItemDto = () => ({
      id: 1,
      betreiberNummer: '1',
      name: 'name',
      display: 'ort',
    });

    const createSecondBetreiber: () => BetreiberListItemDto = () => ({
      id: 2,
      betreiberNummer: '2',
      name: 'name 2',
      display: 'ort 2',
    });

    const createBetreiberListe: () => Array<BetreiberListItemDto> = () => [
      createBetreiber(),
      createSecondBetreiber(),
    ];
    beforeEach(() => {
      dataSubject.next({
        betriebsstaette: createBetreiberBetriebsstaette(),
        betreiberListe: createBetreiberListe(),
      });
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have a betreiber', () => {
      expect(component.keinBetreiber()).toBe(false);
    });

    it('selectedBetreiber should be equal to betreiber', () => {
      expect(component.selectedBetreiber).toEqual(createBetreiber());
    });

    it('should not have selectionDirty state', () => {
      expect(component.selectionDirty).toBe(false);
    });

    describe('select betreiber', () => {
      beforeEach(() => {
        component.selectedBetreiber = createSecondBetreiber();
      });

      it('selectionDirty should be true', () => {
        expect(component.selectionDirty).toBe(true);
      });

      it('selectedBetreiber should not be equal to betreiber', () => {
        expect(component.selectedBetreiber).not.toEqual(createBetreiber());
      });

      describe('verwerfen', () => {
        beforeEach(() => {
          component.verwerfen();
        });

        it('selectionDirty should be false', () => {
          expect(component.selectionDirty).toBe(false);
        });

        it('selectedBetreiber should be equal to betreiber', () => {
          expect(component.selectedBetreiber).toEqual(createBetreiber());
        });
      });

      describe('speichern successful', () => {
        const serviceResponse: BetriebsstaetteDto = {
          ...createBetreiberBetriebsstaette(),
          betreiberId: 2,
        };

        beforeEach(inject(
          [BetriebsstaettenRestControllerService],
          (betriebsstaetteService: BetriebsstaettenRestControllerService) => {
            spyOn(
              betriebsstaetteService,
              'updateBetriebsstaette'
            ).mockReturnValue(of(serviceResponse) as Observable<any>);
            component.speichern();
          }
        ));

        it('selectionDirty should be false', () => {
          expect(component.selectionDirty).toBe(false);
        });

        it('betreiber should be equal to selectedBetreiber', () => {
          expect(component.betreiber).toEqual(createSecondBetreiber());
        });

        it('selectedBetreiber should be equal to selectedBetreiber', () => {
          expect(component.selectedBetreiber).toEqual(createSecondBetreiber());
        });

        it('betriebsstaette should be service response', () => {
          expect(component.betriebsstaette).toEqual(serviceResponse);
        });

        it('should send betriebsstaette with new betreiber to service', inject(
          [BetriebsstaettenRestControllerService],
          (betriebsstaetteService: BetriebsstaettenRestControllerService) => {
            expect(
              betriebsstaetteService.updateBetriebsstaette
            ).toHaveBeenCalledWith({
              ...createBetreiberBetriebsstaette(),
              betreiberId: 2,
            });
          }
        ));
      });

      describe('speichern unsuccessful', () => {
        beforeEach(inject(
          [BetriebsstaettenRestControllerService],
          (betriebsstaetteService: BetriebsstaettenRestControllerService) => {
            spyOn(
              betriebsstaetteService,
              'updateBetriebsstaette'
            ).mockReturnValue(throwError(''));
            component.speichern();
          }
        ));

        it('selectionDirty should be true', () => {
          expect(component.selectionDirty).toBe(true);
        });

        it('betreiber should be equal to betreiber', () => {
          expect(component.betreiber).toEqual(createBetreiber());
        });

        it('selectedBetreiber should be equal to selectedBetreiber', () => {
          expect(component.selectedBetreiber).toEqual(createSecondBetreiber());
        });

        it('betreiberId in betriebsstaette should be equal to betreiber id', () => {
          expect(component.betriebsstaette.betreiberId).toEqual(
            createBetreiber().id
          );
        });

        it('should invoke alert service', inject(
          [AlertService],
          (alertService: AlertService) => {
            expect(alertService.setAlert).toHaveBeenCalled();
          }
        ));
      });
    });

    describe('delete betreiber successful', () => {
      beforeEach(inject(
        [BetreiberRestControllerService],
        (betreiberService: BetreiberRestControllerService) => {
          spyOn(betreiberService, 'deleteBetreiber').mockReturnValue(of(null));
          component.deleteBetreiber();
        }
      ));

      it('should remove betreiber from betreiberliste', () => {
        expect(component.betreiberListe).toEqual([createSecondBetreiber()]);
      });

      it('selectedBetreiber should be null', () => {
        expect(component.selectedBetreiber).toBeNull();
      });

      it('betreiber should be null', () => {
        expect(component.betreiber).toBeNull();
      });
    });

    describe('delete betreiber not successful', () => {
      beforeEach(inject(
        [BetreiberRestControllerService],
        (betreiberService: BetreiberRestControllerService) => {
          spyOn(betreiberService, 'deleteBetreiber').mockReturnValue(
            throwError('')
          );
          component.deleteBetreiber();
        }
      ));

      it('should not remove betreiber from betreiberliste', () => {
        expect(component.betreiberListe).toEqual([
          createBetreiber(),
          createSecondBetreiber(),
        ]);
      });

      it('selectedBetreiber should be betreiber', () => {
        expect(component.selectedBetreiber).toEqual(createBetreiber());
      });

      it('betreiber should be betreiber', () => {
        expect(component.betreiber).toEqual(createBetreiber());
      });

      it('should invoke alert service', inject(
        [AlertService],
        (alertService: AlertService) => {
          expect(alertService.setAlert).toHaveBeenCalled();
        }
      ));
    });
  });
});
