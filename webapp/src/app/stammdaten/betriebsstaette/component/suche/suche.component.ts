import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormUtil } from '@/base/forms/form-util';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { ReferenzDto, ReferenzlisteEnum, Suchattribute } from '@api/stammdaten';
import { ClarityIcons, infoCircleIcon, searchIcon } from '@cds/core/icon';

export interface SucheFormData {
  betreiber: {
    name: string;
    plz: string;
    ort: string;
  };
  betriebsstaette: {
    land: string;
    name: string;
    nummer: string;
    localId: string;
    plz: string;
    ort: string;
    akz: string;
    gemeindeIn: Array<ReferenzDto & { id: number }>;
    behoerdeIn: Array<BehoerdeListItem>;
    naceIn: Array<ReferenzDto & { id: number }>;
    statusIn: Array<ReferenzDto & { id: number }>;
    uebernommen: boolean;
  };
  anlage: {
    searchAnlagenteile: boolean;
    name: string;
    nummer: string;
    localId: string;
    vorschriftenIn: Array<ReferenzDto & { id: number }>;
    zustaendigkeitIn: Array<ReferenzDto & { id: number }>;
    behoerdeIn: Array<BehoerdeListItem>;
    statusIn: Array<ReferenzDto & { id: number }>;
    uebernommen: boolean;
    taetigkeiten: {
      nr4BImSchV: Array<ReferenzDto & { id: number }>;
      prtr: Array<ReferenzDto & { id: number }>;
      ierl: Array<ReferenzDto & { id: number }>;
    };
  };
}

type ReferenzlisteAndSearch = {
  values$: BehaviorSubject<Array<ReferenzDto>>;
  search$: Subject<string>;
};

ClarityIcons.addIcons(searchIcon, infoCircleIcon);
@Component({
  selector: 'app-suche',
  templateUrl: './suche.component.html',
  styleUrls: ['./suche.component.scss'],
})
export class SucheComponent implements OnInit, OnDestroy {
  @Input()
  showLandIfGesamtadmin = false;
  @Output()
  searchChanged = new EventEmitter<Suchattribute>();
  suchForm: FormGroup<SucheFormData>;
  readonly behoerden$ = new BehaviorSubject<Array<BehoerdeListItem>>(null);
  readonly behoerdenSearch$ = new Subject<string>();
  private readonly referenzlisten = new Map<
    ReferenzlisteEnum,
    ReferenzlisteAndSearch
  >();
  private readonly destroyed$ = new Subject();

  constructor(
    private readonly fb: FormBuilder,
    private readonly referenzenService: ReferenzdatenService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly route: ActivatedRoute,
    private readonly benutzerProfil: BenutzerProfil
  ) {}

  get showLandControl(): boolean {
    return this.showLandIfGesamtadmin && this.benutzerProfil.isGesamtadmin;
  }

  get status(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RBETZ);
  }

  get gemeinden(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RGMD);
  }

  get zustaendigkeiten(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RZUST);
  }

  get vorschriften(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RVORS);
  }

  get naceCodes(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RNACE);
  }

  get taetigkeiten4bv(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.R4BV);
  }

  get taetigkeitenPrtr(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RPRTR);
  }

  get taetigkeitenIeRl(): ReferenzlisteAndSearch {
    return this.getReferenzliste(ReferenzlisteEnum.RIET);
  }

  private static extractId(
    referenzDtos: Array<{ id: number }>
  ): Array<number> | null {
    return referenzDtos?.map((dto) => dto.id) || null;
  }

  ngOnInit(): void {
    this.berichtsjahrService.selectedBerichtsjahr$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.resetReferenzenUndBehoerden());
    this.suchForm = this.fb.group<SucheFormData>({
      betreiber: this.fb.group({ name: null, ort: null, plz: null }),
      betriebsstaette: this.fb.group({
        land: null,
        name: null,
        nummer: null,
        localId: null,
        akz: null,
        behoerdeIn: null,
        gemeindeIn: null,
        uebernommen: null,
        naceIn: null,
        ort: null,
        plz: null,
        statusIn: null,
      }),
      anlage: this.fb.group<SucheFormData['anlage']>({
        searchAnlagenteile: null,
        name: null,
        nummer: null,
        localId: null,
        vorschriftenIn: null,
        zustaendigkeitIn: null,
        behoerdeIn: null,
        statusIn: null,
        uebernommen: null,
        taetigkeiten: this.fb.group({
          nr4BImSchV: null,
          prtr: null,
          ierl: null,
        }),
      }),
    });
    this.behoerdenSearch$
      .pipe(
        switchMap(() =>
          this.referenzenService.readBehoerden(
            this.route.snapshot.params.berichtsjahr
          )
        )
      )
      .subscribe(this.behoerden$);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  submitSearch(): void {
    this.searchChanged.emit(this.mapFormData());
  }

  resetForm(): void {
    this.suchForm.reset();
    this.searchChanged.emit(this.mapFormData());
  }

  suchParameter(suchattribute: Suchattribute): Array<[string, never]> {
    return Object.values(suchattribute)
      .reduce(
        (acc, v) =>
          acc.concat(Object.entries(v).filter(([k]) => k !== 'taetigkeiten')),
        []
      )
      .filter(([k, v]) => k !== 'searchAnlagenteile' && v != null);
  }

  getReferenzliste(name: ReferenzlisteEnum): ReferenzlisteAndSearch {
    const entry = this.referenzlisten.get(name);
    if (entry) {
      return entry;
    }
    const search$ = new Subject<string>();
    const values$ = new BehaviorSubject<Array<ReferenzDto>>(null);
    search$
      .pipe(
        switchMap(() =>
          this.referenzenService.readReferenzliste(
            name,
            this.route.snapshot.params.berichtsjahr,
            this.benutzerProfil.landNr
          )
        )
      )
      .subscribe(values$);
    const observables = {
      search$,
      values$,
    };
    this.referenzlisten.set(name, observables);
    return observables;
  }

  private resetReferenzenUndBehoerden(): void {
    for (const subject of this.referenzlisten.values()) {
      subject.values$.next(null);
    }
    this.behoerden$.next(null);
  }

  private mapFormData(): Suchattribute {
    const formValue = this.suchForm.value;
    return FormUtil.emptyStringsToNull<Suchattribute>({
      betriebsstaette: {
        ...formValue.betriebsstaette,
        gemeindeIn: SucheComponent.extractId(
          formValue.betriebsstaette.gemeindeIn
        ),
        behoerdeIn: SucheComponent.extractId(
          formValue.betriebsstaette.behoerdeIn
        ),
        naceIn: SucheComponent.extractId(formValue.betriebsstaette.naceIn),
        statusIn: SucheComponent.extractId(formValue.betriebsstaette.statusIn),
      },
      anlage: {
        ...formValue.anlage,
        zustaendigkeitIn: SucheComponent.extractId(
          formValue.anlage.zustaendigkeitIn
        ),
        behoerdeIn: SucheComponent.extractId(formValue.anlage.behoerdeIn),
        vorschriftenIn: SucheComponent.extractId(
          formValue.anlage.vorschriftenIn
        ),
        statusIn: SucheComponent.extractId(formValue.anlage.statusIn),
        taetigkeiten: {
          nr4BImSchV: SucheComponent.extractId(
            formValue.anlage.taetigkeiten.nr4BImSchV
          ),
          prtr: SucheComponent.extractId(formValue.anlage.taetigkeiten.prtr),
          ierl: SucheComponent.extractId(formValue.anlage.taetigkeiten.ierl),
        },
      },
      betreiber: formValue.betreiber,
    });
  }
}
