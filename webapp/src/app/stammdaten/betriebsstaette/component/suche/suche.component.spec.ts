import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SucheComponent, SucheFormData } from './suche.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ReferenzlisteEnum, ReferenzDto, Suchattribute } from '@api/stammdaten';
import { MockComponent, MockModule, MockProvider } from 'ng-mocks';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import spyOn = jest.spyOn;

describe('SucheComponent', () => {
  let component: SucheComponent;
  let fixture: ComponentFixture<SucheComponent>;
  let routeParams: { berichtsjahr: string; land: string };
  let selectedBerichtsjahr$: BehaviorSubject<ReferenzDto>;

  const emptySuchForm: SucheFormData = {
    betriebsstaette: {
      land: null,
      name: null,
      nummer: null,
      localId: null,
      akz: null,
      behoerdeIn: null,
      gemeindeIn: null,
      naceIn: null,
      ort: null,
      plz: null,
      statusIn: null,
      uebernommen: null,
    },
    betreiber: {
      name: null,
      ort: null,
      plz: null,
    },
    anlage: {
      searchAnlagenteile: null,
      name: null,
      nummer: null,
      localId: null,
      vorschriftenIn: null,
      zustaendigkeitIn: null,
      behoerdeIn: null,
      statusIn: null,
      uebernommen: null,
      taetigkeiten: {
        nr4BImSchV: null,
        prtr: null,
        ierl: null,
      },
    },
  };

  beforeEach(async () => {
    routeParams = { berichtsjahr: '2018', land: '00' };
    selectedBerichtsjahr$ = new BehaviorSubject({ id: 2018 } as ReferenzDto);
    await TestBed.configureTestingModule({
      declarations: [SucheComponent, MockComponent(TooltipComponent)],
      providers: [
        MockProvider(ReferenzdatenService),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr$(): Observable<ReferenzDto> {
            return selectedBerichtsjahr$.asObservable();
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { params: routeParams },
          },
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SucheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have initial state', () => {
    expect(component.suchForm.value).toEqual(emptySuchForm);
  });

  describe('suchParameter', () => {
    it('should map empty', () => {
      const suchParameterListe = component.suchParameter({
        anlage: {},
        betriebsstaette: {},
        betreiber: {},
      });

      expect(suchParameterListe).toEqual([]);
    });

    it('should ignore toggle and taetigkeiten', () => {
      const suchParameterListe = component.suchParameter({
        anlage: { searchAnlagenteile: true, taetigkeiten: { nr4BImSchV: [1] } },
        betriebsstaette: {},
        betreiber: {},
      });

      expect(suchParameterListe).toEqual([]);
    });

    it('should map one value each', () => {
      const suchParameterListe = component.suchParameter({
        anlage: { name: 'Anlage Name' },
        betriebsstaette: { nummer: 'Betriebsstätte Nummer' },
        betreiber: { ort: 'Betreiber Ort' },
      });

      expect(suchParameterListe).toEqual([
        ['name', 'Anlage Name'],
        ['nummer', 'Betriebsstätte Nummer'],
        ['ort', 'Betreiber Ort'],
      ]);
    });
  });

  describe('after filling form', () => {
    beforeEach(() => {
      const zustaendigkeiten = [1, 2].map((id) => ({
        id,
        schluessel: 'Z' + id,
      })) as Required<ReferenzDto>[];
      const behoerden = [3, 4].map((id) => ({
        id,
        schluessel: 'B' + id,
      })) as BehoerdeListItem[];
      const vorschriften = [5, 6].map((id) => ({
        id,
        schluessel: 'V' + id,
      })) as Required<ReferenzDto>[];
      const status = [7, 8].map((id) => ({
        id,
        schluessel: 'S' + id,
      })) as Required<ReferenzDto>[];
      const naceCodes = [9, 10].map((id) => ({
        id,
        schluessel: 'N' + id,
      })) as Required<ReferenzDto>[];
      const gemeinden = [11, 12].map((id) => ({
        id,
        schluessel: 'G' + id,
      })) as Required<ReferenzDto>[];
      const taetigkeiten = [13, 14].map((id) => ({
        id,
        schluessel: 'T' + id,
      })) as Required<ReferenzDto>[];
      component.zustaendigkeiten.values$.next(zustaendigkeiten);
      component.behoerden$.next(behoerden);
      component.vorschriften.values$.next(vorschriften);
      component.status.values$.next(status);
      component.naceCodes.values$.next(naceCodes);
      component.gemeinden.values$.next(gemeinden);
      component.taetigkeiten4bv.values$.next(taetigkeiten);
      component.taetigkeitenPrtr.values$.next(taetigkeiten);
      component.taetigkeitenIeRl.values$.next(taetigkeiten);
      component.suchForm.setValue({
        betriebsstaette: {
          land: '01',
          name: 'Name123',
          nummer: 'Nummer123',
          localId: 'localID123',
          akz: 'AKZ123',
          behoerdeIn: [...behoerden],
          gemeindeIn: [...gemeinden],
          naceIn: [...naceCodes],
          ort: 'Ort123',
          plz: 'Plz123',
          statusIn: [...status],
          uebernommen: false,
        },
        anlage: {
          searchAnlagenteile: true,
          name: 'Bez123',
          nummer: 'AnlNr123',
          localId: 'localID456',
          zustaendigkeitIn: null,
          behoerdeIn: [behoerden[0]],
          statusIn: null,
          taetigkeiten: {
            ierl: [...taetigkeiten],
            nr4BImSchV: [taetigkeiten[0]],
            prtr: null,
          },
          uebernommen: true,
          vorschriftenIn: [vorschriften[1]],
        },
        betreiber: { name: 'Betr123', ort: 'Ort123', plz: 'Plz123' },
      });
      spyOn(component.searchChanged, 'emit');
    });

    it('should emit formValue on search', () => {
      component.submitSearch();

      expect(component.searchChanged.emit).toHaveBeenCalledWith<
        Suchattribute[]
      >({
        betriebsstaette: {
          land: '01',
          name: 'Name123',
          nummer: 'Nummer123',
          localId: 'localID123',
          akz: 'AKZ123',
          behoerdeIn: [3, 4],
          gemeindeIn: [11, 12],
          naceIn: [9, 10],
          ort: 'Ort123',
          plz: 'Plz123',
          statusIn: [7, 8],
          uebernommen: false,
        },
        betreiber: {
          name: 'Betr123',
          ort: 'Ort123',
          plz: 'Plz123',
        },
        anlage: {
          searchAnlagenteile: true,
          name: 'Bez123',
          nummer: 'AnlNr123',
          localId: 'localID456',
          zustaendigkeitIn: null,
          behoerdeIn: [3],
          statusIn: null,
          taetigkeiten: { ierl: [13, 14], nr4BImSchV: [13], prtr: null },
          uebernommen: true,
          vorschriftenIn: [6],
        },
      });
    });

    it('should emit empty formValue on reset', () => {
      component.resetForm();

      expect(component.searchChanged.emit).toHaveBeenCalledWith(emptySuchForm);
    });
  });

  describe('should load behoerdenliste', () => {
    let behoerdenListe$: Subject<Array<BehoerdeListItem>>;
    beforeEach(inject(
      [ReferenzdatenService],
      (referenzenService: ReferenzdatenService) => {
        behoerdenListe$ = new Subject<Array<BehoerdeListItem>>();
        spyOn(referenzenService, 'readBehoerden').mockReturnValue(
          behoerdenListe$.asObservable()
        );
        component.behoerdenSearch$.next();
      }
    ));
    it('should fetch', inject(
      [ReferenzdatenService],
      (referenzenService: ReferenzdatenService) => {
        expect(referenzenService.readBehoerden).toHaveBeenCalledWith(
          routeParams.berichtsjahr
        );
      }
    ));

    it('should emit result on complete', () => {
      const result: Array<BehoerdeListItem> = [
        {
          id: 123,
          land: '02',
          ktext: '01 - ktext1',
          schluessel: '01',
          bezeichnung: 'ktext1',
          gueltigVon: 1999,
          gueltigBis: 2010,
        },
        {
          id: 124,
          land: '02',
          ktext: '02 - ktext2',
          schluessel: '02',
          bezeichnung: 'ktext2',
          gueltigVon: 1999,
          gueltigBis: 2010,
        },
      ];
      const subscriber = jest.fn();
      component.behoerden$.subscribe(subscriber);
      behoerdenListe$.next(result);
      expect(subscriber).toHaveBeenCalledWith(result);
    });
  });

  describe.each([
    ReferenzlisteEnum.RZUST,
    ReferenzlisteEnum.RGMD,
    ReferenzlisteEnum.RNACE,
    ReferenzlisteEnum.RBETZ,
    ReferenzlisteEnum.RVORS,
    ReferenzlisteEnum.R4BV,
    ReferenzlisteEnum.RPRTR,
    ReferenzlisteEnum.RIET,
  ])('should load referenzliste %s', (referenzliste) => {
    let referenzliste$: Subject<Array<ReferenzDto>>;
    beforeEach(inject(
      [ReferenzdatenService],
      (referenzenService: ReferenzdatenService) => {
        referenzliste$ = new Subject<Array<ReferenzDto>>();
        spyOn(referenzenService, 'readReferenzliste').mockReturnValue(
          referenzliste$.asObservable()
        );
        component.getReferenzliste(referenzliste).search$.next();
      }
    ));

    it('should fetch', inject(
      [ReferenzdatenService],
      (referenzenService: ReferenzdatenService) => {
        expect(referenzenService.readReferenzliste).toHaveBeenCalledWith(
          referenzliste,
          '2018',
          '00'
        );
      }
    ));

    it('should emit result on complete', () => {
      const result = [
        { schluessel: '01', ktext: 'ktext1' },
        { schluessel: '02', ktext: 'ktext2' },
      ] as Array<ReferenzDto>;
      const subscriber = jest.fn();
      component.getReferenzliste(referenzliste).values$.subscribe(subscriber);
      referenzliste$.next(result);

      expect(subscriber).toHaveBeenCalledWith(result);
    });
  });
});
