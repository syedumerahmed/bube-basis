import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { BetriebsstaetteDetailsComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaette-details/betriebsstaette-details.component';
import { BetreiberComponent } from '@/stammdaten/betriebsstaette/component/betreiber/betreiber.component';

@Component({
  selector: 'app-betriebsstaette-main',
  templateUrl: './betriebsstaette-main.component.html',
  styleUrls: ['./betriebsstaette-main.component.scss'],
})
export class BetriebsstaetteMainComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(BetriebsstaetteDetailsComponent, { static: true })
  betriebsstaetteDetailsComponent: BetriebsstaetteDetailsComponent;

  @ViewChild(BetreiberComponent, { static: true })
  betreiberComponent: BetreiberComponent;

  quellenDisplay: boolean;

  constructor(
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly quellenZuordungService: QuellenZuordungService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly betriebsstaettenRestControllerService: BetriebsstaettenRestControllerService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      const betriebsstaette: BetriebsstaetteDto = data.betriebsstaette;
      this.quellenZuordungService
        .quellenSindAnBetriebsstaetten(
          betriebsstaette.berichtsjahr.schluessel,
          betriebsstaette.land
        )
        .subscribe(
          (ret) => {
            this.quellenDisplay = ret;
          },
          (error) => this.alertService.setAlert(error)
        );
      this.betriebsstaettenRestControllerService
        .getAllYearsForBetriebsstaette(betriebsstaette.id)
        .subscribe((jahre) => {
          this.berichtsjahrService.setFilter((jahr: ReferenzDto) => {
            return jahre.map((j) => j.schluessel).includes(jahr.schluessel);
          });
        });
    });
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.resetFilter();
  }

  navigateBackToUebersicht(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent.parent });
  }

  canDeactivate(): boolean {
    return (
      this.betriebsstaetteDetailsComponent.canDeactivate() &&
      this.betreiberComponent.canDeactivate()
    );
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
