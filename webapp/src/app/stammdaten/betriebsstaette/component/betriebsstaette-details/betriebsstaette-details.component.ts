import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { DatePipe } from '@angular/common';
import { FormUtil } from '@/base/forms/form-util';
import {
  AnsprechpartnerComponent,
  AnsprechpartnerFormData,
} from '@/stammdaten/betriebsstaette/component/ansprechpartner/ansprechpartner.component';
import {
  BetriebsstaetteAllgemeinComponent,
  BetriebsstaetteAllgemeinFormData,
} from '@/stammdaten/betriebsstaette/component/betriebsstaette-allgemein/betriebsstaette-allgemein.component';
import {
  AdresseComponent,
  AdresseFormData,
} from '@/basedomain/component/adresse/adresse.component';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  ZustaendigeBehoerdenComponent,
  ZustaendigeBehoerdenFormData,
} from '@/stammdaten/anlage/component/zustaendige-behoerden/zustaendige-behoerden.component';
import {
  KommunikationComponent,
  KommunikationsverbindungFormData,
} from '@/basedomain/component/kommunikation/kommunikation.component';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties, lookupLand } from '@/base/model/Land';
import {
  angleIcon,
  buildingIcon,
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  lockIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface BetriebsstaetteFormData {
  allgemein: BetriebsstaetteAllgemeinFormData;
  adresse: AdresseFormData;
  kommunikation: Array<KommunikationsverbindungFormData>;
  ansprechpartner: Array<AnsprechpartnerFormData>;
  zustaendigeBehoerden: Array<ZustaendigeBehoerdenFormData>;
}

ClarityIcons.addIcons(
  lockIcon,
  angleIcon,
  buildingIcon,
  timesIcon,
  floppyIcon,
  errorStandardIcon
);

@Component({
  selector: 'app-betriebsstaette-details',
  templateUrl: './betriebsstaette-details.component.html',
  styleUrls: ['./betriebsstaette-details.component.scss'],
  providers: [DatePipe],
})
export class BetriebsstaetteDetailsComponent implements OnInit, UnsavedChanges {
  @ViewChild(BetriebsstaetteAllgemeinComponent)
  allgemeinComponent: BetriebsstaetteAllgemeinComponent;

  @ViewChild(AdresseComponent)
  adresseComponent: AdresseComponent;

  betriebsstaette: BetriebsstaetteDto;
  betriebsstaetteForm: FormGroup<BetriebsstaetteFormData>;
  uebernahmeAbschliessenDialogOpen = false;

  saveAttempt = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  gemeindekennziffern: Referenzliste<Required<ReferenzDto>>;
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;
  flusseinzugsgebiete: Referenzliste<Required<ReferenzDto>>;
  naceCodes: Referenzliste<Required<ReferenzDto>>;
  isoCodes: Referenzliste<Required<ReferenzDto>>;
  kommunikationsTypen: Referenzliste<Required<ReferenzDto>>;
  funktionen: Referenzliste<Required<ReferenzDto>>;
  epsgCodes: Referenzliste<Required<ReferenzDto>>;
  behoerden: Referenzliste<BehoerdeListItem>;
  zustaendigkeiten: Referenzliste<Required<ReferenzDto>>;

  private mapToForm(
    betriebsstaetteDto: BetriebsstaetteDto
  ): BetriebsstaetteFormData {
    return {
      allgemein: BetriebsstaetteAllgemeinComponent.toForm(
        betriebsstaetteDto,
        this.behoerden,
        this.vertraulichkeitsgruende,
        this.vorschriften,
        this.flusseinzugsgebiete
      ),
      adresse: AdresseComponent.toForm(
        betriebsstaetteDto.adresse,
        this.isoCodes,
        this.vertraulichkeitsgruende
      ),
      kommunikation: KommunikationComponent.toForm(
        betriebsstaetteDto.kommunikationsverbindungen,
        this.kommunikationsTypen
      ),
      ansprechpartner: AnsprechpartnerComponent.toForm(
        betriebsstaetteDto.ansprechpartner,
        this.kommunikationsTypen
      ),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.toForm(
        betriebsstaetteDto.zustaendigeBehoerden,
        this.behoerden
      ),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly betriebsstaettenService: BetriebsstaettenRestControllerService,
    private readonly uebernahmeService: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaette = data.betriebsstaette;
      this.vorschriften = new Referenzliste(data.vorschriftenListe);
      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.vertraulichkeitsgruende = new Referenzliste(
        data.vertraulichkeitsgrundListe
      );
      this.gemeindekennziffern = new Referenzliste(
        data.gemeindekennzifferListe
      );
      this.naceCodes = new Referenzliste(data.naceCodeListe);
      this.flusseinzugsgebiete = new Referenzliste(
        data.flusseinzugsgebietListe
      );
      this.isoCodes = new Referenzliste(data.landIsoCodeListe);
      this.kommunikationsTypen = new Referenzliste(
        data.kommunikationsTypenListe
      );
      this.epsgCodes = new Referenzliste(data.epsgCodeListe);
      this.funktionen = new Referenzliste(data.funktionenListe);
      this.behoerden = new Referenzliste(data.behoerdenListe);
      this.zustaendigkeiten = new Referenzliste(data.zustaendigkeitenListe);

      // Bei neuen Betriebsstätten default Werte aus dem BenutzerProfil lesen
      if (this.isNeu) {
        const behoerdenArray = this.behoerden.values;
        this.betriebsstaette.zustaendigeBehoerde =
          behoerdenArray.find((b) =>
            this.benutzerProfil.zustaendigeBehoerden.includes(b.schluessel)
          ) || behoerdenArray[0];
      }

      this.reset();
    });
    this.betriebsstaetteForm = this.createBetriebsstaetteForm(
      this.mapToForm(this.betriebsstaette)
    );
    if (!this.canWrite) {
      this.betriebsstaetteForm.disable();
    }
  }

  formToBetriebsstaette(): BetriebsstaetteDto {
    // RawValue gibt auch die disabled Values mit (in diesem Fall für "adresse.ort" benötigt)
    const formData = this.betriebsstaetteForm.getRawValue();
    return {
      ...this.betriebsstaette,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      vorschriften: formData.allgemein.vorschriften,
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      flusseinzugsgebiet: formData.allgemein.flusseinzugsgebiet,
      gemeindekennziffer: formData.allgemein.gemeindekennziffer || null,
      naceCode: formData.allgemein.naceCode || null,
      abfallerzeugerNummern: formData.allgemein.abfallerzeugerNummern,
      einleiterNummern: formData.allgemein.einleiterNummern,
      adresse: FormUtil.emptyStringsToNull(formData.adresse),
      geoPunkt: formData.allgemein.geoPunkt.epsgCode
        ? formData.allgemein.geoPunkt
        : null,
      zustaendigeBehoerde: formData.allgemein.zustaendigeBehoerde,
      kommunikationsverbindungen: formData.kommunikation,
      ansprechpartner: formData.ansprechpartner,
      zustaendigeBehoerden: formData.zustaendigeBehoerden,
    };
  }

  save(): void {
    this.saveAttempt = true;
    if (this.betriebsstaetteForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.adresseComponent?.markAllAsTouched();
      return;
    }

    const betriebsstaetteDto: BetriebsstaetteDto = this.formToBetriebsstaette();

    if (this.isNeu) {
      this.createBetriebsstaette(betriebsstaetteDto);
    } else {
      this.updateBetriebsstaette(betriebsstaetteDto);
    }
  }

  updateBetriebsstaette(betriebsstaetteDto: BetriebsstaetteDto): void {
    this.betriebsstaettenService
      .updateBetriebsstaette(betriebsstaetteDto)
      .subscribe(
        (betriebsstaette) => {
          this.betriebsstaette = betriebsstaette;
          this.reset();
          this.updateUrlAfterChange();
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  createBetriebsstaette(betriebsstaetteDto: BetriebsstaetteDto): void {
    this.betriebsstaettenService
      .createBetriebsstaette(betriebsstaetteDto)
      .subscribe(
        (betriebsstaette) => {
          this.betriebsstaette = betriebsstaette;
          this.reset();
          this.updateUrlAfterCreate();
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  reset(): void {
    if (this.betriebsstaetteForm) {
      this.saveAttempt = false;
      const originalBetriebsstaette = this.mapToForm(this.betriebsstaette);
      this.betriebsstaetteForm.reset(originalBetriebsstaette);
      this.betriebsstaetteForm.setControl(
        'adresse',
        AdresseComponent.createForm(
          this.fb,
          originalBetriebsstaette.adresse,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.betriebsstaetteForm.setControl(
        'kommunikation',
        KommunikationComponent.createForm(
          this.fb,
          originalBetriebsstaette.kommunikation,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.betriebsstaetteForm.setControl(
        'ansprechpartner',
        AnsprechpartnerComponent.createForm(
          this.fb,
          originalBetriebsstaette.ansprechpartner,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.betriebsstaetteForm.setControl(
        'zustaendigeBehoerden',
        ZustaendigeBehoerdenComponent.createForm(
          this.fb,
          originalBetriebsstaette.zustaendigeBehoerden,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );

      this.allgemeinForm.get('einleiterNummern').markAsPristine();
      this.allgemeinForm.get('abfallerzeugerNummern').markAsPristine();
      this.adresseForm.get('ort').disable();
    }
  }

  get isNeu(): boolean {
    return this.betriebsstaette.id == null;
  }

  get displayLand(): string {
    return LandProperties[lookupLand(this.betriebsstaette.land)].display;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get allgemeinForm(): FormGroup<BetriebsstaetteAllgemeinFormData> {
    return this.betriebsstaetteForm.get(
      'allgemein'
    ) as FormGroup<BetriebsstaetteAllgemeinFormData>;
  }

  get adresseForm(): FormGroup<AdresseFormData> {
    return this.betriebsstaetteForm.get(
      'adresse'
    ) as FormGroup<AdresseFormData>;
  }

  get kommunikationForm(): FormArray<KommunikationsverbindungFormData> {
    return this.betriebsstaetteForm.get(
      'kommunikation'
    ) as FormArray<KommunikationsverbindungFormData>;
  }

  get ansprechpartnerForm(): FormArray<AnsprechpartnerFormData> {
    return this.betriebsstaetteForm.get(
      'ansprechpartner'
    ) as FormArray<AnsprechpartnerFormData>;
  }

  get zustaendigeBehoerdenForm(): FormArray<ZustaendigeBehoerdenFormData> {
    return this.betriebsstaetteForm.get(
      'zustaendigeBehoerden'
    ) as FormArray<ZustaendigeBehoerdenFormData>;
  }

  private createBetriebsstaetteForm(
    initialBetriebsstaette: BetriebsstaetteFormData
  ): FormGroup<BetriebsstaetteFormData> {
    const form = this.fb.group<BetriebsstaetteFormData>({
      allgemein: BetriebsstaetteAllgemeinComponent.createForm(
        this.fb,
        initialBetriebsstaette.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      adresse: AdresseComponent.createForm(
        this.fb,
        initialBetriebsstaette.adresse,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      kommunikation: KommunikationComponent.createForm(
        this.fb,
        initialBetriebsstaette.kommunikation,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      ansprechpartner: AnsprechpartnerComponent.createForm(
        this.fb,
        initialBetriebsstaette.ansprechpartner,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.createForm(
        this.fb,
        initialBetriebsstaette.zustaendigeBehoerden,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
    form.controls.allgemein
      .get('gemeindekennziffer')
      .valueChanges.subscribe((gemeindekennziffer) => {
        if (gemeindekennziffer !== form.value.allgemein.gemeindekennziffer) {
          const ort = this.getOrtFromGKZ(gemeindekennziffer);
          form.controls.adresse.get('ort').patchValue(ort);
          form.controls.adresse.get('plz').patchValue(null);
        }
      });
    form.controls.adresse.get('ort').disable();
    if (this.behoerden.length <= 1) {
      form.controls.allgemein.get('zustaendigeBehoerde').disable();
    }
    return form;
  }

  private updateUrlAfterCreate(): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(
      '/neu',
      '/nummer/' + encodeURI(this.betriebsstaette.betriebsstaetteNr)
    );
    this.router.navigateByUrl(url, { replaceUrl: true });
  }

  private updateUrlAfterChange(): void {
    let url = this.router
      .createUrlTree([], {
        relativeTo: this.route,
        queryParams: { localId: this.betriebsstaette.localId },
      })
      .toString();
    url = url.replace(
      '/nummer/' + this.route.snapshot.params.nummer,
      '/nummer/' + encodeURI(this.betriebsstaette.betriebsstaetteNr)
    );
    this.router.navigateByUrl(url, { replaceUrl: true });
  }

  betreiberdatenUebernehmen(): void {
    const aktuellesJahr =
      this.berichtsjahrService.selectedBerichtsjahr.schluessel;
    this.router.navigate([
      '/stammdaten/jahr/' +
        aktuellesJahr +
        '/uebernahme/betriebsstaette/' +
        this.betriebsstaette.id,
      {
        berichtsjahr: this.betriebsstaette.berichtsjahr.schluessel,
        land: this.betriebsstaette.land,
        betriebsstaetteNummer: this.betriebsstaette.betriebsstaetteNr,
      },
    ]);
  }

  betreiberdatenSperren(): void {
    this.betriebsstaettenService
      .schreibsperreBetreiberdaten(this.betriebsstaette.id, true)
      .subscribe(
        () => {
          this.betriebsstaette.schreibsperreBetreiber = true;
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  betreiberdatenEntsperren(): void {
    this.betriebsstaettenService
      .schreibsperreBetreiberdaten(this.betriebsstaette.id, false)
      .subscribe(
        () => {
          this.betriebsstaette.schreibsperreBetreiber = false;
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  abschliessen(): void {
    this.uebernahmeService
      .uebernahmeAbschliessen(this.betriebsstaette.id)
      .subscribe(
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        (betriebsstaette) => {
          this.uebernahmeAbschliessenDialogOpen = false;
          this.betriebsstaette = betriebsstaette;
          this.reset();
        },
        () => {
          this.alertService.setAlert(
            'Beim Abschließen der Übernahme ist ein Fehler aufgetreten'
          );
          this.uebernahmeAbschliessenDialogOpen = false;
        }
      );
  }

  canDeactivate(): boolean {
    return this.betriebsstaetteForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }

  getOrtFromGKZ(gkz: ReferenzDto): string {
    return gkz?.ktext.replace(`${gkz.schluessel} - `, '');
  }
}
