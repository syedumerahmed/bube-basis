import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { AdresseFormData } from '@/basedomain/component/adresse/adresse.component';
import { AnsprechpartnerComponent } from '../ansprechpartner/ansprechpartner.component';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockService,
} from 'ng-mocks';
import { of, Subject } from 'rxjs';
import { BetriebsstaetteDetailsComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaette-details/betriebsstaette-details.component';
import { BetriebsstaetteAllgemeinComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaette-allgemein/betriebsstaette-allgemein.component';
import { ZustaendigeBehoerdenComponent } from '@/stammdaten/anlage/component/zustaendige-behoerden/zustaendige-behoerden.component';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('BetriebsstaetteDetailsComponent', () => {
  let component: BetriebsstaetteDetailsComponent;
  let fixture: ComponentFixture<BetriebsstaetteDetailsComponent>;

  const betriebsstatusListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vorschriftenListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vertraulichkeitsgrundListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const flusseinzugsgebietListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const naceCodeListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const behoerdenListe: Array<BehoerdeListItem> = [
    { id: 1, schluessel: '01' },
  ] as Array<BehoerdeListItem>;
  const zustaendigkeitenListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const gemeindekennzifferListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01', ktext: 'Hamburg' },
    { id: 2, schluessel: '02', ktext: 'Lüneburg' },
    { id: 3, schluessel: '03', ktext: 'Celle' },
  ] as Array<ReferenzDto>;

  const betriebsstaette: BetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    name: '',
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '01',
    zustaendigeBehoerde: { schluessel: '11' } as BehoerdeListItem,
    gemeindekennziffer: undefined,
    abfallerzeugerNummern: [],
    einleiterNummern: [],
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(BasedomainModule),
        MockModule(BaseModule),
        RouterTestingModule,
      ],
      declarations: [
        BetriebsstaetteDetailsComponent,
        BetriebsstaetteAllgemeinComponent,
        MockComponents(AnsprechpartnerComponent, ZustaendigeBehoerdenComponent),
      ],
      providers: [
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        MockProvider(StammdatenUebernahmeRestControllerService),
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: { nummer: 'nummer' },
              data: { schreibRecht: true },
            },
            data: of({
              betriebsstaette,
              behoerdenListe,
              betriebsstatusListe,
              gemeindekennzifferListe,
              vorschriftenListe,
              vertraulichkeitsgrundListe,
              flusseinzugsgebietListe,
              naceCodeListe,
              zustaendigkeitenListe,
              landIsoCodeListe: [],
              kommunikationsTypenListe: [],
              epsgCodeListe: [],
              funktionenListe: [],
            }),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BetriebsstaetteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should have initial state', () => {
    const form = component.betriebsstaetteForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [ActivatedRoute],
    (route: ActivatedRoute) => {
      route.snapshot.data.schreibRecht = false;
      component.ngOnInit();
      expect(component.betriebsstaetteForm.enabled).toBe(false);
    }
  ));

  it('should disable ort', () => {
    expect(
      component.betriebsstaetteForm.controls.adresse.get('ort').disabled
    ).toBe(true);
  });

  describe('after changing gemeinde', () => {
    beforeEach(() => {
      component.betriebsstaetteForm.controls.adresse.patchValue({
        plz: '12345',
        ort: 'Ort',
      } as AdresseFormData);
      component.betriebsstaetteForm.controls.allgemein
        .get('gemeindekennziffer')
        .patchValue(gemeindekennzifferListe[1]);
    });
    it('should set ort', () => {
      expect(
        component.betriebsstaetteForm.controls.adresse.get('ort').value
      ).toEqual(gemeindekennzifferListe[1].ktext);
    });
    it('should clear plz', () => {
      expect(
        component.betriebsstaetteForm.controls.adresse.get('plz').value
      ).toBeFalsy();
    });
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.betriebsstaetteForm.markAsDirty();
      component.betriebsstaetteForm.markAsTouched();
      component.betriebsstaetteForm.patchValue({
        allgemein: { name: 'Meine neue Betriebsstaette' },
      });
    });

    it('should be invalid', () => {
      expect(component.betriebsstaetteForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          BetriebsstaetteAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [BetriebsstaettenRestControllerService],
        (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
          expect(
            betriebsstaettenService.createBetriebsstaette
          ).not.toHaveBeenCalled();
          expect(
            betriebsstaettenService.updateBetriebsstaette
          ).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.betriebsstaetteForm.patchValue({
        allgemein: {
          name: 'Meine neue Betriebsstaette',
          betriebsstaetteNr: 'BST1234',
          betriebsstatus: betriebsstatusListe[0],
          zustaendigeBehoerde: behoerdenListe[0],
          vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
          vorschriften: vorschriftenListe.slice(),
          akz: 'akz',
          einleiterNummern: ['nr1', 'nr2'],
          abfallerzeugerNummern: ['abfall', 'erzeuger', 'nr'],
          naceCode: naceCodeListe[0],
          inbetriebnahme: '12.12.2009',
          flusseinzugsgebiet: flusseinzugsgebietListe[0],
          indirekteinleiter: true,
          direkteinleiter: false,
          betriebsstatusSeit: '12.12.2009',
          bemerkung: 'bemerkung',
          gemeindekennziffer: gemeindekennzifferListe[0],
          localId: 'local-id',
        },
        adresse: {
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
        },
      });
      component.betriebsstaetteForm.markAsDirty();
      component.betriebsstaetteForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.betriebsstaetteForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedBetriebsstaetteDto: BetriebsstaetteDto = {
        berichtsjahr: undefined,
        land: '01',
        zustaendigeBehoerde: behoerdenListe[0],
        name: 'Meine neue Betriebsstaette',
        betriebsstaetteNr: 'BST1234',
        betriebsstatus: betriebsstatusListe[0],
        vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        vorschriften: vorschriftenListe,
        akz: 'akz',
        einleiterNummern: ['nr1', 'nr2'],
        abfallerzeugerNummern: ['abfall', 'erzeuger', 'nr'],
        naceCode: naceCodeListe[0],
        inbetriebnahme: '2009-12-12',
        flusseinzugsgebiet: flusseinzugsgebietListe[0],
        indirekteinleiter: true,
        direkteinleiter: false,
        betriebsstatusSeit: '2009-12-12',
        bemerkung: 'bemerkung',
        gemeindekennziffer: gemeindekennzifferListe[0],
        localId: 'local-id',
        adresse: {
          notiz: null,
          postfachPlz: null,
          postfach: null,
          landIsoCode: null,
          vertraulichkeitsgrund: null,
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
          hausNr: null,
          strasse: null,
        },
        kommunikationsverbindungen: [],
        ansprechpartner: [],
        geoPunkt: null,
        zustaendigeBehoerden: [],
        pruefungsfehler: false,
        pruefungVorhanden: false,
      };
      let betriebsstaetteObservable: Subject<BetriebsstaetteDto>;
      beforeEach(inject(
        [BetriebsstaettenRestControllerService],
        (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
          betriebsstaetteObservable = new Subject<BetriebsstaetteDto>();
          spyOn(
            betriebsstaettenService,
            'createBetriebsstaette'
          ).mockReturnValue(betriebsstaetteObservable.asObservable() as never);
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [BetriebsstaettenRestControllerService],
        (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
          expect(
            betriebsstaettenService.createBetriebsstaette
          ).toHaveBeenCalledWith(expectedBetriebsstaetteDto);
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedBetriebsstaetteDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          betriebsstaetteObservable.next(responseDto);
          betriebsstaetteObservable.complete();
        }));

        it('should update betriebsstaette', () => {
          expect(component.betriebsstaette).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.betriebsstaetteForm.touched).toBe(false);
          expect(component.betriebsstaetteForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });
});
