import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetreiberAllgemeinComponent } from './betreiber-allgemein.component';
import { FormBuilder } from '@/base/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { MockComponent, MockModule } from 'ng-mocks';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { of } from 'rxjs';
import { BetreiberDto, ReferenzDto } from '@api/stammdaten';

const betreiber: BetreiberDto = {
  bemerkung: 'Bemerkung',
  berichtsjahr: { schluessel: '2020' } as ReferenzDto,
  betreiberNummer: 'Nr-123',
  land: '02',
  name: 'Betreiber',
  url: 'https://url.de',
  adresse: { ort: 'ort', plz: 'plz' },
  vertraulichkeitsgrund: { schluessel: '03' } as ReferenzDto,
};

describe('BetreiberAllgemeinComponent', () => {
  const vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>> =
    new Referenzliste([]);

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  let component: BetreiberAllgemeinComponent;
  let fixture: ComponentFixture<BetreiberAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        BetreiberAllgemeinComponent,
        MockComponent(ReferenzSelectComponent),
      ],
      imports: [MockModule(ReactiveFormsModule), MockModule(ClarityModule)],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              data: of({
                vertraulichkeitsgrundListe: vertraulichkeitsgruende.values,
              }),
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(BetreiberAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = BetreiberAllgemeinComponent.createForm(
      formBuilder,
      BetreiberAllgemeinComponent.toForm(betreiber, vertraulichkeitsgruende),
      currentBerichtsjahr
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
