import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BetreiberDto, ReferenzDto } from '@api/stammdaten';

export interface BetreiberAllgemeinFormData {
  name: string;
  url: string;
  vertraulichkeitsgrund: Required<ReferenzDto>;
  bemerkung: string;
}

@Component({
  selector: 'app-betreiber-allgemein',
  templateUrl: './betreiber-allgemein.component.html',
  styleUrls: ['./betreiber-allgemein.component.scss'],
})
export class BetreiberAllgemeinComponent
  implements OnInit, AfterViewChecked, AfterViewInit
{
  constructor(private readonly route: ActivatedRoute) {}

  @Input()
  form: FormGroup<BetreiberAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;

  static toForm(
    betreiberDto: BetreiberDto,
    vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>
  ): BetreiberAllgemeinFormData {
    return {
      name: betreiberDto.name,
      url: betreiberDto.url,
      vertraulichkeitsgrund: vertraulichkeitsgruende.find(
        betreiberDto.vertraulichkeitsgrund as Required<ReferenzDto>
      ),
      bemerkung: betreiberDto.bemerkung,
    };
  }

  static createForm(
    fb: FormBuilder,
    betreiber: BetreiberAllgemeinFormData,
    selectedBerichtsjahr: ReferenzDto
  ): FormGroup<BetreiberAllgemeinFormData> {
    return fb.group({
      name: [betreiber.name, [Validators.required, Validators.maxLength(250)]],
      vertraulichkeitsgrund: [
        betreiber.vertraulichkeitsgrund,
        BubeValidators.isReferenceValid(
          Number(selectedBerichtsjahr.schluessel)
        ),
      ],
      url: [betreiber.url, [BubeValidators.url, Validators.maxLength(500)]],
      bemerkung: [betreiber.bemerkung, Validators.maxLength(1000)],
    });
  }

  ngOnInit(): void {
    this.route.parent.data.subscribe((data) => {
      this.vertraulichkeitsgruende = new Referenzliste(
        data.vertraulichkeitsgrundListe
      );
    });
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }
}
