import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdresseComponent } from '@/basedomain/component/adresse/adresse.component';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockProviders,
} from 'ng-mocks';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { of, Subject } from 'rxjs';
import { BetreiberDetailsComponent } from '@/stammdaten/betriebsstaette/component/betreiber-details/betreiber-details.component';
import { BetreiberAllgemeinComponent } from '@/stammdaten/betriebsstaette/component/betreiber-allgemein/betreiber-allgemein.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import {
  BetreiberDto,
  BetreiberRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import spyOn = jest.spyOn;

describe('BetreiberDetailsComponent', () => {
  const vertraulichkeitsgrundListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;
  const landIsoCodeListe: Array<ReferenzDto> = [
    { id: 1, schluessel: '01' },
  ] as Array<ReferenzDto>;

  const betreiber: BetreiberDto = {
    name: 'tollerName',
    betreiberNummer: '2',
    berichtsjahr: { schluessel: '2019' } as ReferenzDto,
    land: '00',
    adresse: { plz: '123', ort: 'HH' },
  };

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  let component: BetreiberDetailsComponent;
  let fixture: ComponentFixture<BetreiberDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
      ],
      declarations: [
        BetreiberDetailsComponent,
        BetreiberAllgemeinComponent,
        MockComponents(AdresseComponent, ReferenzSelectComponent),
      ],
      providers: [
        MockProviders(BetreiberRestControllerService),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return currentBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              betreiber,
              vertraulichkeitsgrundListe,
              landIsoCodeListe,
            }),
            snapshot: {
              params: { betreiberNummer: '2' },
              data: {
                schreibRecht: true,
              },
            },
            parent: { snapshot: { params: { nummer: '0' } } },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BetreiberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable berichtsjahr', inject(
    [BerichtsjahrService],
    (berichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.betreiberForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [ActivatedRoute],
    (route: ActivatedRoute) => {
      route.snapshot.data.schreibRecht = false;
      component.ngOnInit();
      expect(component.betreiberForm.enabled).toBe(false);
    }
  ));

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.betreiberForm.markAsDirty();
      component.betreiberForm.markAsTouched();
      component.betreiberForm.patchValue({
        allgemein: { name: 'Meine neue Betreiber' },
        adresse: { ort: null },
      });
    });

    it('should be invalid', () => {
      expect(component.betreiberForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          BetreiberAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [BetreiberRestControllerService],
        (betreibernService: BetreiberRestControllerService) => {
          expect(betreibernService.createBetreiber).not.toHaveBeenCalled();
          expect(betreibernService.updateBetreiber).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.betreiberForm.patchValue({
        allgemein: {
          name: 'Mein neuer Betreiber',
          vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
          bemerkung: 'bemerkung',
        },
        adresse: {
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
        },
      });
      component.betreiberForm.markAsDirty();
      component.betreiberForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.betreiberForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedBetreiberDto: BetreiberDto = {
        berichtsjahr: { schluessel: '2019' } as ReferenzDto,
        land: '00',
        betreiberNummer: '2',
        url: null,
        name: 'Mein neuer Betreiber',
        vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        bemerkung: 'bemerkung',
        adresse: {
          notiz: null,
          postfachPlz: null,
          postfach: null,
          landIsoCode: null,
          vertraulichkeitsgrund: null,
          ort: 'Ort',
          ortsteil: 'Ortsteil',
          plz: '12345',
          hausNr: null,
          strasse: null,
        },
      };
      let betreiberObservable: Subject<BetreiberDto>;
      beforeEach(inject(
        [BetreiberRestControllerService],
        (betreibernService: BetreiberRestControllerService) => {
          betreiberObservable = new Subject<BetreiberDto>();
          spyOn(betreibernService, 'createBetreiber').mockReturnValue(
            betreiberObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [BetreiberRestControllerService],
        (betreibernService: BetreiberRestControllerService) => {
          expect(betreibernService.createBetreiber).toHaveBeenCalledWith(
            '0',
            expectedBetreiberDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedBetreiberDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          betreiberObservable.next(responseDto);
          betreiberObservable.complete();
        }));

        it('should update betreiber', () => {
          expect(component.betreiber).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.betreiberForm.touched).toBe(false);
          expect(component.betreiberForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });

  describe('on destroy', () => {
    beforeEach(() => {
      component.ngOnDestroy();
    });
    it('should reset berichtsjahr default', inject(
      [BerichtsjahrService],
      (berichtsjahrService) => {
        expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
      }
    ));
  });
});
