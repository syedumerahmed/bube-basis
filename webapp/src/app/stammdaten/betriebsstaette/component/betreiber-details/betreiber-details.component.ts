import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  BetreiberDto,
  BetreiberRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  AdresseComponent,
  AdresseFormData,
} from '@/basedomain/component/adresse/adresse.component';
import {
  BetreiberAllgemeinComponent,
  BetreiberAllgemeinFormData,
} from '@/stammdaten/betriebsstaette/component/betreiber-allgemein/betreiber-allgemein.component';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  importIcon,
  timesIcon,
  userIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface BetreiberFormData {
  allgemein: BetreiberAllgemeinFormData;
  adresse: AdresseFormData;
}

ClarityIcons.addIcons(
  userIcon,
  importIcon,
  timesIcon,
  floppyIcon,
  errorStandardIcon
);

@Component({
  selector: 'app-betreiber-details',
  templateUrl: './betreiber-details.component.html',
  styleUrls: ['./betreiber-details.component.scss'],
})
export class BetreiberDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(BetreiberAllgemeinComponent)
  allgemeinComponent: BetreiberAllgemeinComponent;

  @ViewChild(AdresseComponent)
  adresseComponent: AdresseComponent;

  betreiber: BetreiberDto;
  betreiberForm: FormGroup<BetreiberFormData>;

  saveAttempt = false;

  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;
  isoCodes: Referenzliste<Required<ReferenzDto>>;

  private mapToForm(betreiberDto: BetreiberDto): BetreiberFormData {
    return {
      allgemein: BetreiberAllgemeinComponent.toForm(
        betreiberDto,
        this.vertraulichkeitsgruende
      ),
      adresse: AdresseComponent.toForm(
        betreiberDto.adresse,
        this.isoCodes,
        this.vertraulichkeitsgruende
      ),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly betreiberService: BetreiberRestControllerService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly alertService: AlertService,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {
    this.route.data.subscribe((routeData) => {
      this.vertraulichkeitsgruende = new Referenzliste(
        routeData.vertraulichkeitsgrundListe
      );
      this.isoCodes = new Referenzliste(routeData.landIsoCodeListe);
    });
  }

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.betreiber = data.betreiber;
      this.betreiberForm = this.createBetreiberForm(
        this.mapToForm(this.betreiber)
      );
      this.reset();
    });

    if (!this.canWrite) {
      this.betreiberForm.disable();
    }
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  save(): void {
    this.saveAttempt = true;
    if (this.betreiberForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.adresseComponent?.markAllAsTouched();
      return;
    }
    const formData = this.betreiberForm.value;
    const betreiberDto: BetreiberDto = {
      ...this.betreiber,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      adresse: FormUtil.emptyStringsToNull(formData.adresse),
    };

    if (this.isNeu) {
      const betriebsstaettenNummer = this.route.parent.snapshot.params.nummer;
      this.createBetreiber(betriebsstaettenNummer, betreiberDto);
    } else {
      this.updateBetreiber(betreiberDto);
    }
  }

  private createBetreiber(
    betriebsstaettenNummer,
    betreiberDto: BetreiberDto
  ): void {
    this.betreiberService
      .createBetreiber(betriebsstaettenNummer, betreiberDto)
      .subscribe(
        (betreiber) => {
          this.betreiber = betreiber;
          this.reset();
          this.updateUrlAfterCreate(betreiber.betreiberNummer);
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  private updateBetreiber(betreiberDto: BetreiberDto): void {
    this.betreiberService.updateBetreiber(betreiberDto).subscribe(
      (betreiber) => {
        this.betreiber = betreiber;
        this.reset();
        this.updateUrlAfterChange(betreiber.betreiberNummer);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  reset(): void {
    this.saveAttempt = false;
    this.betreiberForm.reset(this.mapToForm(this.betreiber));
  }

  navigateBackToBetriebsstaette(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  get isNeu(): boolean {
    return this.betreiber.id == null;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get allgemeinForm(): FormGroup<BetreiberAllgemeinFormData> {
    return this.betreiberForm.controls
      .allgemein as FormGroup<BetreiberAllgemeinFormData>;
  }

  get adresseForm(): FormGroup<AdresseFormData> {
    return this.betreiberForm.controls.adresse as FormGroup<AdresseFormData>;
  }

  private createBetreiberForm(
    initialBetreiber: BetreiberFormData
  ): FormGroup<BetreiberFormData> {
    return this.fb.group<BetreiberFormData>({
      allgemein: BetreiberAllgemeinComponent.createForm(
        this.fb,
        initialBetreiber.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      adresse: AdresseComponent.createForm(
        this.fb,
        initialBetreiber.adresse,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, '/betreiber/nummer/' + nummer);
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    this.updateUrl(nummer, '/betreiber/neu');
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      nummer,
      '/betreiber/nummer/' + this.route.snapshot.params.betreiberNummer
    );
  }

  betreiberdatenUebernehmen(): void {
    const aktuellesJahr =
      this.berichtsjahrService.selectedBerichtsjahr.schluessel;
    this.router.navigate([
      '/stammdaten/jahr/' +
        aktuellesJahr +
        '/uebernahme/betreiber/' +
        this.betreiber.id,
    ]);
  }

  canDeactivate(): boolean {
    return this.betreiberForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
