import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClrLoadingState } from '@clr/angular';
import * as fileSaver from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';
import {
  EmitEvent,
  Event,
  EventBusService,
} from '@/base/service/event-bus.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { AlertService } from '@/base/service/alert.service';
import {
  BetriebsstaettenRestControllerService,
  ExportiereStammdatenCommand,
} from '@api/stammdaten';

@Component({
  selector: 'app-stammdaten-export-modal',
  templateUrl: './stammdaten-export-modal.component.html',
  styleUrls: ['./stammdaten-export-modal.component.scss'],
})
export class StammdatenExportModalComponent {
  @Input() selectedIds: number[];
  @Input() open: boolean;
  @Output() openChange = new EventEmitter<boolean>();
  exportButtonState = ClrLoadingState.DEFAULT;

  constructor(
    private readonly eventBus: EventBusService,
    private readonly dateFormat: FilenameDateFormatPipe,
    private readonly alertService: AlertService,
    private readonly betriebsstatteService: BetriebsstaettenRestControllerService
  ) {}

  changeOpen(isOpen: boolean): void {
    this.open = isOpen;
    this.openChange.emit(isOpen);
  }

  exportMarkierte(): void {
    const exportCommand: ExportiereStammdatenCommand = {
      betriebsstaetten: this.selectedIds,
    };

    this.exportButtonState = ClrLoadingState.LOADING;
    this.betriebsstatteService.exportStammdaten(exportCommand).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(new Date());
        fileSaver.saveAs(blob, 'stammdaten_' + datum + '.xml');
        this.exportButtonState = ClrLoadingState.SUCCESS;
        this.changeOpen(false);
        this.eventBus.emit(
          new EmitEvent(
            Event.BetriebsstaettenExportFertig,
            'BetriebsstaettenExportFertig'
          )
        );
        const plural = this.selectedIds.length > 1 ? 'n' : '';
        this.alertService.setSuccess(
          `${this.selectedIds.length} Betriebsstätte${plural} wurde${plural} exportiert.`
        );
      },
      (err: HttpErrorResponse) => {
        err.error.text().then((message) => {
          this.exportButtonState = ClrLoadingState.ERROR;
          this.changeOpen(false);
          this.alertService.setAlert(
            'Es ist ein Fehler beim Exportieren der XML-Datei aufgetreten: ' +
              message
          );
        });
      }
    );
  }
}
