import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StammdatenExportModalComponent } from './stammdaten-export-modal.component';
import { MockModule, MockPipe, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { BetriebsstaettenRestControllerService } from '@api/stammdaten';

describe('StammdatenExportModalComponent', () => {
  let component: StammdatenExportModalComponent;
  let fixture: ComponentFixture<StammdatenExportModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StammdatenExportModalComponent],
      providers: [
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
        {
          provide: FilenameDateFormatPipe,
          useValue: MockPipe(FilenameDateFormatPipe),
        },
      ],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StammdatenExportModalComponent);
    component = fixture.componentInstance;
    component.open = false;
    component.selectedIds = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
