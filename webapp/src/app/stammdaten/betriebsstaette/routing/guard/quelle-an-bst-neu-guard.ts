import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';

@Injectable({
  providedIn: 'root',
})
export class QuelleAnBstNeuGuard implements CanActivate {
  constructor(
    private readonly quellenZuordungService: QuellenZuordungService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.quellenZuordungService.quellenSindAnBetriebsstaetten(
      route.params.berichtsjahr,
      route.params.land
    );
  }
}
