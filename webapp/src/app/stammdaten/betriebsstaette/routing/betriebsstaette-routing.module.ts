import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntityNotFoundComponent } from '@/stammdaten/component/entity-not-found/entity-not-found.component';
import { BetriebsstaetteComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaette/betriebsstaette.component';
import { BetriebsstaettenResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betriebsstaetten-resolver';
import { BetriebsstatusListResolver } from '@/referenzdaten/routing/resolver/betriebsstatus-list-resolver';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { GemeindeKennzifferListResolver } from '@/referenzdaten/routing/resolver/gemeinde-kennziffer-list-resolver';
import { VorschriftenListResolver } from '@/referenzdaten/routing/resolver/vorschriften-list-resolver';
import { VertraulichkeitsgrundListResolver } from '@/referenzdaten/routing/resolver/vertraulichkeitsgrund-list-resolver';
import { FlusseinzugsgebietListResolver } from '@/referenzdaten/routing/resolver/flusseinzugsgebiet-list-resolver';
import { NaceCodeListResolver } from '@/referenzdaten/routing/resolver/nace-code-list-resolver';
import { KommunikationsTypenListResolver } from '@/referenzdaten/routing/resolver/kommunikations-typen-list-resolver';
import { LandisocodeListResolver } from '@/referenzdaten/routing/resolver/landisocode-list-resolver';
import { ZustaendigkeitListResolver } from '@/referenzdaten/routing/resolver/zustaendigkeit-list-resolver';
import { EpsgListResolver } from '@/referenzdaten/routing/resolver/epsg-list-resolver';
import { FunktionListResolver } from '@/referenzdaten/routing/resolver/funktion-list-resolver';
import { BetreiberListResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betreiber-list-resolver';
import { BetriebsstaetteMainComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaette-main/betriebsstaette-main.component';
import { QuelleDetailsComponent } from '@/stammdaten/quelle/component/quelle-details/quelle-details.component';
import { BetreiberDetailsComponent } from '@/stammdaten/betriebsstaette/component/betreiber-details/betreiber-details.component';
import { AnlageModule } from '@/stammdaten/anlage/anlage.module';
import { BetriebsstaetteNeuResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betriebsstaette-neu-resolver';
import { BetreiberNeuResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betreiber-neu-resolver';
import { QuelleAnBstNeuGuard } from '@/stammdaten/betriebsstaette/routing/guard/quelle-an-bst-neu-guard';
import { BetriebsstaettenListeComponent } from '@/stammdaten/betriebsstaette/component/betriebsstaetten-liste/betriebsstaetten-liste.component';
import { BstSchreibrechtResolver } from '@/stammdaten/betriebsstaette/routing/resolver/bst-schreibrecht-resolver';
import { BstLoeschrechtResolver } from '@/stammdaten/betriebsstaette/routing/resolver/bst-loeschrecht-resolver';
import { BetreiberResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betreiber-resolver';
import { QuellenBetriebsstaetteNeuResolver } from '@/stammdaten/quelle/routing/resolver/quellen-betriebsstaette-neu-resolver';
import { QuelleKopierenResolver } from '@/stammdaten/quelle/routing/resolver/quelle-kopieren-resolver';
import { QuelleNeuUebernehmenResolver } from '@/stammdaten/quelle/routing/resolver/quelle-uebernehmen-resolver.service';
import { QuellenBetriebsstaetteResolver } from '@/stammdaten/quelle/routing/resolver/quellen-betriebsstaette-resolver';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const betriebsstaetteReferenzenResolvers = {
  betriebsstatusListe: BetriebsstatusListResolver,
  behoerdenListe: BehoerdenListResolver,
  gemeindekennzifferListe: GemeindeKennzifferListResolver,
  vorschriftenListe: VorschriftenListResolver,
  vertraulichkeitsgrundListe: VertraulichkeitsgrundListResolver,
  flusseinzugsgebietListe: FlusseinzugsgebietListResolver,
  naceCodeListe: NaceCodeListResolver,
  kommunikationsTypenListe: KommunikationsTypenListResolver,
  landIsoCodeListe: LandisocodeListResolver,
  zustaendigkeitenListe: ZustaendigkeitListResolver,
  epsgCodeListe: EpsgListResolver,
  funktionenListe: FunktionListResolver,
  betreiberListe: BetreiberListResolver,
};

const quelleReferenzenResolvers = {
  epsgCodeListe: EpsgListResolver,
};

const betreiberReferenzenResolvers = {
  vertraulichkeitsgrundListe: VertraulichkeitsgrundListResolver,
  landIsoCodeListe: LandisocodeListResolver,
};

const routes: Routes = [
  { path: '404', component: EntityNotFoundComponent },
  {
    path: '',
    component: BetriebsstaettenListeComponent,
  },
  {
    path: 'betriebsstaette/land/:land/neu',
    component: BetriebsstaetteComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      betriebsstaette: BetriebsstaetteNeuResolver,
      ...betriebsstaetteReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: BetriebsstaetteMainComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          schreibRecht: BstSchreibrechtResolver,
        },
      },
    ],
  },
  {
    path: 'betriebsstaette/land/:land/nummer/:nummer',
    component: BetriebsstaetteComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      betriebsstaette: BetriebsstaettenResolver,
      ...betriebsstaetteReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: BetriebsstaetteMainComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          schreibRecht: BstSchreibrechtResolver,
          loeschRecht: BstLoeschrechtResolver,
        },
      },
      {
        path: 'anlage',
        resolve: {
          schreibRecht: BstSchreibrechtResolver,
          loeschRecht: BstLoeschrechtResolver,
        },
        loadChildren: () => AnlageModule,
      },
      {
        path: 'quelle/neu',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnBstNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuellenBetriebsstaetteNeuResolver,
          ...quelleReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
      {
        path: 'quelle/kopieren',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnBstNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuelleKopierenResolver,
          ...quelleReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
      {
        path: 'quelle/uebernehmen',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnBstNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuelleNeuUebernehmenResolver,
          ...quelleReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
      {
        path: 'quelle/nummer/:quelleNummer',
        component: QuelleDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuellenBetriebsstaetteResolver,
          ...quelleReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
      {
        path: 'betreiber/neu',
        component: BetreiberDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          betreiber: BetreiberNeuResolver,
          ...betreiberReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
      {
        path: 'betreiber/nummer/:betreiberNummer',
        component: BetreiberDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          betreiber: BetreiberResolver,
          ...betreiberReferenzenResolvers,
          schreibRecht: BstSchreibrechtResolver,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    BetreiberListResolver,
    BetreiberResolver,
    BetreiberNeuResolver,
    BetriebsstaettenResolver,
    BetriebsstaetteNeuResolver,
    QuellenBetriebsstaetteResolver,
    QuellenBetriebsstaetteNeuResolver,
    QuelleKopierenResolver,
    QuelleNeuUebernehmenResolver,
    BstSchreibrechtResolver,
    BstLoeschrechtResolver,
  ],
})
export class BetriebsstaetteRoutingModule {}
