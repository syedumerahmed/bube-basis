import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { catchError } from 'rxjs/operators';
import { BetriebsstaettenRestControllerService } from '@api/stammdaten';

@Injectable()
export class BstSchreibrechtResolver implements Resolve<boolean> {
  constructor(
    private readonly service: BetriebsstaettenRestControllerService,
    private readonly alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
    const id = route.parent.data.betriebsstaette.id;
    if (!id) {
      return of(true);
    }

    return this.service.canWriteBst(id).pipe(
      catchError(() => {
        this.alertService.setWarning(
          'Fehler beim Abfragen der Berechtigung. Die Anwendung kann dennoch weiterhin verwendet werden.'
        );
        return of(true);
      })
    );
  }
}
