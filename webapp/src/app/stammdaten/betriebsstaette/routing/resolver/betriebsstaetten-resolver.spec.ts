import { Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import { inject, TestBed } from '@angular/core/testing';
import { MockService } from 'ng-mocks';
import { BetriebsstaettenResolver } from '@/stammdaten/betriebsstaette/routing/resolver/betriebsstaetten-resolver';
import { EMPTY, throwError } from 'rxjs';
import spyOn = jest.spyOn;
import { BetriebsstaettenRestControllerService } from '@api/stammdaten';

describe('BetriebsstaettenResolver', () => {
  let resolver: BetriebsstaettenResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BetriebsstaettenResolver,
        {
          provide: BetriebsstaettenRestControllerService,
          useValue: MockService(BetriebsstaettenRestControllerService),
        },
        { provide: Router, useValue: MockService(Router) },
        { provide: Location, useValue: MockService(Location) },
        { provide: AlertService, useValue: MockService(AlertService) },
      ],
    });
    resolver = TestBed.inject(BetriebsstaettenResolver);
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { nummer: 'neu', land: '01', berichtsjahr: '2021' },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [BetriebsstaettenRestControllerService],
      (betriebsstaettenService: BetriebsstaettenRestControllerService) => {
        jest
          .spyOn(betriebsstaettenService, 'readBetriebsstaetteByJahrLandNummer')
          .mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot, null);

        expect(
          betriebsstaettenService.readBetriebsstaetteByJahrLandNummer
        ).toHaveBeenCalledWith('2021', '01', 'neu');
        expect(
          betriebsstaettenService.readBetriebsstaetteByJahrLandLocalId
        ).not.toHaveBeenCalled();
      }
    ));

    it('should load from server using localId and fallback to nr', inject(
      [BetriebsstaettenRestControllerService],
      async (
        betriebsstaettenService: BetriebsstaettenRestControllerService
      ) => {
        activatedRouteSnapshot.queryParams.localId = 'localId';
        spyOn(
          betriebsstaettenService,
          'readBetriebsstaetteByJahrLandLocalId'
        ).mockReturnValue(throwError({ status: 404 }));
        spyOn(
          betriebsstaettenService,
          'readBetriebsstaetteByJahrLandNummer'
        ).mockReturnValue(EMPTY);

        await resolver.resolve(activatedRouteSnapshot, null).toPromise();

        expect(
          betriebsstaettenService.readBetriebsstaetteByJahrLandLocalId
        ).toHaveBeenCalledWith('2021', '01', 'localId');
        expect(
          betriebsstaettenService.readBetriebsstaetteByJahrLandNummer
        ).toHaveBeenCalledWith('2021', '01', 'neu');
      }
    ));
  });
});
