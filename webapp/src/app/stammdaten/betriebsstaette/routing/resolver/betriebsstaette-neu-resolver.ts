import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BetriebsstaetteDto } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable()
export class BetriebsstaetteNeuResolver implements Resolve<BetriebsstaetteDto> {
  constructor(
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly benutzerProfil: BenutzerProfil
  ) {}

  resolve(): Observable<BetriebsstaetteDto> {
    // Bei neuen Betriebsstätten default Werte aus dem BenutzerProfil lesen
    return of({
      berichtsjahr: this.berichtsjahrService.selectedBerichtsjahr,
      betriebsstatus: null,
      betreiberId: null,
      name: null,
      betriebsstaetteNr: null,
      land: this.benutzerProfil.landNr,
      zustaendigeBehoerde: null,
      adresse: null,
      vertraulichkeitsgrund: null,
      flusseinzugsgebiet: null,
      naceCode: null,
      gemeindekennziffer: null,
      ansprechpartner: [],
      einleiterNummern: [],
      abfallerzeugerNummern: [],
      pruefungsfehler: false,
      pruefungVorhanden: false,
    });
  }
}
