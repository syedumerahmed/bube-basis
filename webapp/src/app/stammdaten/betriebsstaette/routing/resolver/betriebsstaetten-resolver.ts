import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import {
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';

@Injectable()
export class BetriebsstaettenResolver implements Resolve<BetriebsstaetteDto> {
  constructor(
    private readonly service: BetriebsstaettenRestControllerService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<BetriebsstaetteDto> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const localId = route.queryParams.localId;
    const nummer = route.params.nummer;

    let betriebsstaette: Observable<BetriebsstaetteDto>;
    if (localId) {
      betriebsstaette = this.service
        .readBetriebsstaetteByJahrLandLocalId(
          berichtsjahr,
          landSchluessel,
          localId
        )
        .pipe(
          catchError((err: HttpErrorResponse) => {
            if (err.status === 404) {
              // localId existiert nicht für Jahr/Land -> Betriebsstättennummer verwenden
              return this.service.readBetriebsstaetteByJahrLandNummer(
                berichtsjahr,
                landSchluessel,
                nummer
              );
            }
            this.alertService.setAlert(err);
            return EMPTY;
          })
        );
    } else {
      betriebsstaette = this.service.readBetriebsstaetteByJahrLandNummer(
        berichtsjahr,
        landSchluessel,
        nummer
      );
    }
    return betriebsstaette.pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router
            .navigate(['stammdaten', 'jahr', berichtsjahr, '404'])
            .then(() => this.location.replaceState(state.url));
        }
        this.alertService.setAlert(err);
        return EMPTY;
      })
    );
  }
}
