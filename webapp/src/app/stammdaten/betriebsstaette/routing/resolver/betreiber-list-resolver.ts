import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  BetreiberListItemDto,
  BetreiberRestControllerService,
} from '@api/stammdaten';

@Injectable()
export class BetreiberListResolver
  implements Resolve<Array<BetreiberListItemDto>>
{
  constructor(
    private service: BetreiberRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<Array<BetreiberListItemDto>> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    return this.service
      .readAllBetreiberByJahrAndLand(berichtsjahr, landSchluessel)
      .pipe(this.alertService.catchError());
  }
}
