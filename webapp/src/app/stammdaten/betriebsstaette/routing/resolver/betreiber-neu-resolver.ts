import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import {
  BetreiberDto,
  BetriebsstaetteDto,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';

@Injectable()
export class BetreiberNeuResolver implements Resolve<BetreiberDto> {
  constructor(
    private betriebsstaettenService: BetriebsstaettenRestControllerService,
    private alertService: AlertService
  ) {}

  private static createBetreiberFromBetriebsstaette(
    betriebsstaette: BetriebsstaetteDto
  ): BetreiberDto {
    return {
      id: null,
      land: betriebsstaette.land,
      berichtsjahr: betriebsstaette.berichtsjahr,
      name: null,
      vertraulichkeitsgrund: null,
      url: null,
      bemerkung: null,
      ersteErfassung: null,
      letzteAenderung: null,
      adresse: betriebsstaette.adresse,
    };
  }

  resolve(route: ActivatedRouteSnapshot): Observable<BetreiberDto> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const betriebsstaettenNummer = route.params.nummer;

    return this.betriebsstaettenService
      .readBetriebsstaetteByJahrLandNummer(
        berichtsjahr,
        landSchluessel,
        betriebsstaettenNummer
      )
      .pipe(
        map((betriebsstaette) =>
          BetreiberNeuResolver.createBetreiberFromBetriebsstaette(
            betriebsstaette
          )
        ),
        this.alertService.catchError()
      );
  }
}
