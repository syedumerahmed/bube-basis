import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  BetreiberDto,
  BetreiberRestControllerService,
  BetriebsstaettenRestControllerService,
} from '@api/stammdaten';

@Injectable()
export class BetreiberResolver implements Resolve<BetreiberDto> {
  constructor(
    private service: BetreiberRestControllerService,
    private betriebsstaettenService: BetriebsstaettenRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<BetreiberDto> {
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const betreiberNummer = route.params.betreiberNummer;

    return this.service
      .readBetreiberByJahrLandNummer(
        berichtsjahr,
        landSchluessel,
        betreiberNummer
      )
      .pipe(this.alertService.catchError());
  }
}
