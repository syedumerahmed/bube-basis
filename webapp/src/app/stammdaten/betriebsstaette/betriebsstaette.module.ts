import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BetriebsstaetteDetailsComponent } from './component/betriebsstaette-details/betriebsstaette-details.component';
import { BetriebsstaettenListeComponent } from './component/betriebsstaetten-liste/betriebsstaetten-liste.component';
import { BetreiberComponent } from './component/betreiber/betreiber.component';
import { BetreiberDetailsComponent } from './component/betreiber-details/betreiber-details.component';
import { AnsprechpartnerComponent } from './component/ansprechpartner/ansprechpartner.component';
import { PruefberichteComponent } from './component/pruefberichte/pruefberichte.component';
import { BetriebsstaetteMainComponent } from './component/betriebsstaette-main/betriebsstaette-main.component';
import { BetriebsstaetteComponent } from './component/betriebsstaette/betriebsstaette.component';
import { BetriebsstaetteAllgemeinComponent } from './component/betriebsstaette-allgemein/betriebsstaette-allgemein.component';
import { BetreiberAllgemeinComponent } from './component/betreiber-allgemein/betreiber-allgemein.component';
import { SucheComponent } from './component/suche/suche.component';

import { AnlageModule } from '@/stammdaten/anlage/anlage.module';
import { QuelleModule } from '@/stammdaten/quelle/quelle.module';
import { BaseModule } from '@/base/base.module';
import { StammdatenExportModalComponent } from './component/stammdaten-export-modal/stammdaten-export-modal.component';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BerichtsobjekteComponent } from './component/berichtsobjekte/berichtsobjekte.component';
import { BetriebsstaetteRoutingModule } from '@/stammdaten/betriebsstaette/routing/betriebsstaette-routing.module';

@NgModule({
  declarations: [
    SucheComponent,
    BetriebsstaetteDetailsComponent,
    BetriebsstaettenListeComponent,
    BetreiberComponent,
    BetreiberDetailsComponent,
    AnsprechpartnerComponent,
    PruefberichteComponent,
    BetriebsstaetteMainComponent,
    BetriebsstaetteComponent,
    BetriebsstaetteAllgemeinComponent,
    BetreiberAllgemeinComponent,
    StammdatenExportModalComponent,
    BerichtsobjekteComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,

    BaseModule,
    BasedomainModule,
    AnlageModule,
    QuelleModule,
    BetriebsstaetteRoutingModule,
  ],
  providers: [FilenameDateFormatPipe],
  exports: [SucheComponent, AnsprechpartnerComponent],
})
export class BetriebsstaetteModule {}
