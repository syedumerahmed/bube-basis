import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { AlertService } from '@/base/service/alert.service';
import {
  AnlageUebernahmeDto,
  LeistungVectorDto,
  SpbAnlageVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { GeoPunktDto } from '@api/stammdaten';
import {
  ClarityIcons,
  factoryIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(factoryIcon, timesIcon, floppyIcon);
@Component({
  selector: 'app-uebernehme-anlage',
  templateUrl: './uebernehme-anlage.component.html',
  styleUrls: ['./uebernehme-anlage.component.scss'],
})
export class UebernehmeAnlageComponent implements OnInit {
  vergleich: SpbAnlageVergleichDto;
  quellenDisplay: boolean;
  uebernahmeDialogOpen = false;

  nameUebernehmen = false;
  nummerUebernehmen = false;
  inbetriebnahmeUebernehmen = false;
  betriebsstatusUebernehmen = false;
  betriebsstatusSeitUebernehmen = false;
  geoPunktUebernehmen = false;
  leistungenUebernehmen = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly quellenZuordungService: QuellenZuordungService,
    private readonly alertService: AlertService,
    private readonly location: Location,
    private readonly service: StammdatenUebernahmeRestControllerService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.vergleich = data.vergleich;
      this.readQuellenDisplay(
        this.route.snapshot.params.berichtsjahr,
        this.route.snapshot.params.land
      );
    });
  }

  readQuellenDisplay(berichtsjahr: string, land: string): void {
    this.quellenZuordungService
      .quellenSindAnBetriebsstaetten(berichtsjahr, land)
      .subscribe(
        (ret) => {
          this.quellenDisplay = !ret;
        },
        (error) => this.alertService.setAlert(error)
      );
  }

  back(): void {
    this.location.back();
  }

  getGeoPunktText(geoPunkt: GeoPunktDto): string {
    if (geoPunkt == null) {
      return null;
    }
    return geoPunkt.epsgCode.ktext + ' ' + geoPunkt.nord + '/' + geoPunkt.ost;
  }

  countAllgemein(): number {
    return Object.values(this.vergleich.allgemein).filter(
      (value) => !value.equal
    ).length;
  }

  countLeistungen(): number {
    return this.vergleich.leistungen.filter(
      (leistung: LeistungVectorDto) => !leistung.equal
    ).length;
  }

  canUebernehmen(): boolean {
    return this.vergleich.hasChanges;
  }

  uebernehmen($event: boolean): void {
    if ($event) {
      const uebernahme: AnlageUebernahmeDto = {
        id: this.vergleich.id,
        spbId: this.vergleich.spbId,
        anlageNr: this.nummerUebernehmen,
        geoPunkt: this.geoPunktUebernehmen,
        betriebsstatus: this.betriebsstatusUebernehmen,
        betriebsstatusSeit: this.betriebsstatusSeitUebernehmen,
        leistungen: this.leistungenUebernehmen,
        name: this.nameUebernehmen,
        inbetriebnahme: this.inbetriebnahmeUebernehmen,
      };

      this.service.uebernehmeAnlage(uebernahme).subscribe(
        (anlage) => {
          this.router.navigate([
            'stammdaten',
            'jahr',
            this.route.snapshot.params.berichtsjahr,
            'betriebsstaette',
            'land',
            this.route.snapshot.params.land,
            'nummer',
            this.route.snapshot.params.betriebsstaetteNummer,
            'anlage',
            'nummer',
            anlage.anlageNr,
          ]);
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Problem beim Übernehmen der Daten aufgetreten.'
          );
        }
      );
    }
  }
}
