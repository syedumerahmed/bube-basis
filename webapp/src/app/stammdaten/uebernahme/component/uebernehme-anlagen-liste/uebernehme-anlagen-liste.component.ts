import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbAnlageListItemDto,
  SpbAnlagenRestControllerService,
} from '@api/stammdatenbetreiber';
import { AnlageDto } from '@api/stammdaten';
import { ClarityIcons, factoryIcon, importIcon } from '@cds/core/icon';

const UEBERNEHMEN_FLAG = 'uebernehmen';

ClarityIcons.addIcons(factoryIcon, importIcon);
@Component({
  selector: 'app-uebernehme-anlagen-liste',
  templateUrl: './uebernehme-anlagen-liste.component.html',
  styleUrls: ['./uebernehme-anlagen-liste.component.scss'],
})
export class UebernehmeAnlagenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  anlagenListe: SpbAnlageListItemDto[];
  betriebsstaetteId: number;
  loading = true;
  anlageId: number;

  constructor(
    private route: ActivatedRoute,
    private service: SpbAnlagenRestControllerService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaetteId = data.vergleich.id;
      this.refresh();
    });
  }

  refresh(): void {
    this.loading = true;
    this.service.listAllNeueSpbAnlagen(this.betriebsstaetteId).subscribe(
      (liste: SpbAnlageListItemDto[]) => {
        this.anlagenListe = liste;
        this.loading = false;
      },
      (error) => {
        this.alertService.setAlert(error);
        this.loading = false;
      }
    );
  }

  uebernehmen(anlage: SpbAnlageListItemDto): void {
    this.service
      .readSpbAnlage(this.betriebsstaetteId, anlage.anlageNr)
      .subscribe((spbAnlage) => {
        const anlage: AnlageDto = {
          ...spbAnlage,
          id: null,
        };
        const aktuellesJahr = this.route.snapshot.params.berichtsjahr;
        const land = this.route.snapshot.params.land;
        const betriebsstaettenNummer =
          this.route.snapshot.params.betriebsstaetteNummer;
        this.router.navigate(
          [
            '/stammdaten/jahr/' +
              aktuellesJahr +
              '/betriebsstaette/land/' +
              land +
              '/nummer/' +
              betriebsstaettenNummer +
              '/anlage/' +
              UEBERNEHMEN_FLAG,
          ],
          {
            state: { anlage: anlage },
            queryParams: { spbAnlageId: spbAnlage.id },
          }
        );
      });
  }
}
