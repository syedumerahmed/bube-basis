import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeAnlagenListeComponent } from './uebernehme-anlagen-liste.component';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import { SpbAnlagenRestControllerService } from '@api/stammdatenbetreiber';
import { BetriebsstaetteDto } from '@api/stammdaten';

describe('UebernehmeAnlagenListeComponent', () => {
  const betriebsstaette: BetriebsstaetteDto = {
    adresse: undefined,
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    gemeindekennziffer: undefined,
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  let dataSubject: Subject<Data>;
  let component: UebernehmeAnlagenListeComponent;
  let fixture: ComponentFixture<UebernehmeAnlagenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [UebernehmeAnlagenListeComponent],
      providers: [
        {
          provide: SpbAnlagenRestControllerService,
          useValue: MockService(SpbAnlagenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(UebernehmeAnlagenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
