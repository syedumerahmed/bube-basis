import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VergleichstabelleAnsprechpartnerComponent } from './vergleichstabelle-ansprechpartner.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('VergleichstabelleAnsprechpartnerComponent', () => {
  let component: VergleichstabelleAnsprechpartnerComponent;
  let fixture: ComponentFixture<VergleichstabelleAnsprechpartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VergleichstabelleAnsprechpartnerComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(
      VergleichstabelleAnsprechpartnerComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
