import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AnsprechpartnerVectorDto } from '@api/stammdaten-uebernahme';
import { KommunikationsverbindungDto } from '@api/stammdaten';

@Component({
  selector: 'app-vergleichstabelle-ansprechpartner',
  templateUrl: './vergleichstabelle-ansprechpartner.component.html',
  styleUrls: ['./vergleichstabelle-ansprechpartner.component.scss'],
})
export class VergleichstabelleAnsprechpartnerComponent {
  @Input()
  ansprechpartner: Array<AnsprechpartnerVectorDto>;

  @Input()
  isEqual: boolean;

  @Input()
  checkValue: boolean;
  @Output()
  checkValueChange = new EventEmitter<boolean>();

  updateValue(): void {
    this.checkValueChange.emit(this.checkValue);
  }

  getKommunikationsverbindungenText(
    liste: KommunikationsverbindungDto[]
  ): string {
    let text = '';

    if (liste == null) {
      return null;
    }

    liste.forEach(
      (kom: KommunikationsverbindungDto) =>
        (text += kom.typ.ktext + ' ' + kom.verbindung + ' ')
    );
    return text ? '( ' + text + ')' : null;
  }
}
