import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VergleichszeileComponent } from './vergleichszeile.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('VergleichszeileComponent', () => {
  let component: VergleichszeileComponent;
  let fixture: ComponentFixture<VergleichszeileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VergleichszeileComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VergleichszeileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
