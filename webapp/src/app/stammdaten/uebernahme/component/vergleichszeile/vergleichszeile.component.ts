import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-vergleichszeile',
  templateUrl: './vergleichszeile.component.html',
  styleUrls: ['./vergleichszeile.component.scss'],
})
export class VergleichszeileComponent {
  @Input()
  label: string;

  @Input()
  spl: string | boolean;

  @Input()
  spb: string | boolean;

  @Input()
  isEqual: boolean;

  @Input()
  checkValue: boolean;

  @Output()
  checkValueChange = new EventEmitter<boolean>();

  updateValue(): void {
    this.checkValueChange.emit(this.checkValue);
  }
}
