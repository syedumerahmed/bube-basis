import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VergleichstabelleKommunikationComponent } from './vergleichstabelle-kommunikation.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('VergleichstabelleKommunikationComponent', () => {
  let component: VergleichstabelleKommunikationComponent;
  let fixture: ComponentFixture<VergleichstabelleKommunikationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VergleichstabelleKommunikationComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VergleichstabelleKommunikationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
