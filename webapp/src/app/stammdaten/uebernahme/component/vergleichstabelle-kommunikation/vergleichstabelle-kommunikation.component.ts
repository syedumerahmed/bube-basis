import { Component, EventEmitter, Input, Output } from '@angular/core';
import { KommunikationsverbindungVectorDto } from '@api/stammdaten-uebernahme';

@Component({
  selector: 'app-vergleichstabelle-kommunikation',
  templateUrl: './vergleichstabelle-kommunikation.component.html',
  styleUrls: ['./vergleichstabelle-kommunikation.component.scss'],
})
export class VergleichstabelleKommunikationComponent {
  @Input()
  kommunikationsverbindungen: Array<KommunikationsverbindungVectorDto>;

  @Input()
  isEqual: boolean;

  @Input()
  checkValue: boolean;
  @Output()
  checkValueChange = new EventEmitter<boolean>();

  updateValue(): void {
    this.checkValueChange.emit(this.checkValue);
  }
}
