import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import {
  QuelleUebernahmeDto,
  SpbQuelleVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { GeoPunktDto } from '@api/stammdaten';
import { ClarityIcons, cloudIcon, floppyIcon, timesIcon } from '@cds/core/icon';

ClarityIcons.addIcons(cloudIcon, timesIcon, floppyIcon);
@Component({
  selector: 'app-uebernehme-quelle',
  templateUrl: './uebernehme-quelle.component.html',
  styleUrls: ['./uebernehme-quelle.component.scss'],
})
export class UebernehmeQuelleComponent implements OnInit {
  vergleich: SpbQuelleVergleichDto;
  uebernahmeDialogOpen = false;

  nameUebernehmen = false;
  quelleNrUebernehmen = false;
  geoPunktUebernemen = false;
  bauhoeheUebernehmen = false;
  breiteUebernehmen = false;
  durchmesserUebernehmen = false;
  flaecheUebernehmen = false;
  laengeUebernehmen = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly service: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.vergleich = data.vergleich;
    });
  }

  back(): void {
    this.location.back();
  }

  getGeoPunktText(geoPunkt: GeoPunktDto): string {
    if (geoPunkt == null) {
      return null;
    }
    return geoPunkt.epsgCode.ktext + ' ' + geoPunkt.nord + '/' + geoPunkt.ost;
  }

  uebernehmen($event: boolean): void {
    if ($event) {
      const uebernahme: QuelleUebernahmeDto = {
        id: this.vergleich.id,
        spbId: this.vergleich.spbId,
        name: this.nameUebernehmen,
        quelleNr: this.quelleNrUebernehmen,
        geoPunkt: this.geoPunktUebernemen,
        bauhoehe: this.bauhoeheUebernehmen,
        breite: this.breiteUebernehmen,
        durchmesser: this.durchmesserUebernehmen,
        flaeche: this.flaecheUebernehmen,
        laenge: this.laengeUebernehmen,
      };
      this.service.uebernehmeQuelle(uebernahme).subscribe(
        (quelle) => {
          if (this.route.snapshot.params.hasAnlage === 'true') {
            this.router.navigate([
              'stammdaten',
              'jahr',
              this.route.snapshot.params.berichtsjahr,
              'betriebsstaette',
              'land',
              this.route.snapshot.params.land,
              'nummer',
              this.route.snapshot.params.betriebsstaetteNummer,
              'anlage',
              'nummer',
              this.route.snapshot.params.anlageNr,
              'quelle',
              'nummer',
              quelle.quelleNr,
            ]);
          } else {
            this.router.navigate([
              'stammdaten',
              'jahr',
              this.route.snapshot.params.berichtsjahr,
              'betriebsstaette',
              'land',
              this.route.snapshot.params.land,
              'nummer',
              this.route.snapshot.params.betriebsstaetteNummer,
              'quelle',
              'nummer',
              quelle.quelleNr,
            ]);
          }
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Problem beim Übernehmen der Daten aufgetreten.'
          );
        }
      );
    }
  }

  canUebernehmen(): boolean {
    return this.vergleich.hasChanges;
  }
}
