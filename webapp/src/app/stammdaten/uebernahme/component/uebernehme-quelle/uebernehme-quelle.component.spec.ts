import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeQuelleComponent } from './uebernehme-quelle.component';
import { MockComponents, MockModule, MockPipe, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { VergleichszeileComponent } from '@/stammdaten/uebernahme/component/vergleichszeile/vergleichszeile.component';
import { VergleichstabelleNummernComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-nummern/vergleichstabelle-nummern.component';
import { VergleichstabelleAnsprechpartnerComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-ansprechpartner/vergleichstabelle-ansprechpartner.component';
import { VergleichstabelleKommunikationComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-kommunikation/vergleichstabelle-kommunikation.component';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { UebernahmeDialogComponent } from '@/stammdaten/uebernahme/component/uebernahme-dialog/uebernahme-dialog.component';
import {
  SpbQuelleVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { ReferenzDto } from '@api/stammdaten';

describe('UebernehmeQuelleComponent', () => {
  let component: UebernehmeQuelleComponent;
  let fixture: ComponentFixture<UebernehmeQuelleComponent>;

  const splRef: ReferenzDto = {
    schluessel: '16',
    gueltigBis: 16,
    gueltigVon: 16,
    ktext: '16',
    land: '16',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const spbRef: ReferenzDto = {
    schluessel: '15',
    gueltigBis: 15,
    gueltigVon: 15,
    ktext: '15',
    land: '15',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const vergleich: SpbQuelleVergleichDto = {
    id: 0,
    hasChanges: true,
    spbId: 0,
    name: { spb: 'test', spl: 'spl', equal: false },
    bauhoehe: { spb: 10, spl: 15, equal: false },
    breite: { spb: 10, spl: 15, equal: false },
    durchmesser: { spb: 10, spl: 15, equal: false },
    flaeche: { spb: 10, spl: 15, equal: false },
    geoPunkt: {
      spb: {
        nord: 15,
        ost: 15,
        epsgCode: spbRef,
      },
      spl: {
        nord: 16,
        ost: 16,
        epsgCode: spbRef,
      },
      equal: false,
    },
    laenge: { spb: 10, spl: 15, equal: false },
    quelleArt: {
      spb: spbRef,
      spl: splRef,
      equal: false,
    },
    quelleNr: { spb: '10', spl: '15', equal: false },
  };

  const routeData$ = new BehaviorSubject({ vergleich });
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UebernehmeQuelleComponent,
        MockComponents(
          VergleichszeileComponent,
          VergleichstabelleNummernComponent,
          VergleichstabelleAnsprechpartnerComponent,
          VergleichstabelleKommunikationComponent,
          UebernahmeDialogComponent
        ),
        MockPipe(EllipsizePipe),
      ],
      providers: [
        MockProviders(StammdatenUebernahmeRestControllerService, Router),
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$,
          },
        },
      ],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UebernehmeQuelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
