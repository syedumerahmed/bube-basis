import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VergleichstabelleLeistungenComponent } from './vergleichstabelle-leistungen.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('VergleichstabelleLeistungenComponent', () => {
  let component: VergleichstabelleLeistungenComponent;
  let fixture: ComponentFixture<VergleichstabelleLeistungenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VergleichstabelleLeistungenComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VergleichstabelleLeistungenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
