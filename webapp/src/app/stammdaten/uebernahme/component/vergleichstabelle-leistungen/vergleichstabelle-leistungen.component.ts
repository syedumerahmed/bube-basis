import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LeistungVectorDto } from '@api/stammdaten-uebernahme';
import { LeistungsklasseEnum } from '@api/stammdaten';

@Component({
  selector: 'app-vergleichstabelle-leistungen',
  templateUrl: './vergleichstabelle-leistungen.component.html',
  styleUrls: ['./vergleichstabelle-leistungen.component.scss'],
})
export class VergleichstabelleLeistungenComponent {
  @Input()
  leistungen: Array<LeistungVectorDto>;
  @Input()
  isEqual: boolean;

  @Input()
  checkValue: boolean;

  @Output()
  checkValueChange = new EventEmitter<boolean>();

  readonly leistungsklasseDisplayText: Record<LeistungsklasseEnum, string> = {
    INSTALLIERT: 'Installiert',
    BETRIEBEN: 'Betrieben',
    GENEHMIGT: 'Genehmigt',
  };

  updateValue(): void {
    this.checkValueChange.emit(this.checkValue);
  }
}
