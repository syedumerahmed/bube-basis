import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeAnlagenQuellenListeComponent } from './uebernehme-anlagen-quellen-liste.component';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { SpbQuellenRestControllerService } from '@api/stammdatenbetreiber';

describe('UebernehmeAnlagenQuellenListeComponent', () => {
  let component: UebernehmeAnlagenQuellenListeComponent;
  let fixture: ComponentFixture<UebernehmeAnlagenQuellenListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [UebernehmeAnlagenQuellenListeComponent],
      providers: [
        {
          provide: SpbQuellenRestControllerService,
          useValue: MockService(SpbQuellenRestControllerService),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(UebernehmeAnlagenQuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
