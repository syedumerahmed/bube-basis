import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbQuelleListItemDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import { QuelleDto } from '@api/stammdaten';
import { ClarityIcons, cloudIcon, importIcon } from '@cds/core/icon';

const UEBERNEHMEN_FLAG = 'uebernehmen';

ClarityIcons.addIcons(cloudIcon, importIcon);
@Component({
  selector: 'app-uebernehme-anlagen-quellen-liste',
  templateUrl: './uebernehme-anlagen-quellen-liste.component.html',
  styleUrls: ['./uebernehme-anlagen-quellen-liste.component.scss'],
})
export class UebernehmeAnlagenQuellenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  quellenListe: SpbQuelleListItemDto[];
  anlageId: number;
  loading = true;

  constructor(
    private route: ActivatedRoute,
    private service: SpbQuellenRestControllerService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.anlageId = data.vergleich.id;
      this.refresh();
    });
  }

  refresh(): void {
    this.loading = true;
    this.service.listAllNeueSpbQuellenForAnlage(this.anlageId).subscribe(
      (liste: SpbQuelleListItemDto[]) => {
        this.quellenListe = liste;
        this.loading = false;
      },
      (error) => {
        this.alertService.setAlert(error);
        this.loading = false;
      }
    );
  }

  uebernehmen(spbQuelleListItemDto: SpbQuelleListItemDto): void {
    const routeParams = this.route.snapshot.params;
    const betriebsstaetteId = routeParams.betriebsstaetteId;
    this.service
      .readSpbQuelleByAnlage(
        betriebsstaetteId,
        this.anlageId,
        spbQuelleListItemDto.quelleNr
      )
      .subscribe((spbQuelle) => {
        const quelle: QuelleDto = {
          ...spbQuelle,
          id: null,
        };
        const aktuellesJahr = routeParams.berichtsjahr;
        const land = routeParams.land;
        const betriebsstaettenNummer = routeParams.betriebsstaetteNummer;
        const anlageNummer = routeParams.anlageNummer;
        this.router.navigate(
          [
            '/stammdaten/jahr/' +
              aktuellesJahr +
              '/betriebsstaette/land/' +
              land +
              '/nummer/' +
              betriebsstaettenNummer +
              '/anlage/nummer/' +
              anlageNummer +
              '/quelle/' +
              UEBERNEHMEN_FLAG,
          ],
          {
            state: { quelle: quelle },
            queryParams: { spbQuelleId: spbQuelleListItemDto.spbQuelleId },
          }
        );
      });
  }
}
