import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeAnlagenteilComponent } from './uebernehme-anlagenteil.component';
import { MockComponents, MockModule, MockPipe, MockProviders } from 'ng-mocks';
import { VergleichszeileComponent } from '@/stammdaten/uebernahme/component/vergleichszeile/vergleichszeile.component';
import { VergleichstabelleNummernComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-nummern/vergleichstabelle-nummern.component';
import { VergleichstabelleAnsprechpartnerComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-ansprechpartner/vergleichstabelle-ansprechpartner.component';
import { VergleichstabelleKommunikationComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-kommunikation/vergleichstabelle-kommunikation.component';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { ActivatedRoute, Router } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { BehaviorSubject } from 'rxjs';
import { VergleichstabelleLeistungenComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-leistungen/vergleichstabelle-leistungen.component';
import { UebernahmeDialogComponent } from '@/stammdaten/uebernahme/component/uebernahme-dialog/uebernahme-dialog.component';
import {
  SpbAnlagenteilVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { ReferenzDto } from '@api/stammdaten';

describe('UebernehmeAnlagenteilComponent', () => {
  let component: UebernehmeAnlagenteilComponent;
  let fixture: ComponentFixture<UebernehmeAnlagenteilComponent>;

  const isEqual = false;

  const splRef: ReferenzDto = {
    schluessel: '16',
    gueltigBis: 16,
    gueltigVon: 16,
    ktext: '16',
    land: '16',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const spbRef: ReferenzDto = {
    schluessel: '15',
    gueltigBis: 15,
    gueltigVon: 15,
    ktext: '15',
    land: '15',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const vergleich: SpbAnlagenteilVergleichDto = {
    id: 15,
    spbId: 16,
    hasChanges: true,
    allgemein: {
      betriebsstatus: { spb: spbRef, spl: splRef, equal: isEqual },
      anlagenteilNr: { spb: 'spb', spl: 'spl', equal: isEqual },
      betriebsstatusSeit: { spb: 'spb', spl: 'spl', equal: isEqual },
      name: { spb: 'spb', spl: 'spl', equal: isEqual },
      inbetriebnahme: { spb: 'spb', spl: 'spl', equal: isEqual },
      geoPunkt: {
        spb: {
          nord: 15,
          ost: 15,
          epsgCode: spbRef,
        },
        spl: {
          nord: 16,
          ost: 16,
          epsgCode: spbRef,
        },
        equal: false,
      },
    },
    leistungen: [
      {
        spb: {
          leistung: 15,
          leistungseinheit: spbRef,
          leistungsklasse: 'BETRIEBEN',
        },
        spl: {
          leistung: 15,
          leistungseinheit: splRef,
          leistungsklasse: 'BETRIEBEN',
        },
        equal: isEqual,
      },
    ],
  };

  const routeData$ = new BehaviorSubject({ vergleich });
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UebernehmeAnlagenteilComponent,
        MockComponents(
          VergleichszeileComponent,
          VergleichstabelleNummernComponent,
          VergleichstabelleAnsprechpartnerComponent,
          VergleichstabelleKommunikationComponent,
          VergleichstabelleLeistungenComponent,
          UebernahmeDialogComponent
        ),
        MockPipe(EllipsizePipe),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$,
          },
        },
        MockProviders(StammdatenUebernahmeRestControllerService, Router),
      ],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UebernehmeAnlagenteilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('count', () => {
    it('should count Allgemein', () => {
      expect(component.countAllgemein()).toEqual(6);
    });
    it('should count Leistungen', () => {
      expect(component.countLeistungen()).toEqual(1);
    });
  });
});
