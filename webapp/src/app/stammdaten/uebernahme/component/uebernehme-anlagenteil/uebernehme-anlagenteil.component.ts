import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import {
  AnlagenteilUebernahmeDto,
  LeistungVectorDto,
  SpbAnlagenteilVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { GeoPunktDto } from '@api/stammdaten';
import {
  ClarityIcons,
  factoryIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(factoryIcon, timesIcon, floppyIcon);
@Component({
  selector: 'app-uebernehme-anlagenteil',
  templateUrl: './uebernehme-anlagenteil.component.html',
  styleUrls: ['./uebernehme-anlagenteil.component.scss'],
})
export class UebernehmeAnlagenteilComponent implements OnInit {
  vergleich: SpbAnlagenteilVergleichDto;
  uebernahmeDialogOpen = false;

  nameUebernehmen = false;
  nummerUebernehmen = false;
  inbetriebnahmeUebernehmen = false;
  betriebsstatusUebernehmen = false;
  betriebsstatusSeitUebernehmen = false;
  geoPunktUebernehmen = false;
  leistungenUebernehmen = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly service: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.vergleich = data.vergleich;
    });
  }

  back(): void {
    this.location.back();
  }

  getGeoPunktText(geoPunkt: GeoPunktDto): string {
    if (geoPunkt == null) {
      return null;
    }
    return geoPunkt.epsgCode.ktext + ' ' + geoPunkt.nord + '/' + geoPunkt.ost;
  }

  countAllgemein(): number {
    return Object.values(this.vergleich.allgemein).filter(
      (value) => !value.equal
    ).length;
  }

  countLeistungen(): number {
    return this.vergleich.leistungen.filter(
      (leistung: LeistungVectorDto) => !leistung.equal
    ).length;
  }

  uebernehmen($event: boolean): void {
    if ($event) {
      const uebernahme: AnlagenteilUebernahmeDto = {
        id: this.vergleich.id,
        spbId: this.vergleich.spbId,
        anlagenteilNr: this.nummerUebernehmen,
        geoPunkt: this.geoPunktUebernehmen,
        betriebsstatus: this.betriebsstatusUebernehmen,
        betriebsstatusSeit: this.betriebsstatusSeitUebernehmen,
        name: this.nameUebernehmen,
        inbetriebnahme: this.inbetriebnahmeUebernehmen,
        leistungen: this.leistungenUebernehmen,
      };

      this.service.uebernehmeAnlagenteil(uebernahme).subscribe(
        (anlagenteil) => {
          this.router.navigate([
            'stammdaten',
            'jahr',
            this.route.snapshot.params.berichtsjahr,
            'betriebsstaette',
            'land',
            this.route.snapshot.params.land,
            'nummer',
            this.route.snapshot.params.betriebsstaetteNummer,
            'anlage',
            'nummer',
            this.route.snapshot.params.anlageNr,
            'anlagenteil',
            'nummer',
            anlagenteil.anlagenteilNr,
          ]);
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Problem beim Übernehmen der Daten aufgetreten.'
          );
        }
      );
    }
  }

  canUebernehmen(): boolean {
    return this.vergleich.hasChanges;
  }
}
