import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VergleichstabelleNummernComponent } from './vergleichstabelle-nummern.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

describe('VergleichstabelleNummernComponent', () => {
  let component: VergleichstabelleNummernComponent;
  let fixture: ComponentFixture<VergleichstabelleNummernComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VergleichstabelleNummernComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VergleichstabelleNummernComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
