import { Component, EventEmitter, Input, Output } from '@angular/core';
import { StringVectorDto } from '@api/stammdaten-uebernahme';

@Component({
  selector: 'app-vergleichstabelle-nummern',
  templateUrl: './vergleichstabelle-nummern.component.html',
  styleUrls: ['./vergleichstabelle-nummern.component.scss'],
})
export class VergleichstabelleNummernComponent {
  @Input()
  nummern: Array<StringVectorDto>;

  @Input()
  isEqual: boolean;

  @Input()
  checkValue: boolean;
  @Output()
  checkValueChange = new EventEmitter<boolean>();

  updateValue(): void {
    this.checkValueChange.emit(this.checkValue);
  }
}
