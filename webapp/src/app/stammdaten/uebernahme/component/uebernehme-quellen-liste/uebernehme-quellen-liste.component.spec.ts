import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeQuellenListeComponent } from './uebernehme-quellen-liste.component';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { SpbQuellenRestControllerService } from '@api/stammdatenbetreiber';

describe('UebernehmeQuellenListeComponent', () => {
  let component: UebernehmeQuellenListeComponent;
  let fixture: ComponentFixture<UebernehmeQuellenListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [UebernehmeQuellenListeComponent],
      providers: [
        {
          provide: SpbQuellenRestControllerService,
          useValue: MockService(SpbQuellenRestControllerService),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(UebernehmeQuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
