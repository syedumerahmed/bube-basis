import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbQuelleListItemDto,
  SpbQuellenRestControllerService,
} from '@api/stammdatenbetreiber';
import { QuelleDto } from '@api/stammdaten';
import { ClarityIcons, cloudIcon, importIcon } from '@cds/core/icon';

const UEBERNEHMEN_FLAG = 'uebernehmen';

ClarityIcons.addIcons(cloudIcon, importIcon);
@Component({
  selector: 'app-uebernehme-quellen-liste',
  templateUrl: './uebernehme-quellen-liste.component.html',
  styleUrls: ['./uebernehme-quellen-liste.component.scss'],
})
export class UebernehmeQuellenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  quellenListe: SpbQuelleListItemDto[];
  betriebsstaetteId: number;
  loading = true;
  anlageId: number;

  constructor(
    private route: ActivatedRoute,
    private service: SpbQuellenRestControllerService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaetteId = data.vergleich.id;
      this.refresh();
    });
  }

  refresh(): void {
    this.loading = true;
    this.service
      .listAllNeueSpbQuellenForBetriebsstaette(this.betriebsstaetteId)
      .subscribe(
        (liste: SpbQuelleListItemDto[]) => {
          this.quellenListe = liste;
          this.loading = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  uebernehmen(spbQuelleListItemDto: SpbQuelleListItemDto): void {
    this.service
      .readSpbQuelleByBetriebsstaette(
        this.betriebsstaetteId,
        spbQuelleListItemDto.quelleNr
      )
      .subscribe((spbQuelle) => {
        const quelle: QuelleDto = {
          ...spbQuelle,
          id: null,
        };
        const aktuellesJahr = this.route.snapshot.params.berichtsjahr;
        const land = this.route.snapshot.params.land;
        const betriebsstaettenNummer =
          this.route.snapshot.params.betriebsstaetteNummer;
        this.router.navigate(
          [
            '/stammdaten/jahr/' +
              aktuellesJahr +
              '/betriebsstaette/land/' +
              land +
              '/nummer/' +
              betriebsstaettenNummer +
              '/quelle/' +
              UEBERNEHMEN_FLAG,
          ],
          {
            state: { quelle: quelle },
            queryParams: { spbQuelleId: spbQuelleListItemDto.spbQuelleId },
          }
        );
      });
  }
}
