import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeAnlagenteileListeComponent } from './uebernehme-anlagenteile-liste.component';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { SpbAnlagenteilRestControllerService } from '@api/stammdatenbetreiber';

describe('UebernehmeAnlagenteileListeComponent', () => {
  let component: UebernehmeAnlagenteileListeComponent;
  let fixture: ComponentFixture<UebernehmeAnlagenteileListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [UebernehmeAnlagenteileListeComponent],
      providers: [
        {
          provide: SpbAnlagenteilRestControllerService,
          useValue: MockService(SpbAnlagenteilRestControllerService),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(UebernehmeAnlagenteileListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
