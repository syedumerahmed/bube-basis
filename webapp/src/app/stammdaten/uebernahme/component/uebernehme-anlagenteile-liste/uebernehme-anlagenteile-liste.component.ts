import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  SpbAnlagenteilListItemDto,
  SpbAnlagenteilRestControllerService,
} from '@api/stammdatenbetreiber';
import { AnlagenteilDto } from '@api/stammdaten';
import { ClarityIcons, factoryIcon, importIcon } from '@cds/core/icon';

const UEBERNEHMEN_FLAG = 'uebernehmen';

ClarityIcons.addIcons(factoryIcon, importIcon);

@Component({
  selector: 'app-uebernehme-anlagenteile-liste',
  templateUrl: './uebernehme-anlagenteile-liste.component.html',
  styleUrls: ['./uebernehme-anlagenteile-liste.component.scss'],
})
export class UebernehmeAnlagenteileListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  anlagenteileListe: SpbAnlagenteilListItemDto[];
  loading = true;
  anlageId: number;

  constructor(
    private route: ActivatedRoute,
    private service: SpbAnlagenteilRestControllerService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.anlageId = data.vergleich.id;
      this.refresh();
    });
  }

  refresh(): void {
    this.loading = true;
    this.service.listAllNeueSpbAnlagenteile(this.anlageId).subscribe(
      (liste: SpbAnlagenteilListItemDto[]) => {
        this.anlagenteileListe = liste;
        this.loading = false;
      },
      (error) => {
        this.alertService.setAlert(error);
        this.loading = false;
      }
    );
  }

  uebernehmen(anlagenteil: SpbAnlagenteilListItemDto): void {
    const betriebsstaetteId = this.route.snapshot.params.betriebsstaetteId;
    this.service
      .readSpbAnlagenteil(
        betriebsstaetteId,
        this.anlageId,
        anlagenteil.anlagenteilNr
      )
      .subscribe((spbAnlagenteil) => {
        const anlagenteil: AnlagenteilDto = {
          ...spbAnlagenteil,
          id: null,
          name: spbAnlagenteil.name,
          parentAnlageId: spbAnlagenteil.parentAnlageId,
          anlagenteilNr: spbAnlagenteil.anlageNr,
          parentBetriebsstaetteId: betriebsstaetteId,
        };
        const aktuellesJahr = this.route.snapshot.params.berichtsjahr;
        const land = this.route.snapshot.params.land;
        const betriebsstaettenNummer =
          this.route.snapshot.params.betriebsstaetteNummer;
        const anlageNummer = this.route.snapshot.params.anlageNummer;
        this.router.navigate(
          [
            '/stammdaten/jahr/' +
              aktuellesJahr +
              '/betriebsstaette/land/' +
              land +
              '/nummer/' +
              betriebsstaettenNummer +
              '/anlage/nummer/' +
              anlageNummer +
              '/anlagenteil/' +
              UEBERNEHMEN_FLAG,
          ],
          {
            state: { anlagenteil: anlagenteil },
            queryParams: { spbAnlagenteilId: spbAnlagenteil.id },
          }
        );
      });
  }
}
