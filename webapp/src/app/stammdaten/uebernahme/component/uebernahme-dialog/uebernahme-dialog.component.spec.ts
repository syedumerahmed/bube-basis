import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernahmeDialogComponent } from './uebernahme-dialog.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';

describe('UebernahmeDialogComponent', () => {
  let component: UebernahmeDialogComponent;
  let fixture: ComponentFixture<UebernahmeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UebernahmeDialogComponent],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UebernahmeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
