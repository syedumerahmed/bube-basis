import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-uebernahme-dialog',
  templateUrl: './uebernahme-dialog.component.html',
  styleUrls: ['./uebernahme-dialog.component.scss'],
})
export class UebernahmeDialogComponent {
  @Input()
  open = true;
  @Input()
  ortChanged = false;

  @Output()
  uebernehmenOut = new EventEmitter<boolean>();
  @Output()
  openChange = new EventEmitter<boolean>();

  abbrechen(): void {
    this.openChange.emit(false);
    this.open = false;
  }

  uebernehmen(): void {
    this.uebernehmenOut.emit(true);
    this.openChange.emit(false);
    this.open = false;
  }
}
