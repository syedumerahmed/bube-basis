import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeBetreiberComponent } from './uebernehme-betreiber.component';
import { MockComponents, MockModule, MockPipe, MockProvider } from 'ng-mocks';
import { VergleichszeileComponent } from '@/stammdaten/uebernahme/component/vergleichszeile/vergleichszeile.component';
import { VergleichstabelleNummernComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-nummern/vergleichstabelle-nummern.component';
import { VergleichstabelleAnsprechpartnerComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-ansprechpartner/vergleichstabelle-ansprechpartner.component';
import { VergleichstabelleKommunikationComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-kommunikation/vergleichstabelle-kommunikation.component';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { ActivatedRoute } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { BehaviorSubject } from 'rxjs';
import { UebernahmeDialogComponent } from '@/stammdaten/uebernahme/component/uebernahme-dialog/uebernahme-dialog.component';
import {
  SpbBetreiberVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';
import { ReferenzDto } from '@api/stammdaten';

describe('UebernehmeBetreiberComponent', () => {
  let component: UebernehmeBetreiberComponent;
  let fixture: ComponentFixture<UebernehmeBetreiberComponent>;

  const splRef: ReferenzDto = {
    schluessel: '16',
    gueltigBis: 16,
    gueltigVon: 16,
    ktext: '16',
    land: '16',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const spbRef: ReferenzDto = {
    schluessel: '15',
    gueltigBis: 15,
    gueltigVon: 15,
    ktext: '15',
    land: '15',
    referenzliste: 'RBATC',
    sortier: '',
  };
  const vergleich: SpbBetreiberVergleichDto = {
    id: 15,
    spbId: 16,
    hasChanges: true,
    allgemein: {
      name: { spb: 'spb', spl: 'spl', equal: false },
    },
    adresse: {
      hausNr: { spb: 'spb', spl: 'spl', equal: false },
      ort: { spb: 'spb', spl: 'spl', equal: false },
      plz: { spb: 'spb', spl: 'spl', equal: false },
      ortsteil: { spb: 'spb', spl: 'spl', equal: false },
      postfach: { spb: 'spb', spl: 'spl', equal: false },
      postfachPlz: { spb: 'spb', spl: 'spl', equal: false },
      strasse: { spb: 'spb', spl: 'spl', equal: false },
      landIsoCode: { spb: spbRef, spl: splRef, equal: false },
    },
  };

  const routeData$ = new BehaviorSubject({ vergleich });
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UebernehmeBetreiberComponent,
        MockComponents(
          VergleichszeileComponent,
          VergleichstabelleNummernComponent,
          VergleichstabelleAnsprechpartnerComponent,
          VergleichstabelleKommunikationComponent,
          UebernahmeDialogComponent
        ),
        MockPipe(EllipsizePipe),
      ],
      providers: [
        MockProvider(StammdatenUebernahmeRestControllerService),
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$,
          },
        },
      ],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UebernehmeBetreiberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
