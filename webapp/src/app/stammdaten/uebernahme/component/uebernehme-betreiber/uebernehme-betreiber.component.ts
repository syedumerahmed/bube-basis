import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import {
  BetreiberUebernahmeDto,
  ReferenzVectorDto,
  SpbBetreiberVergleichDto,
  StammdatenUebernahmeRestControllerService,
  StringVectorDto,
} from '@api/stammdaten-uebernahme';
import { ClarityIcons, floppyIcon, timesIcon, userIcon } from '@cds/core/icon';

ClarityIcons.addIcons(userIcon, timesIcon, floppyIcon);
@Component({
  selector: 'app-uebernehme-betreiber',
  templateUrl: './uebernehme-betreiber.component.html',
  styleUrls: ['./uebernehme-betreiber.component.scss'],
})
export class UebernehmeBetreiberComponent implements OnInit {
  vergleich: SpbBetreiberVergleichDto;
  uebernahmeDialogOpen = false;

  nameUebernehmen = false;
  strasseUebernehmen = false;
  hausNrUebernehmen = false;
  plzUebernehmen = false;
  ortUebernehmen = false;
  ortsteilUebernehmen = false;
  postfachUebernehmen = false;
  postfachPlzUebernehmen = false;
  landIsoCodeUebernehmen = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly service: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.vergleich = data.vergleich;
    });
  }

  back(): void {
    this.location.back();
  }

  countAllgemein(): number {
    return Object.values(this.vergleich.allgemein).filter(
      (value: StringVectorDto) => !value.equal
    ).length;
  }

  countAdresse(): number {
    return Object.values(this.vergleich.adresse).filter(
      (value: ReferenzVectorDto | StringVectorDto) => !value.equal
    ).length;
  }

  uebernehmen($event: boolean): void {
    if ($event) {
      const uebernahme: BetreiberUebernahmeDto = {
        id: this.vergleich.id,
        spbId: this.vergleich.spbId,
        name: this.nameUebernehmen,
        adresse: {
          strasse: this.strasseUebernehmen,
          hausNr: this.hausNrUebernehmen,
          plz: this.plzUebernehmen,
          ort: this.ortUebernehmen,
          ortsteil: this.ortsteilUebernehmen,
          postfach: this.postfachUebernehmen,
          postfachPlz: this.postfachPlzUebernehmen,
          landIsoCode: this.landIsoCodeUebernehmen,
        },
      };
      this.service.uebernehmeBetreiber(uebernahme).subscribe(
        () => {
          this.back();
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Problem beim Übernehmen der Daten aufgetreten.'
          );
        }
      );
    }
  }

  canUebernehmen(): boolean {
    return this.vergleich.hasChanges;
  }
}
