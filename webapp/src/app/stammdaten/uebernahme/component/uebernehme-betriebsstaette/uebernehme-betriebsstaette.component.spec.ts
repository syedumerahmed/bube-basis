import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UebernehmeBetriebsstaetteComponent } from './uebernehme-betriebsstaette.component';
import {
  MockComponents,
  MockModule,
  MockPipe,
  MockProvider,
  MockService,
} from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { VergleichszeileComponent } from '@/stammdaten/uebernahme/component/vergleichszeile/vergleichszeile.component';
import { VergleichstabelleNummernComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-nummern/vergleichstabelle-nummern.component';
import { VergleichstabelleAnsprechpartnerComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-ansprechpartner/vergleichstabelle-ansprechpartner.component';
import { VergleichstabelleKommunikationComponent } from '@/stammdaten/uebernahme/component/vergleichstabelle-kommunikation/vergleichstabelle-kommunikation.component';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { UebernehmeAnlagenListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlagen-liste/uebernehme-anlagen-liste.component';
import { UebernehmeQuellenListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-quellen-liste/uebernehme-quellen-liste.component';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { UebernahmeDialogComponent } from '@/stammdaten/uebernahme/component/uebernahme-dialog/uebernahme-dialog.component';
import {
  SpbBetriebsstaetteVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

describe('UebernehmeBetriebsstaetteComponent', () => {
  let component: UebernehmeBetriebsstaetteComponent;
  let fixture: ComponentFixture<UebernehmeBetriebsstaetteComponent>;

  const vergleich: SpbBetriebsstaetteVergleichDto = {
    id: 7,
    spbId: 1,
    hasChanges: true,
    adresse: {
      strasse: { spb: '10', spl: null, equal: false },
      hausNr: { spb: '10', spl: null, equal: false },
      plz: { spb: '201', spl: '22085', equal: false },
      ort: { spb: 'Hamburg5', spl: 'Hamburg', equal: false },
      ortsteil: { spb: '10', spl: null, equal: false },
      postfachPlz: { spb: '45', spl: null, equal: false },
      postfach: { spb: '21', spl: null, equal: false },
      landIsoCode: {
        spl: null,
        spb: {
          id: 10081,
          referenznummer: '10081',
          land: '00',
          referenzliste: 'RSTAAT',
          schluessel: 'DE',
          sortier: '166',
          ktext: 'Deutschland',
          ltext: 'DE',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2020-02-08T23:00:00Z',
          eu: null,
          strg: '',
        },
        equal: false,
      },
    },
    allgemein: {
      abfallerzeugerNummern: [
        { spl: '123', spb: '156', equal: false },
        { spl: '111', spb: '111', equal: true },
      ],
      einleiterNummern: [
        { spl: '123', spb: '156', equal: false },
        { spl: '111', spb: '111', equal: true },
      ],
      flusseinzuggebiet: {
        spl: null,
        spb: {
          id: 10041,
          referenznummer: '10041',
          land: '00',
          referenzliste: 'RFLEZ',
          schluessel: '500',
          sortier: '01',
          ktext: 'Elbe/Labe',
          ltext: 'Elbe/Labe',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2020-02-08T23:00:00Z',
          eu: null,
          strg: '',
        },
        equal: false,
      },
      naceCode: {
        spl: null,
        spb: {
          id: 10059,
          referenznummer: '10059',
          land: '00',
          referenzliste: 'RNACE',
          schluessel: '0146',
          sortier: '0146',
          ktext: 'Haltung von Schweinen',
          ltext: 'Haltung von Schweinen',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2020-02-08T23:00:00Z',
          eu: null,
          strg: '',
        },
        equal: false,
      },
      direkteinleiter: { spb: true, spl: false, equal: false },
      indirekteinleiter: { spb: true, spl: false, equal: false },
      name: { spb: '17', spl: '18', equal: false },
      geoPunkt: {
        spb: {
          nord: 10,
          ost: 10,
          epsgCode: {
            id: 10113,
            referenznummer: '10113',
            land: '02',
            referenzliste: 'REPSG',
            schluessel: '3',
            sortier: '5',
            ktext: 'ETRS89/UTM Zone 32N (EPSG: 25832)',
            ltext:
              'ETRS 1989 UTM Zone 32N (EPSG: 25832) - kurze Koordinaten (6/7)',
            gueltigVon: 2007,
            gueltigBis: 2099,
            letzteAenderung: '2020-02-08T23:00:00Z',
            eu: null,
            strg: '',
          },
        },
        spl: null,
        equal: false,
      },
      betriebsstatus: {
        spl: {
          id: 10011,
          referenznummer: '10011',
          land: '00',
          referenzliste: 'RBETZ',
          schluessel: '01',
          sortier: '01',
          ktext: 'in Betrieb',
          ltext: 'in Betrieb',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2020-02-08T23:00:00Z',
          eu: null,
          strg: '',
        },
        spb: {
          id: 10012,
          referenznummer: '10012',
          land: '00',
          referenzliste: 'RBETZ',
          schluessel: '02',
          sortier: '02',
          ktext: 'außer Betrieb',
          ltext: 'außer Betrieb',
          gueltigVon: 2007,
          gueltigBis: 2099,
          letzteAenderung: '2020-02-08T23:00:00Z',
          eu: null,
          strg: '',
        },
        equal: false,
      },
      betriebsstatusSeit: { spb: '2021-09-09', spl: null, equal: false },
      inbetriebnahme: { spb: '2021-09-08', spl: null, equal: false },
    },
    ansprechpartner: [
      {
        spl: null,
        spb: {
          id: 52,
          vorname: 'Peter',
          nachname: 'Petersen',
          funktion: {
            id: 10091,
            referenznummer: '10091',
            land: '00',
            referenzliste: 'RFKN',
            schluessel: '01',
            sortier: '01',
            ktext: 'Geschäftsführer',
            ltext: '',
            gueltigVon: 2007,
            gueltigBis: 2099,
            letzteAenderung: '2020-02-08T23:00:00Z',
            eu: null,
            strg: '',
          },
          bemerkung: null,
          kommunikationsverbindungen: [
            {
              typ: {
                id: 10152,
                referenznummer: '10152',
                land: '00',
                referenzliste: 'RKOM',
                schluessel: '02',
                sortier: '02',
                ktext: 'Tel',
                ltext: '',
                gueltigVon: 2007,
                gueltigBis: 2099,
                letzteAenderung: '2020-02-08T23:00:00Z',
                eu: null,
                strg: '',
              },
              verbindung: '12',
            },
          ],
        },
        equal: false,
      },
    ],
    kommunikationsverbindung: [
      {
        spl: {
          typ: {
            id: 10152,
            referenznummer: '10152',
            land: '00',
            referenzliste: 'RKOM',
            schluessel: '02',
            sortier: '02',
            ktext: 'Tel',
            ltext: '',
            gueltigVon: 2007,
            gueltigBis: 2099,
            letzteAenderung: '2020-02-08T23:00:00Z',
            eu: null,
            strg: '',
          },
          verbindung: '0123',
        },
        spb: null,
        equal: false,
      },
      {
        spl: null,
        spb: {
          typ: {
            id: 10152,
            referenznummer: '10152',
            land: '00',
            referenzliste: 'RKOM',
            schluessel: '02',
            sortier: '02',
            ktext: 'Tel',
            ltext: '',
            gueltigVon: 2007,
            gueltigBis: 2099,
            letzteAenderung: '2020-02-08T23:00:00Z',
            eu: null,
            strg: '',
          },
          verbindung: '01234',
        },
        equal: false,
      },
      {
        spl: null,
        spb: {
          typ: {
            id: 10153,
            referenznummer: '10153',
            land: '00',
            referenzliste: 'RKOM',
            schluessel: '03',
            sortier: '03',
            ktext: 'Mobil',
            ltext: '',
            gueltigVon: 2007,
            gueltigBis: 2099,
            letzteAenderung: '2020-02-08T23:00:00Z',
            eu: null,
            strg: '',
          },
          verbindung: '86479',
        },
        equal: false,
      },
    ],
  };

  const routeData$ = new BehaviorSubject({ vergleich });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UebernehmeBetriebsstaetteComponent,
        MockComponents(
          VergleichszeileComponent,
          VergleichstabelleNummernComponent,
          VergleichstabelleAnsprechpartnerComponent,
          VergleichstabelleKommunikationComponent,
          UebernehmeAnlagenListeComponent,
          UebernehmeQuellenListeComponent,
          UebernahmeDialogComponent
        ),
        MockPipe(EllipsizePipe),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$,
          },
        },
        {
          provide: QuellenZuordungService,
          useValue: MockService(QuellenZuordungService),
        },
        MockProvider(StammdatenUebernahmeRestControllerService),
      ],
      imports: [MockModule(ClarityModule)],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UebernehmeBetriebsstaetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('count', () => {
    it('should count allgemein', () => {
      expect(component.countAllgemein()).toBe(9);
    });

    it('should count AbfallerzeugerNummern', () => {
      expect(component.countAllgemeinAbfallerzeugerNr()).toBe(1);
    });

    it('should count EinleiterNummer', () => {
      expect(component.countAllgemeinEinleiterNr()).toBe(1);
    });

    it('should count Adresse', () => {
      expect(component.countAdresse()).toBe(8);
    });

    it('should count Kommunikation', () => {
      expect(component.countKommunikation()).toBe(3);
    });

    it('should count Ansprechpersonen', () => {
      expect(component.countAnsprechpersonen()).toBe(1);
    });
  });
});
