import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import {
  AnsprechpartnerVectorDto,
  BetriebsstaetteUebernahmeDto,
  KommunikationsverbindungVectorDto,
  ReferenzVectorDto,
  SpbBetriebsstaetteVergleichDto,
  StammdatenUebernahmeRestControllerService,
  StringVectorDto,
} from '@api/stammdaten-uebernahme';
import { GeoPunktDto } from '@api/stammdaten';
import {
  buildingIcon,
  ClarityIcons,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(buildingIcon, timesIcon, floppyIcon);
@Component({
  selector: 'app-uebernehme-betriebsstaette',
  templateUrl: './uebernehme-betriebsstaette.component.html',
  styleUrls: ['./uebernehme-betriebsstaette.component.scss'],
})
export class UebernehmeBetriebsstaetteComponent implements OnInit {
  vergleich: SpbBetriebsstaetteVergleichDto;
  quellenDisplay: boolean;
  uebernahmeDialogOpen = false;

  nameUebernehmen = false;
  inbetriebnahmeUebernehmen = false;
  betriebsstatusUebernehmen = false;
  betriebsstatusSeitUebernehmen = false;
  geoPunktUebernehmen = false;

  abfallerzeugerNummernUebernehmen = false;
  ansprechpartnerUebernehmen = false;
  einleiterNummernUebernehmen = false;
  direkteinleiterUebernehmen = false;
  flusseinzugsgebietUebernehmen = false;
  indirekteinleiterUebernehmen = false;
  kommunikationsverbindungenUebernehmen = false;
  naceCodeUebernehmen = false;

  strasseUebernehmen = false;
  hausNrUebernehmen = false;
  plzUebernehmen = false;
  ortUebernehmen = false;
  ortsteilUebernehmen = false;
  postfachUebernehmen = false;
  postfachPlzUebernehmen = false;
  landIsoCodeUebernehmen = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly quellenZuordungService: QuellenZuordungService,
    private readonly alertService: AlertService,
    private readonly location: Location,
    private readonly service: StammdatenUebernahmeRestControllerService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.vergleich = data.vergleich;
      this.readQuellenDisplay(
        this.route.snapshot.params.berichtsjahr,
        this.route.snapshot.params.land
      );
    });
  }

  readQuellenDisplay(berichtsjahr: string, land: string): void {
    this.quellenZuordungService
      .quellenSindAnBetriebsstaetten(berichtsjahr, land)
      .subscribe(
        (ret) => {
          this.quellenDisplay = ret;
        },
        (error) => this.alertService.setAlert(error)
      );
  }

  back(): void {
    this.location.back();
  }

  getGeoPunktText(geoPunkt: GeoPunktDto): string {
    if (geoPunkt == null) {
      return null;
    }
    return geoPunkt.epsgCode.ktext + ' ' + geoPunkt.nord + '/' + geoPunkt.ost;
  }

  countAllgemein(): number {
    return (
      Object.values(this.vergleich.allgemein).filter((value) => !value.equal)
        .length - 2
    );
    // Minus 2 für abfallerzeugerNummern und einleiterNummern Listen
  }

  countAllgemeinAbfallerzeugerNr(): number {
    return this.vergleich.allgemein.abfallerzeugerNummern.filter(
      (nummer: StringVectorDto) => !nummer.equal
    ).length;
  }

  countAllgemeinEinleiterNr(): number {
    return this.vergleich.allgemein.einleiterNummern.filter(
      (nummer: StringVectorDto) => !nummer.equal
    ).length;
  }

  countAdresse(): number {
    return Object.values(this.vergleich.adresse).filter(
      (value: ReferenzVectorDto | StringVectorDto) => !value.equal
    ).length;
  }

  countKommunikation(): number {
    return this.vergleich.kommunikationsverbindung.filter(
      (kommunikation: KommunikationsverbindungVectorDto) => !kommunikation.equal
    ).length;
  }

  countAnsprechpersonen(): number {
    return this.vergleich.ansprechpartner.filter(
      (ansprechpartner: AnsprechpartnerVectorDto) => !ansprechpartner.equal
    ).length;
  }

  uebernehmen($event: boolean): void {
    if ($event) {
      const uebernahme: BetriebsstaetteUebernahmeDto = {
        id: this.vergleich.id,
        spbId: this.vergleich.spbId,

        name: this.nameUebernehmen,
        geoPunkt: this.geoPunktUebernehmen,
        betriebsstatus: this.betriebsstatusUebernehmen,
        betriebsstatusSeit: this.betriebsstatusSeitUebernehmen,
        inbetriebnahme: this.inbetriebnahmeUebernehmen,

        abfallerzeugerNummern: this.abfallerzeugerNummernUebernehmen,
        ansprechpartner: this.ansprechpartnerUebernehmen,
        einleiterNummern: this.einleiterNummernUebernehmen,
        direkteinleiter: this.direkteinleiterUebernehmen,
        flusseinzugsgebiet: this.flusseinzugsgebietUebernehmen,
        indirekteinleiter: this.indirekteinleiterUebernehmen,
        kommunikationsverbindungen: this.kommunikationsverbindungenUebernehmen,
        naceCode: this.naceCodeUebernehmen,

        adresse: {
          strasse: this.strasseUebernehmen,
          hausNr: this.hausNrUebernehmen,
          plz: this.plzUebernehmen,
          ort: this.ortUebernehmen,
          ortsteil: this.ortsteilUebernehmen,
          postfach: this.postfachUebernehmen,
          postfachPlz: this.postfachPlzUebernehmen,
          landIsoCode: this.landIsoCodeUebernehmen,
        },
      };
      this.service.uebernehmeBetriebsstaette(uebernahme).subscribe(
        () => {
          this.back();
        },
        () => {
          this.alertService.setAlert(
            'Es ist ein Problem beim Übernehmen der Daten aufgetreten.'
          );
        }
      );
    }
  }

  canUebernehmen(): boolean {
    return this.vergleich.hasChanges;
  }
}
