import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { UebernehmeBetriebsstaetteComponent } from './component/uebernehme-betriebsstaette/uebernehme-betriebsstaette.component';
import { UebernahmeRoutingModule } from '@/stammdaten/uebernahme/routing/uebernahme-routing.module';
import { BaseModule } from '@/base/base.module';
import { VergleichszeileComponent } from './component/vergleichszeile/vergleichszeile.component';
import { VergleichstabelleKommunikationComponent } from './component/vergleichstabelle-kommunikation/vergleichstabelle-kommunikation.component';
import { VergleichstabelleAnsprechpartnerComponent } from './component/vergleichstabelle-ansprechpartner/vergleichstabelle-ansprechpartner.component';
import { VergleichstabelleNummernComponent } from './component/vergleichstabelle-nummern/vergleichstabelle-nummern.component';
import { UebernehmeAnlageComponent } from './component/uebernehme-anlage/uebernehme-anlage.component';
import { UebernehmeAnlagenteilComponent } from './component/uebernehme-anlagenteil/uebernehme-anlagenteil.component';
import { UebernehmeBetreiberComponent } from './component/uebernehme-betreiber/uebernehme-betreiber.component';
import { UebernehmeQuelleComponent } from './component/uebernehme-quelle/uebernehme-quelle.component';
import { VergleichstabelleLeistungenComponent } from './component/vergleichstabelle-leistungen/vergleichstabelle-leistungen.component';
import { UebernehmeAnlagenListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlagen-liste/uebernehme-anlagen-liste.component';
import { UebernehmeQuellenListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-quellen-liste/uebernehme-quellen-liste.component';
import { UebernehmeAnlagenteileListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlagenteile-liste/uebernehme-anlagenteile-liste.component';
import { UebernehmeAnlagenQuellenListeComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlagen-quellen-liste/uebernehme-anlagen-quellen-liste.component';
import { UebernahmeDialogComponent } from './component/uebernahme-dialog/uebernahme-dialog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    UebernehmeBetriebsstaetteComponent,
    VergleichszeileComponent,
    VergleichstabelleKommunikationComponent,
    VergleichstabelleAnsprechpartnerComponent,
    VergleichstabelleNummernComponent,
    UebernehmeAnlageComponent,
    UebernehmeAnlagenteilComponent,
    UebernehmeBetreiberComponent,
    UebernehmeQuelleComponent,
    VergleichstabelleLeistungenComponent,
    UebernehmeAnlagenListeComponent,
    UebernehmeQuellenListeComponent,
    UebernehmeAnlagenteileListeComponent,
    UebernehmeAnlagenQuellenListeComponent,
    UebernahmeDialogComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    ClarityModule,
    BaseModule,
    UebernahmeRoutingModule,
  ],
})
export class UebernahmeModule {}
