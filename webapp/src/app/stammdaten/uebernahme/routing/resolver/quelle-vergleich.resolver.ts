import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbQuelleVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

@Injectable({
  providedIn: 'root',
})
export class QuelleVergleichResolver implements Resolve<SpbQuelleVergleichDto> {
  constructor(
    private service: StammdatenUebernahmeRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbQuelleVergleichDto> {
    const id: number = route.params.id;

    return this.service
      .readQuelleSpbVergleichsobjekt(id)
      .pipe(this.alertService.catchError());
  }
}
