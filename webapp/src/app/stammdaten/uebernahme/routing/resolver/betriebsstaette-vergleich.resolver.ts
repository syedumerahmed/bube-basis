import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbBetriebsstaetteVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

@Injectable({
  providedIn: 'root',
})
export class BetriebsstaetteVergleichResolver
  implements Resolve<SpbBetriebsstaetteVergleichDto>
{
  constructor(
    private service: StammdatenUebernahmeRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<SpbBetriebsstaetteVergleichDto> {
    const betriebsstaetteId: number = route.params.id;

    return this.service
      .readBetriebsstaetteSpbVergleichsobjekt(betriebsstaetteId)
      .pipe(this.alertService.catchError());
  }
}
