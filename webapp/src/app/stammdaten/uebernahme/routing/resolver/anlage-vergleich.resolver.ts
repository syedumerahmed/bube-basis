import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlageVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

@Injectable({
  providedIn: 'root',
})
export class AnlageVergleichResolver implements Resolve<SpbAnlageVergleichDto> {
  constructor(
    private service: StammdatenUebernahmeRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbAnlageVergleichDto> {
    const id: number = route.params.id;

    return this.service
      .readAnlageSpbVergleichsobjekt(id)
      .pipe(this.alertService.catchError());
  }
}
