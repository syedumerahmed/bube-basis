import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbBetreiberVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

@Injectable({
  providedIn: 'root',
})
export class BetreiberVergleichResolver
  implements Resolve<SpbBetreiberVergleichDto>
{
  constructor(
    private service: StammdatenUebernahmeRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<SpbBetreiberVergleichDto> {
    const id: number = route.params.id;

    return this.service
      .readBetreiberSpbVergleichsobjekt(id)
      .pipe(this.alertService.catchError());
  }
}
