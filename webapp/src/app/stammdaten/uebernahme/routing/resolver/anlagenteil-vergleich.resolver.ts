import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  SpbAnlagenteilVergleichDto,
  StammdatenUebernahmeRestControllerService,
} from '@api/stammdaten-uebernahme';

@Injectable({
  providedIn: 'root',
})
export class AnlagenteilVergleichResolver
  implements Resolve<SpbAnlagenteilVergleichDto>
{
  constructor(
    private service: StammdatenUebernahmeRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<SpbAnlagenteilVergleichDto> {
    const id: number = route.params.id;

    return this.service
      .readAnlagenteilSpbVergleichsobjekt(id)
      .pipe(this.alertService.catchError());
  }
}
