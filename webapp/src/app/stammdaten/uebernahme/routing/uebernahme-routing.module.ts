import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UebernehmeBetriebsstaetteComponent } from '@/stammdaten/uebernahme/component/uebernehme-betriebsstaette/uebernehme-betriebsstaette.component';
import { BetriebsstaetteVergleichResolver } from '@/stammdaten/uebernahme/routing/resolver/betriebsstaette-vergleich.resolver';
import { UebernehmeAnlageComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlage/uebernehme-anlage.component';
import { AnlageVergleichResolver } from '@/stammdaten/uebernahme/routing/resolver/anlage-vergleich.resolver';
import { UebernehmeAnlagenteilComponent } from '@/stammdaten/uebernahme/component/uebernehme-anlagenteil/uebernehme-anlagenteil.component';
import { AnlagenteilVergleichResolver } from '@/stammdaten/uebernahme/routing/resolver/anlagenteil-vergleich.resolver';
import { QuelleVergleichResolver } from '@/stammdaten/uebernahme/routing/resolver/quelle-vergleich.resolver';
import { UebernehmeBetreiberComponent } from '@/stammdaten/uebernahme/component/uebernehme-betreiber/uebernehme-betreiber.component';
import { BetreiberVergleichResolver } from '@/stammdaten/uebernahme/routing/resolver/betreiber-vergleich.resolver';
import { UebernehmeQuelleComponent } from '@/stammdaten/uebernahme/component/uebernehme-quelle/uebernehme-quelle.component';

const routes: Routes = [
  {
    path: 'betriebsstaette/:id',
    component: UebernehmeBetriebsstaetteComponent,
    resolve: {
      vergleich: BetriebsstaetteVergleichResolver,
    },
  },
  {
    path: 'anlage/:id',
    component: UebernehmeAnlageComponent,
    resolve: {
      vergleich: AnlageVergleichResolver,
    },
  },
  {
    path: 'anlagenteil/:id',
    component: UebernehmeAnlagenteilComponent,
    resolve: {
      vergleich: AnlagenteilVergleichResolver,
    },
  },
  {
    path: 'quelle/:id',
    component: UebernehmeQuelleComponent,
    resolve: {
      vergleich: QuelleVergleichResolver,
    },
  },
  {
    path: 'betreiber/:id',
    component: UebernehmeBetreiberComponent,
    resolve: {
      vergleich: BetreiberVergleichResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    BetriebsstaetteVergleichResolver,
    AnlageVergleichResolver,
    AnlagenteilVergleichResolver,
    QuelleVergleichResolver,
    BetreiberVergleichResolver,
  ],
})
export class UebernahmeRoutingModule {}
