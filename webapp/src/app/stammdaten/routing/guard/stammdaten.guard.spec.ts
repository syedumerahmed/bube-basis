import { inject, TestBed } from '@angular/core/testing';

import { StammdatenGuard } from './stammdaten.guard';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { MockProviders } from 'ng-mocks';

describe('StammdatenGuard', () => {
  let guard: StammdatenGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        StammdatenGuard,
        MockProviders(Router, Location, AlertService),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
    });
    guard = TestBed.inject(StammdatenGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should allow', inject([BenutzerProfil], async (benutzerProfil) => {
    jest
      .spyOn(benutzerProfil, 'canSeeStammdatenModule', 'get')
      .mockReturnValue(true);
    const isAuthorized = await guard.canActivate(
      {} as ActivatedRouteSnapshot,
      { url: '/betriebsstaetten' } as RouterStateSnapshot
    );
    expect(isAuthorized).toBe(true);
  }));

  it('should deny', inject(
    [BenutzerProfil, Router, Location, AlertService],
    async (
      benutzerProfil,
      router: Router,
      location: Location,
      alertService
    ) => {
      jest
        .spyOn(benutzerProfil, 'canSeeStammdatenModule', 'get')
        .mockReturnValue(false);
      jest.spyOn(router, 'navigate').mockResolvedValue(true);
      const isAuthorized = await guard.canActivate(
        {} as ActivatedRouteSnapshot,
        { url: '/betriebsstaetten' } as RouterStateSnapshot
      );

      expect(isAuthorized).toBe(false);
      expect(router.navigate).toHaveBeenCalledWith(['/404']);
      expect(location.replaceState).toHaveBeenCalledWith('/betriebsstaetten');
      expect(alertService.setAlert).toHaveBeenCalled();
    }
  ));
});
