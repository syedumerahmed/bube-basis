import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable({
  providedIn: 'root',
})
export class RedirectGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree {
    const isRoot: boolean = state.url == '/';

    if (
      this.benutzerProfil.isBehoerdenbenutzer &&
      (isRoot || state.url.startsWith('/stammdatenbetreiber/'))
    ) {
      return this.router.createUrlTree(['stammdaten']);
    }

    if (
      !this.benutzerProfil.isBehoerdenbenutzer &&
      (isRoot || state.url.startsWith('/stammdaten/'))
    ) {
      return this.router.createUrlTree(['stammdatenbetreiber']);
    }

    return true;
  }
}
