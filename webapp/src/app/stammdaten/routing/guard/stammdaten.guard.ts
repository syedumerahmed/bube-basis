import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Location } from '@angular/common';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable({ providedIn: 'root' })
export class StammdatenGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly location: Location,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly alertService: AlertService
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    const isAuthorized = this.benutzerProfil.canSeeStammdatenModule;
    if (!isAuthorized) {
      this.alertService.setAlert('Das gesuchte Element wurde nicht gefunden');
      // return this.router.createUrlTree(['/404'], {skipLocationChange: true});

      // skipLocationChange funktioniert nicht in Guards - siehe
      // https://github.com/angular/angular/issues/27148
      // https://github.com/angular/angular/issues/35764
      // https://github.com/angular/angular/issues/17004
      // Workaround/Hack von https://github.com/angular/angular/issues/16981#issuecomment-549330207
      // führt dazu, dass für eine Millisekunde die URL zu sehen ist
      await this.router
        .navigate(['/404'])
        .then(() => this.location.replaceState(state.url));
    }
    return isAuthorized;
  }
}
