import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StammdatenComponent } from '../component/stammdaten/stammdaten.component';
import { BerichtsjahrListResolver } from '@/referenzdaten/routing/resolver/berichtsjahr-list-resolver';
import { BetriebsstaetteModule } from '@/stammdaten/betriebsstaette/betriebsstaette.module';
import { BerichtsjahrGuard } from '@/base/routing/guard/berichtsjahr.guard';
import { UebernahmeModule } from '@/stammdaten/uebernahme/uebernahme.module';
import { StammdatenUebernahmeComponent } from '@/stammdaten/component/stammdaten-uebernahme/stammdaten-uebernahme.component';

const routes: Routes = [
  {
    path: 'jahr/:berichtsjahr',
    component: StammdatenComponent,
    canActivate: [BerichtsjahrGuard],
    resolve: {
      berichtsjahre: BerichtsjahrListResolver,
    },
    loadChildren: () => BetriebsstaetteModule,
  },
  {
    path: 'jahr/:berichtsjahr/uebernahme',
    component: StammdatenUebernahmeComponent,
    loadChildren: () => UebernahmeModule,
  },
  {
    path: '',
    redirectTo: 'jahr/',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class StammdatenRoutingModule {}
