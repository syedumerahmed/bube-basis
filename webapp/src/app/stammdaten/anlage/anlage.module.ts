import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseModule } from '@/base/base.module';

import { AnlageDetailsComponent } from './component/anlage-details/anlage-details.component';
import { ZustaendigeBehoerdenComponent } from './component/zustaendige-behoerden/zustaendige-behoerden.component';
import { AnlagenListeComponent } from './component/anlagen-liste/anlagen-liste.component';
import { AnlageMainComponent } from './component/anlage-main/anlage-main.component';
import { AnlagenteileListeComponent } from './component/anlagenteile-liste/anlagenteile-liste.component';
import { AnlagenteilDetailsComponent } from './component/anlagenteil-details/anlagenteil-details.component';
import { AnlageAllgemeinComponent } from './component/anlage-allgemein/anlage-allgemein.component';
import { AnlageComponent } from './component/anlage/anlage.component';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { QuelleModule } from '@/stammdaten/quelle/quelle.module';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { AnlageRoutingModule } from '@/stammdaten/anlage/routing/anlage-routing.module';

@NgModule({
  declarations: [
    AnlageDetailsComponent,
    ZustaendigeBehoerdenComponent,
    AnlagenListeComponent,
    AnlageMainComponent,
    AnlagenteileListeComponent,
    AnlagenteilDetailsComponent,
    AnlageAllgemeinComponent,
    AnlageComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    ReactiveFormsModule,

    BaseModule,
    QuelleModule,
    AnlageRoutingModule,
    BasedomainModule,
  ],
  exports: [AnlagenListeComponent, ZustaendigeBehoerdenComponent],
})
export class AnlageModule {}
