import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetriebsstatusListResolver } from '@/referenzdaten/routing/resolver/betriebsstatus-list-resolver';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { VorschriftenListResolver } from '@/referenzdaten/routing/resolver/vorschriften-list-resolver';
import { VertraulichkeitsgrundListResolver } from '@/referenzdaten/routing/resolver/vertraulichkeitsgrund-list-resolver';
import { ZustaendigkeitListResolver } from '@/referenzdaten/routing/resolver/zustaendigkeit-list-resolver';
import { EpsgListResolver } from '@/referenzdaten/routing/resolver/epsg-list-resolver';
import { QuelleDetailsComponent } from '@/stammdaten/quelle/component/quelle-details/quelle-details.component';
import { QuellenArtListResolver } from '@/referenzdaten/routing/resolver/quellen-art-list-resolver';
import { Nummer4BImSchVListResolver } from '@/referenzdaten/routing/resolver/nummer4bimschv-list-resolver';
import { PrtrTaetigkeitenListResolver } from '@/referenzdaten/routing/resolver/prtr-taetigkeiten-list-resolver';
import { IedTaetigkeitenListResolver } from '@/referenzdaten/routing/resolver/ied-taetigkeiten-list-resolver';
import { AnlageNeuResolver } from '@/stammdaten/anlage/routing/resolver/anlage-neu-resolver';
import { AnlagenteilNeuResolver } from '@/stammdaten/anlage/routing/resolver/anlagenteil-neu-resolver';
import { AnlagenteilKopierenResolver } from '@/stammdaten/anlage/routing/resolver/anlagenteil-kopieren-resolver';
import { AnlageKopierenResolver } from '@/stammdaten/anlage/routing/resolver/anlage-kopieren-resolver';
import { QuelleAnAnlageNeuGuard } from '@/stammdaten/anlage/routing/guard/quelle-an-anlage-neu-guard';
import { AnlageNeuUebernehmenResolver } from '@/stammdaten/anlage/routing/resolver/anlage-neu-uebernehmen-resolver.service';
import { AnlagenteilNeuUebernehemenResolver } from '@/stammdaten/anlage/routing/resolver/anlagenteil-uebernehmen-resolver';
import { AnlageComponent } from '@/stammdaten/anlage/component/anlage/anlage.component';
import { LeistungseinheitenListResolver } from '@/referenzdaten/routing/resolver/leistungseinheiten-list-resolver';
import { AnlageMainComponent } from '@/stammdaten/anlage/component/anlage-main/anlage-main.component';
import { AnlagenteilDetailsComponent } from '@/stammdaten/anlage/component/anlagenteil-details/anlagenteil-details.component';
import { AnlagenteilResolver } from '@/stammdaten/anlage/routing/resolver/anlagenteil-resolver';
import { AnlagenResolver } from '@/stammdaten/anlage/routing/resolver/anlagen-resolver';
import { QuellenAnlageNeuResolver } from '@/stammdaten/quelle/routing/resolver/quellen-anlage-neu-resolver';
import { QuelleKopierenResolver } from '@/stammdaten/quelle/routing/resolver/quelle-kopieren-resolver';
import { QuelleNeuUebernehmenResolver } from '@/stammdaten/quelle/routing/resolver/quelle-uebernehmen-resolver.service';
import { QuellenAnlageResolver } from '@/stammdaten/quelle/routing/resolver/quellen-anlage-resolver';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const anlageReferenzenResolvers = {
  betriebsstatusListe: BetriebsstatusListResolver,
  vorschriftenListe: VorschriftenListResolver,
  nummer4BImSchVTaetigkeitenListe: Nummer4BImSchVListResolver,
  prtrTaetigkeitenListe: PrtrTaetigkeitenListResolver,
  iedTaetigkeitenListe: IedTaetigkeitenListResolver,
  vertraulichkeitsgrundListe: VertraulichkeitsgrundListResolver,
  behoerdenListe: BehoerdenListResolver,
  zustaendigkeitenListe: ZustaendigkeitListResolver,
  einheitenListe: LeistungseinheitenListResolver,
  epsgCodeListe: EpsgListResolver,
};

const quelleReferenzenResolvers = {
  quellenArtListe: QuellenArtListResolver,
  epsgCodeListe: EpsgListResolver,
};

const routes: Routes = [
  {
    path: 'neu',
    component: AnlageComponent,
    resolve: {
      anlage: AnlageNeuResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: AnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
    ],
  },
  {
    path: 'kopieren',
    component: AnlageComponent,
    resolve: {
      anlage: AnlageKopierenResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: AnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
    ],
  },
  {
    path: 'uebernehmen',
    component: AnlageComponent,
    resolve: {
      anlage: AnlageNeuUebernehmenResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: AnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
    ],
  },
  {
    path: 'nummer/:anlageNummer',
    component: AnlageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      anlage: AnlagenResolver,
      ...anlageReferenzenResolvers,
    },
    children: [
      {
        path: '',
        component: AnlageMainComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
      {
        path: 'anlagenteil/neu',
        component: AnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: AnlagenteilNeuResolver,
        },
      },
      {
        path: 'anlagenteil/kopieren',
        component: AnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: AnlagenteilKopierenResolver,
        },
      },
      {
        path: 'anlagenteil/uebernehmen',
        component: AnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: AnlagenteilNeuUebernehemenResolver,
        },
      },
      {
        path: 'anlagenteil/nummer/:anlagenteilNummer',
        component: AnlagenteilDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          anlagenteil: AnlagenteilResolver,
        },
      },
      {
        path: 'quelle/neu',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnAnlageNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuellenAnlageNeuResolver,
          ...quelleReferenzenResolvers,
        },
      },
      {
        path: 'quelle/kopieren',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnAnlageNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuelleKopierenResolver,
          ...quelleReferenzenResolvers,
        },
      },
      {
        path: 'quelle/uebernehmen',
        component: QuelleDetailsComponent,
        canActivate: [QuelleAnAnlageNeuGuard],
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuelleNeuUebernehmenResolver,
          ...quelleReferenzenResolvers,
        },
      },
      {
        path: 'quelle/nummer/:quelleNummer',
        component: QuelleDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          quelle: QuellenAnlageResolver,
          ...quelleReferenzenResolvers,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    AnlagenResolver,
    AnlageNeuResolver,
    AnlagenteilResolver,
    AnlageKopierenResolver,
    AnlageNeuUebernehmenResolver,
    AnlagenteilNeuResolver,
    AnlagenteilKopierenResolver,
    AnlagenteilNeuUebernehemenResolver,
    QuellenAnlageNeuResolver,
    QuelleKopierenResolver,
    QuelleNeuUebernehmenResolver,
    QuellenAnlageResolver,
  ],
})
export class AnlageRoutingModule {}
