import { AnlagenResolver } from './anlagen-resolver';
import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockProviders } from 'ng-mocks';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import spyOn = jest.spyOn;
import { AnlagenRestControllerService } from '@api/stammdaten';

describe('AnlagenResolver', () => {
  let resolver: AnlagenResolver;
  let setAlert: jest.Mock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AnlagenResolver,
        MockProviders(
          Router,
          AnlagenRestControllerService,
          AlertService,
          Location
        ),
      ],
    });
    resolver = TestBed.inject(AnlagenResolver);
    setAlert = jest.fn();
    spyOn(TestBed.inject(AlertService), 'catchError').mockReturnValue(
      catchError(setAlert)
    );
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { anlageNummer: 'neu' },
        parent: {
          parent: { data: { betriebsstaette: { id: 1 } } },
        },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [AnlagenRestControllerService],
      (anlagenService: AnlagenRestControllerService) => {
        spyOn(anlagenService, 'readAnlage').mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot);

        expect(anlagenService.readAnlage).toHaveBeenCalledWith(1, 'neu');
      }
    ));
  });
});
