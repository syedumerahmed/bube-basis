import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { AnlageDto } from '@api/stammdaten';

@Injectable()
export class AnlageKopierenResolver implements Resolve<AnlageDto> {
  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {}

  private static copyAnlage(anlage: AnlageDto): AnlageDto {
    return {
      ...anlage,
      id: null,
      name: 'Kopie - ' + anlage.name,
      anlageNr: null,
      localId: null,
      letzteAenderung: null,
      ersteErfassung: null,
      betreiberdatenUebernommen: false,
    };
  }

  resolve(): Observable<AnlageDto> {
    const anlage = this.router.getCurrentNavigation().extras.state?.anlage;
    if (!anlage) {
      this.location.back();
      return EMPTY;
    }
    return of(AnlageKopierenResolver.copyAnlage(anlage));
  }
}
