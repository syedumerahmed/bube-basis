import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { AnlagenteilDto } from '@api/stammdaten';

@Injectable()
export class AnlagenteilNeuUebernehemenResolver
  implements Resolve<AnlagenteilDto>
{
  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {}

  resolve(): Observable<AnlagenteilDto> {
    const anlagenteil =
      this.router.getCurrentNavigation().extras.state?.anlagenteil;
    if (!anlagenteil) {
      this.location.back();
      return EMPTY;
    }
    return of(anlagenteil);
  }
}
