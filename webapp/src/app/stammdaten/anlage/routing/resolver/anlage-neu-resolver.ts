import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AnlageDto } from '@api/stammdaten';

@Injectable()
export class AnlageNeuResolver implements Resolve<AnlageDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<AnlageDto> {
    return of({
      parentBetriebsstaetteId: route.parent.parent.data.betriebsstaette.id,
      name: null,
      anlageNr: null,
    });
  }
}
