import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockService } from 'ng-mocks';
import { AlertService } from '@/base/service/alert.service';
import { AnlagenteilResolver } from './anlagenteil-resolver';
import { EMPTY } from 'rxjs';
import spyOn = jest.spyOn;
import { AnlagenteilRestControllerService } from '@api/stammdaten';

describe('AnlagenteilResolver', () => {
  let resolver: AnlagenteilResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AnlagenteilResolver,
        { provide: Router, useValue: MockService(Router) },
        {
          provide: AnlagenteilRestControllerService,
          useValue: MockService(AnlagenteilRestControllerService),
        },
        { provide: AlertService, useValue: MockService(AlertService) },
      ],
    });
    resolver = TestBed.inject(AnlagenteilResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { anlagenteilNummer: 'neu' },
        parent: {
          parent: { parent: { data: { betriebsstaette: { id: 1 } } } },
          data: { anlage: { id: 2 } },
        },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [AnlagenteilRestControllerService],
      (anlagenteilService: AnlagenteilRestControllerService) => {
        spyOn(anlagenteilService, 'readAnlagenteil').mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot, null);

        expect(anlagenteilService.readAnlagenteil).toHaveBeenCalledWith(
          1,
          2,
          'neu'
        );
      }
    ));
  });
});
