import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { catchError } from 'rxjs/operators';
import {
  AnlagenteilDto,
  AnlagenteilRestControllerService,
} from '@api/stammdaten';

@Injectable()
export class AnlagenteilResolver implements Resolve<AnlagenteilDto> {
  constructor(
    private service: AnlagenteilRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<AnlagenteilDto> {
    const anlageId = route.parent.data.anlage.id;
    const betriebsstaetteId =
      route.parent.parent.parent.data.betriebsstaette.id;
    const nummer = route.params.anlagenteilNummer;

    return this.service
      .readAnlagenteil(betriebsstaetteId, anlageId, nummer)
      .pipe(
        catchError((err) => {
          if (err.status === 404) {
            this.router.navigateByUrl(
              state.url.replace('/anlagenteil/nummer/' + nummer, '')
            );
          }
          this.alertService.setAlert(err);
          return EMPTY;
        })
      );
  }
}
