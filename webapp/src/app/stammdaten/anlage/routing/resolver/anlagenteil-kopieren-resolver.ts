import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { AnlagenteilDto } from '@api/stammdaten';

@Injectable()
export class AnlagenteilKopierenResolver implements Resolve<AnlagenteilDto> {
  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {}

  private static copyAnlagenteil(anlagenteil: AnlagenteilDto): AnlagenteilDto {
    return {
      ...anlagenteil,
      id: null,
      name: 'Kopie - ' + anlagenteil.name,
      anlagenteilNr: null,
      localId: null,
      letzteAenderung: null,
      ersteErfassung: null,
      betreiberdatenUebernommen: false,
    };
  }

  resolve(): Observable<AnlagenteilDto> {
    const anlagenteil =
      this.router.getCurrentNavigation().extras.state?.anlagenteil;
    if (!anlagenteil) {
      this.location.back();
      return EMPTY;
    }
    return of(AnlagenteilKopierenResolver.copyAnlagenteil(anlagenteil));
  }
}
