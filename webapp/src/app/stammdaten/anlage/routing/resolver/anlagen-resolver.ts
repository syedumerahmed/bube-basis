import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { AnlageDto, AnlagenRestControllerService } from '@api/stammdaten';

@Injectable()
export class AnlagenResolver implements Resolve<AnlageDto> {
  constructor(
    private service: AnlagenRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<AnlageDto> {
    const betriebsstaetteId = route.parent.parent.data.betriebsstaette.id;
    const nummer = route.params.anlageNummer;

    return this.service
      .readAnlage(betriebsstaetteId, nummer)
      .pipe(this.alertService.catchError());
  }
}
