import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AnlagenteilDto } from '@api/stammdaten';

@Injectable()
export class AnlagenteilNeuResolver implements Resolve<AnlagenteilDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<AnlagenteilDto> {
    const anlageId = route.parent.data.anlage.id;
    const betriebsstaetteId =
      route.parent.parent.parent.data.betriebsstaette.id;
    return of({
      parentAnlageId: anlageId,
      parentBetriebsstaetteId: betriebsstaetteId,
      name: null,
      anlagenteilNr: null,
    });
  }
}
