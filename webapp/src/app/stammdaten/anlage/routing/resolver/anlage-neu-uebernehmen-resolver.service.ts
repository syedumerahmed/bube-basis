import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Location } from '@angular/common';
import { EMPTY, Observable, of } from 'rxjs';
import { AnlageDto } from '@api/stammdaten';

@Injectable()
export class AnlageNeuUebernehmenResolver implements Resolve<AnlageDto> {
  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {}

  resolve(): Observable<AnlageDto> {
    const anlage = this.router.getCurrentNavigation().extras.state?.anlage;
    if (!anlage) {
      this.location.back();
      return EMPTY;
    }
    return of(anlage);
  }
}
