import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AnlageAllgemeinComponent } from './anlage-allgemein.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder } from '@/base/forms';
import { MockComponents, MockModule } from 'ng-mocks';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { AnlageDto, ReferenzDto, VorschriftDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';

const anlage: AnlageDto = {
  anlageNr: 'ANLA1',
  name: 'Anlage 1',
  parentBetriebsstaetteId: 20,
  id: 21,
  betriebsstatus: { schluessel: '01' } as ReferenzDto,
  betriebsstatusSeit: null,
  inbetriebnahme: null,
  localId: 'localId',
  bemerkung: 'Bemerkung',
  vertraulichkeitsgrund: { schluessel: 'TS' } as ReferenzDto,
  vorschriften: [
    { art: { schluessel: 'V1' } },
    { art: { schluessel: 'V2' } },
  ] as Array<VorschriftDto>,
  geoPunkt: {
    nord: 1,
    ost: 2,
    epsgCode: { schluessel: '01' } as ReferenzDto,
  },
};

describe('AnlageAllgemeinComponent', () => {
  let component: AnlageAllgemeinComponent;
  let fixture: ComponentFixture<AnlageAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AnlageAllgemeinComponent,
        MockComponents(
          TooltipComponent,
          ReferenzSelectComponent,
          GeoPunktComponent
        ),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(AnlageAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = AnlageAllgemeinComponent.createForm(
      formBuilder,
      AnlageAllgemeinComponent.toForm(anlage),
      {
        gueltigBis: 0,
        gueltigVon: 0,
        ktext: '',
        land: '',
        referenzliste: undefined,
        sortier: '',
        schluessel: '2020',
      }
    );
    component.vertraulichkeitsgrundListe = new Referenzliste<
      Required<ReferenzDto>
    >([]);
    component.betriebsstatusListe = new Referenzliste<Required<ReferenzDto>>(
      anlage.vorschriften.map((v) => v.art as Required<ReferenzDto>)
    );
    component.epsgCodeListe = new Referenzliste<Required<ReferenzDto>>([]);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      AnlageAllgemeinComponent.toForm(anlage)
    );
  });
});
