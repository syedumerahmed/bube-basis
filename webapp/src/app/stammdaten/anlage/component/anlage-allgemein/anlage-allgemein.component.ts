import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { AnlageDto, ReferenzDto } from '@api/stammdaten';
import {
  GeoPunktComponent,
  GeoPunktFormData,
} from '@/basedomain/component/geo-punkt/geo-punkt.component';

export interface AnlageAllgemeinFormData {
  name: string;
  anlageNr: string;
  localId: string;
  inbetriebnahme: string;
  betriebsstatus: ReferenzDto;
  bemerkung: string;
  betriebsstatusSeit: string;
  geoPunkt: GeoPunktFormData;
  vertraulichkeitsgrund: ReferenzDto;
}

@Component({
  selector: 'app-anlage-allgemein',
  templateUrl: './anlage-allgemein.component.html',
  styleUrls: ['./anlage-allgemein.component.scss'],
  providers: [DatePipe],
})
export class AnlageAllgemeinComponent
  implements OnInit, AfterViewInit, AfterViewChecked
{
  constructor(private readonly datePipe: DatePipe) {}

  @Input()
  form: FormGroup<AnlageAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;
  @ViewChild(GeoPunktComponent)
  geoPunktComponent: GeoPunktComponent;

  @Input()
  betriebsstatusListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  vertraulichkeitsgrundListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  isAnlagenteil = false;

  today: string;

  get nummerTooltip(): string {
    if (this.isAnlagenteil) {
      return 'Anlagenteil-Nummer';
    }
    return 'Anlagennummer';
  }

  static toForm(anlageDto: AnlageDto): AnlageAllgemeinFormData {
    const {
      vertraulichkeitsgrund,
      anlageNr,
      inbetriebnahme,
      betriebsstatusSeit,
      name,
      localId,
      bemerkung,
      betriebsstatus,
    } = anlageDto;
    return {
      name,
      anlageNr,
      localId,
      inbetriebnahme: FormUtil.fromIsoDate(inbetriebnahme),
      betriebsstatus: betriebsstatus,
      bemerkung,
      betriebsstatusSeit: FormUtil.fromIsoDate(betriebsstatusSeit),
      geoPunkt: GeoPunktComponent.toForm(anlageDto.geoPunkt),
      vertraulichkeitsgrund: vertraulichkeitsgrund,
    };
  }

  static createForm(
    fb: FormBuilder,
    anlage: AnlageAllgemeinFormData,
    selectedBerichtsjahr: ReferenzDto
  ): FormGroup<AnlageAllgemeinFormData> {
    return fb.group<AnlageAllgemeinFormData>({
      name: [anlage.name, [Validators.required, Validators.maxLength(120)]],
      anlageNr: [
        anlage.anlageNr,
        [Validators.required, Validators.maxLength(20)],
      ],
      bemerkung: [anlage.bemerkung, Validators.maxLength(1000)],
      localId: [anlage.localId, Validators.maxLength(40)],
      inbetriebnahme: [anlage.inbetriebnahme, BubeValidators.date],
      betriebsstatus: [
        anlage.betriebsstatus,
        [
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      betriebsstatusSeit: [anlage.betriebsstatusSeit, BubeValidators.date],
      geoPunkt: GeoPunktComponent.createForm(
        fb,
        anlage.geoPunkt,
        selectedBerichtsjahr
      ),
      vertraulichkeitsgrund: [
        anlage.vertraulichkeitsgrund,
        [
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
    });
  }

  ngOnInit(): void {
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm?.markAsTouched();
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }

  get geoPunktForm(): FormGroup<GeoPunktFormData> {
    return this.form.get('geoPunkt') as FormGroup<GeoPunktFormData>;
  }
}
