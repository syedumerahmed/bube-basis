import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  AnlageListItemDto,
  AnlagenRestControllerService,
  BetriebsstaetteDto,
} from '@api/stammdaten';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(trashIcon, factoryIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-anlagen-liste',
  templateUrl: './anlagen-liste.component.html',
  styleUrls: ['./anlagen-liste.component.scss'],
})
export class AnlagenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  anlagenListe: AnlageListItemDto[];
  betriebsstaette: BetriebsstaetteDto;
  loading = true;
  loeschenDialog: boolean;
  selected: AnlageListItemDto[] = [];
  anlageId: number;
  loescht = false;

  constructor(
    private route: ActivatedRoute,
    private anlagenRestControllerService: AnlagenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaette = data.betriebsstaette;
      this.refresh();
    });
  }

  refresh(): void {
    if (this.neueBetriebsstaette()) {
      this.anlagenListe = [];
      this.loading = false;
      return;
    }

    this.loading = true;
    this.anlagenRestControllerService
      .readAllAnlage(this.betriebsstaette.id)
      .subscribe(
        (liste) => {
          this.anlagenListe = liste;
          this.loading = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  deleteAnlage(): void {
    this.anlagenRestControllerService.deleteAnlage(this.anlageId).subscribe(
      () => {
        this.closeLoeschenDialog();
        // Data Grid refresh
        this.refresh();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  deleteAnlagen(): void {
    this.loescht = true;
    this.anlagenRestControllerService
      .deleteMultipleAnlagen(this.selected.map((anlage) => anlage.id))
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.selected = [];
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  neueBetriebsstaette(): boolean {
    return !this.betriebsstaette.id;
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.anlageId = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(anlageId: number): void {
    this.anlageId = anlageId;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }
}
