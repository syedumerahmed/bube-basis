import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnlagenListeComponent } from './anlagen-liste.component';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  AnlagenRestControllerService,
  BetriebsstaetteDto,
} from '@api/stammdaten';

describe('AnlagenListeComponent', () => {
  const betriebsstaette: BetriebsstaetteDto = {
    adresse: undefined,
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    gemeindekennziffer: undefined,
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  let dataSubject: Subject<Data>;
  let component: AnlagenListeComponent;
  let fixture: ComponentFixture<AnlagenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [AnlagenListeComponent],
      providers: [
        {
          provide: AnlagenRestControllerService,
          useValue: MockService(AnlagenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AnlagenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
