import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnlagenteileListeComponent } from './anlagenteile-liste.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AnlageDto, AnlagenteilRestControllerService } from '@api/stammdaten';

describe('AnlagenteileListeComponent', () => {
  const anlage: AnlageDto = {
    anlageNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    id: 1,
  };

  let dataSubject: Subject<Data>;
  let component: AnlagenteileListeComponent;
  let fixture: ComponentFixture<AnlagenteileListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ anlage });

    await TestBed.configureTestingModule({
      declarations: [AnlagenteileListeComponent],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        {
          provide: AnlagenteilRestControllerService,
          useValue: MockService(AnlagenteilRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { anlage } },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AnlagenteileListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Liste nach kopieren des Parents', () => {
    beforeEach(() => {
      component.anlagenteilListe = [
        { id: 1, nummer: '1', name: 'name' },
        { id: 2, nummer: '2', name: 'name2' },
      ];
      component.anlage = { id: null } as AnlageDto;
      component.refresh();
    });
    it('should have empty data when parent is new', () => {
      expect(component.anlagenteilListe).toEqual([]);
    });
  });
});
