import { Component, OnInit } from '@angular/core';
import { ClrDatagridSortOrder } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import {
  AnlageDto,
  AnlagenteilListItemDto,
  AnlagenteilRestControllerService,
} from '@api/stammdaten';
import {
  ClarityIcons,
  eyeIcon,
  factoryIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(trashIcon, factoryIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-anlagenteile-liste',
  templateUrl: './anlagenteile-liste.component.html',
  styleUrls: ['./anlagenteile-liste.component.scss'],
})
export class AnlagenteileListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  anlagenteilListe: AnlagenteilListItemDto[];
  anlage: AnlageDto;
  loading = true;
  loeschenDialog: boolean;
  anlagenteilId: number;
  selected: AnlagenteilListItemDto[] = [];
  loescht = false;

  constructor(
    private route: ActivatedRoute,
    private anlagenteilRestControllerService: AnlagenteilRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.anlage = data.anlage;
      this.refresh();
    });
  }

  refresh(): void {
    if (this.neueAnlage()) {
      this.anlagenteilListe = [];
      this.loading = false;
      return;
    }

    this.loading = true;
    this.anlagenteilRestControllerService
      .readAllAnlagenteile(this.anlage.id, this.anlage.parentBetriebsstaetteId)
      .subscribe(
        (liste) => {
          this.anlagenteilListe = liste;
          this.loading = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  deleteAnlagenteil(): void {
    this.anlagenteilRestControllerService
      .deleteAnlagenteil(
        this.anlage.parentBetriebsstaetteId,
        this.anlagenteilId
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          // Data Grid refresh
          this.refresh();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  deleteAnlagenteile(): void {
    this.loescht = true;
    this.anlagenteilRestControllerService
      .deleteMultipleAnlagenteile(
        this.anlage.parentBetriebsstaetteId,
        this.selected.map((anlage) => anlage.id)
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.selected = [];
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  neueAnlage(): boolean {
    return !this.anlage.id;
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.anlagenteilId = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(anlagenteilId: number): void {
    this.anlagenteilId = anlagenteilId;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }
}
