import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AnlagenteilDetailsComponent } from './anlagenteil-details.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockModule, MockProvider } from 'ng-mocks';
import { AnlageAllgemeinComponent } from '../anlage-allgemein/anlage-allgemein.component';
import { VorschriftenComponent } from '@/basedomain/component/vorschriften/vorschriften.component';
import { LeistungenComponent } from '@/basedomain/component/leistungen/leistungen.component';
import { ZustaendigeBehoerdenComponent } from '../zustaendige-behoerden/zustaendige-behoerden.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import {
  AnlagenteilDto,
  AnlagenteilRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import spyOn = jest.spyOn;

describe('AnlagenTeilDetailsComponent', () => {
  let component: AnlagenteilDetailsComponent;
  let fixture: ComponentFixture<AnlagenteilDetailsComponent>;
  const betriebsstatusListe: Array<ReferenzDto> = [
    { schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vertraulichkeitsgrundListe: Array<ReferenzDto> = [
    { schluessel: '02' },
  ] as Array<ReferenzDto>;
  const vorschriftenListe: Array<ReferenzDto> = [
    { schluessel: '02' },
    { schluessel: '03' },
  ] as Array<ReferenzDto>;
  const nummer4BImSchVTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'R4BV01' },
    { schluessel: 'R4BV02' },
  ] as Array<ReferenzDto>;
  const iedTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RIET01' },
    { schluessel: 'RIET02' },
  ] as Array<ReferenzDto>;
  const prtrTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RPRTR01' },
    { schluessel: 'RPRTR02' },
  ] as Array<ReferenzDto>;
  const einheitenListe = [
    { schluessel: '1', ktext: 'm' },
    { schluessel: '2', ktext: 'km' },
  ] as ReferenzDto[];
  const anlagenteil: AnlagenteilDto = {
    anlagenteilNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    parentAnlageId: 500,
    vorschriften: [],
  };
  const routeData = new BehaviorSubject({ anlagenteil });
  const route = {
    parent: {
      parent: {
        parent: { snapshot: { data: { betriebsstaette: { id: 123 } } } },
      },
      snapshot: {
        data: {
          vorschriftenListe,
          nummer4BImSchVTaetigkeitenListe,
          iedTaetigkeitenListe,
          prtrTaetigkeitenListe,
          betriebsstatusListe,
          vertraulichkeitsgrundListe,
          einheitenListe,
          behoerdenListe: [],
          zustaendigkeitenListe: [],
          epsgCodeListe: [],
        },
      },
    },
    snapshot: {
      params: { anlagenteilNr: 'neu' },
      queryParams: {},
      data: { schreibRecht: true },
    },
    data: routeData.asObservable(),
  };

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AnlagenteilDetailsComponent,
        AnlageAllgemeinComponent,
        ReferenzSelectComponent,
        MockComponents(
          TooltipComponent,
          VorschriftenComponent,
          LeistungenComponent,
          ZustaendigeBehoerdenComponent,
          GeoPunktComponent
        ),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
      providers: [
        MockProvider(AnlagenteilRestControllerService),
        MockProvider(StammdatenUebernahmeRestControllerService),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return currentBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: route,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AnlagenteilDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should deactivate berichtsjahrwechsel', inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();

      component.ngOnDestroy();

      expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.anlageForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [ActivatedRoute],
    (route: ActivatedRoute) => {
      route.snapshot.data.schreibRecht = false;
      component.ngOnInit();
      expect(component.anlageForm.enabled).toBe(false);
    }
  ));

  describe('copy', () => {
    const currentAnlagenteil = {
      id: 1,
      anlagenteilNr: '123',
      name: 'Teil 1',
    } as AnlagenteilDto;
    beforeEach(inject([Router], (router: Router) => {
      spyOn(router, 'navigate').mockResolvedValue(true);
      component.anlagenteil = currentAnlagenteil;
      component.copy();
    }));
    it('should navigate to kopieren', inject(
      [Router, ActivatedRoute],
      (router: Router, route: ActivatedRoute) => {
        expect(router.navigate).toHaveBeenCalledWith(
          ['anlagenteil', 'kopieren'],
          {
            relativeTo: route.parent,
            state: { anlagenteil: currentAnlagenteil },
          }
        );
      }
    ));
  });

  describe('copied', () => {
    beforeEach(inject([ActivatedRoute], (activatedRoute: ActivatedRoute) => {
      spyOn(component.anlageForm, 'markAsDirty');
      activatedRoute.snapshot.params.anlagenteilNummer = 'kopieren';
      routeData.next({ anlagenteil });
    }));

    it('should not mark form as dirty', () => {
      expect(component.anlageForm.markAsDirty).toHaveBeenCalledTimes(0);
    });
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
      component.anlageForm.patchValue({
        allgemein: { name: 'Mein neues Anlagenteil' },
      });
    });

    it('should be invalid', () => {
      expect(component.anlageForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          AnlageAllgemeinComponent
        ).componentInstance;
        spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save(false);
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [AnlagenteilRestControllerService],
        (anlagenteilService: AnlagenteilRestControllerService) => {
          expect(anlagenteilService.createAnlagenteil).not.toHaveBeenCalled();
          expect(anlagenteilService.updateAnlagenteil).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.anlageForm.patchValue({
        allgemein: {
          name: 'Mein neues Anlagenteil',
          anlageNr: 'ANL1234-TEIL1',
          betriebsstatus: betriebsstatusListe[0],
          vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        },
      });
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.anlageForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedAnlagenteilDto: AnlagenteilDto = {
        anlagenteilNr: 'ANL1234-TEIL1',
        parentAnlageId: 500,
        bemerkung: null,
        betriebsstatus: betriebsstatusListe[0],
        betriebsstatusSeit: null,
        inbetriebnahme: null,
        localId: null,
        name: 'Mein neues Anlagenteil',
        parentBetriebsstaetteId: 123,
        vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        vorschriften: [],
        zustaendigeBehoerden: [],
        leistungen: [],
        geoPunkt: null,
      };
      let anlageObservable: Subject<AnlagenteilDto>;
      beforeEach(inject(
        [AnlagenteilRestControllerService],
        (anlagenteilService: AnlagenteilRestControllerService) => {
          anlageObservable = new Subject<AnlagenteilDto>();
          spyOn(anlagenteilService, 'createAnlagenteil').mockReturnValue(
            anlageObservable.asObservable() as any
          );
          component.save(false);
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [AnlagenteilRestControllerService],
        (anlagenteilService: AnlagenteilRestControllerService) => {
          expect(anlagenteilService.createAnlagenteil).toHaveBeenCalledWith(
            expectedAnlagenteilDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedAnlagenteilDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          anlageObservable.next(responseDto);
          anlageObservable.complete();
        }));

        it('should update anlagenteil', () => {
          expect(component.anlagenteil).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.anlageForm.touched).toBe(false);
          expect(component.anlageForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });
});
