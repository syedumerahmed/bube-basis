import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  AnlageAllgemeinComponent,
  AnlageAllgemeinFormData,
} from '../anlage-allgemein/anlage-allgemein.component';
import {
  ZustaendigeBehoerdenComponent,
  ZustaendigeBehoerdenFormData,
} from '../zustaendige-behoerden/zustaendige-behoerden.component';
import { AnlageFormData } from '../anlage-details/anlage-details.component';
import {
  LeistungenComponent,
  LeistungenFormData,
} from '@/basedomain/component/leistungen/leistungen.component';
import { FormUtil } from '@/base/forms/form-util';
import {
  VorschriftenComponent,
  VorschriftFormData,
} from '@/basedomain/component/vorschriften/vorschriften.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  AnlageDto,
  AnlagenteilDto,
  AnlagenteilRestControllerService,
  GeoPunktDto,
  ReferenzDto,
} from '@api/stammdaten';
import {
  ClarityIcons,
  copyIcon,
  errorStandardIcon,
  factoryIcon,
  floppyIcon,
  importIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import { GeoPunktFormData } from '@/basedomain/component/geo-punkt/geo-punkt.component';

const KOPIEREN_FLAG = 'kopieren';

ClarityIcons.addIcons(
  factoryIcon,
  copyIcon,
  importIcon,
  timesIcon,
  floppyIcon,
  errorStandardIcon
);

@Component({
  selector: 'app-anlagenteil-details',
  templateUrl: './anlagenteil-details.component.html',
  styleUrls: ['./anlagenteil-details.component.scss'],
})
export class AnlagenteilDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(AnlageAllgemeinComponent)
  allgemeinComponent: AnlageAllgemeinComponent;

  anlagenteil: AnlagenteilDto;
  anlageForm: FormGroup<AnlageFormData>;
  saveAttempt = false;
  openSaveWarning = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;
  einheitenListe: Referenzliste<Required<ReferenzDto>>;
  nummer4BImSchVTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  iedTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  prtrTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  zustaendigkeiten: Referenzliste<Required<ReferenzDto>>;
  behoerden: Referenzliste<Required<BehoerdeListItem>>;

  private betriebsstaetteId: number;

  constructor(
    private readonly fb: FormBuilder,
    private readonly anlagenteilRestControllerService: AnlagenteilRestControllerService,
    private readonly uebernahmeService: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  private mapToForm(anlagenteilDto: AnlagenteilDto): AnlageFormData {
    const anlageDto: AnlageDto = {
      ...anlagenteilDto,
      anlageNr: anlagenteilDto.anlagenteilNr,
    };

    return {
      allgemein: AnlageAllgemeinComponent.toForm(anlageDto),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.toForm(
        anlageDto.zustaendigeBehoerden,
        this.behoerden
      ),
      vorschriften: VorschriftenComponent.vorschriftenToForm(
        anlageDto.vorschriften,
        this.nummer4BImSchVTaetigkeitenListe,
        this.prtrTaetigkeitenListe,
        this.iedTaetigkeitenListe
      ),
      leistungen: LeistungenComponent.toForm(anlageDto.leistungen),
    };
  }

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.anlagenteil = data.anlagenteil;
      this.betriebsstaetteId =
        this.route.parent.parent.parent.snapshot.data.betriebsstaette.id;

      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.vorschriften = new Referenzliste(data.vorschriftenListe);
      this.vertraulichkeitsgruende = new Referenzliste(
        data.vertraulichkeitsgrundListe
      );
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.einheitenListe = new Referenzliste(data.einheitenListe);
      this.nummer4BImSchVTaetigkeitenListe = new Referenzliste(
        data.nummer4BImSchVTaetigkeitenListe
      );
      this.iedTaetigkeitenListe = new Referenzliste(data.iedTaetigkeitenListe);
      this.prtrTaetigkeitenListe = new Referenzliste(
        data.prtrTaetigkeitenListe
      );
      this.zustaendigkeiten = new Referenzliste(data.zustaendigkeitenListe);
      this.behoerden = new Referenzliste(data.behoerdenListe);

      this.reset();
    });

    this.anlageForm = this.createAnlageForm(this.mapToForm(this.anlagenteil));
    if (!this.canWrite) {
      this.anlageForm.disable();
    }
    if (this.route.snapshot.queryParams.spbAnlagenteilId != null) {
      this.anlageForm.markAsDirty();
    }
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  formToAnlagenteil(): AnlagenteilDto {
    const formData = this.anlageForm.value;
    const anlagenteilDto = {
      ...this.anlagenteil,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      vorschriften: VorschriftenComponent.formToVorschriften(
        formData.vorschriften
      ),
      zustaendigeBehoerden: formData.zustaendigeBehoerden,
      leistungen: formData.leistungen.map((data) => ({
        ...data,
        leistungseinheit: data.einheit,
        leistungsklasse: data.klasse,
      })),
      vertraulichkeitsgrund: formData.allgemein.vertraulichkeitsgrund,
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
      anlagenteilNr: formData.allgemein.anlageNr,
    };
    anlagenteilDto.parentBetriebsstaetteId = this.betriebsstaetteId;

    // Das wird gebraucht um das Property anlageNr wieder zu entfernen. Kommt über die Form rein
    delete anlagenteilDto.anlageNr;

    return anlagenteilDto;
  }

  save(saveWithoutWarning: boolean): void {
    this.saveAttempt = true;
    this.openSaveWarning = false;
    if (this.anlageForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }

    const anlagenteilDto: AnlagenteilDto = this.formToAnlagenteil();

    if (this.isNeu) {
      this.createAnlagenteil(anlagenteilDto);
    } else {
      if (saveWithoutWarning) {
        this.updateAnlagenteil(anlagenteilDto);
      } else {
        this.anlagenteilRestControllerService
          .anlagenteilBrauchtWarnungVorSpeicherung(anlagenteilDto)
          .subscribe(
            (ret) => {
              if (ret) {
                this.openSaveWarning = true;
              } else {
                this.updateAnlagenteil(anlagenteilDto);
              }
            },
            (err) => this.alertService.setAlert(err)
          );
      }
    }
  }

  createAnlagenteil(anlagenteilDto: AnlagenteilDto): void {
    const spbAnlagenteilId = this.route.snapshot.queryParams.spbAnlagenteilId;
    const createAnlagenteil = spbAnlagenteilId
      ? this.uebernahmeService.uebernehmeNeuesAnlagenteil(
          spbAnlagenteilId,
          anlagenteilDto
        )
      : this.anlagenteilRestControllerService.createAnlagenteil(anlagenteilDto);
    createAnlagenteil.subscribe(
      (anlagenteil) => {
        this.anlagenteil = anlagenteil;
        this.reset();
        this.updateUrlAfterCreate(anlagenteil.anlagenteilNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  updateAnlagenteil(anlagenteilDto: AnlagenteilDto): void {
    this.anlagenteilRestControllerService
      .updateAnlagenteil(anlagenteilDto)
      .subscribe(
        (anlagenteil) => {
          this.anlagenteil = anlagenteil;
          this.reset();
          this.updateUrlAfterChange(anlagenteil.anlagenteilNr);
          this.alertService.clearAlertMessages();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  saveAbbrechen(): void {
    this.openSaveWarning = false;
  }

  getGeoPunkt(geoPunkt: GeoPunktFormData): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.anlageForm) {
      this.saveAttempt = false;
      const originalAnlagenteil = this.mapToForm(this.anlagenteil);
      this.anlageForm.reset(originalAnlagenteil);
      this.anlageForm.setControl(
        'zustaendigeBehoerden',
        ZustaendigeBehoerdenComponent.createForm(
          this.fb,
          originalAnlagenteil.zustaendigeBehoerden,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'leistungen',
        LeistungenComponent.createForm(
          this.fb,
          originalAnlagenteil.leistungen,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'vorschriften',
        VorschriftenComponent.vorschriftenCreateForm(
          this.fb,
          originalAnlagenteil.vorschriften,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );

      // Workaround für ComboBox https://github.com/vmware/clarity/issues/5199
      // this.allgemeinForm.get('vorschriften').markAsPristine();
    }
  }

  copy(): void {
    this.router.navigate(['anlagenteil', KOPIEREN_FLAG], {
      relativeTo: this.route.parent,
      state: { anlagenteil: this.anlagenteil },
    });
  }

  get isNeu(): boolean {
    return this.anlagenteil.id == null;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get allgemeinForm(): FormGroup<AnlageAllgemeinFormData> {
    return this.anlageForm.get(
      'allgemein'
    ) as FormGroup<AnlageAllgemeinFormData>;
  }

  get zustaendigeBehoerdenForm(): FormArray<ZustaendigeBehoerdenFormData> {
    return this.anlageForm.get(
      'zustaendigeBehoerden'
    ) as FormArray<ZustaendigeBehoerdenFormData>;
  }

  get leistungenForm(): FormArray<LeistungenFormData> {
    return this.anlageForm.get('leistungen') as FormArray<LeistungenFormData>;
  }

  get vorschriftenForm(): FormArray<VorschriftFormData> {
    return this.anlageForm.get(
      'vorschriften'
    ) as unknown as FormArray<VorschriftFormData>;
  }

  private createAnlageForm(
    initialAnlage: AnlageFormData
  ): FormGroup<AnlageFormData> {
    return this.fb.group<AnlageFormData>({
      allgemein: AnlageAllgemeinComponent.createForm(
        this.fb,
        initialAnlage.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.createForm(
        this.fb,
        initialAnlage.zustaendigeBehoerden,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      vorschriften: VorschriftenComponent.vorschriftenCreateForm(
        this.fb,
        initialAnlage.vorschriften,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      leistungen: LeistungenComponent.createForm(
        this.fb,
        initialAnlage.leistungen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, '/anlagenteil/nummer/' + encodeURI(nummer));
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder kopieren, uebernehmen oder neu in der URL und muss ersetzt werden
    this.updateUrl(
      nummer,
      new RegExp('/anlagenteil/(kopieren|neu|uebernehmen)+')
    );
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      nummer,
      '/anlagenteil/nummer/' + this.route.snapshot.params.anlagenteilNummer
    );
  }

  navigateBack(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  betreiberdatenUebernehmen(): void {
    const aktuellesJahr =
      this.berichtsjahrService.selectedBerichtsjahr.schluessel;
    const routeBst =
      this.route.snapshot.parent.parent.parent.data.betriebsstaette;
    this.router.navigate([
      '/stammdaten/jahr/' +
        aktuellesJahr +
        '/uebernahme/anlagenteil/' +
        this.anlagenteil.id,
      {
        berichtsjahr: routeBst.berichtsjahr.schluessel,
        land: routeBst.land,
        betriebsstaetteNummer: routeBst.betriebsstaetteNr,
        betriebsstaetteId: routeBst.id,
        anlageNr: this.route.snapshot.parent.data.anlage.anlageNr,
      },
    ]);
  }

  canDeactivate(): boolean {
    return this.anlageForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
