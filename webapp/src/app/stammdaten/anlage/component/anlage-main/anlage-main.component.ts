import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute } from '@angular/router';
import { QuellenZuordungService } from '@/stammdaten/quelle/service/quellen-zuordung.service';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { AnlageDetailsComponent } from '@/stammdaten/anlage/component/anlage-details/anlage-details.component';

@Component({
  selector: 'app-anlage-main',
  templateUrl: './anlage-main.component.html',
  styleUrls: ['./anlage-main.component.scss'],
})
export class AnlageMainComponent implements OnInit, UnsavedChanges {
  @ViewChild(AnlageDetailsComponent, { static: true })
  anlageDetailsComponent: AnlageDetailsComponent;

  quellenDisplay: boolean;

  constructor(
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly quellenZuordungService: QuellenZuordungService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.quellenZuordungService
        .quellenSindAnAnlagen(
          data.betriebsstaette.berichtsjahr.schluessel,
          data.betriebsstaette.land
        )
        .subscribe(
          (ret) => {
            this.quellenDisplay = ret;
          },
          (error) => this.alertService.setAlert(error)
        );
    });
  }

  canDeactivate(): boolean {
    return this.anlageDetailsComponent.canDeactivate();
  }

  showSaveWarning(): void {
    this.anlageDetailsComponent.showSaveWarning();
  }
}
