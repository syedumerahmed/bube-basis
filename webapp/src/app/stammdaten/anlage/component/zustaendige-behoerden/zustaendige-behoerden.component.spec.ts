import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  ZustaendigeBehoerdenComponent,
  ZustaendigeBehoerdenFormData,
} from './zustaendige-behoerden.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReferenzDto } from '@api/stammdaten';
import { FormBuilder } from '@/base/forms';
import { MockModule, MockProvider } from 'ng-mocks';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { of } from 'rxjs';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';

describe('ZustaendigeBehoerdenComponent', () => {
  let component: ZustaendigeBehoerdenComponent;
  let fixture: ComponentFixture<ZustaendigeBehoerdenComponent>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const behoerdenListe = [
    { id: 1, schluessel: '1', bezeichnung: 'b1' },
    { id: 2, schluessel: '2', bezeichnung: 'b2' },
  ] as BehoerdeListItem[];

  const zustaendigkeitenListe = [
    { schluessel: '10', ktext: 'z1' },
    { schluessel: '20', ktext: 'z2' },
  ] as ReferenzDto[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
      declarations: [ZustaendigeBehoerdenComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            parent: { data: of({ zustaendigkeitenListe, behoerdenListe }) },
          },
        },
        MockProvider(BerichtsjahrService),
      ],
    }).compileComponents();
    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(ZustaendigeBehoerdenComponent);
      component = fixture.componentInstance;
      component.formArray = ZustaendigeBehoerdenComponent.createForm(
        fb,
        ZustaendigeBehoerdenComponent.toForm([], null),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(ZustaendigeBehoerdenComponent);
      component = fixture.componentInstance;
      component.formArray = ZustaendigeBehoerdenComponent.createForm(
        fb,
        ZustaendigeBehoerdenComponent.toForm(
          [
            {
              behoerde: behoerdenListe[0],
              zustaendigkeit: zustaendigkeitenListe[0],
            },
            {
              behoerde: behoerdenListe[1],
              zustaendigkeit: zustaendigkeitenListe[1],
            },
          ],
          new Referenzliste<Required<BehoerdeListItem>>(behoerdenListe)
        ),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.behoerdenForm).toBeTruthy();
        expect(component.behoerdenForm.value).toStrictEqual({
          behoerde: null,
          zustaendigkeit: null,
        });
        expect(component.behoerdenForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let patchedValue: ZustaendigeBehoerdenFormData;
        beforeEach(() => {
          patchedValue = {
            behoerde: behoerdenListe[1],
            zustaendigkeit: zustaendigkeitenListe[0],
          };
          component.behoerdenForm.patchValue(patchedValue);
        });
        it('should be valid', () => {
          expect(component.behoerdenForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null behoerdenForm', () => {
            expect(component.behoerdenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null behoerdenForm', () => {
            expect(component.behoerdenForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.behoerdenForm).toBeTruthy();
        expect(component.behoerdenForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.behoerdenForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: ZustaendigeBehoerdenFormData;
        let patchedValue: ZustaendigeBehoerdenFormData;
        beforeEach(() => {
          originalValue = component.behoerdenForm.value;
          patchedValue = {
            behoerde: behoerdenListe[1],
            zustaendigkeit: zustaendigkeitenListe[0],
          };
          component.behoerdenForm.patchValue(patchedValue);
          component.behoerdenForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.behoerdenForm.patchValue(patchedValue);
            component.behoerdenForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null behoerdenForm', () => {
            expect(component.behoerdenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();

            expect(component.formArray.dirty).toBe(true);
          });

          it('should null behoerdenForm', () => {
            expect(component.behoerdenForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: ZustaendigeBehoerdenFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
      });
    });
  });
});
