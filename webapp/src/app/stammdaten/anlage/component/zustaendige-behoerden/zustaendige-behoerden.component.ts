import {
  AfterViewChecked,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ClrForm } from '@clr/angular';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto, ZustaendigeBehoerdeDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface ZustaendigeBehoerdenFormData {
  behoerde: BehoerdeListItem;
  zustaendigkeit: ReferenzDto;
}

ClarityIcons.addIcons(trashIcon, errorStandardIcon, noteIcon, plusIcon);
@Component({
  selector: 'app-zustaendige-behoerden',
  templateUrl: './zustaendige-behoerden.component.html',
  styleUrls: ['./zustaendige-behoerden.component.scss'],
})
export class ZustaendigeBehoerdenComponent implements OnInit, AfterViewChecked {
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private berichtsjahrService: BerichtsjahrService
  ) {}

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  behoerdenForm: FormGroup<ZustaendigeBehoerdenFormData>;

  @Input()
  formArray: FormArray<ZustaendigeBehoerdenFormData>;

  zustaendigkeitenListe: Array<ReferenzDto>;
  behoerdenListe: Array<BehoerdeListItem>;

  editModalOpen = false;

  editExistingIndex: number = null;

  static toForm(
    zustaendigeBehoerden: Array<ZustaendigeBehoerdeDto>,
    behoerdenMap: Referenzliste<BehoerdeListItem>
  ): Array<ZustaendigeBehoerdenFormData> {
    return (zustaendigeBehoerden ?? []).map((zBehoerde) => ({
      behoerde: behoerdenMap.find(new BehoerdeListItem(zBehoerde.behoerde)),
      zustaendigkeit: zBehoerde.zustaendigkeit,
    }));
  }

  static createForm(
    fb: FormBuilder,
    zustaendigeBehoerden: Array<ZustaendigeBehoerdenFormData>,
    selectedBerichtsjahr: ReferenzDto
  ): FormArray<ZustaendigeBehoerdenFormData> {
    return fb.array<ZustaendigeBehoerdenFormData>(
      zustaendigeBehoerden.map((zBehoerde) =>
        fb.group({
          behoerde: [
            zBehoerde.behoerde,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsjahr.schluessel)
              ),
            ],
          ],
          zustaendigkeit: [
            zBehoerde.zustaendigkeit,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsjahr.schluessel)
              ),
            ],
          ],
        })
      )
    );
  }

  ngOnInit(): void {
    this.route.parent.data.subscribe((data) => {
      this.zustaendigkeitenListe = data.zustaendigkeitenListe;
      this.behoerdenListe = data.behoerdenListe;
    });
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.behoerdenForm = null;
  }

  uebernehmen(): void {
    if (this.behoerdenForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.behoerdenForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.behoerdenForm)) {
        this.formArray.push(this.behoerdenForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.behoerdenForm = null;
    }
  }

  private createEmptyForm(): FormGroup<ZustaendigeBehoerdenFormData> {
    return this.fb.group({
      behoerde: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      zustaendigkeit: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
    });
  }

  private openEdit(formGroup: FormGroup<ZustaendigeBehoerdenFormData>): void {
    this.behoerdenForm = formGroup;
    this.editModalOpen = true;
  }
}
