import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AnlageDetailsComponent } from './anlage-details.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockModule, MockProvider } from 'ng-mocks';
import { AnlageAllgemeinComponent } from '@/stammdaten/anlage/component/anlage-allgemein/anlage-allgemein.component';
import { VorschriftenComponent } from '@/basedomain/component/vorschriften/vorschriften.component';
import { LeistungenComponent } from '@/basedomain/component/leistungen/leistungen.component';
import { ZustaendigeBehoerdenComponent } from '@/stammdaten/anlage/component/zustaendige-behoerden/zustaendige-behoerden.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import {
  AnlageDto,
  AnlagenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import spyOn = jest.spyOn;

describe('AnlageDetailsComponent', () => {
  let component: AnlageDetailsComponent;
  let fixture: ComponentFixture<AnlageDetailsComponent>;
  const betriebsstatusListe: Array<ReferenzDto> = [
    { schluessel: '01' },
  ] as Array<ReferenzDto>;
  const vertraulichkeitsgrundListe: Array<ReferenzDto> = [
    { schluessel: '02' },
  ] as Array<ReferenzDto>;
  const vorschriftenListe: Array<ReferenzDto> = [
    { schluessel: '02' },
    { schluessel: '03' },
  ] as Array<ReferenzDto>;
  const nummer4BImSchVTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'R4BV01' },
    { schluessel: 'R4BV02' },
  ] as Array<ReferenzDto>;
  const iedTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RIET01' },
    { schluessel: 'RIET02' },
  ] as Array<ReferenzDto>;
  const prtrTaetigkeitenListe: Array<ReferenzDto> = [
    { schluessel: 'RPRTR01' },
    { schluessel: 'RPRTR02' },
  ] as Array<ReferenzDto>;
  const einheitenListe = [
    { schluessel: '1', ktext: 'm' },
    { schluessel: '2', ktext: 'km' },
  ] as ReferenzDto[];

  const anlage: AnlageDto = {
    anlageNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    vorschriften: [],
  };
  const routeData = new BehaviorSubject({ anlage });
  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      declarations: [
        AnlageDetailsComponent,
        AnlageAllgemeinComponent,
        ReferenzSelectComponent,
        MockComponents(
          VorschriftenComponent,
          LeistungenComponent,
          ZustaendigeBehoerdenComponent,
          TooltipComponent,
          GeoPunktComponent
        ),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
      providers: [
        MockProvider(AnlagenRestControllerService),
        MockProvider(StammdatenUebernahmeRestControllerService),
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              snapshot: {
                data: {
                  vorschriftenListe,
                  nummer4BImSchVTaetigkeitenListe,
                  iedTaetigkeitenListe,
                  prtrTaetigkeitenListe,
                  betriebsstatusListe,
                  vertraulichkeitsgrundListe,
                  einheitenListe,
                  behoerdenListe: [],
                  zustaendigkeitenListe: [],
                  epsgCodeListe: [],
                },
              },
            },
            snapshot: {
              params: { anlageNummer: 'neu' },
              queryParams: {},
              data: { schreibRecht: true },
            },
            data: routeData.asObservable(),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AnlageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should deactivate berichtsjahrwechsel', inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();

      component.ngOnDestroy();

      expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.anlageForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [ActivatedRoute],
    (route: ActivatedRoute) => {
      route.snapshot.data.schreibRecht = false;
      component.ngOnInit();
      expect(component.anlageForm.enabled).toBe(false);
    }
  ));

  describe('copy', () => {
    const currentAnlage = {
      id: 1,
      anlageNr: '123',
      name: 'Anlage 1',
    } as AnlageDto;
    beforeEach(inject([Router], (router: Router) => {
      spyOn(router, 'navigate').mockResolvedValue(true);
      component.anlage = currentAnlage;
      component.copy();
    }));
    it('should navigate to kopieren', inject(
      [Router, ActivatedRoute],
      (router: Router, route: ActivatedRoute) => {
        expect(router.navigate).toHaveBeenCalledWith(['kopieren'], {
          relativeTo: route.parent.parent,
          state: { anlage: currentAnlage },
        });
      }
    ));
  });

  describe('copied', () => {
    beforeEach(inject([ActivatedRoute], (activatedRoute: ActivatedRoute) => {
      spyOn(component.anlageForm, 'markAsDirty');
      activatedRoute.snapshot.params.anlageNummer = 'kopieren';
      routeData.next({ anlage });
    }));

    it('should not mark form as dirty', () => {
      expect(component.anlageForm.markAsDirty).toHaveBeenCalledTimes(0);
    });
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
      component.anlageForm.patchValue({
        allgemein: { name: 'Meine neue Anlage' },
      });
    });

    it('should be invalid', () => {
      expect(component.anlageForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          AnlageAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save(false);
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [AnlagenRestControllerService],
        (anlagenService: AnlagenRestControllerService) => {
          expect(anlagenService.createAnlage).not.toHaveBeenCalled();
          expect(anlagenService.updateAnlage).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.anlageForm.patchValue({
        allgemein: {
          name: 'Meine neue Anlage',
          anlageNr: 'ANL1234',
          betriebsstatus: betriebsstatusListe[0],
          vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        },
        vorschriften: vorschriftenListe.map((r) => ({
          art: r,
          haupttaetigkeit: null,
          taetigkeiten: null,
        })),
      });
      component.anlageForm.markAsDirty();
      component.anlageForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.anlageForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedAnlageDto: AnlageDto = {
        anlageNr: 'ANL1234',
        bemerkung: null,
        betriebsstatus: betriebsstatusListe[0],
        betriebsstatusSeit: null,
        inbetriebnahme: null,
        localId: null,
        name: 'Meine neue Anlage',
        parentBetriebsstaetteId: 0,
        vertraulichkeitsgrund: vertraulichkeitsgrundListe[0],
        vorschriften: [],
        zustaendigeBehoerden: [],
        leistungen: [],
        geoPunkt: null,
      };
      let anlageObservable: Subject<AnlageDto>;
      beforeEach(inject(
        [AnlagenRestControllerService],
        (anlagenService: AnlagenRestControllerService) => {
          anlageObservable = new Subject<AnlageDto>();
          spyOn(anlagenService, 'createAnlage').mockReturnValue(
            anlageObservable.asObservable() as any
          );
          component.save(false);
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [AnlagenRestControllerService],
        (anlagenService: AnlagenRestControllerService) => {
          expect(anlagenService.createAnlage).toHaveBeenCalledWith(
            expectedAnlageDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedAnlageDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          anlageObservable.next(responseDto);
          anlageObservable.complete();
        }));

        it('should update anlage', () => {
          expect(component.anlage).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.anlageForm.touched).toBe(true);
          expect(component.anlageForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });
});
