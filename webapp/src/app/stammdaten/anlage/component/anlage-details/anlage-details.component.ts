import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import {
  AnlageAllgemeinComponent,
  AnlageAllgemeinFormData,
} from '../anlage-allgemein/anlage-allgemein.component';
import { DatePipe } from '@angular/common';
import {
  ZustaendigeBehoerdenComponent,
  ZustaendigeBehoerdenFormData,
} from '../zustaendige-behoerden/zustaendige-behoerden.component';
import {
  LeistungenComponent,
  LeistungenFormData,
} from '@/basedomain/component/leistungen/leistungen.component';
import { FormUtil } from '@/base/forms/form-util';
import {
  VorschriftenComponent,
  VorschriftFormData,
} from '@/basedomain/component/vorschriften/vorschriften.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  AnlageDto,
  AnlagenRestControllerService,
  GeoPunktDto,
  ReferenzDto,
} from '@api/stammdaten';
import {
  ClarityIcons,
  copyIcon,
  errorStandardIcon,
  factoryIcon,
  floppyIcon,
  importIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';

const KOPIEREN_FLAG = 'kopieren';

export interface AnlageFormData {
  allgemein: AnlageAllgemeinFormData;
  zustaendigeBehoerden: Array<ZustaendigeBehoerdenFormData>;
  vorschriften: Array<VorschriftFormData>;
  leistungen: Array<LeistungenFormData>;
}

ClarityIcons.addIcons(
  factoryIcon,
  copyIcon,
  importIcon,
  timesIcon,
  floppyIcon,
  errorStandardIcon
);

@Component({
  selector: 'app-anlagen-details',
  templateUrl: './anlage-details.component.html',
  styleUrls: ['./anlage-details.component.scss'],
  providers: [DatePipe],
})
export class AnlageDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(AnlageAllgemeinComponent)
  allgemeinComponent: AnlageAllgemeinComponent;

  anlage: AnlageDto;
  anlageForm: FormGroup<AnlageFormData>;

  saveAttempt = false;
  openSaveWarning = false;

  betriebsstatus: Referenzliste<Required<ReferenzDto>>;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;
  einheitenListe: Referenzliste<Required<ReferenzDto>>;
  nummer4BImSchVTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  iedTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  prtrTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>;
  zustaendigkeiten: Referenzliste<Required<ReferenzDto>>;
  behoerden: Referenzliste<Required<BehoerdeListItem>>;

  private mapToForm(anlageDto: AnlageDto): AnlageFormData {
    return {
      allgemein: AnlageAllgemeinComponent.toForm(anlageDto),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.toForm(
        anlageDto.zustaendigeBehoerden,
        this.behoerden
      ),
      vorschriften: VorschriftenComponent.vorschriftenToForm(
        anlageDto.vorschriften,
        this.nummer4BImSchVTaetigkeitenListe,
        this.prtrTaetigkeitenListe,
        this.iedTaetigkeitenListe
      ),
      leistungen: LeistungenComponent.toForm(anlageDto.leistungen),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly anlagenService: AnlagenRestControllerService,
    private readonly uebernahmeService: StammdatenUebernahmeRestControllerService,
    private readonly alertService: AlertService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.anlage = data.anlage;

      this.vertraulichkeitsgruende = new Referenzliste(
        data.vertraulichkeitsgrundListe
      );
      this.betriebsstatus = new Referenzliste(data.betriebsstatusListe);
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.vorschriften = new Referenzliste(data.vorschriftenListe);
      this.einheitenListe = new Referenzliste(data.einheitenListe);
      this.nummer4BImSchVTaetigkeitenListe = new Referenzliste(
        data.nummer4BImSchVTaetigkeitenListe
      );
      this.iedTaetigkeitenListe = new Referenzliste(data.iedTaetigkeitenListe);
      this.prtrTaetigkeitenListe = new Referenzliste(
        data.prtrTaetigkeitenListe
      );
      this.zustaendigkeiten = new Referenzliste(data.zustaendigkeitenListe);
      this.behoerden = new Referenzliste(data.behoerdenListe);

      this.reset();
    });
    this.anlageForm = this.createAnlageForm(this.mapToForm(this.anlage));
    if (!this.canWrite) {
      this.anlageForm.disable();
    }
    if (this.route.snapshot.queryParams.spbAnlageId != null) {
      this.anlageForm.markAsDirty();
    }
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  formToAnlage(): AnlageDto {
    const formData = this.anlageForm.value;
    return {
      ...this.anlage,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      vorschriften: VorschriftenComponent.formToVorschriften(
        formData.vorschriften
      ),
      zustaendigeBehoerden: formData.zustaendigeBehoerden,
      leistungen: formData.leistungen.map((data) => ({
        ...data,
        leistungseinheit: data.einheit,
        leistungsklasse: data.klasse,
      })),
      vertraulichkeitsgrund: formData.allgemein.vertraulichkeitsgrund,
      betriebsstatus: formData.allgemein.betriebsstatus,
      betriebsstatusSeit: FormUtil.toIsoDate(
        formData.allgemein.betriebsstatusSeit
      ),
      inbetriebnahme: FormUtil.toIsoDate(formData.allgemein.inbetriebnahme),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
    };
  }

  save(saveWithoutWarning: boolean): void {
    this.saveAttempt = true;
    this.openSaveWarning = false;
    if (this.anlageForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }

    const anlageDto: AnlageDto = this.formToAnlage();

    if (this.isNeu) {
      this.createAnlage(anlageDto);
    } else {
      if (saveWithoutWarning) {
        this.updateAnlage(anlageDto);
      } else {
        this.anlagenService
          .anlageBrauchtWarnungVorSpeicherung(anlageDto)
          .subscribe(
            (ret) => {
              if (ret) {
                this.openSaveWarning = true;
              } else {
                this.updateAnlage(anlageDto);
              }
            },
            (err) => this.alertService.setAlert(err)
          );
      }
    }
  }

  updateAnlage(anlageDto: AnlageDto): void {
    this.anlagenService.updateAnlage(anlageDto).subscribe(
      (anlage) => {
        this.anlage = anlage;
        this.reset();
        this.updateUrlAfterChange(anlage.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  createAnlage(anlageDto: AnlageDto): void {
    const spbAnlageId = this.route.snapshot.queryParams.spbAnlageId;
    const createAnlage = spbAnlageId
      ? this.uebernahmeService.uebernehmeNeueAnlage(spbAnlageId, anlageDto)
      : this.anlagenService.createAnlage(anlageDto);
    createAnlage.subscribe(
      (anlage) => {
        this.anlage = anlage;
        this.reset();
        this.updateUrlAfterCreate(anlage.anlageNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  saveAbbrechen(): void {
    this.openSaveWarning = false;
  }

  getGeoPunkt(geoPunkt: AnlageAllgemeinFormData['geoPunkt']): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.anlageForm) {
      this.saveAttempt = false;
      const originalAnlage = this.mapToForm(this.anlage);
      this.anlageForm.reset(originalAnlage);
      this.anlageForm.setControl(
        'zustaendigeBehoerden',
        ZustaendigeBehoerdenComponent.createForm(
          this.fb,
          originalAnlage.zustaendigeBehoerden,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'leistungen',
        LeistungenComponent.createForm(
          this.fb,
          originalAnlage.leistungen,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.anlageForm.setControl(
        'vorschriften',
        VorschriftenComponent.vorschriftenCreateForm(
          this.fb,
          originalAnlage.vorschriften,
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );

      // Workaround für ComboBox https://github.com/vmware/clarity/issues/5199
      // this.allgemeinForm.get('vorschriften').markAsPristine();
      this.anlageForm.markAllAsTouched();
    }
  }

  navigateBackToBetriebsstaette(): void {
    this.router.navigate(['.'], {
      relativeTo: this.route.parent.parent.parent,
    });
  }

  copy(): void {
    this.router.navigate([KOPIEREN_FLAG], {
      relativeTo: this.route.parent.parent,
      state: { anlage: this.anlage },
    });
  }

  get isNeu(): boolean {
    return this.anlage.id == null;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get allgemeinForm(): FormGroup<AnlageAllgemeinFormData> {
    return this.anlageForm.get(
      'allgemein'
    ) as FormGroup<AnlageAllgemeinFormData>;
  }

  get zustaendigeBehoerdenForm(): FormArray<ZustaendigeBehoerdenFormData> {
    return this.anlageForm.get(
      'zustaendigeBehoerden'
    ) as FormArray<ZustaendigeBehoerdenFormData>;
  }

  get vorschriftenForm(): FormArray<VorschriftFormData> {
    return this.anlageForm.get('vorschriften') as FormArray<VorschriftFormData>;
  }

  get leistungenForm(): FormArray<LeistungenFormData> {
    return this.anlageForm.get('leistungen') as FormArray<LeistungenFormData>;
  }

  private createAnlageForm(
    initialAnlage: AnlageFormData
  ): FormGroup<AnlageFormData> {
    return this.fb.group<AnlageFormData>({
      allgemein: AnlageAllgemeinComponent.createForm(
        this.fb,
        initialAnlage.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      zustaendigeBehoerden: ZustaendigeBehoerdenComponent.createForm(
        this.fb,
        initialAnlage.zustaendigeBehoerden,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      vorschriften: VorschriftenComponent.vorschriftenCreateForm(
        this.fb,
        initialAnlage.vorschriften,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      leistungen: LeistungenComponent.createForm(
        this.fb,
        initialAnlage.leistungen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, '/anlage/nummer/' + encodeURI(nummer));
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder kopieren, uebernehmen oder neu in der URL und muss ersetzt werden
    this.updateUrl(nummer, new RegExp('/anlage/(kopieren|neu|uebernehmen)+'));
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      nummer,
      '/anlage/nummer/' + this.route.snapshot.params.anlageNummer
    );
  }

  betreiberdatenUebernehmen(): void {
    const aktuellesJahr =
      this.berichtsjahrService.selectedBerichtsjahr.schluessel;
    const routeBST = this.route.snapshot.data.betriebsstaette;
    this.router.navigate([
      '/stammdaten/jahr/' +
        aktuellesJahr +
        '/uebernahme/anlage/' +
        this.anlage.id,
      {
        berichtsjahr: routeBST.berichtsjahr.schluessel,
        land: routeBST.land,
        betriebsstaetteNummer: routeBST.betriebsstaetteNr,
        betriebsstaetteId: routeBST.id,
        anlageNummer: this.anlage.anlageNr,
      },
    ]);
  }

  canDeactivate(): boolean {
    return this.anlageForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
