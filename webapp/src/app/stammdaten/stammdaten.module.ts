import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { StammdatenComponent } from './component/stammdaten/stammdaten.component';

import { NaviBaumComponent } from './component/navi-baum/navi-baum.component';
import { EntityNotFoundComponent } from './component/entity-not-found/entity-not-found.component';

import { StammdatenRoutingModule } from './routing/stammdaten-routing.module';
import { BetriebsstaetteModule } from './betriebsstaette/betriebsstaette.module';
import { BaseModule } from '@/base/base.module';
import { StammdatenUebernahmeComponent } from './component/stammdaten-uebernahme/stammdaten-uebernahme.component';
import { BasedomainModule } from '@/basedomain/basedomain.module';

@NgModule({
  declarations: [
    StammdatenComponent,
    EntityNotFoundComponent,
    NaviBaumComponent,
    StammdatenUebernahmeComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    BaseModule,
    BasedomainModule,
    StammdatenRoutingModule,
    BetriebsstaetteModule,
  ],
  exports: [NaviBaumComponent],
})
export class StammdatenModule {}
