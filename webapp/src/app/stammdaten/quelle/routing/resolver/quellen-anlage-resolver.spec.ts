import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockService } from 'ng-mocks';
import { AlertService } from '@/base/service/alert.service';
import { QuellenAnlageResolver } from './quellen-anlage-resolver';
import { EMPTY } from 'rxjs';
import { QuellenRestControllerService } from '@api/stammdaten';
import spyOn = jest.spyOn;

describe('QuellenAnlageResolver', () => {
  let resolver: QuellenAnlageResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        QuellenAnlageResolver,
        { provide: Router, useValue: MockService(Router) },
        {
          provide: QuellenRestControllerService,
          useValue: MockService(QuellenRestControllerService),
        },
        { provide: AlertService, useValue: MockService(AlertService) },
      ],
    });
    resolver = TestBed.inject(QuellenAnlageResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { quelleNummer: 'neu' },
        parent: {
          parent: { parent: { data: { betriebsstaette: { id: 1 } } } },
          data: { anlage: { id: 2 } },
        },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [QuellenRestControllerService],
      (quellenService: QuellenRestControllerService) => {
        spyOn(quellenService, 'readQuelleOfAnlage').mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot, null);

        expect(quellenService.readQuelleOfAnlage).toHaveBeenCalledWith(
          2,
          'neu'
        );
      }
    ));
  });
});
