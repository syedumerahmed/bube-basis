import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { QuelleDto } from '@api/stammdaten';

@Injectable()
export class QuelleNeuUebernehmenResolver implements Resolve<QuelleDto> {
  constructor(private router: Router, private location: Location) {}

  resolve(): Observable<QuelleDto> {
    const quelle = this.router.getCurrentNavigation().extras.state?.quelle;
    if (!quelle) {
      this.location.back();
      return EMPTY;
    }
    return of(quelle);
  }
}
