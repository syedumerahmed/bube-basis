import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { QuelleDto } from '@api/stammdaten';

@Injectable()
export class QuelleKopierenResolver implements Resolve<QuelleDto> {
  constructor(private router: Router, private location: Location) {}

  private static copyQuelle(quelle: QuelleDto): QuelleDto {
    return {
      ...quelle,
      id: null,
      quelleNr: null,
      name: 'Kopie - ' + quelle.name,
      letzteAenderung: null,
      ersteErfassung: null,
      betreiberdatenUebernommen: false,
    };
  }

  resolve(): Observable<QuelleDto> {
    const quelle = this.router.getCurrentNavigation().extras.state?.quelle;
    if (!quelle) {
      this.location.back();
      return EMPTY;
    }
    return of(QuelleKopierenResolver.copyQuelle(quelle));
  }
}
