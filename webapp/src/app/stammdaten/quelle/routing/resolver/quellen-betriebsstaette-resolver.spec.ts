import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockService } from 'ng-mocks';
import { AlertService } from '@/base/service/alert.service';
import { QuellenBetriebsstaetteResolver } from './quellen-betriebsstaette-resolver';
import { EMPTY } from 'rxjs';
import spyOn = jest.spyOn;
import { QuellenRestControllerService } from '@api/stammdaten';

describe('QuellenBetriebsstaetteResolver', () => {
  let resolver: QuellenBetriebsstaetteResolver;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        QuellenBetriebsstaetteResolver,
        { provide: Router, useValue: MockService(Router) },
        {
          provide: QuellenRestControllerService,
          useValue: MockService(QuellenRestControllerService),
        },
        { provide: AlertService, useValue: MockService(AlertService) },
      ],
    });
    resolver = TestBed.inject(QuellenBetriebsstaetteResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { quelleNummer: 'neu' },
        parent: { data: { betriebsstaette: { id: 1 } } },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [QuellenRestControllerService],
      (quellenService: QuellenRestControllerService) => {
        spyOn(quellenService, 'readQuelle').mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot, null);

        expect(quellenService.readQuelle).toHaveBeenCalledWith(1, 'neu');
      }
    ));
  });
});
