import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { QuelleDto } from '@api/stammdaten';

@Injectable()
export class QuellenBetriebsstaetteNeuResolver implements Resolve<QuelleDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<QuelleDto> {
    const betriebsstaetteId = route.parent.data.betriebsstaette.id;
    return of({
      name: null,
      quelleNr: null,
      parentBetriebsstaetteId: betriebsstaetteId,
    });
  }
}
