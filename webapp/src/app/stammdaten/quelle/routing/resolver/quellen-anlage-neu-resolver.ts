import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { QuelleDto } from '@api/stammdaten';

@Injectable()
export class QuellenAnlageNeuResolver implements Resolve<QuelleDto> {
  resolve(route: ActivatedRouteSnapshot): Observable<QuelleDto> {
    const betriebsstaetteId =
      route.parent.parent.parent.data.betriebsstaette.id;
    const anlageId = route.parent.data.anlage.id;
    return of({
      name: null,
      quelleNr: null,
      parentBetriebsstaetteId: betriebsstaetteId,
      parentAnlageId: anlageId,
    });
  }
}
