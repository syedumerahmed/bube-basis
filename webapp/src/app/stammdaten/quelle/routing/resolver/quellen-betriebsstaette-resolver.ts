import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { QuelleDto, QuellenRestControllerService } from '@api/stammdaten';

@Injectable()
export class QuellenBetriebsstaetteResolver implements Resolve<QuelleDto> {
  constructor(
    private service: QuellenRestControllerService,
    private router: Router,
    private alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<QuelleDto> {
    const betriebsstaetteId = route.parent.data.betriebsstaette.id;
    const nummer = route.params.quelleNummer;

    return this.service.readQuelle(betriebsstaetteId, nummer).pipe(
      catchError((err) => {
        if (err.status === 404) {
          this.router.navigateByUrl(state.url.replace('/quelle/' + nummer, ''));
        }
        this.alertService.setAlert(err);
        return EMPTY;
      })
    );
  }
}
