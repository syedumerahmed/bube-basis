import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuellenListeComponent } from './quellen-liste.component';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  BetriebsstaetteDto,
  QuellenRestControllerService,
} from '@api/stammdaten';

describe('QuellenListeComponent', () => {
  const betriebsstaette: BetriebsstaetteDto = {
    adresse: { plz: '22085', ort: 'Hamburg' },
    betriebsstatus: undefined,
    berichtsjahr: undefined,
    betriebsstaetteNr: '',
    land: '',
    gemeindekennziffer: undefined,
    name: '',
    zustaendigeBehoerde: undefined,
    id: 1,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  let dataSubject: Subject<Data>;

  let component: QuellenListeComponent;
  let fixture: ComponentFixture<QuellenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        {
          provide: QuellenRestControllerService,
          useValue: MockService(QuellenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
      declarations: [QuellenListeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(QuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save Id from betriebsstaette', () => {
    expect(component.betriebsstaette.id).toBe(betriebsstaette.id);
  });
});
