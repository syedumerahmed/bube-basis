import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClrDatagridSortOrder } from '@clr/angular';
import { AlertService } from '@/base/service/alert.service';
import {
  BetriebsstaetteDto,
  QuelleListItemDto,
  QuellenRestControllerService,
} from '@api/stammdaten';
import {
  ClarityIcons,
  cloudIcon,
  eyeIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(cloudIcon, trashIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-quellen-liste',
  templateUrl: './quellen-liste.component.html',
  styleUrls: ['./quellen-liste.component.scss'],
})
export class QuellenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  quellenListe: QuelleListItemDto[];
  betriebsstaette: BetriebsstaetteDto;
  loading = true;
  loeschenDialog: boolean;
  quelleId: number;
  selected: QuelleListItemDto[] = [];
  loescht = false;

  constructor(
    private route: ActivatedRoute,
    private quellenRestControllerService: QuellenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaette = data.betriebsstaette;
      this.refresh();
    });
  }

  refresh(): void {
    if (this.neueBetriebsstaette()) {
      this.loading = false;
      this.quellenListe = [];
      return;
    }

    this.loading = true;
    this.quellenRestControllerService
      .readAllQuellenFromBetriebsstaette(this.betriebsstaette.id)
      .subscribe(
        (liste) => {
          this.quellenListe = liste;
          this.loading = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  deleteQuelle(): void {
    this.quellenRestControllerService
      .deleteQuelle(this.betriebsstaette.id, this.quelleId)
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          // Data Grid refresh
          this.refresh();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  deleteQuellen(): void {
    this.loescht = true;
    this.quellenRestControllerService
      .deleteMultipleQuellen(
        this.betriebsstaette.id,
        this.selected.map((quelle) => quelle.id)
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.selected = [];
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  neueBetriebsstaette(): boolean {
    return !this.betriebsstaette?.id;
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.quelleId = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(quelleId: number): void {
    this.quelleId = quelleId;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }
}
