import { Component, OnInit } from '@angular/core';
import { ClrDatagridSortOrder } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import {
  AnlageDto,
  QuelleListItemDto,
  QuellenRestControllerService,
} from '@api/stammdaten';
import {
  ClarityIcons,
  cloudIcon,
  eyeIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(trashIcon, cloudIcon, plusIcon, eyeIcon);
@Component({
  selector: 'app-anlage-quellen-liste',
  templateUrl: './anlage-quellen-liste.component.html',
  styleUrls: ['./anlage-quellen-liste.component.scss'],
})
export class AnlageQuellenListeComponent implements OnInit {
  readonly SortOrder = ClrDatagridSortOrder;

  quellenListe: QuelleListItemDto[];
  anlage: AnlageDto;
  loading = true;
  loeschenDialog: boolean;
  quelleId: number;
  selected: QuelleListItemDto[] = [];
  loescht = false;

  constructor(
    private route: ActivatedRoute,
    private quellenRestControllerService: QuellenRestControllerService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.anlage = data.anlage;
      this.refresh();
    });
  }

  refresh(): void {
    if (this.neueAnlage()) {
      this.quellenListe = [];
      this.loading = false;
      return;
    }

    this.loading = true;
    this.quellenRestControllerService
      .readAllQuellenFromAnlage(
        this.anlage.id,
        this.anlage.parentBetriebsstaetteId
      )
      .subscribe(
        (liste) => {
          this.quellenListe = liste;
          this.loading = false;
        },
        (error) => {
          this.alertService.setAlert(error);
          this.loading = false;
        }
      );
  }

  deleteQuelle(): void {
    this.quellenRestControllerService
      .deleteQuelle(this.anlage.parentBetriebsstaetteId, this.quelleId)
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          // Data Grid refresh
          this.refresh();
        },
        (err) => this.alertService.setAlert(err)
      );
  }

  deleteQuellen(): void {
    this.loescht = true;
    this.quellenRestControllerService
      .deleteMultipleQuellen(
        this.anlage.parentBetriebsstaetteId,
        this.selected.map((quelle) => quelle.id)
      )
      .subscribe(
        () => {
          this.closeLoeschenDialog();
          this.selected = [];
          this.refresh();
        },
        (err) => {
          this.closeLoeschenDialog();
          this.alertService.setAlert(err);
        }
      );
  }

  get canDelete(): boolean {
    return this.route.snapshot.data.loeschRecht;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get nothingSelected(): boolean {
    return this.selected.length === 0;
  }

  neueAnlage(): boolean {
    return !this.anlage.id;
  }

  closeLoeschenDialog(): void {
    this.loescht = false;
    this.quelleId = undefined;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(anlageId: number): void {
    this.quelleId = anlageId;
    this.loeschenDialog = true;
  }

  openMarkierteLoeschenDialog(): void {
    this.loeschenDialog = true;
  }
}
