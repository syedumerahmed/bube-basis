import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnlageQuellenListeComponent } from './anlage-quellen-liste.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute, Data } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AnlageDto, QuellenRestControllerService } from '@api/stammdaten';

describe('AnlageQuellenListeComponent', () => {
  const anlage: AnlageDto = {
    anlageNr: '',
    name: '',
    parentBetriebsstaetteId: 0,
    id: 1,
  };

  let dataSubject: Subject<Data>;
  let component: AnlageQuellenListeComponent;
  let fixture: ComponentFixture<AnlageQuellenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ anlage });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        {
          provide: QuellenRestControllerService,
          useValue: MockService(QuellenRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { anlage } },
          },
        },
      ],
      declarations: [AnlageQuellenListeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AnlageQuellenListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Liste nach kopieren des Parents', () => {
    beforeEach(() => {
      component.quellenListe = [
        { id: 1, nummer: '1', name: 'name' },
        { id: 2, nummer: '2', name: 'name2' },
      ];
      component.anlage = { id: null } as AnlageDto;
      component.refresh();
    });
    it('should have empty data when parent is new', () => {
      expect(component.quellenListe).toEqual([]);
    });
  });
});
