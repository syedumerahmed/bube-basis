import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { QuelleDetailsComponent } from '@/stammdaten/quelle/component/quelle-details/quelle-details.component';
import { QuelleAllgemeinComponent } from '@/stammdaten/quelle/component/quelle-allgemein/quelle-allgemein.component';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import {
  QuelleDto,
  QuellenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';
import spyOn = jest.spyOn;

describe('QuelleDetailsComponent', () => {
  const epsgCodeListe: Array<ReferenzDto> = [
    { schluessel: '01' },
  ] as Array<ReferenzDto>;

  const quelle: QuelleDto = {
    parentBetriebsstaetteId: 1,
    name: 'quelle',
    quelleNr: '2',
  };

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const betriebsstaette = { id: 1 };
  const routeData = new BehaviorSubject({
    epsgCodeListe,
    quelle,
    betriebsstaette,
  });
  let component: QuelleDetailsComponent;
  let fixture: ComponentFixture<QuelleDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
      ],
      declarations: [
        QuelleDetailsComponent,
        QuelleAllgemeinComponent,
        MockComponents(ReferenzSelectComponent, GeoPunktComponent),
      ],
      providers: [
        MockProviders(
          BerichtsjahrService,
          QuellenRestControllerService,
          StammdatenUebernahmeRestControllerService
        ),
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData.asObservable(),
            snapshot: {
              params: { quelleNummer: '2' },
              queryParams: {},
              data: { epsgCodeListe, schreibRecht: true },
            },
          },
        },
      ],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);

    fixture = TestBed.createComponent(QuelleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable berichtsjahr', inject(
    [BerichtsjahrService],
    (berichtsjahrService) => {
      expect(berichtsjahrService.disableDropdown).toHaveBeenCalled();
    }
  ));

  it('form should have initial state', () => {
    const form = component.quelleForm;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
    expect(form.untouched).toBe(true);
  });

  it('should be neu', () => {
    expect(component.isNeu).toBe(true);
  });

  it('should disable form with readonly user', inject(
    [ActivatedRoute],
    (route: ActivatedRoute) => {
      route.snapshot.data.schreibRecht = false;
      component.ngOnInit();
      expect(component.quelleForm.enabled).toBe(false);
    }
  ));

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.quelleForm.markAsDirty();
      component.quelleForm.markAsTouched();
      component.quelleForm.patchValue({ allgemein: { laenge: -1 } });
    });

    it('should be invalid', () => {
      expect(component.quelleForm.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          QuelleAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [QuellenRestControllerService],
        (quellenService: QuellenRestControllerService) => {
          expect(quellenService.createQuelle).not.toHaveBeenCalled();
          expect(quellenService.updateQuelle).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.quelleForm.patchValue({
        allgemein: {
          name: 'Meine neue Quelle',
          bemerkung: 'bemerkung',
        },
      });
      component.quelleForm.markAsDirty();
      component.quelleForm.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.quelleForm.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedQuelleDto: QuelleDto = {
        parentBetriebsstaetteId: 1,
        bauhoehe: null,
        durchmesser: null,
        breite: null,
        flaeche: null,
        laenge: null,
        quelleNr: '2',
        name: 'Meine neue Quelle',
        bemerkung: 'bemerkung',
        geoPunkt: null,
      };

      let quelleObservable: Subject<QuelleDto>;
      beforeEach(inject(
        [QuellenRestControllerService],
        (quellenService: QuellenRestControllerService) => {
          quelleObservable = new Subject<QuelleDto>();
          spyOn(quellenService, 'createQuelle').mockReturnValue(
            quelleObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [QuellenRestControllerService],
        (quellenService: QuellenRestControllerService) => {
          expect(quellenService.createQuelle).toHaveBeenCalledWith(
            expectedQuelleDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedQuelleDto,
          parentBetriebsstaetteId: null,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(inject([Router], (router: Router) => {
          spyOn(router, 'navigateByUrl').mockResolvedValue(true);
          quelleObservable.next(responseDto);
          quelleObservable.complete();
        }));

        it('should update quelle and set parentBetriebsstaetteId', () => {
          expect(component.quelle).toStrictEqual({
            ...responseDto,
            parentBetriebsstaetteId: 1,
          });
        });

        it('should reset form', () => {
          expect(component.quelleForm.touched).toBe(false);
          expect(component.quelleForm.dirty).toBe(false);
        });

        it('should update URL', inject([Router], (router: Router) => {
          expect(router.navigateByUrl).toHaveBeenCalled();
        }));
      });
    });
  });

  describe('copy', () => {
    const currentQuelle = {
      id: 1,
      quelleNr: '123',
      name: 'Quelle 1',
    } as QuelleDto;
    beforeEach(inject([Router], (router: Router) => {
      spyOn(router, 'navigate').mockReturnValue(null);
      component.quelle = currentQuelle;
      component.copy();
    }));
    it('should navigate to kopieren', inject(
      [Router, ActivatedRoute],
      (router, route) => {
        expect(router.navigate).toHaveBeenCalledWith(['quelle', 'kopieren'], {
          relativeTo: route.parent,
          state: { quelle: currentQuelle },
        });
      }
    ));
  });

  describe('on destroy', () => {
    beforeEach(() => {
      component.ngOnDestroy();
    });
    it('should reset berichtsjahr default', inject(
      [BerichtsjahrService],
      (berichtsjahrService) => {
        expect(berichtsjahrService.restoreDropdownDefault).toHaveBeenCalled();
      }
    ));
  });
});
