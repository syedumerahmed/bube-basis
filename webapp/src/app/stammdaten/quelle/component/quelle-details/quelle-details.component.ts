import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  QuelleAllgemeinComponent,
  QuelleAllgemeinFormData,
} from '@/stammdaten/quelle/component/quelle-allgemein/quelle-allgemein.component';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  GeoPunktDto,
  QuelleDto,
  QuellenRestControllerService,
  ReferenzDto,
} from '@api/stammdaten';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  ClarityIcons,
  cloudIcon,
  copyIcon,
  floppyIcon,
  importIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { StammdatenUebernahmeRestControllerService } from '@api/stammdaten-uebernahme';

export interface QuelleFormData {
  allgemein: QuelleAllgemeinFormData;
}

ClarityIcons.addIcons(cloudIcon, copyIcon, importIcon, timesIcon, floppyIcon);

@Component({
  selector: 'app-quelle-details',
  templateUrl: './quelle-details.component.html',
  styleUrls: ['./quelle-details.component.scss'],
})
export class QuelleDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges
{
  @ViewChild(QuelleAllgemeinComponent)
  allgemeinComponent: QuelleAllgemeinComponent;

  quelle: QuelleDto;
  quelleForm: FormGroup<QuelleFormData>;

  saveAttempt = false;

  private parentBetriebsstaetteId;
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  private static mapToForm(quelleDto: QuelleDto): QuelleFormData {
    return {
      allgemein: QuelleAllgemeinComponent.toForm(quelleDto),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly quellenService: QuellenRestControllerService,
    private readonly uebernahmeService: StammdatenUebernahmeRestControllerService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.parentBetriebsstaetteId = data.betriebsstaette.id;
      this.epsgCodeListe = new Referenzliste(data.epsgCodeListe);
      this.setQuelle(data.quelle);
      this.reset();
    });
    this.quelleForm = this.createQuelleForm(
      QuelleDetailsComponent.mapToForm(this.quelle)
    );
    if (!this.canWrite) {
      this.quelleForm.disable();
    }
    if (this.route.snapshot.queryParams.spbQuelleId != null) {
      this.quelleForm.markAsDirty();
    }
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  save(): void {
    this.saveAttempt = true;
    if (this.quelleForm.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      return;
    }
    if (this.quelle.parentAnlageId) {
      this.quelle.parentBetriebsstaetteId = null;
    }

    const formData = this.quelleForm.value;
    const quelleDto: QuelleDto = {
      ...this.quelle,
      ...FormUtil.emptyStringsToNull(formData.allgemein),
      geoPunkt: this.getGeoPunkt(formData.allgemein.geoPunkt),
    };

    if (this.isNeu) {
      this.createQuelle(quelleDto);
    } else {
      this.updateQuelle(quelleDto);
    }
  }

  createQuelle(quelleDto: QuelleDto): void {
    const spbQuelleId = this.route.snapshot.queryParams.spbQuelleId;

    const createQuelle = spbQuelleId
      ? this.uebernahmeService.uebernehmeNeueQuelle(spbQuelleId, quelleDto)
      : this.quellenService.createQuelle(quelleDto);
    createQuelle.subscribe(
      (quelle) => {
        this.setQuelle(quelle);
        this.reset();
        this.updateUrlAfterCreate(quelle.quelleNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  updateQuelle(quelleDto: QuelleDto): void {
    this.quellenService.updateQuelle(quelleDto).subscribe(
      (quelle) => {
        this.setQuelle(quelle);
        this.reset();
        this.updateUrlAfterChange(quelle.quelleNr);
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  getGeoPunkt(geoPunkt: QuelleAllgemeinFormData['geoPunkt']): GeoPunktDto {
    let punkt: GeoPunktDto = null;

    if (geoPunkt.epsgCode !== null) {
      punkt = {
        nord: geoPunkt.nord,
        ost: geoPunkt.ost,
        epsgCode: geoPunkt.epsgCode,
      };
    }
    return punkt;
  }

  reset(): void {
    if (this.quelleForm) {
      this.saveAttempt = false;
      const originalQuelle = QuelleDetailsComponent.mapToForm(this.quelle);
      this.quelleForm.reset(originalQuelle);
    }
  }

  navigateBack(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent });
  }

  copy(): void {
    this.router.navigate(['quelle', 'kopieren'], {
      relativeTo: this.route.parent,
      state: { quelle: this.quelle },
    });
  }

  get isNeu(): boolean {
    return this.quelle.id == null;
  }

  get canWrite(): boolean {
    return this.route.snapshot.data.schreibRecht;
  }

  get allgemeinForm(): FormGroup<QuelleAllgemeinFormData> {
    return this.quelleForm.get(
      'allgemein'
    ) as FormGroup<QuelleAllgemeinFormData>;
  }

  private setQuelle(quelle: QuelleDto) {
    this.quelle = quelle;
    if (!this.quelle.parentBetriebsstaetteId) {
      this.quelle.parentBetriebsstaetteId = this.parentBetriebsstaetteId;
    }
  }

  private createQuelleForm(
    initialQuelle: QuelleFormData
  ): FormGroup<QuelleFormData> {
    return this.fb.group<QuelleFormData>({
      allgemein: QuelleAllgemeinComponent.createForm(
        this.fb,
        initialQuelle.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });
  }

  private updateUrl(nummer: string, replace: string | RegExp): void {
    let url = this.router
      .createUrlTree([], { relativeTo: this.route })
      .toString();
    url = url.replace(replace, '/quelle/nummer/' + encodeURI(nummer));
    this.router.navigateByUrl(url);
  }

  private updateUrlAfterCreate(nummer: string): void {
    // Hier steht entweder kopieren, uebernehmen oder neu in der URL und muss ersetzt werden
    this.updateUrl(nummer, new RegExp('/quelle/(kopieren|neu|uebernehmen)+'));
  }

  private updateUrlAfterChange(nummer: string): void {
    this.updateUrl(
      nummer,
      '/quelle/nummer/' + this.route.snapshot.params.quelleNummer
    );
  }

  isChildOfBetriebsstaette(): boolean {
    return !this.quelle.parentAnlageId;
  }

  betreiberdatenUebernehmen(): void {
    const aktuellesJahr =
      this.berichtsjahrService.selectedBerichtsjahr.schluessel;
    const routeBst = this.route.snapshot.data.betriebsstaette;
    const routeAnlage = this.route.snapshot.parent.data.anlage;
    this.router.navigate([
      '/stammdaten/jahr/' +
        aktuellesJahr +
        '/uebernahme/quelle/' +
        this.quelle.id,
      {
        berichtsjahr: aktuellesJahr,
        land: routeBst.land,
        betriebsstaetteNummer: routeBst.betriebsstaetteNr,
        betriebsstaetteId: routeBst.id,
        anlageNr: routeAnlage?.anlageNr,
        hasAnlage: routeAnlage != null,
      },
    ]);
  }

  canDeactivate(): boolean {
    return this.quelleForm.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
