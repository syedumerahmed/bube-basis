import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { QuelleAllgemeinComponent } from './quelle-allgemein.component';
import { FormBuilder } from '@/base/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { MockComponents, MockModule } from 'ng-mocks';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { QuelleDto, ReferenzDto } from '@api/stammdaten';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';

const quelle: QuelleDto = {
  parentBetriebsstaetteId: 1,
  bauhoehe: 1,
  bemerkung: 'Bemerkung',
  breite: 2,
  durchmesser: 3,
  flaeche: 4,
  laenge: 5,
  name: 'Quelle',
  quelleNr: 'Nr-1',
  quellenArt: { schluessel: '01' } as ReferenzDto,
};

describe('QuelleAllgemeinComponent', () => {
  const quellenArtListe: Array<ReferenzDto> = [];
  let component: QuelleAllgemeinComponent;
  let fixture: ComponentFixture<QuelleAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        QuelleAllgemeinComponent,
        MockComponents(ReferenzSelectComponent, GeoPunktComponent),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { data: { quellenArtListe } } },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(QuelleAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = QuelleAllgemeinComponent.createForm(
      formBuilder,
      QuelleAllgemeinComponent.toForm(quelle),
      {} as ReferenzDto
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
