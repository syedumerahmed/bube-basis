import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { QuelleDto, ReferenzDto } from '@api/stammdaten';
import {
  GeoPunktComponent,
  GeoPunktFormData,
} from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';

export interface QuelleAllgemeinFormData {
  quelleNr: string;
  name: string;
  geoPunkt: GeoPunktFormData;
  flaeche: number;
  laenge: number;
  breite: number;
  durchmesser: number;
  bauhoehe: number;
  bemerkung: string;
}

@Component({
  selector: 'app-quelle-allgemein',
  templateUrl: './quelle-allgemein.component.html',
  styleUrls: ['./quelle-allgemein.component.scss'],
})
export class QuelleAllgemeinComponent
  implements AfterViewInit, AfterViewChecked
{
  @Input()
  form: FormGroup<QuelleAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;
  @ViewChild(GeoPunktComponent)
  geoPunktComponent: GeoPunktComponent;

  @Input()
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  static toForm(quelleDto: QuelleDto): QuelleAllgemeinFormData {
    return {
      quelleNr: quelleDto.quelleNr,
      name: quelleDto.name,
      geoPunkt: GeoPunktComponent.toForm(quelleDto.geoPunkt),
      bauhoehe: quelleDto.bauhoehe,
      bemerkung: quelleDto.bemerkung,
      breite: quelleDto.breite,
      durchmesser: quelleDto.durchmesser,
      flaeche: quelleDto.flaeche,
      laenge: quelleDto.laenge,
    };
  }

  static createForm(
    fb: FormBuilder,
    quelle: QuelleAllgemeinFormData,
    selectedBerichtsjahr: ReferenzDto
  ): FormGroup<QuelleAllgemeinFormData> {
    return fb.group<QuelleAllgemeinFormData>({
      quelleNr: [
        quelle.quelleNr,
        [Validators.required, Validators.maxLength(20)],
      ],
      name: [quelle.name, [Validators.required, Validators.maxLength(250)]],
      geoPunkt: GeoPunktComponent.createForm(
        fb,
        quelle.geoPunkt,
        selectedBerichtsjahr
      ),
      flaeche: [
        quelle.flaeche,
        [Validators.min(0), Validators.max(999999.999)],
      ],
      laenge: [quelle.laenge, [Validators.min(0), Validators.max(9999.999)]],
      breite: [quelle.breite, [Validators.min(0), Validators.max(9999.999)]],
      durchmesser: [
        quelle.durchmesser,
        [Validators.min(0), Validators.max(9999.999)],
      ],
      bauhoehe: [quelle.bauhoehe, [Validators.min(0), Validators.max(999.99)]],
      bemerkung: [quelle.bemerkung, Validators.maxLength(1000)],
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }

  get geoPunktForm(): FormGroup<GeoPunktFormData> {
    return this.form.get('geoPunkt') as FormGroup<GeoPunktFormData>;
  }
}
