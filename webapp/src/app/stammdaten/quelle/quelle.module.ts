import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';

import { QuellenListeComponent } from './component/quellen-liste/quellen-liste.component';
import { QuelleDetailsComponent } from './component/quelle-details/quelle-details.component';
import { AnlageQuellenListeComponent } from './component/anlage-quellen-liste/anlage-quellen-liste.component';
import { QuelleAllgemeinComponent } from './component/quelle-allgemein/quelle-allgemein.component';
import { BaseModule } from '@/base/base.module';

import { RouterModule } from '@angular/router';
import { BasedomainModule } from '@/basedomain/basedomain.module';

@NgModule({
  declarations: [
    QuellenListeComponent,
    QuelleDetailsComponent,
    AnlageQuellenListeComponent,
    QuelleDetailsComponent,
    QuelleAllgemeinComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    ReactiveFormsModule,
    RouterModule,
    BaseModule,
    BasedomainModule,
  ],
  exports: [AnlageQuellenListeComponent, QuellenListeComponent],
})
export class QuelleModule {}
