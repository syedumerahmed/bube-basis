import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { AlertService } from '@/base/service/alert.service';
import { ParameterWertDto } from '@api/referenzdaten';

@Injectable({
  providedIn: 'root',
})
export class QuellenZuordungService {
  readonly PARAMETER_SCHLUESSEL = 'que_zuord';

  constructor(
    private readonly parameterService: ParameterService,
    private readonly alertService: AlertService
  ) {}

  public quellenSindAnAnlagen(
    berichtsjahr: string,
    land: string
  ): Observable<boolean> {
    return this.readQuellenZuordnung(berichtsjahr, land).pipe(
      map((parameterWert: ParameterWertDto) => parameterWert.wert === 'Anlage')
    );
  }

  public quellenSindAnBetriebsstaetten(
    berichtsjahr: string,
    land: string
  ): Observable<boolean> {
    return this.readQuellenZuordnung(berichtsjahr, land).pipe(
      map((parameterWert: ParameterWertDto) => parameterWert.wert === 'BST')
    );
  }

  private readQuellenZuordnung(
    berichtsjahr: string,
    land: string
  ): Observable<ParameterWertDto> {
    return this.parameterService
      .readParameterWert(this.PARAMETER_SCHLUESSEL, berichtsjahr, land)
      .pipe(this.alertService.catchError());
  }
}
