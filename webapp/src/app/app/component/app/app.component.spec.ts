import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { MockComponents, MockModule } from 'ng-mocks';
import { ToolbarComponent } from '@/app/component/toolbar/toolbar.component';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  RouterEvent,
} from '@angular/router';
import { Subject } from 'rxjs';
import { AlertComponent } from '@/base/component/alert/alert.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  const routerEvent$ = new Subject<RouterEvent>();
  const routerMock = { events: routerEvent$.asObservable() };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [
        AppComponent,
        ...MockComponents(ToolbarComponent, AlertComponent),
      ],
      providers: [{ provide: Router, useValue: routerMock }],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('navigation start', () => {
    beforeEach(() => {
      routerEvent$.next(new NavigationStart(1, '/stammdaten'));
    });

    it('should start loading when navigation starts', () => {
      expect(component.loading).toBe(true);
    });

    it.each([
      [
        new NavigationEnd(1, '', ''),
        new NavigationCancel(1, '', ''),
        new NavigationError(1, '', null),
      ],
    ])('should cancel loading on %s', (event) => {
      routerEvent$.next(event);
      expect(component.loading).toBe(false);
    });
  });
});
