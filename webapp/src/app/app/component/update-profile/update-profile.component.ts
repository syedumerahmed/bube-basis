import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Validators } from '@angular/forms';
import { AlertService } from '@/base/service/alert.service';
import { BenutzerRestControllerService } from '@api/benutzer';
import { BubeValidators } from '@/base/forms/bube-validators';
import { ClarityIcons, userIcon } from '@cds/core/icon';

interface EmailFormData {
  email;
}

ClarityIcons.addIcons(userIcon);

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss'],
})
export class UpdateProfileComponent implements OnInit {
  emailForm: FormGroup<EmailFormData>;

  constructor(
    private readonly fb: FormBuilder,
    private readonly benutzerService: BenutzerRestControllerService,
    private readonly alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.emailForm = this.fb.group({
      email: [
        '',
        [Validators.required, Validators.maxLength(80), BubeValidators.email],
      ],
    });
  }

  saveEmail(): void {
    if (this.emailForm.invalid) {
      return;
    }
    const email = this.emailForm.value.email;
    this.benutzerService.updateProfile({ email }).subscribe(
      () => {
        // Der Reload wird benötigt, damit das Profil noch mal neu von Keycloak geladen wird
        // Ansonsten denkt Angular, dass immernoch keine E-Mail gesetzt ist
        location.reload();
      },
      (err) => {
        this.alertService.setAlert(err.error);
      }
    );
  }
}
