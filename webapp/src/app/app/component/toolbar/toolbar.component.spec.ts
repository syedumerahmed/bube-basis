import {
  ComponentFixture,
  fakeAsync,
  inject,
  TestBed,
  tick,
} from '@angular/core/testing';

import { ToolbarComponent } from './toolbar.component';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { MockModule, MockProvider, MockService } from 'ng-mocks';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { BenutzerProfilService } from '@/base/service/benutzer-profil.service';
import { AlertService } from '@/base/service/alert.service';
import { JobStatusService } from '@/base/service/job-status.service';
import { Observable, of, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import {
  BenutzerJobRestControllerService,
  EnvironmentRestControllerService,
  JobStatusDto,
} from '@api';
import spyOn = jest.spyOn;

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ToolbarComponent],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: BenutzerProfilService,
          useValue: MockService(BenutzerProfilService),
        },
        { provide: AlertService, useValue: MockService(AlertService) },
        {
          provide: BenutzerJobRestControllerService,
          useValue: MockService(BenutzerJobRestControllerService),
        },
        { provide: JobStatusService, useValue: MockService(JobStatusService) },
        MockProvider(EnvironmentRestControllerService, {
          getVersion(): Observable<any> {
            return of();
          },
        }),
      ],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
  }));

  it('should show name', inject(
    [BenutzerProfil, BenutzerJobRestControllerService],
    (
      benutzerProfil: BenutzerProfil,
      jobService: BenutzerJobRestControllerService
    ) => {
      spyOn(jobService, 'readJobStatus').mockReturnValue(of(null));
      component.ngOnInit();
      expect(component.benutzerName).toBe(benutzerProfil.displayName);
      component.ngOnDestroy();
    }
  ));

  // Da diese Komponente Timer/Intervalle nutzt, wird in jedem Test die fakeAsync-Zone benutzt
  // und die Timer durch ngOnInit() erstellt und ngOnDestroy() aufgeräumt
  describe('JobStatus', () => {
    it('should not read status with no job running', fakeAsync(
      inject(
        [JobStatusService, BenutzerJobRestControllerService],
        (
          jobStatusService: JobStatusService,
          jobService: BenutzerJobRestControllerService
        ) => {
          spyOn(jobStatusService, 'jobIsRunning').mockReturnValue(false);
          spyOn(jobService, 'readJobStatus').mockReturnValue(of(null));

          component.ngOnInit();
          expect(jobService.readJobStatus).toHaveBeenCalledTimes(1); // Durch das OnInit() ein Aufruf
          tick(3_000);
          expect(jobService.readJobStatus).toHaveBeenCalledTimes(1);
          component.ngOnDestroy();
        }
      )
    ));

    describe('with running Job', () => {
      let response$: Subject<JobStatusDto>;
      beforeEach(inject(
        [JobStatusService, BenutzerJobRestControllerService],
        (
          jobStatusService: JobStatusService,
          jobService: BenutzerJobRestControllerService
        ) => {
          response$ = new Subject<JobStatusDto>();
          spyOn(jobStatusService, 'jobIsRunning').mockReturnValue(true);
          spyOn(jobService, 'readJobStatus').mockReturnValue(
            response$.asObservable() as Observable<any>
          );
        }
      ));

      it('should read jobStatus after 2.5s', fakeAsync(
        inject(
          [BenutzerJobRestControllerService],
          (jobService: BenutzerJobRestControllerService) => {
            component.ngOnInit();
            tick(3_000);

            expect(jobService.readJobStatus).toHaveBeenCalledTimes(2);

            component.ngOnDestroy();
          }
        )
      ));

      it('ongoing', fakeAsync(() => {
        const response: JobStatusDto = { completed: false };

        component.ngOnInit();
        tick(3_000);
        response$.next(response);

        expect(component.jobStatus).toBe(response);
        component.ngOnDestroy();
      }));

      it('finished', fakeAsync(
        inject([JobStatusService], (jobStatusService: JobStatusService) => {
          const response: JobStatusDto = { completed: true };

          component.ngOnInit();
          tick(3_000);
          response$.next(response);
          tick();

          expect(component.jobStatus).toEqual(response);
          expect(jobStatusService.jobEnded).toHaveBeenCalled();

          component.ngOnDestroy();
        })
      ));

      it('server error', fakeAsync(
        inject([AlertService], (alertService: AlertService) => {
          const response: JobStatusDto = {
            error: 'Unerwarteter Fehler',
            action: { label: 'actionLabel', route: 'actionRoute' },
          };

          component.ngOnInit();
          tick(3_000);
          response$.next(response);

          expect(alertService.setAlert).toHaveBeenCalledWith(
            response.error,
            response.action
          );
          component.ngOnDestroy();
        })
      ));

      it('network error', fakeAsync(
        inject([AlertService], (alertService: AlertService) => {
          const error: HttpErrorResponse = new HttpErrorResponse({
            error: 'Unerwarteter Fehler',
          });

          component.ngOnInit();
          tick(3_000);
          response$.error(error);

          expect(alertService.setAlert).toHaveBeenCalledWith(error);
          component.ngOnDestroy();
        })
      ));
    });
  });
});
