import { Component, OnDestroy, OnInit } from '@angular/core';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BenutzerProfilService } from '@/base/service/benutzer-profil.service';
import { AlertService } from '@/base/service/alert.service';
import { JobStatusService } from '@/base/service/job-status.service';
import { interval, Subscription } from 'rxjs';
import { BenutzerJobRestControllerService } from '@api/api/benutzerJob.service';
import { JobStatusDto } from '@api/model/jobStatusDto';
import { Rolle } from '@/base/security/model/Rolle';
import {
  angleIcon,
  ClarityIcons,
  cogIcon,
  listIcon,
  logoutIcon,
  unknownStatusIcon,
  userIcon,
} from '@cds/core/icon';
import { EnvironmentRestControllerService } from '@api';
import { FormUtil } from '@/base/forms';

ClarityIcons.addIcons(
  listIcon,
  unknownStatusIcon,
  userIcon,
  angleIcon,
  cogIcon,
  logoutIcon
);

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit, OnDestroy {
  jobStatus: JobStatusDto;
  protokollOpenDialog: boolean;
  private intervalSubscription: Subscription;
  private jobEndMillis: number;
  versionTooltip: string;

  constructor(
    private readonly benutzer: BenutzerProfil,
    public readonly benutzerProfilService: BenutzerProfilService,
    private readonly jobService: BenutzerJobRestControllerService,
    private readonly alertService: AlertService,
    private readonly jobStatusService: JobStatusService,
    private readonly environmentService: EnvironmentRestControllerService
  ) {}

  ngOnInit(): void {
    this.intervalSubscription = interval(2500).subscribe(() => {
      this.readJobStatus();
    });

    // Gibt es noch einen Job der geleaufen ist?
    this.jobService.readJobStatus().subscribe(
      (status) => {
        this.jobStatus = status;
      },
      (err) => {
        this.alertService.setAlert(err);
      }
    );
    this.environmentService.getVersion().subscribe((version) => {
      this.versionTooltip =
        'Version: ' +
        version.version +
        '\nGebaut: ' +
        FormUtil.fromIsoDateTime(version.buildDate);
    });
  }

  ngOnDestroy(): void {
    this.intervalSubscription.unsubscribe();
  }

  get showJobRunning(): boolean {
    // Der Job wird noch 5 Sekunden länger in der Toolbar angezeigt
    return (
      this.jobStatusService.jobIsRunning() ||
      Date.now() - this.jobEndMillis < 5_000
    );
  }

  readJobStatus(): void {
    if (this.jobStatusService.jobIsRunning()) {
      this.jobService.readJobStatus().subscribe(
        (status) => {
          this.jobStatus = status;
          this.jobEndMillis = 0;
          if (status && (status.completed || status.error)) {
            this.jobStatusService.jobEnded(status);
            this.jobEndMillis = Date.now();
            if (status.error) {
              this.alertService.setAlert(status.error, status.action);
            } else if (status.warnungVorhanden) {
              this.alertService.setWarning(
                `Job ${status.jobName} nach ${status.runtimeSec} Sekunden mit Warnung(en) beendet.
                 Anzahl Datensätze: ${status.count}`,
                status.action
              );
            } else {
              this.alertService.setSuccess(
                `Job ${status.jobName} nach ${status.runtimeSec} Sekunden beendet.
                 Anzahl Datensätze: ${status.count}`,
                status.action
              );
            }
          }
        },
        (err) => {
          this.alertService.setAlert(err);
          this.jobStatusService.isRunning = false;
        }
      );
    }
  }

  get benutzerName(): string {
    return this.benutzer.displayName;
  }

  get canSeeStammdatenModule(): boolean {
    return this.benutzer.canSeeStammdatenModule;
  }

  get canSeeStammdatenBetreiberModule(): boolean {
    return this.benutzer.canSeeStammdatenBetreiberModule;
  }

  get canSeeReferenzdatenModule(): boolean {
    return this.benutzer.canSeeReferenzdatenModule;
  }

  get canSeeBenutzerverwaltungModule(): boolean {
    return this.benutzer.canSeeBenutzerverwaltungModule;
  }

  get canSeeEuRegistryModule(): boolean {
    return this.benutzer.canSeeEURegBerichtsdaten;
  }

  get canSeeKomplexpruefungModule(): boolean {
    return this.benutzer.canSeeKomplexpruefungModule;
  }

  get brandingLink(): string {
    if (this.benutzer.hasRolle(Rolle.BETREIBER)) {
      return '/stammdatenbetreiber';
    }
    return '/stammdaten';
  }
}
