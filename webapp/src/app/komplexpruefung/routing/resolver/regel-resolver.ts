import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { map } from 'rxjs/operators';
import { RegelDto, RegelRestControllerService } from '@api/komplexpruefung';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable()
export class RegelResolver implements Resolve<RegelDto> {
  constructor(
    private regelRestControllerService: RegelRestControllerService,
    private alertService: AlertService,
    private benutzerProfil: BenutzerProfil
  ) {}

  private copyRegel(regel: RegelDto): RegelDto {
    return {
      ...regel,
      nummer: null,
      land: this.benutzerProfil.landNr,
      typBundeseinheitlich: false,
      beschreibung: 'Kopie - ' + regel.beschreibung,
      letzteAenderung: null,
      typAenderungen: {},
    };
  }

  private createRegel(): RegelDto {
    return {
      beschreibung: null,
      code: null,
      fehlertext: null,
      gruppe: RegelDto.GruppeEnum.GRUPPE_A,
      objekte: [],
      typ: RegelDto.TypEnum.FEHLER,
      typBundeseinheitlich: false,
      gueltigBis: 2099,
      gueltigVon: 2007,
      nummer: null,
      land: this.benutzerProfil.landNr,
      typAenderungen: {},
    };
  }

  resolve(route: ActivatedRouteSnapshot): Observable<RegelDto> {
    const nummer = route.params.nummer;
    if (nummer === 'neu') {
      return of(this.createRegel());
    }
    if (nummer === 'kopieren') {
      return this.regelRestControllerService
        .loadRegel(route.queryParams.copyNummer)
        .pipe(map((regel) => this.copyRegel(regel)));
    }

    return this.regelRestControllerService
      .loadRegel(nummer)
      .pipe(this.alertService.catchError());
  }
}
