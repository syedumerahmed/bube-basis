import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  RegelListItemDto,
  RegelRestControllerService,
} from '@api/komplexpruefung';

@Injectable({ providedIn: 'root' })
export class RegelListResolver implements Resolve<Array<RegelListItemDto>> {
  constructor(
    private regelRestControllerService: RegelRestControllerService,
    private alertService: AlertService
  ) {}

  resolve(): Observable<Array<RegelListItemDto>> {
    return this.regelRestControllerService
      .readRegelliste()
      .pipe(this.alertService.catchError('Fehler beim Lesen der Regelliste'));
  }
}
