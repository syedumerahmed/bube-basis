import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KomplexpruefungComponent } from '@/komplexpruefung/component/komplexpruefung/komplexpruefung.component';
import { RegellisteComponent } from '@/komplexpruefung/component/regelliste/regelliste.component';
import { RegelListResolver } from '@/komplexpruefung/routing/resolver/regel-list-resolver';
import { RegelResolver } from '@/komplexpruefung/routing/resolver/regel-resolver';
import { RegelDetailsComponent } from '@/komplexpruefung/component/regel-details/regel-details.component';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const routes: Routes = [
  {
    path: '',
    component: KomplexpruefungComponent,
    children: [
      {
        path: 'regeln',
        component: RegellisteComponent,
        resolve: { regelliste: RegelListResolver },
      },
      {
        path: 'regeln/:nummer',
        component: RegelDetailsComponent,
        canDeactivate: [UnsavedChangesGuard],
        resolve: {
          regel: RegelResolver,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RegelResolver],
})
export class KomplexpruefungRoutingModule {}
