import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegellisteComponent } from './regelliste.component';
import {
  MockComponents,
  MockModule,
  MockProvider,
  MockProviders,
} from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute } from '@angular/router';
import { DatagridBooleanFilterComponent } from '@/base/component/datagrid-boolean-filter/datagrid-boolean-filter.component';
import { GemeindekoordinatenImportModalComponent } from '@/komplexpruefung/component/gemeindekoordinaten-import-modal/gemeindekoordinaten-import-modal.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  KoordinatenRestControllerService,
  RegelRestControllerService,
} from '@api/komplexpruefung';
import { ReferenzDto } from '@api/stammdaten';

describe('RegellisteComponentComponent', () => {
  let component: RegellisteComponent;
  let fixture: ComponentFixture<RegellisteComponent>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const berichtsjahrliste = [currentBerichtsjahr];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        RegellisteComponent,
        MockComponents(
          DatagridBooleanFilterComponent,
          GemeindekoordinatenImportModalComponent
        ),
      ],
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: { regel: null } },
          },
        },
        MockProviders(
          AlertService,
          RegelRestControllerService,
          KoordinatenRestControllerService
        ),
        MockProvider(BerichtsjahrService, {
          get alleBerichtsJahre$(): Observable<Array<ReferenzDto>> {
            return new BehaviorSubject(berichtsjahrliste);
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegellisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
