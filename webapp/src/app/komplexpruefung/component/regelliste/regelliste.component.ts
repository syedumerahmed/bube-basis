import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';
import { AlertService } from '@/base/service/alert.service';
import { Land, LandProperties } from '@/base/model/Land';
import { ClrDatagridSortOrder } from '@clr/angular';
import { JobStatusService } from '@/base/service/job-status.service';
import { ImportDialogCommand } from '@/komplexpruefung/component/gemeindekoordinaten-import-modal/gemeindekoordinaten-import-modal.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { take } from 'rxjs/operators';
import {
  ImportiereGemeindekoordinatenCommand,
  KoordinatenRestControllerService,
  RegelDto,
  RegelListItemDto,
  RegelRestControllerService,
} from '@api/komplexpruefung';
import { ReferenzDto } from '@api/stammdaten';
import {
  angleIcon,
  checkIcon,
  ClarityIcons,
  copyIcon,
  eyeIcon,
  plusIcon,
  shieldCheckIcon,
  timesIcon,
  trashIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(
  checkIcon,
  shieldCheckIcon,
  timesIcon,
  angleIcon,
  plusIcon,
  eyeIcon,
  copyIcon,
  trashIcon
);
@Component({
  selector: 'app-regelliste-component',
  templateUrl: './regelliste.component.html',
  styleUrls: ['./regelliste.component.scss'],
})
export class RegellisteComponent implements OnInit {
  public TypEnum = RegelDto.TypEnum;
  readonly SortOrder = ClrDatagridSortOrder;

  public berichtsjahrListe: Array<ReferenzDto>;
  public regelliste: Array<RegelListItemDto>;
  public selected: Array<RegelListItemDto> = [];

  public loeschenDialog: boolean;
  public regel: RegelListItemDto;

  public gemeindekoordinatenImportDialog: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly regelService: RegelRestControllerService,
    private readonly alertService: AlertService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly koordinatenService: KoordinatenRestControllerService,
    private readonly jobStatusService: JobStatusService,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {
    this.berichtsjahrService.alleBerichtsJahre$
      .pipe(take(1))
      .subscribe(
        (berichtsjahre) =>
          (this.berichtsjahrListe = berichtsjahre.sort(
            (a, b) => Number(b.schluessel) - Number(a.schluessel)
          ))
      );
  }

  ngOnInit(): void {
    this.regelliste = this.route.snapshot.data.regelliste;
  }

  get canHinzufuegen(): boolean {
    return this.benutzerProfil.canWriteRegeln;
  }

  get isGesamtadmin(): boolean {
    return this.benutzerProfil.isGesamtadmin;
  }

  get canTypAendern(): boolean {
    return (
      this.benutzerProfil.hasRolle(Rolle.ADMIN) &&
      !this.benutzerProfil.isGesamtadmin
    );
  }

  canCopy(regel: RegelListItemDto): boolean {
    if (!this.benutzerProfil.canWriteRegeln) {
      return false;
    }

    return this.benutzerProfil.isGesamtadmin || regel.gruppe != 'B';
  }

  canDelete(regel: RegelListItemDto): boolean {
    if (!this.benutzerProfil.canDeleteRegeln) {
      return false;
    }

    if (!this.benutzerProfil.isGesamtadmin && regel.gruppe == 'B') {
      return false;
    }

    return !(
      !this.benutzerProfil.isGesamtadmin &&
      regel.land != this.benutzerProfil.landNr
    );
  }

  closeLoeschenDialog(): void {
    this.regel = null;
    this.loeschenDialog = false;
  }

  openLoeschenDialog(regel: RegelListItemDto): void {
    this.regel = regel;
    this.loeschenDialog = true;
  }

  deleteRegel(): void {
    this.regelService.deleteRegel(this.regel.nummer).subscribe(
      () => {
        const index = this.regelliste.findIndex(
          (b) => b.nummer === this.regel.nummer
        );
        if (index !== -1) {
          this.regelliste.splice(index, 1);
        }
        this.closeLoeschenDialog();
        this.changeDetectorRef.markForCheck();
      },
      (error) => {
        this.alertService.setAlert(error);
        this.closeLoeschenDialog();
        this.changeDetectorRef.markForCheck();
      }
    );
  }

  getObjekte(regel: RegelListItemDto): string {
    return regel.objekte.join(', ').toString();
  }

  getBundeseinheitlich(regel: RegelListItemDto): string {
    return regel.typBundeseinheitlich ? 'Ja' : 'Nein';
  }

  kopieren(regel: RegelListItemDto): void {
    this.router.navigate(['kopieren'], {
      relativeTo: this.route,
      queryParams: { copyNummer: regel.nummer },
    });
  }

  hatKeineMarkierung(): boolean {
    return this.selected?.length === 0;
  }

  /* Ist die Regel nicht bundeseinheitlich oder das Land ist nicht Deutschland,
  dann darf man den Typ nicht ändern
  */
  hatKeineMarkierungFuerTypAenderung(): boolean {
    if (this.hatKeineMarkierung()) {
      return true;
    }

    for (const regel of this.selected) {
      if (
        regel.typBundeseinheitlich ||
        regel.land !== LandProperties[Land.DEUTSCHLAND].nr
      ) {
        return true;
      }
    }
    return false;
  }

  alleMarkieren(): void {
    this.selected = this.regelliste;
  }

  markierungAufheben(): void {
    this.selected = [];
  }

  typAendern(typ: RegelDto.TypEnum): void {
    const regelList: Array<number> = [];
    this.selected.forEach((regel) => regelList.push(regel.nummer));
    this.regelService
      .typFuerMeinLandAendern({ regeln: regelList, typ: typ })
      .subscribe(
        () => this.reloadList(),
        (err) => this.alertService.setAlert(err)
      );
  }

  private reloadList() {
    this.regelService.readRegelliste().subscribe(
      (regeln) => {
        this.regelliste = regeln;
        this.markierungAufheben();
        this.changeDetectorRef.markForCheck();
      },
      (err) => this.alertService.setAlert(err)
    );
  }

  getTypAenderungen(regel: RegelListItemDto): string {
    return regel.typAenderungen[this.benutzerProfil.landNr];
  }

  openGemeindekoordinatenImport(): void {
    this.gemeindekoordinatenImportDialog = true;
  }

  closeGemeindekoordinatenImport(): void {
    this.gemeindekoordinatenImportDialog = false;
  }

  gemeindekoordinatenImport(event: ImportDialogCommand): void {
    const importCommand: ImportiereGemeindekoordinatenCommand = {
      downloadLink: event.link,
      zieljahr: event.zieljahr,
    };

    this.koordinatenService.importGemeindekoordinaten(importCommand).subscribe(
      () => {
        this.jobStatusService.jobStarted();
      },
      (err) => {
        this.alertService.setAlert(err);
      }
    );
    // Funktioniert im Lambda nicht
    this.closeGemeindekoordinatenImport();
  }
}
