import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-komplexpruefung-component',
  templateUrl: './komplexpruefung.component.html',
  styleUrls: ['./komplexpruefung.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KomplexpruefungComponent {}
