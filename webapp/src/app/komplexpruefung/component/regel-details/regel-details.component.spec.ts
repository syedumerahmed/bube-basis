import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { RegelDetailsComponent } from './regel-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { AlertService } from '@/base/service/alert.service';
import { ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { RegelTestComponent } from '@/euregistry/component/regel-test/regel-test.component';
import spyOn = jest.spyOn;
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { RegelDto, RegelRestControllerService } from '@api/komplexpruefung';

describe('RegelDetailsComponent', () => {
  let component: RegelDetailsComponent;
  let fixture: ComponentFixture<RegelDetailsComponent>;
  let routeData$: BehaviorSubject<{
    regel: RegelDto;
  }>;

  const regel: RegelDto = {
    nummer: 1,
    beschreibung: 'beschreibung',
    code: 'code',
    fehlertext: 'fehlertext',
    gruppe: RegelDto.GruppeEnum.GRUPPE_A,
    gueltigBis: 2007,
    gueltigVon: 2099,
    land: '00',
    objekte: [RegelDto.ObjekteEnum.ANLAGE, RegelDto.ObjekteEnum.BST],
    typ: RegelDto.TypEnum.FEHLER,
    typBundeseinheitlich: false,
    letzteAenderung: null,
    typAenderungen: {},
  };

  beforeEach(async () => {
    const route = new ActivatedRoute();
    routeData$ = new BehaviorSubject({
      regel: regel,
    });
    route.data = routeData$.asObservable();

    await TestBed.configureTestingModule({
      declarations: [
        RegelDetailsComponent,
        MockComponents(TooltipComponent, RegelTestComponent),
      ],
      imports: [
        RouterTestingModule,
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProviders(AlertService, RegelRestControllerService),
        {
          provide: ActivatedRoute,
          useValue: route,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be valid and pristine', () => {
    expect(component.form.valid).toBe(true);
    expect(component.form.pristine).toBe(true);
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({
        beschreibung: '',
      });
    });

    it('should be invalid', () => {
      expect(component.form.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.save();
      });

      it('should not call service', inject(
        [RegelRestControllerService],
        (service: RegelRestControllerService) => {
          expect(service.updateRegel).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.form.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedDto: RegelDto = regel;
      let observable: Subject<RegelDto>;
      beforeEach(inject(
        [RegelRestControllerService],
        (service: RegelRestControllerService) => {
          observable = new Subject<RegelDto>();
          spyOn(service, 'updateRegel').mockReturnValue(
            observable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should call service', inject(
        [RegelRestControllerService],
        (service: RegelRestControllerService) => {
          expect(service.updateRegel).toHaveBeenCalledWith(expectedDto);
        }
      ));

      describe('on success', () => {
        let responseDto: RegelDto;

        beforeEach(() => {
          responseDto = {
            ...expectedDto,
            letzteAenderung: new Date().toISOString(),
          };
          observable.next(responseDto);
          observable.complete();
        });

        it('should update euRegBetriebsstaette', () => {
          expect(component.regel).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.form.touched).toBe(false);
          expect(component.form.dirty).toBe(false);
        });
      });
    });
  });
});
