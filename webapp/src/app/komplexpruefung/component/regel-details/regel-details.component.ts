import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { map } from 'rxjs/operators';
import { Validators } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { ClrForm } from '@clr/angular';
import { RegelDto, RegelRestControllerService } from '@api/komplexpruefung';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties, lookupLand } from '@/base/model/Land';
import {
  ClarityIcons,
  floppyIcon,
  shieldCheckIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import ObjekteEnum = RegelDto.ObjekteEnum;
import TypEnum = RegelDto.TypEnum;
import GruppeEnum = RegelDto.GruppeEnum;

interface RegelForm {
  land: string;
  objekte: Array<RegelDto.ObjekteEnum>;
  gruppe: RegelDto.GruppeEnum;
  typ: RegelDto.TypEnum;
  typBundeseinheitlich: boolean;
  beschreibung: string;
  code: string;
  fehlertext: string;
  gueltigVon: number;
  gueltigBis: number;
}

ClarityIcons.addIcons(timesIcon, floppyIcon, shieldCheckIcon);

@Component({
  selector: 'app-regel-details',
  templateUrl: './regel-details.component.html',
  styleUrls: ['./regel-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegelDetailsComponent implements OnInit, UnsavedChanges {
  @ViewChild(ClrForm)
  private clrForm: ClrForm;

  readonly LandProperties = LandProperties;

  regel: RegelDto;

  form: FormGroup<RegelForm>;

  testOk = new BehaviorSubject(false);
  canSave = new BehaviorSubject(false);
  testBenoetigt = false;

  readonly objekteListe: Array<ObjekteEnum> = Object.values(ObjekteEnum);

  readonly objekteDisplayText: Record<ObjekteEnum, string> = {
    BST: 'BST',
    ANLAGE: 'Anlage',
    ANLAGENTEIL: 'Anlagenteil',
    QUELLE: 'Quelle',
    BETREIBER: 'Betreiber',
    EUREG_BST: 'EURegBST',
    EUREG_ANLAGE: 'EURegAnlage',
    EUREG_F: 'EURegF',
    TEILBERICHT: 'Teilbericht',
  };

  readonly regelTypListe: Array<TypEnum> = Object.values(TypEnum);

  readonly regelTypDisplayText: Record<TypEnum, string> = {
    FEHLER: 'Fehler',
    WARNUNG: 'Warnung',
    IGNORIERT: 'Ignoriert',
  };

  readonly regelGruppeListe: Array<GruppeEnum> = Object.values(GruppeEnum);

  readonly regelGruppeDisplayText: Record<GruppeEnum, string> = {
    GRUPPE_A: 'A',
    GRUPPE_B: 'B',
  };

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly alertService: AlertService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly regelService: RegelRestControllerService,
    private readonly fb: FormBuilder,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.route.data.pipe(map((data) => data.regel)).subscribe((regel) => {
      this.regel = regel;
      if (this.isNeu) {
        this.regel.typ = TypEnum.IGNORIERT;
      }
      if (!this.regel) {
        throw Error('regel === ' + this.regel);
      }
      if (!this.form) {
        this.form = this.createForm(this.toForm(this.regel));
      } else {
        // Workaround für Angular-Forms-Fehler bei Neuzuweisung der Form-Instanz mit eigenen ControlValueAccessors
        this.form.reset(this.toForm(this.regel));
        this.form.markAsPristine();
      }
    });
    this.form.valueChanges.subscribe((value) => {
      this.setSavable(value);
    });
    this.form.controls.objekte.valueChanges.subscribe(() => {
      this.testBenoetigt = true;
      this.testOk.next(false);
    });
    this.form.controls.code.valueChanges.subscribe(() => {
      this.testBenoetigt = true;
      this.testOk.next(false);
    });
    this.form.controls.typ.valueChanges.subscribe(() => {
      this.testBenoetigt = true;
    });
  }

  get isGesamtadmin(): boolean {
    return this.benutzerProfil.isGesamtadmin;
  }

  get canWrite(): boolean {
    if (!this.benutzerProfil.canWriteRegeln) {
      return false;
    }

    if (
      !this.isGesamtadmin &&
      this.regel.gruppe == RegelDto.GruppeEnum.GRUPPE_B
    ) {
      return false;
    }

    return !(
      !this.isGesamtadmin && this.regel.land != this.benutzerProfil.landNr
    );
  }

  get isNeu(): boolean {
    return this.regel.nummer == null;
  }

  get displayLand(): string {
    return LandProperties[lookupLand(this.regel.land)].display;
  }

  private toForm(regel: RegelDto): RegelForm {
    return {
      land: regel.land,
      objekte: regel.objekte.slice(),
      gruppe: regel.gruppe,
      typ: regel.typ,
      typBundeseinheitlich: regel.typBundeseinheitlich,
      beschreibung: regel.beschreibung,
      code: regel.code,
      fehlertext: regel.fehlertext,
      gueltigVon: regel.gueltigVon,
      gueltigBis: regel.gueltigBis,
    };
  }

  private createForm(regel: RegelForm): FormGroup<RegelForm> {
    const form = this.fb.group<RegelForm>({
      land: [regel.land],
      objekte: [regel.objekte, [Validators.required]],
      gruppe: [regel.gruppe],
      typ: [regel.typ],
      typBundeseinheitlich: [regel.typBundeseinheitlich],
      beschreibung: [
        regel.beschreibung,
        [Validators.required, Validators.maxLength(2000)],
      ],
      code: [regel.code, [Validators.required, Validators.maxLength(4000)]],
      fehlertext: [
        regel.fehlertext,
        [Validators.required, Validators.maxLength(255)],
      ],
      gueltigVon: [
        regel.gueltigVon,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
      gueltigBis: [
        regel.gueltigBis,
        [Validators.required, Validators.min(1900), Validators.max(9999)],
      ],
    });

    if (!this.canWrite) {
      form.disable();
    }

    return form;
  }

  save(): void {
    if (this.form.invalid) {
      this.clrForm.markAsTouched();
      return;
    }
    const regelDto: RegelDto = this.toDto(this.form.getRawValue());

    let createOrUpdate: Observable<RegelDto>;
    if (this.isNeu) {
      createOrUpdate = this.regelService.createRegel(regelDto);
    } else {
      createOrUpdate = this.regelService.updateRegel(regelDto);
    }
    createOrUpdate.subscribe(
      (response) => {
        if (this.isNeu) {
          this.router.navigate(['../../regeln', response.nummer], {
            relativeTo: this.route,
          });
        }
        this.regel = response;
        this.reset();
        this.alertService.clearAlertMessages();
      },
      (error) => this.alertService.setAlert(error || error.message)
    );
  }

  private toDto(formValue: RegelForm): RegelDto {
    return {
      ...this.regel,
      ...FormUtil.emptyStringsToNull(formValue),
    };
  }

  setFehlerfrei(fehlerfrei: boolean): void {
    this.testOk.next(fehlerfrei);
    this.setSavable(this.form.value);
  }

  setSavable(value: RegelForm): void {
    if (!this.form.pristine) {
      this.canSave.next(
        value.typ === TypEnum.IGNORIERT ||
          !this.testBenoetigt ||
          this.testOk.value ||
          this.kannNichtGetestetWerden(this.form.value.objekte)
      );
    } else {
      this.canSave.next(false);
    }
  }

  private kannNichtGetestetWerden(
    objekte: Array<RegelDto.ObjekteEnum>
  ): boolean {
    return objekte?.includes(ObjekteEnum.TEILBERICHT) && objekte.length === 1;
  }

  reset(): void {
    this.form.reset(this.toForm(this.regel));
    this.form.markAsPristine();
    this.testBenoetigt = false;
    // Falls der Typ nicht IGNORIERT ist, können wir annehmen, dass der Code fehlerfrei ist;
    // dadurch sind Wechsel z.B. von FEHLER nach WARNUNG erlaubt, ohne neu testen zu müssen.
    this.testOk.next(this.regel.typ !== TypEnum.IGNORIERT);
    // Für die Save / Reset Buttons. Brute Force Methode.
    this.changeDetectorRef.markForCheck();
  }

  navigateBackToUebersicht(): void {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  openApiHelp(): void {
    window.open(
      '/assets/regel-api/',
      'BUBE-Regel-API',
      'directories=0,titlebar=0,toolbar=0,location=0,status=0, menubar=0,scrollbars=no,resizable=no,width=1200,height=800'
    );
  }

  canDeactivate(): boolean {
    return this.form.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
