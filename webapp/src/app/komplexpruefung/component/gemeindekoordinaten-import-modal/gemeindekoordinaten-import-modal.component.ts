import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ReferenzDto } from '@api/stammdaten';
import { FormControl } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';

export class ImportDialogCommand {
  public link: string;
  public zieljahr: ReferenzDto;
}

@Component({
  selector: 'app-gemeindekoordinaten-import-modal',
  templateUrl: './gemeindekoordinaten-import-modal.component.html',
  styleUrls: ['./gemeindekoordinaten-import-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GemeindekoordinatenImportModalComponent
  implements OnInit, OnChanges
{
  private downloadLinkAktuellesJahr =
    'https://daten.gdz.bkg.bund.de/produkte/vg/vg250_ebenen_0101/aktuell/vg250_01-01.utm32s.shape.ebenen.zip';

  @Input()
  open: boolean;
  @Input()
  berichtsjahrListe: Array<ReferenzDto>;

  @Output()
  openChange = new EventEmitter<boolean>();
  @Output()
  starteKoordinatenImportEvent = new EventEmitter<ImportDialogCommand>();

  zieljahr: FormControl<ReferenzDto>;
  link: FormControl<string>;

  @ViewChild(ClrForm, { static: true }) clrForm: ClrForm;

  ngOnInit(): void {
    this.zieljahr = new FormControl<ReferenzDto>(null, Validators.required);
    this.link = new FormControl<string>(this.downloadLinkAktuellesJahr, [
      Validators.required,
      BubeValidators.url,
    ]);
  }

  ngOnChanges(): void {
    if (this.berichtsjahrListe) {
      this.zieljahr?.setValue(this.berichtsjahrListe[0]);
    }
  }

  close(): void {
    this.openChange.emit(false);
    this.open = false;
  }

  startImport(): void {
    if (this.zieljahr.invalid) {
      this.clrForm.markAsTouched();
    } else {
      this.starteKoordinatenImportEvent.emit({
        zieljahr: this.zieljahr.value,
        link: this.link.value,
      });
      this.close();
    }
  }
}
