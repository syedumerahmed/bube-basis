import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GemeindekoordinatenImportModalComponent } from './gemeindekoordinaten-import-modal.component';
import { ReferenzDto } from '@api/stammdaten';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';

describe('GemeindekoordinatenImportModalComponent', () => {
  let component: GemeindekoordinatenImportModalComponent;
  let fixture: ComponentFixture<GemeindekoordinatenImportModalComponent>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GemeindekoordinatenImportModalComponent],
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GemeindekoordinatenImportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit', () => {
    const subscriber = jest.fn();
    component.starteKoordinatenImportEvent.subscribe(subscriber);
    component.zieljahr.setValue(currentBerichtsjahr);
    component.link.setValue('test');

    component.startImport();

    expect(subscriber).toHaveBeenCalledWith({
      link: 'test',
      zieljahr: currentBerichtsjahr,
    });
  });
});
