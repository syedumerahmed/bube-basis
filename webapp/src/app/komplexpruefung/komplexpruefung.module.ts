import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KomplexpruefungRoutingModule } from 'src/app/komplexpruefung/routing/komplexpruefung-routing.module';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BaseModule } from '@/base/base.module';
import { KomplexpruefungComponent } from './component/komplexpruefung/komplexpruefung.component';
import { RegellisteComponent } from './component/regelliste/regelliste.component';
import { RegelDetailsComponent } from './component/regel-details/regel-details.component';
import { GemeindekoordinatenImportModalComponent } from './component/gemeindekoordinaten-import-modal/gemeindekoordinaten-import-modal.component';
import { EuregistryModule } from '@/euregistry/euregistry.module';

@NgModule({
  declarations: [
    KomplexpruefungComponent,
    RegellisteComponent,
    RegelDetailsComponent,
    GemeindekoordinatenImportModalComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BaseModule,
    KomplexpruefungRoutingModule,
    EuregistryModule,
  ],
})
export class KomplexpruefungModule {}
