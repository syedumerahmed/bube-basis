import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KommunikationComponent } from '@/basedomain/component/kommunikation/kommunikation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { DatenInAnderesJahrUebernehmenModalComponent } from '@/basedomain/component/daten-in-anderes-jahr-uebenrnehmen-modal/daten-in-anderes-jahr-uebernehmen-modal.component';
import { VorschriftenComponent } from '@/basedomain/component/vorschriften/vorschriften.component';
import { LeistungenComponent } from '@/basedomain/component/leistungen/leistungen.component';
import { BaseModule } from '@/base/base.module';
import { AdresseComponent } from '@/basedomain/component/adresse/adresse.component';
import { GeoPunktComponent } from '@/basedomain/component/geo-punkt/geo-punkt.component';
import { BerichtsjahrComponent } from '@/basedomain/component/berichtsjahr/berichtsjahr.component';

@NgModule({
  declarations: [
    KommunikationComponent,
    ReferenzSelectComponent,
    DatenInAnderesJahrUebernehmenModalComponent,
    VorschriftenComponent,
    LeistungenComponent,
    AdresseComponent,
    GeoPunktComponent,
    BerichtsjahrComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    BaseModule,
  ],
  exports: [
    KommunikationComponent,
    ReferenzSelectComponent,
    DatenInAnderesJahrUebernehmenModalComponent,
    VorschriftenComponent,
    LeistungenComponent,
    AdresseComponent,
    GeoPunktComponent,
    BerichtsjahrComponent,
  ],
})
export class BasedomainModule {}
