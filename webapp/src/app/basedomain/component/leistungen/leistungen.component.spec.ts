import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  LeistungenComponent,
  LeistungenFormData,
} from './leistungen.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@/base/forms';
import { MockModule, MockProviders } from 'ng-mocks';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { LeistungsklasseEnum, ReferenzDto } from '@api/stammdaten';

describe('LeistungenComponent', () => {
  let component: LeistungenComponent;
  let fixture: ComponentFixture<LeistungenComponent>;

  const einheitenListe = [
    { schluessel: '1', ktext: 'm' },
    { schluessel: '2', ktext: 'km' },
  ] as ReferenzDto[];

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
      declarations: [LeistungenComponent],
      providers: [
        MockProviders(BerichtsjahrService),
        {
          provide: ActivatedRoute,
          useValue: { parent: { snapshot: { data: { einheitenListe } } } },
        },
      ],
    }).compileComponents();
    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(LeistungenComponent);
      component = fixture.componentInstance;
      component.formArray = LeistungenComponent.createForm(
        fb,
        LeistungenComponent.toForm([]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(LeistungenComponent);
      component = fixture.componentInstance;
      component.formArray = LeistungenComponent.createForm(
        fb,
        LeistungenComponent.toForm([
          {
            leistung: 1,
            leistungseinheit: einheitenListe[0],
            bezug: 'bezug1',
            leistungsklasse: LeistungsklasseEnum.INSTALLIERT,
          },
          {
            leistung: 2,
            leistungseinheit: einheitenListe[1],
            bezug: 'bezug2',
          },
        ]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.leistungenForm).toBeTruthy();
        expect(component.leistungenForm.value).toStrictEqual({
          leistung: null,
          einheit: null,
          bezug: null,
          klasse: null,
        });
        expect(component.leistungenForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let patchedValue: LeistungenFormData;
        beforeEach(() => {
          patchedValue = {
            leistung: 1,
            einheit: einheitenListe[0],
            klasse: LeistungsklasseEnum.INSTALLIERT,
            bezug: 'testbezug',
          };
          component.leistungenForm.patchValue(patchedValue);
        });
        it('should be valid', () => {
          expect(component.leistungenForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null leistungenForm', () => {
            expect(component.leistungenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null leistungenForm', () => {
            expect(component.leistungenForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.leistungenForm).toBeTruthy();
        expect(component.leistungenForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.leistungenForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: LeistungenFormData;
        let patchedValue: LeistungenFormData;
        beforeEach(() => {
          originalValue = component.leistungenForm.value;
          patchedValue = {
            leistung: 1,
            einheit: einheitenListe[0],
            klasse: LeistungsklasseEnum.INSTALLIERT,
            bezug: 'testbezug',
          };
          component.leistungenForm.patchValue(patchedValue);
          component.leistungenForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.leistungenForm.patchValue(patchedValue);
            component.leistungenForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null leistungenForm', () => {
            expect(component.leistungenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();
            expect(component.formArray.dirty).toBe(true);
          });

          it('should null leistungenForm', () => {
            expect(component.leistungenForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: LeistungenFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
        expect(component.formArray.pristine).toBe(false);
      });
    });
  });
});
