import { AfterViewChecked, Component, Input, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { LeistungDto, LeistungsklasseEnum, ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface LeistungenFormData {
  leistung: number;
  einheit: ReferenzDto;
  bezug: string;
  klasse: LeistungsklasseEnum;
}

ClarityIcons.addIcons(trashIcon, noteIcon, plusIcon, errorStandardIcon);
@Component({
  selector: 'app-leistungen',
  templateUrl: './leistungen.component.html',
  styleUrls: ['./leistungen.component.scss'],
})
export class LeistungenComponent implements AfterViewChecked {
  constructor(
    private fb: FormBuilder,
    private berichtsjahrService: BerichtsjahrService
  ) {}

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  readonly leistungsklasseDisplayText: Record<LeistungsklasseEnum, string> = {
    INSTALLIERT: 'Installiert',
    BETRIEBEN: 'Betrieben',
    GENEHMIGT: 'Genehmigt',
  };

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  leistungenForm: FormGroup<LeistungenFormData>;

  @Input()
  formArray: FormArray<LeistungenFormData>;
  @Input()
  einheitenListe: Referenzliste<Required<ReferenzDto>>;
  leistungsklasseListe: Array<LeistungsklasseEnum> =
    Object.values(LeistungsklasseEnum);

  editModalOpen = false;

  editExistingIndex: number = null;

  static toForm(leistungen: Array<LeistungDto>): Array<LeistungenFormData> {
    return (leistungen ?? []).map((leistung) => ({
      leistung: leistung.leistung,
      einheit: leistung.leistungseinheit,
      bezug: leistung.bezug,
      klasse: leistung.leistungsklasse,
    }));
  }

  static createForm(
    fb: FormBuilder,
    leistungen: Array<LeistungenFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<LeistungenFormData> {
    return fb.array<LeistungenFormData>(
      leistungen.map((leistung) =>
        fb.group({
          leistung: [
            leistung.leistung,
            [
              Validators.required,
              BubeValidators.maxDecimal(15, 3),
              Validators.min(0.001),
            ],
          ],
          einheit: [
            leistung.einheit,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
          bezug: [leistung.bezug, Validators.maxLength(100)],
          klasse: [
            leistung.klasse,
            [
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
        })
      )
    );
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  private createEmptyForm(): FormGroup<LeistungenFormData> {
    return this.fb.group({
      leistung: [
        null,
        [
          Validators.required,
          BubeValidators.maxDecimal(15, 3),
          Validators.min(0.001),
        ],
      ],
      einheit: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      bezug: [null, Validators.maxLength(100)],
      klasse: [
        null,
        BubeValidators.isReferenceValid(
          Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
        ),
      ],
    });
  }

  private openEdit(formGroup: FormGroup<LeistungenFormData>): void {
    this.leistungenForm = formGroup;
    this.editModalOpen = true;
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.leistungenForm = null;
  }

  uebernehmen(): void {
    if (this.leistungenForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.leistungenForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.leistungenForm)) {
        this.formArray.push(this.leistungenForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.leistungenForm = null;
    }
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }
}
