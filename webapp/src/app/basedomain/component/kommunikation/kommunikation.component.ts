import { AfterViewChecked, Component, Input, ViewChild } from '@angular/core';
import { ValidatorFn, Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { KommunikationsTypenSteuerung } from '@/basedomain/component/kommunikation/KommunikationsTypenSteuerung';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { KommunikationsverbindungDto, ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface KommunikationsverbindungFormData {
  typ: Required<ReferenzDto>;
  verbindung: string;
}

const commonKommunikationsverbindungValidators: ValidatorFn[] = [
  Validators.required,
  Validators.maxLength(500),
];
const specificKommunikationsverbindungValidatoren: Map<
  KommunikationsTypenSteuerung,
  ValidatorFn
> = new Map([
  [KommunikationsTypenSteuerung.MAIL, BubeValidators.email],
  [KommunikationsTypenSteuerung.URL, BubeValidators.url],
]);

export function kommunikationsverbindungValidator(
  typSteuerung: ReferenzDto['strg']
): ValidatorFn[] {
  return [
    ...commonKommunikationsverbindungValidators,
    specificKommunikationsverbindungValidatoren.get(
      typSteuerung as KommunikationsTypenSteuerung
    ) ?? Validators.nullValidator,
  ];
}

ClarityIcons.addIcons(trashIcon, noteIcon, plusIcon, errorStandardIcon);

@Component({
  selector: 'app-kommunikation',
  templateUrl: './kommunikation.component.html',
  styleUrls: ['./kommunikation.component.scss'],
})
export class KommunikationComponent implements AfterViewChecked {
  constructor(
    private fb: FormBuilder,
    private berichtsjahrservice: BerichtsjahrService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  kommunikationForm: FormGroup<KommunikationsverbindungFormData>;

  @Input()
  formArray: FormArray<KommunikationsverbindungFormData>;

  @Input()
  kommunikationsTypen: Referenzliste<Required<ReferenzDto>>;

  editModalOpen = false;

  editExistingIndex: number = null;

  static toForm(
    kommunikationsverbindungen: Array<KommunikationsverbindungDto>,
    kommunikationsTypen: Referenzliste<Required<ReferenzDto>>
  ): Array<KommunikationsverbindungFormData> {
    return (kommunikationsverbindungen ?? []).map((komm) => ({
      verbindung: komm.verbindung,
      typ: kommunikationsTypen.find(komm.typ as Required<ReferenzDto>),
    }));
  }

  static createForm(
    fb: FormBuilder,
    kommunikationsverbindungen: Array<KommunikationsverbindungFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<KommunikationsverbindungFormData> {
    return fb.array<KommunikationsverbindungFormData>(
      kommunikationsverbindungen.map((komm) => {
        return fb.group({
          verbindung: [
            komm.verbindung,
            kommunikationsverbindungValidator(komm.typ.strg),
          ],
          typ: [
            komm.typ,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
        });
      })
    );
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.kommunikationForm = null;
  }

  uebernehmen(): void {
    if (this.kommunikationForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.kommunikationForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.kommunikationForm)) {
        this.formArray.push(this.kommunikationForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.kommunikationForm = null;
    }
  }

  kommunikationsTypChanged(): void {
    const verbindung = this.kommunikationForm.controls.verbindung;
    const steuerung = this.kommunikationForm.value.typ?.strg;
    verbindung.setValidators(kommunikationsverbindungValidator(steuerung));
    verbindung.updateValueAndValidity();
  }

  private createEmptyForm(): FormGroup<KommunikationsverbindungFormData> {
    return this.fb.group({
      typ: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrservice.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      verbindung: [null, commonKommunikationsverbindungValidators],
    });
  }

  private openEdit(
    formGroup: FormGroup<KommunikationsverbindungFormData>
  ): void {
    this.kommunikationForm = formGroup;
    this.kommunikationsTypChanged();
    this.editModalOpen = true;
  }
}
