import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  VorschriftenComponent,
  VorschriftFormData,
} from './vorschriften.component';
import { MockModule, MockProvider } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormBuilder } from '@/base/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { ReferenzDto } from '@api/stammdaten';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

describe('VorschriftenComponent', () => {
  let component: VorschriftenComponent;
  let fixture: ComponentFixture<VorschriftenComponent>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const vorschriftenListe = [
    {
      id: 1,
      schluessel: '01',
      strg: 'R4BV',
      ktext: 'r4bv vorschrift',
    } as ReferenzDto,
    {
      id: 2,
      schluessel: '02',
      strg: 'RPRTR',
      ktext: 'rprtr vorschrift',
    } as ReferenzDto,
    { id: 3, schluessel: 'anykey', ktext: 'any vorschrift' } as ReferenzDto,
  ] as Required<ReferenzDto>[];

  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      declarations: [
        VorschriftenComponent,
        EllipsizePipe,
        ReferenzSelectComponent,
      ],
      providers: [
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        MockProvider(BenutzerProfil),
      ],
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
    }).compileComponents();
  });

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(VorschriftenComponent);
      component = fixture.componentInstance;
      component.vorschriften = new Referenzliste<Required<ReferenzDto>>(
        vorschriftenListe
      );
      component.formArray = VorschriftenComponent.vorschriftenCreateForm(
        fb,
        [],
        currentBerichtsjahr
      );
      fixture.detectChanges();
    }));
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu taetigkeiten possible', () => {
      beforeEach(() => {
        component.neueVorschrift({
          schluessel: '02',
          strg: 'R4BV',
        } as ReferenzDto);
        component.prtrTaetigkeiten = new Referenzliste([
          { id: 1, schluessel: 'a', ktext: 'a taet' } as ReferenzDto,
          { id: 2, schluessel: 'b', ktext: 'b taet' } as ReferenzDto,
        ] as Required<ReferenzDto>[]);
      });

      it('should define empty form with selected vorschrift', () => {
        expect(component.vorschriftenForm).toBeTruthy();
        expect(component.vorschriftenForm.value).toStrictEqual({
          art: { schluessel: '02', strg: 'R4BV' } as ReferenzDto,
          haupttaetigkeit: null,
          taetigkeiten: [],
        });
      });

      it('should be valid', () => {
        expect(component.vorschriftenForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form with taetigkeit', () => {
        let patchedValue: VorschriftFormData;
        beforeEach(() => {
          patchedValue = {
            art: { schluessel: '02' } as ReferenzDto,
            haupttaetigkeit: null,
            taetigkeiten: [{ schluessel: 'a' } as ReferenzDto],
          };
          component.vorschriftenForm.patchValue(patchedValue, {
            emitEvent: true,
          });
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null leistungenForm', () => {
            expect(component.vorschriftenForm).toBeNull();
          });
        });
      });

      describe('fill form with taetigkeiten and haupttaetigkeit', () => {
        let patchedValue: VorschriftFormData;
        beforeEach(() => {
          patchedValue = {
            art: { schluessel: '02' } as ReferenzDto,
            haupttaetigkeit: { schluessel: 'a' } as ReferenzDto,
            taetigkeiten: [{ schluessel: 'a' } as ReferenzDto],
          };
          component.vorschriftenForm.patchValue(patchedValue);
        });

        it('should be valid', () => {
          expect(component.vorschriftenForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null leistungenForm', () => {
            expect(component.vorschriftenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null leistungenForm', () => {
            expect(component.vorschriftenForm).toBeNull();
          });
        });
      });
    });

    describe('neu taetigkeiten not possible', () => {
      beforeEach(() => {
        component.neueVorschrift({ schluessel: 'anykey' } as ReferenzDto);
      });

      it('should immediately push vorschrift to array', () => {
        expect(component.vorschriftenForm).toBeFalsy();
        expect(component.editModalOpen).toBe(false);
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(false);
        expect(component.formArray.value).toContainEqual({
          art: { schluessel: 'anykey' },
          haupttaetigkeit: null,
          taetigkeiten: [],
        });
      });
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(VorschriftenComponent);
      component = fixture.componentInstance;
      component.nummer4BImSchVTaetigkeiten = new Referenzliste([
        { id: 1, schluessel: 'ab' } as ReferenzDto,
        { id: 2, schluessel: 'cd' } as ReferenzDto,
      ] as Required<ReferenzDto>[]);
      component.prtrTaetigkeiten = new Referenzliste([]);
      component.iedTaetigkeiten = new Referenzliste([]);
      component.vorschriften = new Referenzliste([
        {
          id: 1,
          schluessel: '01',
          strg: 'R4BV',
          ktext: 'r4bv vorschrift',
        } as ReferenzDto,
        {
          id: 2,
          schluessel: '02',
          strg: 'RPRTR',
          ktext: 'rprtr vorschrift',
        } as ReferenzDto,
        {
          id: 3,
          schluessel: 'anykey',
          strg: null,
          ktext: 'any vorschrift',
        } as ReferenzDto,
      ] as Required<ReferenzDto>[]);
      component.formArray = VorschriftenComponent.vorschriftenCreateForm(
        fb,
        [
          {
            art: { schluessel: '01', strg: 'R4BV' } as ReferenzDto,
            haupttaetigkeit: { schluessel: 'ab' } as ReferenzDto,
            taetigkeiten: [
              { schluessel: 'ab' } as ReferenzDto,
              { schluessel: 'cd' } as ReferenzDto,
            ],
          },
          {
            art: { schluessel: '02' } as ReferenzDto,
            haupttaetigkeit: null,
            taetigkeiten: null,
          },
          {
            art: { schluessel: 'anykey' } as ReferenzDto,
            haupttaetigkeit: null,
            taetigkeiten: null,
          },
        ],
        currentBerichtsjahr
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('bearbeiten on editable vorschrift', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.vorschriftenForm).toBeTruthy();
        expect(component.vorschriftenForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.vorschriftenForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: VorschriftFormData;
        let patchedValue: VorschriftFormData;
        beforeEach(() => {
          originalValue = component.vorschriftenForm.value;
          patchedValue = {
            art: { schluessel: '01' } as ReferenzDto,
            haupttaetigkeit: { schluessel: 'ab' } as ReferenzDto,
            taetigkeiten: [{ schluessel: 'ab' } as ReferenzDto],
          };
          component.vorschriftenForm.patchValue(patchedValue);
          component.vorschriftenForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null vorschriftenForm', () => {
            expect(component.vorschriftenForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();
            expect(component.formArray.dirty).toBe(true);
          });

          it('should null vorschriftenForm', () => {
            expect(component.vorschriftenForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten on non editable vorschrift', () => {
      beforeEach(() => {
        component.bearbeiten(2);
      });
      it('should do nothing', () => {
        expect(component.vorschriftenForm).toBeFalsy();
        expect(component.editModalOpen).toBe(false);
        expect(component.editExistingIndex).toBeFalsy();
      });
    });

    describe('loeschen', () => {
      let elementToDelete: VorschriftFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
        expect(component.formArray.pristine).toBe(false);
      });
    });
  });
});
