import { AfterViewChecked, Component, Input, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto, VorschriftDto } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import {
  angleIcon,
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface VorschriftFormData {
  art: ReferenzDto;
  haupttaetigkeit: ReferenzDto;
  taetigkeiten: Array<ReferenzDto>;
}

ClarityIcons.addIcons(
  trashIcon,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  angleIcon
);
@Component({
  selector: 'app-vorschriften',
  templateUrl: './vorschriften.component.html',
  styleUrls: ['./vorschriften.component.scss'],
})
export class VorschriftenComponent implements AfterViewChecked {
  constructor(
    private readonly fb: FormBuilder,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly benutzer: BenutzerProfil
  ) {}

  vorschriftenForm: FormGroup<VorschriftFormData>;
  editModalOpen = false;
  editExistingIndex: number = null;

  @ViewChild(ClrForm)
  clrForm: ClrForm;
  @Input()
  formArray: FormArray<VorschriftFormData>;
  @Input()
  vorschriften: Referenzliste<Required<ReferenzDto>>;
  @Input()
  nummer4BImSchVTaetigkeiten: Referenzliste<Required<ReferenzDto>>;
  @Input()
  prtrTaetigkeiten: Referenzliste<Required<ReferenzDto>>;
  @Input()
  iedTaetigkeiten: Referenzliste<Required<ReferenzDto>>;

  neueVorschriften: Array<ReferenzDto>;

  static vorschriftenToForm(
    vorschriften: Array<VorschriftDto>,
    nummer4BImSchVTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>,
    prtrTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>,
    iedTaetigkeitenListe: Referenzliste<Required<ReferenzDto>>
  ): Array<VorschriftFormData> {
    return (vorschriften ?? []).map((vDto) => {
      let taetigkeitenListe;
      switch (vDto.art.strg) {
        case 'R4BV':
          taetigkeitenListe = nummer4BImSchVTaetigkeitenListe;
          break;
        case 'RPRTR':
          taetigkeitenListe = prtrTaetigkeitenListe;
          break;
        case 'RIET':
          taetigkeitenListe = iedTaetigkeitenListe;
          break;
      }
      return {
        art: vDto.art,
        taetigkeiten: taetigkeitenListe
          ? taetigkeitenListe.map(vDto.taetigkeiten)
          : [],
        haupttaetigkeit: vDto.haupttaetigkeit,
      };
    });
  }

  static vorschriftenCreateForm(
    fb: FormBuilder,
    vorschriften: Array<VorschriftFormData>,
    selectedBerichtsjahr: ReferenzDto
  ): FormArray<VorschriftFormData> {
    const vorschriftenForm = [];
    vorschriften.forEach((vorschrift) => {
      vorschriftenForm.push(
        fb.group<VorschriftFormData>({
          art: vorschrift.art,
          taetigkeiten: [
            vorschrift.taetigkeiten ?? [],
            [
              BubeValidators.areReferencesValid(
                Number(selectedBerichtsjahr.schluessel)
              ),
            ],
          ],
          haupttaetigkeit: vorschrift.haupttaetigkeit,
        })
      );
    });
    return fb.array<VorschriftFormData>(vorschriftenForm);
  }

  static formToVorschriften(
    vorschriftenForm: Array<VorschriftFormData>
  ): Array<VorschriftDto> {
    return vorschriftenForm.map((vorschrift) => {
      const vorschriftDto: VorschriftDto = {
        art: vorschrift.art,
        haupttaetigkeit: vorschrift.haupttaetigkeit,
        taetigkeiten: vorschrift.taetigkeiten,
      };
      return vorschriftDto;
    });
  }

  ngAfterViewChecked(): void {
    this.updateNeueVorschriftenliste();

    this.clrForm?.markAsTouched();
  }

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  get canDelete(): boolean {
    return this.canWrite && this.benutzer.canDeleteStammdaten;
  }

  bearbeiten(index: number): void {
    if (this.canHaveTaetigkeiten(this.formArray.at(index).value)) {
      this.editExistingIndex = index;
      const editableForm = this.createEmptyForm();
      const value = this.formArray.at(index).value;
      // Damit nicht die Referenz des Tätigkeiten-Array in das neue Form-objekt geschrieben wird, muss das Tätigkeiten-Array geklont werden
      editableForm.setValue({
        ...value,
        taetigkeiten: value.taetigkeiten?.slice() || [],
      });
      this.openEdit(editableForm);
    }
  }

  loeschen(index: number): void {
    this.updateNeueVorschriftenliste();

    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  private openEdit(formGroup: FormGroup<VorschriftFormData>): void {
    this.vorschriftenForm = formGroup;
    this.editModalOpen = true;
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.vorschriftenForm = null;
  }

  uebernehmen(): void {
    if (this.vorschriftenForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.vorschriftenForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.vorschriftenForm)) {
        this.formArray.push(this.vorschriftenForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.vorschriftenForm = null;
    }
  }

  private createEmptyForm(): FormGroup<VorschriftFormData> {
    return this.fb.group({
      art: [
        null,
        BubeValidators.isReferenceValid(
          Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
        ),
      ],
      haupttaetigkeit: [
        null,
        BubeValidators.isReferenceValid(
          Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
        ),
      ],
      taetigkeiten: [
        [],
        BubeValidators.areReferencesValid(
          Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
        ),
      ],
    });
  }

  onTaetigkeitenChanged(): void {
    if (
      !this.hasTaetigkeiten(this.vorschriftenForm.value) ||
      !this.vorschriftenForm.value.taetigkeiten.includes(
        this.vorschriftenForm.value.haupttaetigkeit
      )
    ) {
      this.vorschriftenForm.get('haupttaetigkeit').patchValue(null);
      this.vorschriftenForm.get('haupttaetigkeit').updateValueAndValidity();
    }
  }

  possibleTaetigkeiten(vorschrift: VorschriftFormData): ReferenzDto[] {
    switch (vorschrift.art.strg) {
      case 'R4BV':
        return [...this.nummer4BImSchVTaetigkeiten.values];
      case 'RPRTR':
        return [...this.prtrTaetigkeiten.values];
      case 'RIET':
        return [...this.iedTaetigkeiten.values];
      default:
        return [];
    }
  }

  canHaveTaetigkeiten(vorschrift: VorschriftFormData): boolean {
    return ['R4BV', 'RPRTR', 'RIET'].includes(vorschrift.art.strg);
  }

  hasTaetigkeiten(vorschrift: VorschriftFormData): boolean {
    return vorschrift.taetigkeiten?.length > 0;
  }

  neueVorschrift(vorschrift: ReferenzDto): void {
    this.updateNeueVorschriftenliste();

    const editableForm = this.createEmptyForm();
    editableForm.get('art').setValue(vorschrift);
    if (this.canHaveTaetigkeiten(editableForm.value)) {
      this.openEdit(editableForm);
    } else {
      this.formArray.push(editableForm);
      this.formArray.markAsDirty();
    }
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }

  updateNeueVorschriftenliste(): void {
    this.neueVorschriften = this.vorschriften.values.filter(
      (vorschrift) =>
        !this.formArray.value.find(
          (v) => v.art.schluessel === vorschrift.schluessel
        )
    );
  }
}
