import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { descend, prop, sort } from 'ramda';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { map, takeUntil } from 'rxjs/operators';
import { ReferenzDto } from '@api/stammdaten';
import { angleIcon, ClarityIcons } from '@cds/core/icon';

ClarityIcons.addIcons(angleIcon);

@Component({
  selector: 'app-berichtsjahr',
  templateUrl: './berichtsjahr.component.html',
  styleUrls: ['./berichtsjahr.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BerichtsjahrComponent implements OnDestroy {
  berichtsjahre$: BehaviorSubject<Array<ReferenzDto>> = new BehaviorSubject([]);
  berichtsjahr$: Observable<ReferenzDto>;
  isEnabled$: Observable<boolean>;

  private readonly destroyed$ = new Subject();

  constructor(private readonly berichtsjahrService: BerichtsjahrService) {
    this.berichtsjahrService.filteredBerichtsJahre$
      .pipe(
        takeUntil(this.destroyed$),
        map(sort<ReferenzDto>(descend(prop('sortier'))))
      )
      .subscribe((jahre) => {
        this.berichtsjahre$.next(jahre);
      });
    this.berichtsjahr$ = this.berichtsjahrService.selectedBerichtsjahr$;
    this.isEnabled$ = this.berichtsjahrService.berichtsjahrEnabled$;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  selectBerichtsjahr(jahr: ReferenzDto): void {
    this.berichtsjahrService.setBerichtsjahr(jahr);
  }
}
