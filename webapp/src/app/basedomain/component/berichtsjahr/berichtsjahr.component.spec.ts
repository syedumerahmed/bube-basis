import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BerichtsjahrComponent } from './berichtsjahr.component';
import { ClarityModule } from '@clr/angular';
import { MockModule, MockProviders } from 'ng-mocks';
import { BehaviorSubject, Subject } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import spyOn = jest.spyOn;

describe('BerichtsjahrComponent', () => {
  let berichtsjahrEnabled$: Subject<boolean>;
  let selectedBerichtsjahr$: Subject<ReferenzDto>;
  let alleBerichtsJahre$: Subject<Array<ReferenzDto>>;

  let component: BerichtsjahrComponent;
  let fixture: ComponentFixture<BerichtsjahrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BerichtsjahrComponent],
      imports: [MockModule(ClarityModule)],
      providers: [MockProviders(BerichtsjahrService)],
    }).compileComponents();
  });

  beforeEach(inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      berichtsjahrEnabled$ = new Subject();
      selectedBerichtsjahr$ = new BehaviorSubject({
        schluessel: '2018',
      } as ReferenzDto);
      alleBerichtsJahre$ = new BehaviorSubject([
        {
          schluessel: '2018',
        } as ReferenzDto,
      ]);
      spyOn(berichtsjahrService, 'berichtsjahrEnabled$', 'get').mockReturnValue(
        berichtsjahrEnabled$
      );
      spyOn(
        berichtsjahrService,
        'selectedBerichtsjahr$',
        'get'
      ).mockReturnValue(selectedBerichtsjahr$);
      spyOn(
        berichtsjahrService,
        'filteredBerichtsJahre$',
        'get'
      ).mockReturnValue(alleBerichtsJahre$);
      fixture = TestBed.createComponent(BerichtsjahrComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }
  ));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('enabled', () => {
    let isEnabledSubscriber;
    beforeEach(() => {
      isEnabledSubscriber = jest.fn();
      component.isEnabled$.subscribe(isEnabledSubscriber);
    });

    it('should be disabled when service emits event', () => {
      berichtsjahrEnabled$.next(false);
      expect(isEnabledSubscriber).toHaveBeenCalledWith(false);
    });

    it('should be enabled when service emits event', () => {
      berichtsjahrEnabled$.next(true);
      expect(isEnabledSubscriber).toHaveBeenCalledWith(true);
    });
  });

  it('should set berichtsjahr in component', inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      const jahr: ReferenzDto = { schluessel: '2019' } as ReferenzDto;
      component.selectBerichtsjahr(jahr);
      expect(berichtsjahrService.setBerichtsjahr).toHaveBeenCalledWith(jahr);
    }
  ));
});
