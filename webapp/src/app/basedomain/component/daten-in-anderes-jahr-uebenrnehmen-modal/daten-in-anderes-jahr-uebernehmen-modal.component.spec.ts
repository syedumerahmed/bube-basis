import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenInAnderesJahrUebernehmenModalComponent } from './daten-in-anderes-jahr-uebernehmen-modal.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { MockModule, MockProviders } from 'ng-mocks';
import { BehaviorSubject } from 'rxjs';
import { ReferenzDto } from '@api/stammdaten';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';

describe('DatenInAnderesJahrUebernehmenModalComponent', () => {
  let component: DatenInAnderesJahrUebernehmenModalComponent;
  let fixture: ComponentFixture<DatenInAnderesJahrUebernehmenModalComponent>;
  let selectedBerichtsjahr$: BehaviorSubject<ReferenzDto>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };
  const oldBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2019',
    sortier: '19',
    id: 19,
  };
  const newBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2021',
    sortier: '21',
    id: 21,
  };

  const berichtsjahrliste = [
    oldBerichtsjahr,
    newBerichtsjahr,
    currentBerichtsjahr,
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DatenInAnderesJahrUebernehmenModalComponent],
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
      providers: [MockProviders(BerichtsjahrService)],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    selectedBerichtsjahr$ = new BehaviorSubject(currentBerichtsjahr);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr$', 'get')
      .mockReturnValue(selectedBerichtsjahr$);

    fixture = TestBed.createComponent(
      DatenInAnderesJahrUebernehmenModalComponent
    );
    component = fixture.componentInstance;

    component.berichtsjahrListe = berichtsjahrliste;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be fitlered and sorted', () => {
    expect(component.berichtsjahrListeFilteredAndSorted).toEqual([
      newBerichtsjahr,
      oldBerichtsjahr,
    ]);
  });

  it('should emit', () => {
    const subscriber = jest.fn();
    component.berichtsJahrSelected.subscribe(subscriber);

    component.zieljahr.setValue(newBerichtsjahr);
    component.selectBerichtsJahr();

    expect(subscriber).toHaveBeenCalledWith(newBerichtsjahr);
  });
});
