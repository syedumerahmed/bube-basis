import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ClrForm } from '@clr/angular';
import { FormControl } from '@/base/forms';
import { ReferenzDto } from '@api/stammdaten';
import { Validators } from '@angular/forms';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { Subscription } from 'rxjs';
import { descend, prop, sort } from 'ramda';

@Component({
  selector: 'app-daten-in-anderes-jahr-uebernehmen-modal',
  templateUrl: './daten-in-anderes-jahr-uebernehmen-modal.component.html',
  styleUrls: ['./daten-in-anderes-jahr-uebernehmen-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatenInAnderesJahrUebernehmenModalComponent
  implements OnInit, OnDestroy, OnChanges
{
  @Input()
  open: boolean;
  @Input()
  berichtsjahrListe: Array<ReferenzDto>;
  @Input()
  zuUebernehmendeDaten: string;

  berichtsjahrListeFilteredAndSorted: Array<ReferenzDto>;

  @Output()
  openChange = new EventEmitter<boolean>();
  @Output()
  berichtsJahrSelected = new EventEmitter<ReferenzDto>();

  subscription: Subscription;
  zieljahr: FormControl<ReferenzDto>;

  @ViewChild(ClrForm, { static: true }) clrForm: ClrForm;

  constructor(private berichtsjahrService: BerichtsjahrService) {}

  ngOnInit(): void {
    this.zieljahr = new FormControl<ReferenzDto>(null, Validators.required);
    this.subscription =
      this.berichtsjahrService.selectedBerichtsjahr$.subscribe(
        (selectedBerichtsjahr) =>
          this.updateBerichtsjahrSelect(selectedBerichtsjahr)
      );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  closeUebernehmenDialog(): void {
    this.openChange.emit(false);
    this.open = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.open) {
      // ist benötigt um das Eingabefeld zu leeren nachdem die Subscription die Jahresliste aktualisiert hat.
      setTimeout(() => this.zieljahr.reset(), 0);
    }
  }

  selectBerichtsJahr(): void {
    if (this.zieljahr.invalid) {
      this.clrForm.markAsTouched();
    } else {
      this.berichtsJahrSelected.emit(this.zieljahr.value);
      this.closeUebernehmenDialog();
    }
  }

  private updateBerichtsjahrSelect(berichtsjahr: ReferenzDto): void {
    this.berichtsjahrListeFilteredAndSorted = sort<ReferenzDto>(
      descend(prop('sortier')),
      this.berichtsjahrListe
    );
    const aktuell = this.berichtsjahrListeFilteredAndSorted.find(
      (ref) => ref.id === berichtsjahr.id
    );
    this.berichtsjahrListeFilteredAndSorted.splice(
      this.berichtsjahrListeFilteredAndSorted.indexOf(aktuell, 0),
      1
    );
    this.zieljahr.reset();
  }
}
