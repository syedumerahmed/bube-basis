import { Component, Input, OnInit } from '@angular/core';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { GeoPunktDto, ReferenzDto } from '@api/stammdaten';
import { FormBuilder, FormGroup } from '@/base/forms';
import { ValidatorFn, Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { distinctUntilChanged } from 'rxjs/operators';

export interface GeoPunktFormData {
  nord: number;
  ost: number;
  epsgCode: ReferenzDto;
}

function geoPunktValidators(required: boolean): ValidatorFn[] {
  return [
    Validators.min(0),
    Validators.max(9_999_999_999),
    required ? Validators.required : Validators.nullValidator,
  ];
}

@Component({
  selector: 'app-geo-punkt',
  templateUrl: './geo-punkt.component.html',
  styleUrls: ['./geo-punkt.component.scss'],
})
export class GeoPunktComponent implements OnInit {
  @Input()
  form: FormGroup<GeoPunktFormData>;
  @Input()
  epsgCodeListe: Referenzliste<Required<ReferenzDto>>;

  static toForm(geoPunktDto: GeoPunktDto): GeoPunktFormData {
    return {
      nord: geoPunktDto?.nord,
      ost: geoPunktDto?.ost,
      epsgCode: geoPunktDto?.epsgCode,
    };
  }

  static createForm(
    fb: FormBuilder,
    data: GeoPunktFormData,
    selectedYear: ReferenzDto
  ): FormGroup<GeoPunktFormData> {
    return fb.group({
      ost: [data.ost, geoPunktValidators(data.epsgCode != null)],
      nord: [data.nord, geoPunktValidators(data.epsgCode != null)],
      epsgCode: [
        data.epsgCode,
        BubeValidators.isReferenceValid(Number(selectedYear.schluessel)),
      ],
    });
  }

  ngOnInit(): void {
    this.form.controls.epsgCode.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.form.controls.ost.setValidators(geoPunktValidators(value != null));
        this.form.controls.nord.setValidators(
          geoPunktValidators(value != null)
        );
        this.form.controls.ost.updateValueAndValidity();
        this.form.controls.nord.updateValueAndValidity();
        this.form.updateValueAndValidity();
      });
  }
}
