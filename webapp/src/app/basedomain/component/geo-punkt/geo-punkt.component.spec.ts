import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { GeoPunktComponent } from './geo-punkt.component';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { FormBuilder } from '@/base/forms';
import { ReferenzDto } from '@api/referenzdaten';

describe('GeoPunktComponent', () => {
  let component: GeoPunktComponent;
  let fixture: ComponentFixture<GeoPunktComponent>;

  const geoPunktFormData = {
    epsgCode: { schluessel: 'ETRS89/UTM Zone 32N' } as ReferenzDto,
    nord: 123,
    ost: 456,
  };
  const selectedYear: ReferenzDto = {
    schluessel: '2020',
  } as ReferenzDto;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GeoPunktComponent],
      imports: [
        RouterTestingModule,
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(BasedomainModule),
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(GeoPunktComponent);
    component = fixture.componentInstance;
    component.epsgCodeListe = new Referenzliste([]);
    component.form = GeoPunktComponent.createForm(
      formBuilder,
      geoPunktFormData,
      selectedYear
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be valid if empty', () => {
    expect(component.form.valid).toBe(true);
  });

  it('should be valid if epsgCode is not set', () => {
    component.form.setValue({ epsgCode: null, ost: 0, nord: 0 });
    expect(component.form.valid).toBe(true);
  });

  it('should be valid if complete', () => {
    component.form.setValue({ epsgCode: {} as ReferenzDto, ost: 0, nord: 0 });
    expect(component.form.valid).toBe(true);
  });

  it('should be invalid if epsgCode is set but ost is not', () => {
    component.form.setValue({
      epsgCode: {} as ReferenzDto,
      ost: null,
      nord: 0,
    });
    expect(component.form.valid).toBe(false);
    expect(component.form.get('epsgCode').valid).toBe(true);
    expect(component.form.get('nord').valid).toBe(true);
    expect(component.form.get('ost').valid).toBe(false);
    expect(component.form.get('ost').errors).toEqual({ required: true });
  });

  it('should be invalid if epsgCode is set but nord is not', () => {
    component.form.setValue({
      epsgCode: {} as ReferenzDto,
      ost: 0,
      nord: null,
    });
    expect(component.form.valid).toBe(false);
    expect(component.form.get('epsgCode').valid).toBe(true);
    expect(component.form.get('ost').valid).toBe(true);
    expect(component.form.get('nord').valid).toBe(false);
    expect(component.form.get('nord').errors).toEqual({ required: true });
  });

  it('should be invalid if numbers lie outside bounds', () => {
    component.form.setValue({
      epsgCode: {} as ReferenzDto,
      ost: -1,
      nord: 10_000_000_000,
    });
    expect(component.form.valid).toBe(false);
    expect(component.form.get('epsgCode').valid).toBe(true);
    expect(component.form.get('ost').valid).toBe(false);
    expect(component.form.get('ost').errors).toEqual({
      min: { min: 0, actual: -1 },
    });
    expect(component.form.get('nord').valid).toBe(false);
    expect(component.form.get('nord').errors).toEqual({
      max: { max: 9_999_999_999, actual: 10_000_000_000 },
    });
  });
});
