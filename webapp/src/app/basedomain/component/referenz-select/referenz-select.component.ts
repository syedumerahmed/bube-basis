import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DoCheck,
  forwardRef,
  Input,
  IterableDiffer,
  IterableDiffers,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormControl, FormUtil } from '@/base/forms';
import {
  ControlContainer,
  FormControlDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

export interface ReferenzLike {
  id: number;
  ktext: string;
}

@Component({
  selector: 'app-referenz-select',
  templateUrl: './referenz-select.component.html',
  styleUrls: ['./referenz-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ReferenzSelectComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferenzSelectComponent
  implements ControlValueAccessor<ReferenzLike>, DoCheck
{
  @Input() referenzliste: Array<ReferenzLike>;

  @ViewChild(FormControlDirective, { static: true })
  formControlDirective: FormControlDirective;
  @Input()
  formControl: FormControl<ReferenzLike>;
  @Input()
  required: string;
  @Input()
  formControlName: string;

  readonly byIdComparison = FormUtil.byIdComparison;
  private differ: IterableDiffer<ReferenzLike>;

  constructor(
    private readonly controlContainer: ControlContainer,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly differs: IterableDiffers
  ) {
    this.differ = differs.find([]).create();
  }

  get control(): FormControl<ReferenzLike> {
    return (
      this.formControl ||
      (this.controlContainer.control.get(
        this.formControlName
      ) as FormControl<ReferenzLike>)
    );
  }

  get ungueltigeReferenz(): boolean {
    const value = this.control.value;
    return value && !this.referenzliste?.find((item) => value.id === item.id);
  }

  ngDoCheck(): void {
    if (this.differ.diff(this.referenzliste)) {
      this.changeDetectorRef.markForCheck();
    }
  }

  registerOnChange(fn: (value: ReferenzLike) => void): void {
    this.formControlDirective.valueAccessor.registerOnChange(fn);
  }

  registerOnTouched(fn: () => void): void {
    this.formControlDirective.valueAccessor.registerOnTouched(fn);
  }

  writeValue(value: ReferenzLike): void {
    this.formControlDirective.valueAccessor.writeValue(value);
  }

  setDisabledState(isDisabled: boolean): void {
    this.formControlDirective.valueAccessor.setDisabledState(isDisabled);
  }
}
