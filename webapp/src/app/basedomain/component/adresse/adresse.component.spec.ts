import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AdresseComponent } from './adresse.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { FormBuilder } from '@/base/forms';
import { MockComponents, MockModule } from 'ng-mocks';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { AdresseDto, ReferenzDto } from '@api/stammdaten';

describe('AdresseComponent', () => {
  const adresse: AdresseDto = {
    hausNr: '1',
    landIsoCode: { schluessel: 'de' } as ReferenzDto,
    ort: 'Ort',
    plz: '12345',
  };

  let component: AdresseComponent;
  let fixture: ComponentFixture<AdresseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AdresseComponent,
        MockComponents(ReferenzSelectComponent, TooltipComponent),
      ],
      imports: [MockModule(ReactiveFormsModule), MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(AdresseComponent);
    component = fixture.componentInstance;
    component.isoCodes = new Referenzliste([]);
    component.vertraulichkeitsgruende = new Referenzliste([]);
    component.form = AdresseComponent.createForm(
      formBuilder,
      AdresseComponent.toForm(
        adresse,
        component.isoCodes,
        component.vertraulichkeitsgruende
      )
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });
});
