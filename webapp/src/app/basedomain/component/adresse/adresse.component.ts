import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { ClrForm } from '@clr/angular';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { AdresseDto, ReferenzDto } from '@api/stammdaten';

export interface AdresseFormData {
  strasse: string;
  hausNr: string;
  plz: string;
  ort: string;
  ortsteil: string;
  landIsoCode: ReferenzDto;
  vertraulichkeitsgrund: ReferenzDto;
  postfach: string;
  postfachPlz: string;
  notiz: string;
}

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.scss'],
})
export class AdresseComponent implements AfterViewInit, AfterViewChecked {
  @Input()
  form: FormGroup<AdresseFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  isoCodes: Referenzliste<Required<ReferenzDto>>;
  @Input()
  vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>;

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  static toForm(
    adresseDto: AdresseDto,
    landIsoCodes: Referenzliste<Required<ReferenzDto>>,
    vertraulichkeitsgruende: Referenzliste<Required<ReferenzDto>>
  ): AdresseFormData {
    return {
      strasse: adresseDto?.strasse,
      hausNr: adresseDto?.hausNr,
      plz: adresseDto?.plz,
      ort: adresseDto?.ort,
      ortsteil: adresseDto?.ortsteil,
      landIsoCode: landIsoCodes.find(
        adresseDto?.landIsoCode as Required<ReferenzDto>
      ),
      vertraulichkeitsgrund: vertraulichkeitsgruende.find(
        adresseDto?.vertraulichkeitsgrund as Required<ReferenzDto>
      ),
      postfach: adresseDto?.postfach,
      postfachPlz: adresseDto?.postfachPlz,
      notiz: adresseDto?.notiz,
    };
  }

  static createForm(
    fb: FormBuilder,
    adresse: AdresseFormData,
    selectedBerichtsjahr?: ReferenzDto
  ): FormGroup<AdresseFormData> {
    const referenzValidator = selectedBerichtsjahr
      ? BubeValidators.isReferenceValid(Number(selectedBerichtsjahr.schluessel))
      : Validators.nullValidator;
    return fb.group({
      strasse: [adresse.strasse, [Validators.maxLength(64)]],
      hausNr: [adresse.hausNr, [Validators.maxLength(14)]],
      plz: [adresse.plz, [Validators.required, Validators.maxLength(11)]],
      ort: [adresse.ort, [Validators.required, Validators.maxLength(64)]],
      ortsteil: [adresse.ortsteil, [Validators.maxLength(64)]],
      postfachPlz: [adresse.postfachPlz, [Validators.maxLength(24)]],
      postfach: [adresse.postfach, [Validators.maxLength(12)]],
      landIsoCode: [adresse.landIsoCode, referenzValidator],
      vertraulichkeitsgrund: [adresse.vertraulichkeitsgrund, referenzValidator],
      notiz: [adresse.notiz, [Validators.maxLength(1000)]],
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    if (this.form.invalid) {
      this.markAllAsTouched();
    }
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }
}
