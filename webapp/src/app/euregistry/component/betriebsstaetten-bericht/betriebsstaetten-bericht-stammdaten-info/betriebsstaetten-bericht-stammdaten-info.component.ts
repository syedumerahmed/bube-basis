import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { BetriebsstaetteInfoDto } from '@api/euregistry';
import { buildingIcon, ClarityIcons } from '@cds/core/icon';

ClarityIcons.addIcons(buildingIcon);
@Component({
  selector: 'app-betriebsstaetten-bericht-stammdaten-info',
  templateUrl: './betriebsstaetten-bericht-stammdaten-info.component.html',
  styleUrls: ['./betriebsstaetten-bericht-stammdaten-info.component.scss'],
})
export class BetriebsstaettenBerichtStammdatenInfoComponent implements OnInit {
  betriebsstaetteInfo: BetriebsstaetteInfoDto;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.route.data
      .pipe(map((data) => data.euRegBetriebsstaette))
      .subscribe((euRegBetriebsstaette) => {
        this.betriebsstaetteInfo = euRegBetriebsstaette.betriebsstaetteInfo;
      });
  }

  getVorschriften(): string {
    return this.betriebsstaetteInfo.vorschriften.join(', ').toString();
  }

  navigate(betriebsstaetteInfo: BetriebsstaetteInfoDto): void {
    this.router.navigate(
      [
        '/stammdaten/jahr',
        betriebsstaetteInfo.berichtsjahr,
        'betriebsstaette',
        'land',
        betriebsstaetteInfo.land,
        'nummer',
        betriebsstaetteInfo.betriebsstaetteNr,
      ],
      {
        queryParams: {
          localId: betriebsstaetteInfo.localId,
        },
      }
    );
  }
}
