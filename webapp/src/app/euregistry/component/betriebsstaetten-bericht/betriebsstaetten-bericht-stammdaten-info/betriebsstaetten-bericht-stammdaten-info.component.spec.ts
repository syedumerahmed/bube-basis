import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetriebsstaettenBerichtStammdatenInfoComponent } from './betriebsstaetten-bericht-stammdaten-info.component';
import { MockComponent, MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { EuRegBetriebsstaetteDto } from '@api/euregistry';

describe('BetriebsstaettenBerichtStammdatenInfoComponent', () => {
  let component: BetriebsstaettenBerichtStammdatenInfoComponent;
  let fixture: ComponentFixture<BetriebsstaettenBerichtStammdatenInfoComponent>;
  let routeData$: BehaviorSubject<{
    euRegBetriebsstaette: EuRegBetriebsstaetteDto;
  }>;

  beforeEach(async () => {
    routeData$ = new BehaviorSubject({
      euRegBetriebsstaette: {
        id: 1,
        betriebsstaetteInfo: {
          localId: '100',
          berichtsjahr: '2020',
          vorschriften: ['VOR1', 'VOR2'],
        },
        berichtsdaten: null,
        ersteErfassung: null,
        letzteAenderung: null,
        pruefungsfehler: false,
        pruefungVorhanden: false,
      },
    });
    await TestBed.configureTestingModule({
      declarations: [
        MockComponent(TooltipComponent),
        BetriebsstaettenBerichtStammdatenInfoComponent,
      ],
      imports: [MockModule(ClarityModule), MockModule(RouterTestingModule)],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$.asObservable(),
            snapshot: { data: routeData$.value },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(
      BetriebsstaettenBerichtStammdatenInfoComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show Info', () => {
    expect(component.betriebsstaetteInfo.localId).toBe('100');
    expect(component.betriebsstaetteInfo.berichtsjahr).toBe('2020');
  });

  it('should show Vorschriften', () => {
    expect(component.getVorschriften()).toBe('VOR1, VOR2');
  });
});
