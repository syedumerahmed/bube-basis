import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ClrForm } from '@clr/angular';
import { LandProperties, lookupLand } from '@/base/model/Land';
import { AlertService } from '@/base/service/alert.service';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import {
  BerichtsdatenDto,
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';
import { ClarityIcons, floppyIcon, listIcon, timesIcon } from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface BetriebsstaettenBerichtForm {
  zustaendigeBehoerde: BehoerdeListItem;
  akz: string;
  bearbeitungsstatus: ReferenzDto;
  bemerkung: string;
}

ClarityIcons.addIcons(listIcon, timesIcon, floppyIcon);

@Component({
  selector: 'app-betriebsstaetten-bericht-details',
  templateUrl: './betriebsstaetten-bericht-details.component.html',
  styleUrls: ['./betriebsstaetten-bericht-details.component.scss'],
})
export class BetriebsstaettenBerichtDetailsComponent
  implements OnInit, AfterViewChecked, OnDestroy, UnsavedChanges
{
  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  euRegBetriebsstaette: EuRegBetriebsstaetteDto;
  form: FormGroup<BetriebsstaettenBerichtForm>;

  behoerdenListe: Referenzliste<BehoerdeListItem>;
  bearbeitungsstatusListe: Array<ReferenzDto> = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly alertService: AlertService,
    private readonly service: EuRegistryRestControllerService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly euRegistryRestControllerService: EuRegistryRestControllerService
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      ({
        bearbeitungsstatusListe: this.bearbeitungsstatusListe,
        euRegBetriebsstaette: this.euRegBetriebsstaette,
      } = data);
      this.behoerdenListe = new Referenzliste(data.behoerdenListe);
      if (!this.euRegBetriebsstaette) {
        throw Error('euRegBetriebsstaette === ' + this.euRegBetriebsstaette);
      }
      if (!this.form) {
        this.form = this.createForm(
          this.toForm(this.euRegBetriebsstaette.berichtsdaten),
          this.berichtsjahrService.selectedBerichtsjahr
        );
      } else {
        // Workaround für Angular-Forms-Fehler bei Neuzuweisung der Form-Instanz mit eigenen ControlValueAccessors
        this.reset();
      }
      this.euRegistryRestControllerService
        .getAllYearsForBericht(this.euRegBetriebsstaette.id)
        .subscribe((jahre) => {
          this.berichtsjahrService.setFilter((jahr: ReferenzDto) => {
            return jahre.map((j) => j.schluessel).includes(jahr.schluessel);
          });
        });
    });
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.resetFilter();
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  private toForm(berichtsdaten: BerichtsdatenDto): BetriebsstaettenBerichtForm {
    return {
      zustaendigeBehoerde: this.behoerdenListe.find(
        new BehoerdeListItem(berichtsdaten.zustaendigeBehoerde)
      ),
      akz: berichtsdaten.akz,
      bearbeitungsstatus: berichtsdaten.bearbeitungsstatus,
      bemerkung: berichtsdaten.bemerkung,
    };
  }

  private createForm(
    bericht: BetriebsstaettenBerichtForm,
    selectedBerichtsJahr: ReferenzDto
  ): FormGroup<BetriebsstaettenBerichtForm> {
    const form = this.fb.group<BetriebsstaettenBerichtForm>({
      zustaendigeBehoerde: [
        bericht.zustaendigeBehoerde,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      akz: [bericht.akz, [Validators.maxLength(14)]],
      bearbeitungsstatus: [
        bericht.bearbeitungsstatus,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      bemerkung: [bericht.bemerkung, [Validators.maxLength(1000)]],
    });

    if (!this.canWrite) {
      form.disable();
    }

    return form;
  }

  get canWrite(): boolean {
    return this.euRegBetriebsstaette?.canWrite;
  }

  get displayLand(): string {
    return LandProperties[
      lookupLand(this.euRegBetriebsstaette.betriebsstaetteInfo.land)
    ].display;
  }

  save(): void {
    if (this.form.invalid) {
      this.clrForm.markAsTouched();
      return;
    }
    const euRegBetriebsstaette: EuRegBetriebsstaetteDto =
      this.formToEURegBetriebsstaetteDto();

    this.service.updateBetriebsstaettenBericht(euRegBetriebsstaette).subscribe(
      (euRegBetriebsstaette) => {
        this.euRegBetriebsstaette = euRegBetriebsstaette;
        this.reset();
        this.alertService.clearAlertMessages();
      },
      (error) => this.alertService.setAlert(error)
    );
  }

  formToEURegBetriebsstaetteDto(): EuRegBetriebsstaetteDto {
    return {
      ...this.euRegBetriebsstaette,
      berichtsdaten: {
        ...this.euRegBetriebsstaette.berichtsdaten,
        ...FormUtil.emptyStringsToNull(this.form.value),
      },
    };
  }

  reset(): void {
    this.form.reset(this.toForm(this.euRegBetriebsstaette.berichtsdaten));
    this.form.markAsPristine();
    // Für die Save / Reset Buttons. Brute Force Methode.
    this.changeDetectorRef.markForCheck();
  }

  canDeactivate(): boolean {
    return this.form.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
