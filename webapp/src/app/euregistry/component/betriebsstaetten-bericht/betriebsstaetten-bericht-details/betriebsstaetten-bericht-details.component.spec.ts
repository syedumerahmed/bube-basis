import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MockModule, MockProvider, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { BetriebsstaettenBerichtDetailsComponent } from '@/euregistry/component/betriebsstaetten-bericht/betriebsstaetten-bericht-details/betriebsstaetten-bericht-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';
import {
  BehoerdeListItemDto,
  ReferenzDto,
  ReferenzlisteEnum,
} from '@api/stammdaten';
import { Subject } from 'rxjs/internal/Subject';
import spyOn = jest.spyOn;

describe('BetriebsstaettenBerichtDetailsComponent', () => {
  let component: BetriebsstaettenBerichtDetailsComponent;
  let fixture: ComponentFixture<BetriebsstaettenBerichtDetailsComponent>;
  let routeData$: BehaviorSubject<{
    euRegBetriebsstaette: EuRegBetriebsstaetteDto;
  }>;

  const euRegBetriebsstaette: EuRegBetriebsstaetteDto = {
    id: 1,
    betriebsstaetteInfo: {
      localId: '100',
      land: '00',
      berichtsjahr: '2020',
      vorschriften: ['VOR1', 'VOR2'],
    },
    berichtsdaten: {
      zustaendigeBehoerde: {
        id: 15,
        gueltigBis: 2099,
        gueltigVon: 2000,
      } as BehoerdeListItemDto,
      akz: null,
      berichtsart: {
        id: 2,
        land: '00',
        referenzliste: ReferenzlisteEnum.RBETZ,
        schluessel: '02',
        sortier: '02',
        ktext: '02',
        gueltigVon: 2007,
        gueltigBis: 2099,
      },
      bearbeitungsstatus: {
        id: 1,
        land: '00',
        referenzliste: ReferenzlisteEnum.RBETZ,
        schluessel: '01',
        sortier: '01',
        ktext: '01',
        gueltigVon: 2007,
        gueltigBis: 2099,
      },
      bearbeitungsstatusSeit: '01.01.2021',
      bemerkung: null,
    },
    ersteErfassung: null,
    letzteAenderung: null,
    canWrite: true,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  beforeEach(async () => {
    routeData$ = new BehaviorSubject({
      euRegBetriebsstaette: euRegBetriebsstaette,
      bearbeitungsstatusListe: [
        euRegBetriebsstaette.berichtsdaten.bearbeitungsstatus,
      ],
      behoerdenListe: [euRegBetriebsstaette.berichtsdaten.zustaendigeBehoerde],
    });

    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      declarations: [TooltipComponent, BetriebsstaettenBerichtDetailsComponent],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$.asObservable(),
            snapshot: { data: routeData$.value },
          },
        },
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        MockProvider(EuRegistryRestControllerService, {
          getAllYearsForBericht(): Observable<any> {
            return of([]);
          },
        }),
        MockProviders(AlertService),
      ],
      imports: [
        MockModule(RouterTestingModule),
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
        MockModule(BasedomainModule),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetriebsstaettenBerichtDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be readonly', () => {
    component.form = null;
    routeData$.next({
      ...routeData$.value,
      euRegBetriebsstaette: { ...euRegBetriebsstaette, canWrite: false },
    });

    expect(component.form.disabled).toBe(true);
  });

  it('should be valid and pristine', () => {
    expect(component.form.valid).toBe(true);
    expect(component.form.pristine).toBe(true);
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({
        akz: '',
        bearbeitungsstatus: null,
        bemerkung: '',
        zustaendigeBehoerde: null,
      });
    });

    it('should be invalid', () => {
      expect(component.form.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.save();
      });

      it('should not call service', inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          expect(service.updateBetriebsstaettenBericht).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({
        akz: euRegBetriebsstaette.berichtsdaten.akz,
        bearbeitungsstatus:
          euRegBetriebsstaette.berichtsdaten.bearbeitungsstatus,
        bemerkung: euRegBetriebsstaette.berichtsdaten.bemerkung,
        zustaendigeBehoerde:
          euRegBetriebsstaette.berichtsdaten.zustaendigeBehoerde,
      });
    });

    it('should be valid', () => {
      expect(component.form.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedDto: EuRegBetriebsstaetteDto = euRegBetriebsstaette;
      let observable: Subject<EuRegBetriebsstaetteDto>;
      const responseDto: EuRegBetriebsstaetteDto = {
        ...expectedDto,
        letzteAenderung: new Date().toISOString(),
      };
      beforeEach(inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          observable = new Subject<EuRegBetriebsstaetteDto>();
          spyOn(service, 'updateBetriebsstaettenBericht').mockReturnValue(
            observable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should call service', inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          const expected = JSON.parse(JSON.stringify(expectedDto));
          expect(service.updateBetriebsstaettenBericht).toHaveBeenCalledWith(
            expected
          );
        }
      ));

      describe('on success', () => {
        beforeEach(() => {
          observable.next(responseDto);
          observable.complete();
        });

        it('should update euRegBetriebsstaette', () => {
          expect(component.euRegBetriebsstaette).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.form.touched).toBe(false);
          expect(component.form.dirty).toBe(false);
        });
      });
    });
  });
});
