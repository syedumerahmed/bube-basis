import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import {
  EuRegAnlageListItemDto,
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';
import { ClarityIcons, eyeIcon, fileGroupIcon } from '@cds/core/icon';

ClarityIcons.addIcons(fileGroupIcon, eyeIcon);
@Component({
  selector: 'app-betriebsstaetten-bericht-anlagen-liste',
  templateUrl: './betriebsstaetten-bericht-anlagen-liste.component.html',
  styleUrls: ['./betriebsstaetten-bericht-anlagen-liste.component.scss'],
})
export class BetriebsstaettenBerichtAnlagenListeComponent implements OnInit {
  anlagenBerichte: Array<EuRegAnlageListItemDto>;
  euRegBetriebsstaette: EuRegBetriebsstaetteDto;

  constructor(
    private route: ActivatedRoute,
    private euRegistryService: EuRegistryRestControllerService,
    private alertService: AlertService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.euRegBetriebsstaette = data.euRegBetriebsstaette;
      this.refresh();
    });
  }

  refresh(): void {
    this.euRegistryService
      .readAllAnlagenBerichte(this.euRegBetriebsstaette.betriebsstaetteInfo.id)
      .subscribe(
        (anlagen) => {
          this.anlagenBerichte = anlagen;
          this.changeDetectorRef.markForCheck();
        },
        (error) => {
          this.alertService.setAlert(error);
        }
      );
  }
}
