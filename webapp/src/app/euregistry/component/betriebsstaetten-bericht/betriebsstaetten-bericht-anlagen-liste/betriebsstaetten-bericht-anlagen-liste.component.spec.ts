import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetriebsstaettenBerichtAnlagenListeComponent } from './betriebsstaetten-bericht-anlagen-liste.component';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { ActivatedRoute, Data } from '@angular/router';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import {
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';

describe('BetriebsstaettenBerichtAnlagenListeComponent', () => {
  const betriebsstaette: EuRegBetriebsstaetteDto = {
    berichtsdaten: {
      zustaendigeBehoerde: undefined,
      berichtsart: undefined,
      bearbeitungsstatus: undefined,
      bearbeitungsstatusSeit: '',
    },
    ersteErfassung: '',
    letzteAenderung: '',
    betriebsstaetteInfo: {
      betriebsstatus: undefined,
      berichtsjahr: undefined,
      betriebsstaetteNr: '',
      land: '',
      name: '',
    },
    id: 1,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  let dataSubject: Subject<Data>;
  let component: BetriebsstaettenBerichtAnlagenListeComponent;
  let fixture: ComponentFixture<BetriebsstaettenBerichtAnlagenListeComponent>;

  beforeEach(async () => {
    dataSubject = new BehaviorSubject({ betriebsstaette });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [
        BetriebsstaettenBerichtAnlagenListeComponent,
        TooltipComponent,
      ],
      providers: [
        {
          provide: EuRegistryRestControllerService,
          useValue: MockService(EuRegistryRestControllerService),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: dataSubject.asObservable(),
            snapshot: { data: { betriebsstaette } },
          },
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(
      BetriebsstaettenBerichtAnlagenListeComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
