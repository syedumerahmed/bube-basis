import { Component, ViewChild } from '@angular/core';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { BetriebsstaettenBerichtDetailsComponent } from '@/euregistry/component/betriebsstaetten-bericht/betriebsstaetten-bericht-details/betriebsstaetten-bericht-details.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-betriebsstaetten-bericht-main',
  templateUrl: './betriebsstaetten-bericht-main.component.html',
  styleUrls: ['./betriebsstaetten-bericht-main.component.scss'],
})
export class BetriebsstaettenBerichtMainComponent implements UnsavedChanges {
  @ViewChild(BetriebsstaettenBerichtDetailsComponent, { static: true })
  betriebsstaetteDetailsComponent: BetriebsstaettenBerichtDetailsComponent;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  canDeactivate(): boolean {
    return this.betriebsstaetteDetailsComponent.canDeactivate();
  }

  showSaveWarning(): void {
    this.betriebsstaetteDetailsComponent.showSaveWarning();
  }

  navigateBackToUebersicht(): void {
    this.router.navigate(['.'], { relativeTo: this.route.parent.parent });
  }
}
