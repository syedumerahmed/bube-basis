import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetriebsstaettenBerichtComponent } from './betriebsstaetten-bericht.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('BetriebsstaettenBerichtComponent', () => {
  let component: BetriebsstaettenBerichtComponent;
  let fixture: ComponentFixture<BetriebsstaettenBerichtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [BetriebsstaettenBerichtComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetriebsstaettenBerichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
