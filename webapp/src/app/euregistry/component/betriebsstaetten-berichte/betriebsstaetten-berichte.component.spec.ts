import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { BetriebsstaettenBerichteComponent } from './betriebsstaetten-berichte.component';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReferenzDto, Suchattribute } from '@api/stammdaten';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { Observable, of, Subject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { SucheComponent } from '@/stammdaten/betriebsstaette/component/suche/suche.component';
import { DesktopService } from '@/desktop/service/desktop.service';
import { DatenInAnderesJahrUebernehmenModalComponent } from '@/basedomain/component/daten-in-anderes-jahr-uebenrnehmen-modal/daten-in-anderes-jahr-uebernehmen-modal.component';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { ActivatedRoute } from '@angular/router';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { LandAuswahlComponent } from '@/base/component/land-auswahl/land-auswahl.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { ExportBerichtsdatenModalComponent } from '@/euregistry/component/export-berichtsdaten-modal/export-berichtsdaten-modal.component';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import { EuRegImportAdapterService } from '@/euregistry/service/EuRegImportAdapterService';
import {
  EuRegistryRestControllerService,
  PageDtoEuRegBetriebsstaetteListItemDto,
  XeurBerichtRestControllerService,
} from '@api/euregistry';
import spyOn = jest.spyOn;

describe(BetriebsstaettenBerichteComponent.name, () => {
  let component: BetriebsstaettenBerichteComponent;
  let fixture: ComponentFixture<BetriebsstaettenBerichteComponent>;
  let selectedBerichtsjahr$: Subject<ReferenzDto>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(RouterTestingModule)],
      providers: [
        MockProviders(
          EuRegistryRestControllerService,
          XeurBerichtRestControllerService,
          BerichtsjahrService,
          DesktopService,
          FilenameDateFormatPipe,
          EuRegImportAdapterService
        ),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              parent: {
                data: { berichtsjahre: [{ id: 2020 }, { id: 2019 }] },
              },
            },
          },
        },
      ],
      declarations: [
        BetriebsstaettenBerichteComponent,
        MockComponents(
          SucheComponent,
          DatenInAnderesJahrUebernehmenModalComponent,
          LandAuswahlComponent,
          TooltipComponent,
          ExportBerichtsdatenModalComponent,
          ImportModalComponent
        ),
      ],
    }).compileComponents();
  });

  beforeEach(inject(
    [BerichtsjahrService],
    (berichtsjahrService: BerichtsjahrService) => {
      selectedBerichtsjahr$ = new Subject();
      jest
        .spyOn(berichtsjahrService, 'selectedBerichtsjahr$', 'get')
        .mockReturnValue(selectedBerichtsjahr$.asObservable());
      fixture = TestBed.createComponent(BetriebsstaettenBerichteComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }
  ));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('after creation', () => {
    it('should indicate loading', () => {
      expect(component.loading$.value).toBe(true);
    });

    describe('after setting berichtsjahr and datagridState', () => {
      let pageResponse: Subject<PageDtoEuRegBetriebsstaetteListItemDto>;
      beforeEach(inject(
        [EuRegistryRestControllerService],
        (euRegistryController: EuRegistryRestControllerService) => {
          pageResponse = new Subject();
          spyOn(
            euRegistryController,
            'readAllBetriebsstaettenBerichte'
          ).mockReturnValue(pageResponse.asObservable() as Observable<any>);
          selectedBerichtsjahr$.next({ id: 15 } as ReferenzDto);
          component.dataGridState$.next({
            page: { current: 1, from: 0, to: 20, size: 20 },
          });
        }
      ));

      it('should indicate loading', () => {
        expect(component.loading$.value).toBe(true);
      });

      it('should fetch Page from service', inject(
        [EuRegistryRestControllerService],
        (euRegistryController: EuRegistryRestControllerService) => {
          expect(
            euRegistryController.readAllBetriebsstaettenBerichte
          ).toHaveBeenCalledWith(15, {
            suche: null,
            page: { current: 0, from: 0, to: 20, size: 20 },
          });
        }
      ));

      describe('on server response', () => {
        beforeEach(() => {
          component.euRegBetriebsstaettenPage = {
            totalElements: 1,
            content: [
              {
                id: 20,
                bstId: 21,
                name: 'Betriebsstätte',
                betriebsstatus: 'In Betrieb',
                nummer: '20',
                ort: 'Hamburg',
                plz: 'Hamburg',
                zustaendigeBehoerde: 'UBA',
                land: '02',
                pruefungsfehler: false,
                pruefungVorhanden: false,
              },
            ],
          };
        });

        it('should select all', inject(
          [EuRegistryRestControllerService, BerichtsjahrService],
          (
            euRegistryController: EuRegistryRestControllerService,
            berichtsjahrService: BerichtsjahrService
          ) => {
            jest
              .spyOn(berichtsjahrService, 'selectedBerichtsjahr', 'get')
              .mockReturnValue({ id: 15 } as ReferenzDto);
            spyOn(
              euRegistryController,
              'readAllBetriebsstaettenBerichteIds'
            ).mockReturnValue(of([4, 8, 15, 16, 23, 42]) as Observable<any>);

            component.alleMarkieren();

            expect(
              euRegistryController.readAllBetriebsstaettenBerichteIds
            ).toHaveBeenLastCalledWith(15, null);
            expect(component.selectedIds).toEqual([4, 8, 15, 16, 23, 42]);
          }
        ));
      });

      describe('on search', () => {
        let suchattribute: Suchattribute;

        beforeEach(() => {
          suchattribute = {
            betriebsstaette: {
              name: '*Betrieb*',
            },
          };
          component.searchattributes$.next(suchattribute);
        });

        it('should fetch Page from service', inject(
          [EuRegistryRestControllerService],
          (euRegistryController: EuRegistryRestControllerService) => {
            expect(
              euRegistryController.readAllBetriebsstaettenBerichte
            ).toHaveBeenLastCalledWith(15, {
              suche: suchattribute,
              page: { current: 0, from: 0, to: 20, size: 20 },
            });
          }
        ));
      });

      describe('on berichtsjahrwechsel', () => {
        beforeEach(() => {
          component.currentPage = 3;
          selectedBerichtsjahr$.next({ id: 2019 } as ReferenzDto);
        });

        it('should set loading to be true', () => {
          expect(component.loading$.value).toBe(true);
        });

        it('should reset to page 1', () => {
          expect(component.currentPage).toBe(1);
        });

        it('should clear selection', () => {
          expect(component.selectedIds).toHaveLength(0);
        });

        it('should invoke readAllBetriebssaette with mapped state to gridstatedto and berichtsjahr', inject(
          [EuRegistryRestControllerService],
          (euRegService: EuRegistryRestControllerService) => {
            expect(
              euRegService.readAllBetriebsstaettenBerichte
            ).toHaveBeenLastCalledWith(2019, {
              suche: null,
              page: { current: 0, size: 20, from: 0, to: 20 },
            });
          }
        ));
      });
    });
  });
});
