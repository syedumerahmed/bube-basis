import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ClrDatagridSortOrder, ClrDatagridStateInterface } from '@clr/angular';
import {
  BehaviorSubject,
  combineLatest,
  of,
  ReplaySubject,
  Subject,
} from 'rxjs';
import { catchError, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { DesktopService } from '@/desktop/service/desktop.service';
import { HttpErrorResponse } from '@angular/common/http';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';
import { JobStatusService } from '@/base/service/job-status.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import * as fileSaver from 'file-saver';
import { Event, EventBusService } from '@/base/service/event-bus.service';
import { EuRegImportAdapterService } from '@/euregistry/service/EuRegImportAdapterService';
import {
  DownloadKomplexpruefungCommand,
  EuRegBetriebsstaetteListItemDto,
  EuRegBetriebsstaettenListStateDto,
  EuRegistryRestControllerService,
  KomplexpruefungDurchfuehrenCommand,
  PageDtoEuRegBetriebsstaetteListItemDto,
  SortStateDtoEuRegBetriebsstaetteColumnByEnum,
  UebernimmEuRegDatenInJahrCommandDto,
  XeurBerichtRestControllerService,
  XeurTeilberichtErstellenCommandDto,
} from '@api/euregistry';
import { ReferenzDto, Suchattribute } from '@api/stammdaten';
import {
  angleIcon,
  checkIcon,
  ClarityIcons,
  eyeIcon,
  fileGroupIcon,
  listIcon,
  timesIcon,
  treeViewIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(
  fileGroupIcon,
  checkIcon,
  timesIcon,
  treeViewIcon,
  angleIcon,
  eyeIcon,
  listIcon
);
@Component({
  selector: 'app-betriebsstaetten-berichte',
  templateUrl: './betriebsstaetten-berichte.component.html',
  styleUrls: ['./betriebsstaetten-berichte.component.scss'],
})
export class BetriebsstaettenBerichteComponent implements OnInit, OnDestroy {
  constructor(
    private readonly euRegistryService: EuRegistryRestControllerService,
    private readonly xeurBerichtService: XeurBerichtRestControllerService,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly alertService: AlertService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly desktopService: DesktopService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly route: ActivatedRoute,
    private readonly jobStatusService: JobStatusService,
    private readonly dateFormat: FilenameDateFormatPipe,
    private readonly eventbus: EventBusService,
    private readonly router: Router,
    readonly euregImportAdapter: EuRegImportAdapterService
  ) {}

  readonly euRegBetriebsstaetteColumns =
    SortStateDtoEuRegBetriebsstaetteColumnByEnum;
  readonly SortOrder = ClrDatagridSortOrder;

  readonly currentBerichtsjahr$ = new ReplaySubject<ReferenzDto>(1);
  readonly dataGridState$ = new ReplaySubject<ClrDatagridStateInterface>(1);
  readonly searchattributes$ = new BehaviorSubject<Suchattribute>(null);
  readonly loading$ = new BehaviorSubject(true);
  private readonly destroyed$ = new Subject();
  private updateEuRegBetriebsstaettenListe$ = new BehaviorSubject<void>(null);

  euRegBetriebsstaettenPage: PageDtoEuRegBetriebsstaetteListItemDto;
  currentPage: number;
  selectedIds: Array<number> = [];
  uebernehmenDialogOpen = false;
  exportDialogVisible = false;

  importDialogVisible = false;
  berichtsjahrListe: Array<ReferenzDto>;

  private static mapState(
    state: ClrDatagridStateInterface
  ): EuRegBetriebsstaettenListStateDto {
    if (state.page) {
      // Die Page wird im Backend mit 0 angefangen und bei Clarity mit 1
      state.page.current--;
    }
    return state as EuRegBetriebsstaettenListStateDto;
  }

  ngOnInit(): void {
    this.berichtsjahrListe = this.route.snapshot.parent.data.berichtsjahre;
    this.currentBerichtsjahr$.subscribe(() => {
      this.currentPage = 1;
      this.markierungAufheben();
    });
    combineLatest([
      this.currentBerichtsjahr$.pipe(map((berichtsjahr) => berichtsjahr.id)),
      this.dataGridState$.pipe(map(BetriebsstaettenBerichteComponent.mapState)),
      this.searchattributes$.pipe(tap(() => this.markierungAufheben())),
      this.updateEuRegBetriebsstaettenListe$,
    ])
      .pipe(
        takeUntil(this.destroyed$),
        tap(() => this.loading$.next(true)),
        switchMap(([berichtsjahrId, listStateDto, suche]) =>
          this.euRegistryService.readAllBetriebsstaettenBerichte(
            berichtsjahrId,
            {
              ...listStateDto,
              suche,
            }
          )
        ),
        catchError((error) => {
          this.alertService.setAlert(error);
          return of({ content: [], totalElements: 0 });
        }),
        tap(() => this.loading$.next(false))
      )
      .subscribe((response) => {
        this.euRegBetriebsstaettenPage = response;
      });
    this.berichtsjahrService.selectedBerichtsjahr$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(this.currentBerichtsjahr$);
    this.eventbus
      .on(
        Event.KomplexpruefungFertig,
        Event.BerichtsdatenExportFertig,
        Event.BerichtsdatenImportFertig,
        Event.XeurTeilberichtFertig
      )
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.markierungAufheben();
        this.updateEuRegBetriebsstaettenListe$.next();
      });
  }

  inJahrUebernehmen(zieljahr: ReferenzDto): void {
    const uebernimmCommand: UebernimmEuRegDatenInJahrCommandDto = {
      berichtsIds: this.selectedIds,
      zieljahr,
    };
    this.euRegistryService.euregDatenUebernehmen(uebernimmCommand).subscribe(
      () => {
        this.uebernehmenDialogOpen = false;
        this.jobStatusService.jobStarted();
        this.markierungAufheben();
      },
      (err) => {
        this.uebernehmenDialogOpen = false;
        this.alertService.setAlert(err);
      }
    );
  }

  createTeilbericht(): void {
    const teilberichtCommand: XeurTeilberichtErstellenCommandDto = {
      berichtsIds: this.selectedIds,
      berichtsjahr: this.berichtsjahrService.selectedBerichtsjahr,
      // TODO Für Gesamtadmin gesondert filtern
      land: this.benutzerProfil.landNr,
    };
    this.xeurBerichtService
      .erstelleXeurTeilbericht(teilberichtCommand)
      .subscribe(
        () => {
          this.jobStatusService.jobStarted();
          this.markierungAufheben();
        },
        (err: HttpErrorResponse) => this.alertService.setAlert(err.message)
      );
  }

  openUebernehmenDialog(): void {
    this.uebernehmenDialogOpen = true;
  }

  get canUebernehmen(): boolean {
    return this.benutzerProfil.hasAnyRolle(
      Rolle.LAND_WRITE,
      Rolle.BEHOERDE_WRITE
    );
  }

  get canCreateTeilbericht(): boolean {
    return (
      this.benutzerProfil.hasRolle(Rolle.LAND_WRITE) &&
      !this.benutzerProfil.isGesamtadmin
    );
  }

  get canExport(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_READ);
  }

  get canImport(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_DELETE);
  }

  get isBehoerdenbenutzer(): boolean {
    return this.benutzerProfil.isBehoerdenbenutzer;
  }

  get showLandColumn(): boolean {
    return this.benutzerProfil.isGesamtadmin;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  alleMarkieren(): void {
    this.euRegistryService
      .readAllBetriebsstaettenBerichteIds(
        this.berichtsjahrService.selectedBerichtsjahr.id,
        this.searchattributes$.value
      )
      .subscribe(
        (allIds) => {
          this.selectedIds = allIds;
          this.changeDetectorRef.markForCheck();
        },
        (err: HttpErrorResponse) => this.alertService.setAlert(err.message)
      );
  }

  markierungAufheben(): void {
    this.selectedIds = [];
    this.changeDetectorRef.markForCheck();
  }

  get nothingSelected(): boolean {
    return this.selectedIds.length === 0;
  }

  inDesktopSpeichern(): void {
    this.desktopService.inDesktopSpeichern(
      'EUREG',
      this.selectedIds,
      this.berichtsjahrService.selectedBerichtsjahr.schluessel
    );
    this.markierungAufheben();
  }

  onDatagridStateChanged(state: ClrDatagridStateInterface): void {
    this.dataGridState$.next(state);
  }

  komplexpruefungAusfuehren(): void {
    const command: KomplexpruefungDurchfuehrenCommand = {
      ids: this.selectedIds,
      regelGruppe: 'GRUPPE_B',
    };
    this.euRegistryService.runKomplexpruefung(command).subscribe(
      () => {
        this.jobStatusService.jobStarted();
        this.markierungAufheben();
      },
      (err) => {
        this.alertService.setAlert(err);
      }
    );
  }

  komplexpruefungDownload(bstId: number, bstNummer: string): void {
    const command: DownloadKomplexpruefungCommand = {
      bstId: bstId,
    };

    this.euRegistryService.komplexpruefungDownload(command).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(new Date());
        fileSaver.saveAs(
          blob,
          'EU-Prüfung_' + bstNummer + '_' + datum + '.txt'
        );
      },
      () => {
        this.alertService.setAlert('Fehler beim Download des Protokolls');
      }
    );
  }

  getPruefungsfehler(bericht: EuRegBetriebsstaetteListItemDto): string {
    if (bericht.pruefungVorhanden) {
      return bericht.pruefungsfehler ? 'Ja' : 'Nein';
    }
    return '-';
  }

  navigate(bericht: EuRegBetriebsstaetteListItemDto): void {
    this.router.navigate(
      [
        'eureg',
        'berichtsdaten',
        'jahr',
        this.berichtsjahrService.selectedBerichtsjahr.schluessel,
        'betriebsstaette',
        'land',
        bericht.land,
        'nummer',
        bericht.nummer,
      ],
      {
        queryParams: { localId: bericht.localId },
      }
    );
  }
}
