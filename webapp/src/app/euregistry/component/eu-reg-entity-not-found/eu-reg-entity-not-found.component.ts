import { Component, OnDestroy, OnInit } from '@angular/core';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ClarityIcons, fileGroupIcon } from '@cds/core/icon';

ClarityIcons.addIcons(fileGroupIcon);
@Component({
  selector: 'app-entity-not-found',
  templateUrl: './eu-reg-entity-not-found.component.html',
  styleUrls: ['./eu-reg-entity-not-found.component.scss'],
})
export class EuRegEntityNotFoundComponent implements OnInit, OnDestroy {
  constructor(private readonly berichtsjahrService: BerichtsjahrService) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }
}
