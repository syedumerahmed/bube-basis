import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EuRegEntityNotFoundComponent } from './eu-reg-entity-not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { MockModule, MockService } from 'ng-mocks';

describe('EntityNotFoundComponent', () => {
  let component: EuRegEntityNotFoundComponent;
  let fixture: ComponentFixture<EuRegEntityNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, MockModule(ClarityModule)],
      declarations: [EuRegEntityNotFoundComponent],
      providers: [
        {
          provide: BerichtsjahrService,
          useValue: MockService(BerichtsjahrService),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EuRegEntityNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
