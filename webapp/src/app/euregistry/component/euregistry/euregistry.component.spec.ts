import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EuregistryComponent } from './euregistry.component';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import {
  BEHOERDE_READ_TEST_USER,
  GESAMTADMIN_DUMMY,
  LANDADMIN_DUMMY,
} from '@/base/security/model/benutzer-profil.mock';

describe('EuregistryComponent', () => {
  let component: EuregistryComponent;
  let fixture: ComponentFixture<EuregistryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EuregistryComponent],
      providers: [{ provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY }],
      imports: [MockModule(ClarityModule), RouterTestingModule],
    }).compileComponents();
  });

  describe('As Gesamtadmin', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(EuregistryComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
    it('should create', () => {
      expect(component).toBeTruthy();
    });
    it('should have access to all', () => {
      expect(component.visibleNavLinks).toEqual({
        BERICHTSDATEN: true,
        XEUR_TEILBERICHTE: true,
      });
    });
    it('should show subnav', () => {
      expect(component.showSubnav).toBe(true);
    });
  });

  describe('As Landadmin', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, {
        useValue: LANDADMIN_DUMMY(),
      });
      fixture = TestBed.createComponent(EuregistryComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have access to all', () => {
      expect(component.visibleNavLinks).toEqual({
        BERICHTSDATEN: true,
        XEUR_TEILBERICHTE: true,
      });
    });
    it('should show subnav', () => {
      expect(component.showSubnav).toBe(true);
    });
  });

  describe('As Behoerde', () => {
    beforeEach(() => {
      TestBed.overrideProvider(BenutzerProfil, {
        useValue: BEHOERDE_READ_TEST_USER(),
      });
      fixture = TestBed.createComponent(EuregistryComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have access to Berichtsdaten', () => {
      expect(component.visibleNavLinks).toEqual({
        BERICHTSDATEN: true,
        XEUR_TEILBERICHTE: false,
      });
    });
    it('should not show subnav', () => {
      expect(component.showSubnav).toBe(false);
    });
  });
});
