import { Component } from '@angular/core';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';

@Component({
  selector: 'app-euregistry',
  templateUrl: './euregistry.component.html',
  styleUrls: ['./euregistry.component.scss'],
})
export class EuregistryComponent {
  readonly visibleNavLinks: Record<
    'BERICHTSDATEN' | 'XEUR_TEILBERICHTE',
    boolean
  > = {
    BERICHTSDATEN: this.benutzerProfil.canSeeEURegBerichtsdaten,
    XEUR_TEILBERICHTE: this.benutzerProfil.hasRolle(Rolle.LAND_READ),
  };

  constructor(private readonly benutzerProfil: BenutzerProfil) {}

  get showSubnav(): boolean {
    return Object.values(this.visibleNavLinks).filter(Boolean).length > 1;
  }
}
