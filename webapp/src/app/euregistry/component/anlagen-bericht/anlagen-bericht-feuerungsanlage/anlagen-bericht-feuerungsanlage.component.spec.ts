import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AnlagenBerichtFeuerungsanlageComponent } from './anlagen-bericht-feuerungsanlage.component';
import { MockComponent, MockModule, MockProvider } from 'ng-mocks';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  FeuerungsanlageBerichtsdatenDto,
  FeuerungsanlagenTypEnum,
} from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';

const feuerungsanlage: FeuerungsanlageBerichtsdatenDto = {
  feuerungsanlageTyp: FeuerungsanlagenTypEnum.WI,
  gesamtKapazitaetAbfall: 0,
  kapazitaetGefAbfall: 0,
  kapazitaetNichtGefAbfall: 0,
  mehrWaermeVonGefAbfall: false,
  mitverbrennungVonUnbehandeltemAbfall: false,
  oeffentlicheBekanntmachung: 'oeffentlicheBekanntmachung',
  ieAusnahmen: [],
  oeffentlicheBekanntmachungUrl: 'oeffentlicheBekanntmachungUrl',
};

describe('AnlagenBerichtFeuerungsanlageComponent', () => {
  let component: AnlagenBerichtFeuerungsanlageComponent;
  let fixture: ComponentFixture<AnlagenBerichtFeuerungsanlageComponent>;

  beforeEach(async () => {
    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtFeuerungsanlageComponent,
        MockComponent(TooltipComponent),
      ],
      imports: [
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(RouterTestingModule),
      ],
      providers: [
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(AnlagenBerichtFeuerungsanlageComponent);
    component = fixture.componentInstance;
    component.ieAusnahmenListe = new Referenzliste([]);
    component.form = AnlagenBerichtFeuerungsanlageComponent.createForm(
      formBuilder,
      AnlagenBerichtFeuerungsanlageComponent.toForm(feuerungsanlage),
      true,
      {
        gueltigBis: 0,
        gueltigVon: 0,
        ktext: '',
        land: '',
        referenzliste: undefined,
        sortier: '',
        schluessel: '2020',
      }
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      AnlagenBerichtFeuerungsanlageComponent.toForm(feuerungsanlage)
    );
  });
});
