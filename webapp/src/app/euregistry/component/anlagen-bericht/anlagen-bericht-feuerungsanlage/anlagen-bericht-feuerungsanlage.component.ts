import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  AnlagenBerichtSpezielleBedingungenComponent,
  SpezielleBedingungFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-spezielle-bedingungen/anlagen-bericht-spezielle-bedingungen.component';
import {
  FeuerungsanlageBerichtsdatenDto,
  FeuerungsanlagenTypEnum,
} from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';

export interface FeuerungsanlageFormData {
  feuerungsanlageTyp: FeuerungsanlagenTypEnum;
  gesamtKapazitaetAbfall: number;
  kapazitaetGefAbfall: number;
  kapazitaetNichtGefAbfall: number;
  mehrWaermeVonGefAbfall: boolean;
  mitverbrennungVonUnbehandeltemAbfall: boolean;
  oeffentlicheBekanntmachung: string;
  oeffentlicheBekanntmachungUrl: string;
  ieAusnahmen: Array<ReferenzDto>;
  spezielleBedingungen: Array<SpezielleBedingungFormData>;
}

@Component({
  selector: 'app-anlagen-bericht-feuerungsanlage',
  templateUrl: './anlagen-bericht-feuerungsanlage.component.html',
  styleUrls: ['./anlagen-bericht-feuerungsanlage.component.scss'],
})
export class AnlagenBerichtFeuerungsanlageComponent
  implements AfterViewInit, AfterViewChecked
{
  @Input()
  form: FormGroup<FeuerungsanlageFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  hat17BImSchV: boolean;
  @Input()
  hat13BImSchV: boolean;
  @Input()
  ieAusnahmenListe: Referenzliste<Required<ReferenzDto>>;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  readonly typListe: Array<FeuerungsanlagenTypEnum> = Object.values(
    FeuerungsanlagenTypEnum
  );

  readonly typen: Record<FeuerungsanlagenTypEnum, string> = {
    LCP: 'LCP',
    WI: 'WI',
    CO_WI: 'co-WI',
  };

  static toForm(dto: FeuerungsanlageBerichtsdatenDto): FeuerungsanlageFormData {
    return {
      feuerungsanlageTyp: dto?.feuerungsanlageTyp,
      gesamtKapazitaetAbfall: dto?.gesamtKapazitaetAbfall,
      kapazitaetGefAbfall: dto?.kapazitaetGefAbfall,
      kapazitaetNichtGefAbfall: dto?.kapazitaetNichtGefAbfall,
      mehrWaermeVonGefAbfall: dto?.mehrWaermeVonGefAbfall,
      mitverbrennungVonUnbehandeltemAbfall:
        dto?.mitverbrennungVonUnbehandeltemAbfall,
      oeffentlicheBekanntmachung: dto?.oeffentlicheBekanntmachung,
      oeffentlicheBekanntmachungUrl: dto?.oeffentlicheBekanntmachungUrl,
      ieAusnahmen: dto?.ieAusnahmen,
      spezielleBedingungen: AnlagenBerichtSpezielleBedingungenComponent.toForm(
        dto?.spezielleBedingungen
      ),
    };
  }

  static createForm(
    fb: FormBuilder,
    feuerungsanlage: FeuerungsanlageFormData,
    hat17BImSchV: boolean,
    selectedBerichtsjahr: ReferenzDto
  ): FormGroup<FeuerungsanlageFormData> {
    return fb.group<FeuerungsanlageFormData>({
      feuerungsanlageTyp: [
        feuerungsanlage?.feuerungsanlageTyp,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      gesamtKapazitaetAbfall: [
        feuerungsanlage?.gesamtKapazitaetAbfall,
        [Validators.required, Validators.min(0), Validators.max(9999.99)],
      ],
      kapazitaetGefAbfall: [
        feuerungsanlage?.kapazitaetGefAbfall,
        [Validators.required, Validators.min(0), Validators.max(9999.99)],
      ],
      kapazitaetNichtGefAbfall: [
        feuerungsanlage?.kapazitaetNichtGefAbfall,
        [Validators.required, Validators.min(0), Validators.max(9999.99)],
      ],
      mehrWaermeVonGefAbfall: [feuerungsanlage?.mehrWaermeVonGefAbfall],
      mitverbrennungVonUnbehandeltemAbfall: [
        feuerungsanlage?.mitverbrennungVonUnbehandeltemAbfall,
      ],
      oeffentlicheBekanntmachung: [
        feuerungsanlage?.oeffentlicheBekanntmachung,
        [
          hat17BImSchV ? Validators.required : Validators.nullValidator,
          Validators.maxLength(100),
        ],
      ],
      oeffentlicheBekanntmachungUrl: [
        feuerungsanlage?.oeffentlicheBekanntmachungUrl,
        [Validators.maxLength(500), BubeValidators.url],
      ],
      ieAusnahmen: [
        feuerungsanlage?.ieAusnahmen,
        BubeValidators.areReferencesValid(
          Number(selectedBerichtsjahr.schluessel)
        ),
      ],
      spezielleBedingungen:
        AnlagenBerichtSpezielleBedingungenComponent.createForm(
          fb,
          feuerungsanlage?.spezielleBedingungen ?? [],
          selectedBerichtsjahr
        ),
    });
  }

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
    this.changeDetectorRef.markForCheck();
  }

  displayTyp(typ: FeuerungsanlagenTypEnum): string {
    return this.typen[typ];
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }
}
