import { Component, ViewChild } from '@angular/core';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';
import { AnlagenBerichtDetailsComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-details/anlagen-bericht-details.component';

@Component({
  selector: 'app-anlagen-bericht-main',
  templateUrl: './anlagen-bericht-main.component.html',
  styleUrls: ['./anlagen-bericht-main.component.scss'],
})
export class AnlagenBerichtMainComponent implements UnsavedChanges {
  @ViewChild(AnlagenBerichtDetailsComponent, { static: true })
  anlagenBerichtDetailsComponent: AnlagenBerichtDetailsComponent;

  canDeactivate(): boolean {
    return this.anlagenBerichtDetailsComponent.canDeactivate();
  }

  showSaveWarning(): void {
    this.anlagenBerichtDetailsComponent.showSaveWarning();
  }
}
