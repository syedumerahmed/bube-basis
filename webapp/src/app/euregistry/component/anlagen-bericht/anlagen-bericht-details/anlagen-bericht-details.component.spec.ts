import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { AnlagenBerichtDetailsComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-details/anlagen-bericht-details.component';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { MockComponents, MockModule, MockProvider } from 'ng-mocks';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { AnlagenBerichtAllgemeinComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-allgemein/anlagen-bericht-allgemein.component';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AnlagenBerichtGenehmigungenComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-genehmigungen/anlagen-bericht-genehmigungen.component';
import { AnlagenBerichtBvtAusnahmenComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-bvt-ausnahmen/anlagen-bericht-bvt-ausnahmen.component';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { AnlagenBerichtFeuerungsanlageComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-feuerungsanlage/anlagen-bericht-feuerungsanlage.component';
import { AnlagenBerichtSpezielleBedingungenComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-spezielle-bedingungen/anlagen-bericht-spezielle-bedingungen.component';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { AnlagenBerichtAuflagenComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-auflagen/anlagen-bericht-auflagen.component';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import {
  EuRegAnlageDto,
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
  EuRegReportEnum,
  FeuerungsanlagenTypEnum,
} from '@api/euregistry';
import { BehoerdeListItemDto, ReferenzDto } from '@api/stammdaten';
import spyOn = jest.spyOn;

describe('AnlagenBerichtDetailsComponent', () => {
  let component: AnlagenBerichtDetailsComponent;
  let fixture: ComponentFixture<AnlagenBerichtDetailsComponent>;
  let routeData$: BehaviorSubject<{
    euRegAnlage: EuRegAnlageDto;
  }>;

  const euRegBetriebsstaette: EuRegBetriebsstaetteDto = {
    berichtsdaten: undefined,
    betriebsstaetteInfo: {
      land: '02',
    },
    ersteErfassung: '',
    id: 0,
    letzteAenderung: '',
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };

  const zustaendigeBehoerde = {
    schluessel: 'SCHL',
    gueltigVon: 2000,
    gueltigBis: 2099,
  };

  const installation: EuRegAnlageDto = {
    anlagenInfo: {
      name: 'Installation',
      bimschv13: false,
      bimschv17: false,
    },
    berichtsdaten: {
      berichtsart: { schluessel: 'EuRegAnl' } as ReferenzDto,
      zustaendigeBehoerde: zustaendigeBehoerde as BehoerdeListItemDto,
      akz: 'akzakz',
      bemerkung: 'Bemerkung',
      bearbeitungsstatus: { schluessel: '01' } as ReferenzDto,
      bearbeitungsstatusSeit: '12.12.2009',
    },
    anwendbareBvt: [],
    kapitelIeRl: [],
    auflagen: [],
    bvtAusnahmen: [],
    ersteErfassung: '',
    id: 1,
    letzteAenderung: '',
    localId2: 'localId2',
    report: EuRegReportEnum.LIEGT_VOR,
    visitUrl: 'https://moin.visit',
    visits: 22,
    tehgNr: 'tehg',
    monitor: 'monitor dies',
    monitorUrl: 'https://gute-monitore-fuer-dich.com',
    eSpirsNr: 'espi',
    canWrite: true,
    genehmigung: {
      erteilt: false,
      neuBetrachtet: false,
      geaendert: false,
      erteilungsdatum: null,
      neuBetrachtetDatum: null,
      aenderungsdatum: null,
      url: '',
      durchsetzungsmassnahmen: '',
    },
  };

  const installationPart: EuRegAnlageDto = {
    ...installation,
    anlagenInfo: {
      name: 'InstallationPart',
      bimschv13: true,
      bimschv17: true,
    },
    berichtsdaten: {
      ...installation.berichtsdaten,
      berichtsart: { schluessel: 'EuRegF' } as ReferenzDto,
    },
    feuerungsanlage: {
      feuerungsanlageTyp: FeuerungsanlagenTypEnum.WI,
      gesamtKapazitaetAbfall: 0,
      kapazitaetGefAbfall: 0,
      kapazitaetNichtGefAbfall: 0,
      mehrWaermeVonGefAbfall: false,
      mitverbrennungVonUnbehandeltemAbfall: false,
      oeffentlicheBekanntmachung: 'oeffentlicheBekanntmachung',
      oeffentlicheBekanntmachungUrl: 'http://oeffentlicheBekanntmachung.url',
      ieAusnahmen: [],
      spezielleBedingungen: [],
    },
  };

  const bearbeitungsstatusListe: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];
  const kapitelIeRlListe: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];
  const anwendbareBVTListe: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];
  const behoerdenListe: Array<BehoerdeListItem> = [
    { schluessel: '01' } as BehoerdeListItem,
  ];

  const emissionswertAusnahmenListe: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];

  const spezielleBedingungArtListe: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];

  const ausnahmenArt31bis35Liste: Array<ReferenzDto> = [
    { schluessel: '01' } as ReferenzDto,
  ];

  let initialRouteData: {
    behoerdenListe: Array<BehoerdeListItem>;
    anwendbareBVTListe: Array<ReferenzDto>;
    spezielleBedingungArtListe: Array<ReferenzDto>;
    euRegBetriebsstaette: EuRegBetriebsstaetteDto;
    bearbeitungsstatusListe: Array<ReferenzDto>;
    ausnahmenArt31bis35Liste: Array<ReferenzDto>;
    emissionswertAusnahmenListe: Array<ReferenzDto>;
    euRegAnlage: EuRegAnlageDto;
    kapitelIeRlListe: Array<ReferenzDto>;
  };

  beforeEach(async () => {
    initialRouteData = {
      euRegAnlage: installationPart,
      behoerdenListe,
      bearbeitungsstatusListe,
      euRegBetriebsstaette,
      kapitelIeRlListe,
      anwendbareBVTListe,
      emissionswertAusnahmenListe,
      spezielleBedingungArtListe,
      ausnahmenArt31bis35Liste,
    };
    routeData$ = new BehaviorSubject(initialRouteData);

    const selectedBerichtsjahr = {
      id: 2018,
    } as ReferenzDto;
    await TestBed.configureTestingModule({
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(RouterTestingModule),
      ],
      declarations: [
        AnlagenBerichtDetailsComponent,
        AnlagenBerichtAllgemeinComponent,
        MockComponents(
          AnlagenBerichtGenehmigungenComponent,
          AnlagenBerichtBvtAusnahmenComponent,
          AnlagenBerichtFeuerungsanlageComponent,
          AnlagenBerichtSpezielleBedingungenComponent,
          TooltipComponent,
          ReferenzSelectComponent,
          AnlagenBerichtAuflagenComponent
        ),
      ],
      providers: [
        MockProvider(BerichtsjahrService, {
          get selectedBerichtsjahr(): ReferenzDto {
            return selectedBerichtsjahr;
          },
        }),
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProvider(EuRegistryRestControllerService),
        MockProvider(ActivatedRoute, { data: routeData$.asObservable() }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnlagenBerichtDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should have initial state', () => {
    const form = component.form;
    expect(form.enabled).toBe(true);
    expect(form.pristine).toBe(true);
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({ allgemein: { monitor: null } });
    });

    it('should be invalid', () => {
      expect(component.form.invalid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.allgemeinComponent = TestBed.createComponent(
          AnlagenBerichtAllgemeinComponent
        ).componentInstance;
        jest.spyOn(component.allgemeinComponent, 'markAllAsTouched');
        component.save();
      });

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should mark descendants as touched', () => {
        expect(
          component.allgemeinComponent.markAllAsTouched
        ).toHaveBeenCalled();
      });

      it('should not call service', inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          expect(service.updateAnlagenBericht).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.form.patchValue({
        allgemein: {
          bearbeitungsstatus: { schluessel: '01' } as ReferenzDto,
          report: EuRegReportEnum.LIEGT_NICHT_VOR,
          zustaendigeBehoerde,
          visits: 0,
          visitUrl: 'https://a.de',
          monitor: 'monitor',
        },
      });
      component.form.markAsDirty();
      component.form.markAsTouched();
    });

    it('should be valid', () => {
      expect(component.form.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      const expectedEuRegAnlDto: EuRegAnlageDto = {
        ...installationPart,
        report: EuRegReportEnum.LIEGT_NICHT_VOR,
        monitor: 'monitor',
        visits: 0,
        visitUrl: 'https://a.de',
      };
      let euRegAnlObservable: Subject<EuRegAnlageDto>;
      beforeEach(inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          euRegAnlObservable = new Subject<EuRegAnlageDto>();
          spyOn(service, 'updateAnlagenBericht').mockReturnValue(
            euRegAnlObservable.asObservable() as any
          );
          component.save();
        }
      ));

      it('should attempt save', () => {
        expect(component.saveAttempt).toBe(true);
      });

      it('should call service', inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          expect(service.updateAnlagenBericht).toHaveBeenCalledWith(
            expectedEuRegAnlDto
          );
        }
      ));

      describe('on success', () => {
        const responseDto = {
          ...expectedEuRegAnlDto,
          letzteAenderung: new Date().toISOString(),
        };
        beforeEach(() => {
          euRegAnlObservable.next(responseDto);
          euRegAnlObservable.complete();
        });

        it('should update behoerde', () => {
          expect(component.euRegAnlage).toBe(responseDto);
        });

        it('should reset form', () => {
          expect(component.form.touched).toBe(false);
          expect(component.form.dirty).toBe(false);
        });
      });
    });

    describe('when navigating to installation', () => {
      beforeEach(() => {
        routeData$.next({ ...initialRouteData, euRegAnlage: installation });
      });

      it('should be pristine', () => {
        expect(component.form.valid).toBe(true);
        expect(component.form.pristine).toBe(true);
        expect(component.form.touched).toBe(false);
      });

      it('should have no feuerungsanlage', () => {
        expect(component.form.value).not.toHaveProperty('feuerungsanlage');
      });

      describe('on reset', () => {
        beforeEach(() => {
          component.reset();
        });
        it('should still have no feuerungsanlage', () => {
          expect(component.form.value).not.toHaveProperty('feuerungsanlage');
        });
      });
    });
  });
});
