import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import {
  AnlageBerichtAllgemeinFormData,
  AnlagenBerichtAllgemeinComponent,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-allgemein/anlagen-bericht-allgemein.component';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { AlertService } from '@/base/service/alert.service';
import {
  AnlagenBerichtBvtAusnahmenComponent,
  BvtAusnahmeFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-bvt-ausnahmen/anlagen-bericht-bvt-ausnahmen.component';
import {
  AnlagenBerichtGenehmigungenComponent,
  GenehmigungFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-genehmigungen/anlagen-bericht-genehmigungen.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  AnlagenBerichtFeuerungsanlageComponent,
  FeuerungsanlageFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-feuerungsanlage/anlagen-bericht-feuerungsanlage.component';
import {
  AnlagenBerichtSpezielleBedingungenComponent,
  SpezielleBedingungFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-spezielle-bedingungen/anlagen-bericht-spezielle-bedingungen.component';
import {
  AnlagenBerichtAuflagenComponent,
  AuflageFormData,
} from '@/euregistry/component/anlagen-bericht/anlagen-bericht-auflagen/anlagen-bericht-auflagen.component';
import {
  BehoerdeListItemDto,
  EuRegAnlageDto,
  EuRegistryRestControllerService,
  GenehmigungDto,
} from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';
import { LandProperties, lookupLand } from '@/base/model/Land';
import {
  ClarityIcons,
  errorStandardIcon,
  floppyIcon,
  timesIcon,
} from '@cds/core/icon';
import { UnsavedChanges } from '@/base/routing/guard/unsaved-changes-guard.service';

export interface AnlagenBerichtFormData {
  allgemein: AnlageBerichtAllgemeinFormData;
  genehmigung: GenehmigungFormData;
  bvtAusnahmen: Array<BvtAusnahmeFormData>;
  feuerungsanlage: FeuerungsanlageFormData;
  auflagen: Array<AuflageFormData>;
}

ClarityIcons.addIcons(floppyIcon, errorStandardIcon, timesIcon);

@Component({
  selector: 'app-anlagen-bericht-details',
  templateUrl: './anlagen-bericht-details.component.html',
  styleUrls: ['./anlagen-bericht-details.component.scss'],
})
export class AnlagenBerichtDetailsComponent
  implements OnInit, OnDestroy, UnsavedChanges, AfterViewChecked
{
  allgemeinReiterActive = true;

  @ViewChild(AnlagenBerichtAllgemeinComponent)
  allgemeinComponent: AnlagenBerichtAllgemeinComponent;
  @ViewChild(AnlagenBerichtFeuerungsanlageComponent)
  feuerungsanlageComponent: AnlagenBerichtFeuerungsanlageComponent;

  form: FormGroup<AnlagenBerichtFormData>;
  saveAttempt = false;
  euRegAnlage: EuRegAnlageDto;
  land: string;
  behoerdenListe: Referenzliste<BehoerdeListItem>;
  bearbeitungsstatusListe: Referenzliste<Required<ReferenzDto>>;
  anwendbareBVTListe: Referenzliste<Required<ReferenzDto>>;
  kapitelIeRlListe: Referenzliste<Required<ReferenzDto>>;
  emissionswertAusnahmenListe: Referenzliste<Required<ReferenzDto>>;
  ausnahmenArt31bis35Liste: Referenzliste<Required<ReferenzDto>>;
  genehmigung: GenehmigungDto;

  zustaendigeBehoerde: BehoerdeListItemDto;

  spezielleBedingungArtListe: Referenzliste<Required<ReferenzDto>>;
  private relativeURL: string;

  mapToForm(dto: EuRegAnlageDto): AnlagenBerichtFormData {
    return {
      allgemein: AnlagenBerichtAllgemeinComponent.toForm(
        dto,
        this.behoerdenListe,
        this.anwendbareBVTListe,
        this.kapitelIeRlListe
      ),
      genehmigung: AnlagenBerichtGenehmigungenComponent.toForm(dto.genehmigung),
      bvtAusnahmen: AnlagenBerichtBvtAusnahmenComponent.toForm(
        dto.bvtAusnahmen
      ),
      auflagen: AnlagenBerichtAuflagenComponent.toForm(dto.auflagen),
      feuerungsanlage: AnlagenBerichtFeuerungsanlageComponent.toForm(
        dto.feuerungsanlage
      ),
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly alertService: AlertService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly service: EuRegistryRestControllerService
  ) {}

  ngOnInit(): void {
    this.berichtsjahrService.disableDropdown();
    this.route.data.subscribe((data) => {
      this.allgemeinReiterActive = true;
      this.land = data.euRegBetriebsstaette.betriebsstaetteInfo.land;
      this.behoerdenListe = new Referenzliste(data.behoerdenListe);
      this.bearbeitungsstatusListe = new Referenzliste(
        data.bearbeitungsstatusListe
      );

      this.anwendbareBVTListe = new Referenzliste(data.anwendbareBVTListe);
      this.kapitelIeRlListe = new Referenzliste(data.kapitelIeRlListe);
      this.emissionswertAusnahmenListe = new Referenzliste(
        data.emissionswertAusnahmenListe
      );
      this.spezielleBedingungArtListe = new Referenzliste(
        data.spezielleBedingungArtListe
      );
      this.ausnahmenArt31bis35Liste = new Referenzliste(
        data.ausnahmenArt31bis35Liste
      );

      this.euRegAnlage = data.euRegAnlage;
      this.zustaendigeBehoerde =
        this.euRegAnlage.berichtsdaten.zustaendigeBehoerde;

      if (!this.euRegAnlage) {
        throw Error('euRegAnlage === ' + this.euRegAnlage);
      }

      this.form = this.createForm(this.mapToForm(this.euRegAnlage));

      if (this.euRegAnlage.anlagenInfo.parentAnlageHatEuRegistryBericht) {
        this.relativeURL =
          'anlage/nummer/' + this.route.snapshot.params.anlagenNummer;
      } else {
        this.relativeURL = '.';
      }
    });
  }

  ngAfterViewChecked(): void {
    this.form?.markAsTouched();
  }

  ngOnDestroy(): void {
    this.berichtsjahrService.restoreDropdownDefault();
  }

  getNavigateBackText(): string {
    if (this.euRegAnlage.anlagenInfo.parentAnlageHatEuRegistryBericht) {
      return 'zur Anlage';
    }
    return 'zum Betriebsstättenbericht';
  }

  navigateBackToParent(): void {
    this.router.navigate([this.relativeURL], {
      relativeTo: this.route.parent,
    });
  }

  get canWrite(): boolean {
    return this.euRegAnlage?.canWrite;
  }

  get displayLand(): string {
    return LandProperties[lookupLand(this.land)].display;
  }

  get allgemeinForm(): FormGroup<AnlageBerichtAllgemeinFormData> {
    return this.form.controls
      .allgemein as FormGroup<AnlageBerichtAllgemeinFormData>;
  }

  reset(): void {
    this.saveAttempt = false;
    const originalBericht = this.mapToForm(this.euRegAnlage);
    this.form.reset(originalBericht);
    this.form.setControl(
      'bvtAusnahmen',
      AnlagenBerichtBvtAusnahmenComponent.createForm(
        this.fb,
        originalBericht.bvtAusnahmen,
        this.berichtsjahrService.selectedBerichtsjahr
      )
    );
    this.form.setControl(
      'auflagen',
      AnlagenBerichtAuflagenComponent.createForm(
        this.fb,
        originalBericht.auflagen,
        this.berichtsjahrService.selectedBerichtsjahr
      )
    );

    if (this.hasFeuerungsanlage && this.canWrite) {
      this.feuerungsanlageForm.setControl(
        'spezielleBedingungen',
        AnlagenBerichtSpezielleBedingungenComponent.createForm(
          this.fb,
          originalBericht.feuerungsanlage?.spezielleBedingungen ?? [],
          this.berichtsjahrService.selectedBerichtsjahr
        )
      );
      this.feuerungsanlageForm.enable();
    }
  }

  get feuerungsanlageForm(): FormGroup<FeuerungsanlageFormData> {
    return this.form.controls
      .feuerungsanlage as FormGroup<FeuerungsanlageFormData>;
  }

  get hasFeuerungsanlage(): boolean {
    return this.hasBImSCHV13 || this.hasBImSCHV17;
  }

  get genehmigungForm(): FormGroup<GenehmigungFormData> {
    return this.form.get('genehmigung') as FormGroup<GenehmigungFormData>;
  }

  get bvtAusnahmenForm(): FormArray<BvtAusnahmeFormData> {
    return this.form.get('bvtAusnahmen') as FormArray<BvtAusnahmeFormData>;
  }

  get auflagenForm(): FormArray<AuflageFormData> {
    return this.form.get('auflagen') as FormArray<AuflageFormData>;
  }

  get spezielleBedingungenForm(): FormArray<SpezielleBedingungFormData> {
    return this.feuerungsanlageForm.controls
      .spezielleBedingungen as FormArray<SpezielleBedingungFormData>;
  }

  save(): void {
    this.saveAttempt = true;
    if (this.form.invalid) {
      this.allgemeinComponent?.markAllAsTouched();
      this.feuerungsanlageComponent?.markAllAsTouched();
      return;
    }
    const formData = this.form.value;
    const euRegAnlageDto: EuRegAnlageDto = {
      ...this.euRegAnlage,
      visits: formData.allgemein.visits,
      monitor: formData.allgemein.monitor,
      eSpirsNr: formData.allgemein.eSpirsNr,
      tehgNr: formData.allgemein.tehgNr,
      monitorUrl: formData.allgemein.monitorUrl,
      visitUrl: formData.allgemein.visitUrl,
      report: formData.allgemein.report,
      anwendbareBvt: formData.allgemein.anwendbareBVTListe,
      kapitelIeRl: formData.allgemein.kapitelIeRlListe,
      localId2: formData.allgemein.localId2,
      bvtAusnahmen: formData.bvtAusnahmen.map((ausnahme) => {
        return {
          ...ausnahme,
          beginntAm: FormUtil.toIsoDate(ausnahme.beginntAm),
          endetAm: FormUtil.toIsoDate(ausnahme.endetAm),
        };
      }),
      genehmigung: {
        erteilt: formData.genehmigung.erteilt,
        neuBetrachtet: formData.genehmigung.neuBetrachtet,
        geaendert: formData.genehmigung.geaendert,
        erteilungsdatum: FormUtil.toIsoDate(
          formData.genehmigung.erteilungsdatum
        ),
        neuBetrachtetDatum: FormUtil.toIsoDate(
          formData.genehmigung.betrachtungsdatum
        ),
        aenderungsdatum: FormUtil.toIsoDate(
          formData.genehmigung.aenderungsdatum
        ),
        url: formData.genehmigung.url,
        durchsetzungsmassnahmen: formData.genehmigung.durchsetzungsmassnahmen,
      },
      feuerungsanlage: formData.feuerungsanlage,
      berichtsdaten: {
        ...this.euRegAnlage.berichtsdaten,
        akz: formData.allgemein.akz,
        bemerkung: formData.allgemein.bemerkung,
        zustaendigeBehoerde: formData.allgemein.zustaendigeBehoerde,
        bearbeitungsstatus: formData.allgemein.bearbeitungsstatus,
      },
      auflagen: formData.auflagen,
    };

    this.service.updateAnlagenBericht(euRegAnlageDto).subscribe(
      (bericht) => {
        this.euRegAnlage = bericht;
        this.reset();
        this.alertService.clearAlertMessages();
      },
      (err) => this.alertService.setAlert(AlertService.getAlertText(err))
    );
  }

  get isAnlageBericht(): boolean {
    return this.euRegAnlage?.anlagenInfo.parentAnlageId == null;
  }

  private createForm(
    initialAnlagenBericht: AnlagenBerichtFormData
  ): FormGroup<AnlagenBerichtFormData> {
    const form = this.fb.group<AnlagenBerichtFormData>({
      allgemein: AnlagenBerichtAllgemeinComponent.createForm(
        this.fb,
        initialAnlagenBericht.allgemein,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      genehmigung: AnlagenBerichtGenehmigungenComponent.createForm(
        this.fb,
        initialAnlagenBericht.genehmigung
      ),
      bvtAusnahmen: AnlagenBerichtBvtAusnahmenComponent.createForm(
        this.fb,
        initialAnlagenBericht.bvtAusnahmen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      feuerungsanlage: AnlagenBerichtFeuerungsanlageComponent.createForm(
        this.fb,
        initialAnlagenBericht.feuerungsanlage,
        this.hasBImSCHV17,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
      auflagen: AnlagenBerichtAuflagenComponent.createForm(
        this.fb,
        initialAnlagenBericht.auflagen,
        this.berichtsjahrService.selectedBerichtsjahr
      ),
    });

    if (!this.canWrite) {
      form.disable();
    }

    if (!this.hasFeuerungsanlage) {
      form.controls.feuerungsanlage.disable();
    }

    return form;
  }

  get hasBImSCHV13(): boolean {
    return this.euRegAnlage?.anlagenInfo.bimschv13;
  }

  get hasBImSCHV17(): boolean {
    return this.euRegAnlage?.anlagenInfo.bimschv17;
  }

  canDeactivate(): boolean {
    return this.form.pristine;
  }

  showSaveWarning(): void {
    this.alertService.setSaveWarning();
  }
}
