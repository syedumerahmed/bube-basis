import { AfterViewChecked, Component, Input, ViewChild } from '@angular/core';
import { ClrForm } from '@clr/angular';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { Validators } from '@angular/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BubeValidators } from '@/base/forms/bube-validators';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { AuflageDto } from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface AuflageFormData {
  emissionswert: ReferenzDto;
  nachArt18: boolean;
  nachArt14: boolean;
}

ClarityIcons.addIcons(errorStandardIcon, noteIcon, trashIcon, plusIcon);
@Component({
  selector: 'app-anlagen-bericht-auflagen',
  templateUrl: './anlagen-bericht-auflagen.component.html',
  styleUrls: ['./anlagen-bericht-auflagen.component.scss'],
})
export class AnlagenBerichtAuflagenComponent implements AfterViewChecked {
  constructor(
    private readonly fb: FormBuilder,
    private readonly benutzer: BenutzerProfil,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;
  auflageForm: FormGroup<AuflageFormData>;

  @Input()
  formArray: FormArray<AuflageFormData>;

  @Input()
  emissionswertListe: Referenzliste<Required<ReferenzDto>>;
  editModalOpen = false;
  editExistingIndex: number = null;

  get canDelete(): boolean {
    return this.benutzer.canWriteStammdaten;
  }

  static toForm(auflagen: Array<AuflageDto>): Array<AuflageFormData> {
    return (auflagen ?? []).map((auflage) => ({
      emissionswert: auflage.emissionswert,
      nachArt14: auflage.nachArt14,
      nachArt18: auflage.nachArt18,
    }));
  }

  static createForm(
    fb: FormBuilder,
    auflagen: Array<AuflageFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<AuflageFormData> {
    return fb.array<AuflageFormData>(
      auflagen.map((auflage) => {
        return fb.group({
          emissionswert: [
            auflage.emissionswert,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
          nachArt14: auflage.nachArt14,
          nachArt18: auflage.nachArt18,
        });
      })
    );
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  private createEmptyForm(): FormGroup<AuflageFormData> {
    return this.fb.group({
      emissionswert: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      nachArt14: false,
      nachArt18: false,
    });
  }

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.auflageForm = null;
  }

  uebernehmen(): void {
    if (this.auflageForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.auflageForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.auflageForm)) {
        this.formArray.push(this.auflageForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.auflageForm = null;
    }
  }

  private openEdit(formGroup: FormGroup<AuflageFormData>): void {
    this.auflageForm = formGroup;
    this.editModalOpen = true;
  }
}
