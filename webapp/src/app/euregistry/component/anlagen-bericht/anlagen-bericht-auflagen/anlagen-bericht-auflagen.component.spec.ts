import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  AnlagenBerichtAuflagenComponent,
  AuflageFormData,
} from './anlagen-bericht-auflagen.component';
import { BehaviorSubject } from 'rxjs';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';

describe('AnlagenBerichtAuflagenComponent', () => {
  let component: AnlagenBerichtAuflagenComponent;
  let fixture: ComponentFixture<AnlagenBerichtAuflagenComponent>;

  let routerParams$: BehaviorSubject<{
    land: string;
    berichtsjahr: string;
  }>;

  const emissionswertListe = new Referenzliste([
    {
      id: 1,
      schluessel: '01',
      ktext: '01',
      land: '00',
      sortier: '01',
      gueltigVon: 2007,
      gueltigBis: 2099,
      referenzliste: ReferenzlisteEnum.RBATE,
    },
    {
      id: 2,
      schluessel: '02',
      ktext: '02',
      land: '00',
      sortier: '02',
      gueltigVon: 2007,
      gueltigBis: 2099,
      referenzliste: ReferenzlisteEnum.RBATE,
    },
  ]);

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  beforeEach(async () => {
    routerParams$ = new BehaviorSubject({
      land: '00',
      berichtsjahr: '2020',
    });
    await TestBed.configureTestingModule({
      declarations: [AnlagenBerichtAuflagenComponent, TooltipComponent],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(RouterTestingModule),
      ],
      providers: [
        MockProviders(BenutzerProfil, BerichtsjahrService),
        {
          provide: ActivatedRoute,
          useValue: {
            params: routerParams$.asObservable(),
          },
        },
      ],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnlagenBerichtAuflagenComponent);
      component = fixture.componentInstance;
      component.emissionswertListe = emissionswertListe as Referenzliste<
        Required<ReferenzDto>
      >;
      component.formArray = AnlagenBerichtAuflagenComponent.createForm(
        fb,
        AnlagenBerichtAuflagenComponent.toForm([]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnlagenBerichtAuflagenComponent);
      component = fixture.componentInstance;
      component.emissionswertListe = emissionswertListe as Referenzliste<
        Required<ReferenzDto>
      >;
      component.formArray = AnlagenBerichtAuflagenComponent.createForm(
        fb,
        AnlagenBerichtAuflagenComponent.toForm([
          {
            emissionswert: emissionswertListe.values[0],
            nachArt14: false,
            nachArt18: false,
          },
          {
            emissionswert: emissionswertListe.values[1],
            nachArt14: true,
            nachArt18: true,
          },
        ]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.auflageForm).toBeTruthy();
        expect(component.auflageForm.value).toStrictEqual({
          emissionswert: null,
          nachArt14: false,
          nachArt18: false,
        });
        expect(component.auflageForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let patchedValue: AuflageFormData;
        beforeEach(() => {
          patchedValue = {
            emissionswert: emissionswertListe.values[0],
            nachArt14: true,
            nachArt18: true,
          };
          component.auflageForm.patchValue(patchedValue);
        });
        it('should be valid', () => {
          expect(component.auflageForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null kommunikationForm', () => {
            expect(component.auflageForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null auflageform', () => {
            expect(component.auflageForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.auflageForm).toBeTruthy();
        expect(component.auflageForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.auflageForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: AuflageFormData;
        let patchedValue: AuflageFormData;
        beforeEach(() => {
          originalValue = component.auflageForm.value;
          patchedValue = {
            emissionswert: emissionswertListe.values[1],
            nachArt14: false,
            nachArt18: true,
          };
          component.auflageForm.patchValue(patchedValue);
          component.auflageForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.auflageForm.patchValue(patchedValue);
            component.auflageForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null form', () => {
            expect(component.auflageForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();

            expect(component.formArray.dirty).toBe(true);
          });

          it('should null auflageform', () => {
            expect(component.auflageForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: AuflageFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
      });
    });
  });
});
