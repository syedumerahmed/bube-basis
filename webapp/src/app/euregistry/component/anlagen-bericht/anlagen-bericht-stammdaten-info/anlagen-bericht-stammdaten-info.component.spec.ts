import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnlagenBerichtStammdatenInfoComponent } from './anlagen-bericht-stammdaten-info.component';
import { MockComponents, MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { BehaviorSubject } from 'rxjs';
import {
  EuRegAnlageDto,
  EuRegBetriebsstaetteDto,
  FeuerungsanlagenTypEnum,
} from '@api/euregistry';

describe('AnlagenBerichtStammdatenInfoComponent', () => {
  let component: AnlagenBerichtStammdatenInfoComponent;
  let fixture: ComponentFixture<AnlagenBerichtStammdatenInfoComponent>;
  let routeData$: BehaviorSubject<{
    euRegBetriebsstaette: EuRegBetriebsstaetteDto;
    euRegAnlage: EuRegAnlageDto;
  }>;

  const euRegBst: EuRegBetriebsstaetteDto = {
    berichtsdaten: null,
    betriebsstaetteInfo: {
      berichtsjahr: '2020',
      land: '02',
      betriebsstaetteNr: 'bst-nr',
    },
    ersteErfassung: null,
    id: 123,
    letzteAenderung: null,
    pruefungsfehler: false,
    pruefungVorhanden: false,
  };
  const euRegAnlage: EuRegAnlageDto = {
    anlagenInfo: {
      anlageNr: 'anl-nr',
    },
    berichtsdaten: null,
    ersteErfassung: '',
    id: 1,
    letzteAenderung: '',
    report: null,
    visitUrl: '',
    monitor: '',
    visits: 0,
    genehmigung: {
      erteilt: false,
      neuBetrachtet: false,
      geaendert: false,
      erteilungsdatum: null,
      neuBetrachtetDatum: null,
      aenderungsdatum: null,
      url: '',
      durchsetzungsmassnahmen: '',
    },
    feuerungsanlage: {
      feuerungsanlageTyp: FeuerungsanlagenTypEnum.WI,
      gesamtKapazitaetAbfall: 0,
      kapazitaetGefAbfall: 0,
      kapazitaetNichtGefAbfall: 0,
      mehrWaermeVonGefAbfall: false,
      mitverbrennungVonUnbehandeltemAbfall: false,
      oeffentlicheBekanntmachung: 'oeffentlicheBekanntmachung',
    },
  };

  beforeEach(async () => {
    routeData$ = new BehaviorSubject({
      euRegBetriebsstaette: euRegBst,
      euRegAnlage: euRegAnlage,
    });

    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtStammdatenInfoComponent,
        MockComponents(TooltipComponent),
      ],
      imports: [MockModule(ClarityModule), RouterTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: routeData$.asObservable(),
            params: new BehaviorSubject({
              anlagenNummer: '100',
            }).asObservable(),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnlagenBerichtStammdatenInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
