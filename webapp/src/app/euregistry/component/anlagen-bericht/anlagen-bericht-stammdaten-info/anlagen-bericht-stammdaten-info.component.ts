import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnlagenInfoDto, BetriebsstaetteInfoDto } from '@api/euregistry';
import { buildingIcon, ClarityIcons } from '@cds/core/icon';

ClarityIcons.addIcons(buildingIcon);
@Component({
  selector: 'app-anlagen-bericht-stammdaten-info',
  templateUrl: './anlagen-bericht-stammdaten-info.component.html',
  styleUrls: ['./anlagen-bericht-stammdaten-info.component.scss'],
})
export class AnlagenBerichtStammdatenInfoComponent implements OnInit {
  betriebsstaettenInfos: BetriebsstaetteInfoDto;
  anlagenInfos: AnlagenInfoDto;
  anlagenNummer: string;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.betriebsstaettenInfos =
        data.euRegBetriebsstaette.betriebsstaetteInfo;
      this.anlagenInfos = data.euRegAnlage.anlagenInfo;
    });

    this.route.params.subscribe((params) => {
      this.anlagenNummer = params.anlagenNummer;
    });
  }

  get isAnlagenBericht(): boolean {
    return this.anlagenInfos.parentBetriebsstaetteId != null;
  }
}
