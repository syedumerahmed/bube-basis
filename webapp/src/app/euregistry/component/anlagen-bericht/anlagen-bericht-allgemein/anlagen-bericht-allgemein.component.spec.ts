import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { MockComponents, MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder } from '@/base/forms';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { AnlagenBerichtAllgemeinComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-allgemein/anlagen-bericht-allgemein.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { ReferenzSelectComponent } from '@/basedomain/component/referenz-select/referenz-select.component';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import {
  EuRegAnlageDto,
  EuRegReportEnum,
  FeuerungsanlagenTypEnum,
} from '@api/euregistry';
import { BehoerdeListItemDto, ReferenzDto } from '@api/stammdaten';

describe('AnlagenBerichtAllgemeinComponent', () => {
  const zustaendigeBehoerde = { id: 1, schluessel: 'SCHL' };
  const euRegAnlage: EuRegAnlageDto = {
    anlagenInfo: null,
    berichtsdaten: {
      berichtsart: { schluessel: '01' } as ReferenzDto,
      zustaendigeBehoerde: zustaendigeBehoerde as BehoerdeListItemDto,
      akz: 'akzakz',
      bemerkung: 'Bemerkung',
      bearbeitungsstatus: { schluessel: '01' } as ReferenzDto,
      bearbeitungsstatusSeit: '2020-02-02',
    },
    anwendbareBvt: [],
    kapitelIeRl: [],
    localId2: 'localId2',
    ersteErfassung: '',
    id: 1,
    letzteAenderung: '',
    report: EuRegReportEnum.LIEGT_VOR,
    visitUrl: 'https://moin.visit',
    visits: 22,
    tehgNr: 'tehg',
    monitor: 'monitor dies',
    monitorUrl: 'https://gute-monitore-fuer-dich.com',
    eSpirsNr: 'espi',

    genehmigung: {
      erteilt: false,
      neuBetrachtet: false,
      geaendert: false,
      erteilungsdatum: null,
      neuBetrachtetDatum: null,
      aenderungsdatum: null,
      url: '',
      durchsetzungsmassnahmen: '',
    },
    feuerungsanlage: {
      feuerungsanlageTyp: FeuerungsanlagenTypEnum.WI,
      gesamtKapazitaetAbfall: 0,
      kapazitaetGefAbfall: 0,
      kapazitaetNichtGefAbfall: 0,
      mehrWaermeVonGefAbfall: false,
      mitverbrennungVonUnbehandeltemAbfall: false,
      oeffentlicheBekanntmachung: 'oeffentlicheBekanntmachung',
      ieAusnahmen: [],
      oeffentlicheBekanntmachungUrl: 'oeffentlicheBekanntmachungUrl',
    },
  };

  const behoerdenListe = new Referenzliste<BehoerdeListItem>([
    zustaendigeBehoerde as BehoerdeListItem,
  ]);
  const anwendbareBVTListe = new Referenzliste<Required<ReferenzDto>>([]);
  const kapitelIeRlListe = new Referenzliste<Required<ReferenzDto>>([]);
  const bearbeitungsstatusListe = new Referenzliste<Required<ReferenzDto>>([]);

  let component: AnlagenBerichtAllgemeinComponent;
  let fixture: ComponentFixture<AnlagenBerichtAllgemeinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtAllgemeinComponent,
        MockComponents(TooltipComponent, ReferenzSelectComponent),
      ],
      imports: [MockModule(ReactiveFormsModule), MockModule(ClarityModule)],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(AnlagenBerichtAllgemeinComponent);
    component = fixture.componentInstance;
    component.form = AnlagenBerichtAllgemeinComponent.createForm(
      formBuilder,
      AnlagenBerichtAllgemeinComponent.toForm(
        euRegAnlage,
        behoerdenListe,
        anwendbareBVTListe,
        kapitelIeRlListe
      ),
      {
        gueltigBis: 0,
        gueltigVon: 0,
        ktext: '',
        land: '',
        referenzliste: undefined,
        sortier: '',
        schluessel: '2020',
      }
    );
    component.behoerdenListe = behoerdenListe;
    component.anwendbareBvtListe = anwendbareBVTListe;
    component.kapitelIeRlListe = kapitelIeRlListe;
    component.bearbeitungsstatusListe = bearbeitungsstatusListe;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      AnlagenBerichtAllgemeinComponent.toForm(
        euRegAnlage,
        behoerdenListe,
        anwendbareBVTListe,
        kapitelIeRlListe
      )
    );
  });
});
