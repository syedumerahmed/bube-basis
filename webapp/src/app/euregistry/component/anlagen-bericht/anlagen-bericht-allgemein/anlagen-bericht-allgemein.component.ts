import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BehoerdeListItem } from '@/referenzdaten/model/behoerdeListItem';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { EuRegAnlageDto, EuRegReportEnum } from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';

export interface AnlageBerichtAllgemeinFormData {
  zustaendigeBehoerde: BehoerdeListItem;
  akz: string;
  bearbeitungsstatus: ReferenzDto;
  report: EuRegReportEnum;
  visits: number;
  visitUrl: string;
  eSpirsNr: string;
  tehgNr: string;
  monitor: string;
  monitorUrl: string;
  kapitelIeRlListe: Array<ReferenzDto>;
  anwendbareBVTListe: Array<ReferenzDto>;
  bemerkung: string;
  localId2: string;
}

@Component({
  selector: 'app-anlagen-bericht-allgemein',
  templateUrl: './anlagen-bericht-allgemein.component.html',
  styleUrls: ['./anlagen-bericht-allgemein.component.scss'],
})
export class AnlagenBerichtAllgemeinComponent
  implements AfterViewInit, AfterViewChecked
{
  @Input()
  form: FormGroup<AnlageBerichtAllgemeinFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  behoerdenListe: Referenzliste<BehoerdeListItem>;
  @Input()
  bearbeitungsstatusListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  kapitelIeRlListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  anwendbareBvtListe: Referenzliste<Required<ReferenzDto>>;
  @Input()
  bearbeitungsstatusSeit: string;

  @ViewChild(ClrForm)
  clrForm: ClrForm;

  readonly reportListe: Array<EuRegReportEnum> = Object.values(EuRegReportEnum);

  readonly reportLabels: Record<EuRegReportEnum, string> = {
    LIEGT_NICHT_VOR: 'Liegt nicht vor',
    LIEGT_VOR: 'Liegt vor',
    NICHT_ERFORDERLICH: 'Nicht erforderlich',
  };

  static toForm(
    euRegAnlageDto: EuRegAnlageDto,
    behoerdenListe: Referenzliste<BehoerdeListItem>,
    anwendbareBVTListe: Referenzliste<Required<ReferenzDto>>,
    kapitelIeRlListe: Referenzliste<Required<ReferenzDto>>
  ): AnlageBerichtAllgemeinFormData {
    const {
      eSpirsNr,
      monitor,
      monitorUrl,
      report,
      tehgNr,
      visitUrl,
      visits,
      localId2,
    } = euRegAnlageDto;
    return {
      eSpirsNr,
      monitor,
      monitorUrl,
      report,
      tehgNr,
      visitUrl,
      visits,
      akz: euRegAnlageDto.berichtsdaten.akz,
      bearbeitungsstatus: euRegAnlageDto.berichtsdaten.bearbeitungsstatus,
      zustaendigeBehoerde: behoerdenListe.find(
        new BehoerdeListItem(euRegAnlageDto.berichtsdaten.zustaendigeBehoerde)
      ),
      bemerkung: euRegAnlageDto.berichtsdaten.bemerkung,
      anwendbareBVTListe: anwendbareBVTListe.map(
        euRegAnlageDto.anwendbareBvt as Array<Required<ReferenzDto>>
      ),
      kapitelIeRlListe: kapitelIeRlListe.map(
        euRegAnlageDto.kapitelIeRl as Array<Required<ReferenzDto>>
      ),
      localId2,
    };
  }

  static createForm(
    fb: FormBuilder,
    bericht: AnlageBerichtAllgemeinFormData,
    selectedBerichtsJahr: ReferenzDto
  ): FormGroup<AnlageBerichtAllgemeinFormData> {
    return fb.group<AnlageBerichtAllgemeinFormData>({
      eSpirsNr: [bericht.eSpirsNr, [Validators.maxLength(10)]],
      monitor: [
        bericht.monitor,
        [Validators.required, Validators.maxLength(250)],
      ],
      monitorUrl: [
        bericht.monitorUrl,
        [Validators.maxLength(500), BubeValidators.url],
      ],
      report: [
        bericht.report,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      tehgNr: [bericht.tehgNr, [Validators.maxLength(20)]],
      visitUrl: [
        bericht.visitUrl,
        [Validators.required, Validators.maxLength(500), BubeValidators.url],
      ],
      visits: [
        bericht.visits,
        [Validators.required, Validators.min(0), Validators.max(99)],
      ],
      zustaendigeBehoerde: [
        bericht.zustaendigeBehoerde,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      akz: [bericht.akz, [Validators.maxLength(14)]],
      bearbeitungsstatus: [
        bericht.bearbeitungsstatus,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      bemerkung: [bericht.bemerkung, [Validators.maxLength(1000)]],
      kapitelIeRlListe: [
        bericht.kapitelIeRlListe,
        [
          BubeValidators.areReferencesValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      anwendbareBVTListe: [
        bericht.anwendbareBVTListe,
        [
          BubeValidators.areReferencesValid(
            Number(selectedBerichtsJahr.schluessel)
          ),
        ],
      ],
      localId2: [
        bericht.localId2,
        [Validators.required, Validators.maxLength(50)],
      ],
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm?.markAsTouched();
  }

  joinInvalidReferences(invalidReferences: Array<ReferenzDto>): string {
    return invalidReferences.map((r) => r.ktext).join(', ');
  }
}
