import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  AnlagenBerichtSpezielleBedingungenComponent,
  SpezielleBedingungFormData,
} from './anlagen-bericht-spezielle-bedingungen.component';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { AlertService } from '@/base/service/alert.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import { FormBuilder } from '@/base/forms';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import spyOn = jest.spyOn;

const spezielleBedingungen = new Referenzliste([
  {
    id: 1,
    schluessel: '01',
    ktext: '01',
    land: '00',
    sortier: '01',
    gueltigVon: 2007,
    gueltigBis: 2099,
    referenzliste: ReferenzlisteEnum.RSPECC,
  },
  {
    id: 2,
    schluessel: '02',
    ktext: '02',
    land: '00',
    sortier: '02',
    gueltigVon: 2007,
    gueltigBis: 2099,
    referenzliste: ReferenzlisteEnum.RSPECC,
  },
]);

const currentBerichtsjahr: ReferenzDto = {
  gueltigBis: 0,
  gueltigVon: 0,
  ktext: '',
  land: '',
  referenzliste: null,
  schluessel: '2020',
  sortier: '20',
  id: 20,
};

describe('AnlagenBerichtSpezielleBedingungenComponent', () => {
  let component: AnlagenBerichtSpezielleBedingungenComponent;
  let fixture: ComponentFixture<AnlagenBerichtSpezielleBedingungenComponent>;
  let routrParams$: BehaviorSubject<{
    land: string;
    berichtsjahr: string;
  }>;

  beforeEach(async () => {
    routrParams$ = new BehaviorSubject({
      land: '00',
      berichtsjahr: '2020',
    });

    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtSpezielleBedingungenComponent,
        MockComponents(TooltipComponent),
      ],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(RouterTestingModule),
      ],
      providers: [
        MockProviders(
          BenutzerProfil,
          AlertService,
          BerichtsjahrService,
          ParameterService
        ),
        {
          provide: ActivatedRoute,
          useValue: {
            params: routrParams$.asObservable(),
          },
        },
      ],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  beforeEach(inject([ParameterService], (parameterService) => {
    spyOn(
      parameterService,
      'readParameterWertBehoerdeOptional'
    ).mockReturnValue(EMPTY);
  }));

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(
        AnlagenBerichtSpezielleBedingungenComponent
      );
      component = fixture.componentInstance;
      component.spezielleBedingungArtListe =
        spezielleBedingungen as Referenzliste<Required<ReferenzDto>>;
      component.formArray =
        AnlagenBerichtSpezielleBedingungenComponent.createForm(
          fb,
          AnlagenBerichtSpezielleBedingungenComponent.toForm([]),
          {
            gueltigBis: 0,
            gueltigVon: 0,
            ktext: '',
            land: '',
            referenzliste: undefined,
            sortier: '',
            schluessel: '2020',
          }
        );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(
        AnlagenBerichtSpezielleBedingungenComponent
      );
      component = fixture.componentInstance;
      component.spezielleBedingungArtListe =
        spezielleBedingungen as Referenzliste<Required<ReferenzDto>>;
      component.formArray =
        AnlagenBerichtSpezielleBedingungenComponent.createForm(
          fb,
          AnlagenBerichtSpezielleBedingungenComponent.toForm([
            {
              art: spezielleBedingungen.values[0],
              info: 'Info 1',
              genehmigungUrl: 'http://wps.de',
            },
            {
              art: spezielleBedingungen.values[1],
              info: 'Info 2',
              genehmigungUrl: 'http://wps2.de',
            },
          ]),
          {
            gueltigBis: 0,
            gueltigVon: 0,
            ktext: '',
            land: '',
            referenzliste: undefined,
            sortier: '',
            schluessel: '2020',
          }
        );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.spezielleBedingungForm).toBeTruthy();
        expect(component.spezielleBedingungForm.value).toStrictEqual({
          art: null,
          info: null,
          genehmigungUrl: null,
        });
        expect(component.spezielleBedingungForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let patchedValue: SpezielleBedingungFormData;
        beforeEach(() => {
          patchedValue = {
            art: spezielleBedingungen.values[0],
            info: 'Info 3',
            genehmigungUrl: 'http://wps3.de',
          };
          component.spezielleBedingungForm.patchValue(patchedValue);
        });
        it('should be valid', () => {
          expect(component.spezielleBedingungForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null Form', () => {
            expect(component.spezielleBedingungForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null Form', () => {
            expect(component.spezielleBedingungForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.spezielleBedingungForm).toBeTruthy();
        expect(component.spezielleBedingungForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.spezielleBedingungForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: SpezielleBedingungFormData;
        let patchedValue: SpezielleBedingungFormData;
        beforeEach(() => {
          originalValue = component.spezielleBedingungForm.value;
          patchedValue = {
            art: spezielleBedingungen.values[1],
            info: 'Info 4',
            genehmigungUrl: 'http://wps4.de',
          };
          component.spezielleBedingungForm.patchValue(patchedValue);
          component.spezielleBedingungForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.spezielleBedingungForm.patchValue(patchedValue);
            component.spezielleBedingungForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null form', () => {
            expect(component.spezielleBedingungForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();

            expect(component.formArray.dirty).toBe(true);
          });

          it('should null Form', () => {
            expect(component.spezielleBedingungForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: SpezielleBedingungFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
      });
    });
  });
});
