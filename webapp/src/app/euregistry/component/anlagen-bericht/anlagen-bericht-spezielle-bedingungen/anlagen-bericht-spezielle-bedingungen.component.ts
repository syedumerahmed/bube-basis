import {
  AfterViewChecked,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@/base/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import {
  BehoerdeListItemDto,
  SpezielleBedingungIE51Dto,
} from '@api/euregistry';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';
import { ParameterService } from '@/referenzdaten/service/parameter.service';

export interface SpezielleBedingungFormData {
  art: ReferenzDto;
  info: string;
  genehmigungUrl: string;
}

ClarityIcons.addIcons(errorStandardIcon, noteIcon, trashIcon, plusIcon);

@Component({
  selector: 'app-anlagen-bericht-spezielle-bedingungen',
  templateUrl: './anlagen-bericht-spezielle-bedingungen.component.html',
  styleUrls: ['./anlagen-bericht-spezielle-bedingungen.component.scss'],
})
export class AnlagenBerichtSpezielleBedingungenComponent
  implements AfterViewChecked, OnInit
{
  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly alertService: AlertService,
    private readonly benutzer: BenutzerProfil,
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly parameterService: ParameterService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;
  spezielleBedingungForm: FormGroup<SpezielleBedingungFormData>;

  @Input()
  formArray: FormArray<SpezielleBedingungFormData>;

  @Input()
  spezielleBedingungArtListe: Referenzliste<Required<ReferenzDto>>;

  @Input()
  zustaendigeBehoerde: BehoerdeListItemDto;

  editModalOpen = false;
  editExistingIndex: number = null;

  spezielleBedingungUrlInit = null;

  static toForm(
    spezielleBedingungen: Array<SpezielleBedingungIE51Dto>
  ): Array<SpezielleBedingungFormData> {
    return (spezielleBedingungen ?? []).map((bedingung) => ({
      art: bedingung.art,
      info: bedingung.info,
      genehmigungUrl: bedingung.genehmigungUrl,
    }));
  }

  static createForm(
    fb: FormBuilder,
    spezielleBedingungen: Array<SpezielleBedingungFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<SpezielleBedingungFormData> {
    return fb.array<SpezielleBedingungFormData>(
      spezielleBedingungen.map((bedingung) => {
        return fb.group({
          art: [
            bedingung.art,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
          info: [bedingung.info, [Validators.maxLength(1000)]],
          genehmigungUrl: [
            bedingung.genehmigungUrl,
            [
              Validators.required,
              Validators.maxLength(500),
              BubeValidators.url,
            ],
          ],
        });
      })
    );
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const land = params.land;
      const berichtsjahr = params.berichtsjahr;
      this.parameterService
        .readParameterWertBehoerdeOptional(
          'url_spcond',
          berichtsjahr,
          land,
          this.zustaendigeBehoerde.id
        )
        .subscribe(
          (parameterWert) =>
            (this.spezielleBedingungUrlInit = parameterWert?.wert),
          (error) => this.alertService.setAlert(error)
        );
    });
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  private createEmptyForm(): FormGroup<SpezielleBedingungFormData> {
    return this.fb.group({
      art: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      info: [null, [Validators.maxLength(1000)]],
      genehmigungUrl: [
        this.spezielleBedingungUrlInit,
        [Validators.required, Validators.maxLength(500), BubeValidators.url],
      ],
    });
  }

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.spezielleBedingungForm = null;
  }

  uebernehmen(): void {
    if (this.spezielleBedingungForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.spezielleBedingungForm.value);
        editedControl.markAsDirty();
      } else if (
        !this.formArray.controls.includes(this.spezielleBedingungForm)
      ) {
        this.formArray.push(this.spezielleBedingungForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.spezielleBedingungForm = null;
    }
  }

  private openEdit(formGroup: FormGroup<SpezielleBedingungFormData>): void {
    this.spezielleBedingungForm = formGroup;
    this.editModalOpen = true;
  }

  get canDelete(): boolean {
    return this.benutzer.canWriteStammdaten;
  }
}
