import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AnlagenBerichtGenehmigungenComponent } from './anlagen-bericht-genehmigungen.component';
import { MockComponent, MockModule, MockProviders } from 'ng-mocks';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { FormBuilder } from '@/base/forms';
import { AlertService } from '@/base/service/alert.service';
import { BehaviorSubject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { GenehmigungDto } from '@api/euregistry';

describe('AnlagenBerichtGenehmigungenComponent', () => {
  const genehmigung: GenehmigungDto = {
    erteilt: false,
    neuBetrachtet: false,
    geaendert: false,
    erteilungsdatum: null,
    neuBetrachtetDatum: null,
    aenderungsdatum: null,
    url: '',
    durchsetzungsmassnahmen: '',
  };
  let component: AnlagenBerichtGenehmigungenComponent;
  let fixture: ComponentFixture<AnlagenBerichtGenehmigungenComponent>;
  let routeParams$: BehaviorSubject<{
    land: string;
    berichtsjahr: string;
  }>;

  beforeEach(async () => {
    routeParams$ = new BehaviorSubject({
      land: '00',
      berichtsjahr: '2020',
    });

    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtGenehmigungenComponent,
        MockComponent(TooltipComponent),
      ],
      imports: [
        MockModule(ReactiveFormsModule),
        MockModule(ClarityModule),
        MockModule(RouterTestingModule),
      ],
      providers: [
        MockProviders(ParameterService, AlertService),
        {
          provide: ActivatedRoute,
          useValue: {
            params: routeParams$.asObservable(),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(inject([FormBuilder], (formBuilder: FormBuilder) => {
    fixture = TestBed.createComponent(AnlagenBerichtGenehmigungenComponent);
    component = fixture.componentInstance;
    component.form = AnlagenBerichtGenehmigungenComponent.createForm(
      formBuilder,
      AnlagenBerichtGenehmigungenComponent.toForm(genehmigung)
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be pristine', () => {
    expect(component.form.pristine).toBe(true);
  });

  it('should have identical form value', () => {
    expect(component.form.value).toEqual(
      AnlagenBerichtGenehmigungenComponent.toForm(genehmigung)
    );
  });
});
