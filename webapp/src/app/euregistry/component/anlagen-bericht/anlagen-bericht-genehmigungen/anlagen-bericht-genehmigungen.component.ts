import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';
import { BubeValidators } from '@/base/forms/bube-validators';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { BehoerdeListItemDto, GenehmigungDto } from '@api/euregistry';

export interface GenehmigungFormData {
  erteilt: boolean;
  neuBetrachtet: boolean;
  geaendert: boolean;
  erteilungsdatum: string | undefined;
  betrachtungsdatum: string | undefined;
  aenderungsdatum: string | undefined;
  url: string;
  durchsetzungsmassnahmen: string;
}

@Component({
  selector: 'app-anlagen-bericht-genehmigungen',
  templateUrl: './anlagen-bericht-genehmigungen.component.html',
  styleUrls: ['./anlagen-bericht-genehmigungen.component.scss'],
})
export class AnlagenBerichtGenehmigungenComponent
  implements AfterViewInit, OnInit, AfterViewChecked
{
  @Input()
  form: FormGroup<GenehmigungFormData>;
  @Input()
  saveAttempt: boolean;
  @Input()
  zustaendigeBehoerde: BehoerdeListItemDto;

  @ViewChild(ClrForm, { static: true })
  clrForm: ClrForm;

  constructor(
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly parameterService: ParameterService,
    private readonly route: ActivatedRoute,
    private readonly alertService: AlertService
  ) {}

  static toForm(genehmigung: GenehmigungDto): GenehmigungFormData {
    return {
      erteilt: genehmigung.erteilt,
      neuBetrachtet: genehmigung.neuBetrachtet,
      geaendert: genehmigung.geaendert,
      erteilungsdatum: FormUtil.fromIsoDate(genehmigung.erteilungsdatum),
      betrachtungsdatum: FormUtil.fromIsoDate(genehmigung.neuBetrachtetDatum),
      aenderungsdatum: FormUtil.fromIsoDate(genehmigung.aenderungsdatum),
      url: genehmigung.url,
      durchsetzungsmassnahmen: genehmigung.durchsetzungsmassnahmen,
    };
  }

  static createForm(
    fb: FormBuilder,
    genehmigung: GenehmigungFormData
  ): FormGroup<GenehmigungFormData> {
    return fb.group({
      erteilt: [genehmigung.erteilt],
      neuBetrachtet: [genehmigung.neuBetrachtet],
      geaendert: [genehmigung.geaendert],
      erteilungsdatum: [
        genehmigung.erteilungsdatum,
        genehmigung.erteilt
          ? [Validators.required, BubeValidators.date]
          : [BubeValidators.date],
      ],
      betrachtungsdatum: [
        genehmigung.betrachtungsdatum,
        genehmigung.neuBetrachtet
          ? [Validators.required, BubeValidators.date]
          : [BubeValidators.date],
      ],
      aenderungsdatum: [
        genehmigung.aenderungsdatum,
        genehmigung.geaendert
          ? [Validators.required, BubeValidators.date]
          : [BubeValidators.date],
      ],
      url: [genehmigung.url, [Validators.maxLength(500), BubeValidators.url]],
      durchsetzungsmassnahmen: [
        genehmigung.durchsetzungsmassnahmen,
        Validators.maxLength(1000),
      ],
    });
  }

  ngAfterViewInit(): void {
    if (this.saveAttempt || this.form.touched) {
      this.markAllAsTouched();
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const land = params.land;
      const berichtsjahr = params.berichtsjahr;
      this.parameterService
        .readParameterWertBehoerdeOptional(
          'url_permit',
          berichtsjahr,
          land,
          this.zustaendigeBehoerde.id
        )
        .subscribe(
          (parameterWert) => {
            if (
              !this.form.controls.url.value ||
              this.form.controls.url.value === ''
            ) {
              this.form.controls.url.patchValue(parameterWert?.wert);
            }
          },
          (error) => this.alertService.setAlert(error)
        );
    });

    this.form.controls.erteilt.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        if (value) {
          this.form.controls.erteilungsdatum.setValidators([
            Validators.required,
            BubeValidators.date,
          ]);
        } else {
          this.form.controls.erteilungsdatum.clearValidators();
          this.form.controls.erteilungsdatum.setValidators([
            BubeValidators.date,
          ]);
        }
        this.form.updateValueAndValidity();
        this.clrForm.markAsTouched();
        this.changeDetectorRef.markForCheck();
      });
    this.form.controls.neuBetrachtet.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        if (value) {
          this.form.controls.betrachtungsdatum.setValidators([
            Validators.required,
            BubeValidators.date,
          ]);
        } else {
          this.form.controls.betrachtungsdatum.clearValidators();
          this.form.controls.betrachtungsdatum.setValidators([
            BubeValidators.date,
          ]);
        }
        this.form.updateValueAndValidity();
        this.clrForm.markAsTouched();
        this.changeDetectorRef.markForCheck();
      });
    this.form.controls.geaendert.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        if (value) {
          this.form.controls.aenderungsdatum.setValidators([
            Validators.required,
            BubeValidators.date,
          ]);
        } else {
          this.form.controls.aenderungsdatum.clearValidators();
          this.form.controls.aenderungsdatum.setValidators([
            BubeValidators.date,
          ]);
        }
        this.form.updateValueAndValidity();
        this.clrForm.markAsTouched();
        this.changeDetectorRef.markForCheck();
      });
  }

  ngAfterViewChecked(): void {
    this.markAllAsTouched();
  }

  markAllAsTouched(): void {
    this.clrForm.markAsTouched();
  }
}
