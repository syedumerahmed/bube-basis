import {
  AfterViewChecked,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormUtil } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { ReferenzDto } from '@api/stammdaten';
import { BehoerdeListItemDto, BVTAusnahmeDto } from '@api/euregistry';
import {
  ClarityIcons,
  errorStandardIcon,
  noteIcon,
  plusIcon,
  trashIcon,
} from '@cds/core/icon';

export interface BvtAusnahmeFormData {
  emissionswertAusnahme: ReferenzDto;
  beginntAm: string;
  endetAm?: string;
  oeffentlicheBegruendungUrl: string;
}

ClarityIcons.addIcons(errorStandardIcon, noteIcon, trashIcon, plusIcon);

@Component({
  selector: 'app-anlagen-bericht-bvt-ausnahmen',
  templateUrl: './anlagen-bericht-bvt-ausnahmen.component.html',
  styleUrls: ['./anlagen-bericht-bvt-ausnahmen.component.scss'],
})
export class AnlagenBerichtBvtAusnahmenComponent
  implements OnInit, AfterViewChecked
{
  constructor(
    private readonly fb: FormBuilder,
    private readonly parameterService: ParameterService,
    private readonly route: ActivatedRoute,
    private readonly alertService: AlertService,
    private readonly benutzer: BenutzerProfil,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  @ViewChild(ClrForm)
  clrForm: ClrForm;
  bvtAusnahmeForm: FormGroup<BvtAusnahmeFormData>;

  @Input()
  formArray: FormArray<BvtAusnahmeFormData>;

  @Input()
  zustaendigeBehoerde: BehoerdeListItemDto;

  @Input()
  emissionswertAusnahmenListe: Referenzliste<Required<ReferenzDto>>;

  editModalOpen = false;
  editExistingIndex: number = null;

  oeffentlicheBegruendungUrlInit = null;

  static toForm(
    bvtAusnahmen: Array<BVTAusnahmeDto>
  ): Array<BvtAusnahmeFormData> {
    return (bvtAusnahmen ?? []).map((ausnahme) => ({
      emissionswertAusnahme: ausnahme.emissionswertAusnahme,
      beginntAm: FormUtil.fromIsoDate(ausnahme.beginntAm),
      endetAm: FormUtil.fromIsoDate(ausnahme.endetAm),
      oeffentlicheBegruendungUrl: ausnahme.oeffentlicheBegruendungUrl,
    }));
  }

  static createForm(
    fb: FormBuilder,
    bvtAusnahmen: Array<BvtAusnahmeFormData>,
    selectedBerichtsJahr: ReferenzDto
  ): FormArray<BvtAusnahmeFormData> {
    return fb.array<BvtAusnahmeFormData>(
      bvtAusnahmen.map((ausnahme) => {
        return fb.group({
          emissionswertAusnahme: [
            ausnahme.emissionswertAusnahme,
            [
              Validators.required,
              BubeValidators.isReferenceValid(
                Number(selectedBerichtsJahr.schluessel)
              ),
            ],
          ],
          beginntAm: [
            ausnahme.beginntAm,
            [Validators.required, BubeValidators.date],
          ],
          endetAm: [ausnahme.endetAm, BubeValidators.date],
          oeffentlicheBegruendungUrl: [
            ausnahme.oeffentlicheBegruendungUrl,
            [
              Validators.required,
              Validators.maxLength(500),
              BubeValidators.url,
            ],
          ],
        });
      })
    );
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const land = params.land;
      const berichtsjahr = params.berichtsjahr;
      this.parameterService
        .readParameterWertBehoerdeOptional(
          'url_batd',
          berichtsjahr,
          land,
          this.zustaendigeBehoerde.id
        )
        .subscribe(
          (parameterWert) =>
            (this.oeffentlicheBegruendungUrlInit = parameterWert?.wert),
          (error) => this.alertService.setAlert(error)
        );
    });
  }

  ngAfterViewChecked(): void {
    this.clrForm?.markAsTouched();
  }

  neu(): void {
    this.openEdit(this.createEmptyForm());
  }

  private createEmptyForm(): FormGroup<BvtAusnahmeFormData> {
    return this.fb.group({
      emissionswertAusnahme: [
        null,
        [
          Validators.required,
          BubeValidators.isReferenceValid(
            Number(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
          ),
        ],
      ],
      beginntAm: [null, [Validators.required, BubeValidators.date]],
      endetAm: [null, BubeValidators.date],
      oeffentlicheBegruendungUrl: [
        this.oeffentlicheBegruendungUrlInit,
        [Validators.required, Validators.maxLength(500), BubeValidators.url],
      ],
    });
  }

  get canWrite(): boolean {
    return this.formArray.enabled;
  }

  bearbeiten(index: number): void {
    this.editExistingIndex = index;
    const editableForm = this.createEmptyForm();
    editableForm.setValue(this.formArray.at(index).value);
    this.openEdit(editableForm);
  }

  loeschen(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  abbrechen(): void {
    this.editExistingIndex = null;
    this.editModalOpen = false;
    this.bvtAusnahmeForm = null;
  }

  uebernehmen(): void {
    if (this.bvtAusnahmeForm.invalid) {
      this.clrForm.markAsTouched();
    } else {
      if (this.editExistingIndex != null) {
        const editedControl = this.formArray.at(this.editExistingIndex);
        editedControl.setValue(this.bvtAusnahmeForm.value);
        editedControl.markAsDirty();
      } else if (!this.formArray.controls.includes(this.bvtAusnahmeForm)) {
        this.formArray.push(this.bvtAusnahmeForm);
        this.formArray.markAsDirty();
      }
      this.editExistingIndex = null;
      this.editModalOpen = false;
      this.bvtAusnahmeForm = null;
    }
  }

  private openEdit(formGroup: FormGroup<BvtAusnahmeFormData>): void {
    this.bvtAusnahmeForm = formGroup;
    this.editModalOpen = true;
  }

  get canDelete(): boolean {
    return this.benutzer.canWriteStammdaten;
  }
}
