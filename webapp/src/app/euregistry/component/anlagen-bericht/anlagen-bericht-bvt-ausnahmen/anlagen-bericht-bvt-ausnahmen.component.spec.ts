import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import {
  AnlagenBerichtBvtAusnahmenComponent,
  BvtAusnahmeFormData,
} from './anlagen-bericht-bvt-ausnahmen.component';
import { MockComponents, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { FormBuilder } from '@/base/forms';
import { ReferenzDto, ReferenzlisteEnum } from '@api/stammdaten';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { Referenzliste } from '@/referenzdaten/model/referenzliste';
import { ParameterService } from '@/referenzdaten/service/parameter.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import spyOn = jest.spyOn;

describe('AnlagenBerichtBvtAusnahmenComponent', () => {
  let component: AnlagenBerichtBvtAusnahmenComponent;
  let fixture: ComponentFixture<AnlagenBerichtBvtAusnahmenComponent>;
  let routrParams$: BehaviorSubject<{
    land: string;
    berichtsjahr: string;
  }>;

  const currentBerichtsjahr: ReferenzDto = {
    gueltigBis: 0,
    gueltigVon: 0,
    ktext: '',
    land: '',
    referenzliste: null,
    schluessel: '2020',
    sortier: '20',
    id: 20,
  };

  const emissionswertAusnahmenListe = new Referenzliste([
    {
      id: 1,
      schluessel: '01',
      ktext: '01',
      land: '00',
      sortier: '01',
      gueltigVon: 2007,
      gueltigBis: 2099,
      referenzliste: ReferenzlisteEnum.RBATE,
    },
    {
      id: 2,
      schluessel: '02',
      ktext: '02',
      land: '00',
      sortier: '02',
      gueltigVon: 2007,
      gueltigBis: 2099,
      referenzliste: ReferenzlisteEnum.RBATE,
    },
  ]);

  beforeEach(async () => {
    routrParams$ = new BehaviorSubject({
      land: '00',
      berichtsjahr: '2020',
    });

    await TestBed.configureTestingModule({
      declarations: [
        AnlagenBerichtBvtAusnahmenComponent,
        MockComponents(TooltipComponent),
      ],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(RouterTestingModule),
      ],
      providers: [
        MockProviders(
          BenutzerProfil,
          ParameterService,
          AlertService,
          BerichtsjahrService
        ),
        {
          provide: ActivatedRoute,
          useValue: {
            params: routrParams$.asObservable(),
          },
        },
      ],
    }).compileComponents();

    const berichtsjahrServiceMock = TestBed.inject(BerichtsjahrService);
    jest
      .spyOn(berichtsjahrServiceMock, 'selectedBerichtsjahr', 'get')
      .mockReturnValue(currentBerichtsjahr);
  });

  beforeEach(inject([ParameterService], (parameterService) => {
    spyOn(parameterService, 'readParameterWertOptional').mockReturnValue(EMPTY);
  }));

  describe('empty', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnlagenBerichtBvtAusnahmenComponent);
      component = fixture.componentInstance;
      component.emissionswertAusnahmenListe =
        emissionswertAusnahmenListe as Referenzliste<Required<ReferenzDto>>;
      component.formArray = AnlagenBerichtBvtAusnahmenComponent.createForm(
        fb,
        AnlagenBerichtBvtAusnahmenComponent.toForm([]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });
  });

  describe('pre-filled', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(AnlagenBerichtBvtAusnahmenComponent);
      component = fixture.componentInstance;
      component.emissionswertAusnahmenListe =
        emissionswertAusnahmenListe as Referenzliste<Required<ReferenzDto>>;
      component.formArray = AnlagenBerichtBvtAusnahmenComponent.createForm(
        fb,
        AnlagenBerichtBvtAusnahmenComponent.toForm([
          {
            emissionswertAusnahme: emissionswertAusnahmenListe.values[0],
            beginntAm: '01.01.2000',
            endetAm: '01.02.2000',
            oeffentlicheBegruendungUrl: 'http://wps.de',
          },
          {
            emissionswertAusnahme: emissionswertAusnahmenListe.values[1],
            beginntAm: '01.01.2000',
            endetAm: '01.01.2001',
            oeffentlicheBegruendungUrl: 'http://wps.de',
          },
        ]),
        {
          gueltigBis: 0,
          gueltigVon: 0,
          ktext: '',
          land: '',
          referenzliste: undefined,
          sortier: '',
          schluessel: '2020',
        }
      );
      fixture.detectChanges();
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should not have open modal', () => {
      expect(component.editModalOpen).toBe(false);
    });

    it('should be pristine', () => {
      expect(component.formArray.pristine).toBe(true);
    });

    describe('neu', () => {
      beforeEach(() => {
        component.neu();
      });

      it('should define empty form', () => {
        expect(component.bvtAusnahmeForm).toBeTruthy();
        expect(component.bvtAusnahmeForm.value).toStrictEqual({
          beginntAm: null,
          emissionswertAusnahme: null,
          endetAm: null,
          oeffentlicheBegruendungUrl: null,
        });
        expect(component.bvtAusnahmeForm.valid).toBe(false);
      });

      it('should not touch original form', () => {
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let patchedValue: BvtAusnahmeFormData;
        beforeEach(() => {
          patchedValue = {
            emissionswertAusnahme: emissionswertAusnahmenListe.values[0],
            beginntAm: '31.05.2021',
            endetAm: null,
            oeffentlicheBegruendungUrl: 'https://doof.com',
          };
          component.bvtAusnahmeForm.patchValue(patchedValue);
        });
        it('should be valid', () => {
          expect(component.bvtAusnahmeForm.valid).toBe(true);
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should null kommunikationForm', () => {
            expect(component.bvtAusnahmeForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.valid).toBe(true);
            expect(component.formArray.pristine).toBe(false);
            expect(component.formArray.value).toContainEqual(patchedValue);
          });

          it('should null kommunikationForm', () => {
            expect(component.bvtAusnahmeForm).toBeNull();
          });
        });
      });
    });

    describe('bearbeiten', () => {
      beforeEach(() => {
        component.bearbeiten(0);
      });

      it('should fill dialog form', () => {
        expect(component.bvtAusnahmeForm).toBeTruthy();
        expect(component.bvtAusnahmeForm.value).toStrictEqual(
          component.formArray.at(0).value
        );
        expect(component.bvtAusnahmeForm.valid).toBe(true);
      });

      it('should not touch original form', () => {
        expect(component.formArray.valid).toBe(true);
        expect(component.formArray.pristine).toBe(true);
      });

      it('should open dialog', () => {
        expect(component.editModalOpen).toBe(true);
      });

      describe('fill form', () => {
        let originalValue: BvtAusnahmeFormData;
        let patchedValue: BvtAusnahmeFormData;
        beforeEach(() => {
          originalValue = component.bvtAusnahmeForm.value;
          patchedValue = {
            emissionswertAusnahme: emissionswertAusnahmenListe.values[1],
            beginntAm: '31.04.2022',
            endetAm: null,
            oeffentlicheBegruendungUrl: 'https://lustig.com',
          };
          component.bvtAusnahmeForm.patchValue(patchedValue);
          component.bvtAusnahmeForm.markAsDirty();
        });

        describe('abbrechen', () => {
          beforeEach(() => {
            component.abbrechen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should not touch original form', () => {
            expect(component.formArray.pristine).toBe(true);
            expect(component.formArray.value).not.toContainEqual(patchedValue);
          });

          it('should create new entry after cancel', () => {
            component.neu();
            component.bvtAusnahmeForm.patchValue(patchedValue);
            component.bvtAusnahmeForm.markAsDirty();
            component.uebernehmen();

            expect(component.formArray).toHaveLength(3);
          });

          it('should null form', () => {
            expect(component.bvtAusnahmeForm).toBeNull();
          });
        });

        describe('übernehmen', () => {
          beforeEach(() => {
            component.uebernehmen();
          });

          it('should close modal', () => {
            expect(component.editModalOpen).toBe(false);
          });

          it('should modify array', () => {
            expect(component.formArray.value).toContainEqual(patchedValue);
            expect(component.formArray.value).not.toContainEqual(originalValue);
            expect(component.formArray.dirty).toBe(true);
          });

          it('should stay dirty after canceling edit', () => {
            component.bearbeiten(0);
            component.abbrechen();

            expect(component.formArray.dirty).toBe(true);
          });

          it('should null kommunikationForm', () => {
            expect(component.bvtAusnahmeForm).toBeNull();
          });
        });
      });
    });

    describe('loeschen', () => {
      let elementToDelete: BvtAusnahmeFormData;
      beforeEach(() => {
        elementToDelete = component.formArray.at(0).value;
        component.loeschen(0);
      });

      it('should modify array', () => {
        expect(component.formArray.value).not.toContainEqual(elementToDelete);
        expect(component.formArray.dirty).toBe(true);
      });
    });
  });
});
