import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BerichtsdatenComponent } from './berichtsdaten.component';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { BaseModule } from '@/base/base.module';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { StammdatenModule } from '@/stammdaten/stammdaten.module';

describe('BerichtsdatenComponent', () => {
  let component: BerichtsdatenComponent;
  let fixture: ComponentFixture<BerichtsdatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BerichtsdatenComponent],
      imports: [
        MockModule(BaseModule),
        MockModule(BasedomainModule),
        MockModule(StammdatenModule),
        RouterTestingModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BerichtsdatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
