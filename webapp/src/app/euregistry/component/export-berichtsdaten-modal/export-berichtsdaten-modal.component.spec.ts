import { ExportBerichtsdatenModalComponent } from './export-berichtsdaten-modal.component';
import { MockBuilder } from 'ng-mocks';
import { EuregistryModule } from '@/euregistry/euregistry.module';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Subject } from 'rxjs';
import Mocked = jest.Mocked;
import { EuRegistryRestControllerService } from '@api/euregistry';

describe(ExportBerichtsdatenModalComponent.name, () => {
  let fixture: ComponentFixture<ExportBerichtsdatenModalComponent>;
  let component: ExportBerichtsdatenModalComponent;

  beforeEach(() =>
    MockBuilder(ExportBerichtsdatenModalComponent, EuregistryModule)
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportBerichtsdatenModalComponent);
    component = fixture.componentInstance;
    component.open = false;
    component.selectedIds = [];
    fixture.detectChanges();
  });

  describe('open with selectedIds', () => {
    beforeEach(() => {
      component.open = true;
      component.selectedIds = [12, 13, 14];
      fixture.detectChanges();
    });
    describe('on confirm', () => {
      let response$: Subject<Blob>;
      beforeEach(inject(
        [EuRegistryRestControllerService],
        (controller: Mocked<EuRegistryRestControllerService>) => {
          response$ = new Subject();
          controller.exportBerichtsdaten.mockReturnValue(
            response$.asObservable() as any
          );
          component.exportMarkierte();
        }
      ));

      it('should call service', inject(
        [EuRegistryRestControllerService],
        (controller: Mocked<EuRegistryRestControllerService>) => {
          expect(controller.exportBerichtsdaten).toHaveBeenCalledWith({
            betriebsstaettenBerichtIds: component.selectedIds,
          });
        }
      ));
    });
  });
});
