import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClrLoadingState } from '@clr/angular';
import * as fileSaver from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';
import {
  EmitEvent,
  Event,
  EventBusService,
} from '@/base/service/event-bus.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { AlertService } from '@/base/service/alert.service';
import {
  EuRegistryRestControllerService,
  ExportiereBerichtsdatenCommand,
} from '@api/euregistry';

@Component({
  selector: 'app-export-berichtsdaten-modal',
  templateUrl: './export-berichtsdaten-modal.component.html',
  styleUrls: ['./export-berichtsdaten-modal.component.scss'],
})
export class ExportBerichtsdatenModalComponent {
  @Input() selectedIds: number[];
  @Input() open: boolean;
  @Output() openChange = new EventEmitter<boolean>();
  exportButtonState = ClrLoadingState.DEFAULT;

  constructor(
    private readonly eventBus: EventBusService,
    private readonly dateFormat: FilenameDateFormatPipe,
    private readonly alertService: AlertService,
    private readonly controllerService: EuRegistryRestControllerService
  ) {}

  changeOpen(isOpen: boolean): void {
    this.open = isOpen;
    this.openChange.emit(isOpen);
  }

  exportMarkierte(): void {
    const exportCommand: ExportiereBerichtsdatenCommand = {
      betriebsstaettenBerichtIds: this.selectedIds,
    };

    this.exportButtonState = ClrLoadingState.LOADING;
    this.controllerService.exportBerichtsdaten(exportCommand).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(new Date());
        fileSaver.saveAs(blob, `euregistry_${datum}.xml`);
        this.exportButtonState = ClrLoadingState.SUCCESS;
        this.changeOpen(false);
        this.eventBus.emit(new EmitEvent(Event.BerichtsdatenExportFertig));
        const message =
          this.selectedIds.length === 1
            ? 'Ein Betriebsstättenbericht wurde exportiert.'
            : `${this.selectedIds.length} Betriebsstättenberichte wurden exportiert.`;
        this.alertService.setSuccess(message);
      },
      (err: HttpErrorResponse) => {
        this.exportButtonState = ClrLoadingState.ERROR;
        err.error.text().then((message) => {
          this.changeOpen(false);
          this.alertService.setAlert(
            'Es ist ein Fehler beim Exportieren der XML-Datei aufgetreten: ' +
              message
          );
        });
      }
    );
  }
}
