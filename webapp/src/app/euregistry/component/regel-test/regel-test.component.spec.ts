import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { RegelTestComponent } from './regel-test.component';
import { MockComponent, MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { RegelRestControllerService } from '@api/komplexpruefung';
import { EuRegistryRestControllerService, RegelTestDto } from '@api/euregistry';
import { ReferenzDto } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import spyOn = jest.spyOn;

describe('RegelTestComponent', () => {
  let component: RegelTestComponent;
  let fixture: ComponentFixture<RegelTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MockComponent(TooltipComponent), RegelTestComponent],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
      ],
      providers: [
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        MockProviders(RegelRestControllerService),
        MockProviders(EuRegistryRestControllerService),
        MockProviders(BerichtsjahrService),
      ],
    }).compileComponents();
  });

  const berichtsjahrObservable = new Subject<ReferenzDto>();
  beforeEach(inject([BerichtsjahrService], (service: BerichtsjahrService) => {
    spyOn(service, 'selectedBerichtsjahr$', 'get').mockReturnValue(
      berichtsjahrObservable.asObservable() as never
    );
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegelTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not fill berichtsjahr', () => {
    expect(component.form.value.berichtsjahr).toBeNull();
  });

  describe('after having berichtsjahr externally selected', () => {
    const jahr: ReferenzDto = { id: 42 } as ReferenzDto;
    beforeEach(() => {
      berichtsjahrObservable.next(jahr);
      berichtsjahrObservable.complete();
    });

    it('should fill berichtsjahr', () => {
      expect(component.form.value.berichtsjahr).toBe(42);
    });
  });

  describe('after filling form invalid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
    });

    it('should be invalid', () => {
      expect(component.form.valid).toBe(false);
    });

    describe('after clicking "Speichern"', () => {
      beforeEach(() => {
        component.testeRegel();
      });

      it('should not call service', inject(
        [RegelRestControllerService],
        (service: RegelRestControllerService) => {
          expect(service.updateRegel).not.toHaveBeenCalled();
        }
      ));
    });
  });

  describe('after filling form valid', () => {
    beforeEach(() => {
      component.form.markAsDirty();
      component.form.markAsTouched();
      component.form.patchValue({
        betriebsstaettennummer: '123',
        land: '02',
        berichtsjahr: 2020,
      });
    });

    it('should be valid', () => {
      expect(component.form.valid).toBe(true);
    });

    describe('after clicking "Speichern"', () => {
      let observable: Subject<RegelTestDto>;

      beforeEach(inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          observable = new Subject<RegelTestDto>();
          spyOn(service, 'testeRegel').mockReturnValue(
            observable.asObservable() as never
          );
          component.testeRegel();
        }
      ));

      it('should call service', inject(
        [EuRegistryRestControllerService],
        (service: EuRegistryRestControllerService) => {
          expect(service.testeRegel).toHaveBeenCalled();
        }
      ));

      describe('on success', () => {
        const responseDto: RegelTestDto = {
          status: 'BST_GUELTIG',
          message: null,
        };

        beforeEach(() => {
          observable.next(responseDto);
          observable.complete();
        });

        it('should update ergebnis', () => {
          expect(component.ergebnis).toBe('Regel-Code trifft zu.');
        });
      });

      describe('on syntax error', () => {
        const responseDto: RegelTestDto = {
          status: 'SYNTAX_FEHLER',
          message: 'Expression [.] @0: EL1044E: Unexpectedly ran out of input',
        };

        beforeEach(() => {
          observable.next(responseDto);
          observable.complete();
        });

        it('should update ergebnis', () => {
          expect(component.ergebnis).toBe(
            'Der Regel-Code ist ungültig (Expression [.] @0: EL1044E: Unexpectedly ran out of input)'
          );
        });
      });
    });
  });
});
