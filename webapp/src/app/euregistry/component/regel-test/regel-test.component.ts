import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  IterableDiffer,
  IterableDiffers,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@/base/forms';
import { Validators } from '@angular/forms';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import {
  EuRegistryRestControllerService,
  RegelTestDto,
  TesteRegelRegelObjekteEnum,
} from '@api/euregistry';
import { RegelDto } from '@api/komplexpruefung';
import { ReferenzDto } from '@api/stammdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties } from '@/base/model/Land';
import StatusEnum = RegelTestDto.StatusEnum;

interface TestFormData {
  betriebsstaettennummer: string;
  land: string;
  berichtsjahr: number;
}

@Component({
  selector: 'app-regel-test',
  templateUrl: './regel-test.component.html',
  styleUrls: ['./regel-test.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegelTestComponent
  implements OnInit, OnDestroy, OnChanges, DoCheck
{
  laender: Array<{ nr: string; display: string }>;
  readonly ergebnisTexte: Record<StatusEnum, (message: string) => string> = {
    BST_GUELTIG: () => `Regel-Code trifft zu.`,
    BST_UNGUELTIG: () => `Regel-Code trifft nicht zu.`,
    BST_NICHT_GEFUNDEN: () =>
      `Es wurde keine passende Betriebsstätte gefunden.`,
    KEINE_OBJEKTE: () =>
      `Die ausgewählte Betriebsstätte enthält keine Objekte des spezifizierten Typs.`,
    SEMANTISCHER_FEHLER: (message) =>
      `Der Regel-Code konnte auf die ausgewählten Objekte nicht angewendet werden: ${message}`,
    SYNTAX_FEHLER: (message) => `Der Regel-Code ist ungültig (${message})`,
  };
  form: FormGroup<TestFormData>;

  private differ: IterableDiffer<RegelDto.ObjekteEnum>;

  @Input() code: string;
  @Input() regelObjekte: Array<RegelDto.ObjekteEnum> = [];

  @Output() fehlerfreiEvent = new EventEmitter<boolean>();

  public ergebnis: string;

  private readonly destroyed$ = new Subject();

  jahre: Observable<Array<ReferenzDto>>;

  constructor(
    fb: FormBuilder,
    private readonly euRegistryService: EuRegistryRestControllerService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly alertService: AlertService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly differs: IterableDiffers,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {
    this.form = fb.group({
      betriebsstaettennummer: [null, [Validators.required]],
      land: [null, [Validators.required]],
      berichtsjahr: [null, [Validators.required]],
    });
    this.form.valueChanges.subscribe(this.reset.bind(this));
    this.differ = differs.find([]).create();
  }

  get testeRegelDisabled(): boolean {
    return this.validRegelObjekteForTest.length !== this.regelObjekte?.length;
  }

  private get validRegelObjekteForTest(): Array<TesteRegelRegelObjekteEnum> {
    return this.regelObjekte
      ? this.regelObjekte
          .map((o) => TesteRegelRegelObjekteEnum[o])
          .filter(Boolean)
      : [];
  }

  testeRegel(): void {
    if (this.form.invalid) {
      this.form.markAsTouched();
      return;
    }
    this.euRegistryService
      .testeRegel({
        code: this.code,
        regelObjekte: this.validRegelObjekteForTest,
        berichtsjahrId: this.form.value.berichtsjahr,
        land: this.form.value.land,
        betriebsstaetteNr: this.form.value.betriebsstaettennummer,
      })
      .subscribe(
        (value) => {
          this.ergebnis = this.ergebnisTexte[value.status](value.message);
          switch (value.status) {
            case StatusEnum.BST_GUELTIG:
            case StatusEnum.BST_UNGUELTIG:
              this.fehlerfreiEvent.emit(true);
              break;
            default:
              this.fehlerfreiEvent.emit(false);
          }
          this.changeDetectorRef.markForCheck();
        },
        (error) => {
          this.alertService.setAlert(
            error.error ? error.error : error.statusText
          );
        }
      );
  }

  ngOnChanges(): void {
    this.reset();
  }

  ngOnInit(): void {
    this.laender = Object.values(LandProperties).filter(
      (land) =>
        land != LandProperties.DEUTSCHLAND &&
        (this.benutzerProfil.landNr == '00' ||
          this.benutzerProfil.landNr == land.nr)
    );

    this.form.controls.land.setValue(this.benutzerProfil.landNr);
    this.berichtsjahrService.selectedBerichtsjahr$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((jahr) => {
        this.form.controls.berichtsjahr.setValue(jahr.id);
      });
    this.jahre = this.berichtsjahrService.alleBerichtsJahre$;
  }

  ngDoCheck(): void {
    if (this.differ.diff(this.regelObjekte)) {
      this.reset();
      this.changeDetectorRef.markForCheck();
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private reset() {
    this.ergebnis = '';
  }
}
