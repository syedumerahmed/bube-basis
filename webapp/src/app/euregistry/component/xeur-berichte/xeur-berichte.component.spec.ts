import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XeurBerichteComponent } from './xeur-berichte.component';
import { MockModule, MockProvider } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import {
  XeurBerichtRestControllerService,
  XeurTeilberichtListItemDto,
} from '@api/euregistry';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { BaseModule } from '@/base/base.module';

describe('XeurBerichteComponent', () => {
  let component: XeurBerichteComponent;
  let fixture: ComponentFixture<XeurBerichteComponent>;

  let routeData: BehaviorSubject<{
    xeurTeilberichtListe: Array<XeurTeilberichtListItemDto>;
  }>;

  beforeEach(async () => {
    routeData = new BehaviorSubject({ xeurTeilberichtListe: [] });

    await TestBed.configureTestingModule({
      declarations: [XeurBerichteComponent],
      imports: [
        MockModule(ClarityModule),
        MockModule(BaseModule),
        MockModule(BasedomainModule),
      ],
      providers: [
        MockProvider(ActivatedRoute, { data: routeData.asObservable() }),
        MockProvider(BenutzerProfil),
        MockProvider(XeurBerichtRestControllerService),
        MockProvider(FilenameDateFormatPipe),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XeurBerichteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
