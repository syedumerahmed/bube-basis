import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';
import { AlertService } from '@/base/service/alert.service';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import * as fileSaver from 'file-saver';
import { ClrDatagridSortOrder } from '@clr/angular';
import {
  XeurBerichtRestControllerService,
  XeurTeilberichtListItemDto,
} from '@api/euregistry';
import {
  checkIcon,
  ClarityIcons,
  downloadIcon,
  hostGroupIcon,
} from '@cds/core/icon';

ClarityIcons.addIcons(hostGroupIcon, downloadIcon, checkIcon);
@Component({
  selector: 'app-xeur-berichte',
  templateUrl: './xeur-berichte.component.html',
  styleUrls: ['./xeur-berichte.component.scss'],
})
export class XeurBerichteComponent implements OnInit {
  xeurBerichtListe: Array<XeurTeilberichtListItemDto>;
  readonly SortOrder = ClrDatagridSortOrder;

  selectedTeilbericht: XeurTeilberichtListItemDto;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly xeurBerichtService: XeurBerichtRestControllerService,
    private readonly alertService: AlertService,
    private readonly dateFormat: FilenameDateFormatPipe
  ) {}

  get canWrite(): boolean {
    return this.benutzerProfil.hasRolle(Rolle.LAND_WRITE);
  }

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.xeurBerichtListe = data.xeurTeilberichtListe;
    });
  }

  downloadTeilbericht(xeurTeilbericht: XeurTeilberichtListItemDto): void {
    this.xeurBerichtService.downloadTeilbericht(xeurTeilbericht.id).subscribe(
      (blob) => {
        const datum = this.dateFormat.transform(
          new Date(xeurTeilbericht.ersteErfassung)
        );
        fileSaver.saveAs(
          blob,
          xeurTeilbericht.jahr.schluessel +
            '_' +
            xeurTeilbericht.land +
            '_eureg_' +
            datum +
            '.zip'
        );
      },
      () => {
        this.alertService.setAlert(
          'Es ist ein Fehler beim Download des Teilberichts aufgetreten'
        );
      }
    );
  }

  teilberichtFreigeben(): void {
    const xeurTeilbericht: XeurTeilberichtListItemDto =
      this.selectedTeilbericht;

    this.xeurBerichtService.teilberichtFreigeben(xeurTeilbericht.id).subscribe(
      () => {
        this.alertService.setSuccess('Freigabe war erfolgreich.');
        this.selectedTeilbericht.freigegeben = true;
        this.selectedTeilbericht = null;
      },
      (error) => {
        this.alertService.setAlert(error);
        this.selectedTeilbericht = null;
      }
    );
  }
}
