import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EuregistryRoutingModule } from './routing/euregistry-routing.module';
import { BetriebsstaettenBerichteComponent } from './component/betriebsstaetten-berichte/betriebsstaetten-berichte.component';
import { ClarityModule } from '@clr/angular';
import { EuregistryComponent } from './component/euregistry/euregistry.component';
import { BaseModule } from '@/base/base.module';
import { BetriebsstaettenBerichtDetailsComponent } from './component/betriebsstaetten-bericht/betriebsstaetten-bericht-details/betriebsstaetten-bericht-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnlagenBerichtDetailsComponent } from './component/anlagen-bericht/anlagen-bericht-details/anlagen-bericht-details.component';
import { BetriebsstaettenBerichtStammdatenInfoComponent } from './component/betriebsstaetten-bericht/betriebsstaetten-bericht-stammdaten-info/betriebsstaetten-bericht-stammdaten-info.component';
import { BetriebsstaettenBerichtAnlagenListeComponent } from './component/betriebsstaetten-bericht/betriebsstaetten-bericht-anlagen-liste/betriebsstaetten-bericht-anlagen-liste.component';
import { BetriebsstaettenBerichtMainComponent } from './component/betriebsstaetten-bericht/betriebsstaetten-bericht-main/betriebsstaetten-bericht-main.component';
import { EuRegEntityNotFoundComponent } from '@/euregistry/component/eu-reg-entity-not-found/eu-reg-entity-not-found.component';
import { BetriebsstaetteModule } from '@/stammdaten/betriebsstaette/betriebsstaette.module';
import { StammdatenModule } from '@/stammdaten/stammdaten.module';
import { BetriebsstaettenBerichtComponent } from './component/betriebsstaetten-bericht/betriebsstaetten-bericht/betriebsstaetten-bericht.component';
import { AnlagenBerichtAllgemeinComponent } from './component/anlagen-bericht/anlagen-bericht-allgemein/anlagen-bericht-allgemein.component';
import { AnlagenBerichtGenehmigungenComponent } from './component/anlagen-bericht/anlagen-bericht-genehmigungen/anlagen-bericht-genehmigungen.component';
import { AnlagenBerichtBvtAusnahmenComponent } from './component/anlagen-bericht/anlagen-bericht-bvt-ausnahmen/anlagen-bericht-bvt-ausnahmen.component';
import { AnlagenBerichtMainComponent } from './component/anlagen-bericht/anlagen-bericht-main/anlagen-bericht-main.component';
import { AnlagenBerichtStammdatenInfoComponent } from './component/anlagen-bericht/anlagen-bericht-stammdaten-info/anlagen-bericht-stammdaten-info.component';
import { ReferenzdatenModule } from '@/referenzdaten/referenzdaten.module';
import { AnlagenBerichtFeuerungsanlageComponent } from './component/anlagen-bericht/anlagen-bericht-feuerungsanlage/anlagen-bericht-feuerungsanlage.component';
import { AnlagenBerichtSpezielleBedingungenComponent } from './component/anlagen-bericht/anlagen-bericht-spezielle-bedingungen/anlagen-bericht-spezielle-bedingungen.component';
import { AnlagenBerichtAuflagenComponent } from './component/anlagen-bericht/anlagen-bericht-auflagen/anlagen-bericht-auflagen.component';
import { BasedomainModule } from '@/basedomain/basedomain.module';
import { XeurBerichteComponent } from './component/xeur-berichte/xeur-berichte.component';
import { BerichtsdatenComponent } from '@/euregistry/component/berichtsdaten/berichtsdaten.component';
import { ExportBerichtsdatenModalComponent } from '@/euregistry/component/export-berichtsdaten-modal/export-berichtsdaten-modal.component';
import { RegelTestComponent } from '@/euregistry/component/regel-test/regel-test.component';

@NgModule({
  declarations: [
    BetriebsstaettenBerichteComponent,
    EuregistryComponent,
    BetriebsstaettenBerichtDetailsComponent,
    AnlagenBerichtDetailsComponent,
    BetriebsstaettenBerichtStammdatenInfoComponent,
    BetriebsstaettenBerichtAnlagenListeComponent,
    BetriebsstaettenBerichtMainComponent,
    EuRegEntityNotFoundComponent,
    BetriebsstaettenBerichtComponent,
    AnlagenBerichtAllgemeinComponent,
    AnlagenBerichtGenehmigungenComponent,
    AnlagenBerichtBvtAusnahmenComponent,
    AnlagenBerichtMainComponent,
    AnlagenBerichtStammdatenInfoComponent,
    AnlagenBerichtFeuerungsanlageComponent,
    AnlagenBerichtSpezielleBedingungenComponent,
    AnlagenBerichtAuflagenComponent,
    XeurBerichteComponent,
    BerichtsdatenComponent,
    ExportBerichtsdatenModalComponent,
    RegelTestComponent,
  ],
  imports: [
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,

    EuregistryRoutingModule,
    BaseModule,
    BasedomainModule,
    BetriebsstaetteModule,
    StammdatenModule,
    ReferenzdatenModule,
  ],
  exports: [RegelTestComponent],
})
export class EuregistryModule {}
