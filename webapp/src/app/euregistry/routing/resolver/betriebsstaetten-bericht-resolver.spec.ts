import { Router } from '@angular/router';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import { inject, TestBed } from '@angular/core/testing';
import { MockProviders } from 'ng-mocks';
import { EMPTY, throwError } from 'rxjs';
import { BetriebsstaettenBerichtResolver } from '@/euregistry/routing/resolver/betriebsstaetten-bericht-resolver';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import spyOn = jest.spyOn;
import { EuRegistryRestControllerService } from '@api/euregistry';

describe('BetriebsstaettenBerichtResolver', () => {
  let resolver: BetriebsstaettenBerichtResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BetriebsstaettenBerichtResolver,
        MockProviders(
          EuRegistryRestControllerService,
          ReferenzdatenService,
          Router,
          Location,
          AlertService
        ),
      ],
    });
    resolver = TestBed.inject(BetriebsstaettenBerichtResolver);
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
  });

  describe('loadExistingWithNummerNeu', () => {
    let activatedRouteSnapshot;
    beforeEach(() => {
      activatedRouteSnapshot = {
        params: { nummer: 'neu', land: '01', berichtsjahr: '2021' },
        queryParams: {},
      };
    });

    it('should load from server by nummer', inject(
      [EuRegistryRestControllerService],
      (service: EuRegistryRestControllerService) => {
        spyOn(
          service,
          'readBetriebsstaettenBerichtByJahrLandNummer'
        ).mockReturnValue(EMPTY);

        resolver.resolve(activatedRouteSnapshot, null);

        expect(
          service.readBetriebsstaettenBerichtByJahrLandNummer
        ).toHaveBeenCalledWith('2021', '01', 'neu');
        expect(
          service.readBetriebsstaettenBerichtByJahrAndLocalId
        ).not.toHaveBeenCalled();
      }
    ));

    it('should load from server using localId and fallback to nr', inject(
      [EuRegistryRestControllerService],
      async (service: EuRegistryRestControllerService) => {
        activatedRouteSnapshot.queryParams.localId = 'localId';
        spyOn(
          service,
          'readBetriebsstaettenBerichtByJahrAndLocalId'
        ).mockReturnValue(throwError({ status: 404 }));
        spyOn(
          service,
          'readBetriebsstaettenBerichtByJahrLandNummer'
        ).mockReturnValue(EMPTY);

        await resolver.resolve(activatedRouteSnapshot, null).toPromise();

        expect(
          service.readBetriebsstaettenBerichtByJahrAndLocalId
        ).toHaveBeenCalledWith('2021', 'localId');
        expect(
          service.readBetriebsstaettenBerichtByJahrLandNummer
        ).toHaveBeenCalledWith('2021', '01', 'neu');
      }
    ));
  });
});
