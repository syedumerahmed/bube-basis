import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';
import { Location } from '@angular/common';
import {
  EuRegAnlageDto,
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';

@Injectable()
export class AnlagenteilBerichtResolver implements Resolve<EuRegAnlageDto> {
  constructor(
    private readonly service: EuRegistryRestControllerService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<EuRegAnlageDto> {
    const euRegBst: EuRegBetriebsstaetteDto =
      route.parent.data.euRegBetriebsstaette;
    const id = euRegBst.betriebsstaetteInfo.id;
    const anlagenNummer = route.params.anlagenNummer;
    const anlagenteilNummer = route.params.anlagenteilNummer;
    return this.service
      .readAnlagenteilBericht(id, anlagenNummer, anlagenteilNummer)
      .pipe(this.alertService.catchError());
  }
}
