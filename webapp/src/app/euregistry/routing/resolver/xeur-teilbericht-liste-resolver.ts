import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { AlertService } from '@/base/service/alert.service';
import { Observable } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import {
  XeurBerichtRestControllerService,
  XeurTeilberichtListItemDto,
} from '@api/euregistry';

@Injectable()
export class XeurTeilberichtListeResolver
  implements Resolve<Array<XeurTeilberichtListItemDto>>
{
  constructor(
    private readonly alertService: AlertService,
    private readonly xeurBerichtService: XeurBerichtRestControllerService,
    private readonly berichtsjahrService: BerichtsjahrService
  ) {}

  resolve(): Observable<Array<XeurTeilberichtListItemDto>> {
    return this.xeurBerichtService
      .getAll(this.berichtsjahrService.selectedBerichtsjahr.schluessel)
      .pipe(this.alertService.catchError());
  }
}
