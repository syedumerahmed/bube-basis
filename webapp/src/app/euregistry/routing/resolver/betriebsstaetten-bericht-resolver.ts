import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '@/base/service/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import {
  EuRegBetriebsstaetteDto,
  EuRegistryRestControllerService,
} from '@api/euregistry';

@Injectable()
export class BetriebsstaettenBerichtResolver
  implements Resolve<EuRegBetriebsstaetteDto>
{
  constructor(
    private readonly service: EuRegistryRestControllerService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly alertService: AlertService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<EuRegBetriebsstaetteDto> {
    const berichtId = route.params.id;
    const landSchluessel = route.params.land;
    const berichtsjahr = route.params.berichtsjahr;
    const localId = route.queryParams.localId;
    const nummer = route.params.nummer;

    let bericht: Observable<EuRegBetriebsstaetteDto>;
    if (berichtId) {
      bericht = this.service.readBetriebsstaettenBerichtById(berichtId);
      bericht.subscribe(
        (dto) => {
          this.router.navigate(
            [
              'eureg',
              'berichtsdaten',
              'jahr',
              dto.betriebsstaetteInfo.berichtsjahr,
              'betriebsstaette',
              'land',
              dto.betriebsstaetteInfo.land,
              'nummer',
              dto.betriebsstaetteInfo.betriebsstaetteNr,
            ],
            {
              queryParams: {
                localId: dto.betriebsstaetteInfo.localId,
              },
            }
          );
        },
        (err) => {
          this.alertService.setAlert(err.message);
        }
      );
      return EMPTY;
    }

    const requestByBstNr =
      this.service.readBetriebsstaettenBerichtByJahrLandNummer(
        berichtsjahr,
        landSchluessel,
        nummer
      );
    if (!localId) {
      bericht = requestByBstNr;
    } else {
      bericht = this.service
        .readBetriebsstaettenBerichtByJahrAndLocalId(berichtsjahr, localId)
        .pipe(
          catchError((err: HttpErrorResponse) => {
            if (err.status !== 404) {
              return throwError(err);
            }
            // localId existiert nicht für Jahr/Land -> Betriebsstättennummer verwenden
            return requestByBstNr;
          })
        );
    }

    return bericht.pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router
            .navigate(['eureg', 'jahr', berichtsjahr, '404'])
            .then(() => this.location.replaceState(state.url));
        }
        this.alertService.setAlert(err);
        return EMPTY;
      })
    );
  }
}
