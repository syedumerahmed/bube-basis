import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EuregistryComponent } from '@/euregistry/component/euregistry/euregistry.component';
import { BerichtsjahrListResolver } from '@/referenzdaten/routing/resolver/berichtsjahr-list-resolver';
import { BerichtsjahrGuard } from '@/base/routing/guard/berichtsjahr.guard';
import { BetriebsstaettenBerichteComponent } from '@/euregistry/component/betriebsstaetten-berichte/betriebsstaetten-berichte.component';
import { BetriebsstaettenBerichtMainComponent } from '@/euregistry/component/betriebsstaetten-bericht/betriebsstaetten-bericht-main/betriebsstaetten-bericht-main.component';
import { BetriebsstaettenBerichtResolver } from '@/euregistry/routing/resolver/betriebsstaetten-bericht-resolver';
import { BehoerdenListResolver } from '@/referenzdaten/routing/resolver/behoerden-list-resolver';
import { EuRegEntityNotFoundComponent } from '@/euregistry/component/eu-reg-entity-not-found/eu-reg-entity-not-found.component';
import { AnlagenBerichtResolver } from '@/euregistry/routing/resolver/anlagen-bericht-resolver';
import { AnlagenteilBerichtResolver } from '@/euregistry/routing/resolver/anlagenteil-bericht-resolver';
import { BetriebsstaettenBerichtComponent } from '@/euregistry/component/betriebsstaetten-bericht/betriebsstaetten-bericht/betriebsstaetten-bericht.component';
import { AnlagenBerichtMainComponent } from '@/euregistry/component/anlagen-bericht/anlagen-bericht-main/anlagen-bericht-main.component';
import { AnwendbareBvtListResolver } from '@/referenzdaten/routing/resolver/anwendbare-bvt-list-resolver';
import { EmissionswertAusnahmenListResolver } from '@/referenzdaten/routing/resolver/emissionswerte-ausnahmen-list-Resolver';
import { SpezielleBedingungArtListResolver } from '@/referenzdaten/routing/resolver/spezielle-bedingung-art-list-resolver';
import { BerichtsdatenComponent } from '@/euregistry/component/berichtsdaten/berichtsdaten.component';
import { XeurBerichteComponent } from '@/euregistry/component/xeur-berichte/xeur-berichte.component';
import { XeurTeilberichtListeResolver } from '@/euregistry/routing/resolver/xeur-teilbericht-liste-resolver';
import { AusnahmenArt31bis35ListResolver } from '@/referenzdaten/routing/resolver/ausnahmen-art31bis35-list-resolver';
import { KapitelIrRlListResolver } from '@/referenzdaten/routing/resolver/kapitel-ir-rl-list-resolver';
import { BearbeitungsstatusBerichtListResolver } from '@/referenzdaten/routing/resolver/bearbeitungsstatus-bericht-list-resolver';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';

const routes: Routes = [
  {
    path: '',
    component: EuregistryComponent,
    children: [
      {
        path: '',
        redirectTo: 'berichtsdaten',
      },
      {
        path: 'berichtsdaten',
        children: [
          {
            path: '',
            redirectTo: 'jahr/',
          },
          {
            path: 'betriebsstaette/:id',
            component: BetriebsstaettenBerichtComponent,
            resolve: {
              euRegBetriebsstaette: BetriebsstaettenBerichtResolver,
            },
          },
          {
            path: 'jahr/:berichtsjahr',
            component: BerichtsdatenComponent,
            canActivate: [BerichtsjahrGuard],
            resolve: {
              berichtsjahre: BerichtsjahrListResolver,
            },
            children: [
              {
                path: '',
                component: BetriebsstaettenBerichteComponent,
              },
              { path: '404', component: EuRegEntityNotFoundComponent },
              {
                path: 'betriebsstaette/land/:land/nummer/:nummer',
                component: BetriebsstaettenBerichtComponent,
                resolve: {
                  euRegBetriebsstaette: BetriebsstaettenBerichtResolver,
                  behoerdenListe: BehoerdenListResolver,
                  bearbeitungsstatusListe:
                    BearbeitungsstatusBerichtListResolver,
                },
                children: [
                  {
                    path: '',
                    component: BetriebsstaettenBerichtMainComponent,
                    canDeactivate: [UnsavedChangesGuard],
                  },
                  {
                    path: 'anlage/nummer/:nummer',
                    component: AnlagenBerichtMainComponent,
                    canDeactivate: [UnsavedChangesGuard],
                    runGuardsAndResolvers: 'always',
                    resolve: {
                      euRegAnlage: AnlagenBerichtResolver,
                      anwendbareBVTListe: AnwendbareBvtListResolver,
                      kapitelIeRlListe: KapitelIrRlListResolver,
                      emissionswertAusnahmenListe:
                        EmissionswertAusnahmenListResolver,
                      spezielleBedingungArtListe:
                        SpezielleBedingungArtListResolver,
                      ausnahmenArt31bis35Liste: AusnahmenArt31bis35ListResolver,
                    },
                  },
                  {
                    path: 'anlage/nummer/:anlagenNummer/anlagenteil/nummer/:anlagenteilNummer',
                    component: AnlagenBerichtMainComponent,
                    canDeactivate: [UnsavedChangesGuard],
                    runGuardsAndResolvers: 'always',
                    resolve: {
                      euRegAnlage: AnlagenteilBerichtResolver,
                      anwendbareBVTListe: AnwendbareBvtListResolver,
                      kapitelIeRlListe: KapitelIrRlListResolver,
                      emissionswertAusnahmenListe:
                        EmissionswertAusnahmenListResolver,
                      spezielleBedingungArtListe:
                        SpezielleBedingungArtListResolver,
                      ausnahmenArt31bis35Liste: AusnahmenArt31bis35ListResolver,
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        path: 'xeurBerichte',
        children: [
          {
            path: '',
            redirectTo: 'jahr/',
          },
          {
            path: 'jahr/:berichtsjahr',
            canActivate: [BerichtsjahrGuard],
            component: XeurBerichteComponent,
            resolve: { xeurTeilberichtListe: XeurTeilberichtListeResolver },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    BerichtsjahrGuard,
    BetriebsstaettenBerichtResolver,
    AnlagenBerichtResolver,
    AnlagenteilBerichtResolver,
    XeurTeilberichtListeResolver,
  ],
})
export class EuregistryRoutingModule {}
