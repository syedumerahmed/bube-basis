import { inject, TestBed } from '@angular/core/testing';

import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import {
  BEHOERDE_READ_TEST_USER,
  BETREIBER_TEST_USER,
  GESAMTADMIN_DUMMY,
} from '@/base/security/model/benutzer-profil.mock';
import { EuregistryGuard } from '@/euregistry/routing/guard/euregistry.guard';

describe('EuregistryGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EuregistryGuard,
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
      ],
    });
  });

  it('should be created', inject([EuregistryGuard], (guard) => {
    expect(guard).toBeTruthy();
  }));

  it('should allow for Gesamtadmin', inject([EuregistryGuard], (guard) => {
    expect(guard.canActivate(null, null)).toBe(true);
  }));

  it('should allow for Behörde', () => {
    TestBed.overrideProvider(BenutzerProfil, {
      useFactory: BEHOERDE_READ_TEST_USER,
    });
    const guard = TestBed.inject(EuregistryGuard);
    expect(guard.canActivate()).toBe(true);
  });

  it('should deny for Betrieber', () => {
    TestBed.overrideProvider(BenutzerProfil, {
      useFactory: BETREIBER_TEST_USER,
    });
    const guard = TestBed.inject(EuregistryGuard);
    expect(guard.canActivate()).toBe(false);
  });
});
