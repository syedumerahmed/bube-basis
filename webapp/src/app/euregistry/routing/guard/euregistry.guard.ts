import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';

@Injectable({ providedIn: 'root' })
export class EuregistryGuard implements CanActivate {
  constructor(private readonly benutzerProfil: BenutzerProfil) {}

  canActivate(): boolean {
    return this.benutzerProfil.canSeeEURegBerichtsdaten;
  }
}
