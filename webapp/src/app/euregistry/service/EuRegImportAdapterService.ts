import { Injectable } from '@angular/core';
import { ImportService } from '@/base/component/import-modal/import-modal.component';
import { Observable } from 'rxjs';
import { EuregBetriebsstaettenRestControllerService } from '@api/euregistry';
import { DateiDto, Validierungshinweis } from '@api/stammdaten';

@Injectable({
  providedIn: 'root',
})
export class EuRegImportAdapterService implements ImportService {
  constructor(
    private readonly euRegBetriebsstaettenService: EuregBetriebsstaettenRestControllerService
  ) {}

  aktuelleDatei(): Observable<DateiDto> {
    return this.euRegBetriebsstaettenService.existierendeEuregDatei();
  }

  dateiLoeschen(): Observable<any> {
    return this.euRegBetriebsstaettenService.euregDateiLoeschen();
  }

  import(jahrSchuessel?: string): Observable<any> {
    return this.euRegBetriebsstaettenService.importEureg(jahrSchuessel);
  }

  upload(datei: File): Observable<any> {
    return this.euRegBetriebsstaettenService.uploadEureg(datei);
  }

  validiere(jahrSchuessel?: string): Observable<Array<Validierungshinweis>> {
    return this.euRegBetriebsstaettenService.validiereEureg(jahrSchuessel);
  }
}
