import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app/component/app/app.component';
import { KeycloakAngularModule } from 'keycloak-angular';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BaseModule } from 'src/app/base/base.module';
import '@angular/common/locales/global/de';
import { BenutzerProfilService } from '@/base/service/benutzer-profil.service';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { UpdateProfileComponent } from './app/component/update-profile/update-profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ToolbarComponent } from '@/app/component/toolbar/toolbar.component';
import { AuthInterceptor } from '@/base/security/routing/auth.interceptor';
import { RouteReuseStrategy } from '@angular/router';
import { DoNotReuseBenutzerComponent } from '@/benutzerverwaltung/routing/do-not-reuse-benutzer-component';
import { BASE_PATH, EnvironmentRestControllerService } from '@api';
import { BASE_PATH as BASE_PATH_BENUTZER } from '@api/benutzer';
import { BASE_PATH as BASE_PATH_DESKTOP } from '@api/desktop';
import { BASE_PATH as BASE_PATH_EUREGISTRY } from '@api/euregistry';
import { BASE_PATH as BASE_PATH_KOMPLEXPRUEFUNG } from '@api/komplexpruefung';
import { BASE_PATH as BASE_PATH_REFERENZEN } from '@api/referenzdaten';
import { BASE_PATH as BASE_PATH_STAMMDATEN } from '@api/stammdaten';
import { BASE_PATH as BASE_PATH_STAMMDATENBETREIBER } from '@api/stammdatenbetreiber';
import { BASE_PATH as BASE_PATH_STAMMDATEN_UEBERNAHME } from '@api/stammdaten-uebernahme';
import { AppRoutingModule } from '@/app-routing.module';

@NgModule({
  declarations: [AppComponent, ToolbarComponent, UpdateProfileComponent],
  imports: [
    BaseModule,
    BrowserModule,
    AppRoutingModule,
    KeycloakAngularModule,
    HttpClientModule,
    ClarityModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory:
        (
          benutzerService: BenutzerProfilService,
          environmentService: EnvironmentRestControllerService
        ) =>
        async () => {
          const environment = await environmentService
            .getEnvironment()
            .toPromise();
          return benutzerService.init({
            config: environment.keycloak.config,
            initOptions: {
              onLoad: 'check-sso',
              silentCheckSsoRedirectUri:
                window.location.origin + '/assets/silent-check-sso.html',
            },
          });
        },
      multi: true,
      deps: [BenutzerProfilService, EnvironmentRestControllerService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: BenutzerProfil,
      useFactory: (benutzerService: BenutzerProfilService) =>
        benutzerService.benutzerProfil,
      deps: [BenutzerProfilService, APP_INITIALIZER],
    },
    { provide: BASE_PATH, useValue: '/api' },
    { provide: BASE_PATH_BENUTZER, useValue: '/api' },
    { provide: BASE_PATH_DESKTOP, useValue: '/api' },
    { provide: BASE_PATH_EUREGISTRY, useValue: '/api' },
    { provide: BASE_PATH_KOMPLEXPRUEFUNG, useValue: '/api' },
    { provide: BASE_PATH_REFERENZEN, useValue: '/api' },
    { provide: BASE_PATH_STAMMDATEN, useValue: '/api' },
    { provide: BASE_PATH_STAMMDATENBETREIBER, useValue: '/api' },
    { provide: BASE_PATH_STAMMDATEN_UEBERNAHME, useValue: '/api' },
    { provide: LOCALE_ID, useValue: 'de' },
    {
      provide: RouteReuseStrategy,
      useClass: DoNotReuseBenutzerComponent,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
