import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'completeUriEncoder',
})
export class CompleteUriEncoderPipe implements PipeTransform {
  transform(value: string): string {
    return encodeURIComponent(value).replace(/[!'()*]/g, function (c) {
      return '%' + c.charCodeAt(0).toString(16);
    });
  }
}
