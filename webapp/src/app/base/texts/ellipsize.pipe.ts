import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsize',
})
export class EllipsizePipe implements PipeTransform {
  transform(value: string, limit = 100, ellipsis = '…'): string {
    return value != null && value.length > limit
      ? value.substr(0, limit) + ellipsis
      : value;
  }
}
