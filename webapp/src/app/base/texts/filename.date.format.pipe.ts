import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'filenameDateFormat',
})
export class FilenameDateFormatPipe implements PipeTransform {
  transform(value: Date): string {
    const datePipe: DatePipe = new DatePipe('de-DE');
    return datePipe.transform(value, 'yyyy-MM-ddTHH_mm_ss');
  }
}
