import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImportService } from '@/base/component/import-modal/import-modal.component';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { DateiDto, Validierungshinweis } from '@api/stammdaten';

@Injectable({
  providedIn: 'root',
})
export class ReferenzdatenImportAdapterService implements ImportService {
  constructor(
    private readonly referenzenService: ReferenzenRestControllerService
  ) {}

  aktuelleDatei(): Observable<DateiDto> {
    return this.referenzenService.existierendeReferenzenDatei();
  }

  dateiLoeschen(): Observable<any> {
    return this.referenzenService.referenzenDateiLoeschen();
  }

  import(): Observable<any> {
    return this.referenzenService.importReferenzen();
  }

  upload(datei: File): Observable<any> {
    return this.referenzenService.uploadReferenzen(datei);
  }

  validiere(): Observable<Array<Validierungshinweis>> {
    return this.referenzenService.validiereReferenzen();
  }
}
