import { Injectable } from '@angular/core';
import { ImportService } from '@/base/component/import-modal/import-modal.component';
import { Observable } from 'rxjs';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { DateiDto, Validierungshinweis } from '@api/stammdaten';

@Injectable({
  providedIn: 'root',
})
export class BehoerdenImportAdapterService implements ImportService {
  constructor(
    private readonly referenzenService: ReferenzenRestControllerService
  ) {}

  aktuelleDatei(): Observable<DateiDto> {
    return this.referenzenService.existierendeBehoerdenDatei();
  }

  dateiLoeschen(): Observable<any> {
    return this.referenzenService.behoerdenDateiLoeschen();
  }

  import(): Observable<any> {
    return this.referenzenService.importBehoerden();
  }

  upload(datei: File): Observable<any> {
    return this.referenzenService.uploadBehoerden(datei);
  }

  validiere(): Observable<Array<Validierungshinweis>> {
    return this.referenzenService.validiereBehoerden();
  }
}
