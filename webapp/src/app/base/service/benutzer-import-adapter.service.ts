import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImportService } from '@/base/component/import-modal/import-modal.component';
import { BenutzerRestControllerService } from '@api/benutzer';
import { DateiDto, Validierungshinweis } from '@api/stammdaten';

@Injectable({
  providedIn: 'root',
})
export class BenutzerImportAdapterService implements ImportService {
  constructor(
    private readonly benutzerService: BenutzerRestControllerService
  ) {}

  aktuelleDatei(): Observable<DateiDto> {
    return this.benutzerService.existierendeDatei();
  }

  dateiLoeschen(): Observable<any> {
    return this.benutzerService.dateiLoeschen();
  }

  import(): Observable<any> {
    return this.benutzerService.importBenutzer();
  }

  upload(datei: File): Observable<any> {
    return this.benutzerService.upload(datei);
  }

  validiere(): Observable<Array<Validierungshinweis>> {
    return this.benutzerService.validiere();
  }
}
