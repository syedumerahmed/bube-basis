import { Injectable } from '@angular/core';
import {
  EmitEvent,
  Event,
  EventBusService,
} from '@/base/service/event-bus.service';
import { JobStatusDto, JobStatusEnum } from '@api';

@Injectable({
  providedIn: 'root',
})
export class JobStatusService {
  public isRunning = false;

  constructor(private eventBus: EventBusService) {}

  public jobIsRunning(): boolean {
    return this.isRunning;
  }

  public jobStarted(): void {
    this.isRunning = true;
  }

  public jobEnded(jobStatus: JobStatusDto): void {
    this.isRunning = false;

    if (jobStatus.jobName === JobStatusEnum.STAMMDATEN_IMPORTIEREN) {
      this.eventBus.emit(
        new EmitEvent(Event.BetriebsstaettenImportFertig, jobStatus)
      );
    } else if (jobStatus.jobName === JobStatusEnum.REFERENZDATEN_IMPORTIEREN) {
      this.eventBus.emit(
        new EmitEvent(Event.ReferenzdatenImportFertig, jobStatus)
      );
    } else if (jobStatus.jobName === JobStatusEnum.BENUTZER_IMPORTIEREN) {
      this.eventBus.emit(
        new EmitEvent(Event.BetreiberBenutzerImportFertig, jobStatus)
      );
    } else if (jobStatus.jobName === JobStatusEnum.BEHOERDEN_IMPORTIEREN) {
      this.eventBus.emit(new EmitEvent(Event.BehoerdenImportFertig, jobStatus));
    } else if (jobStatus.jobName === JobStatusEnum.KOMPLEXPRUEFUNG) {
      this.eventBus.emit(new EmitEvent(Event.KomplexpruefungFertig, jobStatus));
    } else if (jobStatus.jobName === JobStatusEnum.XEUR_TEILBERICHT_ERSTELLEN) {
      this.eventBus.emit(new EmitEvent(Event.XeurTeilberichtFertig, jobStatus));
    } else if (jobStatus.jobName === JobStatusEnum.EUREGISRTY_IMPORTIEREN) {
      this.eventBus.emit(
        new EmitEvent(Event.BerichtsdatenImportFertig, jobStatus)
      );
    }
  }
}
