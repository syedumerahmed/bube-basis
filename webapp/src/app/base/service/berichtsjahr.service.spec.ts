import { inject, TestBed } from '@angular/core/testing';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { MockService } from 'ng-mocks';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { GESAMTADMIN_DUMMY } from '@/base/security/model/benutzer-profil.mock';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { ReferenzDto } from '@api/stammdaten';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import spyOn = jest.spyOn;

describe(BerichtsjahrService.name, () => {
  let service: BerichtsjahrService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ReferenzenRestControllerService,
          useValue: MockService(ReferenzenRestControllerService),
        },
        { provide: BenutzerProfil, useFactory: GESAMTADMIN_DUMMY },
        { provide: Router, useValue: MockService(Router) },
      ],
    });
    service = TestBed.inject(BerichtsjahrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when selecting latest Berichtsjahr', () => {
    let latestJahr: ReferenzDto;
    let selectedSubscriber: jest.Mock<void, [ReferenzDto]>;
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenController: ReferenzenRestControllerService) => {
        latestJahr = { schluessel: '2020' } as ReferenzDto;
        spyOn(
          referenzenController,
          'readAktuellesBerichtsjahr'
        ).mockReturnValue(of(latestJahr) as Observable<any>);
        selectedSubscriber = jest.fn();
        service.selectedBerichtsjahr$.subscribe(selectedSubscriber);
      }
    ));

    it('should complete with latest Jahr', async () => {
      const selected = await service.selectDefaultBerichtsjahr().toPromise();

      expect(selected).toEqual(latestJahr);
    });

    describe('with selected berichtsjahr', () => {
      beforeEach(async () => {
        await service.selectDefaultBerichtsjahr().toPromise();
      });

      it('should emit selected', () => {
        expect(selectedSubscriber).toHaveBeenCalledWith(latestJahr);
      });

      it('should have synchronous value', () => {
        expect(service.selectedBerichtsjahr).toEqual(latestJahr);
      });

      it('should update url', inject([Router], async (router: Router) => {
        const navigation = jest
          .spyOn(router, 'navigateByUrl')
          .mockResolvedValue(true);
        jest
          .spyOn(router, 'url', 'get')
          .mockReturnValue('/somemodule/jahr/2020');
        const newBerichtsjahr = { schluessel: '2017' } as ReferenzDto;

        service.setBerichtsjahr(newBerichtsjahr);
        await navigation;
        expect(router.navigateByUrl).toHaveBeenCalledWith(
          '/somemodule/jahr/2017'
        );
      }));

      it('should reset to previous jahr on navigation error', inject(
        [Router],
        async (router: Router) => {
          const navigation = jest
            .spyOn(router, 'navigateByUrl')
            .mockRejectedValueOnce(new Error())
            .mockResolvedValue(true);
          jest
            .spyOn(router, 'url', 'get')
            .mockReturnValue('/somemodule/jahr/2020');
          const newBerichtsjahr = { schluessel: '2017' } as ReferenzDto;

          service.setBerichtsjahr(newBerichtsjahr);
          await navigation;
          expect(service.selectedBerichtsjahr).not.toEqual(newBerichtsjahr);
          expect(selectedSubscriber).toHaveBeenNthCalledWith(1, latestJahr);
          expect(selectedSubscriber).toHaveBeenNthCalledWith(
            2,
            newBerichtsjahr
          );
          expect(selectedSubscriber).toHaveBeenNthCalledWith(3, latestJahr);
          expect(selectedSubscriber).toHaveBeenCalledTimes(3);
        }
      ));
    });
  });

  describe('when selecting specific Berichtsjahr', () => {
    let jahr: ReferenzDto;
    let selectedSubscriber: jest.Mock<void, [ReferenzDto]>;
    beforeEach(inject(
      [ReferenzenRestControllerService],
      (referenzenController: ReferenzenRestControllerService) => {
        jahr = { schluessel: '2020' } as ReferenzDto;
        spyOn(referenzenController, 'readReferenz').mockReturnValue(
          of(jahr) as Observable<any>
        );
        selectedSubscriber = jest.fn();
        service.selectedBerichtsjahr$.subscribe(selectedSubscriber);
      }
    ));

    it('should complete with Jahr', async () => {
      const selected = await service
        .selectBerichtsjahr(jahr.schluessel)
        .toPromise();

      expect(selected).toEqual(jahr);
    });

    it('should use user data', inject(
      [ReferenzenRestControllerService, BenutzerProfil],
      async (
        referenzenController: ReferenzenRestControllerService,
        benutzer: BenutzerProfil
      ) => {
        await service.selectBerichtsjahr(jahr.schluessel).toPromise();

        expect(referenzenController.readReferenz).toHaveBeenCalledWith(
          'RJAHR',
          2020,
          benutzer.landNr,
          jahr.schluessel
        );
      }
    ));

    it('should emit selected', async () => {
      await service.selectBerichtsjahr(jahr.schluessel).toPromise();

      expect(selectedSubscriber).toHaveBeenCalledWith(jahr);
    });

    it('should have synchronous value', async () => {
      await service.selectBerichtsjahr(jahr.schluessel).toPromise();

      expect(service.selectedBerichtsjahr).toEqual(jahr);
    });
  });

  describe('when setting a filter', () => {
    let alleJahre: Array<ReferenzDto>;
    let jahr: ReferenzDto;
    let jahrVorher: ReferenzDto;
    let jahrNachher: ReferenzDto;

    let filterJahre: Array<ReferenzDto>;

    let selectedSubscriber: jest.Mock<void, [ReferenzDto]>;
    beforeEach(inject(
      [ReferenzenRestControllerService, ReferenzdatenService],
      (
        referenzenController: ReferenzenRestControllerService,
        referenzdatenService: ReferenzdatenService
      ) => {
        jahr = { schluessel: '2020' } as ReferenzDto;
        jahrVorher = { schluessel: '2019' } as ReferenzDto;
        jahrNachher = { schluessel: '2021' } as ReferenzDto;
        alleJahre = [jahr, jahrVorher, jahrNachher];
        spyOn(
          referenzenController,
          'readAktuellesBerichtsjahr'
        ).mockReturnValue(of(jahr) as Observable<any>);
        selectedSubscriber = jest.fn();
        service.selectedBerichtsjahr$.subscribe(selectedSubscriber);
        spyOn(referenzdatenService, 'readReferenzliste').mockReturnValue(
          of(alleJahre)
        );
      }
    ));

    it('should start without filter', async () => {
      await service.filteredBerichtsJahre$.subscribe((filteredJahre) => {
        expect(filteredJahre).toEqual(alleJahre);
      });
    });

    it('should apply filter and reset', async () => {
      filterJahre = [jahr, jahrVorher];
      service.setFilter((jahr: ReferenzDto) =>
        filterJahre.map((j) => j.schluessel).some((s) => s === jahr.schluessel)
      );
      await service.filteredBerichtsJahre$.subscribe((filteredJahre) => {
        expect(filteredJahre).toEqual(filterJahre);
        expect(filteredJahre).not.toContain(jahrNachher);
      });

      service.resetFilter();

      await service.filteredBerichtsJahre$.subscribe((filteredJahre) => {
        expect(filteredJahre).toEqual(alleJahre);
      });
    });
  });
});
