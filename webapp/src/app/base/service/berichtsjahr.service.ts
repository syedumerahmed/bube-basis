import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { ReferenzDto } from '@api/stammdaten';
import {
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import { Router } from '@angular/router';
import { ReferenzdatenService } from '@/referenzdaten/service/referenzdaten.service';
import { ReferenzenRestControllerService } from '@api/referenzdaten';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { LandProperties } from '@/base/model/Land';

type JahrFilterFunction = (jahr: ReferenzDto) => boolean;

@Injectable({ providedIn: 'root' })
export class BerichtsjahrService {
  private static readonly defaultEnabledStatus = true;
  private readonly _berichtsjahrEnabled$ = new BehaviorSubject(
    BerichtsjahrService.defaultEnabledStatus
  );
  private readonly _selectedBerichtsjahr$ = new BehaviorSubject<ReferenzDto>(
    null
  );

  private _defaultBerichtsJahr: Subscription = null;
  private filter: BehaviorSubject<JahrFilterFunction> =
    new BehaviorSubject<JahrFilterFunction>(() => true);

  constructor(
    private readonly referenzenController: ReferenzenRestControllerService,
    private readonly referenzdatenService: ReferenzdatenService,
    private readonly benutzerProfil: BenutzerProfil,
    private readonly router: Router
  ) {
    this.selectedBerichtsjahr$
      .pipe(pairwise())
      .subscribe(([previousJahr, newJahr]) =>
        this.updateUrl(previousJahr, newJahr)
      );
  }

  get alleBerichtsJahre$(): Observable<Array<ReferenzDto>> {
    return this.selectDefaultBerichtsjahr().pipe(
      switchMap((jahr) =>
        this.referenzdatenService.readReferenzliste(
          'RJAHR',
          Number(jahr.schluessel),
          LandProperties.DEUTSCHLAND.nr
        )
      )
    );
  }

  get filteredBerichtsJahre$(): Observable<Array<ReferenzDto>> {
    return combineLatest([this.alleBerichtsJahre$, this.filter]).pipe(
      map(([alleJahre, filterFunc]) => alleJahre.filter(filterFunc))
    );
  }

  get selectedBerichtsjahr$(): Observable<ReferenzDto> {
    return this._selectedBerichtsjahr$.pipe(filter((jahr) => jahr != null));
  }

  get selectedBerichtsjahr(): ReferenzDto {
    if (!this._selectedBerichtsjahr$.value) {
      throw new Error('Kein Berichtsjahr ausgewählt');
    }
    return this._selectedBerichtsjahr$.value;
  }

  selectBerichtsjahr(schluessel: string): Observable<ReferenzDto> {
    if (schluessel === this._selectedBerichtsjahr$.value?.schluessel) {
      return this.selectedBerichtsjahr$;
    }
    return this.referenzenController
      .readReferenz(
        'RJAHR',
        Number(schluessel),
        this.benutzerProfil.landNr,
        schluessel
      )
      .pipe(tap((jahr) => this._selectedBerichtsjahr$.next(jahr)));
  }

  setBerichtsjahr(jahr: ReferenzDto): void {
    this._selectedBerichtsjahr$.next(jahr);
  }

  selectDefaultBerichtsjahr(): Observable<ReferenzDto> {
    if (!this._selectedBerichtsjahr$.value && !this._defaultBerichtsJahr) {
      this._defaultBerichtsJahr = this.referenzenController
        .readAktuellesBerichtsjahr()
        .subscribe((jahr: ReferenzDto) =>
          this._selectedBerichtsjahr$.next(jahr)
        );
    }
    return this.selectedBerichtsjahr$.pipe(take(1));
  }

  get berichtsjahrEnabled$(): Observable<boolean> {
    return this._berichtsjahrEnabled$.pipe(distinctUntilChanged());
  }

  enableDropdown(): void {
    this._berichtsjahrEnabled$.next(true);
  }

  disableDropdown(): void {
    this._berichtsjahrEnabled$.next(false);
  }

  restoreDropdownDefault(): void {
    this._berichtsjahrEnabled$.next(BerichtsjahrService.defaultEnabledStatus);
  }

  private updateUrl(previousJahr: ReferenzDto, newJahr: ReferenzDto): void {
    const url: string = this.router.url.replace(
      `/jahr/${previousJahr.schluessel}`,
      `/jahr/${newJahr.schluessel}`
    );
    this.router
      .navigateByUrl(url)
      .catch(() => this.setBerichtsjahr(previousJahr));
  }

  public resetFilter(): void {
    this.filter.next(() => true);
  }

  public setFilter(filter: JahrFilterFunction): void {
    this.filter.next(filter);
  }
}
