import { TestBed } from '@angular/core/testing';

import { AlertService, AlertTyp } from './alert.service';

describe('AlertService', () => {
  let service: AlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.getAlertMessages()).toHaveLength(0);
  });

  it('should create info', () => {
    service.setSuccess('Info Text');
    expect(service.getAlertMessages()).toHaveLength(1);
    expect(service.getAlertMessages()[0].message).toBe('Info Text');
    expect(service.getAlertMessages()[0].typ).toBe(AlertTyp.SUCCESS);
  });

  it('should create alert', () => {
    service.setAlert('Alert Text');
    expect(service.getAlertMessages()).toHaveLength(1);
    expect(service.getAlertMessages()[0].message).toBe('Alert Text');
    expect(service.getAlertMessages()[0].typ).toBe(AlertTyp.ALERT);
  });

  it('should create warning', () => {
    service.setWarning('Warning Text');
    expect(service.getAlertMessages()).toHaveLength(1);
    expect(service.getAlertMessages()[0].message).toBe('Warning Text');
    expect(service.getAlertMessages()[0].typ).toBe(AlertTyp.WARNING);
  });

  it('should clear Messages', () => {
    service.setAlert('Warning Text');
    expect(service.getAlertMessages()).toHaveLength(1);
    service.clearAlertMessages();
    expect(service.getAlertMessages()).toHaveLength(0);
  });

  it('should stack messages', () => {
    service.setAlert('Am Ältesten');
    service.setWarning('Alt', { label: 'warningAction', route: '' });
    service.setSuccess('Neu');
    expect(service.getAlertMessages()).toStrictEqual([
      { message: 'Neu', typ: AlertTyp.SUCCESS },
      {
        message: 'Alt',
        typ: AlertTyp.WARNING,
        action: { label: 'warningAction', route: '' },
      },
      { message: 'Am Ältesten', typ: AlertTyp.ALERT },
    ]);
  });
});
