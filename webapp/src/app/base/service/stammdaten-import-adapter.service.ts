import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImportService } from '@/base/component/import-modal/import-modal.component';
import {
  BetriebsstaettenRestControllerService,
  DateiDto,
  Validierungshinweis,
} from '@api/stammdaten';

@Injectable({
  providedIn: 'root',
})
export class StammdatenImportAdapterService implements ImportService {
  constructor(
    private readonly betriebsstaettenService: BetriebsstaettenRestControllerService
  ) {}

  aktuelleDatei(): Observable<DateiDto> {
    return this.betriebsstaettenService.existierendeStammdatenDatei();
  }

  dateiLoeschen(): Observable<any> {
    return this.betriebsstaettenService.stammdatenDateiLoeschen();
  }

  import(jahrSchuessel?: string): Observable<any> {
    return this.betriebsstaettenService.importStammdaten(jahrSchuessel);
  }

  upload(datei: File): Observable<any> {
    return this.betriebsstaettenService.uploadStammdaten(datei);
  }

  validiere(): Observable<Array<Validierungshinweis>> {
    return this.betriebsstaettenService.validiereStammdaten();
  }
}
