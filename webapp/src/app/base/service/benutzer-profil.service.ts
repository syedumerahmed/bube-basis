import { Injectable } from '@angular/core';
import { KeycloakOptions, KeycloakService } from 'keycloak-angular';
import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { KeycloakProfile } from 'keycloak-js';
import { Rolle } from '@/base/security/model/Rolle';
import { Router } from '@angular/router';
import {
  DatenberechtigungDto,
  DatenberechtigungFachmoduleEnum,
  DatenberechtigungThemenEnum,
} from '@api/benutzer';

type Attributes = { [key: string]: string[] };

interface KeycloakProfileWithAttributes extends KeycloakProfile {
  attributes: Attributes;
}

const fachmodule: Record<DatenberechtigungFachmoduleEnum, string> = {
  PRTR_LCP: 'PRTR+LCP',
  E_ERKLAERUNG: 'E-Erklaerung',
  RECHERCHE: 'Recherche',
  PROTOTYP: 'Prototyp',
};

const themen: Record<DatenberechtigungThemenEnum, string> = {
  WASSER: 'Wasser',
  ABFALL: 'Abfall',
  LUFT: 'Luft',
  BODEN: 'Boden',
  LCP: 'LCP',
};

@Injectable({ providedIn: 'root' })
export class BenutzerProfilService {
  constructor(
    private readonly keycloak: KeycloakService,
    private readonly router: Router
  ) {}

  private _benutzerProfil: BenutzerProfil;

  private static extractRollen(keycloakRoles: string[]): Rolle[] {
    return keycloakRoles
      .map((s) => s.toUpperCase())
      .map((role) => Rolle[role])
      .filter((role) => role != null);
  }

  private static extractDatenberechtigungen(
    attributes: Attributes
  ): DatenberechtigungDto {
    return {
      land: attributes.Land[0],
      behoerden: attributes.Behoerden,
      akz: attributes.AKZ,
      verwaltungsgebiete: attributes.Verwaltungsgebiete,
      betriebsstaetten: attributes.Betriebsstaetten,
      fachmodule: attributes.Fachmodule?.map(
        (f) => Object.entries(fachmodule).find(([, v]) => f === v)[0]
      ) as DatenberechtigungFachmoduleEnum[],
      themen: attributes.Themen?.map(
        (f) => Object.entries(themen).find(([, v]) => f === v)[0]
      ) as DatenberechtigungThemenEnum[],
    };
  }

  async init(options: KeycloakOptions): Promise<void> {
    const isLoggedIn =
      (await this.keycloak.init(options)) && (await this.keycloak.isLoggedIn());

    if (!isLoggedIn) {
      await this.keycloak.login({ redirectUri: window.location.href });
    }
    this._benutzerProfil = await this.loadBenutzerProfil();
  }

  get benutzerProfil(): BenutzerProfil {
    if (!this._benutzerProfil) {
      throw Error('BenutzerProfil nicht initialisiert!');
    }
    return this._benutzerProfil;
  }

  private async loadBenutzerProfil(): Promise<BenutzerProfil> {
    const keycloakProfile =
      (await this.keycloak.loadUserProfile()) as KeycloakProfileWithAttributes;
    return new BenutzerProfil(
      keycloakProfile.username,
      keycloakProfile.firstName,
      keycloakProfile.lastName,
      keycloakProfile.email,
      BenutzerProfilService.extractRollen(this.keycloak.getUserRoles(true)),
      BenutzerProfilService.extractDatenberechtigungen(
        keycloakProfile.attributes
      )
    );
  }

  openBenutzerProfil(): void {
    this.router.navigate([
      'benutzerverwaltung',
      this.benutzerProfil.isBehoerdenbenutzer ? 'behoerden' : 'betreiber',
      'benutzer',
      this.benutzerProfil.username,
    ]);
  }

  async logout(): Promise<void> {
    await this.keycloak.logout();
  }
}
