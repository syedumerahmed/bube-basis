import { BenutzerProfilService } from './benutzer-profil.service';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { Rolle } from '@/base/security/model/Rolle';
import { MockService } from 'ng-mocks';
import { Land, LandProperties } from '@/base/model/Land';
import { Router } from '@angular/router';
import spyOn = jest.spyOn;
import {
  DatenberechtigungFachmoduleEnum,
  DatenberechtigungThemenEnum,
} from '@api/benutzer';

describe('BenutzerProfilService', () => {
  let keycloakMock: KeycloakService;
  let routerMock: Router;
  let service: BenutzerProfilService;

  beforeEach(() => {
    keycloakMock = MockService(KeycloakService);
    routerMock = MockService(Router);
    service = new BenutzerProfilService(keycloakMock, routerMock);
    spyOn(keycloakMock, 'init').mockResolvedValue(true);
    spyOn(keycloakMock, 'isLoggedIn').mockResolvedValue(true);
  });

  describe('with profile and roles', () => {
    let profile: KeycloakProfile;
    let roles: Rolle[];
    beforeEach(async () => {
      profile = {
        username: 'mjoergens',
        email: 'mj@wps.de',
        firstName: 'Malte',
        lastName: 'Jörgens',
        attributes: {
          Land: [LandProperties.DEUTSCHLAND.nr],
          Behoerden: ['12'],
          AKZ: ['123456789'],
          Verwaltungsgebiete: ['03101'],
          Betriebsstaetten: ['10'],
          Fachmodule: ['Prototyp'],
          Themen: ['Luft', 'Wasser'],
        },
      } as KeycloakProfile;
      roles = [Rolle.GESAMTADMIN, Rolle.BEHOERDE_READ];

      spyOn(keycloakMock, 'loadUserProfile').mockResolvedValue(profile);
      spyOn(keycloakMock, 'getUserRoles').mockReturnValue(roles);
      await service.init({});
    });

    it('should initialize keycloak', () => {
      expect(keycloakMock.init).toHaveBeenCalledTimes(1);
    });

    it('should check for existing login', () => {
      expect(keycloakMock.isLoggedIn).toHaveBeenCalledTimes(1);
    });

    it('should load and map benutzer', () => {
      const benutzer = service.benutzerProfil;
      expect(benutzer).toBeDefined();
      expect(benutzer.displayName).toBe('Malte Jörgens');
      expect(benutzer.hasRolle(Rolle.GESAMTADMIN)).toBe(true);
      expect(benutzer.land).toBe(Land.DEUTSCHLAND);
      expect(benutzer.datenberechtigung).toEqual({
        land: LandProperties.DEUTSCHLAND.nr,
        behoerden: ['12'],
        akz: ['123456789'],
        verwaltungsgebiete: ['03101'],
        betriebsstaetten: ['10'],
        fachmodule: [DatenberechtigungFachmoduleEnum.PROTOTYP],
        themen: [
          DatenberechtigungThemenEnum.LUFT,
          DatenberechtigungThemenEnum.WASSER,
        ],
      });
    });
  });
});
