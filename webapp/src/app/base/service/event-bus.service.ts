import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export class EmitEvent<T = unknown> {
  constructor(public name: Event, public value?: T) {}
}

export enum Event {
  BetriebsstaettenImportFertig,
  BetreiberBenutzerImportFertig,
  ReferenzdatenImportFertig,
  BetriebsstaettenExportFertig,
  BehoerdenImportFertig,
  KomplexpruefungFertig,
  BerichtsdatenImportFertig,
  BerichtsdatenExportFertig,
  XeurTeilberichtFertig,
}

@Injectable({
  providedIn: 'root',
})
export class EventBusService {
  private subject$ = new Subject<EmitEvent>();

  on(...event: Event[]): Observable<unknown> {
    return this.subject$.pipe(
      filter((e: EmitEvent) => event.includes(e.name)),
      map((e: EmitEvent) => e.value)
    );
  }

  emit(event: EmitEvent): void {
    this.subject$.next(event);
  }
}
