import { TestBed } from '@angular/core/testing';

import { JobStatusService } from './job-status.service';
import { JobStatusEnum } from '@api';

describe('JobStatusService', () => {
  let service: JobStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('job should not be running', () => {
    expect(service.jobIsRunning()).toBe(false);
  });

  it('job should be running', () => {
    service.jobStarted();
    expect(service.jobIsRunning()).toBe(true);
  });

  it('job should not be running after calling jobEnded', () => {
    service.jobStarted();
    service.jobEnded({ jobName: JobStatusEnum.STAMMDATEN_IMPORTIEREN });
    expect(service.jobIsRunning()).toBe(false);
  });
});
