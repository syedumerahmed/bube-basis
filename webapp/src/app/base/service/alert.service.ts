import { Injectable } from '@angular/core';
import { EMPTY, OperatorFunction } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { JobActionDto } from '@api';

export interface ErrorAttributes {
  error: string;
  message?: string;
  path?: string;
  status?: number;
  timestamp?: string;
  exception?: string;
}

export enum AlertTyp {
  ALERT,
  WARNING,
  SUCCESS,
}

export interface AlertMessage {
  message: string;
  typ: AlertTyp;
  action?: JobActionDto;
}

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  private alertMessages: AlertMessage[] = [];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
  public static getAlertText(error: any): string {
    if (error.error?.error) {
      return `BUBE-Server Fehler: ${error.error.error} ${error.error.status} ${error.error.path}`;
    } else if (error.error) {
      return error.error;
    } else if (error.message) {
      return 'BUBE-Server Fehler: ' + error.message;
    } else {
      return error;
    }
  }

  public catchError<T>(message?: string): OperatorFunction<T, T> {
    return catchError((err) => {
      this.setAlert(message || err);
      return EMPTY;
    });
  }

  getAlertMessages(): AlertMessage[] {
    return this.alertMessages;
  }

  clearAlertMessages(): void {
    this.alertMessages = [];
  }

  clearAlertMessage(alertMessage: AlertMessage): void {
    const index = this.alertMessages.findIndex(
      (message) => alertMessage === message
    );
    if (index !== -1) {
      this.alertMessages.splice(index, 1);
    }
  }

  setSuccess(infoText: string, action?: JobActionDto): void {
    this.addAlertMessage(infoText, AlertTyp.SUCCESS, action);
  }

  setSaveWarning(): void {
    this.setWarning('Sie haben die Daten noch nicht gespeichert!');
  }

  setWarning(warning: string, action?: JobActionDto): void {
    this.addAlertMessage(warning, AlertTyp.WARNING, action);
  }

  setAlert(error: unknown, action?: JobActionDto): void {
    const message = AlertService.getAlertText(error);
    this.addAlertMessage(message, AlertTyp.ALERT, action);
  }

  private addAlertMessage(
    message: string,
    typ: AlertTyp,
    action?: JobActionDto
  ): void {
    const alert: AlertMessage = {
      message,
      typ,
    };
    if (action) {
      alert.action = action;
    }
    this.alertMessages.unshift(alert);
  }
}
