import { DatenberechtigungDto } from '@api/benutzer';
import { Land, LandProperties, lookupLand } from '@/base/model/Land';
import { Rolle } from '@/base/security/model/Rolle';

export class BenutzerProfil {
  private readonly _datenberechtigung: Required<DatenberechtigungDto>;
  private readonly _land: Land;

  constructor(
    readonly username: string,
    readonly vorname: string,
    readonly nachname: string,
    readonly email: string,
    readonly rollen: Rolle[],
    datenberechtigung: DatenberechtigungDto
  ) {
    this._land = lookupLand(datenberechtigung.land);
    if (!this._land) {
      throw Error('Ungültiges Land: ' + datenberechtigung.land);
    }
    this._datenberechtigung = {
      land: datenberechtigung.land,
      behoerden: datenberechtigung.behoerden || [],
      akz: datenberechtigung.akz || [],
      verwaltungsgebiete: datenberechtigung.verwaltungsgebiete || [],
      betriebsstaetten: datenberechtigung.betriebsstaetten || [],
      fachmodule: datenberechtigung.fachmodule || [],
      themen: datenberechtigung.themen || [],
    };
  }

  get datenberechtigung(): Required<DatenberechtigungDto> {
    return this._datenberechtigung;
  }

  get land(): Land {
    return this._land;
  }

  get displayName(): string {
    return this.vorname && this.nachname
      ? `${this.vorname} ${this.nachname}`
      : this.username;
  }

  hasRolle(rolle: Rolle): boolean {
    return this.rollen.includes(rolle);
  }

  hasAllRollen(...rollen: Rolle[]): boolean {
    return rollen.every((rolle) => this.rollen.includes(rolle));
  }

  hasAnyRolle(...rollen: Rolle[]): boolean {
    return rollen.some((rolle) => this.rollen.includes(rolle));
  }

  get canSeeStammdatenModule(): boolean {
    return this.hasAnyRolle(Rolle.LAND_READ, Rolle.BEHOERDE_READ);
  }

  get canSeeStammdatenBetreiberModule(): boolean {
    return this.hasRolle(Rolle.BETREIBER);
  }

  get canSeeReferenzdatenModule(): boolean {
    return this.hasAnyRolle(
      Rolle.REFERENZLISTEN_LAND_READ,
      Rolle.REFERENZLISTEN_BUND_READ
    );
  }

  get canSeeBenutzerverwaltungModule(): boolean {
    return this.hasRolle(Rolle.BENUTZERVERWALTUNG_READ);
  }

  get canSeeKomplexpruefungModule(): boolean {
    return this.hasRolle(Rolle.LAND_READ);
  }

  get canDeleteStammdaten(): boolean {
    return this.hasAnyRolle(Rolle.LAND_DELETE, Rolle.BEHOERDE_DELETE);
  }

  get canWriteStammdaten(): boolean {
    return this.hasAnyRolle(Rolle.LAND_WRITE, Rolle.BEHOERDE_WRITE);
  }

  get canDeleteBenutzer(): boolean {
    return this.hasRolle(Rolle.BENUTZERVERWALTUNG_DELETE);
  }

  get canWriteBenutzer(): boolean {
    return this.hasRolle(Rolle.BENUTZERVERWALTUNG_WRITE);
  }

  get canReadReferenzdaten(): boolean {
    return this.hasAnyRolle(
      Rolle.REFERENZLISTEN_LAND_READ,
      Rolle.REFERENZLISTEN_BUND_READ
    );
  }

  get canSeeReferenzLand(): boolean {
    return this.hasRolle(Rolle.REFERENZLISTEN_LAND_READ);
  }

  get canWriteReferenzLand(): boolean {
    return this.hasRolle(Rolle.REFERENZLISTEN_LAND_WRITE);
  }

  get canDeleteReferenzLand(): boolean {
    return this.hasRolle(Rolle.REFERENZLISTEN_LAND_DELETE);
  }

  get canWriteReferenzBund(): boolean {
    return this.hasRolle(Rolle.REFERENZLISTEN_BUND_WRITE);
  }

  get canDeleteReferenzBund(): boolean {
    return this.hasRolle(Rolle.REFERENZLISTEN_BUND_DELETE);
  }

  get isBehoerdenbenutzer(): boolean {
    return this.hasRolle(Rolle.BEHOERDENBENUTZER);
  }

  get zustaendigeBehoerden(): string[] {
    return this.datenberechtigung.behoerden;
  }

  get landNr(): string {
    return LandProperties[this.land].nr;
  }

  get isGesamtadmin(): boolean {
    return this.hasRolle(Rolle.GESAMTADMIN);
  }

  get isLandesadmin(): boolean {
    return this.hasRolle(Rolle.ADMIN);
  }

  get canSeeEURegBerichtsdaten(): boolean {
    return this.hasAnyRolle(Rolle.BEHOERDE_READ, Rolle.LAND_READ);
  }

  get canWriteEURegBerichtsdaten(): boolean {
    return this.hasAnyRolle(Rolle.BEHOERDE_WRITE, Rolle.LAND_WRITE);
  }

  get canWriteRegeln(): boolean {
    return this.hasAnyRolle(Rolle.LAND_WRITE);
  }

  get canDeleteRegeln(): boolean {
    return this.hasAnyRolle(Rolle.LAND_DELETE);
  }
}
