import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';

export const GESAMTADMIN_DUMMY = (): BenutzerProfil =>
  new BenutzerProfil(
    'Gesamtadmin',
    null,
    null,
    null,
    Object.values(Rolle).filter((rolle) => rolle !== Rolle.BETREIBER),
    { land: '00' }
  );

export const LANDADMIN_DUMMY = (): BenutzerProfil =>
  new BenutzerProfil(
    'Landadmin',
    null,
    null,
    null,
    [
      Rolle.ADMIN,
      Rolle.REFERENZLISTEN_USE,
      ...Object.values(Rolle).filter(
        (rolle) =>
          rolle.startsWith('REFERENZLISTEN_LAND') ||
          rolle.startsWith('LAND') ||
          rolle.startsWith('BEHOERDE')
      ),
    ],
    { land: '01' }
  );

export const BENUTZERVERWALTUNG_DUMMY_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'Benutzerverwaltung',
    null,
    null,
    null,
    Object.values(Rolle).filter((rolle) =>
      rolle.startsWith('BENUTZERVERWALTUNG')
    ),
    { land: '00' }
  );

export const REFERENZLISTEN_BUND_DUMMY_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'ReferenzlistenBund',
    null,
    null,
    null,
    Object.values(Rolle).filter((rolle) =>
      rolle.startsWith('REFERENZLISTEN_BUND')
    ),
    { land: '00' }
  );

export const REFERENZLISTEN_LAND_DUMMY_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'ReferenzlistenLand',
    null,
    null,
    null,
    Object.values(Rolle).filter((rolle) =>
      rolle.startsWith('REFERENZLISTEN_LAND')
    ),
    { land: '00' }
  );

export const LAND_TEST_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'land_test_user',
    null,
    null,
    null,
    [Rolle.LAND_WRITE, Rolle.LAND_READ, Rolle.REFERENZLISTEN_USE],
    { land: '02' }
  );
export const LAND_LOESCHEN_TEST_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'land_loeschen_test_user',
    null,
    null,
    null,
    [
      Rolle.LAND_DELETE,
      Rolle.LAND_WRITE,
      Rolle.LAND_READ,
      Rolle.REFERENZLISTEN_USE,
    ],
    { land: '02' }
  );
export const BEHOERDE_READ_TEST_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'behoerde_read_test_user',
    null,
    null,
    null,
    [Rolle.BEHOERDE_READ, Rolle.REFERENZLISTEN_USE],
    { land: '01', behoerden: ['01'] }
  );
export const BETREIBER_TEST_USER = (): BenutzerProfil =>
  new BenutzerProfil(
    'betreiber_test_user',
    null,
    null,
    null,
    [
      Rolle.BETREIBER,
      Rolle.BENUTZERVERWALTUNG_DELETE,
      Rolle.BENUTZERVERWALTUNG_WRITE,
      Rolle.BENUTZERVERWALTUNG_READ,
    ],
    {
      land: '00',
      betriebsstaetten: ['9', '13'],
    }
  );
