export enum Rolle {
  BEHOERDE_READ = 'BEHOERDE_READ',
  BEHOERDE_WRITE = 'BEHOERDE_WRITE',
  BEHOERDE_DELETE = 'BEHOERDE_DELETE',

  LAND_READ = 'LAND_READ',
  LAND_WRITE = 'LAND_WRITE',
  LAND_DELETE = 'LAND_DELETE',

  ADMIN = 'ADMIN',
  GESAMTADMIN = 'GESAMTADMIN',

  REFERENZLISTEN_BUND_READ = 'REFERENZLISTEN_BUND_READ',
  REFERENZLISTEN_BUND_WRITE = 'REFERENZLISTEN_BUND_WRITE',
  REFERENZLISTEN_BUND_DELETE = 'REFERENZLISTEN_BUND_DELETE',

  REFERENZLISTEN_LAND_READ = 'REFERENZLISTEN_LAND_READ',
  REFERENZLISTEN_LAND_WRITE = 'REFERENZLISTEN_LAND_WRITE',
  REFERENZLISTEN_LAND_DELETE = 'REFERENZLISTEN_LAND_DELETE',
  REFERENZLISTEN_USE = 'REFERENZLISTEN_USE',

  BENUTZERVERWALTUNG_READ = 'BENUTZERVERWALTUNG_READ',
  BENUTZERVERWALTUNG_WRITE = 'BENUTZERVERWALTUNG_WRITE',
  BENUTZERVERWALTUNG_DELETE = 'BENUTZERVERWALTUNG_DELETE',

  BETREIBER = 'BETREIBER',
  BEHOERDENBENUTZER = 'BEHOERDENBENUTZER',
}

export const RolleProperties: Record<Rolle, { display: string }> = {
  BEHOERDE_READ: { display: 'Behörde lesen' },
  BEHOERDE_WRITE: { display: 'Behörde schreiben' },
  BEHOERDE_DELETE: { display: 'Behörde löschen' },

  LAND_READ: { display: 'Land lesen' },
  LAND_WRITE: { display: 'Land schreiben' },
  LAND_DELETE: { display: 'Land löschen' },

  ADMIN: { display: 'Admin' },
  GESAMTADMIN: { display: 'Gesamtadmin' },

  REFERENZLISTEN_BUND_READ: { display: 'Referenzlisten Bund lesen' },
  REFERENZLISTEN_BUND_WRITE: { display: 'Referenzlisten Bund schreiben' },
  REFERENZLISTEN_BUND_DELETE: { display: 'Referenzlisten Bund löschen' },

  REFERENZLISTEN_LAND_READ: { display: 'Referenzlisten Land lesen' },
  REFERENZLISTEN_LAND_WRITE: { display: 'Referenzlisten Land schreiben' },
  REFERENZLISTEN_LAND_DELETE: { display: 'Referenzlisten Land löschen' },
  REFERENZLISTEN_USE: { display: 'Referenzlisten benutzen' },

  BENUTZERVERWALTUNG_READ: { display: 'Benutzerverwaltung lesen' },
  BENUTZERVERWALTUNG_WRITE: { display: 'Benutzerverwaltung schreiben' },
  BENUTZERVERWALTUNG_DELETE: { display: 'Benutzerverwaltung löschen' },

  BETREIBER: { display: 'Betreiber' },
  BEHOERDENBENUTZER: { display: 'Behördenbenutzer' },
};
