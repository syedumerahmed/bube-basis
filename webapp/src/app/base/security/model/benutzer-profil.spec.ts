import { BenutzerProfil } from '@/base/security/model/benutzer-profil';
import { Rolle } from '@/base/security/model/Rolle';
import {
  BEHOERDE_READ_TEST_USER,
  BENUTZERVERWALTUNG_DUMMY_USER,
  GESAMTADMIN_DUMMY,
  REFERENZLISTEN_BUND_DUMMY_USER,
  REFERENZLISTEN_LAND_DUMMY_USER,
} from '@/base/security/model/benutzer-profil.mock';
import { Land, LandProperties } from '@/base/model/Land';

describe('BenutzerProfil', () => {
  describe('valid Benutzer', () => {
    let benutzerProfil: BenutzerProfil;
    beforeEach(() => {
      benutzerProfil = new BenutzerProfil(
        'mjoergens',
        'Malte',
        'Jörgens',
        'mj@wps.de',
        [
          Rolle.GESAMTADMIN,
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.BENUTZERVERWALTUNG_DELETE,
        ],
        { land: LandProperties.DEUTSCHLAND.nr }
      );
    });

    it('should have displayName', () => {
      expect(benutzerProfil.displayName).toBe('Malte Jörgens');
    });

    it('should map land', () => {
      expect(benutzerProfil.land).toBe(Land.DEUTSCHLAND);
    });

    it('should have landNr', () => {
      expect(benutzerProfil.landNr).toBe(LandProperties.DEUTSCHLAND.nr);
    });

    it('should have Rolle GESAMTADMIN', () => {
      expect(benutzerProfil.hasRolle(Rolle.GESAMTADMIN)).toBe(true);
    });

    it('should not have Rolle REFERENZLISTEN_BUND_READ', () => {
      expect(benutzerProfil.hasRolle(Rolle.REFERENZLISTEN_BUND_READ)).toBe(
        false
      );
    });

    it('should have both of two Roles', () => {
      expect(
        benutzerProfil.hasAllRollen(
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.BENUTZERVERWALTUNG_DELETE
        )
      ).toBe(true);
    });

    it('should have not both of two Roles', () => {
      expect(
        benutzerProfil.hasAllRollen(
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.REFERENZLISTEN_LAND_READ
        )
      ).toBe(false);
    });

    it('should have any of two Roles', () => {
      expect(
        benutzerProfil.hasAnyRolle(
          Rolle.BENUTZERVERWALTUNG_READ,
          Rolle.REFERENZLISTEN_LAND_READ
        )
      ).toBe(true);
    });

    it('should have none of two Roles', () => {
      expect(
        benutzerProfil.hasAnyRolle(
          Rolle.REFERENZLISTEN_LAND_DELETE,
          Rolle.REFERENZLISTEN_LAND_READ
        )
      ).toBe(false);
    });

    it('should fall back to username without real name', () => {
      const username = 'mjoergens';
      const benutzer = new BenutzerProfil(username, null, null, null, [], {
        land: '00',
      });

      expect(benutzer.displayName).toBe(username);
    });
  });

  it('Behördennutzer should have Behoerden', () => {
    expect(BEHOERDE_READ_TEST_USER().zustaendigeBehoerden).toHaveLength(1);
  });

  describe('Gesamtadmin', () => {
    (
      [
        ['canSeeStammdatenModule', true],
        ['canSeeReferenzdatenModule', true],
        ['canSeeBenutzerverwaltungModule', true],
      ] as Array<[keyof BenutzerProfil, boolean]>
    ).forEach(([moduleAccessFn, shouldHaveAccess]) => {
      it(`${moduleAccessFn} should be ${shouldHaveAccess}`, () => {
        expect(GESAMTADMIN_DUMMY()[moduleAccessFn]).toBe(shouldHaveAccess);
      });
    });
  });

  describe('Benutzerverwaltung', () => {
    (
      [
        ['canSeeStammdatenModule', false],
        ['canSeeReferenzdatenModule', false],
        ['canSeeBenutzerverwaltungModule', true],
      ] as Array<[keyof BenutzerProfil, boolean]>
    ).forEach(([moduleAccessFn, shouldHaveAccess]) => {
      it(`${moduleAccessFn} should be ${shouldHaveAccess}`, () => {
        expect(BENUTZERVERWALTUNG_DUMMY_USER()[moduleAccessFn]).toBe(
          shouldHaveAccess
        );
      });
    });
  });

  describe.each([
    ['ReferenzlistenBundUser', REFERENZLISTEN_BUND_DUMMY_USER()],
    ['ReferenzlistenLandUser', REFERENZLISTEN_LAND_DUMMY_USER()],
  ])('%s', (name, user) => {
    it.each([
      ['canSeeStammdatenModule', false],
      ['canSeeReferenzdatenModule', true],
      ['canSeeBenutzerverwaltungModule', false],
    ] as Array<[keyof BenutzerProfil, boolean]>)(
      '%s should be %s',
      (moduleAccessFn, shouldHaveAccess) => {
        expect(user[moduleAccessFn]).toBe(shouldHaveAccess);
      }
    );
  });
});
