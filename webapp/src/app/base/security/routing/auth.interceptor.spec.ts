import { inject, TestBed } from '@angular/core/testing';

import { MockService } from 'ng-mocks';
import { BenutzerProfilService } from '@/base/service/benutzer-profil.service';
import { HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { AuthInterceptor } from '@/base/security/routing/auth.interceptor';

describe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthInterceptor,
        {
          provide: BenutzerProfilService,
          useValue: MockService(BenutzerProfilService),
        },
      ],
    });
    interceptor = TestBed.inject(AuthInterceptor);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

  describe('intercept', () => {
    const next = { handle: jest.fn() };

    it('should logout on 401', inject(
      [BenutzerProfilService],
      async (benutzerService: BenutzerProfilService) => {
        jest.spyOn(benutzerService, 'logout');
        next.handle.mockImplementation(() => throwError({ status: 401 }));
        await interceptor.intercept(null, next).toPromise();
        expect(benutzerService.logout).toHaveBeenCalled();
      }
    ));

    it('should rethrow other errors', async () => {
      const error: HttpErrorResponse = new HttpErrorResponse({ status: 403 });
      next.handle.mockImplementation(() => throwError(error));

      await expect(
        interceptor.intercept(null, next).toPromise()
      ).rejects.toEqual(error);
    });

    it('should not do anything when no error', inject(
      [BenutzerProfilService],
      async (benutzerService: BenutzerProfilService) => {
        jest.spyOn(benutzerService, 'logout');
        next.handle.mockImplementation(() => of(null));
        await interceptor.intercept(null, next).toPromise();
        expect(benutzerService.logout).not.toHaveBeenCalled();
      }
    ));
  });
});
