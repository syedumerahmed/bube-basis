import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingNotFoundComponent } from './routing-not-found.component';
import { MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';

describe('NotFoundComponent', () => {
  let component: RoutingNotFoundComponent;
  let fixture: ComponentFixture<RoutingNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoutingNotFoundComponent],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();

    fixture = TestBed.createComponent(RoutingNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
