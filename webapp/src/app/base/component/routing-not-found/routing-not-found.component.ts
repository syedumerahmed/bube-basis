import { Component } from '@angular/core';
import { ClarityIcons, exclamationCircleIcon } from '@cds/core/icon';

ClarityIcons.addIcons(exclamationCircleIcon);

@Component({
  selector: 'app-not-found',
  templateUrl: './routing-not-found.component.html',
  styleUrls: ['./routing-not-found.component.scss'],
})
export class RoutingNotFoundComponent {}
