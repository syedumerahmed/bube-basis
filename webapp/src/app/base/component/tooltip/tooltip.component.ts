import { Component, Input } from '@angular/core';
import { ClarityIcons, infoCircleIcon } from '@cds/core/icon';

ClarityIcons.addIcons(infoCircleIcon);

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent {
  @Input()
  positionBottom = false;
}
