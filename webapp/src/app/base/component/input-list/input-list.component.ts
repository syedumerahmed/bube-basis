import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@/base/forms';
import { ClarityIcons, timesIcon } from '@cds/core/icon';

ClarityIcons.addIcons(timesIcon);

@Component({
  selector: 'app-input-list',
  templateUrl: './input-list.component.html',
  styleUrls: ['./input-list.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputListComponent),
      multi: true,
    },
  ],
})
export class InputListComponent implements ControlValueAccessor<Array<string>> {
  @Input()
  placeholder: string;

  formDisabled: boolean;

  inputList: Array<string> = [];

  inputValue = '';

  onChange: (value: Array<string>) => void = () => {
    //do nothing
  };
  onTouch: () => void = () => {
    //do nothing
  };

  registerOnChange(fn: (value: Array<string>) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  writeValue(obj: Array<string>): void {
    this.inputList = obj;
    this.onChange(obj);
  }

  setDisabledState(isDisabled: boolean): void {
    this.formDisabled = isDisabled;
  }

  onEnterAdd(event: KeyboardEvent): void {
    // Verhindere Clarity default "enter" handlers
    event.preventDefault();
    this.addItem(this.inputValue);
  }

  onBackspaceRemoveLast(): void {
    if (!this.inputValue) {
      this.removeItem(-1);
    }
  }

  onButtonAdd(): void {
    this.addItem(this.inputValue);
  }

  private addItem(value: string): void {
    const item = value.trim();
    if (item !== '') {
      this.inputList.push(item);
      this.hasChanged(this.inputList);
    }
    this.inputValue = '';
  }

  removeItem(index: number): void {
    this.inputList.splice(index, 1);
    this.hasChanged(this.inputList);
  }

  private hasChanged(value: string[]): void {
    this.onTouch();
    this.onChange(value);
  }
}
