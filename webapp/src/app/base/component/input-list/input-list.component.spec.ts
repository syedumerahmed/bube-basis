import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputListComponent } from './input-list.component';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';
import { MockModule } from 'ng-mocks';
import { By } from '@angular/platform-browser';
import spyOn = jest.spyOn;

describe('InputListComponent', () => {
  let component: InputListComponent;
  let fixture: ComponentFixture<InputListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
      declarations: [InputListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(InputListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'onChange');
    spyOn(component, 'onTouch');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('write value', () => {
    const input: Array<string> = ['Nr 1', 'Nr 2', 'Nr 3'];

    beforeEach(() => {
      component.writeValue(input);
    });

    it('should write correct value to list', () => {
      expect(component.inputList).toStrictEqual(input);
    });
    it('should call onChange', () => {
      expect(component.onChange).toHaveBeenCalledWith(input);
    });
    it('should not call onTouch', () => {
      expect(component.onTouch).not.toHaveBeenCalledWith(input);
    });
  });

  describe('on Enter add', () => {
    const event: KeyboardEvent = new KeyboardEvent('keydown');

    beforeEach(() => {
      spyOn(event, 'preventDefault');
      component.inputList.push('hello');
      component.inputValue = 'world';

      component.onEnterAdd(event);
    });

    it('should prevent default', () => {
      expect(event.preventDefault).toHaveBeenCalled();
    });
    it('should clear input value', () => {
      expect(component.inputValue).toBeFalsy();
    });
    it('should call onChange', () => {
      expect(component.onChange).toHaveBeenCalled();
    });
    it('should call onTouch', () => {
      expect(component.onTouch).toHaveBeenCalled();
    });
    it('should add value to list', () => {
      expect(component.inputList).toStrictEqual(['hello', 'world']);
    });
  });

  describe('on backspace remove last', () => {
    describe('empty value', () => {
      beforeEach(() => {
        component.inputList = ['hello', 'world'];
        component.inputValue = '';
        component.onBackspaceRemoveLast();
      });
      it('should remove last item', () => {
        expect(component.inputList).toStrictEqual(['hello']);
      });
      it('should call onChange', () => {
        expect(component.onChange).toHaveBeenCalled();
      });
      it('should call onTouch', () => {
        expect(component.onTouch).toHaveBeenCalled();
      });
      it('should remove multiple times', () => {
        component.onBackspaceRemoveLast();
        expect(component.inputList).toStrictEqual([]);
      });
    });
    it('should not remove when value typed in', () => {
      component.inputList = ['hello', 'world'];
      component.inputValue = 'v';
      component.onBackspaceRemoveLast();
      expect(component.inputList).toStrictEqual(['hello', 'world']);
    });
  });

  describe('remove item on click', () => {
    beforeEach(() => {
      component.inputList = ['hello', 'angular', 'world'];
      fixture.detectChanges();
      const removeBtns = fixture.debugElement.queryAll(
        By.css('.clr-combobox-remove-btn')
      );
      removeBtns[1].nativeElement.click();
    });
    it('should remove selected item', () => {
      expect(component.inputList).toStrictEqual(['hello', 'world']);
    });
    it('should call onChange', () => {
      expect(component.onChange).toHaveBeenCalledWith(['hello', 'world']);
    });
    it('should call onTouch', () => {
      expect(component.onTouch).toHaveBeenCalledWith();
    });
  });
});
