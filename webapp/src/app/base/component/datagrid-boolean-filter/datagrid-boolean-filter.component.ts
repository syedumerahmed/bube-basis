import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ClrDatagridFilter, ClrDatagridFilterInterface } from '@clr/angular';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-datagrid-boolean-filter',
  templateUrl: './datagrid-boolean-filter.component.html',
  styleUrls: ['./datagrid-boolean-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatagridBooleanFilterComponent<T>
  implements ClrDatagridFilterInterface<T>
{
  @Input()
  property: string = null;
  @Input()
  accessor: (item: T) => boolean = null;

  @Input()
  trueLabel;
  @Input()
  falseLabel;

  trueSelected = true;
  falseSelected = true;

  changes = new Subject();

  constructor(private filterContainer: ClrDatagridFilter) {
    filterContainer.setFilter(this);
  }

  accepts(item: T): boolean {
    const boolProperty: boolean = this.property
      ? item[this.property]
      : this.accessor(item);
    return (
      (boolProperty && this.trueSelected) ||
      (!boolProperty && this.falseSelected)
    );
  }

  isActive(): boolean {
    return !(this.trueSelected && this.falseSelected);
  }
}
