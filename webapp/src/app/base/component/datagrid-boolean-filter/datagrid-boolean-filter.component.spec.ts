import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MockModule, MockProviders } from 'ng-mocks';
import { ClarityModule, ClrDatagridFilter } from '@clr/angular';
import { FormsModule } from '@angular/forms';
import { DatagridBooleanFilterComponent } from '@/base/component/datagrid-boolean-filter/datagrid-boolean-filter.component';

describe('DatagridBooleanFilter', () => {
  let component: DatagridBooleanFilterComponent<never>;
  let fixture: ComponentFixture<DatagridBooleanFilterComponent<never>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DatagridBooleanFilterComponent],
      imports: [MockModule(ClarityModule), MockModule(FormsModule)],
      providers: [MockProviders(ClrDatagridFilter)],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatagridBooleanFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
