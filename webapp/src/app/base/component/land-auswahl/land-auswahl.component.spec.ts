import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandAuswahlComponent } from './land-auswahl.component';
import { Land, LandProperties } from '@/base/model/Land';
import { MockComponent, MockModule } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';

describe('LandAuswahlComponent', () => {
  let component: LandAuswahlComponent;
  let fixture: ComponentFixture<LandAuswahlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MockComponent(TooltipComponent), LandAuswahlComponent],
      imports: [
        MockModule(ClarityModule),
        MockModule(ReactiveFormsModule),
        MockModule(FormsModule),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandAuswahlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('no germany', () => {
    beforeEach(() => {
      component.canSelectDeutschland = false;
      component.ngOnChanges();
    });

    it('should not allow germany as option', () => {
      expect(component.landesListe).not.toContain(LandProperties.DEUTSCHLAND);
    });
  });

  describe('with germany', () => {
    beforeEach(() => {
      component.canSelectDeutschland = true;
      component.ngOnChanges();
    });

    it('should allow germany as option', () => {
      expect(component.landesListe).toEqual(Object.values(LandProperties));
    });

    describe('select', () => {
      let subscriber;
      beforeEach(() => {
        subscriber = jest.fn();
        component.landSelected.subscribe(subscriber);

        component.land.setValue(Land.BERLIN);
        component.selectLand();
      });

      it('shoudl emit selected Land', () => {
        expect(subscriber).toHaveBeenCalledWith(Land.BERLIN);
      });
    });
  });
});
