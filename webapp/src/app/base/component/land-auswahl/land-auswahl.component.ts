import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Land, LandProperties } from '@/base/model/Land';
import { FormControl } from '@/base/forms';
import { ClrForm } from '@clr/angular';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-land-auswahl',
  templateUrl: './land-auswahl.component.html',
  styleUrls: ['./land-auswahl.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandAuswahlComponent implements OnInit, OnChanges {
  @Input()
  open: boolean;
  @Input()
  canSelectDeutschland = true;

  @Output()
  openChange = new EventEmitter<boolean>();
  @Output()
  landSelected = new EventEmitter<Land>();

  land: FormControl<Land>;
  landesListe: Array<{ nr: string; display: string }>;

  @ViewChild(ClrForm, { static: true }) clrForm: ClrForm;

  ngOnInit(): void {
    this.land = new FormControl<Land>(null, Validators.required);
  }

  ngOnChanges(): void {
    this.landesListe = Object.values(LandProperties).filter(
      (land) => this.canSelectDeutschland || land !== LandProperties.DEUTSCHLAND
    );
  }

  closeLandAuswahlDialog(): void {
    this.openChange.emit(false);
    this.open = false;
  }

  selectLand(): void {
    if (this.land.invalid) {
      this.clrForm.markAsTouched();
    } else {
      this.landSelected.emit(this.land.value);
      this.closeLandAuswahlDialog();
    }
  }
}
