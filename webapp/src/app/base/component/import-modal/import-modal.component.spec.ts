import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MockModule, MockProvider, MockService } from 'ng-mocks';
import { JobStatusService } from '@/base/service/job-status.service';
import { AlertService } from '@/base/service/alert.service';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import { EMPTY, of } from 'rxjs';
import { DateiDto, ReferenzDto, Validierungshinweis } from '@api/stammdaten';
import { StammdatenImportAdapterService } from '@/base/service/stammdaten-import-adapter.service';
import spyOn = jest.spyOn;

describe('ImportModalComponent', () => {
  let component: ImportModalComponent;
  let fixture: ComponentFixture<ImportModalComponent>;

  let importService: StammdatenImportAdapterService;

  const berichtsjahr = {
    schluessel: '2018',
  } as ReferenzDto;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImportModalComponent],
      providers: [
        { provide: JobStatusService, useValue: MockService(JobStatusService) },
        { provide: AlertService, useValue: MockService(AlertService) },
        MockProvider(StammdatenImportAdapterService),
      ],
      imports: [MockModule(ClarityModule), MockModule(ReactiveFormsModule)],
    }).compileComponents();
  });

  beforeEach(inject([StammdatenImportAdapterService], (service) => {
    fixture = TestBed.createComponent(ImportModalComponent);
    component = fixture.componentInstance;
    component.importService = service;
    importService = service;
    component.berichtsjahr = berichtsjahr;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Happy Path', () => {
    let fileList: FileList;
    let file: File;
    const dateiName = 'dateiName';
    const datei = {
      dateiName,
    } as DateiDto;

    describe('onUploadChanged Datei auf Server vorhanden', () => {
      beforeEach(() => {
        fileList = {
          0: file,
          length: 1,
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          item: (index: number) => file,
        };
        spyOn(importService, 'aktuelleDatei').mockReturnValue(of(datei));
      });

      it('dateiname should be equal', () => {
        component.onUploadChanged(fileList);

        expect(component.existingDateiName).toEqual(dateiName);
      });
    });

    describe('onUploadChanged Datei nicht vorhanden', () => {
      beforeEach(() => {
        fileList = {
          0: file,
          length: 1,
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          item: (index: number) => file,
        };
        spyOn(importService, 'aktuelleDatei').mockReturnValue(of({}));
        component.toBeUploaded = file;
        spyOn(importService, 'upload');
      });

      it('dateiname should be equal', () => {
        component.onUploadChanged(fileList);

        expect(component.existingDateiName).toBeUndefined();
        expect(component.dateiStatusPruefen).toBeFalsy();
      });
    });

    describe('validierungStarten', () => {
      const hinweis = {
        hinweis: 'hinweis',
      } as Validierungshinweis;

      beforeEach(() => {
        spyOn(importService, 'validiere').mockReturnValue(of([hinweis]));
      });

      it('should contain hinweis', () => {
        component.validierungStarten();

        expect(component.hinweise).toContain(hinweis);
      });
    });

    describe('startImport', () => {
      beforeEach(() => {
        spyOn(importService, 'import').mockReturnValue(EMPTY);
      });

      it('should close dialog', () => {
        component.startImport();

        expect(component.open).toBeFalsy();
      });
    });
  });
});
