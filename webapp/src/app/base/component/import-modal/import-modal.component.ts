import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { AlertService, ErrorAttributes } from '@/base/service/alert.service';
import { JobStatusService } from '@/base/service/job-status.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DateiDto, ReferenzDto, Validierungshinweis } from '@api/stammdaten';
import { ClarityIcons, fileIcon, uploadIcon } from '@cds/core/icon';
import SeverityEnum = Validierungshinweis.SeverityEnum;

interface Alert {
  type: 'danger' | 'success' | 'warning' | 'info';
  text: string;
}

export interface ImportService {
  import(jahrSchuessel: string): Observable<any>;

  aktuelleDatei(): Observable<DateiDto>;

  upload(datei: File): Observable<any>;

  validiere(jahrSchuessel?: string): Observable<Array<Validierungshinweis>>;

  dateiLoeschen(): Observable<any>;
}

ClarityIcons.addIcons(uploadIcon, fileIcon);

@Component({
  selector: 'app-import-modal',
  templateUrl: './import-modal.component.html',
  styleUrls: ['./import-modal.component.scss'],
})
export class ImportModalComponent {
  constructor(
    private readonly alertService: AlertService,
    private readonly jobStatusService: JobStatusService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  get infoText(): string {
    if (this.uploadRunning) {
      return this.fileName + ' wird hochgeladen ...';
    }
    if (this.validiert) {
      return 'Validierung wird durchgeführt ...';
    }
    if (this.dateiStatusPruefen) {
      return 'Dateistatus wird überprüft ...';
    }
    return 'Klicken oder Datei hereinziehen, um Upload zu starten';
  }

  get importBereit(): boolean {
    return this.fileName && !this.loading && this.alert?.type !== 'danger';
  }

  get showUpload(): boolean {
    return !this.fileName || this.loading;
  }

  get loading(): boolean {
    return this.uploadRunning || this.validiert || this.dateiStatusPruefen;
  }

  get showHinweise(): boolean {
    return this.hinweise.length > 0 && !this.loading;
  }

  get validierungsFehler(): Array<Validierungshinweis> {
    return this.hinweise.filter(
      (hinweis) => hinweis.severity === SeverityEnum.FEHLER
    );
  }

  get validierungsWarnungen(): Array<Validierungshinweis> {
    return this.hinweise.filter(
      (hinweis) => hinweis.severity === SeverityEnum.WARNUNG
    );
  }

  @Input() open: boolean;
  @Input() importService: ImportService;
  @Input() berichtsjahr: ReferenzDto;
  @Input() title: string;

  @Output() openChange = new EventEmitter<boolean>();

  @ViewChild('fileUploadInput')
  private uploadInput: ElementRef;

  private uploadSubscriptions: Array<Subscription> = [];

  infoText$: BehaviorSubject<string> = new BehaviorSubject<string>(
    'Klicken oder Datei hereinziehen, um Upload zu starten'
  );
  toBeUploaded: File;
  fileName: string;
  uploadRunning = false;
  validiert: boolean;
  dateiStatusPruefen: boolean;
  existingDateiName: string;
  hinweise: Array<Validierungshinweis> = [];
  alert: Alert;

  // Für Import
  private static getAlert(hinweise: Array<Validierungshinweis>): Alert {
    if (hinweise.length === 0) {
      return {
        text: 'Erfolg!',
        type: 'success',
      };
    }
    if (hinweise.some((hinweis) => hinweis.severity === SeverityEnum.FEHLER)) {
      return {
        text: 'Es liegen Validierungsfehler oder Fehler in der Datei vor. Der Import kann nicht gestartet werden.',
        type: 'danger',
      };
    }
    return {
      text: 'Es liegen Warnungen vor. Bitte prüfen Sie diese, bevor Sie den Import starten.',
      type: 'warning',
    };
  }

  changeOpen(isOpen: boolean): void {
    this.open = isOpen;
    this.openChange.emit(isOpen);
  }

  closeImportDialog(): void {
    // this.open = false;
    // Zustand zurücksetzen
    this.alert = null;
    this.fileName = null;
    this.toBeUploaded = null;
    this.uploadRunning = false;
    this.validiert = false;
    this.dateiStatusPruefen = false;
    this.existingDateiName = null;
    this.hinweise = [];
    this.uploadSubscriptions.forEach((sub) => sub.unsubscribe());
    this.changeOpen(false);
  }

  startImport(): void {
    this.importService.import(this.berichtsjahr?.schluessel).subscribe(
      () => {
        this.closeImportDialog();
        this.jobStatusService.jobStarted();
      },
      (error) => {
        this.closeImportDialog();
        this.alertService.setAlert(error);
      }
    );
  }

  onUploadChanged(files: FileList): void {
    if (files.length > 1) {
      this.alert = {
        type: 'danger',
        text: 'Es darf nicht mehr als eine Datei hochgeladen werden.',
      };
      return;
    }

    this.toBeUploaded = files.item(0);
    this.dateiStatusPruefen = true;
    this.uploadSubscriptions.push(
      this.importService
        .aktuelleDatei()
        .pipe(tap(() => this.changeDetectorRef.markForCheck()))
        .subscribe(
          (datei) => {
            if (datei.dateiName) {
              this.existingDateiName = datei.dateiName;
            } else {
              this.dateiStatusPruefen = false;
              this.doUpload();
            }
          },
          (err) => {
            this.dateiStatusPruefen = false;
            this.alertService.setAlert(err);
          }
        )
    );
  }

  private doUpload(): void {
    this.fileName = this.toBeUploaded.name;
    this.uploadRunning = true;
    this.infoText$.next(this.fileName + ' wird hochgeladen ...');
    this.uploadSubscriptions.push(
      this.importService
        .upload(this.toBeUploaded)
        .pipe(tap(() => this.changeDetectorRef.markForCheck()))
        .subscribe(
          () => {
            this.toBeUploaded = null;
            this.uploadRunning = false;
            this.validierungStarten();
          },
          (error) => {
            this.toBeUploaded = null;
            this.uploadRunning = false;
            this.alert = {
              type: 'danger',
              text: error,
            };
          }
        )
    );
  }

  validierungStarten(): void {
    this.validiert = true;
    this.uploadSubscriptions.push(
      this.importService
        .validiere(this.berichtsjahr?.schluessel)
        .pipe(tap(() => this.changeDetectorRef.markForCheck()))
        .subscribe(
          (hinweise) => {
            this.hinweise = hinweise;
            this.alert = ImportModalComponent.getAlert(hinweise);
            this.validiert = false;
          },
          (error) => {
            const errorAttributes: ErrorAttributes = error;
            this.validiert = false;
            this.alert = {
              type: 'danger',
              text: errorAttributes.error,
            };
            this.changeDetectorRef.markForCheck();
          }
        )
    );
  }

  drop(event: DragEvent): void {
    event.preventDefault();
    if (!this.loading) {
      this.onUploadChanged(event.dataTransfer.files);
    }
  }

  dragover(e: DragEvent): void {
    e.preventDefault();
  }

  ueberschreiben(): void {
    this.existingDateiName = null;
    this.dateiStatusPruefen = false;
    this.doUpload();
  }

  nichtUeberschreiben(): void {
    this.fileName = this.existingDateiName;
    this.existingDateiName = null;
    this.dateiStatusPruefen = false;
    this.validierungStarten();
  }

  onUploadClicked(): void {
    if (!this.loading) {
      this.uploadInput.nativeElement.click();
    }
  }

  dateiLoeschen(): void {
    this.uploadSubscriptions.push(
      this.importService.dateiLoeschen().subscribe(
        () => {
          this.fileName = null;
          this.hinweise = [];
          this.alert = null;
          this.changeDetectorRef.markForCheck();
        },
        (error) => {
          this.alert = {
            type: 'danger',
            text: error,
          };
          this.changeDetectorRef.markForCheck();
        }
      )
    );
  }
}
