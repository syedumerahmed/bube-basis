import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { AlertComponent } from './alert.component';
import { MockModule, MockService } from 'ng-mocks';
import { ClarityModule } from '@clr/angular';
import {
  NavigationEnd,
  NavigationStart,
  Router,
  RouterEvent,
} from '@angular/router';
import { Subject } from 'rxjs';
import { AlertService } from '@/base/service/alert.service';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  const routerEvent$ = new Subject<RouterEvent>();
  const routerMock = { events: routerEvent$.asObservable() };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AlertComponent],
      providers: [
        { provide: Router, useValue: routerMock },
        { provide: AlertService, useValue: MockService(AlertService) },
      ],
      imports: [MockModule(ClarityModule)],
    }).compileComponents();

    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on route change', () => {
    it('should clear alert text', inject([AlertService], (alertService) => {
      routerEvent$.next(new NavigationStart(1, '/stammdaten'));
      expect(alertService.clearAlertMessages).toHaveBeenCalled();
    }));
    it('should not clear alert text when no Start Event', inject(
      [AlertService],
      (alertService) => {
        routerEvent$.next(new NavigationEnd(1, '/stammdaten', ''));
        expect(alertService.clearAlertMessages).not.toHaveBeenCalled();
      }
    ));
  });
});
