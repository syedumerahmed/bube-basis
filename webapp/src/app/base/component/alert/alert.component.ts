import { Component, OnInit } from '@angular/core';
import {
  AlertMessage,
  AlertService,
  AlertTyp,
} from '@/base/service/alert.service';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { JobActionDto } from '@api';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  private static readonly ALERT_TYPES: Record<AlertTyp, string> = {
    [AlertTyp.ALERT]: 'danger',
    [AlertTyp.WARNING]: 'warning',
    [AlertTyp.SUCCESS]: 'success',
  };

  constructor(
    private readonly service: AlertService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationStart))
      .subscribe(() => this.service.clearAlertMessages());
  }

  getAlertMessages(): AlertMessage[] {
    return this.service.getAlertMessages();
  }

  clearAlertMessages(): void {
    this.service.clearAlertMessages();
  }

  clearAlertMessage(alertMessage: AlertMessage): void {
    this.service.clearAlertMessage(alertMessage);
  }

  getType(alertMessage: AlertMessage): string {
    return AlertComponent.ALERT_TYPES[alertMessage.typ];
  }

  navigateToAction(action: JobActionDto): void {
    this.router.navigateByUrl(action.route, { state: action.payload });
  }
}
