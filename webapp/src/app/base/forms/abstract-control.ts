import { AbstractControl as NgAbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';

import { Options } from './form-types';

export type Path<T> =
  | [Extract<keyof T, string>, ...Array<string>]
  | Extract<keyof T, string>
  | string;

export abstract class AbstractControl<T> extends NgAbstractControl {
  readonly value: T;
  readonly valueChanges: Observable<T>;

  abstract setValue(value: T, options?: Options): void;

  abstract patchValue(value: T, options?: Options): void;

  abstract reset(value?: T, options?: Options): void;

  get<U extends Extract<keyof T, string>>(path: U): AbstractControl<T[U]>;
  get<U>(path: Path<T>): AbstractControl<U> | null {
    return super.get(path);
  }

  getError(errorCode: string, path?: Path<T>): any {
    return super.getError(errorCode, path);
  }

  hasError(errorCode: string, path?: Path<T>): boolean {
    return super.hasError(errorCode, path);
  }
}
