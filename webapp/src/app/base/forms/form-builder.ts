import {
  AbstractControlOptions,
  FormBuilder as NgFormBuilder,
} from '@angular/forms';
import { Injectable } from '@angular/core';

import { FormGroup } from './form-group';
import { FormControl } from './form-control';
import { FormArray } from './form-array';
import {
  AsyncValidator,
  FormBuilderControlOption,
  ValidatorOrOpts,
} from './form-types';

type GroupParams<T> = { [key in keyof T]: FormBuilderControlOption<T[key]> };

@Injectable({
  providedIn: 'root',
})
export class FormBuilder extends NgFormBuilder {
  group<T>(
    controlsConfig: GroupParams<T>,
    options?: AbstractControlOptions | null
  ): FormGroup<T> {
    return super.group(controlsConfig, options) as FormGroup<T>;
  }

  control<T>(
    formState: FormBuilderControlOption<T>,
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidator
  ): FormControl<T> {
    return super.control(formState, validatorOrOpts, asyncValidator);
  }

  array<T>(
    controlsConfig: FormBuilderControlOption<T>[],
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidator
  ): FormArray<T> {
    return super.array(controlsConfig, validatorOrOpts, asyncValidator);
  }
}
