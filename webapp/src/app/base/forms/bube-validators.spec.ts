import { FormControl } from '@angular/forms';
import { BubeValidators } from '@/base/forms/bube-validators';

describe('BubeValidators', () => {
  describe('url', () => {
    it.each([
      // http scheme is accepted
      'http://www.wps.de',
      'http://wps.de',
      // TLD with length > 6 is accepted
      'http://www.wps.hamburg',
      'http://wps.hamburg',
      // https scheme is accepted
      'https://www.wps.hamburg',
      // paths, queries and fragments are accepted (in combinations, too)
      'https://www.wps.hamburg/deep-link',
      'https://www.wps.hamburg?query1=value1?query2=value2',
      'https://www.wps.hamburg#anchor',
      'https://www.wps.hamburg/deep-link?query1=value1?query2=value2',
      'https://www.wps.hamburg/deep-link#anchor',
      // port numbers are accepted
      'http://www.wps.hamburg:8080',
      'http://www.wps.hamburg:8080/deep-link',
      'http://www.wps.hamburg:8080?query1=value1?query2=value2',
      'http://www.wps.hamburg:8080#anchor',
      'http://www.wps.hamburg:8080/deep-link?query1=value1?query2=value2',
      'http://www.wps.hamburg:8080/deep-link#anchor',
    ])('%s should be valid', (url) => {
      expect(BubeValidators.url(new FormControl(url))).toBeNull();
    });

    it.each([
      'http::/www.wps.de', // wrong scheme/host separator
      'www.wps.de', //        missing scheme
      'ftp://www.wps.de', //  invalid scheme
    ])('%s should have invalid scheme', (url) => {
      expect(BubeValidators.url(new FormControl(url))).toHaveProperty(
        'url.invalid_scheme'
      );
    });

    it.each([
      'https://www.wps.hamburg ', //  illegal character in host
      'https://www.wps.münchen', //   illegal character in host
      'https://www.wps.hamburg{}', // illegal separator after host
      'https://www.wps.d', //         length 1 TLD
    ])('%s should be invalid', (url) => {
      expect(BubeValidators.url(new FormControl(url))).toHaveProperty('url');
    });
  });

  describe('maxDecimal', () => {
    it.each([
      12345, 12345.1, 12345.12, 12345.123, 0.123, 1.123, 12.123, 123.123,
      1234.123, 0.1, 1, 0, 0.0,
    ])('%s should be valid', (num) => {
      expect(BubeValidators.maxDecimal(5, 3)(new FormControl(num))).toBeNull();
    });

    it.each([123456, 123456, 123456.1])(
      '%s should be invalid and have preDecimalError',
      (num) => {
        expect(
          BubeValidators.maxDecimal(5, 3)(new FormControl(num))
        ).toHaveProperty('preDecimalError');
      }
    );

    it.each([0.1234, 0.1234])(
      '%s should be invalid and have decimalError',
      (num) => {
        expect(
          BubeValidators.maxDecimal(5, 3)(new FormControl(num))
        ).toHaveProperty('decimalError');
      }
    );
  });

  describe('email', () => {
    it.each([
      'bube-dev@wps.de',
      /*'spam+bube-dev@wps.de' Technisch eigentlich valide, aber ich habe keine Lust, das Pattern bei der XML-Validierung anzupassen */
    ])('%s should be valid', (email) => {
      expect(BubeValidators.email(new FormControl(email))).toBeNull();
    });

    it.each(['bube-dev@wps', 'bube-dev<AT>wps.de', 'bube-dev@', '@wps.de'])(
      '%s should be invalid and have email Error',
      (email) => {
        expect(BubeValidators.email(new FormControl(email))).toHaveProperty(
          'email'
        );
      }
    );
  });
});
