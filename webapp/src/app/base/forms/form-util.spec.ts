import { FormUtil } from '@/base/forms/form-util';

describe('FormUtil', () => {
  describe('emptyStringsToNull', () => {
    it('should handle null', () => {
      expect(FormUtil.emptyStringsToNull(null)).toBeNull();
    });
    it('should handle undefined', () => {
      expect(FormUtil.emptyStringsToNull(undefined)).toBeUndefined();
    });
    it('should handle NaN', () => {
      expect(FormUtil.emptyStringsToNull(NaN)).toBeNaN();
    });
    it('should handle empty string', () => {
      expect(FormUtil.emptyStringsToNull('')).toBeNull();
    });
    it('should handle string', () => {
      expect(FormUtil.emptyStringsToNull('string')).toBe('string');
    });
    it('should handle empty Array', () => {
      expect(FormUtil.emptyStringsToNull([])).toStrictEqual([]);
    });
    it('should handle string Array', () => {
      expect(
        FormUtil.emptyStringsToNull(['string', '', 'notEmpty'])
      ).toStrictEqual(['string', null, 'notEmpty']);
    });
    it('should handle mixed Array', () => {
      expect(
        FormUtil.emptyStringsToNull(['string', '', undefined, {}, 0])
      ).toStrictEqual(['string', null, undefined, {}, 0]);
    });
    it('should handle empty Object', () => {
      expect(FormUtil.emptyStringsToNull({})).toStrictEqual({});
    });
    it('should handle object with strings', () => {
      expect(
        FormUtil.emptyStringsToNull({ prop: 'string', prop2: '' })
      ).toStrictEqual({ prop: 'string', prop2: null });
    });
    it('should handle object with mixed properties', () => {
      const obj = {
        prop: 'string',
        prop2: '',
        prop3: 0,
        prop4: () => true,
        prop5: {},
        prop6: null,
        prop7: [],
      };
      expect(FormUtil.emptyStringsToNull(obj)).toStrictEqual({
        ...obj,
        prop2: null,
      });
    });

    it('should handle object nested object recursively', () => {
      const obj = {
        prop: {
          stringProp: '',
        },
      };
      expect(FormUtil.emptyStringsToNull(obj)).toStrictEqual({
        prop: { stringProp: null },
      });
    });

    it('should handle object nested array recursively', () => {
      const obj = {
        prop: ['', 'string'],
      };
      expect(FormUtil.emptyStringsToNull(obj)).toStrictEqual({
        prop: [null, 'string'],
      });
    });

    it('should handle array nested object recursively', () => {
      const array = [
        {
          stringProp: '',
        },
      ];
      expect(FormUtil.emptyStringsToNull(array)).toStrictEqual([
        {
          stringProp: null,
        },
      ]);
    });

    it('should handle array nested array recursively', () => {
      const obj = [['', 'string']];
      expect(FormUtil.emptyStringsToNull(obj)).toStrictEqual([
        [null, 'string'],
      ]);
    });
  });
});
