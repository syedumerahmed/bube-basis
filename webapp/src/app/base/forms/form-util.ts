type ObjWithId = { id: number };

export class FormUtil {
  static emptyStringsToNull<T>(obj: T): T {
    if (!obj || typeof obj !== 'object') {
      return typeof obj === 'string' ? obj || null : obj;
    } else if (Array.isArray(obj)) {
      return obj.map(FormUtil.emptyStringsToNull) as unknown as T;
    }
    return Object.entries(obj)
      .map(([key, value]: [string, unknown]) => {
        return [key, FormUtil.emptyStringsToNull(value)];
      })
      .reduce(
        (acc, [k, v]: [string, unknown]) => ({ ...acc, [k]: v }),
        {}
      ) as T;
  }

  static byIdComparison(a: ObjWithId, b: ObjWithId): boolean {
    return a?.id === b?.id;
  }

  static fromIsoDate(str: string): string {
    if (!str) {
      return null;
    } else {
      const pattern = /(\d{2,4})-(\d{1,2}).(\d{1,2})/;
      return str.replace(pattern, '$3.$2.$1');
    }
  }

  static fromIsoDateTime(str: string): string {
    if (!str) {
      return null;
    } else {
      const pattern = /(\d{2,4})-(\d{1,2})-(\d{1,2})[^\d]+([:\d]*)([^\d].*)?$/;
      return str.replace(pattern, '$3.$2.$1 $4');
    }
  }

  static toIsoDate(str: string): string {
    if (!str) {
      return null;
    } else {
      const pattern = /(\d{1,2})\.(\d{1,2}).(\d{2,4})/;
      return str.replace(pattern, '$3-$2-$1');
    }
  }
}
