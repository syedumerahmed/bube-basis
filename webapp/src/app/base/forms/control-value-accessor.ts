import { ControlValueAccessor as NgControlValueAccessor } from '@angular/forms';

export interface ControlValueAccessor<T> extends NgControlValueAccessor {
  writeValue(obj: T): void;

  registerOnChange(fn: (value: T) => void): void;

  registerOnTouched(fn: () => void): void;
}
