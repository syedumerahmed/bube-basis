import { FormArray as NgFormArray } from '@angular/forms';
import { Observable } from 'rxjs';

import { AbstractControl } from './abstract-control';
import {
  AsyncValidator,
  ControlValue,
  Options,
  ValidatorOrOpts,
} from './form-types';

export class FormArray<T> extends NgFormArray {
  constructor(
    controls: Array<AbstractControl<T>>,
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidator
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  readonly value: Array<T>;
  readonly valueChanges: Observable<Array<T>>;

  controls: AbstractControl<T>[];

  at(index: number): AbstractControl<T> {
    return super.at(index);
  }

  push(control: AbstractControl<T>): void {
    super.push(control);
  }

  insert(index: number, control: AbstractControl<T>): void {
    super.insert(index, control);
  }

  setControl(index: number, control: AbstractControl<T>): void {
    super.setControl(index, control);
  }

  setValue(value: Array<T>, options?: Options): void {
    super.setValue(value, options);
  }

  patchValue(value: Array<Partial<T> | T>, options?: Options): void {
    super.patchValue(value, options);
  }

  reset(
    value: { [key in keyof T]: ControlValue<T[key]> }[],
    options?: Options
  ): void {
    super.reset(value, options);
  }

  getRawValue(): Array<T> {
    return super.getRawValue();
  }
}
