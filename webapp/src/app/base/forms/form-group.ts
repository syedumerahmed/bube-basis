import { FormGroup as NgFormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { AbstractControl, Path } from './abstract-control';
import {
  AsyncValidator,
  ControlValue,
  Options,
  ValidatorOrOpts,
} from './form-types';

type RecursivePartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U> ? Array<Value<U>> : Value<T[P]>;
};
type AllowedPrimitives = boolean | string | number;
type Value<T> = T extends AllowedPrimitives ? T : RecursivePartial<T>;

export class FormGroup<T> extends NgFormGroup implements AbstractControl<T> {
  constructor(
    controls: { [key in keyof T]: AbstractControl<T[key]> },
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidator
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  readonly value: T;
  readonly valueChanges: Observable<T>;

  controls: {
    [key in keyof T]: AbstractControl<T[key]>;
  };

  setControl<U extends Extract<keyof T, string>>(
    name: U,
    control: AbstractControl<T[U]>
  ): void {
    super.setControl(name, control);
  }

  setValue(value: T, options?: Options): void {
    super.setValue(value, options);
  }

  patchValue(value: RecursivePartial<T>, options?: Options): void {
    super.patchValue(value, options);
  }

  reset(
    value?: { [key in keyof T]: ControlValue<T[key]> },
    options?: Options
  ): void {
    super.reset(value, options);
  }

  get<U extends Extract<keyof T, string>>(path: U): AbstractControl<T[U]>;
  get<U>(path: Path<T>): AbstractControl<U> | null;
  get<U>(path: Path<T>): AbstractControl<U> | null {
    return super.get(path);
  }

  getError(errorCode: string, path?: Path<T>): any {
    return super.getError(errorCode, path);
  }

  hasError(errorCode: string, path?: Path<T>): boolean {
    return super.hasError(errorCode, path);
  }

  getRawValue(): T {
    return super.getRawValue();
  }
}
