import { FormControl as NgFormControl } from '@angular/forms';
import { Observable } from 'rxjs';

import {
  AsyncValidator,
  ControlValue,
  Options,
  ValidatorOrOpts,
} from './form-types';

export class FormControl<T> extends NgFormControl {
  readonly value: T;
  readonly valueChanges: Observable<T>;

  constructor(
    formState: ControlValue<T>,
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidator
  ) {
    super(formState, validatorOrOpts, asyncValidator);
  }

  setValue(value: T, options?: Options): void {
    super.setValue(value, options);
  }

  patchValue(value: T, options?: Options): void {
    super.patchValue(value, options);
  }

  reset(value?: ControlValue<T>, options?: Options): void {
    super.reset(value, options);
  }
}
