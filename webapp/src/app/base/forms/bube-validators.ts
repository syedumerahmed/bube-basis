import { ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AbstractControl } from '@/base/forms/abstract-control';
import { ReferenzDto } from '@api/stammdaten';

export class BubeValidators {
  private static readonly EMAIL_PATTERN =
    '([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})';

  private static readonly DATE_PATTERN =
    '((0?[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))\\.((0?[1-9])|(1[0-2]))\\.([1-2]\\d{3})';

  /**
   * The URL scheme we are accepting must either be "http" or "https".
   */
  private static readonly URL_SCHEME = 'https?://';

  /**
   * The host name must<br/>
   * <ul>
   *   <li>start with a letter or digit,
   *   <li>consist of letters, digits, dashes and dots,
   *   <li>be between 1 and 253 characters long,
   *   <li>consist of (dot-separated) labels, each one being between 1 and 63 characters long,
   *   <li>end with a TLD label consisting of at least 2 characters.
   * </ul>
   * @see https://en.wikipedia.org/wiki/Hostname
   */
  private static readonly URL_HOST =
    '(?=[a-zA-Z0-9][a-zA-Z0-9-.]{0,252})(((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\\.)+[a-zA-Z]{2,63})';

  /**
   * The port (if specified) must be a decimal integer between 1 and 65535. Since this is difficult to
   * check with regular expressions, we simply enforce the port number to be at most 5 digits long.
   */
  private static readonly URL_PORT = '(:\\d{1,5})?';

  /**
   * The remainder of the URL consists of
   * <ol>
   *   <li>a <em>path</em>, starting with a slash ('/'),
   *   <li>a <em>query</em>, starting with a question mark ('?')
   *   <li>a <em>fragment</em>, starting with a hash sign ('#').
   * </ol>
   * Each of these constituents is optional and can consist of ASCII letters, digits as well as several
   * special characters.
   */
  private static readonly URL_PATH = '([/?#][-a-zA-Z0-9()@:%_+.~#?&/=]*)?';

  private static readonly URL_SCHEME_REGEX = new RegExp(
    `^${BubeValidators.URL_SCHEME}.*$`
  );
  private static readonly URL_REGEX = new RegExp(
    `^${BubeValidators.URL_SCHEME}${BubeValidators.URL_HOST}${BubeValidators.URL_PORT}${BubeValidators.URL_PATH}$`
  );

  static date(control: AbstractControl<string>): ValidationErrors {
    return BubeValidators.renamePatternError(
      Validators.pattern(BubeValidators.DATE_PATTERN)(control),
      'date'
    );
  }

  static email(control: AbstractControl<string>): ValidationErrors {
    return BubeValidators.renamePatternError(
      Validators.pattern(BubeValidators.EMAIL_PATTERN)(control),
      'email'
    );
  }

  static url(control: AbstractControl<string>): ValidationErrors {
    // 1. Ensure that the URL starts with a valid scheme (otherwise return an error)
    const invalid_scheme = BubeValidators.renamePatternError(
      Validators.pattern(BubeValidators.URL_SCHEME_REGEX)(control),
      'invalid_scheme'
    );
    if (invalid_scheme) {
      return { url: { invalid_scheme } };
    }
    // 2. If the scheme is valid,  ensure that the rest of the URL is valid (otherwise return a different error).
    return BubeValidators.renamePatternError(
      Validators.pattern(BubeValidators.URL_REGEX)(control),
      'url'
    );
  }

  private static renamePatternError(
    patternError: ValidationErrors,
    name: string
  ): ValidationErrors {
    return patternError ? { [name]: patternError.pattern } : null;
  }

  static areReferencesValid(currentYear: number): ValidatorFn {
    return (control: AbstractControl<ReferenzDto[]>) => {
      const invalidReferences = control.value
        ? control.value
            .map((referenz) => this.isValidInYear(referenz, currentYear))
            .filter(Boolean)
            .map((validationError) => validationError.invalidReference)
        : [];
      return invalidReferences.length ? { invalidReferences } : null;
    };
  }

  static isReferenceValid(currentYear: number): ValidatorFn {
    return (control: AbstractControl<ReferenzDto>) => {
      return this.isValidInYear(control.value, currentYear);
    };
  }

  private static isValidInYear(
    referenz: ReferenzDto,
    currentYear: number
  ): ValidationErrors {
    if (referenz == null) {
      return null;
    }

    if (
      referenz.gueltigVon > currentYear ||
      referenz.gueltigBis < currentYear
    ) {
      return {
        invalidReference: referenz,
      };
    }
    return null;
  }

  static maxDecimal(
    maxPreDecimalLength: number,
    maxDecimalLength: number
  ): ValidatorFn {
    return (control: AbstractControl<number>) => {
      if (control.value == null || !isFinite(control.value)) {
        return null;
      }
      const splittedNumber = control.value.toString().split('.');
      const preDecimals = splittedNumber[0];
      const decimals = splittedNumber[1];
      if (preDecimals && preDecimals.length > maxPreDecimalLength) {
        return {
          preDecimalError: {
            maxPredecimalLength: maxPreDecimalLength,
            actualLength: preDecimals.length,
          },
        };
      } else if (decimals && decimals.length > maxDecimalLength) {
        return {
          decimalError: { maxDecimalLength, actualLength: decimals.length },
        };
      }
      return null;
    };
  }
}
