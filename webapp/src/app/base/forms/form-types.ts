import {
  AbstractControlOptions,
  AsyncValidatorFn,
  ValidatorFn,
} from '@angular/forms';
import { AbstractControl } from '@/base/forms/abstract-control';

export type ValidatorOrOpts =
  | ValidatorFn
  | ValidatorFn[]
  | AbstractControlOptions;
export type AsyncValidator = AsyncValidatorFn | AsyncValidatorFn[];
export type ControlValue<T> = T | { value: T; disabled: boolean };
export type FormBuilderControlOption<T> =
  | ControlValue<T>
  | AbstractControl<T>
  | [ControlValue<T>, ValidatorOrOpts?, AsyncValidator?];

export type Options = {
  onlySelf?: boolean;
  emitEvent?: boolean;
};
