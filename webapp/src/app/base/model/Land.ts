export enum Land {
  'DEUTSCHLAND' = 'DEUTSCHLAND',
  'SCHLESWIG_HOLSTEIN' = 'SCHLESWIG_HOLSTEIN',
  'HAMBURG' = 'HAMBURG',
  'NIEDERSACHSEN' = 'NIEDERSACHSEN',
  'BREMEN' = 'BREMEN',
  'NORDRHEIN_WESTFALEN' = 'NORDRHEIN_WESTFALEN',
  'HESSEN' = 'HESSEN',
  'RHEINLAND_PFALZ' = 'RHEINLAND_PFALZ',
  'BADEN_WUERTTEMBERG' = 'BADEN_WUERTTEMBERG',
  'BAYERN' = 'BAYERN',
  'SAARLAND' = 'SAARLAND',
  'BERLIN' = 'BERLIN',
  'BRANDENBURG' = 'BRANDENBURG',
  'MECKLENBURG_VORPOMMERN' = 'MECKLENBURG_VORPOMMERN',
  'SACHSEN' = 'SACHSEN',
  'SACHSEN_ANHALT' = 'SACHSEN_ANHALT',
  'THUERINGEN' = 'THUERINGEN',
}

export const LandProperties: Record<Land, { nr: string; display: string }> = {
  DEUTSCHLAND: { nr: '00', display: 'Deutschland' },
  SCHLESWIG_HOLSTEIN: { nr: '01', display: 'Schleswig-Holstein' },
  HAMBURG: { nr: '02', display: 'Hamburg' },
  NIEDERSACHSEN: { nr: '03', display: 'Niedersachsen' },
  BREMEN: { nr: '04', display: 'Bremen' },
  NORDRHEIN_WESTFALEN: { nr: '05', display: 'Nordrhein-Westfalen' },
  HESSEN: { nr: '06', display: 'Hessen' },
  RHEINLAND_PFALZ: { nr: '07', display: 'Rheinland-Pfalz' },
  BADEN_WUERTTEMBERG: { nr: '08', display: 'Baden-Württemberg' },
  BAYERN: { nr: '09', display: 'Bayern' },
  SAARLAND: { nr: '10', display: 'Saarland' },
  BERLIN: { nr: '11', display: 'Berlin' },
  BRANDENBURG: { nr: '12', display: 'Brandenburg' },
  MECKLENBURG_VORPOMMERN: { nr: '13', display: 'Mecklenburg-Vorpommern' },
  SACHSEN: { nr: '14', display: 'Sachsen' },
  SACHSEN_ANHALT: { nr: '15', display: 'Sachsen-Anhalt' },
  THUERINGEN: { nr: '16', display: 'Thüringen' },
};

export const lookupLand: (nr: string) => Land | undefined = (nr) =>
  Object.entries(LandProperties).find(([, v]) => v.nr === nr)[0] as Land;
