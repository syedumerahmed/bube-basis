import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class BerichtsjahrGuard implements CanActivate {
  constructor(
    private readonly berichtsjahrService: BerichtsjahrService,
    private readonly router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<UrlTree | boolean> {
    if (route.params.berichtsjahr) {
      return this.berichtsjahrService
        .selectBerichtsjahr(route.params.berichtsjahr)
        .pipe(map(() => true));
    }
    return this.berichtsjahrService
      .selectDefaultBerichtsjahr()
      .pipe(
        map((berichtsjahr) =>
          this.router.createUrlTree([state.url, berichtsjahr.schluessel])
        )
      );
  }
}
