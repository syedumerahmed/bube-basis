import { inject, TestBed } from '@angular/core/testing';

import { BerichtsjahrGuard } from '@/base/routing/guard/berichtsjahr.guard';
import { BerichtsjahrService } from '@/base/service/berichtsjahr.service';
import { MockService } from 'ng-mocks';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { of } from 'rxjs';
import spyOn = jest.spyOn;
import { ReferenzDto } from '@api/stammdaten';

describe('BerichtsjahrGuard', () => {
  let guard: BerichtsjahrGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: BerichtsjahrService,
          useValue: MockService(BerichtsjahrService),
        },
        { provide: Router, useValue: MockService(Router) },
      ],
    });
    guard = TestBed.inject(BerichtsjahrGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return true if berichtsjahr is present in route data', inject(
    [BerichtsjahrService],
    async (berichtsjahrService: BerichtsjahrService) => {
      const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
      route.params = { berichtsjahr: '1234' };
      spyOn(berichtsjahrService, 'selectBerichtsjahr').mockReturnValue(
        of({} as ReferenzDto)
      );

      const result = await guard
        .canActivate(route, {} as RouterStateSnapshot)
        .toPromise();

      expect(result).toBe(true);
      expect(berichtsjahrService.selectBerichtsjahr).toHaveBeenCalledWith(
        route.params.berichtsjahr
      );
    }
  ));

  it('should redirect to latest berichtsjahr provided by service', inject(
    [BerichtsjahrService, Router],
    async (berichtsjahrService: BerichtsjahrService, router: Router) => {
      const expectedUrlTree: UrlTree = new UrlTree();
      spyOn(berichtsjahrService, 'selectDefaultBerichtsjahr').mockReturnValue(
        of({ schluessel: '12' } as ReferenzDto)
      );
      spyOn(router, 'createUrlTree').mockReturnValue(expectedUrlTree);

      const result = guard.canActivate(
        { params: {} } as ActivatedRouteSnapshot,
        { url: 'stammdaten/jahr' } as RouterStateSnapshot
      );

      expect(await result.toPromise()).toBe(expectedUrlTree);
      expect(router.createUrlTree).toHaveBeenCalledWith([
        'stammdaten/jahr',
        '12',
      ]);
    }
  ));
});
