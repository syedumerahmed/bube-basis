import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

export interface UnsavedChanges {
  canDeactivate(): boolean;

  showSaveWarning(): void;
}

@Injectable()
export class UnsavedChangesGuard implements CanDeactivate<UnsavedChanges> {
  canDeactivate(component: UnsavedChanges): boolean {
    if (component.canDeactivate()) {
      return true;
    } else {
      component.showSaveWarning();
      return false;
    }
  }
}
