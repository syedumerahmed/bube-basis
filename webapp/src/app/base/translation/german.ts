import { ClrCommonStrings } from '@clr/angular';

// s. webapp/node_modules/@clr/angular/esm2015/utils/i18n/common-strings.default.js for defaults
export const germanLocale: ClrCommonStrings = {
  delete: 'Löschen',
  filterItems: 'Filtern',
  selection: 'Auswählen',
  open: 'Öffnen',
  close: 'Schließen',
  show: 'Anzeigen',
  hide: 'Ausblenden',
  expand: 'Aufklappen',
  collapse: 'Zuklappen',
  more: 'Mehr',
  select: 'Auswählen',
  selectAll: 'Alle auswählen',
  previous: 'Vorherige',
  next: 'Nächste',
  current: 'Jump to current', // TODO Kontext unklar
  info: 'Info',
  success: 'Erfolg',
  warning: 'Warnung',
  danger: 'Fehler',
  rowActions: 'Verfügbare Aktionen',
  pickColumns: 'Spalten anzeigen oder ausblenden',
  showColumns: 'Spalten anzeigen',
  sortColumn: 'Sort Column', // TODO Kontext unklar
  firstPage: 'Erste Seite',
  lastPage: 'Letzte Seite',
  nextPage: 'Nächste Seite',
  previousPage: 'Vorherige Seite',
  currentPage: 'Aktuelle Seite',
  totalPages: 'Gesamtseitenanzahl',
  minValue: 'Minimum',
  maxValue: 'Maximum',
  modalContentStart: 'Anfang von Dialoginhalt',
  modalContentEnd: 'Ende von Dialoginhalt',
  showColumnsMenuDescription: 'Spaltenmenü anzeigen',
  allColumnsSelected: 'Alle Spalten ausgewählt',
  signpostToggle: 'Kontextinformationen auf- oder zuklappen',
  signpostClose: 'Kontextinformationen schließen',
  loading: 'Lädt',
  // Datagrid
  detailPaneStart: 'Anfang von Zeileninformation',
  detailPaneEnd: 'Ende von Zeileninformation',
  singleSelectionAriaLabel: 'Single selection header', // TODO Kontext unklar
  singleActionableAriaLabel: 'Single actionable header', // TODO Kontext unklar
  detailExpandableAriaLabel: 'Zeileninhalt aufklappen',
  datagridFilterAriaLabel: 'Spaltenfilter öffnen',
  columnSeparatorAriaLabel: 'Spaltenbreite anpassen',
  columnSeparatorDescription: 'Nutzung nicht nötig',
  // Alert
  alertCloseButtonAriaLabel: 'Benachrichtigung schließen',
  // Date Picker
  datepickerToggle: 'Datumsauswahl auf- oder zuklappen',
  datepickerPreviousMonth: 'Vorheriger Monat',
  datepickerCurrentMonth: 'Aktueller Monat',
  datepickerNextMonth: 'Nächster Monat',
  datepickerPreviousDecade: 'Vorheriges Jahrzehnt',
  datepickerNextDecade: 'Nächstes Jahrzehnt',
  datepickerCurrentDecade: 'Aktuelles Jahrzehnt',
  datepickerSelectMonthText:
    'Monat auswählen, die aktuelle Auswahl ist {CALENDAR_MONTH}',
  datepickerSelectYearText:
    'Jahr auswählen, die aktuelle Auswahl ist {CALENDAR_YEAR}',
  // Stack View
  stackViewChanged: 'Wert geändert',
  // Vertical Nav
  verticalNavGroupToggle: 'Navigationselement auf- oder zuklappen',
  verticalNavToggle: 'Navigation auf- oder zuklappen',
  // Timeline steps
  timelineStepNotStarted: 'Nicht begonnen',
  timelineStepCurrent: 'Aktuell',
  timelineStepSuccess: 'Abgeschlossen',
  timelineStepError: 'Fehler',
  timelineStepProcessing: 'Verarbeite',
  // Combobox
  comboboxDelete: 'Lösche Option',
  comboboxSearching: 'Suche nach "{INPUT}"',
  comboboxSelection: 'Auswahl',
  comboboxSelected: 'Ausgewählt',
  comboboxNoResults: 'Keine Treffer',
  comboboxOpen: 'Optionen anzeigen',
  // Datagrid expandable rows
  dategridExpandableBeginningOf: 'Anfang von',
  dategridExpandableEndOf: 'Ende von',
  dategridExpandableRowContent: 'Erweiterbarer Zeileninhalt',
  dategridExpandableRowsHelperText:
    'Bildschirmleser können mit erweiterbaren Zeilen möglicherweise nicht korrekt umgehen; bitte nutzen Sie die "Durchsuchen" Funktionalität, um den Inhalt zu überprüfen',
};
