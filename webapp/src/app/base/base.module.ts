import { NgModule } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import { CommonModule } from '@angular/common';
import { InputListComponent } from './component/input-list/input-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EllipsizePipe } from '@/base/texts/ellipsize.pipe';
import { FilenameDateFormatPipe } from '@/base/texts/filename.date.format.pipe';
import { ImportModalComponent } from '@/base/component/import-modal/import-modal.component';
import { TooltipComponent } from '@/base/component/tooltip/tooltip.component';
import { LandAuswahlComponent } from './component/land-auswahl/land-auswahl.component';
import { DatagridBooleanFilterComponent } from '@/base/component/datagrid-boolean-filter/datagrid-boolean-filter.component';
import { CompleteUriEncoderPipe } from './texts/complete.uri.encoder.pipe';
import { AlertComponent } from '@/base/component/alert/alert.component';
import { RoutingNotFoundComponent } from '@/base/component/routing-not-found/routing-not-found.component';
import { UnsavedChangesGuard } from '@/base/routing/guard/unsaved-changes-guard.service';
import { SidenavComponent } from './component/sidenav/sidenav.component';

@NgModule({
  declarations: [
    InputListComponent,
    EllipsizePipe,
    FilenameDateFormatPipe,
    ImportModalComponent,
    TooltipComponent,
    LandAuswahlComponent,
    DatagridBooleanFilterComponent,
    CompleteUriEncoderPipe,
    AlertComponent,
    RoutingNotFoundComponent,
    SidenavComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
  ],
  exports: [
    InputListComponent,
    EllipsizePipe,
    FilenameDateFormatPipe,
    ImportModalComponent,
    TooltipComponent,
    LandAuswahlComponent,
    DatagridBooleanFilterComponent,
    AlertComponent,
    SidenavComponent,
  ],
  providers: [CompleteUriEncoderPipe, UnsavedChangesGuard],
})
export class BaseModule {}
